/*******************************************App Config - Start******************************************************/

(function(_global) {
    var _appConfig = (function(jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "App Config");
        }

        var appName = "tata-capital-moneyfy";
        var contentRoot = "/content/" + appName;
        var apiRoot = contentRoot + "/shaft-api";
        var apiExtension = "";
        var jocataDomain =  typeof(jocataUrl) !== 'undefined' ? jocataUrl : '';
        return jsHelper.freezeObj({
            appName: appName,
            contentRoot: contentRoot,
            apiRoot: apiRoot,
            apiExtension: apiExtension,
            jocataDomain: jocataDomain
        });
    })(_global.jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'appConfig', _appConfig);
})(this);

/*******************************************App Config - Start******************************************************/