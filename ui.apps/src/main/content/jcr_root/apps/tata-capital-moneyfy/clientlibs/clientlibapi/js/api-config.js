/*******************************************API Config Module - Start******************************************************/
(function (_global) {
    var _apiConfig = (function (jsHelper) {
        if (exceptionUtility) {
            exceptionUtility.dependencyCheck([jsHelper], "API Config");
        }
        /**
         * API Constant Values
         */

        var DEMO = "DEMO";

        var DOWNLOAD_APP = "DOWNLOAD_APP";

        var GET_TOP_PICKS = "GET_TOP_PICKS";

        var CHOOSE_FROM_AMC = "CHOOSE_FROM_AMC";

        var FUND_MANAGER = "FUND_MANAGER";

        var COMPARE_PAGE = "COMPARE_PAGE";

        var SEARCH_API = "SEARCH_API";

        var POPULAR_CATEGORY_FUND_LIST = "POPULAR_CATEGORY_FUND_LIST";

        var FUNDS_VIA_FILTER = "FUNDS_VIA_FILTER";

        var GRAPH = "GRAPH";

        var ADD_TO_WATCH_LIST = "ADD_TO_WATCH_LIST";
        
        var FETCH_FROM_WATCH_LIST = "FETCH_FROM_WATCH_LIST";

        var FETCH_FROM_PORTFOLIO = "FETCH_FROM_PORTFOLIO";

        var WATCHLIST_PORTFOLIO_FUNDS_DATA = "WATCHLIST_PORTFOLIO_FUNDS_DATA";

        var TESTIMONAL_API = "TESTIMONAL_API";

        var CART_COUNT_API = "CART_COUNT_API";

        /**
         * API Constants 
         */

        var apiConstants = {};
        apiConstants[DEMO] = DEMO;
        apiConstants[DOWNLOAD_APP] = DOWNLOAD_APP;
        apiConstants[GET_TOP_PICKS] = GET_TOP_PICKS;
        apiConstants[CHOOSE_FROM_AMC]= CHOOSE_FROM_AMC;
        apiConstants[FUND_MANAGER]= FUND_MANAGER;
        apiConstants[COMPARE_PAGE]= COMPARE_PAGE ;
        apiConstants[SEARCH_API]= SEARCH_API;
        apiConstants[POPULAR_CATEGORY_FUND_LIST] = POPULAR_CATEGORY_FUND_LIST;
        apiConstants[FUNDS_VIA_FILTER] = FUNDS_VIA_FILTER;
        apiConstants[GRAPH] = GRAPH;
        apiConstants[ADD_TO_WATCH_LIST] = ADD_TO_WATCH_LIST;
        apiConstants[FETCH_FROM_WATCH_LIST] = FETCH_FROM_WATCH_LIST;
        apiConstants[FETCH_FROM_PORTFOLIO] = FETCH_FROM_PORTFOLIO;
        apiConstants[WATCHLIST_PORTFOLIO_FUNDS_DATA] = WATCHLIST_PORTFOLIO_FUNDS_DATA;
        apiConstants[TESTIMONAL_API] = TESTIMONAL_API;
        apiConstants[CART_COUNT_API] = CART_COUNT_API;
        /**
         * API Selectors
         */

        var apiSelectors = {};
        apiSelectors[DEMO] = "demo";
        apiSelectors[DOWNLOAD_APP] = "downloadSMS";
        apiSelectors[GET_TOP_PICKS] = "getMoneyfyTopPicksByClassification";
        apiSelectors[CHOOSE_FROM_AMC]="getFundsViaAmc";
        apiSelectors[FUND_MANAGER]="getFundsViaFundManagerName";
        apiSelectors[COMPARE_PAGE]="getComparePage";
        apiSelectors[SEARCH_API] = "getFunds";
        apiSelectors[POPULAR_CATEGORY_FUND_LIST] = "getThemeFundList";
		apiSelectors[FUNDS_VIA_FILTER] = "getFundsViaFilter";
        apiSelectors[GRAPH] = "getGraph";
        apiSelectors[ADD_TO_WATCH_LIST] = "addToWatchList";
        apiSelectors[FETCH_FROM_WATCH_LIST] = "fetchFromWatchList";
        apiSelectors[FETCH_FROM_PORTFOLIO] = "fetchFromPortfolio";
        apiSelectors[WATCHLIST_PORTFOLIO_FUNDS_DATA] = "getFundsData";
        apiSelectors[TESTIMONAL_API] = "mdmApi" + ".customerSpeakServlet.json";
        apiSelectors[CART_COUNT_API] = "cartCount";
        /**
         * API Config Object to expose
         */


        var apiConfig = {};
        Object.keys(apiConstants).forEach(function (eachApiConstant) {
            apiConfig[eachApiConstant] = jsHelper.freezeObj({
                "name": apiConstants[eachApiConstant],
                "selector": apiSelectors[eachApiConstant]
            });
        });
        return jsHelper.freezeObj(apiConfig);
    })(_global.jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'apiConfig', _apiConfig);
})(this);
