
var digitalData=digitalData || {};  
digitalData.page=digitalData.page||{}; 				// Information of the page access by consumer
digitalData.product=digitalData.product||{};
digitalData.impression=digitalData.impression||{}; 				// Information of product accees or purchesed by consumer.
digitalData.consumer=digitalData.consumer||{}; 				// Information about the consumer
digitalData.cart=digitalData.cart||{};				// Information of Order or application made by consumer.
digitalData.event=digitalData.event||{};				// ClickStream events made by consumer.
digitalData.system=digitalData.system||{};              //contain the information which provided by the system not fill by user.

//this function will call on each page load
function pageInitialization(pageName,siteSection,siteSubSection,pageType,pathName,loginStatus,userId)
{
    try{
        digitalData.page.pageName=pageName||"",
        digitalData.page.siteSection=siteSection||"",
        digitalData.page.siteSubSection=siteSubSection||"",
        digitalData.page.pageType=pageType||"",
        digitalData.page.pathName=pathName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loginStatus=loginStatus||"",
        digitalData.event.name="page initialization",
        callSatellite('all-page-initialization')
    }catch(er){}
}

//this function will fire when user click on any banner image or banner cta.
function bannerInteraction(bannerTitle,bannerCTA,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.bannerTitle=bannerTitle||"",
        digitalData.event.eventContext.bannerCTA=bannerCTA||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="banner Interaction",
        callSatellite("banner-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user click on Menu ITEM
function menuInteraction(menuTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.menuTitle=menuTitle||"", 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="menu Interaction",
        callSatellite("menu-interaction")
    }
    catch(err){console.log(err)}
}

//this function will call when side widget will open

function widgetnteraction(widgetTitle,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.widgetTitle=widgetTitle||"", 
        digitalData.consumer.userId=userId||"", 
        digitalData.event.eventName="widget Interaction",
        callSatellite("widget-interaction")
    }
    catch(err){console.log(err)}
}

//this function will fire when user interact with any tab over the website from NLI
function tabInteractionNLI(tabTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.tabTitle=tabTitle||"", 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="tab Interaction",
        callSatellite("tab-interaction-NLI",{tabTitle:tabTitle,componentName:componentName,userId:userId})
    }
    catch(err){console.log(err)}
}

//this function will fire for all the CTA's and Tiles interaction
function allCTAInteraction(ctaText,ctaTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="all cta Interaction",
        callSatellite("all-cta-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user click on Blog item 
function blogItemView(blogTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.blogTitle=blogTitle||"",  
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="blog item view ",
        callSatellite("blog-item-interaction")
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on video
function videoItemClick(videoTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.videoTitle=videoTitle||"",  
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="video item click",
        callSatellite("video-item-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user submit the Get App Link Form
function getAppLink(mobileNo,userId,componentName){
    try{
        digitalData.consumer=digitalData.consumer||{},
        digitalData.consumer.mobileNo=mobileNo==undefined?"":window.btoa(mobileNo),
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="get app Link",
        callSatellite('get-app-link')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on play store or App store img or icone click

function appStoreImgClick(storeName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.storeName=storeName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="store icon click",
        callSatellite('store-icon-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the internal search
function internalSearch(searchTerm,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.searchTerm=searchTerm||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="internal search",
        callSatellite('internal-search-term')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the filter
function filterApplied(filterType,FilterValue,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.filterType=filterType||"",
        digitalData.event.eventContext.FilterValue=FilterValue||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="filter applied",
        callSatellite('filter-applied')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the add to wishlist
function addToWishList(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="add to wish list",
        callSatellite('add-to-wish-list')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did compare fund
function compareFund(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="compare fund",
        callSatellite('compare-fund')
    }
    catch(err){console.log(err)}
}


//this function will fire when user click on Invest now button from NLI section
function fundInvestNowNLI(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund invest now",
        callSatellite('fund-invest-now-NLI',{fundType:fundType,fundName:fundName,fundRiskCat:fundRiskCat,userId:userId})
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on explore more/ view details from NLI section
function fundViewDetailsNLI(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund view details",
        callSatellite('fund-view-details-NLI',{fundType:fundType,fundName:fundName,fundRiskCat:fundRiskCat,userId:userId})
    }
    catch(err){console.log(err)}
}

//this function will call when user click on similar AMC items
function amcItemClick(amcTitle,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.amcTitle=amcTitle||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventName='amc title click',
        callSatellite('amc-item-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on goal based investment icon
function goalBaseInvestmentClick(goalItem,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.goalItem=goalItem||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='goal base item click',
        callSatellite('goal-base-item-click')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on fund category item from NLI
function fundCategoryItemClickNLI(fundCategory,userId,fundType){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundCategory=fundCategory||"",
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='fundCategory title click',
        callSatellite('fundCategory-item-click-NLI',{fundCategory:fundCategory,userId:userId,fundType:fundType})
    }
    catch(err){console.log(err)}
}

//this function will fire when fund section expended
function fundSectionExpend(fundSectionName,userId){
try{
    digitalData.event.eventContext=digitalData.event.eventContext||{},
    digitalData.event.eventContext.fundSectionName=fundSectionName||"",
    digitalData.consumer.userId=userId||"",
    digitalData.event.eventName='fund section expend',
    callSatellite('fund-section-expend')
}
catch(err){console.log(err)}
}

//this function will fire when FAQ Expend
function faqExpend(faqTitle,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.faqTitle=faqTitle||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='faq expend',
        callSatellite('faq-expend')
    }
    catch(err){console.log(err)}
    }

//this function will fire when user click on start SIP
function startSIP(componentName,userId){
    try{digitalData.event.eventContext=digitalData.event.eventContext||{},
    digitalData.event.eventContext.componentName=componentName||"",
    digitalData.consumer.userId=userId||"",
    digitalData.event.eventName='start sip click',
    callSatellite('start-sip-click')}
    catch(err){console.log(err)}
}

//this function will call when user click on insurance item 
function insuranceItemClick(insuranceName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.insuranceName=insuranceName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='insurance item click',
        callSatellite('insurance-item-click')
    }
    catch(err){console.log(err)}
}


//this function will call when user click on individual insurance page cta interaction
function insurancePageCtaInteraction(insuranceName,userId,componentName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.insuranceName=insuranceName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='insurance page cta interaction',
        callSatellite('insurance-page-cta-interaction')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on loan item   
function loanItemClick(loanName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loanName=loanName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='loan item click',
        callSatellite('loan-item-click')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on individual loan page cta interaction
function loanPageCtaInteraction(loanName,userId,componentName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loanName=loanName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='loan page cta interaction',
        callSatellite('loan-page-cta-interaction')
    }
    catch(err){console.log(err)}
}


//this function will call when user click on start investing from NLI section
function calculatorCtaInteractionNLI(componentName,userId,calculatorName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.calculatorName=calculatorName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='calculator cta interaction',
        callSatellite('calculator-cta-interaction-NLI',{componentName:componentName,userId:userId,calculatorName:calculatorName,ctaText:ctaText})

    }catch(er){}
}

//this function will call when user click on Open NPS Proceed button   
function openNPSProceed(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='open-nps-proceed',
        callSatellite('open-nps-proceed')
    }catch(er){}
}

//this function will call when user click on how do you like contribute   
function likeToContributeClick(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='how-do-contribute-cta',
        callSatellite('how-do-contribute-cta')
    }catch(er){}
}

//common function for _satellite. eventData is optional for this function we will use this if required later.
function callSatellite(eventName,eventData){
    try{
        window.parent._satellite.track(eventName,eventData)
    }catch(er){console.log(er)}
}
//_satellite.track('listing-next-page')
//_satellite.track('listing-prev-page')

//this function will call when user selects the no of years  
function selectYears(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext || {}, 
        digitalData.event.eventContext.componentName=componentName || "",
        digitalData.consumer.userId=userId || "",
        digitalData.event.eventContext.ctaText=ctaText || "",     //pass selected no of years in cta Text
        digitalData.event.eventName='select-years',
        callSatellite('select-years')
    }catch(er){}
}

//this function will fire when user click on any component on fundmanager
 function fundmanagerClick(fundmanagerId, noofFunds, fundSize, highestReturn, userId) {
    try {
        digitalData.event.eventContext = digitalData.event.eventContext || {},
            digitalData.event.eventContext.noofFunds = noofFunds || "",
            digitalData.event.eventContext.fundSize = fundSize || "",
            digitalData.event.eventContext.highestReturn = highestReturn || "",
            digitalData.consumer.userId = userId || "",
            digitalData.consumer.fundmanagerId = fundmanagerId || "",
            digitalData.event.eventName = "fund manager click",
            callSatellite('moneyfy-fund-manager-click')
    }
    catch (err) { console.log(err) }
}
