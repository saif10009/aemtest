
//analytics userId call

var userIdObj = {};
if (!getUserObj().isLoggedIn) {
    userIdObj['userId'] = "anonymous user";
    Object.freeze(userIdObj)
} else {
    var userData = getUserObj().userData;
    userIdObj['userId'] = userData.appCode;
    Object.freeze(userIdObj)
}
//analytics userId call

function getUserObj(){
  if(sessionStorage.getItem('user') != null){
      var userObj = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
      var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
      return { "isLoggedIn" : true, "userData" : userData};
  }else if(sessionStorage.getItem('user') == null){
      return { "isLoggedIn" : false, "userData" : "null"};
  }    
}

$(document).ready(function () {

  if ($(window).width() < 992) {
    $('.accordian-mobsubmenu .submenu-head').click(function () {
        $('body').addClass('scroll-hide');
    })
  }
  /* javascript void */
  if ($('a[href="#"]').attr('href') == '#') {
    $('a[href="#"]').attr('href', 'javascript:void(0)')
  }
  /* javascript void */

  /*start animation js*/
  AOS.init({
    duration: 1000
  });
  /*end animation js*/


  /*29-8-2022*/
  $('.nested-modal').click(function () {
    $(this).parents('.popover-modals').removeClass('popover-show');
    $(this).parents('.popover-modals').removeAttr('style');
  })

  $('.jsStayUpdated').click(function () {
    $(this).parents('.stay-updated').addClass('closed');
  })

  input_animation();

  $('#jsWebinarRegistration .input-textbox').keyup(function () {
    var element = $(this);
    var ele_value = element.val();
    var ele_required = "Field is required";
    var ele_name = "Please enter name";
    var ele_email = "Please enter valid email";
    var ele_phoneNumber = "Please enter valid number";

    $(this).next('.error-msgs').remove();
    $(this).after('<span class="error-msgs"></span>');

    if ($(element).val() != '') {
      if ($(element).data('type') === 'name') {
        var regName = /[A-Za-z]+[ ][A-Za-z]+$/;

        if (ele_value != '' && !regName.test(ele_value)) {
          $(element).parents('.form-textbox').addClass('textboxerror');
          $(element).next('.error-msgs').text(ele_name);
        }
        else {
          $(element).parents('.form-textbox').removeClass('textboxerror');
          $(element).next().text('');
          $(this).next('.error-msgs').remove();
        }
      }
      if ($(element).data('type') === 'email') {
        var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

        if (ele_value != '' && !regEmail.test(ele_value)) {
          $(element).parents('.form-textbox').addClass('textboxerror');
          $(element).next('.error-msgs').text(ele_email);
        }
        else {
          $(element).parents('.form-textbox').removeClass('textboxerror');
          $(element).next().text('');
          $(this).next('.error-msgs').remove();
        }
      }
      if ($(element).data('type') === 'mobile') {
        if (!validateMobile(element)) {
          $(element).parents('.form-textbox').addClass('textboxerror');
          $(element).next('.error-msgs').text(ele_phoneNumber);
        } else {
          $(element).parents('.form-textbox').removeClass('textboxerror');
          $(element).next().text('');
          $(this).next('.error-msgs').remove();
        }
      }
    } else {
      $(element).parents('.form-textbox').addClass('textboxerror');
      $(element).next('.error-msgs').text(ele_required);
    }
  })

  $('#jsWebinarRegistration .js-proceed-btn').click(function (e) {
    var ele_input = $('#jsWebinarRegistration .input-textbox');
    var errors = [];
    allFilled = true;
    var ele_required = "Field is required";

    $(ele_input).each(function () {
      var element = $(this);
      var ele_value = element.val();
      var ele_name = "Please enter name";
      var ele_email = "Please enter valid email";
      var ele_phoneNumber = "Please enter valid number";

      $(element).next().remove();

      if (element.is(":visible")) {
        if (element.val() != '') {
          $(element).after('<span class="error-msgs"></span>');
          if ($(element).data('type') === 'name') {
            var regName = /[A-Za-z]+[ ][A-Za-z]+$/;

            if (ele_value != '' && !regName.test(ele_value)) {
              $(element).parents('.form-textbox').addClass('textboxerror');
              $(element).next('.error-msgs').text(ele_name);
              errors.push(ele_name);
            }
            else {
              $(element).parents('.form-textbox').removeClass('textboxerror');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'email') {
            var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

            if (ele_value != '' && !ele_value.match(regEmail)) {
              $(element).parents('.form-textbox').addClass('textboxerror');
              $(element).next('.error-msgs').text(ele_email);
              errors.push(ele_email);
            }
            else {
              $(element).parents('.form-textbox').removeClass('textboxerror');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'mobile') {

            if (!validateMobile(element)) {
              $(element).parents('.form-textbox').addClass('textboxerror');
              $(element).next('.error-msgs').text(ele_phoneNumber);
              errors.push(ele_phoneNumber);
            } else {
              $(element).parents('.form-textbox').removeClass('textboxerror');
              $(element).next().text('');
            }
          }
        } else {
          $(element).parents('.form-textbox').addClass('textboxerror');
          $(element).after('<span class="error-msgs">' + ele_required + '</span>');
          errors.push(ele_required);
        }
      }
    });

    if (errors.length == 0) {
      if($('#stay-registration-modal').find('.textbox-inner').find('[data-type="name"]').val() !== ''){
        stayFormSubmit();
      }else if ($('#registration-modal').find('.textbox-inner').find('[data-type="name"]').val() !== ''){
        upcomingFormSubmit();
      }
      
      ele_input.val('');
      $('#jsWebinarRegistration .form-textbox').removeClass('active');
      $('[data-form="register-webinar"]').addClass('d-none');
      $('[data-form="webinar-thanks"]').removeClass('d-none');
    }
  });

  $('.jsRegistratorClear').click(function () {
    $('#jsWebinarRegistration .form-textbox').removeClass('textboxerror');
    $('#jsWebinarRegistration .form-textbox').removeClass('active');
    $('#jsWebinarRegistration .input-textbox').val('');
    $('#jsWebinarRegistration .form-textbox .error-msgs').text('');
    $('[data-form="register-webinar"]').removeClass('d-none');
    $('[data-form="webinar-thanks"]').addClass('d-none');
  })



  /*most view slider js*/
  if ($('#jsMostViewsSlider').is(':visible')) {
    if ($('#jsMostViewsSlider.blog-video-slider .blog-slider-col').length <= 3) {
      if ($(window).width() > 991) {
        $('#jsMostViewsSlider').removeClass('slider-dots');
      }
    }
  }

  $('#jsMostViewsSlider').slick({
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true,
        }
      }
    ]
  });

  if ($('[data-slickid]').is(':visible')) {
    $('[data-slickid]').each(function(i,e){
    if ($(this).find('.blog-slider-col').length <= 3) {
      if ($(window).width() > 991) {
        $(this).removeClass('slider-dots');
      }
    }
  })
  }
  $('[data-slickid]').slick({
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true,
        }
      }
    ]
  });

if (screen.width > 760 && screen.width < 992) {
    try{
  if ((document.querySelectorAll('[data-slickid="jsNewInvestmentSlider"]')[1]).slick.getSlick().slideCount == 2) {
    document.querySelectorAll('[data-slickid="jsNewInvestmentSlider"]')[1].classList.remove('slider-dots')
  }
    } catch(error){
		console.log('not found', error);
	}
}

  if ($('#jsKnowEveryThingSlider').is(':visible')) {
    if ($('#jsKnowEveryThingSlider.blog-video-slider .blog-slider-col').length <= 3) {
      if ($(window).width() > 991) {
        $('#jsKnowEveryThingSlider').removeClass('slider-dots');
      }
    }
  }
  $('#jsKnowEveryThingSlider').slick({
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true,
        }
      }
    ]
  });

  /*most view slider js*/

  /*tata capital playlist slider js*/

  if ($('.jsTataPlayListSlider').is(':visible')) {
    if ($('.jsTataPlayListSlider.blog-video-slider .blog-slider-col').length <= 4) {
      if ($(window).width() > 1023) {
        $('.jsTataPlayListSlider').removeClass('slider-dots');
      }
    }
  }
  $('.jsTataPlayListSlider').slick({
    dots: true,
    infinite: false,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
          infinite: true,
        }
      }
    ]
  });
  /*tata capital playlist slider js*/

  /*Mob upcoming webinar*/

  if ($('#jsMobUpcomingWebinarSlider').is(':visible')) {
    if ($('#jsMobUpcomingWebinarSlider .mob-sliders').length <= 1) {
      if ($(window).width() < 768) {
        $('#jsMobUpcomingWebinarSlider').removeClass('slider-dots');
      }
    }
  }
  if ($(window).width() > 767) {
    if ($('#jsMobUpcomingWebinarSlider').hasClass('slick-initialized')) {
      $('#jsMobUpcomingWebinarSlider').slick('unslick');
    }
  } else {
    $('#jsMobUpcomingWebinarSlider').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 360,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
  }
  /*Mob upcoming webinar*/

  /*stay updated space*/
  var StayHeight = $('.stay-updated').outerHeight();
  console.log(StayHeight);
  $('body').find('.stay-updated').parents('.wrapper').css('padding-bottom', StayHeight);
  /*stay updated space*/ 
  
  /*video modal url*/ 
  /* $('[data-video]').click(function(){
    var abc = $(this).attr('data-video');
    console.log(abc);
    $('#video-modal .play-video-boxs').find('iframe').attr('src', abc);
  }) */
  /*video modal url*/ 

  /*29-8-2022*/

  /*14-2-2022*/
  $('.jsNotifyMeBtn').click(function () {
    $(this).addClass('d-none');
    $(this).parents('.top-row').siblings('.desktop-view-more').removeClass('d-none');
  })
  /*14-2-2022*/


  /*11-5-2022*/
  if ($(window).width() < 992) {
    $('.accordian-mobsubmenu .submenu-head').click(function () {
      $('body').addClass('scroll-hide');
    })
  }

  setTimeout(function () {
    $('#jsFilterAddCompare .js-addCompare').change(function () {
      // var filterCheckLength = $('#jsFilterAddCompare .js-addCompare input:checked').length === 1;
      var isFilterFixed = $('.js-filterWrap').hasClass('affix');
      if (!isFilterFixed) {
        var headerOuterHeight = $('.header').outerHeight();
        /*19-5-2022*/
        if ($(window).width() > 991) {
          var floatingHeight = $('.js-filterWrap').outerHeight();
        }
        else {
          var floatingHeight = $('.js-fixOnTop').outerHeight();
        }
        /*19-5-2022*/
        var CardoffsetTop = $(this).parents('#jsFilterAddCompare').offset().top;
        var totalOffset = CardoffsetTop - (headerOuterHeight + floatingHeight - 30)
        $('body, html').animate({
          scrollTop: totalOffset
        }, 800);
      }
    })
  }, 4000);

  /*11-5-2022*/


  /*2 line Dot in mutual fund strip*/
  if ($(window).width() > 767) {
    var showChar = 45;
  }
  else if ($(window).width() > 374) {
    var showChar = 32;
  }
  else {
    var showChar = 20;
  }

  $('.similar-mutual-funds-items .name-rating-wrap h6').each(function () {
    var content = $(this).html();
    if (content.length > showChar) {
      var showLine = content.substr(0, showChar);
      var remainContent = content.substr(showChar, content.length - showChar);
      var allContent = showLine + '<span class="remaining-content d-none">' + remainContent + '</span> <span>...</span>';
      $(this).html(allContent);
    }
  })


  /*footer Back To Top*/
  $('.jsBackToTop').click(function () {
    $('html, body').animate({
      scrollTop: 0
    }, '300');
  })
  /*footer Back To Top*/

  /*Begin a Successful Financial Journey with Us slider js*/
  $('#jsFinancialJourneySlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {

    if ($('#jsFinancialJourneySlider .financial-info-li').length <= 4) {
      if ($(window).width() > 1199) {
        $('#jsFinancialJourneySlider').removeClass('slider-dots slick-dotted');
      }
    }
  });


  // 15-03-2022 start
  $('#jsFinancialJourneySlider3').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {

    if ($('#jsFinancialJourneySlider3 .financial-info-li').length <= 3) {
      if ($(window).width() > 1199) {
        $('#jsFinancialJourneySlider3').removeClass('slider-dots slick-dotted');
      }
    }
  });

  if ($(window).width() < 992) {
    if ($('#jsFinancialJourneySlider3').hasClass('slick-initialized')) {
      $('#jsFinancialJourneySlider3').slick('unslick');
    }
  } else {
    $('#jsFinancialJourneySlider3').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 360,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
  }
  // 15-03-2022 end

  /*Begin a Successful Financial Journey with Us slider js*/

  /*blog slider js*/
  $('.jsBlogSlider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
        }
      }
    ]
  });
  /*blog slider js*/

  /*video slider js*/
  $('.jsVideoSlider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          variableWidth: true,
        }
      }
    ]
  });
  /*video slider js*/


  /* similar mutual funds */
  if ($(window).width() > 767) {
    if ($('#similarMFSlider').hasClass('slick-initialized')) {
      $('#similarMFSlider').slick('unslick');
    }
  } else {
    $('#similarMFSlider').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: true,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            centerMode: true,
            centerPadding: '15px',
          }
        }
      ]
    });
  }

  /* similar mutual funds */


  $('#loginRegisterSlider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });

  $('.jsbanner-calculate-form .jsInvestKnow').click(function () {
    var ele_input = $('.jsbanner-calculate-form').find('.form-group input[data-type]:visible');
    var errors = [];
    allFilled = true;
    var ele_required = 'Field is required';
    var ele_amtval = 'Amount should be minmum 5000';
    var ele_amtinvestval = 'Min investment per month for this fund Starts from Rs. 10,000';
    var ele_lumsumval = 'Min investment per month for this fund Starts from Rs. 10,000';


    $(ele_input).each(function () {
      var element = $(this);
      var ele_value = element.val();


      $(element).parents('.form-group').find('.error-msgs').remove();
      $(element).parents('.form-group').addClass('error');

      if (element.is(':visible')) {
        if (element.val() != '') {
          $(element).after('<span class="error-msgs"></span>');

          if ($(element).data('type') === 'monthly-amount') {
            var monthly_amt = $('input[data-type="monthly-amount"]').val().replace(/\D/g, '');
            // console.log(monthly_amt);
            if (ele_value != '' && (monthly_amt < 5000)) {
              $(element).parents('.form-group').addClass('error');
              $(element).next('.error-msgs').text(ele_amtval);
              errors.push(ele_amtval);
            } else {
              $(element).parents('.form-group').removeClass('error');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'monthly-invest-amount') {
            var monthly_amt_invest = $('input[data-type="monthly-invest-amount"]').val().replace(/\D/g, '');
            // console.log(monthly_amt_invest);
            if (ele_value != '' && (monthly_amt_invest < 10000)) {
              $(element).parents('.form-group').addClass('error');
              $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').text(ele_amtinvestval);
              errors.push(ele_amtval);
            } else {
              $(element).parents('.form-group').removeClass('error');
              $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').text('');
            }
          }

          if ($(element).data('type') === 'monthly-year') {
            if (ele_value != '') {
              $(element).parents('.form-group').removeClass('error');
              $(element).next('.error-msgs').text();
            } else {
              $(element).parents('.form-group').addClass('error');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'monthly-rate') {
            if (ele_value != '') {
              $(element).parents('.form-group').removeClass('error');
              $(element).next('.error-msgs').text();
            } else {
              $(element).parents('.form-group').addClass('error');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'monthly-amtsave') {
            if (ele_value != '') {
              $(element).parents('.form-group').removeClass('error');
              $(element).next('.error-msgs').text();
            } else {
              $(element).parents('.form-group').addClass('error');
              $(element).next().text('');
            }
          }

          if ($(element).data('type') === 'monthly-date') {
            if (ele_value != '') {
              $(element).parents('.form-group').removeClass('error');
              $(element).next('.error-msgs').text();
            } else {
              $(element).parents('.form-group').addClass('error');
              $(element).next().text('');
            }
          }
          if ($(element).data('type') === 'lumsum') {
            var monthly_amt = $('input[data-type="lumsum"]').val().replace(/\D/g, '');
            if (ele_value != '' && (monthly_amt < 10000)) {
              $(element).parents('.form-group').addClass('error');
              $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').text(ele_lumsumval);
              errors.push(ele_lumsumval);
            } else {
              $(element).parents('.form-group').removeClass('error');
              $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').text('');
            }
          }
        } else {
          $(element).after('<span class="error-msgs">' + ele_required + '</span>');
          errors.push(ele_required);
        }
      }
    });
    if (errors.length == 0) {
      console.log('Success');
    }
  })

  $('.jsbanner-calculate-form [data-type]').keyup(function () {
    var element = $(this);
    var ele_value = element.val();
    var ele_required = 'Field is required';
    var ele_amtval = 'Amount should be minmum 5000';
    var ele_amtinvestval = 'Min investment per month for this fund Starts from Rs. 10,000';
    var ele_lumsumval = 'Min investment per month for this fund Starts from Rs. 10,000';

    $(this).next('.error-msgs').remove();
    $(this).after('<span class="error-msgs"></span>');
    $(this).parents('.form-group').addClass('error');
    $(this).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').remove();
    $(this).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').after('<span class="error-msgs"></span>');

    if ($(element).val() != '') {

      if ($(element).data('type') === 'monthly-amount') {
        var monthly_amt = $(this).val();
        console.log(monthly_amt);

        if (ele_value != '' && (monthly_amt < 5000)) {
          $(element).parents('.form-group').addClass('error');
          $(element).next('.error-msgs').text(ele_amtval);
        } else {
          $(element).parents('.form-group').removeClass('error');
          $(element).next().text('');
        }
        var monthly_amt = '₹' + ele_value;
        $(this).val(monthly_amt);
      }

      if ($(element).data('type') === 'monthly-invest-amount') {
        var monthly_amt_invest = $(this).val();
        console.log(monthly_amt_invest);

        if (ele_value != '' && (monthly_amt_invest < 10000)) {
          $(element).parents('.form-group').addClass('error');
          $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').text(ele_amtinvestval);
        } else {
          $(element).parents('.form-group').removeClass('error');
          $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').text('');
        }
        var monthly_amt_invest = '₹' + ele_value;
        $(this).val(monthly_amt_invest);
      }

      if ($(element).data('type') === 'monthly-year') {
        var monthly_val = ele_value + ' Years';
        $(this).val(monthly_val);

        if (ele_value != '') {
          $(element).parents('.form-group').removeClass('error');
          $(element).next().text('');
        } else {
          $(element).parents('.form-group').addClass('error');
          $(element).next('.error-msgs').text(ele_required);
        }
      }

      if ($(element).data('type') === 'monthly-amtsave') {
        var monthly_amtsave = '₹' + ele_value;
        $(this).val(monthly_amtsave);
        if (ele_value != '') {
          $(element).parents('.form-group').removeClass('error');
          $(element).next().text('');
        } else {
          $(element).parents('.form-group').addClass('error');
          $(element).next('.error-msgs').text(ele_required);
        }
      }

      if ($(element).data('type') === 'monthly-rate') {
        var monthly_rate = ele_value + '%';
        $(this).val(monthly_rate);
        if (ele_value != '') {
          $(element).parents('.form-group').removeClass('error');
          $(element).next().text('');
        } else {
          $(element).parents('.form-group').addClass('error');
          $(element).next('.error-msgs').text(ele_required);
        }
      }

      if ($(element).data('type') === 'monthly-date') {
        var monthly_date = ele_value;
        $(this).val(monthly_date);
        if (ele_value != '') {
          $(element).parents('.form-group').removeClass('error');
          $(element).next().text('');
        } else {
          $(element).parents('.form-group').addClass('error');
          $(element).next('.error-msgs').text(ele_required);
        }
      }

      if ($(element).data('type') === 'lumsum') {
        var monthly_lumsum = $(this).val();

        if (ele_value != '' && (monthly_lumsum < 10000)) {
          $(element).parents('.form-group').addClass('error');
          $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next('.error-msgs').text(ele_lumsumval);
        } else {
          $(element).parents('.form-group').removeClass('error');
          $(element).parents('.card-bottom-error').find('.calculate-return-bottom .error-wrap').next().text('');
        }
        var monthly_lumsum = '₹' + ele_value;
        $(this).val(monthly_lumsum);
      }
    } else {
      $(element).next('.error-msgs').text(ele_required);
    }
  });

  if ($(window).width() < 768) {
    // Read more js start
    var showChar = 370;
    $('.textwithknow').each(function () {
      var content = $(this).html();
      if (content.length > showChar) {
        var showLine = content.substr(0, showChar);
        var remainContent = content.substr(showChar, content.length - showChar);
        var allContent = showLine + '<span class="remaining-dots"> ...</span><span class="remaining-content hidden">' + remainContent + '</span> <a href="javascript:void(0)" class="read-more-link jsKnowMore">Read more</a>';
        $(this).html(allContent);
      }
    })

    /*29-8-2022*/
    var mobshowChar = 250;
    $('.mobtextwithknow').each(function () {
      var content = $(this).html();
      if (content.length > mobshowChar) {
        var showLine = content.substr(0, mobshowChar);
        var remainContent = content.substr(mobshowChar, content.length - mobshowChar);
        var allContent = showLine + '<span class="remaining-dots"> ...</span><span class="remaining-content hidden">' + remainContent + '</span> <a href="javascript:void(0)" class="read-more-link jsKnowMore">Read more</a>';
        $(this).html(allContent);
      }
    })
    /*29-8-2022*/

    $('.jsKnowMore').click(function () {
      if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).text('Read less');
        $(this).siblings('.remaining-dots').addClass('hidden');
      }
      else {
        $(this).addClass('active');
        $(this).text('Read more');
        $(this).siblings('.remaining-dots').removeClass('hidden');
      }
      $(this).parents('.textwithknow').find('.remaining-content').toggleClass('hidden');
      $(this).parents('.textwithknow').find('.remaining-dots').toggleClass('hidden');
      /*29-8-2022*/
      $(this).parents('.mobtextwithknow').find('.remaining-content').toggleClass('hidden');
      $(this).parents('.mobtextwithknow').find('.remaining-dots').toggleClass('hidden');
      /*29-8-2022*/
      return false;
    });
    // Read more js end

  }


    if ($(window).width() < 991) {
      $('.js-investNowBtn').click(function () {
        $('[data-toggleinvest]').toggle();
        $('body').toggleClass('overlay');
      });
      $('.js-closeInvest').click(function () {
        $('[data-toggleinvest]').toggle();
        $('body').toggleClass('overlay');
      });
    }

    // allocation tab js start
    $('.jsAllocationTabList a').click(function () {
      $('.jsAllocationTabList a').removeClass('active-tab');
      $(this).addClass('active-tab');
      var selectTab = $(this).attr('data-allocation');
      $('.allocation-info-item').removeClass('active');
      $('.allocation-info-item[data-title="' + selectTab + '"]').addClass('active');
    });
    // allocation tab js end

    // datepicker
    // $( ".js-datePicker").datepicker({
    //   dateFormat: 'yy-m-d',
    //   inline: true,
    //   onSelect: function(dateText, inst) { 
    //       var date = $(this).datepicker('getDate'),
    //       day  = date.getDate();
    //       $(this).val(day);
    //   }
    // });

    /* custom date calendar start */
    // open calendar
    $('.js-dateCalendarInput').focus(function () {
      $(this).parents('.inner-form-group').find('.js-calendarCard').addClass('show');
    });

    // close calendar
    $('.js-calendarCloseBtn').click(function () {
      $(this).parents('.inner-form-group').find('.js-calendarCard').removeClass('show');
    });

    // select date
    $('.js-calendarTable td span').click(function () {
      $('.js-calendarTable td span').removeClass('active');
      $(this).addClass('active');
      // var getDateVal =  $(this).data('value');
      // $('.js-dateCalendarInput').val(getDateVal + ' of every month');
    });

    // 15-03-2022
    // calendar submit
    $('.js-calendarSubmit').click(function () {
      var getDateVal = $('.js-calendarTable td span.active').data('value')
      if (getDateVal === undefined) {
        console.log(getDateVal);
        $('.js-dateCalendarInput').val('');
      } else {
        console.log(getDateVal);
        $('.js-dateCalendarInput').val(getDateVal + ' of every month');
      }

      $(this).parents('.inner-form-group').find('.js-calendarCard').removeClass('show');
    });
    /* custom date calendar end */





    /* calculator section js start */
    // DOB Date picker start
    if ($("#dob-datepicker").is(":visible")) {
      $("#dob-datepicker").datepicker({
        dateFormat: "d M yy",
      });
    }
    // DOB Date picker end

    // Date format
    $(".date-input").keyup(function () {
      var enteredValue = this.value;
      enteredValue = enteredValue.replace(/\s+/g, "").replace(/[^0-9]/gi, "");
      this.value = enteredValue;
      var matches = enteredValue.match(/\d{1,8}/g);
      var match = matches && matches[0] || ''
      var parts = [];
      var i = 0;
      while (i < match.length) {
        if (i < 4) {
          parts.push(match.substring(i, i + 2));
          i += 2
        } else {
          parts.push(match.substring(i, i + 4));
          i += 4
        }
      }
      if (parts.length) {
        this.value = parts.join('/')
      }
    });


    /* 15-12-2021 */
    // year month toggle
    $('[data-yrMthFilter]').click(function () {
      filterVal = $(this).attr('data-yrMthFilter');
      $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
      $(this).addClass('active');
      $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
      $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
    });
    /* 15-12-2021 */

    /*start check credit scrore js 9-2-2022*/
    $('.jsCheckCredit input').keyup(function () {
      var element = $(this);
      var ele_required = "Field is required";
      var ele_phoneNumber = "Please enter valid number";

      $(element).next('.error-msgs').remove();
      $(element).after('<span class="error-msgs"></span>');

      if (element.is(":visible")) {
        if ($(element).val() != '') {
          if ($(element).data('type') === 'mobile') {
            if (!validateMobile(element)) {
              $(element).next('.error-msgs').text(ele_phoneNumber);
            } else {
              $(element).next('.error-msgs').remove();
            }
          }

        } else {
          $(element).next('.error-msgs').text(ele_required);
        }
      }
    });
    $('.jsCheckCredit #check-credit-score-btn').click(function (e) {
      var ele_input = $(this).parents('.checkcredit-from').find('input');
      var errors = [];
      var ele_required = "Field is required";

      $(ele_input).each(function () {
        var element = $(this);
        var ele_phoneNumber = "Please enter valid number";

        $(element).next('.error-msgs').remove();
        $(element).after('<span class="error-msgs"></span>');

        if (element.is(":visible")) {
          if ($(element).val() != '') {
            if ($(element).data('type') === 'mobile') {
              if (!validateMobile(element)) {
                $(element).next('.error-msgs').text(ele_phoneNumber);
                errors.push(ele_phoneNumber);
              } else {
                $(element).next('.error-msgs').remove();
              }
            }

          } else {
            $(element).next('.error-msgs').text(ele_required);
            errors.push(ele_required);
          }
        }
      });

      if (errors.length == 0) {
        $('.checkcredit-from input').val("");
        setTimeout(function () {
          $('#check-credit-score-modal').addClass('popover-show');
        }, 80);
        $("#check-credit-score-modal").css('display', 'block');
        $('body').addClass('popover-modal-open');
        $('body').append('<div class="modal-backdrop"></div>');
      }
    });


    /*end check credit scrore js 9-2-2022*/
    
  $('.jsDropDownMenuGet li a').click(function () {
    var getMenuLink = $(this).text();
    $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
    $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
    $(this).parents('li').addClass('active');



    /* -------- sebi mobile filter js start -------- */
    if ($(window).width() < 991) {
      var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
      $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
    }
    /* -------- sebi mobile filter js end -------- */
  });  

   /*Close dropdown when click on link*/ 
   $('.jsDropDownMenuClose li a').click(function(){
    $(this).parents('.custom-dropdown').find('.js-filterBtn').removeClass('active');
    $(this).parents('.rating-filter-card').slideUp('fast');
  })
  });

  // validation mobile
function validateMobile(mobileField) {
  var re = /^[4-9][0-9]{9}$/;
  var check = re.test($(mobileField).val());
  if ($(mobileField).val().length != 10 || !check) {
    return false;
  } else {
    return true;
  }
}


    /* input field label animation 29-8-2022  */
    function input_animation() {
      $('.form-textbox .input-textbox').change(function () {
        if ($(this).val().length != 0) {
          $(this).parents('.form-textbox').addClass("active");
        } else if ($(this).val().length == 0) {
          $(this).parents('.form-textbox').removeClass("active");
        }
      }).focus(function () {
        $(this).parents('.form-textbox').addClass("active");
      }).blur(function () {
        if ($(this).val().length != 0) {
          $(this).parents('.form-textbox').addClass("active");
        } else if ($(this).val().length == 0) {
          $(this).parents('.form-textbox').removeClass("active");
        }
      });
    }
/* input field label animation 29-8-2022  */

 // document click js
 $(document).mouseup(function (e) {
  var container = $('.custom-dropdown');
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $('[data-filter-card]').slideUp('fast');
    $('.js-filterBtn').removeClass('active');
  }
  /*29-4-2022*/ 
  $('.wishlist-fund-list').removeClass('mob-wishlistmodal-scroll');
  /*29-4-2022*/ 
  
});

 /* this is for app related changes without header & footer */
  document.addEventListener('DOMContentLoaded', function() {
  var learnsUrlType = 'learn-academy?pageType=learn';
  var webinaUrlType = 'learn-webinar?pageType=webinar';
  var marketUrlType = 'market-insights?pageType=market';
  var blogUrlType = 'blog?pageType=blogs';
  var videoUrlType = 'watch-video?pageType=videos';
  var podcastUrlType = 'podcasts?pageType=podcasts';
  function urlsToRemoveHeaderFooter(url, aemUrl){
    if(window.location.href.split('/').pop() == url ||  window.location.href.split('/').pop() == aemUrl){
      document.querySelector('.header').remove();
      document.querySelector('.footer').remove();
      document.querySelector('.download-app').remove();
      document.querySelector('.moneyfy-breadcrumb').remove();
      document.querySelector('.home-page-container').firstChild.style.paddingTop = '0px';
      document.querySelector('.home-page-container').firstChild.classList.remove('wrapper');  
    }
  }
  urlsToRemoveHeaderFooter(learnsUrlType, 'learn-academy.html?pageType=learn');
  urlsToRemoveHeaderFooter(webinaUrlType, 'learn-webinar.html?pageType=webinar');
  urlsToRemoveHeaderFooter(marketUrlType, 'market-insights.html?pageType=market');
  urlsToRemoveHeaderFooter(blogUrlType, 'blog.html?pageType=blogs');
  urlsToRemoveHeaderFooter(videoUrlType, 'watch-video.html?pageType=videos');
  urlsToRemoveHeaderFooter(podcastUrlType, 'podcasts.html?pageType=podcasts');
});
