var isNotMobileApp = true;
if ((window.location.href.split('/').pop() == 'learn-academy?pageType=learn' || window.location.href.split('/').pop() == 'learn-academy.html?pageType=learn')
    || (window.location.href.split('/').pop() == 'learn-webinar?pageType=webinar' || window.location.href.split('/').pop() == 'learn-webinar.html?pageType=webinar')
    || (window.location.href.split('/').pop() == 'market-insights?pageType=market' || window.location.href.split('/').pop() == 'market-insights.html?pageType=market')
    || (window.location.href.split('/').pop() == 'blog?pageType=blogs' || window.location.href.split('/').pop() == 'blog.html?pageType=blogs')
    || (window.location.href.split('/').pop() == 'watch-video?pageType=videos' || window.location.href.split('/').pop() == 'watch-video.html?pageType=videos')
    || (window.location.href.split('/').pop() == 'podcasts?pageType=podcasts' || window.location.href.split('/').pop() == 'podcasts.html?pageType=podcasts')) {
    isNotMobileApp = false;
}

var mfLoginTimeoutId = null;
var oneTimeMfPopup = false;
$(document).ready(function () {
document.querySelector('.sf-search-wrapper').addEventListener('click', function () {
    document.querySelector('.close-popup').addEventListener('click', function () {
        if((window.location.href.split('/').pop() == 'mutual-funds' || (window.location.href.split('/').pop().split('.')[0] == 'mutual-funds')) &&
        !getUserObj().isLoggedIn && (oneTimeMfPopup == false)){
            sessionStorage.setItem("loginPopupCount", 0);
        }
        setTimeout(function () {
        if ((Number(sessionStorage.getItem("loginPopupCount")) == 0) && (isNotMobileApp) && !getUserObj().isLoggedIn) {
            showLoginPopup();
            document.querySelector('[data-login="true"]').addEventListener('click', function () {
                location.href = appConfig.jocataDomain;
            });
        }
        sessionStorage.setItem("loginPopupCount", 1);
        }, 550)
    });
    if (mfLoginTimeoutId) {
        clearTimeout(mfLoginTimeoutId);
    }
});
});
// FA Popup onLoad start
if (localStorage.getItem("faCount") == null) {
    localStorage.setItem("faCount", 0);
}

// Login Popup OnLoad

if (sessionStorage.getItem("loginPopupCount") == null) {
    sessionStorage.setItem("loginPopupCount", 0);
}

if ((window.location.href.split('/').pop() == 'mutual-funds' || (window.location.href.split('/').pop().split('.')[0] == 'mutual-funds')) &&
    !getUserObj().isLoggedIn) {
    sessionStorage.setItem("loginPopupCount", 0);
    document.querySelector('[data-login="true"]').addEventListener('click', function () {
        location.href = appConfig.jocataDomain;
    });
    mfLoginTimeoutId = setTimeout(function () {
        showLoginPopup()
		oneTimeMfPopup = true;
    }, 3000);
    sessionStorage.setItem("loginPopupCount", 1);
}

var isLoginPopup = false;
var loginPopupShown = false;
function showLoginPopup() {
    var ele_target = document.getElementById('login-modal');
    setTimeout(function () {
        $('#jsLoginModalSlider').slick('setPosition')
        $('#jsLoginModalSlider').slick('slickGoTo', 0, true);
        $(ele_target).addClass('popover-show');
    }, 80);
    $(ele_target).css('display', 'block');
    $('body').addClass('popover-modal-open');
    $('body').append('<div class="modal-backdrop"></div>');
    loginPopupShown = true
    isLoginPopup = true
    /* this is to handle popup overlap for learn centre pages 14-12-22 */
    if (window.location.href.includes('learn-webinar') || window.location.href.includes('learn-academy') ||
        window.location.href.includes('market-insights')) {
        if ((isLoginPopup == true) && (document.querySelector('#video-modal').style.display == 'block')) {
            $('body').append('<div class="modal-backdrop-vid-login"></div>');
        }
    }
}

var npsUrlType = 'national-pension-scheme.html?pageType=nps';
var fapushVal = Number(localStorage.getItem('faCount'));
var loginPopupClosed = false;
document.querySelector('#login-modal button').addEventListener('click', function () {
    loginPopupClosed = true;
    if (loginPopupClosed == true) {
        if (window.location.href.split('/').pop() != npsUrlType) {
            if (jsHelper.isDefined(window.Notification)) {
                if (Notification.permission != 'granted' && Notification.permission != 'denied') {
                    if (fapushVal < 3) {
                        if (screen.width <= 768) {
                            mfLoginTimeoutId = setTimeout(function () {
                                showFaPopup();
                            }, 3000);
                        } else {
                            mfLoginTimeoutId = setTimeout(function () {
                                showFaPopup();
                            }, 500);
                        }
                    }
                }
            }
        }
        loginPopupClosed = false;
    }
})

if ((Number(sessionStorage.getItem("loginPopupCount")) == 0) && (isNotMobileApp) && !getUserObj().isLoggedIn) {
    mfLoginTimeoutId = setTimeout(function () {
        showLoginPopup();
        document.querySelector('[data-login="true"]').addEventListener('click', function () {
            location.href = appConfig.jocataDomain;
        });
        sessionStorage.setItem("loginPopupCount", 1);
    }, 15000);
}

if (window.location.href.split('/').pop() != npsUrlType && window.location.href.split('/').pop().split('.').shift() !== 'mutual-funds'){
    if (jsHelper.isDefined(window.Notification)) {
        console.log(Notification.permission);
       if (Notification.permission != 'granted' && Notification.permission != 'denied') {
            if (fapushVal < 3) {
                var faPopup = document.getElementById('notification-modal');
                if (screen.width <= 768) {
                    document.addEventListener('touchstart', function () {
                        if (Number(sessionStorage.getItem("loginPopupCount")) == 1 && (document.querySelector('#login-modal').style.display !== 'block')) {
                            mfLoginTimeoutId = setTimeout(function () {
                                showFaPopup();
                            }, 3000);
                        }
                    });
                } else {
                    mfLoginTimeoutId = setTimeout(function () {
                        if (Number(sessionStorage.getItem("loginPopupCount")) == 1 && (document.querySelector('#login-modal').style.display !== 'block')) {
                            showFaPopup();
                        }
                    }, 500);
                }
            }
       }
    }
}

function showFaPopup() {
    if (jsHelper.isDef(window.Notification)) {
        if (Notification.permission !== "granted" && Notification.permission !== "denied") {
            if (parseInt(localStorage.getItem("faCount")) < 3) {
                var url = window.location.href;
                var ele_target = document.getElementById('notification-modal');
                setTimeout(function () { $(ele_target).addClass('popover-show'); }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');

                if ($(window).width() < 767) {
                    document.querySelector('#notification-modal .navigate-info').classList.add('hidden');
                }

                document.querySelector('#notification-modal .js-notificationClose').addEventListener('click', function(){
                    $(this).parents('.popover-modal').removeClass('popover-show');
                    $(this).parents('.popover-modal').removeAttr('style');
                    $('.height-scroll').removeAttr('style');
                    $('body').removeClass('popover-modal-open');
                    $('.modal-backdrop').remove();
                });

                
                if (localStorage.getItem("faCount") !== null) {
                    var faCountIndex = parseInt(localStorage.getItem("faCount"));
                    faCountIndex++;
                    localStorage.setItem("faCount", faCountIndex);
                }

                var Moengage = moe({
                    app_id: appIdMoengage,
                    debug_logs: parseInt(debugLogMoengage),
                    swPath: "/service-worker.js",
                    cluster: "DC_3"
                });
                Moengage.call_web_push({
                    "soft_ask": true,
                    "main_class": "moe-main-class",
                    "allow_class": "moe-allow-class",
                    "block_class": "moe-block-class"
                });

                Notification.requestPermission().then(function () {
                    $('#notification-modal').removeClass('popover-show');
                    $('#notification-modal').removeAttr('style');
                    $('.height-scroll').removeAttr('style');
                    $('body').removeClass('popover-modal-open');
                    $('.modal-backdrop').remove();

                    // show loginPopup after 15 seconds
                    /*if ((Number(sessionStorage.getItem("loginPopupCount")) == 0) && (isNotMobileApp) && !getUserObj().isLoggedIn) {
                        mfLoginTimeoutId = setTimeout(function () {
                            showLoginPopup();
                            document.querySelector('[data-login="true"]').addEventListener('click', function () {
                                location.href = appConfig.jocataDomain;
                            });
                            sessionStorage.setItem("loginPopupCount", 1);
                        }, 15000);
                    }*/
                });

                if ($(window).width() > 768) {
                    $(this).parents('#notification-modal').find('.navigate-info').removeClass('hidden');
                    $(this).addClass('btn-disabled');

                } else {
                    $(this).parents('.popover-modal').removeClass('popover-show');
                    $(this).parents('.popover-modal').removeAttr('style');
                    $('.height-scroll').removeAttr('style');
                    $('body').removeClass('popover-modal-open');
                    $('.modal-backdrop').remove();
                }
                $('body').append('<div class="modal-backdrop"></div>');
            }
        }
    }
}
// FA Popup onLoad end

// Login Popup OnLoad

if (sessionStorage.getItem("loginPopupCount") == null) {
    sessionStorage.setItem("loginPopupCount", 0);
}

// if (window.location.href.split('/').pop() != npsUrlType) {
//     if (Number(localStorage.getItem("faCount")) >= 3 && !getUserObj().isLoggedIn &&
//         Number(sessionStorage.getItem("loginPopupCount")) == 0 && isNotMobileApp) {
//             mfLoginTimeoutId = setTimeout(function () {
//             showLoginPopup();
//             document.querySelector('[data-login="true"]').addEventListener('click', function () {
//                 location.href = appConfig.jocataDomain;
//             });
//             sessionStorage.setItem("loginPopupCount", 1);
//         }, 15000);
//     }
// }

function getParentElement(element, level = 1) {
    while (level-- > 0) {
      element = element.parentElement;
      if (!element) return null;
    }
    return element;
}
//analytics userId call

var userIdObj = {};
if (!getUserObj().isLoggedIn) {
    userIdObj['userId'] = "anonymous user";
    Object.freeze(userIdObj)
} else {
    var userData = getUserObj().userData;
    userIdObj['userId'] = userData.appCode;
    Object.freeze(userIdObj)
}
//analytics userId call

var watchListSchemeIds = [];

function showWatchListErrorPopup() {
    document.getElementById('addWatchlistMsg').innerHTML = "There was some issue in adding to watchlist, please try again later.";
    if (!$('.watchListError').hasClass('d-none')) {
        $('.watchListError').addClass('d-none');
    }
    showAddWatchListPopup();
}

function showAddWatchListPopup() {
    var ele_target = document.getElementById('watchlist-modal');
    setTimeout(function () {
        $(ele_target).addClass('popover-show');
    }, 80);
    $(ele_target).css('display', 'block');
    $('body').addClass('popover-modal-open');
    $('body').append('<div class="modal-backdrop"></div>');
}

function fetchWatchListPopup() {
    var ele_target = document.getElementById('addFromWishList');
    setTimeout(function () {
        $(ele_target).addClass('popover-show');
    }, 80);
    $(ele_target).css('display', 'block');
    $('body').addClass('popover-modal-open');
    $('body').append('<div class="modal-backdrop"></div>');
    $('.modal-backdrop').addClass('white-backdrop');
}

function fetchPortfolioPopup() {
    var ele_target = document.getElementById('addFromPortfolio');
    setTimeout(function () {
        $(ele_target).addClass('popover-show');
    }, 80);
    $(ele_target).css('display', 'block');
    $('body').addClass('popover-modal-open');
    $('body').append('<div class="modal-backdrop"></div>');
    $('.modal-backdrop').addClass('white-backdrop');
}

function showWatchListExistPopup() {
    var ele_target = document.getElementById('watchListExistPopup');
    document.getElementById('viewWatchList').addEventListener('click', function () {
        location.href = appConfig.jocataDomain + '?action=watchlist';
    });
    setTimeout(function () {
        $(ele_target).addClass('popover-show');
    }, 80);
    $(ele_target).css('display', 'block');
    $('body').addClass('popover-modal-open');
    $('body').append('<div class="modal-backdrop"></div>');
}

//analytics userId call

// check for null or 0
function isNullOrZeroCheck(cagrVal) {
    return cagrVal == undefined || cagrVal == 'null' || cagrVal == '0.0' ? '-' : cagrVal;
}

// check for null or empty
function isNullOrEmptyCheck(checkVal) {
    return checkVal == undefined || checkVal == 'null' || checkVal == '' ? '-' : checkVal;
}

// add from search or add from portfolio for popup
document.querySelector('[data-search="watchlist-popover"]').addEventListener('click', function (event) {
    document.getElementById('addFromWishList').classList.remove('popover-show');
    document.getElementById('addFromWishList').removeAttribute('style');
    $('body').removeClass('popover-modal-open');
    $('.modal-backdrop').remove();
    document.querySelector('[data-compareselect="' + event.currentTarget.dataset.targetSearch + '"] [data-target="#searchFund-modal-' + event.currentTarget.dataset.targetSearch + '"]').click();
});

document.querySelector('[data-search="portfolio-popover"]').addEventListener('click', function (event) {
    document.getElementById('addFromPortfolio').classList.remove('popover-show');
    document.getElementById('addFromPortfolio').removeAttribute('style');
    $('body').removeClass('popover-modal-open');
    $('.modal-backdrop').remove();
    document.querySelector('[data-compareselect="' + event.currentTarget.dataset.targetSearch + '"] [data-target="#searchFund-modal-' + event.currentTarget.dataset.targetSearch + '"]').click();
});

function getURLParams(url) {
    var queryParams = {};
    try {
        url = url ? url : window.location.search;
        url.split("?")[1].split("&").forEach(function (pair) {
            var key = pair.split("=")[0];
            var val = pair.split("=")[1];
            queryParams[key] = val;
        });
    }
    catch (err) { return "" }
    return queryParams;
}

const validParams = ["affiliate:moneyfyMf:Zaggle::#", "affiliate:moneyfyMf:earlysalary::#"];
const urlParams = getURLParams(location.href);
if (urlParams.hasOwnProperty('cid')) {
    if (validParams.indexOf(urlParams['cid']) != -1) {
        document.querySelectorAll('.restricted-partner').forEach(function (ele) {
            ele.parentNode.removeChild(ele);
        });
    }
    sessionStorage.setItem('cid', JSON.stringify(urlParams.cid));
} else if (sessionStorage.getItem("cid") != null) {
    if (validParams.indexOf(JSON.parse(sessionStorage.getItem('cid'))) != -1) {
        document.querySelectorAll('.restricted-partner').forEach(function (ele) {
            ele.parentNode.removeChild(ele);
        });
    }
}

/*11-5-2022*/
if ($(window).width() < 992) {
    $('.accordian-mobsubmenu .submenu-head').click(function () {
        $('body').addClass('scroll-hide');
    })
}

setTimeout(function () {
    $('#jsFilterAddCompare .js-addCompare').change(function () {
        // var filterCheckLength = $('#jsFilterAddCompare .js-addCompare input:checked').length === 1;
        var isFilterFixed = $('.js-filterWrap').hasClass('affix');
        if (!isFilterFixed) {
            var headerOuterHeight = $('.header').outerHeight();
            var floatingHeight = $('.js-filterWrap').outerHeight();
            var CardoffsetTop = $(this).parents('#jsFilterAddCompare').offset().top;
            var totalOffset = CardoffsetTop - (headerOuterHeight + floatingHeight - 30)
            $('body, html').animate({
                scrollTop: totalOffset
            }, 800);
        }
    })
}, 4000);

/*11-5-2022*/

function getUserObj() {
    if (sessionStorage.getItem('user') != null) {
        var userObj = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
        return { "isLoggedIn": true, "userData": userData };
    } else if (sessionStorage.getItem('user') == null) {
        return { "isLoggedIn": false, "userData": "null" };
    }
}

//this is for app related changes without header & footer
if (window.location.href.split('/').pop() == npsUrlType || window.location.href.split('/').pop() == 'national-pension-scheme?pageType=nps') {
    document.querySelector('.header').remove();
    document.querySelector('.footer').remove();
    document.querySelector('.download-app').remove();
    document.querySelector('.moneyfy-breadcrumb').remove();
    document.querySelector('.home-page-container').firstChild.style.paddingTop = '0px';
    document.querySelector('.home-page-container').firstChild.classList.remove('wrapper');
}

function removeRating (selector){
    document.querySelectorAll(selector).forEach(function (rating) {
        if (Number(rating.innerText.trim()) == 0 || rating.innerText.trim() == '-') {
            rating.parentElement.remove();
        }
    });
}
removeRating('#sebiCatFundList .rating-wrap .rate');
removeRating('#amcFundList .rating-wrap .rate');