/*******************************************API Utility Module - Start******************************************************/
(function (_global) {
  var _apiUtility = (function fnApiUtility(
    jsHelper,
    appConfig,
    apiConfig,
    ajaxUtility
  ) {
    if (exceptionUtility) {
      exceptionUtility.dependencyCheck(
        [jsHelper, appConfig, apiConfig, ajaxUtility],
        "API Utility"
      );
    }
    var apiUtilityObj = {};
    var call = function callApi(eachApiConfig, data) {
      return new Promise(function (resolve, reject) {
        var apiUrl = appConfig.apiRoot + "/" + eachApiConfig.selector+appConfig.apiExtension;
        ajaxUtility
          .postJson(apiUrl, data)
          .then(function (responseText) {
            resolve(responseText);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };

    var getcall = function callApi(eachApiConfig, data) {
      return new Promise(function (resolve, reject) {
        var apiUrl = appConfig.apiRoot + "/" + eachApiConfig.selector+appConfig.apiExtension;
        ajaxUtility
          .getJson(apiUrl, data)
          .then(function (responseText) {
            resolve(responseText);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };

    /***********POST DEMO API**********/
    var demoPostApi = function demoPostApi(data) {
      return new Promise(function (resolve, reject) {
        call(apiConfig.DEMO, data)
          .then(function (response) {
            if (jsHelper.isStr(response)) {
              response = jsHelper.parseJson(response);
            }
            resolve(response);
          })
          .catch(function (err) {
            reject(err);
          });
      });
    };
    apiUtilityObj.demoPostApi = demoPostApi;

    /***********POST DEMO API**********/

    /***********GET DEMO API**********/

    var demoGetApi = function (data) {
      return new Promise(function (resolve, reject) {
        getcall(apiConfig.demo, data)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };
    apiUtilityObj.demoGetApi = demoGetApi;

    /***********GET DEMO API**********/

    /***********DOWNLOAD APP API**********/

    var downloadAppApiFn = function (data) {
      return new Promise(function (resolve, reject) {
        call(apiConfig.DOWNLOAD_APP, data)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };
    apiUtilityObj.downloadAppApiFn = downloadAppApiFn;

    /***********DOWNLOAD APP API**********/

    /***********Moeyfy Top Picks for you API**********/

    var getMoneyfyTopPicksForYou = function (data) {
      return new Promise(function (resolve, reject) {
        call(apiConfig.GET_TOP_PICKS, data)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };
    apiUtilityObj.getMoneyfyTopPicksForYou = getMoneyfyTopPicksForYou;

    /***********Moneyfy Top Picks for you API**********/

    /***********Get Funds Through AMC**********/

    var getFundsThroughAmc = function (data) {
      return new Promise(function (resolve, reject) {
        call(apiConfig.CHOOSE_FROM_AMC, data)
          .then(function (response) {
            resolve(response);
          })
          .catch(function (error) {
            reject(error);
          });
      });
    };
    apiUtilityObj.getFundsThroughAmc = getFundsThroughAmc;

    /***********Get Funds Through AMC**********/  

    /***********Get Funds Via Funad Manager**********/

    var getFundsViaFundManagerName = function (data) {
        return new Promise(function (resolve, reject) {
          call(apiConfig.FUND_MANAGER, data)
            .then(function (response) {
              resolve(response);
            })
            .catch(function (error) {
              reject(error);
            });
        });
      };
      apiUtilityObj.getFundsViaFundManagerName = getFundsViaFundManagerName;
  
      /***********Get Funds Via Funad Manager**********/
      /***********POST COMPARE PAGE API**********/
          var comparePagePostApi = function comparePagePostApi(data) {
            return new Promise(function (resolve, reject) {
              call(apiConfig.COMPARE_PAGE, data)
                .then(function (response) {
                  if (jsHelper.isStr(response)) {
                    response = jsHelper.parseJson(response);
                  }
                  resolve(response);
                })
                .catch(function (err) {
                  reject(err);
                });
            });
          };
          apiUtilityObj.comparePagePostApi = comparePagePostApi;

          /***********POST COMPARE PAGE API**********/
          /***********POST Search API End ***********/
                var getSearchApi = function (data) {
                      return new Promise(function (resolve, reject) {
                          call(apiConfig.SEARCH_API, data)
                              .then(function (response) {
                                  resolve(response);
                              })
                              .catch(function (error) {
                                  reject(error);
                              });
                      });

                  };

                  apiUtilityObj.getSearchApi = getSearchApi;
              /***********POST Search API End ***********/


                /***********POPULAR CATEGORY FUND LIST API**********/
                    var popularCategoryFundList = function popularCategoryPostApi(data) {
                      return new Promise(function (resolve, reject) {
                        call(apiConfig.POPULAR_CATEGORY_FUND_LIST, data)
                          .then(function (response) {
                            if (jsHelper.isStr(response)) {
                              response = jsHelper.parseJson(response);
                            }
                            resolve(response);
                          })
                          .catch(function (err) {
                            reject(err);
                          });
                      });
                    };
                    apiUtilityObj.popularCategoryFundList = popularCategoryFundList;

                 /***********POPULAR CATEGORY FUND LIST API**********/

             /***********Fund Filter API**********/
                var getFundsViaFilter = function (data) {
                  return new Promise(function (resolve, reject) {
                      call(apiConfig.FUNDS_VIA_FILTER, data)
                          .then(function (response) {
                              resolve(response);
                          })
                          .catch(function (error) {
                              reject(error);
                          });
                  });
              };

              apiUtilityObj.getFundsViaFilter = getFundsViaFilter;

            /***********Fund Filter API**********/

      		/***********Graph API**********/
                var getGraph = function (data) {
                  return new Promise(function (resolve, reject) {
                      call(apiConfig.GRAPH, data)
                          .then(function (response) {
                              resolve(response);
                          })
                          .catch(function (error) {
                              reject(error);
                          });
                  });
              };

              apiUtilityObj.getGraph = getGraph;

          /***********Graph API**********/
          
          /***********ADD TO WATCHIST**********/
          var addToWatchList = function (data) {
            return new Promise(function (resolve, reject) {
                call(apiConfig.ADD_TO_WATCH_LIST, data)
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };
        apiUtilityObj.addToWatchList = addToWatchList;
        /***********ADD TO WATCHIST**********/
    
        /***********FETCH FROM WATCH LIST**********/
        var fetchFromWatchList = function (data) {
            return new Promise(function (resolve, reject) {
                call(apiConfig.FETCH_FROM_WATCH_LIST, data)
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };
        apiUtilityObj.fetchFromWatchList = fetchFromWatchList;
        /***********FETCH FROM WATCH LIST**********/  
        
        /***********FETCH FROM PORTFOLIO**********/
        var fetchFromPortfolio = function (data) {
          return new Promise(function (resolve, reject) {
              call(apiConfig.FETCH_FROM_PORTFOLIO, data)
                  .then(function (response) {
                      resolve(response);
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          });
        };
        apiUtilityObj.fetchFromPortfolio = fetchFromPortfolio;
    /***********FETCH FROM PORTFOLIO**********/
    
    /***********FETCH WATCHLIST/PORTFOLIO FUNDS DATA**********/
    var fetchFundsData = function (data) {
      return new Promise(function (resolve, reject) {
          call(apiConfig.WATCHLIST_PORTFOLIO_FUNDS_DATA, data)
              .then(function (response) {
                  resolve(response);
              })
              .catch(function (error) {
                  reject(error);
              });
      });
    };
    apiUtilityObj.fetchFundsData = fetchFundsData;
    /***********FETCH WATCHLIST/PORTFOLIO FUNDS DATA**********/
        /***********FETCH FROM WATCH LATER**********/

        
        /***********TESTIMONAL API**********/
        var testimonalApi = function (data) {
            return new Promise(function (resolve, reject) {
                getcall(apiConfig.TESTIMONAL_API, data)
                    .then(function (response) {
                        resolve(response);
                    })
                    .catch(function (error) {
                        reject(error);
                    });
            });
        };
        apiUtilityObj.testimonalApi = testimonalApi;
        /***********TESTIMONAL API**********/    

        /********** CART COUNT API *************/
        var cartCountApi = function (data) {
          return new Promise(function (resolve, reject) {
              call(apiConfig.CART_COUNT_API, data)
                  .then(function (response) {
                      resolve(response);
                  })
                  .catch(function (error) {
                      reject(error);
                  });
          });
        };
        apiUtilityObj.cartCountApi = cartCountApi;
        /********** CART COUNT API *************/

    return jsHelper.freezeObj(apiUtilityObj);
  })(_global.jsHelper, _global.appConfig, _global.apiConfig, _global.ajaxUtility);
  _global.jsHelper.defineReadOnlyObjProp(_global, "apiUtility", _apiUtility);
})(this);
/*******************************************API Utility Module - End******************************************************/
