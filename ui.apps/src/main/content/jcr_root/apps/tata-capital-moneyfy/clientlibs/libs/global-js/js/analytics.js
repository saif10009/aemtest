if (window.location.href.includes('learn-academy') || window.location.href.includes('learn-webinar') ||
  window.location.href.includes('market-insights') || window.location.href.includes('blog')) {

  var componentName = '';
  var sectionName = ''
  if (!getUserObj().isLoggedIn){
    var userId = 'anonymous user';
  } else if(getUserObj().isLoggedIn) {
    var userId = getUserObj().userData.appCode;
  }

  document.querySelectorAll('.learn-tabs li').forEach(function (tab) {
    if (tab.classList.contains('active')) {
      sectionName = tab.innerText.toLowerCase()
    }
  })

  var ctaTitle = '';
  document.querySelectorAll('.icon-share').forEach(function (iconshare) {
    iconshare.addEventListener('click', function () {
      ctaTitle = iconshare.getAttribute('data-subheading');
    })
  });

  function getParentElement(element, level = 1) {
    while (level-- > 0) {
      element = element.parentElement;
      if (!element) return null;
    }
    return element;
  }

  var popupExitCaptureTitleArr = [];
  var compSharevideo = [];
  var compNameSharevideoSocial = ''
  var popupTypeVideoShareFlag = false;
  var popupExitCapture = '';
  var popupexitCompNameArr = [];


  /* play video initiate */
  document.querySelectorAll('.blog-sliders').forEach(function (card) {
    card.addEventListener('click', function (e) {
      if (window.location.href.includes('learn-webinar')) {
        componentName = (getParentElement(e.currentTarget, 6)).querySelector('.heading-view').innerText;
      } else {
        componentName = (getParentElement(e.currentTarget, 6)).querySelector('.heading-with-view').innerText;
      }
      compSharevideo.push(componentName);
      if (sectionName == 'webinar') {
        componentName = document.querySelector('.webinars-with-sort .heading-view').innerText;
      } else if (sectionName == 'market insights') {
        componentName = document.querySelector('.heading-with-view').innerText
      }
      popupexitCompNameArr.push(componentName);
      var ctaTitle = e.currentTarget.querySelector('.blog-content a').getAttribute('data-subheading');
      popupExitCaptureTitleArr.push(ctaTitle);
      var ctaText = 'play video';
      playvideoInitiate(sectionName, componentName, ctaTitle, ctaText, userId);
    });
  })

  /* sharevideoInitiate */
  document.querySelectorAll('.icon-share').forEach(function (iconshare) {
    iconshare.addEventListener('click', function (e) {
      if (window.location.href.includes('learn-webinar')) {
        componentName = (document.getElementById('video-modal').style.display == 'block') ? '' : (getParentElement(e.currentTarget, 8)).querySelector('.heading-view').innerText;
      } else {
        componentName = (document.getElementById('video-modal').style.display == 'block') ? '' : (getParentElement(e.currentTarget, 8)).querySelector('.heading-with-view').innerText;
      }
      compNameSharevideoSocial = componentName;

      if (sectionName == 'webinar') {
        componentName = document.querySelector('.webinars-with-sort .heading-view').innerText
      } else if (sectionName == 'market insights') {
        componentName = document.querySelector('.heading-with-view').innerText
      }
      var ctaTitle = iconshare.getAttribute('data-subheading');
      popupExitCaptureTitleArr.push(ctaTitle);
      popupexitCompNameArr.push(componentName);
      var ctaText = iconshare.classList[0].split('-').reverse().join(' ');
      var popupType = 'share video';
      if (getParentElement(e.currentTarget, 3).classList.contains('play-video-boxs') && (document.getElementById('video-modal').style.display == 'block')) {
        popupTypeVideoShareFlag = true;
      } else {
        popupTypeVideoShareFlag = false;
      }

      if (document.getElementById('video-modal').style.display == 'block') {
        var compNameSharevideo = ''
        compNameSharevideo = compSharevideo.pop();
        compNameSharevideoSocial = compNameSharevideo;
        popupExitCapture = 'share video step 2'
        sharevideoInitiate(sectionName, compNameSharevideo, ctaTitle, popupType, ctaText, userId)
      } else {
        shareInitiate(sectionName, componentName, ctaTitle, ctaText, userId)
      }
    })
  })

  /* shareIconClick */
  document.querySelector('[data-copybtn="copyBtn"]').addEventListener('click', function () {
    var copyLink = document.querySelector('[data-copybtn="copyBtn"]').innerText.toLowerCase();
    var popupType = popupTypeVideoShareFlag == true ? 'share video' : 'share article';
    var componentName = compNameSharevideoSocial
    shareiconClick(sectionName, componentName, ctaTitle, popupType, copyLink, userId)
  })

  document.querySelectorAll('.list-shares li').forEach(function (shareIcon) {
    shareIcon.addEventListener('click', function (e) {
      var shareIconName = e.currentTarget.querySelector('span').getAttribute('data-shareicon');
      var popupType = popupTypeVideoShareFlag == true ? 'share video' : 'share article';
      var componentName = compNameSharevideoSocial
      shareiconClick(sectionName, componentName, ctaTitle, popupType, shareIconName, userId)
    })
  })
  /* shareIconClick */

  /* -------------------- webinar start ---------------- */
/* webinar - registration step 1*/
if (window.location.href.includes('learn-webinar')) {
  var componentNameThanks = [];
  document.querySelector('.stay-inner [data-target="#stay-registration-modal"]').addEventListener('click', function (e) {
    var componentName = 'main page';
    var ctaTitle = document.querySelector('.stay-inner h5').innerText;
    var ctaText = e.currentTarget.innerText.toLowerCase();
    registrationstepOne(sectionName, componentName, ctaTitle, ctaText, userId)
  })

  document.querySelectorAll('.up-webinar-inner [data-upcomingmodal="upcoming-modal"]').forEach(function (btn) {
    btn.addEventListener('click', function (e) {
      var componentName = 'Register now - upcoming webinars';
      var ctaTitle = document.querySelector('#webinar-title').innerText;
      var ctaText = e.currentTarget.innerText.toLowerCase()
      registrationstepOne(sectionName, componentName, ctaTitle, ctaText, userId)
    })
  })

  /* register now click */
  function registerNowClickCall(e) {
    var mobileNo = getParentElement(e.currentTarget, 2).querySelector('[data-type="mobile"]').value;
    var emailID = getParentElement(e.currentTarget, 2).querySelector('[data-type="email"]').value;
    var ctaText = e.currentTarget.innerText.toLowerCase();
    var componentName = getParentElement(e.currentTarget, 3).querySelector('h5').hasAttribute('id')
      ? 'upcoming webinar registration now' : 'stay updated registration now'
    var ctaTitle = getParentElement(e.currentTarget, 3).querySelector('h5').innerText
    componentNameThanks.push(componentName);
    registernowClick(sectionName, componentName, ctaTitle, ctaText, mobileNo, emailID, userId)
  }

  document.querySelector('#registration-modal [data-upcoming="registerBtn"]').addEventListener('click', function (e) {
    registerNowClickCall(e)
  });

  document.querySelector('#stay-registration-modal .js-proceed-btn').addEventListener('click', function (e) {
    registerNowClickCall(e)
  });

  /* webinar thanks */
  document.querySelectorAll('[data-form="webinar-thanks"] a.btn-blue').forEach(function (backbtn) {
    backbtn.addEventListener('click', function (e) {
      var ctaText = e.currentTarget.innerText.toLowerCase();
      var ctaTitle = ctaText + ' popup';
      console.log(ctaTitle);
      var componentName = componentNameThanks.pop();
      allCTAInteraction(ctaText, ctaTitle, componentName, userId)
    })
  })

  /* webinar sort */
  document.querySelectorAll('[data-filter-card="sorting"] ul li').forEach(function (sortCard) {
    sortCard.addEventListener('click', function (e) {
      var componentName = getParentElement(document.querySelector('[data-filter-card="sorting"]'), 3).querySelector('.heading-view').innerText
      ctaText = e.currentTarget.innerText.trim();
      ctaTitle = 'sort by'
      selectSortby(sectionName, componentName, ctaTitle, ctaText, userId)
    });
  })
}
/* webinar end */

/* blogs start */
if (window.location.href.includes('blog')) {
  document.querySelectorAll('[data-filter-card="sorting"] ul li').forEach(function (sortCard) {
    sortCard.addEventListener('click', function (e) {
      var componentName = getParentElement(document.querySelector('[data-filter-card="sorting"]'), 3).querySelector('.heading-view').innerText
      var ctaText = e.currentTarget.innerText.trim();
      var ctaTitle = 'sort by'
      selectSortby(sectionName, componentName, ctaTitle, ctaText, userId)
    });
  });

  document.querySelectorAll('.btn-viewalls').forEach(function (blog) {
    blog.addEventListener('click', function (e) {
      var componentName = getParentElement(e.currentTarget, 7).querySelector('.heading-view').innerText;
      var ctaTitle = getParentElement(e.currentTarget, 2).querySelector('.blog-content p').innerText;
      var ctaText = e.currentTarget.innerText
      blogreadmoreClick(sectionName, componentName, ctaTitle, ctaText, userId)
    })
  })
}
/* blogs end */
}

