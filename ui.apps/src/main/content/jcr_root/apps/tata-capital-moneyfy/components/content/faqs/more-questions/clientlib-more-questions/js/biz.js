
/*More questions form js start*/
(function (_global) {

    var moreQuestionformFn = (function (jsHelper) {
        var moreQuestionformObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            keyupFn();
            onchangeFn();
            clickFn();
            // select2 initialize
            if ($('.single-select2').is(":visible")) {
                $('.single-select2').select2({
                    minimumResultsForSearch: -1
                });

                $('.nps-select').on('change', function () {
                    var data = $(this).find("option:selected").text();
                    console.log(data);
                    if (data != 'Select') {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').removeClass('hide-data');
                    } else {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').addClass('hide-data');
                    }
                });
            }

        });
        function keyupFn() {
            $('.only-numeric-input').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, '')));
            });

            $('.js-moreQuestionformWrap .input-textbox').keyup(function () {
                var element = $(this);
                var ele_value = element.val();
                var ele_required = "Field is required";
                var ele_name = "Please enter name";
                var ele_email = "Please enter valid email";
                var ele_phoneNumber = "Please enter valid number";

                $(this).next('.error-msgs').remove();
                $(this).after('<span class="error-msgs"></span>');

                if ($(element).val() != '') {
                    if ($(element).data('type') === 'name') {
                        var regName = /[A-Za-z]+[ ][A-Za-z]+$/;

                        if (ele_value != '' && !regName.test(ele_value)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_name);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                            $(this).next('.error-msgs').remove();
                        }
                    }
                    if ($(element).data('type') === 'email') {
                        var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

                        if (ele_value != '' && !regEmail.test(ele_value)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_email);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                            $(this).next('.error-msgs').remove();
                        }
                    }
                    if ($(element).data('type') === 'mobile') {
                        if (!validateMobile(element)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_phoneNumber);
                        } else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                            $(this).next('.error-msgs').remove();
                        }
                    }
                } else {
                    $(element).parents('.textbox-box').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_required);
                }
            });

        };

        function onchangeFn() {
            $('.js-moreQuestionformWrap .js-Selectsingle').on('change', function () {
                $(this).parents('.textbox-box').removeClass('textboxerror');
                $(this).parents('.textbox-box').find('.error-msgs').remove();
            });

        };

        function clickFn() {
            $('.js-moreQuestionformWrap .js-proceed-btn').click(function (e) {
                var ele_input = $('.js-moreQuestionformWrap .input-textbox');
                var selectElements = $('.js-moreQuestionformWrap .select2-hidden-accessible');
                var errors = [];
                allFilled = true;
                var ele_required = "Field is required";
                var mobile = $('input[data-type="mobile"]').val();
                // Select 2 validation
                $(selectElements).each(function () {
                    var select = $(this);

                    if ($(select).val() == '') {
                        allFilled = false;
                        $(select).parents('.textbox-box').addClass('textboxerror');
                        $(select).next('.error-msgs').remove();
                        $(select).after('<span class="error-msgs" style="top: 100%;">' + ele_required + '</span>');
                        errors.push(ele_required);
                    } else {
                        $(select).parents('.textbox-box').removeClass('textboxerror');
                        $(select).next('.error-msgs').remove();
                    }
                });

                $(ele_input).each(function () {
                    var element = $(this);
                    var ele_value = element.val();
                    var ele_name = "Please enter name";
                    var ele_email = "Please enter valid email";
                    var ele_phoneNumber = "Please enter valid number";

                    $(element).next().remove();

                    if (element.is(":visible")) {
                        if (element.val() != '') {
                            $(element).after('<span class="error-msgs"></span>');
                            if ($(element).data('type') === 'name') {
                                var regName = /[A-Za-z]+[ ][A-Za-z]+$/;

                                if (ele_value != '' && !regName.test(ele_value)) {
                                    $(element).parents('.textbox-box').addClass('textboxerror');
                                    $(element).next('.error-msgs').text(ele_name);
                                    errors.push(ele_name);
                                }
                                else {
                                    $(element).parents('.textbox-box').removeClass('textboxerror');
                                    $(element).next().text('');
                                }
                            }

                            if ($(element).data('type') === 'email') {
                                var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

                                if (ele_value != '' && !ele_value.match(regEmail)) {
                                    $(element).parents('.textbox-box').addClass('textboxerror');
                                    $(element).next('.error-msgs').text(ele_email);
                                    errors.push(ele_email);
                                }
                                else {
                                    $(element).parents('.textbox-box').removeClass('textboxerror');
                                    $(element).next().text('');
                                }
                            }

                            if ($(element).data('type') === 'mobile') {

                                if (!validateMobile(element)) {
                                    $(element).parents('.textbox-box').addClass('textboxerror');
                                    $(element).next('.error-msgs').text(ele_phoneNumber);
                                    errors.push(ele_phoneNumber);
                                } else {
                                    $(element).parents('.textbox-box').removeClass('textboxerror');
                                    $(element).next().text('');
                                }
                            }
                        } else {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).after('<span class="error-msgs">' + ele_required + '</span>');
                            errors.push(ele_required);
                        }
                    }
                });


                if (errors.length == 0) {
                    //  alert('success');
                    ele_input.val('');
                    selectElements.val('').trigger('change');
                }
            });

        };




        return jsHelper.freezeObj(moreQuestionformObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'moreQuestionformObj', moreQuestionformFn);
})(this || window || {});

/*More questions form js end*/