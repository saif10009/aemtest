/*testimonal render js start*/
(function (_global) {
    var testimonalRenderFn = (function (jsHelper) {
        var testimonalRenderObj = {};
        var renderTestimonal = function (cardEle) {
            var renderHtmlStr = '';
            var testimonalCard = document.querySelector('.contain');
            cardEle.forEach(function (ele) {
                if (ele.approval == 'true') {
                    renderHtmlStr += '<div class="customer-slider-col">' +
                        '                                <div class="customer-sliders">' +
                        '                                    <h5>' + ele.customername + '</h5>' +
                        '                                    <span class="text-location">' + ele.location + '</span>' +
                        '                                    <div class="list-stars">' + ratingsCount(ele.ratingcount) +
                        '                                    </div>' +
                        '                                   <p>' + ele.description + '</p>' +
                        '                                </div>' +
                        '                            </div>';

                }
            });   
            testimonalCard.innerHTML = '<div class="customer-slider slider-dots slider-arrows flex-slider jsCustomerSlider">' + renderHtmlStr + '</div>';
        }

        function ratingsCount(len) {
            var ratingStr = "";
            for (i = 0; i < Number(len); i++) {
                ratingStr += '<span class="icon-star"></span>'
            }
            return ratingStr;
        }

        testimonalRenderObj.renderTestimonal = renderTestimonal;
        return jsHelper.freezeObj(testimonalRenderObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'testimonalRenderObj', testimonalRenderFn);
})(this || window || {});
/*testimonal render js start*/