AMC Fund List
====
The `AMC Fund List` component is for adding amc related fund list on moneyfy amc details page.



## Feature
* It is an dynamic component with data populated using api.



## Edit Dialog Properties
This is an dynamic component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.amc-details-fund-list` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5