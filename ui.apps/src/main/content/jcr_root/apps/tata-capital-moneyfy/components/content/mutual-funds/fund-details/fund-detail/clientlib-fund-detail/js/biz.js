/* fund detail js */
(function (_global) {
    var fundDetailsBizObj = (function (jsHelper) {
        var fundDetailsObj = {};
        var graphArray;
        var minSipAmt, minLumsumAmt;
        var inputDate = new Date();
        document.addEventListener("DOMContentLoaded", function () {
            getGraphData();
            initializefundDetails();
            initializeCustomScrollBarAndSingleSelect();
            showReturns();
            setStartInvestInputValue();
            if (document.getElementById('managerName') != null) {
                // set manager name initials as icon in manager detail section
                var managerName = document.getElementById('managerName').innerText.split(' ');
                managerName = managerName.shift().charAt(0) + managerName.pop().charAt(0);
                document.getElementById('managerNameInitials').innerHTML = managerName.toUpperCase();
                document.getElementById('fundManagerPagePath').href = "/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/" + fundManager.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}\s]/g, '-').concat('.html');
                document.getElementById('fundManagerPagePathForHeading').href = "/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/" + fundManager.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}\s]/g, '-').concat('.html');
            }

            $('#js-addCompare').on('click', function (event) {
                event.preventDefault();
                var fundName = $(this).parents('.quick-review-inner').find('h4').text();
                var fundRiskCat = $(this).parents('.quick-review-inner').find('[data-detailrisktype]').data('detailrisktype');
                var fundType = $(this).parents('.quick-review-inner').find('[data-detailcatname]').data('detailcatname');
                compareFund(fundType, fundName, fundRiskCat, userIdObj.userId)
                var schemeIdArray = [];
                schemeIdArray.push(schemeId);
                sessionStorage.setItem('moneyfyCompareFunds', JSON.stringify(schemeIdArray));
                location.href = "/content/tata-capital-moneyfy/en/compare-mutual-funds.html"
            });

            $('#amcPagePath').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parents('.card-title').find('h5').text().trim();
                allCTAInteraction(ctaText, ctaTitle, 'fund-details', userIdObj.userId)
                location.href = $(this).attr('href')
            })

            minSipAmt = Number(document.querySelector('[data-minsipamount]').dataset.minsipamount);
            minLumsumAmt = Number(document.querySelector('[data-minsipamount]').dataset.minlumsumamount);
            document.getElementById('amcPagePath').href = "/content/tata-capital-moneyfy/en/mutual-funds/" + amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}\s]/g, '-').concat('.html');
            document.getElementById('amcPagePathForHeading').href = "/content/tata-capital-moneyfy/en/mutual-funds/" + amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}\s]/g, '-').concat('.html');
        });

        function setStartInvestInputValue() {
            setTimeout(function () {
                document.querySelector('[data-type="monthly-invest-amount"]').value = minSipAmt;
                // set default date to post current for sip invest now
                inputDate.setDate(inputDate.getDate() + 15);
                var showDate;
                if (inputDate.getDate() == 1) {
                    showDate = inputDate.getDate()+'st' + ' of every month';
                } else if (inputDate.getDate() == 2) {
                    showDate = inputDate.getDate()+'nd' + ' of every month';
                } else if (inputDate.getDate() == 3) {
                    showDate = inputDate.getDate()+'rd' + ' of every month';
                } else {
                      if(siptValues == ''){
                        showDate = '';
                        //$(".js-dateCalendarInput").prop('disabled', true);
                      }else{
                        if (siptValues[1] == 1) {
                            showDate = siptValues[1]+'st' + ' of every month';
                        } else if (siptValues[1] == 2) {
                            showDate = siptValues[1]+'nd' + ' of every month';
                        } else if (siptValues[1] == 3) {
                            showDate = siptValues[1]+'rd' + ' of every month';
                        }else{
                            showDate = siptValues[1]+'th' + ' of every month';
                        }
                      }
                }
                document.querySelector('.js-dateCalendarInput').value = showDate;
                document.querySelector('[data-type="lumsum"]').value = minLumsumAmt;
            }, 100);
        }

        function getGraphData() {
            let requestObj = {
                "body": {
                    "schemeIds": [],
                }
            };
            requestObj.body.schemeIds.push(schemeId);
            fundDetailApiObj.getGraph(requestObj).then(function (response) {
                console.log(response);
                if (response.status.toLowerCase() == 'success') {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == "success") {
                        if (responseData.responseJson.body.graphDetails[0].schemeId == schemeId) {
                            graphArray = responseData.responseJson.body.graphDetails;
                            if (graphArray[0].schemeMasterGraphResponseList != "No data found") {
                                initializeGraph();
                            } else {
                                $('[data-return]').click(function () {
                                    $('[data-return]').removeClass('active');
                                    $(this).addClass('active');
                                });
                            }

                        } else {
                            graphArray = [];
                            initializeGraph();
                        }
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        function showReturns() {
            $('.returnsVal').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $('[data-rate="rate"]').removeClass().addClass('trend-info up-trend');
                    $('[data-rate="rate"]').html('<span class="icon-caret-top"></span>' + value + '%');
                } else if (Number.isNaN(Number(value))) {
                    $('[data-rate="rate"]').removeClass();
                    $('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $('[data-rate="rate"]').removeClass().addClass('trend-info down-trend');
                    $('[data-rate="rate"]').html('<span class="icon-caret-down"></span>' + value + '%');
                }   
            });
        }

        function initializeCustomScrollBarAndSingleSelect() {
            // select2 initialize
            $(".single-select2").select2({
                minimumResultsForSearch: -1,
            });

            // dropdown js
            $(".js-filterBtn").click(function () {
                $(".js-filterBtn").not(this).removeClass("active");
                $(this).toggleClass("active");
                $("[data-filter-card]").slideUp();

                var filterCard = $(this).attr("data-filter");
                var toggleFilterCard = $(this)
                    .parents(".custom-dropdown")
                    .find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(":visible") ?
                    toggleFilterCard.slideUp() :
                    toggleFilterCard.slideDown();
            });

            $(".mCustomScroll").mCustomScrollbar({});

            $(".filterTags-mCustomScrollbar").mCustomScrollbar({
                axis: "x",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });

            $(".filterList-mCustomScrollbar").mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });
        }

        function convertInDays(date1, date2) {
            var date = new Date(date1);
            var date1 = new Date(String(date.getMonth() + 1 + '/' + date.getDate() + '/' + Math.abs(date.getFullYear())));
            var diffTime = Math.abs(date2 - date1);
            var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            return diffDays;
        }

        function parseData(data) {
            var convertedObj = {};
            data.forEach(value => {
                value.schemeMasterGraphResponseList.forEach(data => {
                    if (!convertedObj.hasOwnProperty(data.navDate)) {
                        convertedObj[data.navDate] = {};
                    }
                    convertedObj[data.navDate][value.schemeId] = data.navValue;
                });
            });
            return convertedObj;
        }

        function getGraphDate(days, data) {
            if (days !== "") {
                return Object.keys(data).filter(function (elementDate) {
                    return convertInDays(new Date(elementDate), new Date()) <= days;
                }).sort(function (a, b) {
                    return new Date(b.date) - new Date(a.date);
                });
            } else {
                return Object.keys(data).sort(function (a, b) {
                    return new Date(b.date) - new Date(a.date);
                });
            }
        }

        function renderGraph(graphData, days, convertedData) {
            if (jsHelper.isDef(chart)) {
                chart.destroy();
            }

            var chart = Highcharts.chart("performance-chart", {
                chart: {
                    type: "spline",
                    animation: {
                        enabled: false,
                    },
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                subtitle: {
                    text: "",
                },
                yAxis: {
                    title: {
                        text: "",
                        style: {
                            color: "#606060",
                            fontSize: "12px",
                            fontFamily: "Poppins",
                            fontWeight: "400",
                        },
                    },
                    tickInterval: 10,
                    visible: false,
                },
                xAxis: {
                    categories: getGraphDate(days, convertedData),
                    style: {
                        color: "#606060",
                        fontSize: "12px",
                        fontFamily: "Poppins",
                        fontWeight: "400",
                    },
                },
                tooltip: {
                    headerFormat: '<div class="custom-tooltip">',
                    pointFormat: '<span class="tooltip-label">NAV ({point.category})</span>' +
                        '<span class="tooltip-result">₹{point.y}</span>',
                    footerFormat: "</div>",
                    shared: true,
                    useHTML: true,
                    backgroundColor: "#333",
                    shadow: false,
                    borderWidth: 0,
                    borderRadius: 6,
                    style: {
                        fontFamily: "Poppins",
                    },
                },
                plotOptions: {
                    series: {
                        color: "#48A476",
                        showInLegend: false,
                        // pointPlacement: 'on'
                    },
                },

                series: [{
                    data: graphData,
                },],

                responsive: {
                    rules: [{
                        condition: {
                            maxWidth: 500,
                        },
                        chartOptions: {},
                    },],
                },
            });

        }

        function initializeGraph() {

            var dataDynamic = {
                dataDefault: [],
                data_1M: [],
                data_3M: [],
                data_6M: [],
                data_1Y: [],
                data_3Y: [],
                data_5Y: [],
                data_Max: []
            };

            var daysObj = {
                dataDefault: (365 * 3),
                data_1M: 30,
                data_3M: 90,
                data_6M: 180,
                data_1Y: 365,
                data_3Y: (365 * 3),
                data_5Y: (365 * 5),
                data_Max: ""
            }
            var convertedData = parseData(graphArray);
            graphArray.forEach(function (element, index) {
                for (var dataType in dataDynamic) {
                    var dataArray;
                    dataArray = getGraphDate(daysObj[dataType], convertedData).map(value => convertedData[value][element.schemeId] ? Number(convertedData[value][element.schemeId]) : "");
                    dataDynamic[dataType] = dataArray;
                }
            });
            /* console.log("dataDynamic :", dataDynamic); */

            renderGraph(dataDynamic.dataDefault, daysObj["dataDefault"], convertedData);

            // change data on click desktop
            $("[data-return]").click(function () {
                $("[data-return]").removeClass("active");
                $(this).addClass("active");
                var returnType = $(this).attr("data-return");
                renderGraph(dataDynamic[returnType], daysObj[returnType], convertedData)

                var dynaVal = $(this).data("value");
                var returnDur = $(this).text();
                var trend = $(this).data("trend");
                $(".text-dyna").text(dynaVal);
                $(".return-text").text(returnDur);

                if (trend == "down") {
                    $(".trend-data").removeClass("green-text").addClass("red-text");
                    $(".trend-data i")
                        .removeClass("icon-caret-top")
                        .addClass("icon-caret-down");
                } else {
                    $(".trend-data").removeClass("red-text").addClass("green-text");
                    $(".trend-data i")
                        .removeClass("icon-caret-dwon")
                        .addClass("icon-caret-top");
                }
                $('.highcharts-xaxis-labels text').css('transform', 'translate(0,0)');
            });

            // change data on click mobile
            $(".js-ReturnSelect").on("change", function () {
                var returnType = $(".js-ReturnSelect option:selected").attr("value");
                renderGraph(dataDynamic[returnType], daysObj[returnType], convertedData)
                var dynaVal = $(".js-ReturnSelect")
                    .select2()
                    .find(":selected")
                    .data("value");
                $(".text-dyna").text(dynaVal);
                var dynaVal = $(this).data("value");
                var returnDur = $(".js-ReturnSelect")
                    .select2()
                    .find(":selected")
                    .data("duration");
                var trend = $(".js-ReturnSelect")
                    .select2()
                    .find(":selected")
                    .data("trend");
                $(".text-dyna").text(dynaVal);
                $(".return-text").text(returnDur);

                if (trend == "down") {
                    $(".trend-data").removeClass("green-text").addClass("red-text");
                    $(".trend-data i")
                        .removeClass("icon-caret-top")
                        .addClass("icon-caret-down");
                } else {
                    $(".trend-data").removeClass("red-text").addClass("green-text");
                    $(".trend-data i")
                        .removeClass("icon-caret-dwon")
                        .addClass("icon-caret-top");
                }
            });

            // performance chart js end

            // allcation chart js start
            var renderAssetAllocation = function (catName, asset) {
            if (catName === 'Debt') {
                return '<li class="debt-item">' + catName + ' ' + asset + '%</li>'
            } else if (catName === 'Equity') {
                return '<li class="equity-item">' + catName + ' ' + asset + '%</li>'
            } else if (catName === 'Others') {
                return '<li class="yellow-item">' + catName + ' ' + asset + '%</li>'
            }
            }

            var assetHolder = []
            for (catName in allocationPercentage) {
                if (allocationPercentage[catName] != '') {
                    var asset = Number(allocationPercentage[catName.toLowerCase()]);
                    assetHolder.push({
                        catName, asset
                    })
                    catName = catName.replace(catName.charAt(0), catName.charAt(0).toUpperCase());
                    var allocationType = document.getElementById('allocationType');
                    allocationType.innerHTML += renderAssetAllocation(catName, asset)
                    document.querySelectorAll('.fund-details-card .card-blocks-wrap li p').forEach(function (list) {
                        if (list.innerText == 'Fund size') {
                            var renderAllocation = list.nextElementSibling.innerText;
                            if (renderAllocation.length >= 8 && ($(window).width() > 767)) {
                                document.getElementById('allocationPercentage').innerHTML = renderAllocation;
                                document.getElementById('allocationPercentage').style.fontSize = '20px'
                            } else {
                                document.getElementById('allocationPercentage').innerHTML = renderAllocation;
                            }
                        };
                    })
                }
            }
            
            assetHolder.forEach(function(el) {
                if(el.catName == 'debt'){
                    debtAsset = el.asset
                } else if (el.catName == 'equity'){
                    equityAsset = el.asset
                } else if (el.catName == 'others'){
                    othersAsset = el.asset
                }
            })

            Highcharts.chart("asset-allocation", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },
                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [{
                    data: [{
                        color: '#D7DF23',
                        radius: '115%',
                        innerRadius: '88%',
                        y: 100
                    }]
                }, {
                    data: [{
                        color: "#66CC99",
                        radius: "115%",
                        innerRadius: "88%",
                        y: equityAsset + debtAsset
                    }]
                }, {
                    data: [{
                        color: "#2C6EB5",
                        radius: "115%",
                        innerRadius: "88%",
                        y: debtAsset
                    }]
                }] 
            });

            // if ($(window).width() > 767) {
            //     chart.update({
            //         plotOptions: {
            //             series: {
            //                 pointPlacement: "on",
            //             },
            //         },
            //     });
            // }
            // if ($(window).width() < 767) {
            //     chart.update({
            //         xAxis: {
            //             categories: dateDynamic[returnType],
            //         },
            //     });
            // }

            $('.highcharts-xaxis-labels text').css('transform', 'translate(0,0)');
        }

        function initializefundDetails() {
            /* invest now btn click */
            document.querySelector('.jsInvestKnow').addEventListener('click', function () {
                if (document.querySelector('#investmentType .active-tab').innerText.toLowerCase() == "sip") {
                    var mode = 'sip';
                    var amount = document.querySelector('[data-type="monthly-invest-amount"]').value;
                    var date = document.querySelector('.js-calendarTable .active') == null ? inputDate.getDate() : document.querySelector('.js-calendarTable .active').innerText;
                    var source = window.location.href;
                    startSIP('fund-detail', userIdObj.userId);
                    console.log('Invest Now redirect link :', appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source)
                    if (Number(amount) >= minSipAmt) {
                        if (!headerBizObj.getUserObj().isLoggedIn) {
                            showLoginPopup();
                            document.querySelector('[data-login="true"]').addEventListener('click', function () {
                                location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source;
                            });
                        } else {
                            location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source;
                        }
                    }
                } else {
                    var mode = 'lumpsum';
                    var amount = document.querySelector('[data-type="lumsum"]').value;
                    var source = window.location.href;
                    startSIP('fund-detail', userIdObj.userId);
                    console.log('Invest Now redirect link :', appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source)
                    if (Number(amount) >= minLumsumAmt) {
                        if (!headerBizObj.getUserObj().isLoggedIn) {
                            showLoginPopup();
                            document.querySelector('[data-login="true"]').addEventListener('click', function () {
                                location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source;
                            });
                        } else {
                            location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&amount=' + amount + '&mode=' + mode + '&date=' + date + '&source=' + source;
                        }
                    }
                }
            });

            /*banner-tab js*/
            $(".jsReturnTabList [data-returnmenu]").click(function () {
                $(".jsReturnTabList li a").removeClass("active-tab");
                $(this).addClass("active-tab");
                var ele_id = $(this).attr("data-returnmenu");
                $(".jsReturnTabRow").addClass("d-none");
                $("#" + ele_id)
                    .fadeIn()
                    .removeClass("d-none");
            });
            /*banner tab js*/

            $(".js-toggleExitLoad").click(function () {
                $(this)
                    .find(".text-wrap")
                    .text(function (i, text) {
                        return text === "View more" ? "View less" : "View more";
                    });
                $(this).toggleClass("show");
                $(".exit-load-content").slideToggle();
            });

            $(".jsbanner-calculate-form .jsInvestKnow").click(function () {
                var ele_input = $(".jsbanner-calculate-form").find(
                    ".form-group input[data-type]:visible"
                );
                var errors = [];
                allFilled = true;
                var ele_required = "Field is required";
                var ele_amtval = "Amount should be minmum 5000";
                var ele_amtinvestval =
                    "Min investment per month for this fund Starts from Rs." + Number(minSipAmt).toFixed().toLocaleString('en-IN');
                var ele_lumsumval =
                    "Min investment per month for this fund Starts from Rs." + Number(minLumsumAmt).toFixed().toLocaleString('en-IN');

                $(ele_input).each(function () {
                    var element = $(this);
                    var ele_value = element.val();

                    $(element).parents(".form-group").find(".error-msgs").remove();
                    $(element).parents(".form-group").addClass("error");

                    if (element.is(":visible")) {
                        if (element.val() != "") {
                            $(element).after('<span class="error-msgs"></span>');

                            if ($(element).data("type") === "monthly-amount") {
                                var monthly_amt = $('input[data-type="monthly-amount"]')
                                    .val()
                                    .replace(/\D/g, "");
                                console.log(monthly_amt);
                                if (ele_value != "" && monthly_amt < 5000) {
                                    $(element).parents(".form-group").addClass("error");
                                    $(element).next(".error-msgs").text(ele_amtval);
                                    errors.push(ele_amtval);
                                } else {
                                    $(element).parents(".form-group").removeClass("error");
                                    $(element).next().text("");
                                }
                            }

                            if ($(element).data("type") === "monthly-invest-amount") {
                                var monthly_amt_invest = $(
                                    'input[data-type="monthly-invest-amount"]'
                                )
                                    .val()
                                    .replace(/\D/g, "");
                                console.log(monthly_amt_invest);
                                if (ele_value != "" && monthly_amt_invest < Number(minSipAmt)) {
                                    $(element).parents(".form-group").addClass("error");
                                    $(element)
                                        .parents(".card-bottom-error")
                                        .find(".calculate-return-bottom .error-wrap")
                                        .next(".error-msgs")
                                        .text(ele_amtinvestval);
                                    errors.push(ele_amtval);
                                } else {
                                    $(element).parents(".form-group").removeClass("error");
                                    $(element)
                                        .parents(".card-bottom-error")
                                        .find(".calculate-return-bottom .error-wrap")
                                        .text("");
                                }
                            }

                            if ($(element).data("type") === "lumsum") {
                                var monthly_amt = $('input[data-type="lumsum"]')
                                    .val()
                                    .replace(/\D/g, "");
                                if (ele_value != "" && monthly_amt < Number(minLumsumAmt)) {
                                    $(element).parents(".form-group").addClass("error");
                                    $(element)
                                        .parents(".card-bottom-error")
                                        .find(".calculate-return-bottom .error-wrap")
                                        .next(".error-msgs")
                                        .text(ele_lumsumval);
                                    errors.push(ele_lumsumval);
                                } else {
                                    $(element).parents(".form-group").removeClass("error");
                                    $(element)
                                        .parents(".card-bottom-error")
                                        .find(".calculate-return-bottom .error-wrap")
                                        .text("");
                                }
                            }
                        } else {
                            $(element).after(
                                '<span class="error-msgs">' + ele_required + "</span>"
                            );
                            errors.push(ele_required);
                        }
                    }
                });
                if (errors.length == 0) {
                    console.log("Success");
                }
            });

            $(".jsbanner-calculate-form [data-type]").keyup(function () {
                var element = $(this);
                var ele_value = element.val();
                var ele_required = "Field is required";
                var ele_amtval = "Amount should be minmum 5000";
                var ele_amtinvestval = "Min investment per month for this fund Starts from Rs." + Number(minSipAmt).toFixed().toLocaleString('en-IN');
                var ele_lumsumval = "Min investment per month for this fund Starts from Rs." + Number(minLumsumAmt).toFixed().toLocaleString('en-IN');
                

                $(this).next(".error-msgs").remove();
                $(this).after('<span class="error-msgs"></span>');
                $(this).parents(".form-group").addClass("error");
                $(this)
                    .parents(".card-bottom-error")
                    .find(".calculate-return-bottom .error-wrap")
                    .next(".error-msgs")
                    .remove();
                $(this)
                    .parents(".card-bottom-error")
                    .find(".calculate-return-bottom .error-wrap")
                    .after('<span class="error-msgs"></span>');

                if ($(element).val() != "") {
                    if ($(element).data("type") === "monthly-amount") {
                        var monthly_amt = $(this).val();
                        console.log(monthly_amt);

                        if (ele_value != "" && monthly_amt < minLumsumAmt) {
                            $(element).parents(".form-group").addClass("error");
                            $(element).next(".error-msgs").text(ele_amtval);
                        } else {
                            $(element).parents(".form-group").removeClass("error");
                            $(element).next().text("");
                        }
                        var monthly_amt = "₹" + ele_value;
                        $(this).val(monthly_amt);
                    }

                    if ($(element).data("type") === "monthly-invest-amount") {
                        var monthly_amt_invest = Number($(this).val());
                        console.log(monthly_amt_invest);

                        if (ele_value != "" && monthly_amt_invest < minSipAmt) {
                            $(element).parents(".form-group").addClass("error");
                            $(element)
                                .parents(".card-bottom-error")
                                .find(".calculate-return-bottom .error-wrap")
                                .next(".error-msgs")
                                .text(ele_amtinvestval);
                        }else {
                            $(element).parents(".form-group").removeClass("error");
                            $(element)
                                .parents(".card-bottom-error")
                                .find(".calculate-return-bottom .error-wrap")
                                .next(".error-msgs")
                                .text("");
                        }
                        var monthly_amt_invest = "₹" + ele_value;
                        $(this).val(monthly_amt_invest);
                    }

                    if ($(element).data("type") === "lumsum") {
                        var monthly_lumsum = $(this).val();

                        if (ele_value != "" && monthly_lumsum < minLumsumAmt) {
                            $(element).parents(".form-group").addClass("error");
                            $(element)
                                .parents(".card-bottom-error")
                                .find(".calculate-return-bottom .error-wrap")
                                .next(".error-msgs")
                                .text(ele_lumsumval);
                        }else {
                            $(element).parents(".form-group").removeClass("error");
                            $(element)
                                .parents(".card-bottom-error")
                                .find(".calculate-return-bottom .error-wrap")
                                .next()
                                .text("");
                        }
                        var monthly_lumsum = "₹" + ele_value;
                        $(this).val(monthly_lumsum);
                    }
                } else {
                    $(element).next(".error-msgs").text(ele_required);
                }
            });

            $(".js-ToggleBtn").click(function () {
                $(this).toggleClass("active");
                var toggleDiv = $(this).attr("data-btn");
                $('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });

            // $(".js-viewMoreToggleBtn").click(function () {
            //     $(this).toggleClass("active");
            //     $(this)
            //         .find(".text-wrap")
            //         .text(function (i, text) {
            //             return text === "More" ? "Less" : "More";
            //         });
            //     var toggleDiv = $(this).attr("data-btn");
            //     $(this)
            //         .parents(".similar-mutual-funds-items")
            //         .find('[data-toggleCard="' + toggleDiv + '"]')
            //         .slideToggle();
            // });

            if ($(window).width() > 991) {
                $(".js-floating").before('<div class="floating"></div>');
            }
            if ($(window).width() < 991) {
                $(".js-fixContainer").before('<div class="mobFloating"></div>');
            }

            $(window).on("load", function () {
                header_fixed();
            });

            $(window).on("scroll", function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $("header").height();
                    if ($(".js-floating").length > 0) {
                        var ele_floating = $(".js-floating");
                        var ele_height = $(ele_floating).height();
                        var ele_position = $(".floating").position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating
                                .addClass("affix")
                                .css({ width: "100%", top: headerHeight, "z-index": "6" })
                                .prev(".floating")
                                .css("height", ele_height);
                        } else {
                            ele_floating
                                .removeClass("affix")
                                .css({ width: "100%", top: "0", "z-index": "1" })
                                .prev(".floating")
                                .css("height", "0");
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $(".js-rightSideFix").removeClass("affix-absolute");
                        } else {
                            $(".js-rightSideFix").addClass("affix-absolute");
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $("header").height();
                    var conatiner_position =
                        $(".js-fixContainer").position().top - headerHeight;
                    var ele_selector = $(".js-fixOnTop");
                    var scrollTop = $(window).scrollTop();

                    // console.log('header height '+headerHeight);
                    // console.log('ele pos '+ele_position);
                    // console.log('container pos '+conatiner_position);
                    // console.log('win scroll top '+scrollTop);

                    if (scrollTop > conatiner_position) {
                        ele_selector
                            .addClass("affix")
                            .css({ width: "100%", top: headerHeight, "z-index": "8" });
                    } else {
                        ele_selector
                            .removeClass("affix")
                            .css({ width: "100%", top: "0", "z-index": "1" });
                    }

                    var conatiner_filter_position =
                        $(".mobFloating").position().top - headerHeight;
                    var ele_selector_filter = $(".js-fixContainer");
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop >= conatiner_filter_position) {
                        ele_selector_filter
                            .addClass("affix")
                            .css({ width: "100%", top: headerHeight, "z-index": "8" })
                            .prev(".mobFloating")
                            .css("height", ele_height);
                    } else {
                        ele_selector_filter
                            .removeClass("affix")
                            .css({ width: "100%", top: "0", "z-index": "1" })
                            .prev(".mobFloating")
                            .css("height", "0");
                    }
                }
            });

            if ($(window).width() < 768) {
                // Read more js start
                var showChar = 370;
                $(".textwithknow").each(function () {
                    var content = $(this).html();
                    if (content.length > showChar) {
                        var showLine = content.substr(0, showChar);
                        var remainContent = content.substr(
                            showChar,
                            content.length - showChar
                        );
                        var allContent =
                            showLine +
                            '<span class="remaining-dots"> ...</span><span class="remaining-content hidden">' +
                            remainContent +
                            '</span> <a href="javascript:void(0)" class="read-more-link jsKnowMore">Read more</a>';
                        $(this).html(allContent);
                    }
                });
                $(".jsKnowMore").click(function () {
                    if ($(this).hasClass("active")) {
                        $(this).removeClass("active");
                        $(this).text("Read more");
                        $(this).siblings(".remaining-dots").addClass("hidden");
                    } else {
                        $(this).addClass("active");
                        $(this).text("Read less");
                        $(this).siblings(".remaining-dots").removeClass("hidden");
                    }
                    $(this)
                        .parents(".textwithknow")
                        .find(".remaining-content")
                        .toggleClass("hidden");
                    $(this)
                        .parents(".textwithknow")
                        .find(".remaining-dots")
                        .toggleClass("hidden");
                    return false;
                });
                // Read more js end

                $(".js-ToggleBtn").click(function () {
                    $(this).removeClass("active");
                    $("body").toggleClass("overlay");
                });
                $('.js-closeContact').click(function () {
                    $('[data-togglecard="contact-card"]').toggle();
                    $('body').toggleClass('overlay');
                });
            }

            if ($(window).width() < 991) {
                $(".js-investNowBtn").click(function () {
                    $("[data-toggleinvest]").toggle();
                    $("body").toggleClass("overlay");
                });
                $(".js-closeInvest").click(function () {
                    $("[data-toggleinvest]").toggle();
                    $("body").toggleClass("overlay");
                });
            }

            // allocation tab js start
            $(".jsAllocationTabList a").click(function () {
                $(".jsAllocationTabList a").removeClass("active-tab");
                $(this).addClass("active-tab");
                var selectTab = $(this).attr("data-allocation");
                $(".allocation-info-item").removeClass("active");
                $('.allocation-info-item[data-title="' + selectTab + '"]').addClass("active");
            });
            // allocation tab js end

            /* custom date calendar start */
            // open calendar
            $(".js-dateCalendarInput").focus(function () {
                $(this)
                    .parents(".inner-form-group")
                    .find(".js-calendarCard")
                    .addClass("show");
            });

            // close calendar
            $(".js-calendarCloseBtn").click(function () {
                $(this)
                    .parents(".inner-form-group")
                    .find(".js-calendarCard")
                    .removeClass("show");
            });

            // select date
            

            var a = siptValues;
            var b = document.querySelectorAll('.calendar-table-wrap [data-value]');
            b.forEach(function(ele){
                $(ele).addClass('diabled');
            })
            b.forEach(function(ele){
                a.forEach(function(elem){
                    if(ele.innerText == elem){
                        $(ele).removeClass('diabled');
                        $(ele).attr("data-date","activeDate")
                    }
                    
                })
            })
            $("[data-date]").click(function () {
                $(".js-calendarTable td span").removeClass("active");
                $(this).addClass("active");
                var getDateVal = $(this).data('value');
                $('.js-dateCalendarInput').val(getDateVal + ' of every month');
                $(this)
                    .parents(".inner-form-group")
                    .find(".js-calendarCard")
                    .removeClass("show");
            });

            // document click js
            $(document).mouseup(function (e) {
                var container = $(".custom-dropdown");
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $("[data-filter-card]").slideUp();
                    $(".js-filterBtn").removeClass("active");
                }
            });

            // numeric input validation
            $(".only-numeric-input").keyup(function (e) {
                $(this).val($(this).val().replace(/\D/g, ""));
            });
        }

        /*header scroll fixed animation*/
        var TOPBAR_HEIGHT = $(".top-bar").outerHeight();
        var SECONDBAR_HEIGHT = $(".second-bar").innerHeight();

        function header_fixed() {
            var windowScroll = $(window).scrollTop();
            var topbarHeight = $(".top-bar").outerHeight();

            if (windowScroll > topbarHeight) {
                $("header").addClass("affix");
                $(".wrapper").css("padding-top", SECONDBAR_HEIGHT);
            } else {
                $("header").removeClass("affix");
                $(".wrapper").css("padding-top", TOPBAR_HEIGHT + SECONDBAR_HEIGHT);
            }
        }

        return jsHelper.freezeObj(fundDetailsObj);
        //adobe analytics 17-21,28-34,36-41
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, "fundDetailsBizObj", fundDetailsBizObj);
})(this || window || {});
/* fund detail js */
