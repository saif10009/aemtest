(function (_global) {
    var fundProcessApiFnObj = (function (jsHelper) {
        var fundProcessApiObj = {};
        var fundProcess = function (data) {
            return apiUtility.downloadAppApiFn(data);
        }
        fundProcessApiObj.fundProcess = fundProcess;

        return jsHelper.freezeObj(fundProcessApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundProcessApiObj', fundProcessApiFnObj);
})(this || window || {});