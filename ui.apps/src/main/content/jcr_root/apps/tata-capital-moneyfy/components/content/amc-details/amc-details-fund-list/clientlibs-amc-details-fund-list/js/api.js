/* amcFundList api js start */
(function (_global) {
    var amcFundListApiFn = (function (jsHelper) {
        var amcFundListApiObj = {};

        /* Compare Api Calling */
        var compareSearch = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getSearchApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fund Filter Api Calling */
        var amcDetailFundFilter = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getFundsViaFilter(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            });
        }

        /* Add To WatchList Api calling */
        var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        
        /* Fetch from Portfolio Api calling */
         var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        amcFundListApiObj.fetchFundsData = getFundsData;
        amcFundListApiObj.fetchFromPortfolio = fetchFromPortfolio;
        amcFundListApiObj.fetchwatchListApi = fetchwatchListApi;
        amcFundListApiObj.addTowatchListApi = addTowatchListApi;
        amcFundListApiObj.amcDetailFundFilter = amcDetailFundFilter;
        amcFundListApiObj.compareSearch = compareSearch;

        return jsHelper.freezeObj(amcFundListApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'amcFundListApiObj', amcFundListApiFn);
})(this || window || {});
/* amcFundList api js end */