/*insurance calculator render js start*/
(function (_global) {
    var insuranceCalculatorRenderFn = (function (jsHelper) {
        var insuranceCalculatorRenderObj = {}

        function renderInsuranceCalculation(e, inputObj) {
            if ((isNaN(inputObj.monthlyIncome)) || (inputObj.monthlyIncome == 0) || (inputObj.monthlyIncome < 1000) || (inputObj.currentAge > 80) || (inputObj.retirementAge > 80) || (inputObj.retirementAge <= inputObj.currentAge)) {
                document.getElementById('requieredCover').innerText = "0";
                document.getElementById('annualIncome').innerText = "₹ 0";
                document.getElementById('totalInterest').innerText = (e.data.returnRoi / 100 * 10000).toFixed(2) + '%';
            } else {
                document.getElementById('annualIncome').innerText = "₹ " + (e.data.annualIncome).toLocaleString('en-IN');
                document.getElementById('requieredCover').innerText = Math.round(e.data.requiredCover).toLocaleString('en-IN');
                document.getElementById('totalInterest').innerText = (e.data.returnRoi / 100 * 10000).toFixed(2) + '%';
            }
        }

        insuranceCalculatorRenderObj.renderInsuranceCalculation = renderInsuranceCalculation;
        return jsHelper.freezeObj(insuranceCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "insuranceCalculatorRenderObj", insuranceCalculatorRenderFn)
})(this || window || {});
  /*insurance calculator render js end*/