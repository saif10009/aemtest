

// upcoming webinar modal date show
$(document).ready(function () {
  $('[data-upcomingmodal="upcoming-modal"]').click(function (e) {
    var modalDate = $(this).parent('.up-webinar-inner').find('h5');
    var modaldateDesktop = modalDate[0].innerText;
    if (modaldateDesktop.indexOf('Online Webinar')) {
      modaldateDesktop = modaldateDesktop.replace(', Online Webinar', '');
    }
    else {
      modaldateDesktop = modaldateDesktop;
    }
    var getTitle = $(this).parent().parent().parent().find('#webinar-title').text();
    $('#upcomingWebinar-thankyouTitle').text(getTitle)
    $('#upcomingWebinar-thankyouText').text('Please join the link on ' + modaldateDesktop + '.');
    $('#upcomingWebinar-regTitle').text(getTitle)
    $('#upcoming-title').val(getTitle)
  })
})
// upcoming webinar modal date show

function upcomingFormSubmit() {

  var upcomingObj = {};
  upcomingObj.title = $('#registration-modal').find('.textbox-inner').find('#upcoming-title').val()
  upcomingObj.name = $('#registration-modal').find('.textbox-inner').find('[data-type="name"]').val();
  upcomingObj.mobileno = $('#registration-modal').find('.textbox-inner').find('[data-type="mobile"]').val();
  upcomingObj.email = $('#registration-modal').find('.textbox-inner').find('[data-type="email"]').val();

  var upcomingOriginalData = {};
  upcomingOriginalData.Master = [];
  upcomingOriginalData.Master.push(upcomingObj)
  console.log(upcomingOriginalData)
  upcomingWebinarApiCall(upcomingOriginalData)
}