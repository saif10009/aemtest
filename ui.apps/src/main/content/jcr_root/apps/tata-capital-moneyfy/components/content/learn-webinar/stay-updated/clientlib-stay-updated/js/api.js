function learnWebinarStayUpdatedForm(lwsu) {
   
    return new Promise(function (resolve, reject) {
        
        $.ajax({
            type: "POST",
            url: "/content/tata-capital/mdm.learnWebinarStayupdated.json",
            data: JSON.stringify(lwsu),
            async: true,
            contentType: "application/json",
            dataType:'json',
            success: function (res) {
                resolve(res)
            },
            failure: function (res) {
                reject(res)
            }
        })
    })
}