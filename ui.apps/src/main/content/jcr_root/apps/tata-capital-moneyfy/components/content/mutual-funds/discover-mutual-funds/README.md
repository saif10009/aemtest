Discover Mutual Funds
====
This is a `Discover Mutual Funds` component provides various types of Mutual Funds. 

## Feature
* This is a multifield component.
* This component provides wide range of life goals.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used for rendering title of the component.
2. `./description` Used for rendering description of the component.
3. `./mutualFundList` Used to create multifield in component.
4. `./link` Used to render redirection link of a card.
5. `./openNewTab` Enable open redirection link in a new tab.
6. `./image` Used for rendering image on page.
7. `./hoverImage` Used for rendering image on page.
8. `./cardHeading` Used for rendering heading of card on page.
9. `./buttonLink` Used to render redirection link of a card.
10. `./newTab` Enable open redirection link in a new tab.
11. `./buttonTitle` Used to rendering the name of the button.


## Client Libraries
The component provides a `moneyfy.discover-mutual-funds` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5