/*sebi filter js start*/
(function(_global) {
    var amcFundFilterRenderFn = (function(jsHelper) {
        var amcFundFilterRenderObj = {};

        function renderCategoryList(sebiCategoryList) {
            for (var listType in sebiCategoryList) {
                //var htmlStr = "";
                sebiCategoryList[listType].forEach((cat, index) => {
                    document.querySelector('[data-list=' + listType.toLowerCase() + ']').innerHTML += '<li class="filter-checkbox">'+
                    '<div class="custom-checkbox">'+
                    '    <label class="js-filterCheck">'+
                    '        <input type="checkbox" name="subCategory" value="'+ cat.subCatName +'" data-value="'+ cat.subCatName +'">'+
                    '        <span class="checkbox-wrap"></span>'+ cat.subCatName +'</label>'+
                    '</div>'+
                    '</li>';    
                });
            }
        }
        amcFundFilterRenderObj.renderCategoryList = renderCategoryList;
        return jsHelper.freezeObj(amcFundFilterRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "amcFundFilterRenderObj", amcFundFilterRenderFn)
})(this || window || {});
/*sebi filter js end*/


	
