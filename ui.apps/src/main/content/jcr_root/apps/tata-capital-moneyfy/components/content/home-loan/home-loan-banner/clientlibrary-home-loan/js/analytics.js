(function (_global) {
    var analyticsObjCall = (function (jsHelper) {
        var analyticsCall = {}
        var popupTermsBoxBtn = document.querySelector('#term-p').nextElementSibling;
        popupTermsBoxBtn.addEventListener('click', function () {
            var ctaText = popupTermsBoxBtn.innerText.toLowerCase();
            var ctaTitle = popupTermsBoxBtn.getAttribute('href').split('/loans/')[1].split('/')[0].replace('-', ' ');
            var componentName = document.querySelector('.home-loan-banner').classList[0].replaceAll('-', ' ');
            allCTAInteraction(ctaText, ctaTitle, componentName, userIdObj.userId)
        })

        return jsHelper.freezeObj(analyticsCall);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "analyticsObjCall", analyticsObjCall)
})(this || window || {});
