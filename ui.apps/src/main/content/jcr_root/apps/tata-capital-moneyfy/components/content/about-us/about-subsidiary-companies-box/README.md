About subsidiary companies box
====
This is a `About subsidiary companies box` component used in about us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as images, headings, title and button txt , link is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading , ./companySlideHeading` Used for rendering headings in the component.
2. `./companySlideDescription` Used for rendering description of the component.
3. `./creditImg` Used for rendering image on page.
4. `./companySlideButtonLink` Used to render redirection link of a card.
5. `./companySlideButtonTarget` Enable open redirection link in a new tab.
6. `./companySlideButton` Used to rendering the name of the button.
7. `./companySlideItemMultifield` Used to create multifield in component.


## Client Libraries
The component provides a `moneyfy.about-subsidiary-companies-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5