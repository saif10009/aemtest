(function (_global) {
    var analyticsObjCall = (function (jsHelper) {
        var analyticsCall = {}
        document.querySelector('.credit-cards-box a').addEventListener('click', function (e) {
            var ctaText = e.currentTarget.innerText.toLowerCase();
            var ctaTitle = 'credit card'
            var componentName = document.querySelectorAll('.credit-cards')[0].classList[0].replace('-', ' ');
            allCTAInteraction(ctaText, ctaTitle, componentName, userIdObj.userId)
        })

        return jsHelper.freezeObj(analyticsCall);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "analyticsObjCall", analyticsObjCall)
})(this || window || {});