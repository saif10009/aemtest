document.addEventListener('DOMContentLoaded', function() {
  $('.blog-slider-col').click(function () {
    var video = $(this).attr('data-video')
    var embedVideo = video.substring(video.lastIndexOf('/') + 1);
    var videos = 'https://youtube.com/embed/' + embedVideo;
    $('#video-modal .play-video-boxs').find('iframe').attr('src', videos);
    var head = $(this).data('heading');
    $('.play-video-right').find("h6").text(head)
    var subhead = $(this).data('subheading');
    $('.play-video-right').find("h5").text(subhead)
    var desc = $(this).data('desc');
    $('.play-video-right').find("p").text(desc)
    var nameDate = $(this).data('namedate');
    $('.play-video-right').find("span").text(nameDate)
    $('.play-video-right').find('a').attr('data-subheading', subhead)
    $('.play-video-right').find('a').attr('data-video', video)
  });

var videoDataObj = {}
  $('[data-target="#share-modal"]').click(function (e) {
    e.stopImmediatePropagation()
    videoDataObj.videoText = $(this).attr('data-subheading');
    videoDataObj.videoLinks = $(this).attr('data-video');
    originalLink = videoDataObj.videoLinks;
    var video = $(this).attr('data-video')
    var embedVideo = video.substring(video.lastIndexOf('/') + 1);
    var updatedLink = 'https://www.youtube.com/watch?v=' + embedVideo;
    $('.copy-links input').val(updatedLink);
  })

$('[data-copybtn]').click(function (event) {
  event.preventDefault()
  var ctaText = $(this).text().trim();
  $(this).parents('.copy-links').find('input').select();
  document.execCommand("copy");
})

$('[data-shareicon]').click(function (event) {
  event.preventDefault()
  if ($(this).data('shareicon') == 'facebook') {
    var facebookWindow = window.open(
      "https://www.facebook.com/sharer/sharer.php?hashtag=" +
      videoDataObj.videoText +
      "&" +
      "u=" +
      originalLink,
      "facebook-popup",
      "height=350,width=600"
    );
    if (facebookWindow.focus) {
      facebookWindow.focus();
    }
  } else if ($(this).data('shareicon') == 'twitter') {
    var twitterWindow = window.open(
      "https://twitter.com/share?text=" + (videoDataObj.videoText) + "&" + "url=" + originalLink
    );
    if (twitterWindow.focus) {
      twitterWindow.focus();
    }
  } else if ($(this).data('shareicon') == 'whatsapp') {
    var whatsappWindow = window.open(
        "https://api.whatsapp.com/send?text=" + (videoDataObj.videoText + " " +originalLink)
      );
      if (whatsappWindow.focus) {
        whatsappWindow.focus();
      }
  } else if ($(this).data('shareicon') == 'linkedin') {
    var linkedinWindow = window.open(
        "https://www.linkedin.com/shareArticle?mini\x3dtrue\x26url\x3d" + (videoDataObj.videoText) + "&" + "url=" + originalLink, "width\x3d300,height\x3d200,personalbar\x3d0,toolbar\x3d0,scrollbars\x3d0,resizable\x3d0", 'linkedin-popup'
      );
      if (linkedinWindow.focus) {
        linkedinWindow.focus();
      }
  } else {
    location.href = $(this).attr('href')
  }
})
})
$('.dropdown-list-menu li').click(function(){
  $('[data-filter-card]').hide();
})

$(document).ready(function () {
/* select card show */
var allCardMainDiv = $('.watch-videos');
Array.from(allCardMainDiv).forEach(function(elementInArray,indexOfElement){
  var selectValue = $(elementInArray).find('.sort-outer-dropdown').find('.dropdown-list-menu li.active').text().trim()
   $(elementInArray).find('.jsDropDownMenuShow').text(selectValue)
   var activeList = $(elementInArray).find('.jsDropDownMenuShow').text();
   var allDiv = $(elementInArray).children('.blog-video-box').children().children('.blog-video-slider').children('.blog-slider-col')
   Array.from(allDiv).forEach(function (elementInArray, indexOfElement) {

    if (activeList.toLowerCase() != elementInArray.dataset.sortyearly.toLowerCase()) {
      $(elementInArray).addClass('webinar-card-hidden');
    }
    if(activeList.toLowerCase() == 'all cards'){
      $(elementInArray).removeClass('webinar-card-hidden');
    }

  });
   console.log(allDiv)
})
/* select card show */

  // dropdown js
  $('.js-filterBtnWatchVideo').click(function () {
    $('.js-filterBtn').not(this).removeClass('active');
    $(this).toggleClass('active');
    $('[data-filter-card]').slideUp('fast');

    var filterCard = $(this).attr('data-filter');
    var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
    toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
  });

  /* short by select option tab select to show
  var activeTabe =    $('.rating-filter-card').find('.active').text().trim();
  console.log(activeTabe)
 $('.jsDropDownMenuShow').text(activeTabe);
  short by select option tab select to show */

  /* shorting functionality */
  $('.jsDropDownMenuGetWatchVideo li a').click(function () {
    var activeList = $(this).text().trim();
    $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(activeList);
    $(this).parents('.jsDropDownMenuGetWatchVideo').find('li').removeClass('active');
    $(this).parents('li').addClass('active');

    var allDiv = $(this).parentsUntil('.watch-videos').find('[data-sortyearly]');
    Array.from(allDiv).forEach(function (elementInArray, indexOfElement) {
      if (!$(elementInArray).hasClass('webinar-card-hidden')) {
        if (activeList.toLowerCase() != elementInArray.dataset.sortyearly.toLowerCase()) {
          $(elementInArray).addClass('webinar-card-hidden');
        }
        if(activeList.toLowerCase() == 'all cards'){
          $(elementInArray).removeClass('webinar-card-hidden');
        }
      }
      else {
        if (activeList.toLowerCase() == elementInArray.dataset.sortyearly.toLowerCase()) {
          $(elementInArray).removeClass('webinar-card-hidden');
        }
        if(activeList.toLowerCase() == 'all cards'){
          $(elementInArray).removeClass('webinar-card-hidden');
        }
      }
    });
  });
  /* shorting functionality */
});