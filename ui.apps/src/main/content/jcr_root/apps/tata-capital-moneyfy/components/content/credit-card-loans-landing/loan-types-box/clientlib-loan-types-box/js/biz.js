/*credit card loan types js start*/
(function (_global) {
    var creditCardLoanTypesBizObj = (function (jsHelper) {
        var creditCardLoanTypesObj = {}
        document.addEventListener('DOMContentLoaded', function () {;
            $('[data-loan-item="redirectitem"]').click(function (event) {
                event.preventDefault();
                var loanName = $(this).find('h5').text().trim();
                loanItemClick(loanName,userIdObj.userId,'loan-types-box');
                location.href = $(this).attr('href');
            })
        })
        return jsHelper.freezeObj(creditCardLoanTypesObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "creditCardLoanTypesBizObj", creditCardLoanTypesBizObj)
})(this || window || {});
  /*credit card loan types js end*/
