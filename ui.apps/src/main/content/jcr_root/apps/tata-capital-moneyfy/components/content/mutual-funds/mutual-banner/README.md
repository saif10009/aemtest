Mutual banner box
====
The `Mutual banner box` component is for adding Banner box on moneyfy mututal funds page. 



## Feature
* It is an multifield component.
* All the various elements of the component such as title , heading , descriptions,image and link are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./sipCardHeading` Used for add heading text.
2. `./sipCardDescription` Used for add description text.
3. `./sipCardBtn` Used for add button text.
4. `./sipCardBtnLink` Used for add link in button .
5. `./sipBtnTarget` are used for open link in new tab.
6. `./sipBtnImg` are used for add image.
7. `./leftCardHeading` Used for add heading text in left side card.
8. `./leftCardTitle` Used for add text title in left side card.
9. `./leftCardDescription` Used for add description text in left side card .
10. `./leftCardBtnOne` Used for add button text of first button in left side card.
11. `./leftCardBtnOneLink` Used for add button link of first button in left side card .
12. `./leftCardBtnOneTarget` are used for open link in new tab.
13. `./leftCardBtnTwo` Used for add button text of second button in left side card.
14. `./leftCardBtnTwoLink` Used for add button link of second button in left side card .
15. `./bannerType` are used for open link in new tab.
16. `./leftCardBtnTwoTarget` are used for open link in new tab.
17. `./leftHeading` Used for add heading text in left side card.
18. `./leftDescription` Used for add description text in left side card .
19. `./leftBtnOne` Used for add button text of first button in left side card.
20. `./leftBtnOneLink` Used for add button link of first button in left side card .
21. `./leftBtnOneTarget` are used for open link in new tab.
22. `./leftBtnTwo` Used for add button text of second button in left side card.
23. `./leftBtnTwoLink` Used for add button link of second button in left side card .
24. `./leftBtnTwoTarget` are used for open link in new tab.


## Client Libraries
The component provides a `moneyfy.mutual-banner-box` editor client library category that includes CSS
handling for dialog interaction and it call in HTML


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5