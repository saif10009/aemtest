/*Retirement calculator render js start*/
(function (_global) {
    var retirementCalculatorRenderFn = (function (jsHelper) {
        var retirementCalculatorRenderObj = {}

        function renderRetirementCalculation(e, retirementInputObj) {
            let totalInvestment = document.getElementById("totalInvestment");
            let totalSip = document.getElementById("totalSip");
            let totalInterest = document.getElementById("totalInterest");
            let rateReturn = document.getElementById("rateReturn");
            let annualExpenceAmount = document.getElementById("annualExpenceAmount");
            let totalCorpus = document.getElementById("totalCorpus");
            if ((retirementInputObj.currentAge == "") || (retirementInputObj.retirementAge == "") || (retirementInputObj.currentAge > retirementInputObj.retirementAge) || (retirementInputObj.currentAge < 20) || (retirementInputObj.lifeExpectancy <= retirementInputObj.retirementAge) || (retirementInputObj.lifeExpectancy == "") || (retirementInputObj.currentExpenceInput == "") ||((retirementInputObj.currentExpenceInput > 1000000000)) || (retirementInputObj.currentCorpusInput == "") || isNaN(retirementInputObj.lifeExpectancy) || isNaN(retirementInputObj.currentExpenceInput) || isNaN(retirementInputObj.currentCorpusInput) ||(retirementInputObj.currentCorpusInput >100000000000) || isNaN(e.data.totalInvestmentResult) || isNaN(e.data.corpus)) {
                totalInterest.innerHTML = "₹ 0";
                totalSip.innerHTML = "₹ 0";
                rateReturn.innerHTML = ((e.data.rrr) / 100 * 10000).toFixed(2) + " %";
                annualExpenceAmount.innerHTML = "₹ 0 ";
                totalCorpus.innerHTML = "₹ 0";
                totalInvestment.innerHTML = "₹ 0";
                retirementInputObj.retirementHighchartReact(0, 0, 0)
            } else {
                totalInterest.innerHTML = "₹ " + Math.round(e.data.corpus - e.data.totalInvestmentResult).toLocaleString('en-IN');
                totalSip.innerHTML = "₹ " + Math.round(e.data.pmt).toLocaleString('en-IN');
                rateReturn.innerHTML = ((e.data.rrr) / 100 * 10000).toFixed(2) + " %";
                annualExpenceAmount.innerHTML = "₹ " + Math.round(e.data.annualExpense).toLocaleString('en-IN');
                totalCorpus.innerHTML = "₹ " + Math.round(e.data.corpus).toLocaleString('en-IN');
                totalInvestment.innerHTML = "₹ " + Math.round(e.data.totalInvestmentResult).toLocaleString('en-IN');
                retirementInputObj.retirementHighchartReact(Math.round(e.data.totalInvestmentResult), Math.round(e.data.corpus - e.data.totalInvestmentResult), 100)
            }
        }

        retirementCalculatorRenderObj.renderRetirementCalculation = renderRetirementCalculation;
        return jsHelper.freezeObj(retirementCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "retirementCalculatorRenderObj", retirementCalculatorRenderFn)
})(this || window || {});
  /*Retirement calculator render js end*/