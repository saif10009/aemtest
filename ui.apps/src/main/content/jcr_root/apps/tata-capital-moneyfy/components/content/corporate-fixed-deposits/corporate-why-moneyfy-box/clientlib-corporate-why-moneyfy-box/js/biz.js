/* Corporate-why-Moneyfy tab js start*/
(function (_global) {
    var corporateWhyMoneyfyBizfn = (function (jsHelper) {
        var corporateWhyMoneyfyObj = {}
        document.addEventListener('DOMContentLoaded', function () {
                corporateSlick();
        });
        function corporateSlick() { 
            $('#corporateBenefitSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {

                if ($('#corporateBenefitSlider .why-monefy-li').length <= 4) {
                  if ($(window).width() > 991) {
                    $('#corporateBenefitSlider').removeClass('slider-dots slick-dotted');
                  }
                }
              });
            $('#corporateBenefitSlider').slick({
                dots: true,
                infinite: false,
                speed: 500,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        }
        return jsHelper.freezeObj(corporateWhyMoneyfyObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "corporateWhyMoneyfyBizfn", corporateWhyMoneyfyBizfn)
})(this || window || {});
/* Corporate-why-Moneyfy tab js end*/