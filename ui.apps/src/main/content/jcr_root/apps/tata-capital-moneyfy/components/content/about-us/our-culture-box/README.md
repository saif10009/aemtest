Our culture box
====
This is a `Our culture box` component used in about us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as images, headings, description txt is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./cultureBoxHeading` Used for rendering header of the component.
2. `./cultureBoxDescription` Used for rendering description of the component.
3. `./cultureBoxImage` Used for rendering image on page.
4. `./imageSide` Used for select image side.
5. `./cultureBoxRowMultifield` Used for render multifiled over html.
6. `./imageSide` Used for select image side.


## Client Libraries
The component provides a `moneyfy.our-culture-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5