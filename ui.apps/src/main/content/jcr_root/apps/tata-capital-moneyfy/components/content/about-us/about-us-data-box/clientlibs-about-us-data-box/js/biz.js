/*about us data box slider js start*/
(function (_global) {
    var aboutUsDataBoxBizObj = (function (jsHelper) {
        var aboutUsDataBoxObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            aboutUsDataBoxSlick();
        })
        function aboutUsDataBoxSlick() {
            if ($(window).width() > 767) {
                if ($('#whyMonefySlider').hasClass('slick-initialized')) {
                    $('#whyMonefySlider').slick('unslick');
                }
                if ($('#aboutUsDataSlider').hasClass('slick-initialized')) {
                    $('#aboutUsDataSlider').slick('unslick');
                }
            } else {
                $('#whyMonefySlider').not('.slick-initialized').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 360,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });

                $('#aboutUsDataSlider').not('.slick-initialized').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true,
                    variableWidth: true,
                });
            }
        }
        return jsHelper.freezeObj(aboutUsDataBoxObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "aboutUsDataBoxBizObj", aboutUsDataBoxBizObj)
})(this || window || {});
  /*about us data box slider js end*/
