/*credit card loan js start*/
(function (_global) {
    var swpWhyChooseBizObj = (function (jsHelper) {
        var swpWhyChooseObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            adobeAnalyticsCalls();
        });

        function adobeAnalyticsCalls() {
            $('[data-adobeanalytics="swpWhyChoose"]').click(function () {
                var bannerTitle = document.querySelector('[data-whyChooseTitle]').dataset.whychoosetitle;
                var banneCTA = $(this).text().trim();
                bannerInteraction(bannerTitle, banneCTA, 'why-choode-swp', userIdObj.userId);
            });
            
        }

        return jsHelper.freezeObj(swpWhyChooseObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "swpWhyChooseBizObj", swpWhyChooseBizObj)
})(this || window || {});
  /*credit card loan js end*/
