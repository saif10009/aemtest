Product box
====
The `Product box` component is for adding product box on iho-schemes page.



## Feature
* It is an multifield based component.
* All the various elements of the component such as descriptions, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./productHeading` Used for add section heading.
2. `./productBackground ./buttonTwo` used to select background image for section.
3. `.leftCardTitle` Used to add title of card.
4. `./leftCardImg` are used for add card image.
5. `./leftCardHeading` are used for add heading in card.
6. `./checkOne ./checkTwo ./bannerCheck` are used to check if the link should open in new tabs.


## Client Libraries
The component provides a `moneyfy.product-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5