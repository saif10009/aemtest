/*sebi filter js start*/
(function(_global) {
    var sebiFundFilterRenderFn = (function(jsHelper) {
        var sebiFundFilterRenderObj = {};

        function render(filterlist) {
            for (var list in filterlist) {
                var dataId = '[data-id="' + list + '"]'
                var dataIds = '[data-ids="' + list + '"]'
                filterlist[list].forEach((value, index) => {
                    document.querySelector(dataId).innerHTML += '<li>' +
                        '    <a href="' + value.subCatPagePath + '">' + value.subCatName + '</a>' +
                        '</li>';

                    document.querySelector(dataIds).innerHTML += '<li>' +
                        '    <a href="' + value.subCatPagePath + '">' + value.subCatName + '</a>' +
                        '</li>';
                })
            }
        }
        sebiFundFilterRenderObj.render = render;

        function renderSebiList() {
            var sebiData = JSON.parse(sebiCatList);
            Object.keys(sebiData).forEach(function(keys) {
                var catElement = document.querySelector('[data-id="' + keys + '"]');
                var mobCatElement = document.querySelector('[data-ids="' + keys + '"]');
                if (jsHelper.isDef(catElement)) {
                    var htmlStr = '';
                    sebiData[keys].forEach(function(data) {
                        htmlStr += sebiCatElement(data);
                    });
                    catElement.innerHTML = htmlStr;
                    mobCatElement.innerHTML = htmlStr;
                }
            });
        }
        sebiFundFilterRenderObj.renderSebiList = renderSebiList;

        function sebiCatElement(data) {
            return '<li>' +
                '    <a href="' + data.subCatPagePath + '">' + data.subCatName + '</a>' +
                '</li>';
        }
        return jsHelper.freezeObj(sebiFundFilterRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "sebiFundFilterRenderObj", sebiFundFilterRenderFn)
})(this || window || {});
/*sebi filter js end*/