Credit card banner
====
This is a `credit card banner` component used in credit card loan page.

## Feature
* This is a multifield component.
* This component provides wide range of life goals.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./tabs` Used for add tabs on dialog box.
1. `./bannerLeft` this is a one tab.
1. `./bannerCard` this is second tab.
2. `./bannerHeadingText` Used for rendering heading of the component.
3. `./bannerTitletext` Used for rendering title of the component.
4. `./btnLink` Used to render redirection link of a card.
5. `./btnTarget` Enable open redirection link in a new tab.
6. `./btnText` Used to rendering the name of the button.
7. `./listData` Used for rendering list items.
8. `./enableCard` Used to enable and disable the card.
8. `./bannerCardmulti` Used to add multifield in component.
9. `./cardTitle` Used for rendering heading of the component.
10. `./cardHeading` Used for rendering title of the component.
12. `./cardDescription` Used for rendering description of the component.
4. `./cardBtnlink` Used to render redirection link of a card.
5. `./cardBtntarget` Enable open redirection link in a new tab.
6. `./cardBtn` Used to rendering the name of the button.
13. `./cardImg` Used for rendering image on page.



## Client Libraries
The component provides a `moneyfy.creditCard-banner` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5