Insurence partner nox
====
The `Insurence partner nox` component is for adding Work video box on insurence landing page. 



## Feature
* It is an multifield component.
* All the various elements of the component such as title , heading , descriptions and link are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./headingText` Used for add heading text.
2. `./imageMulti` Used for as an multifield for loop over html.
3. `./img` are used for add image path.


## Client Libraries
The component provides a `moneyfy.insuranc-partner-box` editor client library category that includes CSS
handling for dialog interaction and it call in HTML


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5