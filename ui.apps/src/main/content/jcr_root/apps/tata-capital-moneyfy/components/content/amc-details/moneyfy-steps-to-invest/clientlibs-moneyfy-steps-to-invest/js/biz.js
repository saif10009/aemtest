document.querySelector('.steps-to-invest .invest-now-btn').addEventListener('click', function(){
    if (!headerBizObj.getUserObj().isLoggedIn) {
        showLoginPopup();
        document.querySelector('[data-login="true"]').addEventListener('click', function () {
            location.href = appConfig.jocataDomain;
        });
    } else {
        location.href = appConfig.jocataDomain ;
    }
});