Why Moneyfy Benefits
====
Why Moneyfy Benefits written in HTL language used for displaying moneyfy benefits.
## Feature
* This is a multifield component.
* Used to display benefits of moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering title of the component.
2. `./heading` Used for rendering heading of the component.
3. `./image-path` Used to select image path for this component.
4. `./description` Used to render description of a card.
4. `./description` Used to render description of a card.
5. `./buttonTitle` Used to render button titile of a card.

## Client Libraries
The component provides a `moneyfy.home-loan-banner` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz` consist all the rendering logic of component on page. It is already included in component HTML file.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5