if(window.location.href.includes('pageType=webinar')){
    pageTypeWebinarRedirection()
}

function stayFormSubmit(){
    var learnWebinarStayUpdated = {};
    learnWebinarStayUpdated.name = $('#stay-registration-modal').find('.textbox-inner').find('[data-type="name"]').val();
    learnWebinarStayUpdated.mobileno = $('#stay-registration-modal').find('.textbox-inner').find('[data-type="mobile"]').val();
    learnWebinarStayUpdated.email = $('#stay-registration-modal').find('.textbox-inner').find('[data-type="email"]').val();

   var learnWebinarStayUpdatedOriginalData = {};
   learnWebinarStayUpdatedOriginalData.Master=[];
   learnWebinarStayUpdatedOriginalData.Master.push(learnWebinarStayUpdated)
   console.log(learnWebinarStayUpdatedOriginalData)
   learnWebinarStayUpdatedForm(learnWebinarStayUpdatedOriginalData)
  }

  function pageTypeWebinarRedirection(){
    var allBacktoWebinarsBtn = document.querySelectorAll('[data-form = webinar-thanks] a');
    allBacktoWebinarsBtn.forEach(function(element) {
        if(window.location.href.includes('pageType=webinar')){
            element.href = '/content/tata-capital-moneyfy/en/learn-webinar?pageType=webinar'
        } else {
            element.href = '/content/tata-capital-moneyfy/en/learn-webinar.html'
        }
    });
  }