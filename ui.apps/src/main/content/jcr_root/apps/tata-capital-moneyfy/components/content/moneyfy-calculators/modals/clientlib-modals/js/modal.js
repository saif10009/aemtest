$(document).ready(function () {

  $('.single-select3').select2({    
    dropdownParent: $('.full-select2')
  });
  
  $(document).on('focus', '.select2.select2-container', function (e) {
    if (e.originalEvent && $(this).find(".select2-selection--single").length > 0) {
      $(this).siblings('select').select2('open');    
      setTimeout(function (){
        const searchField = document.querySelector('.select2-search__field');
        if(searchField){
          searchField.focus();
        }
      }, 10)  
    }
  });

  $('#jsGetResultForms [data-type="checkbox"]').change(function() {
    if(this.checked) {
        $(this).parents('.form-group').removeClass('textboxerror');
    }else{
        $(this).parents('.form-group').addClass('textboxerror');
    }      
  });

  $('#jsGetResultForms .single-select3').change(function () {
    $(this).parents('.form-group').removeClass('textboxerror');    
    $(this).next('.error-msgs').remove();
  })

  $('#jsGetResultForms .jsGetResultBtn').click(function () {
    var ele_input = $('#jsGetResultForms .input-textbox:visible');   
    var selectElements = $('#jsGetResultForms .select2-hidden-accessible[data-type]:visible'); 
    var checkboxElements = $('#jsGetResultForms [data-type="checkbox"]');
    var errors = [];
    allFilled = true;
    var ele_required = "Field is required";

    $(selectElements).each(function () {
      var select = $(this);
      $(select).parents('.form-group').find('.error-msgs').remove();

      if ($(select).val() == '') {
          allFilled = false;
          $(select).parents('.form-group').addClass('textboxerror');
          $(select).next('.error-msgs').remove();
          $(select).after('<span class="error-msgs">' + ele_required + '</span>');
          errors.push(ele_required);
      } else {
          $(select).parents('.form-group').removeClass('textboxerror');
          $(select).next('.error-msgs').remove();
      }
    });

    $(ele_input).each(function () {
        var element = $(this);
        var ele_value = element.val();
        var ele_name = "Enter full name as PAN card";
        var ele_phoneNumber = "Please enter valid Phone Number";
        var ele_email = "Please enter valid email ID";

        $(element).next().remove();

        if (element.is(":visible")) {
            if (element.val() != '') {
                $(element).after('<span class="error-msgs"></span>');
                if ($(element).data('type') === 'name') {
                    var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

                    if (ele_value != '' && !ele_value.match(regName)) {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).next('.error-msgs').text(ele_name);
                        errors.push(ele_name);
                    }
                    else {
                        $(element).parents('.form-group').removeClass('textboxerror');
                        $(element).next().text('');
                    }
                }
                if ($(element).data('type') === 'email') {
                  var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;      
                  if (ele_value != '' && !ele_value.match(regEmail)) {
                    $(element).parents('.form-group').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_email);
                    errors.push(ele_email);
                  }
                  else {
                    $(element).parents('.form-group').removeClass('textboxerror');
                    $(element).next().text('');
                  }
                }
                if ($(element).data('type') === 'mobile') {
                    if (!validateMobile(element)) {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).next('.error-msgs').text(ele_phoneNumber);
                        errors.push(ele_phoneNumber);
                    } else {
                        $(element).parents('.form-group').removeClass('textboxerror');
                        $(element).next().text('');
                        $(this).next('.error-msgs').remove();
                    }
                }
            } else {
                $(element).parents('.form-group').addClass('textboxerror');
                $(element).after('<span class="error-msgs">' + ele_required + '</span>');
                errors.push(ele_required);
            }
        }
       
    });

    $('#jsGetResultForms [data-type="checkbox"]').next('.error-msgs').remove();
    // checkbox validation
    if(checkboxElements.prop("checked") == false){
        checkboxElements.parents('.form-group').addClass('textboxerror');
        $(checkboxElements).parents('.agree-checkbox').next('.error-msgs').remove();
        checkboxElements.parents('.agree-checkbox').after('<span class="error-msgs">' + ele_required + '</span>');
        errors.push(ele_required);
    } else {
        $(checkboxElements).parents('.form-group').removeClass('textboxerror');
        $(checkboxElements).parents('.agree-checkbox').next('.error-msgs').remove();
    }

    if (errors.length == 0) {
      // if(errors.length==0){
        calcGetEmailApiObj.generateOTP();
      // }
        var getWhatsappNub = $('.jsGetWhatsappNumber').val();
        var jsGetNumberLast = String(getWhatsappNub).slice(-2);
        $('.jsShowWhatsappNumber').text(jsGetNumberLast);

        var getEmail = $('#jsGetResultForms .js-emailField').val();
        var emailEncrypt = getEmail.replace(/(\w{2})(.*)(\w{2})@(.*)/,'$1XXXXXX$3@$4');
        $('.js-encryptEmail').text(emailEncrypt);
        console.log(emailEncrypt);

        $('[data-otpverify]').removeClass('d-none');
        $('[data-forms]').addClass('d-none');        
        $('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();

       // $('#jsGetResultForms .input-textbox').val(''); 
        $('#jsGetResultForms .single-select3').val(null).trigger('change');
    }
});


$('#jsGetResultForms .input-textbox[data-type]').keyup(function () {
  var element = $(this);
  var ele_value = element.val();
  var ele_required = 'Field is required';
  var ele_name = "Enter full name as PAN card";
  var ele_phoneNumber = "Please enter valid number";
  var ele_email = "Please enter valid email ID";

  $(this).next('.error-msgs').remove();
  $(this).after('<span class="error-msgs"></span>');
  $(this).parents('.form-group').addClass('textboxerror');

  if ($(element).val() != '') {
      if ($(element).data('type') === 'name') {
          var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

          if (ele_value != '' && !ele_value.match(regName)) {
              $(element).parents('.form-group').addClass('textboxerror');
              $(element).next('.error-msgs').text(ele_name);
          }
          else {
              $(element).parents('.form-group').removeClass('textboxerror');
              $(element).next().text('');
          }
      }
      if ($(element).data('type') === 'mobile') {
          if (!validateMobile(element)) {
              $(element).parents('.form-group').addClass('textboxerror');
              $(element).next('.error-msgs').text(ele_phoneNumber);
          } else {
              $(element).parents('.form-group').removeClass('textboxerror');
              $(element).next().text('');
              $(this).next('.error-msgs').remove();
          }
      }
      if ($(element).data('type') === 'email') {
        var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

        if (ele_value != '' && !ele_value.match(regEmail)) {
          $(element).parents('.form-group').addClass('textboxerror');
          $(element).next('.error-msgs').text(ele_email);
        }
        else {
          $(element).parents('.form-group').removeClass('textboxerror');
          $(element).next().text('');
        }
      }

  } else {
      $(element).next('.error-msgs').text(ele_required);
  }
});


$("#jsVeryfyOTP .js-OtpBox .input-textbox").keyup(function () {    
    if (this.value.length == this.maxLength) {
        $(this).next('.input-textbox').focus();
        $(this).next('.input-textbox').removeClass('pointer-none');
    } else {
        $(this).prev('.input-textbox').focus();
        $(this).addClass('pointer-none');
        $('#jsVeryfyOTP .input-textbox:first').removeClass('pointer-none');
    }    

    var ele_input = $('.js-OtpBox .input-textbox');
    $(ele_input).each(function () {
        if ($(this).val().length != 0) {
            $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').removeClass('btn-disabled');
            $(this).parents('.form-group').addClass('active');
        }
        else {
            $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
        }
    });
});

$('.jsVeryfyBtn').click(function(){
  $('#jsGetResultForms .input-textbox').val(''); 
    var values = []
    $('#jsVeryfyOTP .js-OtpBox .input-textbox').each(function (i, ele) { values.push(ele.value) });
    console.log(values.join(""))
    apiCall.verifyOTP();       
  })

  $('.jsTryAgainOtp').click(function(){
      $('[otp-unsuccessfull]').addClass('d-none');
      $('[data-otpverify]').removeClass('d-none');
      $('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
      $('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
  })

  $('#jsVeryfyOTP .js-resendOTP').click(function () {
      $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox').val('');
      $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
      $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled'); 
      var getMobileNumber = calcObj.mobileNumber;
      var firstTwoDigit = getMobileNumber.toString().slice(-2);
      document.querySelector('#jsVeryfyOTP .jsShowWhatsappNumber').innerText = firstTwoDigit;
      apiCall.generateOTP();     
  });

  $('.jsGetResultClose').click(function(){
    $('[data-forms]').removeClass('d-none');
    $('[data-otpverify]').addClass('d-none');
    $('[otp-successfull]').addClass('d-none');
    $('[otp-unsuccessfull]').addClass('d-none');
    $('#jsGetResultForms .input-textbox').val(''); 
    $('#jsGetResultForms .form-group').removeClass('textboxerror');
    $('#jsGetResultForms .input-textbox').next().text('');    
    $('#jsGetResultForms .single-select3').val(null).trigger('change');
    $('#jsGetResultForms [data-type="checkbox"]').prop("checked", false);
  })
});




// validation mobile
function validateMobile(mobileField) {
  var re = /^[6-9][0-9]{9}$/;
  var check = re.test($(mobileField).val());
  if ($(mobileField).val().length != 10 || !check) {
    return false;
  } else {
    return true;
  }
}
