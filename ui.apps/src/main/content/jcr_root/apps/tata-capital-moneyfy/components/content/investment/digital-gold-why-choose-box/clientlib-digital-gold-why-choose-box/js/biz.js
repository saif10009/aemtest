(function (_global) {

    var whyChooseDigitalGoldBizFnObj = (function (jsHelper) {
        var whyChooseDigitalGoldObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            /*choose digital gold js*/
            $(".js-digiGoldMobileSlider").slick({
                infinite: true,
                arrows: false,
                dots: false,
                autoplay: false,
                speed: 300,
                slidesToShow: 1,
                fade: true,
                cssEase: 'linear',
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            dots: true,
                            arrows: true,
                            fade: false,
                        }
                    },
                ]
            });

            //progress time
            var percentTimeDigiGold;
            var tickDigiGold;
            var timeDigiGold = 0.1;
            var progressBarIndexDigiGold = 0;

            function startDigiGoldProgressbar() {
                resetDigiGoldProgressbar();
                percentTimeDigiGold = 0;
                tickDigiGold = setInterval(intervalDigiGold, 15);
            }

            function intervalDigiGold() {
                if (($('.js-digiGoldMobileSlider .slick-track div[data-slick-index="' + progressBarIndexDigiGold + '"]').attr("aria-hidden")) === "true") {
                    progressBarIndexDigiGold = $('.js-digiGoldMobileSlider .slick-track div[aria-hidden="false"]').data("slickIndex");

                    startDigiGoldProgressbar();
                } else {
                    $('.js-digiGOldprogressBarContainer .make-li').removeClass('active');
                    $('.js-digiGOldprogressBarContainer svg[data-slick-index="' + progressBarIndexDigiGold + '"]').parents('.make-li').addClass('active');

                    percentTimeDigiGold += 1 / (timeDigiGold + 5);

                    $('.choose-digiGold-box .inProgress' + progressBarIndexDigiGold).css({
                        strokeDashoffset: percentTimeDigiGold + 35
                    });
                    if (percentTimeDigiGold >= 100) {
                        $('.choose-digiGold-box .progress-slider').slick('slickNext');
                        progressBarIndexDigiGold++;
                        if (progressBarIndexDigiGold > 2) {
                            progressBarIndexDigiGold = 0;
                        }
                        startDigiGoldProgressbar();
                    }
                }
            }

            function resetDigiGoldProgressbar() {
                $('.choose-digiGold-box .inProgress').css({
                    strokeDashoffset: 0
                });
                clearInterval(tickDigiGold);
                console.log('dg')
            }
            startDigiGoldProgressbar();
            //progress time end

            $('.js-digiGOldprogressBarContainer .make-li').click(function () {
                resetDigiGoldProgressbar();
                var goToThisIndexDigiGold = $(this).find("svg").data("slickIndex");
                $('.choose-digiGold-box .progress-slider').slick('slickGoTo', goToThisIndexDigiGold, false);
                startDigiGoldProgressbar();
            });
            /*choose digital gold js*/
        });

        return jsHelper.freezeObj(whyChooseDigitalGoldObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'whyChooseDigitalGoldObj', whyChooseDigitalGoldBizFnObj);
})(this || window || {});