(function (_global) {
    var stepsToInvestRenderObj = (function (jsHelper) {
        var stepsToInvestRenderObj = {};

        function stepsToInvestHeadingRender() {
            var stepsToInvestTitle = document.querySelector('.steps-to-invest [data-heading="steps-to-invest"]');
            stepsToInvestTitle.innerText = 'Steps to invest in ' + stepsToInvestTitle.innerText.split(' ').reverse().splice(2).reverse().join(' ')
                + ' via Tata Moneyfy'
        }
        stepsToInvestHeadingRender();
        
        stepsToInvestRenderObj.stepsToInvestHeadingRender = stepsToInvestHeadingRender;

        return jsHelper.freezeObj(stepsToInvestRenderObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, "stepsToInvestRenderObj", stepsToInvestRenderObj);
})(this || window || {});