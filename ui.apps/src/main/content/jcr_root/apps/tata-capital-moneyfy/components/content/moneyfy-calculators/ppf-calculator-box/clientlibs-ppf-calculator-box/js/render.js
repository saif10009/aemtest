/*ppf calculator render js start*/
(function (_global) {
    var ppfCalculatorRenderFn = (function (jsHelper) {
        var ppfCalculatorRenderObj = {}

        function renderPpfCalculation(e,inputObj) {
            var fvVal = document.getElementById("fvVal");
            var totalInvestment = document.getElementById("totalInvestment");
            var totalEarned = document.getElementById("totalEarned");
            var resultText = document.getElementById('starts-investing-text');

            if ((isNaN(inputObj.ppfAmount)) || (inputObj.ppfAmount < 0) || (inputObj.ppfAmount > 150000)) {
                fvVal.innerHTML = "₹ 0";
                totalInvestment.innerHTML = "₹ 0";
                totalEarned.innerHTML = "₹ 0";
                resultText.innerHTML = "If you invest ₹ 0 per yer today for " + e.data.ppfMonths + " you will get";
                inputObj.ppfHighchartReact(0, 0, 0)
            }
            else {
                fvVal.innerHTML =
                    "₹ " + Math.round(e.data.ppf).toLocaleString('en-IN');
                totalInvestment.innerHTML = "₹ " + Math.round(e.data.ppfInvestment).toLocaleString('en-IN');
                totalEarned.innerHTML = "₹ " + Math.round(e.data.ppf - e.data.ppfInvestment).toLocaleString('en-IN');
                resultText.innerHTML = "If you invest ₹ " + (inputObj.ppfAmount).toLocaleString('en-IN') + " per year from today for " + e.data.ppfMonths + " you will get";
                inputObj.ppfHighchartReact(Math.round(e.data.ppfInvestment), Math.round(e.data.ppf - e.data.ppfInvestment), 100)
            }
        }

        ppfCalculatorRenderObj.renderPpfCalculation = renderPpfCalculation;
        return jsHelper.freezeObj(ppfCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "ppfCalculatorRenderObj", ppfCalculatorRenderFn)
})(this || window || {});
  /*ppf calculator render js end*/