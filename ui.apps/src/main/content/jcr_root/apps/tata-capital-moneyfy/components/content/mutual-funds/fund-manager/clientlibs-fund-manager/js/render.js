/*Fund Manager Reneder js Start*/
(function (_global) {
    var fundManagerRenderFnObj = (function (jsHelper) {
        var fundManagerRenderObj = {}

        var renderFundManager = function (managersArray) {
            var count = 0;
            var htmlRenderStr = '';
            managersArray.forEach(function (fundManager) {
                var path = '/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/';
                path = path.concat(fundManager.name.toLowerCase().replaceAll(' ', '-').concat('.html'));
                if (count <= 5) {
                    count = count + 1;
                    htmlRenderStr += '<a href="' + path + '" class="fund-manager-rows">' +
                        '                <div class="fund-manager-col">' +
                        '                  <div class="card-div fund-manager-card">' +
                        '                    <div class="fund-manager-top">' +
                        '                      <h5>' + fundManager["name"] + '</h5>' +
                        '                    </div>' +
                        '                    <div class="fund-manager-bottom">' +
                        '                      <div class="fund-ul">' +
                        '                        <div class="fund-li">' +
                        '                          <p>No. of funds</p>' +
                        '                          <h6>' + fundManager["totalFunds"] + '</h6>' +
                        '                        </div>' +
                        '                        <div class="fund-li">' +
                        '                          <p>Total Fund Size</p>' +
                        '                          <h6>₹ ' + (Math.round(Number(fundManager["totalFundSize"])).toLocaleString('en-IN')) + ' Cr</h6>' +
                        '                        </div>' +
                        '                        <div class="fund-li">' +
                        '                          <p>Highest Returns</p>' +
                        '                          <h6>' + fundManager["highestReturns"] + '%</h6>' +
                        '                        </div>' +
                        '                      </div>' +
                        '                    </div>' +
                        '                  </div>' +
                        '                  <div class="card-div fund-manager-card mob-card-show" style="display:none">' +
                        '                    <div class="fund-manager-top">' +
                        '                      <h5>' + fundManager["fundManagerName"] + '</h5>' +
                        '                    </div>' +
                        '                    <div class="fund-manager-bottom">' +
                        '                      <div class="fund-ul">' +
                        '                        <div class="fund-li">' +
                        '                          <p>No. of funds</p>' +
                        '                          <h6>' + fundManager["noOfFunds"] + '</h6>' +
                        '                        </div>' +
                        '                        <div class="fund-li">' +
                        '                          <p>Total Fund Size</p>' +
                        '                          <h6>₹' + fundManager["totalFundSize"] + ' Cr</h6>' +
                        '                        </div>' +
                        '                        <div class="fund-li">' +
                        '                          <p>Highest Returns</p>' +
                        '                          <h6>' + fundManager["highestReturns"] + '%</h6>' +
                        '                        </div>' +
                        '                      </div>' +
                        '                    </div>' +
                        '                  </div>' +
                        '                </div>' +
                        '</a>';
                }
            });
            document.getElementById('fundManagerSlider').innerHTML = htmlRenderStr;
        }

        fundManagerRenderObj.renderFundManager = renderFundManager;

        return jsHelper.freezeObj(fundManagerRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundManagerRenderObj", fundManagerRenderFnObj);
})(this || window || {});
/*Fund Manager Reneder js end*/