NFO
====
The `NFO` component is for showing the new fund offerings.



## Feature
* It is an multifield component.
* All the various elements of the component such as title , heading , descriptions are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used for add heading text.
2. `./description` Used for add description text.
3. `./button` Used for add button text.


## Client Libraries
The component provides a `moneyfy.choose-from-amc` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML. `moneyfy.jquery`,`moneyfy.aos`,`moneyfy.slick` these dependancies libraries are use for load jquery,aos and slick file in component.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5