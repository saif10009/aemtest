Corporate Fd Steps
====
corporate fd steps component written in HTL, used to redirect the component on other pages.

## Feature
* This is a multifield component.
* Used to show steps of corporate fd on page.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./corporateTitle` Used for rendering title of the component.
2. `./corporateHeading` Used for rendering main heading of the component.
3. `./moneyfyList` Used to create multifield in component.
4. `./corporateBtnOne` Used for rendering button title on component.
5. `./corporateBtnOneLink` Used for rendering link of button
6. `./corporateBtnOneTarget` used for targeting link in page.
7. `./corporateBtnTwo` Used for rendering button title on component.
8. `./corporateBtnTwoLink` Used for rendering link of button
9. `./corporateBtnTwoTarget` used for targeting link in page.
10. `./corporateBackground` used for add  background image in component.

## Client Libraries
The component provides a `moneyfy.steps-to-start` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.nps-landing-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-nps-landing`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5