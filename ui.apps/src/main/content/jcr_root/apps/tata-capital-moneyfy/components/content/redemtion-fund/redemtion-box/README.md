Redemtion box
====
The `Redemtion box` component is added on page.



## Feature
* It is an authorable component.



## Edit Dialog Properties
1. `./jcr:title` - will store the text of the title to be rendered.
2. `./heading` - will store the heading of the component.
3. `./description` - will store the description of the component.
4. `./subTitle` - will store the benefit title of the component.
5. `./boxlist` - is use for rendering multifield on page.
6. `./benefittypeImage` - is use for rendering image on page.
7. `./benifitdescription` - is use for rendering benefit discription on page.


## Client Libraries
The component provides a `moneyfy.redemtion-box` editor client library category that includes JavaScript and CSS handling for dialog interaction and it call in HTML.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5