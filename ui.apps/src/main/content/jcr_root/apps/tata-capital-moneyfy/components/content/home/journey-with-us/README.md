Financial Journey With Us
====
Journey with us component written in HTL, used to provide different financial plains.

## Feature
* This is a multifield component.
* Used to provide different financial journey plan.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./mainHeading` Used for rendering title of the component.
2. `./description` Used for rendering description of the component.
3. `./multi` Used to create multifield in component.
4. `./link` Used to render redirection link of a card on page.
5. `./linkInNewtab` Enable open redirection link in new tab.
6. `./img` Used for rendering image on page.
7. `./hoverImage` Used for rendering hover image on page.
8. `./heading` Used for rendering heading of card on page

## Client Libraries
The component provides a `moneyfy.journeywithUs` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.homePage` clientlib path `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5