/*risk-info js start*/
(function(_global) {
  var riskInfoBizObj = (function(jsHelper) {
      var riskInfoObj = {}
      document.addEventListener('DOMContentLoaded', function() {
          riskInfoslick();
          riskInfoClick();
          adobeAnalytics();
      })
      
      function riskInfoslick() {
        if ($(window).width() > 991) {
            if ($('#jsRiskInfoSlider').hasClass('slick-initialized')) {
              $('#jsRiskInfoSlider').slick('unslick');
            }
          } else {
            $('#jsRiskInfoSlider').not('.slick-initialized').slick({
              dots: true,
              infinite: false,
              speed: 300,
              slidesToShow: 2,
              slidesToScroll: 1,
              arrows: false,
              responsive: [        
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                },
                {
                  breakpoint: 360,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                  }
                }
              ]
            });
          }
      }
      function riskInfoClick(){
        $('.jsRiskTabList [data-risktab]').click(function(){
            $('.jsRiskTabList li a').removeClass('active-tab');
            $(this).addClass('active-tab');
            var ele_id = $(this).attr('data-risktab');
            $('.jsRiskTabRow').addClass('d-none');
            $('#' + ele_id).fadeIn().removeClass('d-none');
          });
        $('[data-action="risk-profile-login"]').click(function () {
          if (!headerBizObj.getUserObj().isLoggedIn) {
            showLoginPopup();
            document.querySelector('[data-login="true"]').addEventListener('click', function () {
              location.href = appConfig.jocataDomain + "?action=risk-profile&source=" + window.location.href;
            });
          }else{
            location.href = appConfig.jocataDomain + "?action=risk-profile&source=" + window.location.href;
          }
        })
      }
      function adobeAnalytics(){
        $('[data-riskbtn]').click(function(event){
            event.preventDefault();
            if($(this).data('riskbtn') == "riskInvestBtn"){
                  var ctaText = $(this).text().trim();
                  var ctaTitle = $(this).parents('.sip-cards-info').find('h4').text().trim();
                  allCTAInteraction(ctaText,ctaTitle,'risk-info',userIdObj.userId)
                  location.href = $(this).attr('href')
              }else{
                allCTAInteraction('Image','Only Image','risk-info', userIdObj.userId)
                location.href = $(this).attr('href')
            }
        })

        $('[data-risktabredirect]').click(function(event){
            event.preventDefault();
            if($(this).data('risktabredirect') == 'riskTabRedirectHeading'){
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parents('.sip-cards-info').find('h4').text().trim();
                allCTAInteraction(ctaText,ctaTitle,'risk-info', userIdObj.userId)
                location.href = $(this).attr('href')
            }else{
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parents('.risk-tab-left').find('[data-cardheading]').data('cardheading');;
                allCTAInteraction(ctaText,ctaTitle,'risk-info', userIdObj.userId)
                location.href = $(this).attr('href')
            }
        })
      }
      
      return jsHelper.freezeObj(riskInfoObj);
  })(jsHelper)
  _global.jsHelper.defineReadOnlyObjProp(_global, "riskInfoBizObj", riskInfoBizObj)
})(this || window || {});
/*risk-info js end*/