/*popular categories box js start*/
(function(_global) {
    var popCatBoxBizFnObj = (function(jsHelper) {
        var popCatBoxBizObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            if($('[data-popregisterbtn]') != undefined){
                $('[data-popregisterbtn]').click(function(){
                    var ctaText = $(this).text().trim();
                    var ctaTitle = $(this).parents('.invest-inlarge-box').find('h3').text().trim();
                    allCTAInteraction(ctaText,ctaTitle,'popular-categories-box', userIdObj.userId)
                    location.href = appConfig.jocataDomain;
                })
            }
            
            if (headerBizObj.getUserObj().isLoggedIn) { 
                if(!document.querySelector('[data-journey="invest-inlarge"]').classList.contains('d-none')){
                    document.querySelector('[data-journey="invest-inlarge"]').classList.add('d-none');
                }
            } else {
                if(document.querySelector('[data-journey="invest-inlarge"]').classList.contains('d-none')){
                    document.querySelector('[data-journey="invest-inlarge"]').classList.remove('d-none');
                }
            }
            var imgHandle = document.getElementById('paddingHandle');
            if (imgHandle) {
                document.querySelector('.invest-inlarge-box .invest-inlarge-mx').style.paddingRight = '150px';
            }
        });

        return jsHelper.freezeObj(popCatBoxBizObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "popCatBoxBizObj", popCatBoxBizFnObj)
})(this || window || {});
/*popular categories box js end*/  