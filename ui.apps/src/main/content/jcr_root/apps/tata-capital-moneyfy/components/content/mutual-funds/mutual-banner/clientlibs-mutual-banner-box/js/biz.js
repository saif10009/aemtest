/*mutual banner js start*/
(function (_global) {
    var mutualBannerBizFn = (function (jsHelper) {
        var mutualBannerObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            adobeAnalytics();
        });

        function adobeAnalytics(){
            $('[data-siprightcard] a').click(function(event){
                event.preventDefault();
                var bannerTitle = $('[data-info]').find('h4').text().trim();
                var bannerCTA = $(this).text().trim()
                bannerInteraction(bannerTitle,bannerCTA,'mutual-banner', userIdObj.userId)
                location.href = $(this).attr('href');
            });
            $('[data-sipleftcard] a').click(function(event){
                event.preventDefault();
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parents('.banner-left').find('h1').text().trim();
                allCTAInteraction(ctaText,ctaTitle,'mutual-banner', userIdObj.userId)
                location.href = $(this).attr('href');
              })
        }
        return jsHelper.freezeObj(mutualBannerObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "mutualBannerBizObj", mutualBannerBizFn)
})(this || window || {});
/*mutual banner js end*/