/*credit card loan js start*/
(function (_global) {
    var creditCardLoanBizObj = (function (jsHelper) {
        var creditCardLoanObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            creditCardLoanSlick();
            $('[data-creditredirect="adobecall"]').click(function (event) {
                event.preventDefault();
                var bannerTitle = $(this).parent().parent().find('h4').text().trim();
                var bannerCTA = $(this).text().trim()
                bannerInteraction(bannerTitle,bannerCTA,'creditCard-banner',userIdObj.userId);
                location.href = $(this).attr('href');
            })
        })
        function creditCardLoanSlick() {
            $('#creditBannerSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
        }
        return jsHelper.freezeObj(creditCardLoanObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "creditCardLoanBizObj", creditCardLoanBizObj)
})(this || window || {});
  /*credit card loan js end*/
