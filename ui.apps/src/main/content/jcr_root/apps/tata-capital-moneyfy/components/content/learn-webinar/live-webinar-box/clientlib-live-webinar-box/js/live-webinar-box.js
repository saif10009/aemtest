

   var videoDataObj = {}
   setTimeout(function(){
     $('[data-target="#share-modal"]').click(function () {
         videoDataObj.videoText = $(this).data('heading');
         console.log(videoDataObj.videoText)
         videoDataObj.videoLinks = $(this).data('video').replace('/embed','');
         originalLink = videoDataObj.videoLinks.replace('youtube.com','youtu.be');       
         $('.copy-links input').val(originalLink);        
       })
 }, 1000)   

 $('[data-copybtn]').click(function (event) {
   event.preventDefault()
   var ctaText = $(this).text().trim();
   $(this).parents('.copy-links').find('input').select();
   document.execCommand("copy");
   // navigator.clipboard.writeText($(this).parents('.copy-links').find('input').val());
   // navigator.clipboard.   write($(this).parents('.copy-links').find('input').val());
 })

 $('[data-shareicon]').click(function (event) {
   event.preventDefault()
   if ($(this).data('shareicon') == 'facebook') {
     var facebookWindow = window.open(
       "https://www.facebook.com/sharer/sharer.php?hashtag=" +
       videoDataObj.videoText +
       "&" +
       "u=" +
       originalLink,
       "facebook-popup",
       "height=350,width=600"
     );
     if (facebookWindow.focus) {
       facebookWindow.focus();
     }
   } else if ($(this).data('shareicon') == 'twitter') {
     var twitterWindow = window.open(
       "https://twitter.com/share?text=" + (videoDataObj.videoText).replace('|','%7C') + "&" + "url=" + originalLink
     );
     if (twitterWindow.focus) {
       twitterWindow.focus();
     }
   } else if ($(this).data('shareicon') == 'whatsapp') {
     location.href = $(this).attr('href')
   } else {
     location.href = $(this).attr('href')
   }
 })