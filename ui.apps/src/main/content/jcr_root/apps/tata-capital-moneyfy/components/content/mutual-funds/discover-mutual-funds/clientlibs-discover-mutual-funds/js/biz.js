/*discover mutual fund biz js start*/
(function (_global) {
    var discoverMutualFundBizFn = (function (jsHelper) {
      var discoverMutualFundBizObj = {};
      document.addEventListener("DOMContentLoaded", function () {
        adobeAnalytics();
      });
      function adobeAnalytics() {
        $("[data-discoverfund]").click(function (event) {
          event.preventDefault();
          var fundType = $(this).text().trim();
          fundCategoryItemClick('Discover Mutual Fund', userIdObj.userId, fundType);
          location.href = $(this).attr("href");
        });
        
        $('[data-discovermore]').click(function(event){
          event.preventDefault();
          var ctaTitle = $(this).parents('.discover-mutual-box').find('h2').text().trim()
          var ctaText = $(this).text().trim();
          allCTAInteraction(ctaText,ctaTitle,'discover-mutual-funds', userIdObj.userId);
          location.href = $(this).attr("href");
        })
      }
      return jsHelper.freezeObj(discoverMutualFundBizObj);
    })(jsHelper);
  
    _global.jsHelper.defineReadOnlyObjProp( _global,"discoverMutualFundBizObj",discoverMutualFundBizFn);
  })(this || window || {});
  
  /*discover mutual fund biz js end*/
  

