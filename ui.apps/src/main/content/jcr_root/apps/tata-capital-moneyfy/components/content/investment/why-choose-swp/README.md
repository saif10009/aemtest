Why Choose SWP 
====
The `Why Choose SWP` component provides systematic plan to withdraw your mutual scheme. 



## Feature
* It is a Multifield component.    


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` used to render heading.
2. `./description` Used for description.
3. `./image` used to render image.
4. `./headingFeature` used to render heading for features and benefits.
5. `./featuresBenefits` used to describe the features and benefits.
6. `./buttonTitle` used to render the title of button.
7. `./buttonLink used to provide link for redirection.
8. `./newTab` uses checkbox which redirects into new tab.

## Client Libraries
The component provides a `moneyfy.why-choose-swp` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.swp-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-swp.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5