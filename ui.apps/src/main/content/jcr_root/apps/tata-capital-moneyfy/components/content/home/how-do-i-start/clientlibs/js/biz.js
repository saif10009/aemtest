/*how-do-i-start js*/
(function (_global) {

    var howDoIStartBizObj = (function (jsHelper) {
        var howDoIStartObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            adobeAnalytics();
        });
        function adobeAnalytics() {
            $('[data-howstartbtn] a').click(function (event) {
                event.preventDefault();
                var clickButtonTitle = $(this).text().trim();
                var clickTextTitle = $(this).parents('.how-work-box').find('h2').text();
                allCTAInteraction(clickButtonTitle, clickTextTitle, 'how-do-i-start',userIdObj.userId)
                location.href = $(this).attr('href')
            });
        }

        return jsHelper.freezeObj(howDoIStartObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'howDoIStartBizObj', howDoIStartBizObj);
})(this || window || {});
/*how-do-i-start js*/