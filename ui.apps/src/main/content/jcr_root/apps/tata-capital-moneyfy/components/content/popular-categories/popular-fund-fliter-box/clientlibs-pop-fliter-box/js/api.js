/* Popular categories Tab APi.js */
(function(_global) {
    var popularCategoriesApiFn = (function(jsHelper) {
        var popularCategoriesfilterObj = {};

        var fundSearchApi = function(requestObj) {
            return new Promise(function(resolve) {
                apiUtility.getSearchApi(requestObj).then(function(response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function(error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }

        var popularCategoryFundList = function(requestObj) {
            return new Promise(function(resolve) {
                apiUtility.popularCategoryFundList(requestObj).then(function(response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function(error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }

        /* Add To WatchList Api calling */
        var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        
         /* Fetch from Portfolio Api calling */
         var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        popularCategoriesfilterObj.fetchFundsData = getFundsData;
        popularCategoriesfilterObj.fetchFromPortfolio = fetchFromPortfolio;
        popularCategoriesfilterObj.fetchwatchListApi = fetchwatchListApi;
        popularCategoriesfilterObj.addTowatchListApi = addTowatchListApi;
        popularCategoriesfilterObj.fundSearchApi = fundSearchApi;
        popularCategoriesfilterObj.popularCategoryFundList = popularCategoryFundList;

        return jsHelper.freezeObj(popularCategoriesfilterObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'popularCategoriesfilterObj', popularCategoriesApiFn);
})(this || window || {});
/* Popular categories Tab APi.js end*/