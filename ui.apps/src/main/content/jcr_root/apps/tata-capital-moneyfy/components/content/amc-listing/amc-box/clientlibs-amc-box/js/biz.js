/*AMC listing biz.js start*/
(function (_global) {
    var amcListingBizObj = (function (jsHelper) {
        var amcListingObj = {};
        var lessThanFiveHundred = [];
        var lessThanTwoThousand = [];
        var lessThanTenThousand = [];
        var greaterThanTenThousand = [];
        var filterType = {};
        var renderArray = [];
        var mobSortBy = ['aum - high to low'];

        document.addEventListener('DOMContentLoaded', function () {
            createArray(amcArray);
            //filterFunds('aum - high to low', amcArray);
            if (amcArray.length > 0) {
                var sortedArray = amcArray.sort(function (a, b) {
                    return Number(b.aum) - Number(a.aum);
                });
            }
            amcListingRenderObj.renderAmc(amcArray);
            showCards();
            searchAMC();
            eventInitializer();

            //clear all checkBox onLoad
            setTimeout(function () {
                document.querySelectorAll('input[name="sort"]:checked').forEach(function (checkbox) {
                    checkbox.checked = false;
                });
            }, 200)

            document.querySelector('#amcListFilter').addEventListener('change', function (event) {
                var sortBy = event.target.value;
                console.log(sortBy);
                amcItemClick(sortBy, userIdObj.userId , "amc-box")
                let values = getAumChecklist();
                console.log('checkList :', values);
                if (values.length == 0) {
                    var sortByFilter = $('[data-filter]').text().trim().toLowerCase();
                    filterFunds(sortByFilter, amcArray)
                    renderArray = [];
                } else {
                    filterAmcList(sortBy, values);
                }
                adobeAnalytics()
            });
            //sort by filter
            if (window.outerWidth <= 768) {
                $("input[name='sortBy']").click(function () {
                    mobSortBy = []
                    var sortBy = $("input[name='sortBy']:checked").val();
                    mobSortBy.push(sortBy)    
                    console.log(sortBy);
                    amcItemClick(sortBy, userIdObj.userId , "amc-box")
                    if (getAumChecklist().length > 0) {
                        filterFunds(sortBy.toLowerCase(), renderArray);
                    } else {
                        filterFunds(sortBy.toLowerCase(), amcArray);
                    }
                });
            } else {
            document.querySelector('.getSortBy').addEventListener('click', function (event) {
                    var amcTitle = $(this).find('li.active').text().trim();
                    amcItemClick(amcTitle, userIdObj.userId , "amc-box")
                    var sortBy = event.target.innerText.toLowerCase();
                    if ($('.jsDropDownMenuGet').find('.active') == false) {
                        if (getAumChecklist().length > 0) {
                            amcListingRenderObj.renderAmc(renderArray);
                            showCards();
                        } else {
                            amcListingRenderObj.renderAmc(amcArray);
                            showCards();
                        }
                    } else {
                        if (getAumChecklist().length > 0) {
                            filterFunds(sortBy.toLowerCase(), renderArray);
                        } else {
                            filterFunds(sortBy.toLowerCase(), amcArray);
                        }
                    }
            });
        }
        });

        function adobeAnalytics() {
            $('[data-fundmangercard]').click(function (event) {
                event.preventDefault();
                var amcTitle = $(this).find('h5').data('amcfundname');
                amcItemClick(amcTitle, userIdObj.userId , "amc-box")
                location.href = $(this).attr('href')
            })
        }

        function destroyFilterArray() {
            lessThanFiveHundred.length = 0;
            lessThanTwoThousand.length = 0;
            lessThanTenThousand.length = 0;
            greaterThanTenThousand.length = 0;
        }

        function createArray(amcArray) {
            amcArray.forEach(function (element) {
                if (Number(element.aum) >= 0 && Number(element.aum) <= 500) {
                    lessThanFiveHundred.push(element);
                    filterType['0-500'] = lessThanFiveHundred;
                } else if (Number(element.aum) > 500 && Number(element.aum) <= 2000) {
                    lessThanTwoThousand.push(element);
                    filterType['501-2000'] = lessThanTwoThousand;
                } else if (Number(element.aum) > 2000 && Number(element.aum) <= 10000) {
                    lessThanTenThousand.push(element);
                    filterType['2001-10000'] = lessThanTenThousand;
                } else if (Number(element.aum) > 10000) {
                    greaterThanTenThousand.push(element);
                    filterType['10001'] = greaterThanTenThousand;
                }
            });
            console.log('0-500 :', lessThanFiveHundred);
            console.log('500-2000 :', lessThanTwoThousand);
            console.log('2000-10000 :', lessThanTenThousand);
            console.log('above 10000 :', greaterThanTenThousand);
            console.log('Filter Type :', filterType);
        }

        function getAumChecklist() {
            let values = []
            document.querySelectorAll('input[name="sort"]:checked').forEach(function (checkbox) {
                values.push(checkbox.value);
            });
            return values;
        }

        /* sort funds based on sortby value */
        function filterFunds(sortBy, sortArray) {
            document.querySelector('#amcList').innerHTML = '';
            if (sortBy === 'returns - high to low') {
                if (sortArray.length > 0) {
                    var sortedArray = sortArray.sort(function (a, b) {
                        return Number(b.highestReturns) - Number(a.highestReturns);
                    });
                }

               
                amcListingRenderObj.renderAmc(sortArray);
                showCards();
            }
            else if (sortBy === 'returns - low to high') {
                if (sortArray.length > 0) {
                    var sortedArray = sortArray.sort(function (a, b) {
                        return Number(a.highestReturns) - Number(b.highestReturns);
                    });
                }

                amcListingRenderObj.renderAmc(sortArray);
                showCards();
            }
            else if (sortBy === 'aum - high to low') {
                if (sortArray.length > 0) {
                    var sortedArray = sortArray.sort(function (a, b) {
                        return Number(b.aum) - Number(a.aum);
                    });
                }

                amcListingRenderObj.renderAmc(sortArray);
                showCards();
            }
            else if (sortBy === 'aum - low to high') {
                if (sortArray.length > 0) {
                    var sortedArray = sortArray.sort(function (a, b) {
                        return Number(a.aum) - Number(b.aum);
                    });
                }

                amcListingRenderObj.renderAmc(sortArray);
                showCards()
            }
           
            adobeAnalytics();
        }

        function filterAmcList(filterVal, checkList) {
            if (checkList.length >= 1) {
                if (checkList.indexOf(filterVal) != -1) {
                    if (filterType[filterVal] != undefined) {
                        filterType[filterVal].forEach(function (element) {
                            renderArray.push(element);
                        });
                    }
                } else if (checkList.indexOf(filterVal) == -1) {
                    console.log(filterType[filterVal][0]);
                    //renderArray.splice(renderArray.indexOf(filterType[filterVal][0]), filterType[filterVal].length);
                    filterType[filterVal].forEach(ele =>{
                        renderArray.forEach(ele1 => {
                            if(ele.fundName.toLowerCase() == ele1.fundName.toLowerCase()){
                                renderArray.splice(renderArray.indexOf(ele),1);
                            }
                        })
                    })
                }
            } else {
                checkList.forEach(function (type) {
                    if (type.indexOf(filterVal) != -1) {
                        if (filterType[filterVal] != undefined) {
                            filterType[filterVal].forEach(function (element) {
                                renderArray.push(element);
                            });
                           
                        }
                    }
                });

                if (checkList.indexOf(filterVal) == -1) {
                    //renderArray.splice(renderArray.indexOf(filterType[filterVal][0]), filterType[filterVal].length);
                    filterType[filterVal].forEach(ele =>{
                        renderArray.forEach(ele1 => {
                            if(ele.fundName.toLowerCase() == ele1.fundName.toLowerCase()){
                                renderArray.splice(renderArray.indexOf(ele),1);
                            }
                        })
                    })
                }
            }
            console.log('renderArray :', renderArray);
            var sortBy = $('[data-filter]').text().trim().toLowerCase();
            if (sortBy === 'returns - high to low') {
                if (renderArray.length > 0) {
                    var sortedArray = renderArray.sort(function (a, b) {
                        return Number(b.highestReturns) - Number(a.highestReturns);
                    });
                }
               
                amcListingRenderObj.renderAmc(sortedArray);
               
                showCards();
            }
            else if (sortBy === 'returns - low to high') {
                if (renderArray.length > 0) {
                    var sortedArray = renderArray.sort(function (a, b) {
                        return Number(a.highestReturns) - Number(b.highestReturns);
                    });
                }
               
                amcListingRenderObj.renderAmc(sortedArray);
               
                showCards();
            }
            else if (sortBy === 'aum - high to low') {
                if (renderArray.length > 0) {
                    var sortedArray = renderArray.sort(function (a, b) {
                        return Number(b.aum) - Number(a.aum);
                    });
                }
               
                amcListingRenderObj.renderAmc(sortedArray);
             
                showCards();
            }
            else if (sortBy === 'aum - low to high') {
                if (renderArray.length > 0) {
                    var sortedArray = renderArray.sort(function (a, b) {
                        return Number(a.aum) - Number(b.aum);
                    });
                }
               
                amcListingRenderObj.renderAmc(sortedArray);
               
                showCards();
            }
            adobeAnalytics();
        }

        function showCards() {
            /* load-more/less amc listing card js */
            if ($('.jsAmcListing .fund-manager-rows').length < 21) {
                $('.amc-listing-tab-btn').addClass('d-none');
            } else {
                $('.amc-listing-tab-btn').removeClass('d-none');
            }

            if ($('.jsAmcListing .fund-manager-rows').length < 13) {
                $('.jsAmcListing').siblings('.amc-listing-tab-btn').addClass('d-none');
            }

            $('.jsAmcListing .fund-manager-rows').slice(21, amcArray.length).hide();

           
            /* load-more/less amc listing card js */
        }

        function searchAMC() {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var searchamc = amcArray.map(function (amc) {
                return amc.fundName
            });

            
            $('#jsSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
                {
                    name: 'searchamc',
                    source: substringMatcher(searchamc)
                });

            $("[data-search='amc']").on('keyup', function () {
                var searchVal = $(this).val();
                console.log(searchVal)
                if (searchVal.length > 0) {
                    var searchArray = [];
                    amcArray.forEach(function (amc) {
                        if (amc.fundName.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(amc);
                        }
                    });
                    amcListingRenderObj.renderAmc(searchArray);
                    showCards();
                } else {
                    amcListingRenderObj.renderAmc(amcArray);
                    showCards();
                }
            });

            $(".tt-menu").on('click', function () {
                console.log('tt-input', $('.tt-input').val())
                var searchVal = $('.tt-input').val();
                console.log(searchVal)
                if (searchVal.length > 0) {
                    var searchArray = [];
                    amcArray.forEach(function (amc) {
                        if (amc.fundName.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(amc);
                        }
                    });
                    amcListingRenderObj.renderAmc(searchArray);
                    showCards();
                } else {
                    amcListingRenderObj.renderAmc(amcArray);
                    showCards();
                }
                amcItemClick(searchVal, userIdObj.userId, 'amc-box')
            });
            adobeAnalytics()
        }

        function eventInitializer() {

            $("#jsAmcLoadMore").on('click', function (e) {
                e.preventDefault();
                $(".jsAmcListing .fund-manager-rows:hidden").slice(0, 30).fadeIn();
                if ($(".jsAmcListing .fund-manager-rows:hidden").length == 0) {
                    $("#jsAmcLoadLess").removeClass('d-none').fadeIn('slow');
                    $("#jsAmcLoadMore").hide();
                }
            });
            $("#jsAmcLoadLess").on('click', function (e) {
                e.preventDefault();
                if ($(".jsAmcListing .fund-manager-rows").length == 43){
                $('.jsAmcListing .fund-manager-rows:not(:lt(-30))').fadeOut();}
                else{
                    $('.jsAmcListing .fund-manager-rows:not(:lt(-4))').fadeOut();
                }
                $("#jsAmcLoadMore").fadeIn('slow');
                $("#jsAmcLoadLess").hide();
            });

            $('.jsDropDownMenuGet li a').click(function () {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');
                $('[data-filter-card]').slideUp('fast');
                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });

            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });

            // filters js
            // category filter js
            $('[data-category]').click(function () {
                $('[data-category]').removeClass('active');
                $('[data-category-item]').removeClass('active');
                $(this).addClass('active');
                var categoryName = $(this).attr('data-category');
                $('[data-category-item="' + categoryName + '"]').addClass('active');
            });

            // filter tag check js
            $('.js-filterCheck').change(function () {
                var filterVal = $(this).find('input').val();
                amcItemClick(filterVal, userIdObj.userId , 'amc-box')
                if ($(this).find('input').is(':checked')) {
                    $(this).find('input').attr("data-checked", "true");
                } else {
                    $(this).find('input').removeAttr("data-checked");
                }
                console.log(filterVal);
                var filterTagHtml = '<li class="filter-tag" data-tag="' + filterVal + '">' + filterVal + '<a href="javascript:void(0);" class="remove-tag js-removeTag"><span class="icon-close"></span></a></li>'


                if ($(this).find('input').is(':checked')) {
                    setTimeout(function () {
                        $('.filter-tags').append(filterTagHtml);
                    }, 1500);
                } else {
                    // $('[data-tag="'+filterVal+'"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                }

                // mobile add yellow dot to filter title if checkbox checked
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    var checkboxLength = $('[data-filter-item="' + parentCard + '"]').find('input:checked').length;
                    if (checkboxLength > 0) {
                        $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    } else {
                        $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');
                    }

                    if ($(".js-fundCategoryBox").is(":visible")) {
                        var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                        if (fundCategoryCheckboxLength > 0) {
                            $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                        } else {
                            $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                        }
                    }

                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }

            });

            // clear all js
            $('.js-clearAll').click(function () {
                // $('.filter-tag').remove();
                $('.js-FilterSlide').slick('slickRemove', null, null, true);
                $('.js-filterCheck input').prop("checked", false);
                $('.filter-card .js-searchInput').val('');

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    $('[data-filter-link]').removeClass('hasFilter');
                    $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                }

                // initialize loader
                loader();

                // remove filter count
                $('.js-filterCount').removeClass('show').text('');
            });

            // initialize loader
            if ($(window).width() > 991) {
                loader();
            }

            // clear filter card js
            $('.js-clearBtn').click(function () {
                $(this).parents('.filter-card').find('input').prop("checked", false);

                // amc filter search clear and show list js
                $(this).parents('.filter-card').find('.js-searchInput').val('');
                $(this).parents('.filter-card').find('.amc-filter-list li').show();
                $(this).parents('.filter-card').find('.js-noSearchResult').addClass('d-none');

                $(this).parents('.filter-card').find('input').each(function () {
                    var filterVal = $(this).val();
                    $('[data-tag="' + filterVal + '"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                });

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');
                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }
            });

            // filter search js
            $(".js-searchInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".amc-filter-list li").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
                if ($(".amc-filter-list li").is(':visible')) {
                    $('.js-noSearchResult').addClass('d-none');
                } else {
                    $('.js-noSearchResult').removeClass('d-none');
                }
            });

            function checkboxCount() {
                if ($('.js-inlineFilterClearAll input:checked').length >= 1) {
                    $('.js-clearAll').show();
                } else {
                    $('.js-clearAll').hide();
                }
            }


            if ($(".js-inlineFilterClearAll").is(":visible")) {

                $('.js-inlineFilterClearAll').change(function () {
                    // initialize loader
                    if ($(window).width() > 991) {
                        loader();
                    }
                    checkboxCount();
                });

                $('.js-clearBtn').click(function () {
                    checkboxCount();
                });
                $('.js-clearAll').click(function () {
                    $('.js-inlineFilterClearAll input').prop("checked", false);
                    checkboxCount();
                });
            }

            /* -------- mobile filter js start -------- */

            // show filter
            $('.js-showFilter').click(function () {
                $('.js-mobFilterBox').addClass('open');
                $('body').addClass('stopScroll');
            });

            // hide filter
            $('.js-closeFilter').click(function () {
                $('.js-mobFilterBox').removeClass('open');
                $('body').removeClass('stopScroll');
            });

            // show filter respective card
            $('[data-filter-link]').click(function () {
                $('[data-filter-link]').removeClass('active');
                $(this).addClass('active');
                var filterCardItem = $(this).attr('data-filter-link');
                $('[data-filter-item]').removeClass('active');
                $('[data-filter-item="' + filterCardItem + '"]').addClass('active');
            });

            // apply filer
            $('.js-applyFilter').click(function () {

                var values = [];
                var sortBy = mobSortBy[0].toLowerCase();
                document.querySelectorAll('input[name="sort"]:checked').forEach((checkbox) => {
                    if (values.indexOf(checkbox.value) == -1) {
                        values.push(checkbox.value);
                    }
                });
                console.log('checkList :', values);
                if (values.length == 0) {
                    filterFunds(sortBy, amcArray)
                    renderArray = [];
                } else {
                    renderArray = [];
                    values.forEach(function (sortBy) {
                        if (filterType[sortBy] != undefined) {
                            filterType[sortBy].forEach(function (element) {
                                renderArray.push(element)
                            })
                        }
                    });
                   
                    if (sortBy === 'returns - high to low') {
                        if (renderArray.length > 0) {
                            var sortedArray = renderArray.sort(function (a, b) {
                                return Number(b.highestReturns) - Number(a.highestReturns);
                            });
                        }
                        amcListingRenderObj.renderAmc(sortedArray);
                        showCards();
                    }
                    else if (sortBy === 'returns - low to high') {
                        if (renderArray.length > 0) {
                            var sortedArray = renderArray.sort(function (a, b) {
                                return Number(a.highestReturns) - Number(b.highestReturns);
                            });
                        }
                        amcListingRenderObj.renderAmc(sortedArray);
                        showCards()
                    }
                    else if (sortBy === 'aum - high to low') {
                        if (renderArray.length > 0) {
                            var sortedArray = renderArray.sort(function (a, b) {
                                return Number(b.aum) - Number(a.aum);
                            });
                        }
                        amcListingRenderObj.renderAmc(sortedArray);
                        showCards()
                    }
                    else if (sortBy === 'aum - low to high') {
                        if (renderArray.length > 0) {
                            var sortedArray = renderArray.sort(function (a, b) {
                                return Number(a.aum) - Number(b.aum);
                            });
                        }
                        amcListingRenderObj.renderAmc(sortedArray);
                        showCards()
                    }
                   
                }
                adobeAnalytics();
                // initialize loader
                loader();

                // close filter
                setTimeout(function () {
                    $('body').removeClass('stopScroll');
                    $('.js-mobFilterBox').removeClass('open');
                }, 1500);

                // filter count
                var filterCount = $('.js-filterListWrap li .hasFilter').length;
                if (filterCount > 0) {
                    $('.js-filterCount').addClass('show').text(filterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }

                // sebi filter count
                var sebiFilterCount = $('.js-filterListWrap li .hasFilter').length;
                if (sebiFilterCount > 0) {
                    $('.js-filterCount').addClass('show').text(sebiFilterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }
            });

            /* -------- mobile filter js end -------- */
            $('.js-FilterSliderInit').slick({
                autoplay: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });
            $('.js-AmcFilterSlideInit').slick({
                autoplay: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });
            /*jsMobSorting*/

            $('.jsMobSorting').click(function () {
                $('.jsSelectSortList li input').prop('checked', false);
                $(this).parents('body').append('<div class="sorting-backdrop"></div>')
                $(this).parents('body').addClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').addClass('opened-sort')
            })

            $('.jsSelectSortList li input, .jsSortByClose').on('click', function () {
                $(this).parents('body').find('.sorting-backdrop').remove();
                $(this).parents('body').removeClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').removeClass('opened-sort')
            })

            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }

            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

            $(window).on('scroll', function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        }
                        else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();

                    // console.log('header height '+headerHeight);
                    // console.log('ele pos '+ele_position);
                    // console.log('container pos '+conatiner_position);
                    // console.log('win scroll top '+scrollTop);

                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop >= conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' }).prev('.mobFloating').css('height', ele_height);;
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');;
                    }
                }
            });

            var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
            var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();
            function header_fixed() {
                var windowScroll = $(window).scrollTop();
                var topbarHeight = $('.top-bar').outerHeight();

                if (windowScroll > topbarHeight) {
                    $('header').addClass('affix');
                    $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
                } else {
                    $('header').removeClass('affix');
                    $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
                }
            }

        }

        // loader js
        function loader() {
            $('.js-loader').addClass('show');
            setTimeout(function () {
                $('.js-loader').removeClass('show');
            }, 1500);
        }
        // adobe analytics changes 28,38,56-58,86-94,146,179,211,282,331
        return jsHelper.freezeObj(amcListingObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "amcListingBizObj", amcListingBizObj)
})(this || window || {});
/*AMC listing biz.js end*/