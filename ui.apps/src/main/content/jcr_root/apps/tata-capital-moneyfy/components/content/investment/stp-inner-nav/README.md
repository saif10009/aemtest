STP Inner Nav
====
The `STP Inner Nav` component is used for navigation.



## Feature
* It is an authorable component.
* It has Multifield feature which makes authoring more easier. 



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:


1. `./dropdownList` Used as Multifield in component.
2. `./tabTitle` Used to give heading in component.
3. `./checkbox` Used to mark active class in tabs.
4. `./tabLink` Used to render link.
5. `./newTab` Used to redirect link in new tab.


## Client Libraries
The component provides a `moneyfy.stp-inner-nav` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.stp-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-stp.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5