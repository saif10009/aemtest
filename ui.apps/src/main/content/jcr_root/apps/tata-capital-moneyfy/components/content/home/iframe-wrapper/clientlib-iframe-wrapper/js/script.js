(function () {
    document.addEventListener("DOMContentLoaded", function () {
        var container = document.getElementsByClassName('iframe-wrapper');
        var aTags = container[0].querySelectorAll('a[data-sw-iframe="true"]');
        aTags.forEach(function (element) {
            if (element.attributes.href != '#' && element.attributes.href != 'javascript:void(0);' && element.attributes.href != '#void') {
                element.addEventListener('click', function (event) {
                    event.preventDefault();
                    var domain = window.location.origin;
                    var messageData = {
                        name: "tc:moneyfy:" + event.currentTarget.dataset.swIframeComponent,
                        action: "redirect",
                        data: {
                            url: event.currentTarget.dataset.swIframeUrl
                        },
                    };

                    window.parent.postMessage(messageData, domain);
                });    
            }
        });

    });
})();