AMC Fund Managers
====
The `AMC Fund Managers` component is for displaying fund-managers associated to that amc in amc-details page.



## Feature
* It is an dynamic component with data populated using api.


## Edit Dialog Properties
There are no properties stored in the edit dialog of the component.

## Client Libraries
The component provides a `moneyfy.amc-fund-managers` editor client library category that includes JavaScript and CSS.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5