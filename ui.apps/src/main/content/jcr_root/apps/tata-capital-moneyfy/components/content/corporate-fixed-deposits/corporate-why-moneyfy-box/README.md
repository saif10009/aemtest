corporate Why Moneyfy
====
This is a authorable why moneyfy component can be used to author the various reasons for using moneyfy.

## Feature
* This is a multifield component.
* Used to provide reason on page to chose moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./corporateHeading` Used for rendering heading of the component.
2. `./whyMoneyfyList` Used for rendering mutltified in component.
3. `./corporateImg` Used to select image path for this component.
4. `./corporateDescription` Used to render description of a card.

## Client Libraries
The component provides a `moneyfy.corporate-why-moneyfy` editor client library category that includes JavaScript and CSS
handling for dialog interaction.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5