Compare Page Title
====
The `Compare Page Title` component is for adding page heading with additional links on the compare page.



## Feature
* It is an static component.


## Edit Dialog Properties
There are no properties stored in the edit dialog.


## Client Libraries
The component provides a `moneyfy.compare-page-head` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5