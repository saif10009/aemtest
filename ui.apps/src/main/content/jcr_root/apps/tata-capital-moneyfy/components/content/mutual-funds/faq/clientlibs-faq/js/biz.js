/* FAQ JS start */
(function (_global) {
    var faqBizObj = (function (jsHelper) {
        var faqObj = {}
        document.addEventListener('DOMContentLoaded', function () {

            faqClick();
            npsViewAllBtnRemove();
        })
        function faqClick() {
            /************ accordian Js *************/
            $('[data-toggle="collapses"]').click(function(ele){
                var ele_parents = $(ele.target).parents('.accordian').attr('id');

                $('#' + ele_parents).find('.collapse').slideUp();
                $('#' + ele_parents).find('.accordian-card').removeClass('active');
            
                if($(ele.target).parent().siblings().css( 'display') == "block"){
                    $(ele.target).parent().siblings().slideUp();
                    $(ele.target).parents('.accordian-card').removeClass('active');
                } else {
                    $(ele.target).parent().siblings().slideDown();
                    $(ele.target).parents('.accordian-card').addClass('active');
                }
              });    
            //adobe analytics
            $('[data-expand]').click(function () {
               var faqTitle = $(this).text().trim();
                faqExpend(faqTitle,userIdObj.userId)
             })
            $('[data-faqviewall]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parents('.heading-with-view').find('h2').text().trim();
                allCTAInteraction(ctaText,ctaTitle,'faq',userIdObj.userId);
                location.href = $(this).attr('href')
            })
        }
        function npsViewAllBtnRemove(){
            if(window.location.href.includes('pageType=nps')){
                $('.heading-with-view .btn-view-all').remove()
            }
        }
        return jsHelper.freezeObj(faqObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, "faqBizObj", faqBizObj);
})(this || window || {});
/* FAQ JS end */
        