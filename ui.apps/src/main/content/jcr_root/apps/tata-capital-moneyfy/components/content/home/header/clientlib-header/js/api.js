(function (_global) {
    var headerApiFn = (function (jsHelper) {
        var headerApiObj = {};

        var headerSearch = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getSearchApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }

        var cartCountFn = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.cartCountApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        headerApiObj.headerSearch = headerSearch;
        headerApiObj.cartCountFn = cartCountFn;
        return jsHelper.freezeObj(headerApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'headerApiObj', headerApiFn);
})(this || window || {});