Why Choose STP 
====
The `Why Choose STP` component provides systematic plan to transfer your mutual fund from on to other. 



## Feature
* It is a Multifield component.    


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used to render heading.
2. `./description` Used for description.
3. `./multi` Used to create Multifield.
4. `./image` used to render image.
5. `./imageDescription` used to description about image.


## Client Libraries
The component provides a `moneyfy.why-choose-stp` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.stp-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-stp.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5