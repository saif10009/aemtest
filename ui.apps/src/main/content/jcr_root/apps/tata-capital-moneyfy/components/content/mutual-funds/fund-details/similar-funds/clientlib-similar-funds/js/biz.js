/* similar mutual funds js */
(function (_global) {
  var similarFundsBizObj = (function (jsHelper) {
    var similarFundsObj = {};

    document.addEventListener("DOMContentLoaded", function () {
      getUserWatchList(showWatchListAddedIcons);
      initializeViewMoreLessEvents();
      addEllipseToFundName();
      investNow();
      initializeWatchListEvents();
      showReturns();
      adobeAnalytics();
      $('.js-viewMoreToggleBtn').click(function (e) {
        e.preventDefault();
        e.stopPropagation();
        $(this).toggleClass('active');
        $(this).find('.text-wrap').text(function (i, text) {
          return text === "More" ? "Less" : "More";
        });
        var toggleDiv = $(this).attr('data-btn');
        $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
      });

      if ($(window).width() < 768) {
        $('.js-similarMFcard').click(function(e){
          $(this).find('.tag-more-wrap .js-viewMoreToggleBtn[data-btn]').trigger('click');
        });
        $(".js-similarMFcard .form-select2, .js-similarMFcard .watchlist-compare-btn, .js-similarMFcard .invest-detail-wrap").click(function (e) {
          e.stopPropagation();
        });
      }
    });

    function initializeViewMoreLessEvents() {
      /*similar mutual fund strip*/
      if ($('.js-similar-fund-list').find('.js-similar-list').length < 4) {
        $('.js-similar-fund-list').siblings('.similar-mutual-btn').addClass('d-none');
      }
      $('.js-similar-fund-list .js-similar-list').slice(0, 4).show();

      $("#JsSimilarViewMore").on('click', function (e) {
        e.preventDefault();
        $(".js-similar-fund-list .js-similar-list:hidden").slice(0, 4).fadeIn();
        if ($(".js-similar-fund-list .js-similar-list:hidden").length == 0) {
          $("#JsSimilarViewLess").removeClass('d-none').fadeIn('slow');
          $("#JsSimilarViewMore").hide();
        }
      });

      $("#JsSimilarViewLess").on('click', function (e) {
        e.preventDefault();
        $('.js-similar-fund-list .js-similar-list:not(:lt(4))').fadeOut();
        $("#JsSimilarViewMore").fadeIn('slow');
        $("#JsSimilarViewLess").hide();
      });
    };

    function addEllipseToFundName() {
      /*2 line Dot in mutual fund strip*/
      if ($(window).width() > 767) {
        var showChar = 45;
      }
      else if ($(window).width() > 374) {
        var showChar = 32;
      }
      else {
        var showChar = 20;
      }

      $('.similar-mutual-funds-items .name-rating-wrap h6').each(function () {
        var content = $(this).html();
        if (content.length > showChar) {
          var showLine = content.substr(0, showChar);
          var remainContent = content.substr(showChar, content.length - showChar);
          var allContent = showLine + '<span class="remaining-content d-none">' + remainContent + '</span> <span>...</span>';
          $(this).html(allContent);
        }
      });
    }

    function adobeAnalytics() {
      $('#similarfundbtn').click(function (event) {//similarFund view all
        event.preventDefault();
        var ctaText = $(this).text().trim();
        var ctaTitle = $(this).parents('.similar-mutual-funds').find('h2').text().trim();
        allCTAInteraction(ctaText, ctaTitle, 'similar-funds', userIdObj.userId)
        location.href = $(this).attr('href')
      })
      $('[data-viewdetailbtn]').click(function (event) {
        event.preventDefault();
        var fundName = $(this).parents('.js-similar-list').find('h6').data('schemename');
        var fundRiskCat = $(this).parents('.js-similar-list').find('[data-risktype]').data('risktype');
        var fundType = $(this).parents('.js-similar-list').find('[data-catname]').data('catname');
        fundViewDetails(fundType, fundName, fundRiskCat, userIdObj.userId)
        window.open($(this).attr('href'))
      })
    }

    function initializeWatchListEvents() {
      // add to watchList
      $('[data-addwatchlist]').click(function () {
        var schemeId = $(this).parents('.js-similar-list').data('schemeid');
        var schemeName = $(this).parents('.js-similar-list').find('h6').data('schemename');
        var fundRiskCat = $(this).parents('.js-similar-list').find('[data-risktype]').data('risktype');
        var fundType = $(this).parents('.js-similar-list').find('[data-catname]').data('catname');
        addToWishList(fundType, schemeName, fundRiskCat, userIdObj.userId)
        if (!$(this).hasClass('addedWatchlist')) {
          addWatchListBizFn(schemeId, schemeName);
        } else {
          showWatchListExistPopup();
        }
      });

      // fundReview add to watchList
      $('[data-fundReviewAddToWatchList="true"]').click(function () {
        var schemeId = $('.quick-review-wrap').data('schemeid');
        var schemeName = $('.quick-review-wrap').data('schemename');
        var fundRiskCat = $(".quick-review-wrap").find('[data-detailrisktype]').data("detailrisktype");
        var fundType = $(".quick-review-wrap").find('[data-detailcatname]').data('detailcatname');
        addToWishList(fundType, schemeName, fundRiskCat, userIdObj.userId);
        if (!document.querySelector('[data-schemeid] .js-addWatchlisttop').classList.contains('active-watchlist-added')) {
          addWatchListBizFn(schemeId, schemeName, 'fundDetailCard');
        } else {
          showWatchListExistPopup();
        }
      });
    }

    function getUserWatchList(callShowWatchListAddedIcons) {
      if (headerBizObj.getUserObj().isLoggedIn) {
        var userData = headerBizObj.getUserObj().userData;
        var requestObj = {
          "body": {
            "mobileNumber": userData.mobileNumber,
            "tatId": userData.tatId,
            "tenure": "3y"
          }
        }
        similarFundApiObj.fetchwatchListApi(requestObj).then(function (response) {
          var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
          if (responseData.responseJson.header.status.toLowerCase() == 'success') {
            var watchListArray = responseData.responseJson.body.watchListDetails;
            watchListArray.forEach(function (fund) {
              watchListSchemeIds.push(fund.schemeId);
            });
            if (jsHelper.isDef(callShowWatchListAddedIcons)) {
              callShowWatchListAddedIcons();
            }
          }
        }).catch(function (error) {
          console.log(error);
        });
      }
    }

    function showWatchListAddedIcons() {
      document.querySelectorAll('[data-schemeid]').forEach(function (schemeId) {
        if (watchListSchemeIds.indexOf(Number(schemeId.dataset.schemeid)) != -1) {
          if (document.getElementById('similarMutualFunds').querySelector('[data-schemeid="' + schemeId.dataset.schemeid + '"]') != null) {
            document.getElementById('similarMutualFunds').querySelector('[data-schemeid="' + schemeId.dataset.schemeid + '"]').querySelectorAll('.js-addWatchlist').forEach(function (element) {
              element.classList.add('addedWatchlist');
              $('[data-schemeid="' + schemeId.dataset.schemeid + '"] .wishlist-text').html('Added To Watchlist');
            })  
          }
        }

        if (watchListSchemeIds.indexOf(Number(document.querySelector('[data-schemeid]').dataset.schemeid)) != -1) {
          document.querySelector('[data-schemeid] .js-addWatchlisttop').classList.add('active-watchlist-added');
        }
      })
    };

    function addWatchListBizFn(schemeId, schemeName, watchListType) {
      if (!headerBizObj.getUserObj().isLoggedIn) {
        showLoginPopup();
        document.querySelector('[data-login="true"]').addEventListener('click', function () {
          location.href = appConfig.jocataDomain + '?action=watchlist&schemeId=' + schemeId + '&source=' + window.location.href
        });
      } else {
        var userData = headerBizObj.getUserObj().userData;
        var requestObj = {
          "body": {
            "mobileNumber": userData.mobileNumber,
            "tatId": userData.tatId,
            "product": "moneyfy",
            "schemeId": schemeId,
          }
        };
        similarFundApiObj.addTowatchListApi(requestObj).then(function (response) {
          var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
          if (responseData.responseJson.header.status.toLowerCase() == 'success') {
            if (responseData.responseJson.body.status.statusMessage == 'Fund added to Watchlist') {
              if ($('.watchListError').hasClass('d-none')) {
                $('.watchListError').removeClass('d-none');
              }
              //watchListSchemeIds.push(schemeId);
              document.getElementById('addWatchlistMsg').innerHTML = 'Fund added to Watchlist';
              showAddWatchListPopup();
              if (watchListType == 'fundDetailCard') {
                document.querySelector('[data-schemeid] .js-addWatchlisttop').classList.add('active-watchlist-added');
              }else{
                document.querySelector('[data-schemeid="' + schemeId + '"]').querySelectorAll('.js-addWatchlist').forEach(function (element) {
                  element.classList.add('addedWatchlist');
                  $('[data-schemeid="' + schemeId + '"] .wishlist-text').html('Added To Watchlist');
                });  
              }
              
            }
          } else {
            showWatchListErrorPopup();
          }
        }).catch(function (error) {
          console.log(error);
          showWatchListErrorPopup();
        });
      }
    }

    function investNow() {
      /* Invest Now click button */
      $('[data-investnow="true"]').click(function () {
        var schemaId = $(this).parents('.js-similar-list').data('schemeid');
        var source = window.location.href;
        var fundName = $(this).parents('.js-similar-list').find('h6').data('schemename');
        var fundRiskCat = $(this).parents('.js-similar-list').find('[data-risktype]').data('risktype');
        var fundType = $(this).parents('.js-similar-list').find('[data-catname]').data('catname');
        fundInvestNow(fundType, fundName, fundRiskCat, userIdObj.userId)
        console.log('Invest Now redirect link :', appConfig.jocataDomain + '?action=invest' + '&schemaId=' + schemaId + '&source=' + source)
        if (!headerBizObj.getUserObj().isLoggedIn) {
          showLoginPopup();
          document.querySelector('[data-login="true"]').addEventListener('click', function () {
            location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
          });
        } else {
          location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
        }
      });
    }

    /* Mobile View show returns value based on dropdown */
    function showReturns() {
      $('.similarFundReturnsVal').on('change', function (e) {
        console.log(e.target.parentElement.parentElement.lastChild)
        var value = $(this).val();
        if (Number.isNaN(value) == false && Number(value) > 0) {
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info up-trend');
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span>' + value + '%');
        } else if (Number.isNaN(Number(value))) {
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass();
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('- %');
        } else if (Number.isNaN(value) == false && Number(value) <= 0) {
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info down-trend');
          $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span>' + value + '%');
        }
      });
    }

    return jsHelper.freezeObj(similarFundsObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "similarFundsBizObj", similarFundsBizObj);
})(this || window || {});

/* similar mutual funds js */

document.querySelectorAll('#similarMutualFunds .rating-wrap .rate').forEach(function(rating){
  if(Number(rating.innerText.trim()) == 0 || rating.innerText.trim() == '-' ) {
      rating.parentElement.remove();
  }
});

document.querySelectorAll('.quick-review-wrap .rating-wrap .rate').forEach(function(rating){
  if(Number(rating.innerText.trim()) == 0 || rating.innerText.trim() == '-' ) {
      rating.parentElement.remove();
  }
});