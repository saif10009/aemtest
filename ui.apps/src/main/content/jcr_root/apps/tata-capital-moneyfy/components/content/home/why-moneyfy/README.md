Why Moneyfy
====
This is a authorable why moneyfy component can be used to author the various reasons for using moneyfy.

## Feature
* This is a multifield component.
* Used to provide reason on page to chose moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering title of the component.
2. `./sub-title` Used for rendering heading of the component.
3. `./image-path` Used to select image path for this component.
4. `./description` Used to render description of a card.

## Client Libraries
The component provides a `moneyfy.whyMoneyfy` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is already included in component HTML file.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5