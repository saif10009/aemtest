/*leadership slider js start*/
(function (_global) {
    var ladershipSliderBizObj = (function (jsHelper) {
        var ladershipSliderObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            ladershipSliderSlick();
        })
        function ladershipSliderSlick() {
            $('#leadershipSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                  {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    }
                  },
                  {
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    }
                  },
                  {
                    breakpoint: 768,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      centerMode: true,
                      centerPadding: '30px',
                    }
                  },
                  {
                    breakpoint: 376,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      centerMode: true,
                      centerPadding: '20px',
                    }
                  }
                ]
              });
        }
        return jsHelper.freezeObj(ladershipSliderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "ladershipSliderBizObj", ladershipSliderBizObj)
})(this || window || {});
  /*leadership slider js end*/
