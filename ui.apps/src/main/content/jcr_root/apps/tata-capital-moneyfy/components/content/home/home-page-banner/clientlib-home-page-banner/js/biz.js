/*banner slider js*/
(function (_global) {

    var homePageBannerBizObj = (function (jsHelper) {
        var homePageBannerObj = {};
        
        document.addEventListener('DOMContentLoaded', function   () {
            if ($('.banner-image').length > 1) {
                initializeHomeBanner();
            }
            adobeAnalytics();
        });

        function adobeAnalytics() {	
            $('[data-leftcon="leftContent"] a').click(function (event) {	
                event.preventDefault();	
                var clickButtonTitle = $(this).text().trim();	
                var clickTextTitle = $(this).parents('.banner-left').find('h2').text().trim();
                bannerInteraction(clickTextTitle, clickButtonTitle, 'home-page-banner',userIdObj.userId)	;
                location.href = $(this).attr('href')
            });	
            $('[data-leftcon="popupButton"] a').click(function (event) {	
                event.preventDefault();	
                var clickButtonTitle = $(this).text().trim();	
                var clickTextTitle = $(this).parents('.banner-left').find('h2').text().trim();	
                allCTAInteraction(clickTextTitle, clickButtonTitle, 'home-page-banner',userIdObj.userId);
            });	
            $('[data-imgouter] a').click(function (event) {	
                event.preventDefault();	
                bannerInteraction('Banner image', 'Simple Image', 'home-page-banner',userIdObj.userId);
                location.href = $(this).attr('href')
            });	
        }


        function initializeHomeBanner() {
            /*banner slider js*/
            $('#jsBannerSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                autoplay: false,
              });

              setTimeout(function(){
                if($('.banner-slider').hasClass('slick-initialized')){
                    $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                }
              }, 2000);
            /*banner slider js*/
        }

        return jsHelper.freezeObj(homePageBannerObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'homePageBannerBizObj', homePageBannerBizObj);
})(this || window || {});
/*banner slider js*/