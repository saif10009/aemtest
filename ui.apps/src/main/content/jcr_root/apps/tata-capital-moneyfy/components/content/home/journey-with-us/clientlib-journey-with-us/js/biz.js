 /* Begin a Successful Financial Journey with Us tab js start*/

 (function(_global) {
     var journeyBizObj = (function(jsHelper) {
         var journeyObj = {}
         document.addEventListener('DOMContentLoaded', function() {
             journeyWithUsSlick();
             adobeAnalytics();
         })

         function adobeAnalytics() {	
            $('[data-listinfo] a').click(function (event) {	
                event.preventDefault();	
                var clickButtonTitle = $(this).find('h5').text().trim();	
                var clickTextTitle = $('.financial-text').find('h2').text().trim();	
                allCTAInteraction(clickButtonTitle, clickTextTitle, 'journey-with-us',userIdObj.userId)	
                location.href = $(this).attr('href')	
            });	
        }

         function journeyWithUsSlick() {
             $('#jsFinancialJourneySlider').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {

                 if ($('.list-financial-info .financial-info-li').length <= 4) {
                     if ($(window).width() > 1199) {
                         $('.list-financial-info').removeClass('slider-dots');
                     }
                 }
             });
             if ($(window).width() < 992) {
                 if ($('#jsFinancialJourneySlider').hasClass('slick-initialized')) {
                     $('#jsFinancialJourneySlider').slick('unslick');
                 }
             } else {
                 var numberOfCards = $('#jsFinancialJourneySlider .financial-info-li').length;
                 if (numberOfCards >= 4) {
                    $('#jsFinancialJourneySlider').not('.slick-initialized').slick({
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 4,
                        slidesToScroll: 2,
                        arrows: true,
                        responsive: [{
                                breakpoint: 1200,
                                settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 1,
                                }
                            },
                            {
                                breakpoint: 991,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                }
                            },
                            {
                                breakpoint: 768,
                                settings: {
                                    slidesToShow: 2,
                                    slidesToScroll: 1,
                                }
                            },
                            {
                                breakpoint: 360,
                                settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                }
                            }
                        ]
                    });    
                 } else {
                    $('#jsFinancialJourneySlider').not('.slick-initialized').slick({
                        dots: true,
                        infinite: false,
                        speed: 300,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        arrows: true,
                        responsive: [
                          {
                            breakpoint: 991,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1,
                            }
                          },
                          {
                            breakpoint: 768,
                            settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1,
                            }
                          },
                          {
                            breakpoint: 360,
                            settings: {
                              slidesToShow: 1,
                              slidesToScroll: 1,
                            }
                          }
                        ]
                    });   
                 }    
             }
         }
         return jsHelper.freezeObj(journeyObj);
     })(jsHelper);
     _global.jsHelper.defineReadOnlyObjProp(_global, "journeyBizObj", journeyBizObj);
 })(this || window || {});
 /* Begin a Successful Financial Journey with Us tab js end*/