/*Fund Manager List render.js start*/
(function(_global) {
    var fundManagerListRenderFnObj = (function(jsHelper) {
        var fundManagerListRenderObj = {}

        var renderManagerList = function (fundManagerArray) {
            document.querySelector('.inner-fund-manager').innerHTML = '';
            if (fundManagerArray.length > 0) {
                var renderHtmlStr = "";
                $('.js-loader').addClass('show');
                fundManagerArray.forEach(function (manager) {   
                    renderHtmlStr += 
                    '<div class="fund-manager-rows fund-list-li" data-amc-id ="'+ manager.amcId +'">'+
                    '                  <div class="fund-manager-col">'+
                    '                    <a href="'+ manager.pagePath +'" class="card-div fund-manager-card" data-noOfFunds="'+(manager.noOfFunds ? manager.noOfFunds : 'N/A')+'"  data-fundSize="'+Math.trunc(manager.totalFundSize).toLocaleString('en-IN')+'Cr" data-highReturns="'+(manager['highestReturns'] ? manager['highestReturns']+'%' : 'N/A')+'" data-fundManagerCard="fundmangercard">'+
                    '                      <div class="fund-manager-top">'+
                    '                        <h5>'+(manager.fundManagerName ? manager.fundManagerName : '-')+'</h5>'+
                    '                      </div>'+
                    '                      <div class="fund-manager-bottom">'+
                    '                        <div class="fund-ul">'+
                    '                          <div class="fund-li">'+
                    '                            <p>No. of funds</p>'+
                    '                            <h6>'+(manager.noOfFunds ? manager.noOfFunds : '-')+'</h6>'+
                    '                          </div>'+
                    '                          <div class="fund-li">'+
                    '                            <p>Total Fund Size</p>'+
                    '                            <h6>₹'+Math.trunc(manager.totalFundSize).toLocaleString('en-IN')+'Cr</h6>'+
                    '                          </div>'+
                    '                          <div class="fund-li">'+
                    '                            <p>Highest Returns</p>'+
                    '                            <h6>'+(manager['highestReturns'] ? manager['highestReturns']+'%' : '-')+'</h6>'+
                    '                          </div>'+
                    '                        </div>'+
                    '                        <div class="text-center d-block d-md-none">'+
                    '                          <button type="button" class="btn-blue radius100 mg-t15">View Details</button>'+
                    '                        </div>'+
                    '                      </div>'+
                    '                    </a>'+
                    '                  </div>'+
                    '                </div>';    
                });
                setTimeout(function () {
                    $('.js-loader').removeClass('show');
                }, 1700);
                $('.fund-list-result-right').removeClass('d-none');
                document.querySelector('.inner-fund-manager').innerHTML = renderHtmlStr;
            } else {
                $('.fund-list-result-right').addClass('d-none');
                document.querySelector('.inner-fund-manager').innerHTML = "No managers found in this category."
            }          
        }

        fundManagerListRenderObj.renderManagerList = renderManagerList;
        return jsHelper.freezeObj(fundManagerListRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundManagerListRenderObj", fundManagerListRenderFnObj);
})(this || window || {});
/*fundManagerList render.js end*/




	
