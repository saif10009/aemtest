/*subsidiary company slider js start*/
(function (_global) {
    var subsidiarySliderBizObj = (function (jsHelper) {
        var subsidiarySliderObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            subsidiarySliderSlick();
        })
        function subsidiarySliderSlick() {
            $('#subsidiaryCompanySlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '30px',
                        }
                    },
                    {
                        breakpoint: 376,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            centerMode: true,
                            centerPadding: '20px',
                        }
                    }
                ]
            });
        }
        return jsHelper.freezeObj(subsidiarySliderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "subsidiarySliderBizObj", subsidiarySliderBizObj)
})(this || window || {});
  /*subsidiary company slider js end*/
