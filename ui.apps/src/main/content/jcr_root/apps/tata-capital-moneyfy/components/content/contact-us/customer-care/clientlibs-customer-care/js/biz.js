/*customer care js start*/
(function (_global) {

    var customerCareBizFn = (function (jsHelper) {
        var customerCareBizObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            clickEvents();
            keyupEvents();
        });
        function clickEvents() {

            $('[data-dismiss="popover-modal"]').on('click', function () {
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();
            });

            $('.js-getapp-form button').click(function (e) {
                var ele_input = $(this).parents('.js-getapp-form').find('input');
                var errors = [];
                var ele_required = "Field is required";

                $(ele_input).each(function () {
                    var element = $(this);
                    var ele_phoneNumber = "Please enter valid number";

                    $(element).next('.error-msgs').remove();
                    $(element).after('<span class="error-msgs"></span>');

                    if (element.is(":visible")) {
                        if ($(element).val() != '') {
                            if ($(element).data('type') === 'mobile') {
                                if (!validateMobile(element)) {
                                    $(element).next('.error-msgs').text(ele_phoneNumber);
                                    errors.push(ele_phoneNumber);
                                } else {
                                    $(element).next('.error-msgs').remove();
                                }
                            }

                        } else {
                            $(element).next('.error-msgs').text(ele_required);
                            errors.push(ele_required);
                        }
                    }
                });

                if (errors.length == 0) {
                    e.preventDefault();

                    var getMobileNumber = $('.js-getapp-form input').val();
                    var lastTwoDigit = getMobileNumber.toString().slice(-2);
                    $('.jsShowMobileNumber').text(lastTwoDigit);
                    var requestData = {
                        "body": {
                            "mobile": getMobileNumber
                        }
                    }
                    customerCareApiObj.customerCare(requestData).then(function (response) {
                        setTimeout(function () {
                            $('#getAppLinkViaSms').addClass('popover-show');
                            $("#getAppLinkViaSms").css('display', 'block');
                            $('body').addClass('popover-modal-open');
                            $('body').append('<div class="modal-backdrop"></div>');
                            $('#getAppLinkViaSms').find('.fund-api-error').css('display', 'none');
                            $('#getAppLinkViaSms').find('.fund-api-success').css('display', '')
                        }, 80);
                    }).catch(function (error) {
                        console.log("Customer Care APi Failed");
                    });

                }
            });

            $('#getAppLinkViaSms .popover-modal-close').click(function () {
                $('.js-getapp-form input').val('');
            })

            $('.jsgetAppLinkViaSms').click(function () {
                $('#getAppLinkViaSms').removeClass('popover-show');
                $("#getAppLinkViaSms").removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();
            })

            $('.jsEditGetAppModal').click(function () {
                $(this).parents('.js-getapp-form-modal').find('.error-msgs').remove();
                $(this).parents('.js-getapp-form-modal').find('#moneyfy01').removeClass('hidden');
                $(this).parents('.js-getapp-form-modal').find('#moneyfy02').addClass('hidden');
                $(this).parents('.js-getapp-form-modal').find('.get-app-loader').addClass('hidden');
            })
        }

        function keyupEvents() {

            $('.only-numeric-input').keyup(function(e) {
                $(this).val($(this).val().replace(/[^\w\s]/gi, ''));
            });

            $('.js-getapp-form-modal input').keyup(function () {
                var element = $(this);
                var ele_required = "Field is required";
                var ele_phoneNumber = "Please enter valid number";

                $(element).next('.error-msgs').remove();
                $(element).after('<span class="error-msgs"></span>');

                if (element.is(":visible")) {
                    if ($(element).val() != '') {
                        if ($(element).data('type') === 'mobile') {
                            if (!validateMobile(element)) {
                                $(element).next('.error-msgs').text(ele_phoneNumber);
                            } else {
                                $(element).next('.error-msgs').remove();
                            }
                        }

                    } else {
                        $(element).next('.error-msgs').text(ele_required);
                    }
                }
            });
            $('.js-getapp-form input').keyup(function() {
                var element = $(this);
                var ele_required = "Field is required";
                var ele_phoneNumber = "Please enter valid number";

                $(element).next('.error-msgs').remove();
                $(element).after('<span class="error-msgs"></span>');

                if (element.is(":visible")) {
                    if ($(element).val() != '') {
                        if ($(element).data('type') === 'mobile') {
                            if (!validateMobile(element)) {
                                $(element).next('.error-msgs').text(ele_phoneNumber);
                            } else {
                                $(element).next('.error-msgs').remove();
                            }
                        }

                    } else {
                        $(element).next('.error-msgs').text(ele_required);
                    }
                }
            });
        }
        function validateMobile(mobileField) {
            var re = /^[4-9][0-9]{9}$/;
            var check = re.test($(mobileField).val());
            if ($(mobileField).val().length != 10 || !check) {
                return false;
            } else {
                return true;
            }
        }
        return jsHelper.freezeObj(customerCareBizObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'customerCareBizObj', customerCareBizFn);
})(this || window || {});

/*customer care js end*/