/*fund listing filter js start*/
(function(_global) {
    var fundFilterListRenderFn = (function(jsHelper) {
        var fundFilterListRenderObj = {};

        function renderCategoryList(sebiCategoryList) {
            for (var listType in sebiCategoryList) {
                var htmlRenderStr = "";
                sebiCategoryList[listType].forEach((cat, index) => {
                    htmlRenderStr  += '<li class="filter-checkbox">' +
                    '<div class="custom-checkbox">'+
                    '    <label class="js-filterCheck">'+
                    '        <input type="checkbox" name="subCategory" value="'+ cat.subCatName +'">'+
                    '        <span class="checkbox-wrap"></span>'+ cat.subCatName +'</label>'+
                    '</div>'+
                    '</li>';
                });
                document.querySelectorAll('[data-list=' + listType.toLowerCase() + ']').forEach(function (element) {
                    element.innerHTML = htmlRenderStr; 
                });
            }
        }
        fundFilterListRenderObj.renderCategoryList = renderCategoryList;
        return jsHelper.freezeObj(fundFilterListRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundFilterListRenderObj", fundFilterListRenderFn)
})(this || window || {});
/*fund listing filter js end*/