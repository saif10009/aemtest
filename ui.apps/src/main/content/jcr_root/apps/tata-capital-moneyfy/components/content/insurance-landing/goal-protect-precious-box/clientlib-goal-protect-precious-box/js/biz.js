/*goal protect box js start*/
(function (_global) {
    var goalProtectBoxBizObj = (function (jsHelper) {
        var goalProtectBoxObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            $('[data-adobeAnalytics="goalprotectbox"]').click(function (event) {
                event.preventDefault();
                var insuranceName = $(this).data('goal-list-name');
                insuranceItemClick(insuranceName,userIdObj.userId,'goal-protect-precious-box');
                location.href = $(this).attr('href')
            });
        })

        return jsHelper.freezeObj(goalProtectBoxObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "goalProtectBoxBizObj", goalProtectBoxBizObj)
})(this || window || {});
  /*goal protect box js end*/
    