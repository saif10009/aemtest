    /*Check credit js*/
(function (_global) {

    var checkCreditBizObj = (function (jsHelper) {
        var checkCreditObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            checkCreditClick();
        });
        function checkCreditClick() {
            $('[data-checkleft] a').click(function (event) {
                event.preventDefault();
                var anchorLink = $(this).attr('href');
                var clickButtonTitle = $(this).text().trim();
                var clickTextTitle = $(this).parents('.checkcredit-left').find('h2').text().trim();
                bannerInteraction(clickTextTitle, clickButtonTitle, 'check-credit-box',userIdObj.userId)
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + anchorLink;
                    });
                } else {
                    location.href = appConfig.jocataDomain + anchorLink;
                }

            });
        }

        return jsHelper.freezeObj(checkCreditObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'checkCreditBizObj', checkCreditBizObj);
})(this || window || {});
/*Check credit js*/