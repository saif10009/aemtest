/*corporate fd steps js start*/
(function (_global) {
    var corporateFdStepsBizObj = (function (jsHelper) {
        var corporateFdStepsObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            corporateFdStepsClickFn();
        })
        function corporateFdStepsClickFn(){
            $('[data-fd-btn="redirectfdbtn"]').click(function (event) {
                event.preventDefault();
                var anchorLink = $(this).attr('href');
                var ctaTitle = $(this).parent().parent().find('h2').text().trim();
                var ctaText = $(this).text().trim();
                allCTAInteraction(ctaText,ctaTitle,'corporate-fd-steps',userIdObj.userId);
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + anchorLink;
                    });
                } else {
                    location.href = appConfig.jocataDomain + anchorLink;
                }
            })
        }
        return jsHelper.freezeObj(corporateFdStepsObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "corporateFdStepsBizObj", corporateFdStepsBizObj)
})(this || window || {});
  /*corporate fd steps js end*/