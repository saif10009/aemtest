Fund Managers Fund List
====
The `Fund Managers fund List` component is for showing the list of types of funds on the fund-managers-detail page.



## Feature
* It is an dynamic component with data populated using JS.



## Edit Dialog Properties
The component has no properties stored in edit dialog.


## Client Libraries
The component provides a `moneyfy.fund-managers-fund-list` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5