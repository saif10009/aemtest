$('.jsTooltopLink').click(function(){
  $(this).siblings('.tooltip-box').fadeIn();    
  setTimeout (function(){
    $('.tooltip-box').fadeOut();
  }, 2000);
}) 

document.querySelector('#video-modal button').addEventListener('click', function () {
  var iframe = document.getElementById("videoId");
  $(iframe).attr('src', "");
  $('.modal-backdrop-vid-login').remove();
});

document.querySelector('#video-modal [data-target="#share-modal"]').addEventListener('click', function(){
  var iframe = document.getElementById("videoId");
  $(iframe).attr('src', "");
});

if(sessionStorage.getItem('learnModalCount') == null){
  var learnKeySet = sessionStorage.setItem('learnModalCount', 0);
}
var learnModalCount = Number(sessionStorage.getItem('learnModalCount'));
if(learnModalCount == 0 && learnKeySet!== null){
  document.addEventListener('DOMContentLoaded', function(){
    setTimeout(function(){
    var learnModal = document.getElementById('exporenow');
    if(isNotMobileApp && learnModalCount == 0) {
      learnModal.style.display = 'block';
      $('body').append('<div class="modal-backdrop"></div>');
      sessionStorage.setItem('learnModalCount', 1);
      document.querySelector('body').classList.add('popover-modal-open');
    }
    if (mfLoginTimeoutId) clearTimeout(mfLoginTimeoutId);
    learnModal.querySelector('.popover-modal-close').addEventListener('click', function(){
      document.querySelector('body').classList.remove('popover-modal-open');
      setTimeout(function(){
        if ((Number(sessionStorage.getItem("loginPopupCount")) == 0) && (isNotMobileApp) && !getUserObj().isLoggedIn) {
          showLoginPopup();
          document.querySelector('[data-login="true"]').addEventListener('click', function () {
              location.href = appConfig.jocataDomain;
          });
      }
      sessionStorage.setItem("loginPopupCount", 1);
      }, 3000)
    });
  }, 550);
});
}