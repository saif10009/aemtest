/*compare fund render.js start*/
(function(_global) {
    var comparefundRenderFn = (function(jsHelper) {
        var comparefundRenderObj = {}

        var renderFund = function(fundArray,schemeIDvalues) {
            fundArray.forEach(function (value) {
                var index = schemeIDvalues.indexOf(value.schemeDetails.id);
                var explorePath = document.querySelectorAll('[data-attr="explore-' + index + '"]');
                var path = '/content/tata-capital-moneyfy/en/mutual-funds/';
                var fundPath = value.schemeDetails.name.toLowerCase().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '-').replace(/\s/g, "-").concat('.html');
                path = path.concat(value.amcName.toLowerCase().replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '-').replace(/\s/g, "-") + '/' + fundPath);
                path = path.replace(/[-]+/g, '-');
                $(explorePath).find('[data-attr="explore-more"]').attr('href', path);
                $(explorePath).find('[data-investnow="true"]').attr('data-schemeid', value.schemeDetails.id);
                $(explorePath).find('[data-attr="explore-more"]').attr('data-riskType', value.schemeDetails.riskType);
                $(explorePath).find('[data-investnow="true"]').attr('data-riskType', value.schemeDetails.riskType);
                $(explorePath).find('[data-attr="explore-more"]').attr('data-schemeName', value.schemeDetails.name);
                $(explorePath).find('[data-investnow="true"]').attr('data-schemeName', value.schemeDetails.name);
                var fundPerentElem = document.querySelector('[data-fund="fund-' + index + '"]');
                var ratingPerentElem = document.querySelector('[data-rating="rating-' + index + '"]');
                var returnFund = document.querySelector('[data-return="return-' + index + '"]');
                var fundAllocation = document.querySelector('[data-allocation="acllocate-' + index + '"]');
                var fundfilter = document.querySelector('[data-filters="filter-' + index + '"]');
                var schemeData = document.querySelector('[data-scheme="scheme-' + index + '"]');
                var fundManager = document.querySelector('[data-fundManager="fundManager-' + index + '"]');
                var sectoral = document.querySelector('[data-sectoral="sector-' + index + '"]');
                var exitvalueRender = schemeData.querySelector('[data-bench="benchmark"]');
                value.exitLoadValues.forEach(function (val, ind) {
                    var remarkVal = val.remark ? val.remark : '-';
                    var exitLoadVal = val.exitLoad ? val.exitLoad : '-';
                    var myvar = '<div class="feature-item">' +
                        '                            <p>' + remarkVal + '</p>' +
                        '                            <h6>' + exitLoadVal + '</h6>' +
                        '                        </div>';
                    $(myvar).insertAfter(exitvalueRender)
                })
                var holdingDetailTemp = '';
                value.holdingDetails.sector.forEach(function (value, index) {
                    holdingDetailTemp += '<div class="feature-item">' +
                        '                            <p>' + value.name + '</p>' +
                        '                            <h6>' + value.percentage + '%</h6>' +
                        '                        </div>';

                })
                sectoral.innerHTML = holdingDetailTemp;
                var flatObj = jsHelper.flattenObj(value);
                console.log('flat', flatObj);
                Object.keys(flatObj).forEach(function (key) {
                    var fundfilterEle = fundfilter.querySelector('[data-attr="' + key + '"]')
                    if (jsHelper.isDef(fundfilterEle)) {
                        fundfilterEle.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }
                    var element = fundPerentElem.querySelector('[data-attr="' + key + '"]');
                    if (jsHelper.isDef(element)) {
                        element.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }

                    var ratingEle = ratingPerentElem.querySelector('[data-attr="' + key + '"]');
                    if (key == 'ratings.morningStar')
                        var rateClassName = 'ms-no-rating';
                    else if (key == 'ratings.valueResearch') {
                        var rateClassName = 'vr-no-rating';
                    }
                    var currKey = flatObj[key] ? flatObj[key] : '-';
                    if (jsHelper.isDef(ratingEle)) {
                    ratingEle.innerHTML = (Number(flatObj[key]) == 0) ? (ratingEle.parentElement.parentElement.innerHTML = '<h6 class="' + rateClassName + '">-</h6>') : flatObj[key];
                    $('.js-removeCompareItem').click(function () {
                        document.querySelectorAll('.' + rateClassName).forEach(function(el) {
                        if (el.parentElement.parentElement.parentElement.classList.contains('hide-data')) {
                            var headTitle = rateClassName == 'ms-no-rating' ? 'Morning Star' : 'Value Research';
                            el.parentElement.innerHTML = '<h6>' + headTitle + '</h6>' +
                            '<p><span data-attr="' + key + '">' + currKey + '</span> <span class="icon icon-star"></span></p>';
                            }
                        })
                    })
                    }

                    var returnEle = returnFund.querySelector('[data-attr="' + key + '"]')
                    if (jsHelper.isDef(returnEle)) {
                        returnEle.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }
                    var fundAllocEle = fundAllocation.querySelector('[data-attr="' + key + '"]')
                    if (jsHelper.isDef(fundAllocEle)) {
                        fundAllocEle.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }
                    var schemDetailEle = schemeData.querySelector('[data-attrs="' + key + '"]')
                    if (jsHelper.isDef(schemDetailEle)) {
                        schemDetailEle.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }
                    var fundManagerData = fundManager.querySelector('[data-attr="' + key + '"]')
                    if (jsHelper.isDef(fundManagerData)) {
                        fundManagerData.innerHTML = flatObj[key] ? flatObj[key] : "-";
                    }
                });

                var modalId = 'searchFund-modal-' + (index + 1);
                var fundItem = $('[data-select-modal="' + modalId + '"]').find('.js-compareSelected').attr('data-item');
                $('.compare-feature.' + fundItem + '').removeClass('hide-data');
                $('[data-select-modal="' + modalId + '"]').find('.js-compareDropdown').addClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-compareSelected').removeClass('d-none');
            
            });
        }

        function renderWatchListPortfolioFunds(renderArray, addFromType) {
            var fundRenderStr = '';
            var selector = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
            if (renderArray.length > 0) {
                renderArray.forEach(function (fund) {
                    var iconPath = '/content/dam/tata-capital-moneyfy/amc-icons/';
                    iconPath = iconPath + fund.amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.png');
                    fundRenderStr += '<div class="fund-list-li" data-schemeid="' + fund.schemeDetails.id + '">' +
                        '                                <div class="card-div similar-mutual-funds-items">' +
                        '                                    <div class="top-row">' +
                        '                                        <div class="checkbox-name-wrap">' +
                        '                                            <div class="custom-checkbox">' +
                        '                                                <label class="js-addCompare">' +
                        '                                                    <input type="checkbox">' +
                        '                                                    <span class="checkbox-wrap"></span>' +
                        '                                                </label>' +
                        '                                            </div>' +
                        '                                            <div class="info-rating-wrap">' +
                        '                                                <div class="name-return-wrap">' +
                        '                                                    <div class="img-name-wrap">' +
                        '                                                        <div class="img-wrap">' +
                        '                                                            <img src="'+ iconPath +'" alt="" />' +
                        '                                                        </div>' +
                        '                                                        <div class="name-rating-wrap">' +
                        '                                                            <h6 class="fund-name two-lines" data-schemename="' + fund.schemeDetails.name + '">' + fund.schemeDetails.name + '</h6>' +
                        '                                                            <div class="rating-wrap d-none d-md-flex">' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Morning Star' +
                        '                                                                </p>' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Value Research' +
                        '                                                                </p>' +
                        '                                                            </div>' +
                        '                                                        </div>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="returs-box d-none d-lg-block">' +
                        '                                            <div class="d-flex">' +
                        '                                                <div class="returs-wrap">' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">NAV</p>' +
                        '                                                        <h6>' + compareFundFilterBizObj.reduceDigit(fund.nav, 2) + '</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1M Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1m']) != "-" ? compareFundFilterBizObj.reduceDigit(fund.cagrValues['1m'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1y']) != "-" ? compareFundFilterBizObj.reduceDigit(fund.cagrValues['1y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">3Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['3y']) != "-" ? compareFundFilterBizObj.reduceDigit(fund.cagrValues['3y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="return-info d-lg-none">' +
                        '                                            <div class="form-select2">' +
                        '                                                <select class="single-select2 portfolioWatchListReturns" data-placeholder="-- Select --">' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1m']) + '>1M Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1y']) + '>1Y Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['3y']) + '>3Y Returns</option>' +
                        '                                                </select>' +
                        '                                            </div>' +
                        '                                            <h6 class="trend-info text-right' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? ' up-trend"' : ' down-trend"') : '-') + 'data-rate="rate"' + '>' +
                        '                                            <span class=' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? "icon-angle-up" : "icon-angle-down") : '-') + '></span> ' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%' +
                        '                                        </h6>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                    <div class="rating-mob">' +
                        '                                        <div class="rating-wrap d-md-none">' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Morning Star' +
                        '                                            </p>' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Value Research' +
                        '                                            </p>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                </div>' +
                        '                            </div>';
                });    
            } else {
                fundRenderStr = '<p class="no-found">No funds found.</p>'
            }
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
        }

        comparefundRenderObj.renderWatchListPortfolioFunds = renderWatchListPortfolioFunds;
        comparefundRenderObj.renderFund = renderFund;
        return jsHelper.freezeObj(comparefundRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "comparefundRenderObj", comparefundRenderFn);
})(this || window || {});
/*compare fund render.js end*/