FAQ
====
This is a `FAQ` component in which some popular questions are answered about Mutual Funds. 

## Feature
* This is a multifield component.
* It will be easy to customers to learn about Mutual Funds


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used for rendering title of the component.
2. `./link` Used to render redirection link of a card on page.
3. `./faqLinkText` Used for rendering of click text. 
4. `./multi` Used to create multifield in component.
5. `./questions` Used for rendering frequently asked questions on the page.
6. `./description` Used for rendering description of the component.


## Client Libraries
The component provides a `moneyfy.faq` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5