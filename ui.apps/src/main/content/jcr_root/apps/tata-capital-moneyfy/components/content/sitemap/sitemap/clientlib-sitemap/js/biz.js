document.addEventListener("DOMContentLoaded", function(){
    $('[data-action="investment-choices"]').click('click', function () {
        location.href = appConfig.jocataDomain + "?action=investment-choices&source=" + window.location.href;
    });
    $('[data-action="risk-profile"]').click('click', function () {
        location.href = appConfig.jocataDomain + "?action=risk-profile&source=" + window.location.href;
    });
});