FAQ Categories
====
`FAQ Categories` component used to categorise Frequently Asked Questions.

## Feature
* This is a multifield component.
* It shows popular question answers as per category.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used to render the heading of a component.
2. `./categoriesList` It is used to create Multifield.
3. `./link` Used to enter the link.
4. `./newTab` Used to redirect link in a new tab.
6. `./image`Used to render the image.
7. `./hoverImage` Used to render hover image.
8. `./cardHeading` Used to render heading for card.
9. `./description` Used to render description of a card.
10. `./buttonTxt`Used to render button text.


## Client Libraries
The component provides a `moneyfy.faq-categories` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.faq-category-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-faq-category`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5