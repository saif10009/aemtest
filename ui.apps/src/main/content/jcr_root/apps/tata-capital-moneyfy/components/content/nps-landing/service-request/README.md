Service Request
====
Service Request component written in HTL, used to generate service request.

## Feature
* This is authorable component.
* Used to review your NPS Investments and also generate service request for same.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `heading` Used for rendering heading of the component.
2. `buttonTitle` Used for rendering title of the button.
3. `./buttonLink` Used to render redirection link of a button on page.
4. `./newTab` Enable open redirection link in new tab.


## Client Libraries
The component provides a `moneyfy.service-request` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.nps-landing-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-nps-landing`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5