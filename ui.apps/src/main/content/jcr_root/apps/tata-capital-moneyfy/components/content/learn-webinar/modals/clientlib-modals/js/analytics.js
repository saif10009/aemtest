document.querySelectorAll('.popover-modal-close').forEach(function (closeBtn) {
  closeBtn.addEventListener('click', function (e) {
    if (document.getElementById('video-modal').style.display == 'block' || document.querySelectorAll('.modal-backdrop').length > 1) {
      popupExitCapture = popupExitCapture == '' ? 'share video step1' : popupExitCapture;
      if (sectionName == 'webinar') {
        popupExitCapture = popupExitCapture == '' ? 'share video step1' : popupExitCapture;
      } else if (sectionName == 'market insights') {
        popupExitCapture = popupExitCapture == '' ? 'share video step1' : popupExitCapture;
      }
      componentName = (sectionName == 'webinar') ?  document.querySelector('.heading-view').innerText 
      : (window.location.href.includes('learn-academy'))? popupexitCompNameArr.pop() :document.querySelector('.heading-with-view').innerText
    } else {
      console.log(popupexitCompNameArr);
      popupExitCapture = 'share article';
      componentName = (sectionName == 'webinar') ?  document.querySelector('.heading-view').innerText 
      : (window.location.href.includes('learn-academy'))? popupexitCompNameArr.pop() :document.querySelector('.heading-with-view').innerText
    }
    var ctaText = 'exit popup';
    var popupExitCaptureTitle = ''
    popupExitCaptureTitle = popupExitCaptureTitleArr.pop();

    if(getParentElement(e.currentTarget, 5).id == 'stay-registration-modal' || getParentElement(e.currentTarget, 5).id == 'registration-modal'){
      var popupExitCaptureTitle = getParentElement(e.currentTarget, 5).querySelector('.registration-form h5').innerText;
      var popupType = 'registration popup'
      var ctaText = 'exit popup'
      var componentName = getParentElement(e.currentTarget, 5).querySelector('h5').hasAttribute('id')
       ? 'upcoming webinar registration now' : 'stay updated registration now' 
       if(getParentElement(e.currentTarget, 4).id !== 'login-modal'){
         popupexitClick(sectionName, componentName, popupExitCaptureTitle, popupType, ctaText, userIdObj.userId)
       }
    } else {
      if(getParentElement(e.currentTarget, 4).id !== 'login-modal'){
       popupexitClick(sectionName, componentName, popupExitCaptureTitle, popupExitCapture, ctaText, userIdObj.userId)
      }
    }
    
  })
})
