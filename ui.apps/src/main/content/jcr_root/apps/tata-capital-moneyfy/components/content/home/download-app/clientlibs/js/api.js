/*download app api js start*/
(function (_global) {
    var downloadAppApiFn = (function (jsHelper) {
        var downloadAppApiObj = {};
        var downloadApp = function (data) {
            return apiUtility.downloadAppApiFn(data);
        }
        downloadAppApiObj.downloadApp = downloadApp;
        return jsHelper.freezeObj(downloadAppApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'downloadAppApiObj', downloadAppApiFn);
})(this || window || {});
/*download app api js end*/