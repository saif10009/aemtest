    /* mutual fund search js start*/
    (function (_global) {

        var mutualfundsearchFn = (function (jsHelper) {
            var mutualfundsearchObj = {};
            document.addEventListener('DOMContentLoaded', function () {
                clickEvent();

            });
            function clickEvent() {

                /* mutual fund search modal */
                var substringMatcher = function (strs) {
                    return function findMatches(q, cb) {
                        var matches, substringRegex;

                        // an array that will be populated with substring matches
                        matches = [];

                        // regex used to determine if a string contains the substring `q`
                        substrRegex = new RegExp(q, 'i');

                        // iterate through the pool of strings and for any string that
                        // contains the substring `q`, add it to the `matches` array
                        $.each(strs, function (i, str) {
                            if (substrRegex.test(str)) {
                                matches.push(str);
                            }
                        });

                        cb(matches);
                    };
                };

                var searchmutualFundList = ['Reliance Long Term Mutual Fund', 'Reliance Focused 25 Mutual Fund', 'Reliance Largcap Mutual Fund', 'Reliance Midcap Mutual Fund', 'Reliance Captial Ltd. Mutual Fund', 'SBI Dynamic Bond Fund', 'HDFC Bond Fund'];

                $('#jsMutualFundSearchTypehead .typeahead').typeahead({
                    hint: false,
                    highlight: true,
                    minLength: 1
                },
                    {
                        name: 'searchmutualFundList',
                        source: substringMatcher(searchmutualFundList)
                    });

            }

            return jsHelper.freezeObj(mutualfundsearchObj);
        })(jsHelper);

        _global.jsHelper.defineReadOnlyObjProp(_global, 'mutualfundsearchObj', mutualfundsearchFn);
    })(this || window || {});

    /* mutual fund search js end*/