/*fund manager biz js start*/
(function (_global) {
  var fundManagerBizObj = (function (jsHelper) {
    var fundManagerObj = {}

    document.addEventListener('DOMContentLoaded', function () {
      // fundManagerslick();
      renderFundManagerData();
    })
    function renderFundManagerData() {
      let requestObj = {
        'body': {}
      };

      fundManagerApiObj.fundManager(requestObj).then(function (response) {
        if (response.status.toUpperCase() == 'SUCCESS') {
          var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
          fundManagerRenderObj.renderFundManager(responseData.responseJson.exploreFundsViaFundManager);
          fundManagerslick();
        } else {
          document.getElementById('fundManagerSlider').innerHTML = 'There was some issue in getting response.';
          console.log('There was some error in getting Fund Manager api response.');
        }
      }).catch(function (error) {
        console.log(error);
      });
    }
    function fundManagerslick() {
      $('#fundManagerSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        if ($('.inner-fund-manager .fund-manager-rows').length <= 3) {
          if ($(window).width() > 1199) {
            $('.inner-fund-manager').removeClass('slider-dots');
          }
        }
      });

      $('#fundManagerSlider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              // variableWidth: true,
              centerMode: true,
              centerPadding: '20px'
            }
          },
          {
            breakpoint: 375,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: true,
              centerMode: true,
            }
          },
          {
            breakpoint: 360,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: true,
              centerMode: true,
            }
          }
        ]
      });
    }
    return jsHelper.freezeObj(fundManagerObj);
  })(jsHelper)
  _global.jsHelper.defineReadOnlyObjProp(_global, "fundManagerObj", fundManagerBizObj)
})(this || window || {});
/*fund manager biz js end*/
