Loan Insurance plans
====
The `Loan insurance plans` component can be used for show loan insurance plans. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as images, headings, title and button txt is authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering titles of the components.
2. `./heading ./subHeading` Used for rendering heading & subheading of the component.
3. `./image ./hoverImage` are used to select the different images paths of these component.
4. `./planName` Used to render heading.



## Client Libraries
The component provides a `moneyfy.loan-insurance-plans` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

