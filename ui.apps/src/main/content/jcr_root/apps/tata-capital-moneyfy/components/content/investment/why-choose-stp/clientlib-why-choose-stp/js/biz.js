  /*why choose stp js start*/
(function (_global) {

    var chooseStpFn = (function (jsHelper) {
        var chooseStpObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            slickEvent();
            
        });
        function slickEvent() {
            if ($(window).width() > 991) {
                if ($('#chooseSTPslider').hasClass('slick-initialized')) {
                  $('#chooseSTPslider').slick('unslick');
                }
              } else {
                $('#chooseSTPslider').not('.slick-initialized').slick({
                  dots: true,
                  infinite: false,
                  speed: 300,
                  slidesToShow: 3,
                  slidesToScroll: 1,
                  arrows: true,
                  responsive: [
                    {
                      breakpoint: 768,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                      }
                    },
                    {
                      breakpoint: 360,
                      settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                      }
                    }
                  ]
                });
              }
        }

        return jsHelper.freezeObj(chooseStpObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'chooseStpObj', chooseStpFn);
})(this || window || {});

/*why choose stp js end*/