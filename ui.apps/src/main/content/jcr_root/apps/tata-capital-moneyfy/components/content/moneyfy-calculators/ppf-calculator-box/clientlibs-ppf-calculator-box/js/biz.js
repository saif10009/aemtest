/*ppf calculator js start*/
(function (_global) {
    var ppfCalculatorBizObj = (function (jsHelper) {
        var ppfCalculatorObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            ppfCalculatorInitializer();
            ppfCalculatorEvents();
            ppfHighchartReact(150000, 121214, 100)
            if ($('[data-ppfcalbtn]') != undefined) {
                $('[data-ppfcalbtn]').click(function () {
                    var ctaText = $(this).text().trim();
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('ppf-calculator-box', userId, 'PPF calculator', ctaText);
                        }else{
                            calculatorCtaInteraction('ppf-calculator-box', userId, 'PPF calculator', ctaText);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('ppf-calculator-box', "anonymous user", 'PPF calculator', ctaText);
                        }else{
                            calculatorCtaInteraction('ppf-calculator-box', "anonymous user", 'PPF calculator', ctaText);
                        }
                    }
                })
            }
        })
        function ppfCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
        }

        function ppfCalculatorEvents() {
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
                        $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            // year month toggle
            $('[data-yrMthFilter]').click(function () {
                filterVal = $(this).attr('data-yrMthFilter');
                $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
                $(this).addClass('active');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
            });
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var textChange = $this.val();
                    textChange = textChange.replace(/,/g, "");
                    my_range.update({
                        from: textChange,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end 

            // calculator js
            var tenureObj = {
                ppfTenureYear: document.getElementById("ppf_tenure_year"),
                ppfTenureMonth: document.getElementById("ppf_tenure_month"),
                ppfErrorMsg: document.getElementById("ppfErrorMsg"),
            }
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").on("input", function () {
                    if (document.querySelector('[class="yr-mth-btn active"]').innerHTML == 'Years') {
                        var convertYears = Number(tenureObj.ppfTenureYear.value) * 12;
                        ppfCalculationFn(convertYears)
                    } else {
                        ppfCalculationFn(Number(tenureObj.ppfTenureMonth.value));
                    }
                });
            }
            document.getElementById("ppf_amount").addEventListener('keyup', function (e) {
                if (parseFloat(this.value.replace(/,/g, '')) > 150000) {
                    tenureObj.ppfErrorMsg.innerHTML = '';
                    tenureObj.ppfErrorMsg.innerHTML = 'Amount should not greater than ₹150000';
                } else {
                    tenureObj.ppfErrorMsg.innerHTML = '';
                }
                var activeBtn = document.querySelector('[class="yr-mth-btn active"]');
                if (activeBtn.innerHTML == 'Years') {
                    var convertYears = Number(tenureObj.ppfTenureYear.value) * 12;
                    ppfCalculationFn(convertYears)
                } else {
                    ppfCalculationFn(Number(tenureObj.ppfTenureMonth.value));
                }
            })
            $('[data-yrMthFilter="year"]').click(function () {
                ppfCalculationFn(Number(tenureObj.ppfTenureYear.value) * 12);
            })
            $('[data-yrMthFilter="month"]').click(function () {
                ppfCalculationFn(Number(tenureObj.ppfTenureMonth.value));
            })
            //calculator js


        }

        function ppfCalculationFn(ppfTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            var ppfInputObj = {
                calculator: "PPF",
                ppfAmount: Number(parseFloat(document.getElementById("ppf_amount").value.replace(/,/g, ''))),
                ppfTenure: ppfTenure,
                ppfRoi: Number(document.getElementById("ppf_roi").value) / 100,
            }
            myWorker.postMessage(ppfInputObj);
            myWorker.onmessage = function (e) {
                ppfInputObj.ppfHighchartReact = ppfHighchartReact;
                ppfCalculatorRenderObj.renderPpfCalculation(e, ppfInputObj);
                myWorker.terminate();
            }
        };
        //calculator js
        function ppfHighchartReact(invest, interest, initialAxis) {
            var reactChart = (invest / (invest + interest)) * 100;
            // Range slider end
            Highcharts.chart("ppf-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });
        }
        return jsHelper.freezeObj(ppfCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "ppfCalculatorBizObj", ppfCalculatorBizObj)
})(this || window || {});
/*ppf calculator js end*/