/*emi calculator render js start*/
(function (_global) {
    var emiCalculatorRenderFn = (function (jsHelper) {
        var emiCalculatorRenderObj = {}
        function renderEmiCalculation(e, emiObj) {
            var MonthlyEmi = document.getElementsByClassName("MonthlyEmi");
            var totalPayAmount = document.getElementsByClassName('totalPayAmount');
            var totalInterest = document.getElementById("totalInterest");
            var principalAmount = document.getElementById('principalAmount');

            if ((isNaN(emiObj.emiamount)) || (emiObj.emiamount < 25000) || (emiObj.emiamount > 500000000)) { 
                MonthlyEmi[0].innerHTML = '₹ 0';
                MonthlyEmi[1].innerHTML = '₹ 0';
                totalPayAmount[0].innerHTML = "₹ 0";
                totalPayAmount[1].innerHTML = "₹ 0";
                totalInterest.innerHTML = '₹ 0';
                principalAmount.innerHTML = '₹ 0';
                emiObj.emiHighchartReact(0, 0, 0)
            }
            else {
                MonthlyEmi[0].innerHTML =
                    "₹ " + Math.round(e.data.emi).toLocaleString('en-IN');
                MonthlyEmi[1].innerHTML =
                    "₹ " + Math.round(e.data.emi).toLocaleString('en-IN');
                principalAmount.innerHTML = "₹ " + emiObj.emiamount.toLocaleString('en-IN');
                totalInterest.innerHTML = "₹ " + Math.round(e.data.interest).toLocaleString('en-IN');
                totalPayAmount[0].innerHTML = "₹ " + e.data.tpa.toLocaleString('en-IN');
                totalPayAmount[1].innerHTML = "₹ " + e.data.tpa.toLocaleString('en-IN');
                emiObj.emiHighchartReact(emiObj.emiamount, Math.round(e.data.interest), 100)
            }
        }

        emiCalculatorRenderObj.renderEmiCalculation = renderEmiCalculation;
        return jsHelper.freezeObj(emiCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "emiCalculatorRenderObj", emiCalculatorRenderFn)
})(this || window || {});
  /*emi calculator render js end*/