 /*banner slider js*/ 
  $('#jsBannerSlider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    autoplay: false,
  });

  setTimeout(function(){
    if($('.banner-slider').hasClass('slick-initialized')){
        $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
    }
  }, 2000);

  /*banner slider js*/ 
