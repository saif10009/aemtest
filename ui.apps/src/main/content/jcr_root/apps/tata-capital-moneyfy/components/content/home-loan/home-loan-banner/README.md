Home loan Banner
====
The `Home loan Banner` component is for adding banners on moneyfy. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as descriptions, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./img` Used to select the images for the banners in the component.
2. `./description` are used for the short description that can be given to the respective cards.
3. `./bannerLink ./btn1Link ./checkOne` are used for select redirection paths if the tabs should open in new tab.



## Client Libraries
The component provides a `moneyfy.home-loan-banner` editor client library category that includes JavaScript and CSS and it is call in html file. `biz.js` consist business logic.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5