About us data box
====
This is a `About us data box` component used in about us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as images, headings, title and button txt is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./whyMoneyfyList` Used for looping for html .
2. `./heading` Used for rendering title of the component.
3. `./description` Used for rendering description of the component.
4. `./image` Used for rendering image on page.



## Client Libraries
The component provides a `moneyfy.about-us-data-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5