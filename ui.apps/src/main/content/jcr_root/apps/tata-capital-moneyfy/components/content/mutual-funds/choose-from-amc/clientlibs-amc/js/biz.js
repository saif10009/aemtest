/*choose from amc  biz.js start*/
(function (_global) {
    var chooseFromAmcBizFn = (function (jsHelper) {
        var chooseFromAmcObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            initializeChooseAmcSlick();
            if($('[data-amcFundManger]') != undefined){
              $('[data-amcFundManger]').click(function (event) {
                  event.preventDefault();
                  var amcTitle = $(this).find('h5').text().trim();
                  amcItemClick(amcTitle, userIdObj.userId,"choose-from-amc");
                  location.href = $(this).attr('href');
              })
           }
           if($('[data-amcviewall]') != undefined){
           $('[data-amcviewall]').click(function(event){
               event.preventDefault();
            var ctaText = $(this).text().trim();
            var ctaTitle = $(this).parents('.heading-with-view').find('h2').text().trim();
            allCTAInteraction(ctaText,ctaTitle,"choose-from-amc", userIdObj.userId)
            location.href=$(this).attr('href');
          })
        }
        });

        function initializeChooseAmcSlick() {
            $('#amcSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                if ($('.inner-amc-slide .fund-manager-rows').length <= 3) {
                    if ($(window).width() > 1199) {
                        $('.inner-amc-slide').removeClass('slider-dots');
                    }
                }
            });

            $('#amcSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            // variableWidth: true,
                            centerMode: true,
                            centerPadding: '20px'
                        }
                    },
                    {
                        breakpoint: 375,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: true,
                            centerMode: true,
                        }
                    },
                    {
                        breakpoint: 360,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: true,
                            centerMode: true,
                        }
                    }
                ]
            });
        }

        return jsHelper.freezeObj(chooseFromAmcObj);
        //adobe analytics 7-23
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "chooseFromAmcBizObj", chooseFromAmcBizFn)
})(this || window || {});
/*choose from amc biz.js end*/