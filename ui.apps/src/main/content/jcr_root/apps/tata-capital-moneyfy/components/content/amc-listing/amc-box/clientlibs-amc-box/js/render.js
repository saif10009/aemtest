/*AMC listing render.js start*/
(function (_global) {
    var amcListingRenderObj = (function (jsHelper) {
        var amcListingRenderObj = {}
        
        function renderAmc(amcArray) {
            document.getElementById('amcList').innerHTML = '';
            if (amcArray.length > 0) {
                amcArray.forEach(function(amc){
                    document.getElementById('amcList').innerHTML += '<div class="fund-manager-rows">'+
                    '              <div class="fund-manager-col">' +
                    '               <a href="'+ amc.pagePath +'" class="card-div fund-manager-card" data-fundmangercard="amcFundCard">'+    
                    '                  <div class="card-div fund-manager-card">'+
                    '                      <div class="fund-manager-top">'+
                    '                          <div class="amc-text-logo">'+
                    '                              <span class="amc-logo">'+
                    '                                  <img src="' + amc.iconPath + '" alt="">'+
                    '                              </span>'+
                    '                              <h5 data-amcfundname="'+amc.fundName+'">'+ (amc.fundName != undefined || amc.fundName != null ? amc.fundName : '-') +'</h5>'+
                    '                          </div>'+
                    '                      </div>'+
                    '                      <div class="fund-manager-bottom">'+
                    '                          <div class="fund-ul">'+
                    '                              <div class="fund-li">'+
                    '                                  <p>Funds offered</p>'+
                    '                                  <h6>'+ (amc.numberOfFunds != undefined || amc.numberOfFunds != null ? amc.numberOfFunds : '-') +'</h6>'+
                    '                              </div>'+
                    '                              <div class="fund-li">'+
                    '                                  <p>AUM</p>'+
                    '                                  <h6>₹'+ (amc.aum != undefined || amc.aum != null ? (Math.round(amc.aum)).toLocaleString('en-IN') + ' Cr' : '-') +'</h6>'+
                    '                              </div>'+
                    '                              <div class="fund-li">'+
                    '                                  <p>Highest return</p>'+
                    '                                  <h6>'+ (amc.highestReturns != undefined || amc.highestReturns != null ? amc.highestReturns + '%' : '-') +'</h6>'+
                    '                              </div>'+
                    '                          </div>'+
                    '                      </div>'+
                    '                  </div>' +
                    '               </a>'+    
                    '              </div>'+
                    '          </div>';
                });    
            } else {
                document.getElementById('amcList').innerHTML = "No results match found.";
            }
            
        }

        amcListingRenderObj.renderAmc = renderAmc;
        return jsHelper.freezeObj(amcListingRenderObj);
        // adobe analytics changes 12,19
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "amcListingRenderObj", amcListingRenderObj)
})(this || window || {});
/*AMC listing render.js end*/
