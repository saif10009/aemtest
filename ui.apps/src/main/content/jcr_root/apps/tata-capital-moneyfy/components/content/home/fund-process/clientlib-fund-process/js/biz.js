(function(_global) {

    var fundProcessBizFnObj = (function(jsHelper) {
        var fundprocessObj = {};
        document.addEventListener('DOMContentLoaded', function() {
            fundprocessInitiallizer();
            clickEvents();
            keyupEvents();
            adobeAnalytics();
        });

        function adobeAnalytics() {	
            $('[data-fundstoreimg]').click(function(event){	
                event.preventDefault();	
                if($(this).data('fundstoreimg') == 'appStoreImg'){	
                   appStoreImgClick('App Store', userIdObj.userId ,'fund-process')	
                   console.log('app',$(this).attr('href'))	
                   window.open($(this).attr('href'))	
                }else{	
                    appStoreImgClick('Google Play', userIdObj.userId,'fund-process')	
                    console.log('google',$(this).attr('href'))	
                    window.open($(this).attr('href'))	
                }	
            })	
        }

        function clickEvents() {
            /*modal js*/
            $('[data-dismiss="popover-modal"]').on('click', function() {
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();
            });
            /*modal js*/

            /*get app js*/

            $('.js-getapp-form button').click(function(e) {
                var ele_input = $(this).parents('.js-getapp-form').find('input');
                var errors = [];
                var ele_required = "Field is required";

                $(ele_input).each(function() {
                    var element = $(this);
                    var ele_phoneNumber = "Please enter valid number";

                    $(element).next('.error-msgs').remove();
                    $(element).after('<span class="error-msgs"></span>');

                    if (element.is(":visible")) {
                        if ($(element).val() != '') {
                            if ($(element).data('type') === 'mobile') {
                                if (!validateMobile(element)) {
                                    $(element).next('.error-msgs').text(ele_phoneNumber);
                                    errors.push(ele_phoneNumber);
                                } else {
                                    $(element).next('.error-msgs').remove();
                                }
                            }

                        } else {
                            $(element).next('.error-msgs').text(ele_required);
                            errors.push(ele_required);
                        }
                    }
                });

                if (errors.length == 0) {
                    e.preventDefault();
                    var getMobileNumber = $('.js-getapp-form input').val();
                    getAppLink(getMobileNumber, userIdObj.userId, 'fund-process');
                    var lastTwoDigit = getMobileNumber.toString().slice(-2);
                    $('.jsShowMobileNumber').text(lastTwoDigit);
                    var requestData = {
						"body": {
							"mobile": getMobileNumber
						}
					}
                    fundProcessApiObj.fundProcess(requestData).then(function(response) {
                        setTimeout(function() {
                            $('#getAppLinkViaSms').addClass('popover-show');
                            $("#getAppLinkViaSms").css('display', 'block');
                            $('body').addClass('popover-modal-open');
                            $('body').append('<div class="modal-backdrop"></div>');
                            $('#getAppLinkViaSms').find('.fund-api-error').css('display', 'none')
                            $('#getAppLinkViaSms').find('.fund-api-success').css('display', '')
                        }, 80);
                    }).catch(function(error) {
                        console.log("APi Failed");
                    });
                }
            });
            $('.jsgetAppLinkViaSms').click(function() {
                $('#getAppLinkViaSms').removeClass('popover-show');
                $("#getAppLinkViaSms").removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();
            })

            $('.jsEditGetApp').click(function() {
                    $(this).parents('.js-getapp-form').find('.error-msgs').remove();
                    $(this).parents('.js-getapp-form').find('#moneyfy01').removeClass('hidden');
                    $(this).parents('.js-getapp-form').find('#moneyfy02').addClass('hidden');
                    $(this).parents('.js-getapp-form').find('.get-app-loader').addClass('hidden');
                })
                /*get app js*/
        }

        function keyupEvents() {
            /*get app js*/
            $('.only-numeric-input').keyup(function(e) {
                $(this).val($(this).val().replace(/[^\w\s]/gi, ''));
            });
            
            $('.js-getapp-form input').keyup(function() {
                var element = $(this);
                var ele_required = "Field is required";
                var ele_phoneNumber = "Please enter valid number";

                $(element).next('.error-msgs').remove();
                $(element).after('<span class="error-msgs"></span>');

                if (element.is(":visible")) {
                    if ($(element).val() != '') {
                        if ($(element).data('type') === 'mobile') {
                            if (!validateMobile(element)) {
                                $(element).next('.error-msgs').text(ele_phoneNumber);
                            } else {
                                $(element).next('.error-msgs').remove();
                            }
                        }

                    } else {
                        $(element).next('.error-msgs').text(ele_required);
                    }
                }
            });
        }

        function fundprocessInitiallizer() {
            /*Make mutual fund investing simpler for you js*/
            $(".js-makeMobileSlider").slick({
                infinite: true,
                arrows: false,
                dots: false,
                autoplay: false,
                speed: 300,
                slidesToShow: 1,
                fade: true,
                cssEase: 'linear',
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        arrows: true,
                        fade: false,
                    }
                }, ]
            });

            //progress time
            var percentTime;
            var tick;
            var time = .1;
            var progressBarIndex = 0;

            function startProgressbar() {
                resetProgressbar();
                percentTime = 0;
                tick = setInterval(interval, 10);
            }

            function interval() {
                if (($('.js-makeMobileSlider .slick-track div[data-slick-index="' + progressBarIndex + '"]').attr("aria-hidden")) === "true") {
                    progressBarIndex = $('.js-makeMobileSlider .slick-track div[aria-hidden="false"]').data("slickIndex");

                    startProgressbar();
                } else {
                    $('.js-progressBarContainer .make-li').removeClass('active');
                    $('.js-progressBarContainer svg[data-slick-index="' + progressBarIndex + '"]').parents('.make-li').addClass('active');

                    percentTime += 1 / (time + 5);

                    $('.inProgress' + progressBarIndex).css({
                        strokeDashoffset: percentTime + 35
                    });
                    if (percentTime >= 100) {
                        $('.progress-slider').slick('slickNext');
                        progressBarIndex++;
                        if (progressBarIndex > 2) {
                            progressBarIndex = 0;
                        }
                        startProgressbar();
                    }
                }
            }

            function resetProgressbar() {
                $('.inProgress').css({
                    strokeDashoffset: 0
                });
                clearInterval(tick);
            }
            startProgressbar();
            //progress time end

            $('.js-progressBarContainer .make-li').click(function() {
                clearInterval(tick);
                var goToThisIndex = $(this).find("svg").data("slickIndex");
                $('.progress-slider').slick('slickGoTo', goToThisIndex, false);
                startProgressbar();
            });
            /*Make mutual fund investing simpler for you js*/
        }

        function validateMobile(mobileField) {
            var re = /^[4-9][0-9]{9}$/;
            var check = re.test($(mobileField).val());
            if ($(mobileField).val().length != 10 || !check) {
                return false;
            } else {
                return true;
            }
        }

        return jsHelper.freezeObj(fundprocessObj);
        //adobe analytics 73
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundprocessObj', fundProcessBizFnObj);
})(this || window || {});