More Questions
====
The `More Questions` component provides a form in which people can submit their own doubts and questions about Mutual Funds. 



## Feature
* It is an authorable component with static form. 
* It gives flexibility to make changes in component such as we can replace a image & description of a form.


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./image` used to render image.
2. `./description` used for form description.
3. `./buttonTitle` Used to render button text.
4. `./buttonLink` used to redirect button link.
5. `./newTab` used to redirect link in new tab.

## Client Libraries
The component provides a `moneyfy.more-questions` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.faqs-detail-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-faqs-detail.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5