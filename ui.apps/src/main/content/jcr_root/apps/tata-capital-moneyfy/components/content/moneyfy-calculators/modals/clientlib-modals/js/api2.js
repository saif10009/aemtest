(function (_global) {
    var calcGetEmailApiFn = (function (jsHelper) {
        var calcGetEmailApiObj = {};
        
        // city search api call
        function apiCall(method, url, data) {
            return new Promise(function (resolve, reject) {
                var xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (this.readyState === 4) {
                        switch (this.status) {
                            case 200:
                                resolve(this.responseText);
                                break;
                            default:
                                reject(this.responseText);
                        }
                    }
                }
                xhr.open(method, url);
                xhr.send(data);
            });
        }

        function fetchData() {
            var calcObj = {}
            var calc = $('[data-title="compound-calc"]').text();
            var calcType = $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g, '');

            if (calc == 'Compound Interest Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    investmentAmount: $('[data-type="investment"]').val(),
                    expectedRoi: $('[data-type="roi"]').val(),
                    tenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') 
                    ? (Number($('.year-wrap [data-type="tenure"]').val())) + " year(s)" 
                    :  Math.floor(Number($('.month-wrap [data-type="tenure"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="tenure"]').val()) % 12 +" month(s)"),
                    futureValue: $('[data-type="futureVal"]').text().replace('₹ ', ''),
                    estimatedGain: $('[data-type="estimateGain"]').text().replace('₹ ', ''),
                }
            } else if (calc == 'Life insurance Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    currentAge: $('[data-type="age"]').val(),
                    retirementAge: $('[data-type="RetirementAge"]').val(),
                    monthlyIncome: $('[data-type="monthlyIncome"]').val(),
                    expectedIncrease: $('[data-type="expectedIncrease"]').val(),
                    returnInvestment: $('[data-type="returnInvestment"]').val(),
                    coverAmount: $('[data-type="coverAmount"]').text(),
                }
            } else if (calc == 'Systematic Investment Plan (SIP) Calculator' && calcType == 'SIP') {
                calcObj = {
                    calcType: $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g, '') + ' Calculator',
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    investmentAmount: $('[data-type="investmentAmount"]').val(),
                    expectedReturn: $('[data-type="expectedReturn"]').val(),
                    investmentTenureYears: document.querySelectorAll('[data-yrmthfilter="year"]')[0].classList.contains('active') 
                    ? $('.year-wrap [data-type="investmentTenureSip"]').val() + ' year(s)' 
                    : Math.floor(Number($('.month-wrap [data-type="investmentTenureSip"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="investmentTenureSip"]').val()) % 12 +" month(s)"),
                    futureValue: $('[data-type="futureValue"]').text().replace('₹ ', ''),
                    estimatedGain: $('[data-type="estimatedGain"]').text().replace('₹ ', ''),
                }
            } else if (calc == 'Systematic Investment Plan (SIP) Calculator' && calcType == 'Lumpsum') {
                calcObj = {
                    calcType: $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g, '') + ' Calculator',
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    investmentAmount: $('[data-type="investmentAmountLump"]').val(),
                    expectedReturn: $('[data-type="expectedReturnLump"]').val(),
                    investmentTenureYears: document.querySelectorAll('[data-yrmthfilter="year"]')[1].classList.contains('active') 
                    ? $('.year-wrap [data-type="investmentTenureLumpsum"]').val() + ' year(s)' 
                    :  Math.floor(Number($('.month-wrap [data-type="investmentTenureLumpsum"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="investmentTenureLumpsum"]').val()) % 12 +" month(s)"),
                    futureValue: $('[data-type="futureValueLump"]').text().replace('₹ ', ''),
                    estimatedGain: $('[data-type="estimatedGainLump"]').text().replace('₹ ', ''),
                }
            } else if (calc == 'EMI Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    principalAmount: $('[data-type="principalAmount"]').val(),
                    expectedInterestRate: $('[data-type="expectedInterestRate"]').val(),
                    investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') 
                    ? (Number($('.year-wrap [data-type="investmentTenure"]').val())) + " year(s)" 
                    : Math.floor(Number($('.month-wrap [data-type="investmentTenure"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="investmentTenure"]').val()) % 12 +" month(s)"),
                    totalAmount: $('[data-type="totalAmount"]').text().replace('₹ ', ''),
                    emiValue: $('[data-type="emiValue"]').text().replace('₹ ', ''),
                }
            } else if (calc == 'Recurring Deposit Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    monthlyContribution: $('[data-type="monthlyContribution"]').val(),
                    expectedReturn: $('[data-type="expectedReturn"]').val(),
                    investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') 
                    ? (Number($('.year-wrap [data-type="investmentTenure"]').val())) + " year(s)" 
                    : Math.floor(Number($('.month-wrap [data-type="investmentTenure"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="investmentTenure"]').val()) % 12 +" month(s)"),
                    expectedMaturity: $('[data-type="expectedMaturity"]').text().replace('₹ ', ''),
                    earningAmount: $('[data-type="earningAmount"]').text().replace('₹', ''),
                }
            } else if (calc == 'Fixed Deposit Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    lumpsumInvestment: $('[data-type="lumpsumInvestmnet"]').val(),
                    expectedRate: $('[data-type="expectedRate"]').val(),
                    investmentTenure: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') 
                    ? (Number($('.year-wrap [data-type="investmentTenure"]').val())) + " year(s)" 
                    :  Math.floor(Number($('.month-wrap [data-type="investmentTenure"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="investmentTenure"]').val()) % 12 +" month(s)"),
                    compoundingTenure: $('.compound-tenure-radio').find('input:checked').val(),
                    maturityValue: $('[data-type="maturityValue"]').text().replace('₹ ', ''),
                    earningAmount: $('[data-type="earningAmounting"]').text().replace('₹ ', ''),
                }
            } else if (calc == 'PPF Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    yearlyContribution: $('[data-type="yearlyContribution"]').val(),
                    expectedReturn: $('[data-type="expectedReturn"]').val(),
                    tenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') 
                    ? (Number($('.year-wrap [data-type="tenure"]').val())) + " year(s)" 
                    :  Math.floor(Number($('.month-wrap [data-type="tenure"]').val()) / 12) + " year(s) and " + (Number($('.month-wrap [data-type="tenure"]').val()) % 12 +" month(s)"),
                    maturityValue: $('[data-type="maturityValue"]').text().replace('₹ ', ''),
                    earningAmount: $('[data-type="earningAmounting"]').text().replace('₹', ''),
                }
            } else if (calc == 'Retirement Calculator') {
                calcObj = {
                    calcType: $('[data-title="compound-calc"]').text(),
                    custName: $('[data-type="name"]').val(),
                    mobileNumber: $('[data-type="mobile"]').val(),
                    city: $('[data-type="city"]').val(),
                    emailId: $('[data-type="email"]').val(),
                    currentAge: $('[data-type="CurrentAge"]').val(),
                    monthlyExpense: $('[data-type="monthlyExpence"]').val(),
                    accumulatedSavings: $('[data-type="accumulatedSaving"]').val(),
                    expectedReturn: $('[data-type="Inflation"]').val(),
                    expectedPreRetirementReturn: $('[data-type="preRetirmentReturn"]').val(),
                    expectedPostRetirementReturn: $('[data-type="postRetiremnetReturn"]').val(),
                    retirementAge: $('[data-type="retirementAge"]').val(),
                    costOfLiving: $('[data-type="costOfLiving"]').val(),
                    monthlySIP: $('[data-type="monthlySIP"]').text().replace('₹ ', ''),
                    expectedYearlyExpenditure: $('[data-type="expectedYearlyExpenditure"]').text().replace('₹ ', ''),
                    accumulatedRetirementKitty: $('[data-type="accumulatedRetirementKitty"]').text().replace('₹ ', ''),
                }
            }
            return calcObj;
        }

        
        var tryAgainOtpArr = [];
        function generateOTP(tryAgainOtp) {
            tryAgainOtpArr.push(fetchData().mobileNumber);
            fetchData();
            //console.log(fetchData());
            var otpMobileNumber = {
                otpMobile: $('[data-type="mobile"]').val()
            };
            var tryAgainOtpObj  = {
                otpMobile : tryAgainOtpArr.pop()
            }
            $('.js-loader').addClass('show');
            $('.js-loader').removeClass('d-none');
            $.ajax({
                url: '/content/tata-capital-moneyfy/moneyfyapi.generateOtp',
                type: "POST",
                async: false,
                data: (tryAgainOtp == true ? tryAgainOtpObj: otpMobileNumber),
                success: function (data, status, xhr) {
                    if ((data.retStatus).toUpperCase() == 'SUCCESS') {
					$('.js-loader').removeClass('show');
					$('.js-loader').addClass('d-none');
						otpRefNumber = data.otpRefNo;
                        $('[data-otpverify]').removeClass('d-none');
                        $('.js-OtpBox input:nth-child(1)').focus();
                        $('body').addClass('popover-modal-open');
                        document.querySelector('[data-otpverify="verifyOTP"]').innerHTML = calcGetEmailRenderObj.generateOTPSuccessPopup;
                        var getMobileNumber = fetchData().mobileNumber;
                        var firstTwoDigit = getMobileNumber.toString().slice(-2);
                        $('.js-OtpBox input:nth-child(1)').focus();
                        document.querySelector('#jsVeryfyOTP .jsShowWhatsappNumber').innerText = firstTwoDigit;
                        calcGetEmailBizObj.onGenerateOtpSuccess();
                        $('.jsGetResultClose').click(function () {
                            $('[data-forms]').removeClass('d-none');
                            $('[data-otpverify]').addClass('d-none');
                            $('[otp-successfull]').addClass('d-none');
                            $('[otp-unsuccessfull]').addClass('d-none');
                            $('#jsGetResultForms .input-textbox').val('');
                            $('#jsGetResultForms .form-group').removeClass('textboxerror');
                            $('#jsGetResultForms .input-textbox').next().text('');
                            $('#jsGetResultForms .single-select3').val(null).trigger('change');
                            $('#jsGetResultForms [data-type="checkbox"]').prop("checked", false);
                            $('.single-select3').each(function () {
                                $('.single-select3').parents('.form-textbox-new').addClass('active');
                                $('.single-select3').addClass('jsValueOK')
                            });
                            $('#jsGetResultForms .input-textbox[data-type]').addClass('jsValueOK');
                            fieldCount = $('#jsGetResultForms .jsValueOK').length;
                            $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled')
                        });
                    } else if((data.retStatus).toUpperCase() == 'FAILURE'){
                        $('.js-loader').addClass('d-none');
                        $('[data-thank="thankYou"]').removeClass('d-none')
                        $('.js-loader').removeClass('show');
                        $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
                        '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
                        '<h3>Oops! Something Went Wrong</h3>'+
                        '<div class="get-edit-link">'+
                        '<p>'+ 'Please Try again in sometime.' +'</p>'+ '</div>');
                        $('.jsGetResultClose').click(function(){
                            $('[data-otpverify="verifyOTP"]').addClass('d-none');
                            $('[data-thank="thankYou"]').addClass('d-none');
                            $('[data-forms]').removeClass('d-none');
                            $('[data-type="city"]').value = '';
                        });
                    }
                }
            });
        }

        function verifyOTP(){
            fetchData();
            //console.log(fetchData());
            //$('.js-loader').removeClass('show');
            $('.js-loader').addClass('show');
            var array = [];
            $('.js-OtpBox input').each(function (e, i) {
                array.push(i.value)
            })
            array.join('')
            var verifyMobileNumber = {
                "otpRefNumber": otpRefNumber,
                "otpValue": array.join('')
            };
        
            $('.js-loader').removeClass('d-none');
            $('[data-otpverify="verifyOTP"').addClass('d-none');
            $('.js-loader').addClass('show');
            $.ajax({
                url: '/content/tata-capital-moneyfy/moneyfyapi.verifyOtp',
                type: "POST",
                async: true,
                data: verifyMobileNumber,
                success: function (data, status, xhr) {
                    if ((data.retStatus).toUpperCase() == 'SUCCESS') { 
                       // $('.js-loader').removeClass('show');
                        $.ajax({
                            url: '/content/tata-capital-moneyfy/moneyfyapi.calcEmailServlet',
                            type: "POST",
                            async: true,
                            data : JSON.stringify(fetchData()),
                            dataType: "json",
                            contentType: "application/json",
                            success: function (data, status, xhr) {
                                if ((data.status).toUpperCase() == 'SUCCESS') {
                                    $('.js-loader').addClass('d-none');
                                    $('.js-loader').removeClass('show');
                                    //$('#getResulton .input-textbox').val('');
                                    $('[data-thank="thankYou"]').removeClass('d-none');
                                    $('.get-result-thankyou').html('<div class="get-result-thankyou"><img src="/content/dam/tata-capital-moneyfy/calculator-template-image/success.png" alt="" class="mgs-icons"><h3>Thank you</h3><p>The results have been sent to your email address at <span class="js-encryptEmail">'+ data.emailId +'</span></p>');
                                }else{
                                    $('.js-loader').removeClass('d-none');
                                    $('.js-loader').removeClass('show');
                                    //$('#getResulton .input-textbox').val('');
                                    $('[data-otpverify="verifyOTP"]').addClass('d-none');
                                    $('[data-thank="thankYou"]').removeClass('d-none')
                                    $('#getResulton').addClass('popover-show');
                                    $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
                                    '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
                                    '<h3>Oops! Something Went Wrong</h3>'+
                                    '<div class="get-edit-link">'+
                                    '<p>'+ 'Please Try again in sometime.' +'</p>'+ '</div>');
                                }
                            }
                        });
                        var mdmReqObj = {
                            "Master": [
                                {
                                    "name":  $('[data-type="name"]').val(),
                                    "email": $('[data-type="email"]').val(),
                                    "city": $('[data-type="city"]').val(),
                                    "mobile": $('[data-type="mobile"]').val() 
                                }
                            ]
                        }
                        apiCall('POST', mdmCalcUrl, JSON.stringify(mdmReqObj)).then(function(response){
                            console.log('mdmResponse', response);
                        });
                    } else if (data.retStatus.toUpperCase() === 'FAILURE') {
                        $('.js-loader').addClass('d-none');
                        $('.js-loader').removeClass('show');
                        //$('#getResulton .input-textbox').val('');
                        $('[data-otpverify="verifyOTP"]').addClass('d-none');
                        $('[data-thank="thankYou"]').removeClass('d-none')
                        $('#getResulton').addClass('popover-show');
                        $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
                        '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
                        '<h3>Verification unsuccessful</h3>'+
                        '<div class="get-edit-link">'+
                        '<p>'+ data.errorMessage +'</p>'+ '</div>' +
                        '<div class="btn-try-again">'+
                        '<a href="javascript:void(0);" class="btn-blue radius100 jsTryAgainOtp">try again</a>'+
                        '</div>');
                        $('.btn-try-again').click(function(){
                            document.querySelector('[data-thank="thankYou"]').classList.add('d-none');
                            document.querySelector('[data-otpverify="verifyOTP"]').classList.remove('d-none');
                            var tryAgainOtp = true;
                            generateOTP(tryAgainOtp);
                        });
                    }
                } 
            });
        }

        calcGetEmailApiObj.apiCall = apiCall;
        calcGetEmailApiObj.generateOTP = generateOTP;
        calcGetEmailApiObj.verifyOTP = verifyOTP;
        calcGetEmailApiObj.fetchData = fetchData;
     
        return jsHelper.freezeObj(calcGetEmailApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, "calcGetEmailApiObj", calcGetEmailApiFn);
})(this || window || {});