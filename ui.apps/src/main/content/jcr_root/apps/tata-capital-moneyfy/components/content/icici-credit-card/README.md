FAQ
====
This is a `ICICI Credit Card` component in which some popular questions are answered about Mutual Funds. 

## Feature
* ICICI Credit Card Journey


## Client Libraries
The component provides a `moneyfy.icici-credit-card` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5