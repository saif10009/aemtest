Digital Gold Why Choose
====
The `Digital Gold Why Choose` component can be used to list the various reasons to choose the digital gold. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as images, headings, title and button txt is authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering titles of the components.
2. `./heading ./subHeading` Used for rendering heading & subheading of the component.
3. `./mobileImage` are used to select the different images paths of these component.
4. `./description` Used to render description of an product type.



## Client Libraries
The component provides a `moneyfy.digital-gold-why-choose-box` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

