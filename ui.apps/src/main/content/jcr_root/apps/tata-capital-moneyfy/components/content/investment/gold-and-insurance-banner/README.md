Digital Gold & Insurance Banner
====
The `Digital Gold & Insurance Banner` component is for adding banners on moneyfy. 



## Feature
* It is an multifield based component and one can select option from one of the dropdown options (for eg. choose between digital gold banner or insurance banner).
* All the various elements of the component such as descriptions, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./goldTitle ./insuranceTitle` Used for adding the title to the gold and insurance banner.
2. `./goldHeading` Used to giive the heading for the digital gold banner.
3. `./goldDescription ./insuranceDescription` are used for the short description that can be given to the gold & insurance respective banners.
4. `./goldImage ./insuranceImage` are used for select images path for the respective banners.
5. `./checkOne ./checkTwo ./bannerCheck` are used to check if the link should open in new tabs.
6. `./goldButtonText ./insuranceButtonText` are for button labels of the banners.
7. `./bannerType` are used to select the banner type for digital gold & insurance banner pages.
8. `./goldLink ./insuranceLink` are for the redirection links on the button click.
9. `./goldNewTab ./insuranceNewTab` are for checking to open links in new tab or not.

## Client Libraries
The component provides a `moneyfy.gold-and-insurance-banner` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5