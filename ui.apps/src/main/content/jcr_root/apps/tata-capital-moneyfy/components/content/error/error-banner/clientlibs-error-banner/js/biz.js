/* error banner JS start */
(function (_global) {
    var errorBannerBizObj = (function (jsHelper) {
        var errorBannerObj = {}
        document.addEventListener('DOMContentLoaded', function () {

            errorBannerClick();
        })
        function errorBannerClick() {
            /************ accordian Js *************/
            $('[data-toggle="collapses"]').click(function(ele){
                var ele_parents = $(ele.target).parents('.accordian').attr('id');

                $('#' + ele_parents).find('.collapse').slideUp();
                $('#' + ele_parents).find('.accordian-card').removeClass('active');
            
                if($(ele.target).parent().siblings().css( 'display') == "block"){
                    $(ele.target).parent().siblings().slideUp();
                    $(ele.target).parents('.accordian-card').removeClass('active');
                } else {
                    $(ele.target).parent().siblings().slideDown();
                    $(ele.target).parents('.accordian-card').addClass('active');
                }
              });          
        }
        return jsHelper.freezeObj(errorBannerObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, "errorBannerBizObj", errorBannerBizObj);
})(this || window || {});
/* error banner JS end */
