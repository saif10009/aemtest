Sebi filter
====
This is a sebi Filter component which is used for filtering the range of Funds.

## Feature
* This component provides suitability to choose different sizes of Mutual Funds.


## Client Libraries
The component provides a `moneyfy.sebi-filter` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. It is already included in component HTML file.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5