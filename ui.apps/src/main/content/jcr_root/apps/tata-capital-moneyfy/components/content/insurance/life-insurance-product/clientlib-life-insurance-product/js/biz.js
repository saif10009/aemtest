/*insurence risk info js start*/
(function (_global) {
    var insuranceRiskInfoBizObj = (function (jsHelper) {
        var insuranceRiskInfoObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            insuranceRiskInfoSlick();
            insuranceRiskInfoClick();
            adobeAnalytics();
        })

        function adobeAnalytics() {
            $('[data-adobeAnalytics="insuranceProduct"]').click(function () {
                var insuranceName = $(this).parents('.sip-cards-info').data('info-card');
                var banneCTA = $(this).text().trim();
                insurancePageCtaInteraction(insuranceName, userIdObj.userId, 'life-insurance-product', banneCTA);
            });
        }

        function insuranceRiskInfoSlick() {
            $('#jsInsuranceRiskInfoSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 360,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                        }
                    }
                ]
            });
            if ($('.insurance-risk-info').is(':visible')) {
                if ($('.insurance-risk-info .risk-slider-flex .risk-slider-row').length <= 2) {
                    if ($(window).width() > 991) {
                        $('.risk-slider-flex').removeClass('slider-dots');
                    }
                }
            }
        }

        function insuranceRiskInfoClick() {
            $('.jsOpenModal').click(function () {
                $('.js-getapp-form-modal input').val('');
                $('.js-getapp-form-modal').find('.error-msgs').remove();
                $('.js-getapp-form-modal #moneyfy01').removeClass('hidden');
                $('.js-getapp-form-modal #moneyfy02').addClass('hidden');
                $('.js-getapp-form-modal .get-app-loader').addClass('hidden');
            });
            $('.mCustomScroll').mCustomScrollbar({
                axix: "x"
            });
            $('.filterList-mCustomScrollbar').mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });
        }


        return jsHelper.freezeObj(insuranceRiskInfoObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "insuranceRiskInfoBizObj", insuranceRiskInfoBizObj)
})(this || window || {});
  /*insurence risk box js end*/
