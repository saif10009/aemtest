/*banner slider js*/
    (function (_global) {
        var bannerBizObj = (function (jsHelper) {
            var bannerObj = {}
            document.addEventListener('DOMContentLoaded', function () {
                if ($('.banner-image').length > 1) {
                    bannerSlick()
                }
                $('[ data-loanpopup="loanpopup"]').click(function (event) {
                    event.preventDefault();
                    var loanName = $('.banner-left').find('h1').text().trim();
                    var ctaText = $(this).text().trim();
                    loanPageCtaInteraction(loanName,userIdObj.userId,'home-loan-banner',ctaText)
                    var redirect = $(this).data('loanredirect');
                    if (!headerBizObj.getUserObj().isLoggedIn) {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = appConfig.jocataDomain + redirect;
                        });
                    } else {
                        location.href = appConfig.jocataDomain + redirect;
                    }
                })
                $('[data-homeloan="redirecthomeloan"]').click(function (event) {
                    event.preventDefault();
                    var loanName = $('.banner-left').find('h1').text().trim();
                    var ctaText = $(this).text().trim();
                    loanPageCtaInteraction(loanName,userIdObj.userId,'home-loan-banner',ctaText)
                    location.href = $(this).attr('href');
                })
                
            })
            
            var loginReqLoans = ['used-car-loan', 'two-wheeler-loan', 'loan-against-property']
            loginReqLoans.forEach(function(loan){
                if(window.location.href.includes(loan)){
                    var proceedBtn = document.querySelector('#loanApplyBanner .js-termsBox .btn-blue')
                    if(proceedBtn !== null){
                        proceedBtn.addEventListener('click', function(){
                            document.querySelector('#loanApplyBanner').style.display = 'none';
                            document.querySelector('.modal-backdrop').remove();
                            var particularParams = loan.split('-')
                            var particularParam = '';
                            particularParams.forEach(function(param){
                                particularParam += param.slice(0,1);
                            });
                            showLoginPopup();
                            document.querySelector('[data-login="true"]').addEventListener('click', function () {
                                location.href = appConfig.jocataDomain + "?action="+particularParam;
                            });
                        });
                    }
                }
            });

            function bannerSlick() {
                $('#jsBannerSlider').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    autoplay: false,
                });
                setTimeout(function () {
                    if ($('.banner-slider').hasClass('slick-initialized')) {
                        $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                    }
                }, 2000);
            }
            return jsHelper.freezeObj(bannerObj);
        })(jsHelper)
        _global.jsHelper.defineReadOnlyObjProp(_global, "bannerBizObj", bannerBizObj)
    })(this || window || {});
  /*banner Slider js end*/