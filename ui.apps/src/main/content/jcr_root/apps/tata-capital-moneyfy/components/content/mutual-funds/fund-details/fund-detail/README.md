Fund Detail
====
The `Fund Detail` component is for displaying the detailed information of a specific mutual fund. 



## Feature
* It is an dynamic component with data populated using JS.    


## Edit Dialog Properties
There are no properties available as resources.


## Client Libraries
The component provides a `moneyfy.fund-detail` editor client library category that includes JavaScript and CSS required for the component.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5