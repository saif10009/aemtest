Life Insurance Product
====
The `Life Insurance Product` component can be used to list the various reasons to choose the digital gold. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as images, headings, title and button txt is authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering titles of the components.
2. `./infoHeading ./infoCardHeading` Used for rendering heading components.
3. `./infoCardImg` are used to select the different images paths of these component.
4. `./popBtn ./infoCardBtnOne ./infoCardBtnTwo` Used to render buttons on component.
5. `./infoCardDescription ./infoDescription` Used to render descrition on component.
6. `./infoCardDescription ./infoDescription` Used to render descrition on component.
7. `./popBtnLink` Used to render link  of buttons.
8. `../popBtnTarget` Used to render target of buttons.



## Client Libraries
The component provides a `moneyfy.life-insurance-product` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

