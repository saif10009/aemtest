(function (_global) {
    var analyticsObjCall = (function (jsHelper) {
        var analyticsCall = {}
        var fileItrBtns = document.querySelectorAll('.taxt-fill-btn');
        fileItrBtns.forEach(function(btn){
            btn.addEventListener('click', function(e){
                var ctaText = e.currentTarget.innerText.toLowerCase();
                var ctaTitle = ''
                var isTaxSpanner = e.currentTarget.parentElement.classList.contains('tax-spanner');
                if(!isTaxSpanner){
                    ctaTitle = 'taxblock'
                } else {
                    ctaTitle = 'tax spanner'
                }
                var componentName = document.querySelectorAll('.tax-filing-box')[0].classList[0]
                allCTAInteraction(ctaText, ctaTitle, componentName, userIdObj.userId)
            });
        })
        
        return jsHelper.freezeObj(analyticsCall);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "analyticsObjCall", analyticsObjCall)
})(this || window || {});