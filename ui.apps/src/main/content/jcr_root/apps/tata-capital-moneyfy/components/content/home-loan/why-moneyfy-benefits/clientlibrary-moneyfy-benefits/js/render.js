.banner-box {
  padding: 0px 0px 50px;
  position: relative;
}
.banner-box .banner-dot-bg {
  position: relative;
  background: url("../img/banner-bg.png") no-repeat top right;
  background-size: contain;
}
.banner-box .banner-dot-bg.banner-blue {
  background-image: url("../img/banner-bg-blue.png");
}
.banner-box .slick-dotted.slick-slider {
  margin-bottom: 0px;
}
.banner-box .slider-dots .slick-dots li {
  margin: 0 4px;
}
.banner-box .slider-dots .slick-dots li.slick-active button {
  background-color: #D7DF23;
}
.banner-box .slider-arrows .slick-arrow {
  bottom: auto;
  top: 50%;
}
.banner-box .slider-arrows .slick-prev {
  margin-left: -615px;
}
.banner-box .slider-arrows .slick-next {
  margin-right: -615px;
}
.banner-box .slick-dots {
  bottom: 30px;
}
.banner-box .banner-inner {
  display: flex;
  flex-wrap: wrap;
  padding: 25px 0 39px;
}
.banner-box .banner-left {
  padding-top: 35px;
  width: 480px;
  padding-right: 20px;
  height: 100%;
}
.banner-box .banner-left h1 {
  margin-bottom: 7px;
}
.banner-box .banner-left p {
  font-size: 17px;
  line-height: 28px;
  color: #606060;
  margin-bottom: 32px;
}
.banner-box .banner-right {
  width: 580px;
}
.banner-box .banner-slider .banner-slide-inner {
  position: relative;
  padding-top: 5px;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info {
  background-color: #034EA2;
  box-shadow: 20px 14px 30px rgba(153, 186, 208, 0.3);
  border-radius: 10px;
  padding: 30px 14px;
  min-width: 175px;
  position: absolute;
  left: 20%;
  bottom: 12%;
  z-index: 4;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info .share-icons {
  width: 30px;
  height: 30px;
  border-radius: 50%;
  background-color: #66CC99;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: 8px;
  font-size: 14px;
  color: #fff;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info p {
  font-weight: 600;
  font-size: 18px;
  line-height: 26px;
  color: #fff;
  margin-bottom: 10px;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info .text-equity {
  display: inline-block;
  font-weight: 500;
  font-size: 12px;
  line-height: 18px;
  text-align: center;
  color: #CC66CC;
  background: #FFE8FF;
  border: 1px solid rgba(204, 102, 204, 0.66);
  border-radius: 25.5px;
  padding: 4px 22px;
  word-break: break-word;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info.share-white {
  background-color: #fff;
  border: 1px solid #84b8f4;
  z-index: 3;
  left: 28px;
  bottom: auto;
  top: 0px;
}
.banner-box .banner-slider .banner-slide-inner .banner-share-info.share-white p {
  color: #333;
}
.banner-box .banner-blue {
  background-color: #2C6EB5;
}
.banner-box .banner-blue h1 {
  color: #fff;
}
.banner-box .banner-blue p {
  color: #fff;
}
.banner-box .banner-blue .btn-blue {
  background-color: #fff;
  color: #2C6EB5;
}
.banner-box .banner-blue .btn-blue.btn-blue-outline {
  background-color: transparent;
  border-color: #fff;
  color: #fff;
}

.banner-heightinner {
  height: 400px;
  overflow: hidden;
}

.banner-image {
  position: relative;
  overflow: hidden;
}
.banner-image .banner-caption {
  position: absolute;
  left: 0px;
  top: 0px;
  right: 0px;
  bottom: 0px;
  z-index: 1;
}
.banner-image .banner-caption .container {
  height: 100%;
}
.banner-image .banner-caption-inner {
  background-color: rgba(0, 88, 153, 0.9);
  width: 380px;
  height: 100%;
  padding: 20px 25px 20px 0px;
  position: relative;
}
.banner-image .banner-caption-inner:after {
  content: "";
  width: 5000px;
  height: 100%;
  left: -5000px;
  background-color: rgba(0, 88, 153, 0.9);
  position: absolute;
  top: 0px;
  display: block;
}
.banner-image .banner-caption-inner h6 {
  font-size: 14px;
  line-height: 20px;
  text-transform: uppercase;
  color: #FFFFFF;
  margin-bottom: 10px;
  font-weight: 400;
}
.banner-image .banner-caption-inner h3 {
  color: #fff;
  margin-bottom: 15px;
}
.banner-image .banner-caption-inner .btn-blue {
  margin-bottom: 15px;
}
.banner-image .banner-caption-inner .btn-view-all {
  color: #fff;
}

.banner-box.mutual-fund-banner {
  background: url("../img/mutual-fund-banner-bg.png") no-repeat top right;
  background-size: contain;
}
.banner-box.mutual-fund-banner .banner-slider {
  padding-bottom: 30px;
}
.banner-box.mutual-fund-banner .slick-dots {
  bottom: 10px;
}
.banner-box.mutual-fund-banner .slider-arrows .slick-arrow {
  top: auto;
  bottom: -22px;
}
.banner-box.mutual-fund-banner .slider-arrows .slick-prev {
  left: 50%;
  margin-left: -105px;
}
.banner-box.mutual-fund-banner .slider-arrows .slick-next {
  right: 50%;
  margin-right: -105px;
}
.banner-box.mutual-fund-banner .banner-image .banner-caption {
  position: static;
}
.banner-box.mutual-fund-banner.banner-heightinner {
  height: 418px;
  overflow: hidden;
}
.banner-box.mutual-fund-banner.no-banner-slider {
  padding-bottom: 70px;
}
.banner-box.mutual-fund-banner.no-banner-slider .banner-slider {
  padding-bottom: 0px;
}
.banner-box.mutual-fund-banner.no-banner-slider .banner-image {
  overflow: visible;
}
.banner-box.mutual-fund-banner.no-banner-slider .banner-inner {
  padding-bottom: 0px;
}
.banner-box.nps-banner {
  padding-bottom: 0;
}
.banner-box.nps-banner .banner-inner.new-banner-inner {
  padding-top: 0;
}
.banner-box.nps-banner .banner-image .banner-caption {
  position: static;
}
.banner-box.nps-banner .banner-image .banner-caption .banner-left {
  width: 630px;
}
.banner-box.nps-banner .banner-image .banner-caption .banner-head {
  margin-bottom: 40px;
}
.banner-box.nps-banner .banner-image .banner-caption .banner-right {
  width: calc(100% - 630px);
}
.banner-box .banner-inner.new-banner-inner {
  padding-top: 5px;
}
.banner-box .banner-inner.new-banner-inner .banner-left {
  width: 493px;
  padding-top: 20px;
}
.banner-box .banner-inner.new-banner-inner .banner-left .btn-blue {
  padding-left: 25px;
  padding-right: 25px;
}
.banner-box .banner-inner.new-banner-inner .banner-right {
  width: calc(100% - 493px);
}
.banner-box .banner-inner.new-banner-inner .sip-cards p {
  margin-bottom: 12px;
}
.banner-box.creditCard-loan-banner .btn-blue {
  min-width: 135px;
  padding: 17px 20px;
}
.banner-box.creditCard-loan-banner .slider-dots .slick-dots li.slick-active button {
  background-color: #333333;
}
.banner-box.creditCard-loan-banner .banner-row {
  display: flex;
  flex-wrap: wrap;
}
.banner-box.creditCard-loan-banner .left-col {
  width: 45%;
}
.banner-box.creditCard-loan-banner .right-col {
  min-width: unset;
  width: 55%;
  background-color: transparent;
}
.banner-box.creditCard-loan-banner .banner-inner {
  padding-left: 35px;
  padding-right: 35px;
}
.banner-box.insurance-banner-box {
  background: none;
}
.banner-box.insurance-banner-box img {
  width: 100%;
}
.banner-box.insurance-banner-box .banner-slider {
  padding-bottom: 0;
}
.banner-box.insurance-banner-box .banner-inner {
  padding-bottom: 0;
}
.banner-box.insurance-banner-box .slick-list {
  padding-bottom: 60px;
}
.banner-box.digital-gold-banner-box {
  background: none;
  padding-bottom: 0;
}
.banner-box.digital-gold-banner-box .banner-right img {
  max-width: 400px;
  margin: auto;
}
.banner-box.digital-gold-banner-box .banner-inner.new-banner-inner {
  padding-bottom: 0;
}
.banner-box.corporate-fd-banner-box {
  background-image: none;
  padding-bottom: 0;
}
.banner-box.corporate-fd-banner-box .banner-inner.new-banner-inner {
  padding-top: 0;
  display: flex;
  align-items: center;
  padding-bottom: 0;
}
.banner-box.corporate-fd-banner-box .banner-image .banner-caption {
  position: static;
}
.banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-left {
  width: 630px;
}
.banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-head {
  margin-bottom: 40px;
}
.banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-right {
  width: calc(100% - 630px);
}
.banner-box.iho-banner-box {
  background-image: none;
  padding-bottom: 0;
}
.banner-box.iho-banner-box .banner-right img {
  max-width: 300px;
  margin: 0 auto;
}
.banner-box.iho-banner-box .banner-inner.new-banner-inner {
  padding-top: 0;
  display: flex;
  align-items: center;
  padding-bottom: 0;
}
.banner-box.iho-banner-box .banner-image .banner-caption {
  position: static;
}
.banner-box.iho-banner-box .banner-image .banner-caption .banner-left {
  width: 630px;
  padding-right: 30px;
}
.banner-box.iho-banner-box .banner-image .banner-caption .banner-head {
  margin-bottom: 40px;
}
.banner-box.iho-banner-box .banner-image .banner-caption .banner-right {
  width: calc(100% - 630px);
}
.banner-box.loan-detail-banner {
  padding-bottom: 50px;
  background-image: none;
}
.banner-box.loan-detail-banner .banner-inner {
  padding-bottom: 0;
}
.banner-box.loan-detail-banner .banner-inner.new-banner-inner {
  padding-top: 0;
}
.banner-box.loan-detail-banner .banner-image .banner-caption {
  position: static;
}
.banner-box.loan-detail-banner .banner-image .banner-caption .banner-left {
  width: 630px;
}
.banner-box.loan-detail-banner .banner-image .banner-caption .banner-head {
  margin-bottom: 40px;
}
.banner-box.loan-detail-banner .banner-image .banner-caption .banner-right {
  width: calc(100% - 630px);
}
.banner-box.swp-banner {
  margin-top: 35px;
  background-image: none;
  padding-bottom: 0;
}
.banner-box.swp-banner .banner-inner.new-banner-inner {
  padding-bottom: 0;
}
.banner-box.swp-banner .banner-inner.new-banner-inner .banner-left {
  width: calc(100% - 520px);
  padding-top: 0px;
}
.banner-box.swp-banner .banner-inner.new-banner-inner .banner-right {
  width: 520px;
}
.banner-box.contact-us-banner-box {
  background-image: none;
}
.banner-box.contact-us-banner-box .banner-slider {
  padding-bottom: 0;
}
.banner-box.contact-us-banner-box .banner-inner.new-banner-inner {
  padding-top: 0;
  padding-bottom: 0;
}
.banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left {
  width: 630px;
  padding-top: 45px;
}
.banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left h1 {
  margin-bottom: 20px;
}
.banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left h5 {
  color: #606060;
}
.banner-box.contact-us-banner-box .banner-image .banner-caption .banner-head {
  margin-bottom: 40px;
}
.banner-box.contact-us-banner-box .banner-image .banner-caption .banner-right {
  width: calc(100% - 630px);
}

.banner-benefit-list {
  display: flex;
  flex-wrap: wrap;
  margin-top: 10px;
  margin-bottom: 15px;
}
.banner-benefit-list li {
  width: 50%;
  margin-bottom: 15px;
  font-size: 17px;
  line-height: 28px;
  color: #333333;
  padding-left: 18px;
  padding-right: 10px;
  position: relative;
}
.banner-benefit-list li:before {
  content: "";
  width: 8px;
  height: 8px;
  background-color: #ACB4D1;
  border-radius: 50%;
  position: absolute;
  top: 10px;
  left: 0;
}

.nps-banner-img {
  width: auto !important;
  margin: auto;
}

@media (max-width: 1279px) {
  .banner-box .slick-dots {
    bottom: 28px;
  }
  .banner-box .slider-arrows .slick-arrow {
    bottom: 7px;
    top: auto;
    width: 30px;
    height: 30px;
  }
  .banner-box .slider-arrows .slick-arrow:before {
    font-size: 10px;
  }
  .banner-box .slider-arrows .slick-prev {
    margin-left: -105px;
  }
  .banner-box .slider-arrows .slick-next {
    margin-right: -105px;
  }
  .banner-box .banner-left {
    padding-top: 35px;
    width: 505px;
  }
  .banner-box .banner-inner {
    padding-bottom: 54px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-arrow {
    bottom: -22px;
    top: auto;
    width: 40px;
    height: 40px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-arrow:before {
    font-size: 14px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-prev {
    margin-left: -105px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-next {
    margin-right: -105px;
  }
  .banner-box.mutual-fund-banner .banner-left {
    padding-top: 35px;
    width: 505px;
  }
  .banner-box.mutual-fund-banner.banner-heightinner {
    height: 433px;
  }
}
@media (max-width: 1199px) {
  .banner-box .banner-left {
    width: 406px;
  }
  .banner-box .banner-left .btn-blue {
    padding-left: 19px;
    padding-right: 19px;
  }
  .banner-box .banner-right {
    width: 420px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info {
    left: 20%;
    bottom: 14%;
  }
  .banner-box .banner-inner.new-banner-inner .banner-left {
    width: 470px;
  }
  .banner-box .banner-inner.new-banner-inner .banner-right {
    width: calc(100% - 470px);
  }
  .banner-box.mutual-fund-banner.banner-heightinner {
    height: 464px;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left {
    padding-top: 20px;
  }
}
@media (max-width: 1024px) {
  .banner-box .banner-left {
    width: 493px;
  }
}
@media (max-width: 991px) {
  .banner-box {
    padding-bottom: 20px;
  }
  .banner-box .slider-arrows .slick-arrow {
    bottom: 20px;
  }
  .banner-box .slick-dots {
    bottom: 41px;
  }
  .banner-box .banner-inner {
    padding-bottom: 78px;
  }
  .banner-box .banner-inner.new-banner-inner {
    padding-bottom: 50px;
  }
  .banner-box .banner-inner.new-banner-inner .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box .banner-inner.new-banner-inner .banner-left .banner-head {
    margin-bottom: 20px;
  }
  .banner-box .banner-inner.new-banner-inner .banner-right {
    width: 100%;
  }
  .banner-box .banner-inner.new-banner-inner {
    padding-bottom: 45px;
  }
  .banner-box.nps-banner .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.nps-banner .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box .banner-left {
    width: 360px;
    padding-right: 15px;
  }
  .banner-box .banner-left h1 {
    font-size: 26px;
    line-height: 32px;
    margin-bottom: 6px;
  }
  .banner-box .banner-left p {
    font-size: 14px;
    line-height: 22px;
    margin-bottom: 24px;
  }
  .banner-box .banner-left .tab-left-btn button {
    min-width: 140px;
  }
  .banner-box .banner-left .tab-left-btn .btn-blue {
    font-size: 11px;
    padding: 11px 15px;
  }
  .banner-box .banner-right {
    width: 335px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info {
    padding: 17px 14px;
    min-width: 122px;
    left: 60px;
    bottom: 13%;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info p {
    font-size: 12px;
    line-height: 15px;
    margin-bottom: 9px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info .text-equity {
    font-size: 8px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info.share-white {
    left: 0;
  }
  .banner-box.mutual-fund-banner {
    padding-bottom: 50px;
  }
  .banner-box.mutual-fund-banner.banner-heightinner {
    height: 620px;
  }
  .banner-box.mutual-fund-banner.no-banner-slider {
    padding-bottom: 50px;
  }
  .banner-box.creditCard-loan-banner .btn-wrap {
    margin-bottom: 15px;
  }
  .banner-box.creditCard-loan-banner .left-col, .banner-box.creditCard-loan-banner .right-col {
    width: 100%;
  }
  .banner-box.creditCard-loan-banner .banner-inner {
    padding-left: 0;
    padding-right: 0;
  }
  .banner-box.creditCard-loan-banner .banner-left {
    width: 100%;
    padding-top: 10px;
  }
  .banner-box.insurance-banner-box .new-banner-inner {
    padding-bottom: 0;
  }
  .banner-box.insurance-banner-box .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.insurance-banner-box .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box.digital-gold-banner-box {
    padding-bottom: 30px;
  }
  .banner-box.digital-gold-banner-box .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.digital-gold-banner-box .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box.digital-gold-banner-box .banner-slider {
    padding-bottom: 0;
  }
  .banner-box.corporate-fd-banner-box {
    padding-bottom: 0;
  }
  .banner-box.corporate-fd-banner-box .banner-slider {
    padding-bottom: 0;
    padding-top: 0;
  }
  .banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box.iho-banner-box {
    padding-bottom: 0;
  }
  .banner-box.iho-banner-box .banner-slider {
    padding-bottom: 0;
    padding-top: 0;
  }
  .banner-box.iho-banner-box .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.iho-banner-box .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box.loan-detail-banner .banner-inner {
    padding-bottom: 0;
  }
  .banner-box.loan-detail-banner .tab-left-btn {
    text-align: center;
  }
  .banner-box.loan-detail-banner .tab-left-btn .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.loan-detail-banner .banner-image .banner-caption .banner-left {
    width: 450px;
  }
  .banner-box.loan-detail-banner .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }
  .banner-box.swp-banner {
    padding-bottom: 0;
    margin-top: 10px;
  }
  .banner-box.swp-banner .banner-inner.new-banner-inner .banner-left {
    width: 100%;
    padding-bottom: 25px;
  }
  .banner-box.swp-banner .banner-inner.new-banner-inner .banner-right {
    margin: auto;
    width: 100%;
  }
  .banner-box.swp-banner .btn-wrap {
    text-align: center;
  }
  .banner-box.swp-banner .btn-wrap .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left {
    width: 450px;
    padding-right: 30px;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-right {
    width: calc(100% - 450px);
  }

  .banner-heightinner {
    height: 350px;
  }

  .banner-benefit-list li {
    font-size: 14px;
    line-height: 22px;
  }
  .banner-benefit-list li:before {
    top: 8px;
  }
}
@media (max-width: 767px) {
  .banner-box {
    background: none;
  }
  .banner-box .banner-inner {
    display: block;
    padding-top: 20px;
    padding-bottom: 71px;
  }
  .banner-box .slick-dots {
    bottom: 25px;
  }
  .banner-box .slider-arrows .slick-arrow {
    bottom: 5px;
  }
  .banner-box .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 15px;
    padding-bottom: 20px;
    text-align: left;
  }
  .banner-box .banner-left p {
    font-size: 13px;
    line-height: 22px;
    margin-bottom: 19px;
  }
  .banner-box .banner-left .tab-left-btn {
    margin: 0;
    display: flex;
    justify-content: center;
  }
  .banner-box .banner-left .tab-left-btn .btn-blue.btn-blue-outline {
    min-width: inherit;
    padding-left: 0px;
    padding-right: 0px;
  }
  .banner-box .banner-right {
    width: 330px;
    margin: auto;
  }
  .banner-box .banner-slider .banner-slide-inner {
    padding-top: 0px;
  }
  .banner-box .banner-slider .banner-slide-inner img {
    margin: auto;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info {
    min-width: 115px;
    padding: 14px 14px 22px;
    left: 23%;
    bottom: 18%;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info.share-white {
    left: 0px;
    top: -30px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info .share-icons {
    width: 24px;
    height: 24px;
    font-size: 10px;
    margin-bottom: 5px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info p {
    font-size: 12px;
    line-height: 17px;
    margin-bottom: 7px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info .text-equity {
    font-size: 8px;
    line-height: 12px;
    padding: 3px 13px;
  }
  .banner-box .banner-inner.new-banner-inner .banner-left {
    padding-top: 15px;
  }
  .banner-box .banner-inner.new-banner-inner .sip-cards p {
    margin-bottom: 20px;
  }
  .banner-box .banner-inner.new-banner-inner .sip-cards .sip-card-btn {
    margin-bottom: 15px;
  }
  .banner-box.mutual-fund-banner .banner-slider {
    padding-top: 20px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-arrow {
    width: 30px;
    height: 30px;
    bottom: -12px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-arrow:before {
    font-size: 10px;
    line-height: 14px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-prev {
    margin-left: -85px;
  }
  .banner-box.mutual-fund-banner .slider-arrows .slick-next {
    margin-right: -85px;
  }
  .banner-box.mutual-fund-banner.banner-heightinner {
    height: 966px;
  }
  .banner-box.nps-banner .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box.nps-banner .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.nps-banner .banner-image .banner-caption .banner-right img {
    max-height: 290px;
    margin: auto;
    object-fit: contain;
  }
  .banner-box.nps-banner .btn-blue-outline {
    border: 1px solid #2C6EB5;
    display: block;
    width: 100%;
    max-width: 265px;
    padding: 13px 15px !important;
  }
  .banner-box.creditCard-loan-banner {
    background-image: none;
  }
  .banner-box.creditCard-loan-banner .banner-slider {
    padding-top: 0;
  }
  .banner-box.creditCard-loan-banner .banner-inner {
    padding-top: 0;
  }
  .banner-box.creditCard-loan-banner h5 {
    font-size: 14px;
    line-height: 21px;
  }
  .banner-box.creditCard-loan-banner .banner-benefit-list li {
    font-size: 12px;
    line-height: 18px;
    padding-left: 14px;
    width: 50%;
  }
  .banner-box.creditCard-loan-banner .banner-benefit-list li:before {
    width: 6px;
    height: 6px;
    top: 6px;
  }
  .banner-box.creditCard-loan-banner .btn-wrap {
    text-align: center;
  }
  .banner-box.creditCard-loan-banner .btn-blue {
    width: 100%;
    max-width: 265px;
    padding: 14px 25px;
  }
  .banner-box.creditCard-loan-banner .sip-card-btn .btn-blue {
    max-width: 130px;
  }
  .banner-box.insurance-banner-box {
    padding-bottom: 40px;
  }
  .banner-box.insurance-banner-box .banner-slider {
    padding-top: 0;
  }
  .banner-box.insurance-banner-box p {
    font-size: 14px;
    line-height: 21px;
    color: #2C6EB5;
    font-weight: 600;
  }
  .banner-box.insurance-banner-box .banner-left .tab-left-btn {
    text-align: center;
  }
  .banner-box.insurance-banner-box .banner-left .tab-left-btn .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.insurance-banner-box .banner-left .tab-left-btn .btn-blue.btn-blue-outline {
    border: 1px solid #2C6EB5;
  }
  .banner-box.insurance-banner-box .btn-wrap {
    text-align: center;
  }
  .banner-box.insurance-banner-box .btn-wrap .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.insurance-banner-box .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box.insurance-banner-box .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.insurance-banner-box .banner-image .banner-caption .banner-right img {
    max-height: 290px;
    margin: auto;
    object-fit: contain;
  }
  .banner-box.digital-gold-banner-box .banner-slider {
    padding-top: 0;
  }
  .banner-box.digital-gold-banner-box h6 {
    font-size: 14px;
    line-height: 21px;
    margin-bottom: 5px;
  }
  .banner-box.digital-gold-banner-box .btn-wrap {
    text-align: center;
  }
  .banner-box.digital-gold-banner-box .btn-wrap .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.digital-gold-banner-box .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 25px;
  }
  .banner-box.digital-gold-banner-box .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.digital-gold-banner-box .banner-image .banner-caption .banner-right img {
    margin: auto;
    object-fit: contain;
  }
  .banner-box.corporate-fd-banner-box .banner-slider {
    padding-top: 0;
    padding-bottom: 40px;
  }
  .banner-box.corporate-fd-banner-box h6 {
    font-size: 14px;
    line-height: 21px;
  }
  .banner-box.corporate-fd-banner-box .btn-wrap {
    text-align: center;
  }
  .banner-box.corporate-fd-banner-box .btn-wrap .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.corporate-fd-banner-box .banner-left p {
    font-size: 14px;
  }
  .banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.corporate-fd-banner-box .banner-image .banner-caption .banner-right img {
    max-width: 220px;
    margin: auto;
    object-fit: contain;
  }
  .banner-box.iho-banner-box {
    margin-bottom: 40px;
  }
  .banner-box.iho-banner-box .banner-slider {
    padding-top: 0;
  }
  .banner-box.iho-banner-box .btn-wrap {
    text-align: center;
  }
  .banner-box.iho-banner-box .btn-wrap .btn-blue {
    width: 100%;
    max-width: 265px;
  }
  .banner-box.iho-banner-box .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box.iho-banner-box .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.iho-banner-box .banner-image .banner-caption .banner-right img {
    max-width: 155px;
    margin: auto;
    object-fit: contain;
  }
  .banner-box.loan-detail-banner {
    padding-bottom: 30px;
  }
  .banner-box.loan-detail-banner .banner-inner {
    padding-bottom: 40px;
  }
  .banner-box.loan-detail-banner .banner-slider {
    padding-top: 0;
  }
  .banner-box.loan-detail-banner .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
    padding-bottom: 15px;
  }
  .banner-box.loan-detail-banner .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.loan-detail-banner .banner-image .banner-caption .banner-right img {
    max-height: 290px;
    margin: auto;
    object-fit: contain;
  }
  .banner-box.swp-banner {
    margin-top: 25px;
  }
  .banner-box.swp-banner .banner-slider {
    padding-top: 0;
  }
  .banner-box.about-us-banner {
    margin-top: 0;
  }
  .banner-box.contact-us-banner-box {
    padding-bottom: 35px;
  }
  .banner-box.contact-us-banner-box .banner-slider {
    padding-top: 0;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left {
    width: 100%;
    padding-right: 0px;
    padding-top: 10px;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left h1 {
    margin-bottom: 10px;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-left h5 {
    font-size: 12px;
    line-height: 18px;
    color: #333333;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-right {
    width: 100%;
  }
  .banner-box.contact-us-banner-box .banner-image .banner-caption .banner-right img {
    max-width: 220px;
    margin: auto;
    object-fit: contain;
  }

  .banner-image .banner-image-outer img {
    width: 100%;
  }
  .banner-image .banner-caption {
    bottom: auto;
  }
  .banner-image .banner-caption-inner {
    padding: 20px 0px 80px;
    width: 100%;
  }
  .banner-image .banner-caption-inner:before {
    content: "";
    width: 5000px;
    height: 100%;
    right: -5000px;
    background-color: rgba(0, 88, 153, 0.9);
    position: absolute;
    top: 0px;
    display: block;
  }

  .banner-heightinner {
    height: 460px;
  }

  .banner-benefit-list li {
    font-size: 13px;
    width: 100%;
    margin-bottom: 12px;
  }
  .banner-benefit-list li::before {
    width: 6px;
    height: 6px;
  }
}
@media (max-width: 359px) {
  .banner-box .slick-dots {
    bottom: 30px;
  }
  .banner-box .slider-arrows .slick-arrow {
    bottom: 9px;
  }
  .banner-box .banner-inner {
    padding-bottom: 75px;
  }
  .banner-box .banner-right {
    width: 290px;
  }
  .banner-box .banner-slider .banner-slide-inner .banner-share-info.share-white {
    top: -50px;
  }
  .banner-box .banner-left .tab-left-btn .btn-blue {
    padding-left: 10px;
    padding-right: 10px;
    min-width: inherit;
    margin-left: 3px;
    margin-right: 3px;
  }

  .banner-heightinner {
    height: 464px;
  }
}

/*# sourceMappingURL=style.css.map */
