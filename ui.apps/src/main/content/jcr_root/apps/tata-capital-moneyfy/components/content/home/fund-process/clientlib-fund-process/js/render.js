(function (_global) {
    var fundProcessRenderFnObj = (function (jsHelper) {
        var fundProcessRenderObj = {};
        return jsHelper.freezeObj(fundProcessRenderObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundProcessRenderObj', fundProcessRenderFnObj);
})(this || window || {});