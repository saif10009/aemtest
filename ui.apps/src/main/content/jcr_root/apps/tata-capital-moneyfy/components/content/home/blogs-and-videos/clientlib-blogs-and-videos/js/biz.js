/*Blogs & Videos js*/
(function (_global) {
  var blogsAndVideosBizObj = (function (jsHelper) {
    var blogsAndVideosObj = {};

    document.addEventListener("DOMContentLoaded", function () {
      initializeblogsAndVideos();
      adobeAnalytics();

    });

    function initializeblogsAndVideos() {
      /*blog slider js*/
      $(".jsBlogSlider").slick({
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 1024,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              variableWidth: true,
            },
          },
        ],
      });
      /*blog slider js*/

      /*video slider js*/
      if ($(".jsVideoSlider .blog-slider-col").length >= 2) {
        $(".jsVideoSlider").slick({
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: true,
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 992,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              },
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                variableWidth: true,
              },
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                variableWidth: true,
              },
            },
          ],
        });
      }
        
      if (screen.width > 760 && screen.width < 992) {
        try {
          if (document.querySelector('.jsVideoSlider').slick.getSlick().slideCount == 1 || document.querySelector('.jsVideoSlider').slick.getSlick().slideCount == 2) {
            document.querySelector('.jsVideoSlider .slick-dots').remove()
          }
        } catch (error) {
          console.log('not found', error);
        }
      }

      $('.jsTooltopLink').click(function(){
        $(this).siblings('.tooltip-box').fadeIn();    
        setTimeout (function(){
          $('.tooltip-box').fadeOut();
        }, 2000);
      })  

    }
    function adobeAnalytics() {
      var videoDataObj = {}

      $('[data-target="#share-modal"]').click(function () {
        videoDataObj.videoLink = $(this).parents('.blog-slider-col').data("video-link");
        videoDataObj.videoText = $(this).parents('.blog-slider-col').find('.blog-content p').text();
        $('.copy-links input').val($(this).parents('.blog-slider-col').data("video-link"));
        
      })
      $('[data-id="blogRedirect"]').click(function (event) {
        event.preventDefault();
        var title = $(this).parents(".blog-sliders").find(".blog-content").find("p").text().trim();
        blogItemView(title, 'blogs-and-videos', userIdObj.userId)
        window.open($(this).attr('href'));
      })
      $('[data-id="videoRedirect"]').click(function (event) {
        event.preventDefault();
        var title = $(this).parents(".blog-sliders").find(".blog-content").find("p").text().trim();
        //videoItemClick(title, 'blogs-and-videos', userIdObj.userId);
        allCTAInteraction('share icon', '', 'blogs-and-videos', userIdObj.userId)
        location.href = $(this).attr('href')
      })
      $('.play-btn').click(function(){
        window.open($(this).parents('.blog-slider-col').data("video-link"))
      })
      $('[data-id-co]').click(function (event) {
        event.preventDefault();
        var checkVal = event.currentTarget.attributes[3].nodeValue;
        if (checkVal == 'blogBtn') {
          var ctaTitle = $(this).parents().find('[data-blog-heading]').data('blog-heading');
          var ctaText = $(this).text().trim();
          allCTAInteraction(ctaText, ctaTitle, 'blogs-and-videos', userIdObj.userId)
          location.href = $(this).attr('href')
        } else {
          var ctaTitle = $(this).parents().find('[data-video-heading]').data('video-heading');
          var ctaText = $(this).text().trim();
          allCTAInteraction(ctaText, ctaTitle, 'blogs-and-videos', userIdObj.userId)
          location.href = $(this).attr('href')
        }
      })


      $('[data-copybtn]').click(function (event) {
        event.preventDefault()
        var ctaText = $(this).text().trim();
        allCTAInteraction(ctaText, 'Page link', 'blogs-and-videos', userIdObj.userId);
        $(this).parents('.copy-links').find('input').select();
        document.execCommand("copy");
        // navigator.clipboard.writeText($(this).parents('.copy-links').find('input').val());
        // navigator.clipboard.write($(this).parents('.copy-links').find('input').val());
      })
      $('[data-shareicon]').click(function (event) {
        event.preventDefault()
        if ($(this).data('shareicon') == 'facebook') {
          allCTAInteraction('Facebook Icon', 'Share with', 'blogs-and-videos', userIdObj.userId)
          var facebookWindow = window.open(
            "https://www.facebook.com/sharer/sharer.php?hashtag=" +
            videoDataObj.videoText +
            "&" +
            "u=" +
            videoDataObj.videoLink,
            "facebook-popup",
            "height=350,width=600"
          );
          if (facebookWindow.focus) {
            facebookWindow.focus();
          }
        } else if ($(this).data('shareicon') == 'twitter') {
          allCTAInteraction('Twitter Icon', 'Share with', 'blogs-and-videos', userIdObj.userId)
          var twitterWindow = window.open(
            "https://twitter.com/share?text=" + (videoDataObj.videoText).replace('|','%7C') + "&" + "url=" + videoDataObj.videoLink
          );
          if (twitterWindow.focus) {
            twitterWindow.focus();
          }
        } else if ($(this).data('shareicon') == 'whatsapp') {
          allCTAInteraction('Whatsapp Icon', 'Share with', 'blogs-and-videos', userIdObj.userId)
          location.href = $(this).attr('href')
        } else {
          allCTAInteraction('Youtube Icon', 'Share with', 'blogs-and-videos', userIdObj.userId)
          location.href = $(this).attr('href')
        }
      })
    }

    return jsHelper.freezeObj(blogsAndVideosObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "blogsAndVideosBizObj", blogsAndVideosBizObj);
})(this || window || {});
  /*Blogs & Videos js*/
