$(document).ready(function () {
    // aplhabet input validation
    $('.only-alpha-input').keypress(function (e) {
        if (String.fromCharCode(e.keyCode).match(/[^a-zA-Z ]/g)) {
            return false;
        }
    });
    if ($('.single-select2').is(":visible")) {
        $('.single-select2').select2({
            minimumResultsForSearch: -1
        });

        $('.nps-select').on('change', function () {
            var data = $(this).find("option:selected").text();
            console.log(data);
            if (data != 'Select') {
                $(this).parents('.feature-wrap').find('.feature-item-wrap').removeClass('hide-data');
            } else {
                $(this).parents('.feature-wrap').find('.feature-item-wrap').addClass('hide-data');
            }
        });
    }

    /*19-10-2022*/
    $('#credit-card-form .js-Selectsingle').on('change', function () {
        $(this).parents('.form-group').removeClass('textboxerror');
        $(this).parents('.form-group').find('.error-msgs').remove();
    });
    /*19-10-2022*/

    $('#credit-card-form .js-proceed-btn').click(function (e) {
        /*23-12-2022*/
        var ele_input = $('#credit-card-form .input-textbox:visible');
        var selectElements = $('#credit-card-form .select-input:visible');
        /*23-12-2022*/
        var errors = [];
        allFilled = true;
        var ele_required = "Field is required";

        /*23-12-2022*/
        $(selectElements).each(function () {
            var select = $(this);
            if ($(select).text() == 'Select') {
                allFilled = false;
                $(select).parents('.form-group').addClass('textboxerror');
                $(select).next('.error-msgs').remove();
                $(select).after('<span class="error-msgs" style="top: 100%;">' + ele_required + '</span>');
                errors.push(ele_required);
            } else {
                $(select).parents('.form-group').removeClass('textboxerror');
                $(select).next('.error-msgs').remove();
            }
        });
        /*23-12-2022*/

        $(ele_input).each(function () {
            var element = $(this);
            var ele_value = element.val();
            var ele_name = "Please enter name as pan card";
            var ele_email = "Please enter valid email";
            var ele_phoneNumber = "Please enter valid number";
            var ele_pinCode = "Please enter valid PIN Code";

            $(element).next().remove();

            if (element.is(":visible")) {
                if (element.val() != '') {
                    $(element).after('<span class="error-msgs"></span>');
                    if ($(element).data('type') === 'name') {
                        var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

                        if (ele_value != '' && !regName.test(ele_value)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_name);
                            errors.push(ele_name);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                        }
                    }

                    if ($(element).data('type') === 'mobile') {

                        if (!validateMobile(element)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_phoneNumber);
                            errors.push(ele_phoneNumber);
                        } else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                        }
                    }

                    if ($(element).data('type') === 'email') {
                        var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

                        if (ele_value != '' && !ele_value.match(regEmail)) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_email);
                            errors.push(ele_email);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                        }
                    }
                    if ($(element).data('type') === 'city') {

                        if (ele_value === '' && document.querySelector('.select-input').innerText == 'Select') {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_required);
                            errors.push(ele_required);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next().text('');
                        }
                    }
                    if ($(element).data('type') === 'pincode') {
                        if (ele_value != '' && ele_value.length != 6) {
                            $(element).parents('.textbox-box').addClass('textboxerror');
                            $(element).next('.error-msgs').text(ele_pinCode);
                            errors.push(ele_pinCode);
                        }
                        else {
                            $(element).parents('.textbox-box').removeClass('textboxerror');
                            $(element).next('.error-msgs').text('');
                        }
                    }


                } else {
                    $(element).parents('.textbox-box').addClass('textboxerror');
                    $(element).after('<span class="error-msgs">' + ele_required + '</span>');
                    errors.push(ele_required);
                }
            }
        });


        if (errors.length == 0) {
            var City;
            if (document.querySelector('[data-city="manualType"] input').value !== "") {
                City = document.querySelector('[data-city="manualType"] input').value;
            } else {
                City = $('[data-city="optionsType"]').find('button').text();
            }
            apiCall.iciciCardApiObj = {
                name: $('[data-type="name"]').val(),
                mobileNumber: $('[data-type="mobile"]').val(),
                city: City,
                pinCode: $('[data-type="pincode"]').val(),
                emailId: $('[data-type="email"]').val(),
                subsource: apiCall.getSubSource.subsource ? apiCall.getSubSource.subsource : 'null',
                productName: 'Tata Cards',
                productCode: 'Tata Cards'
            }
            apiCall.generateOTP();
        }
    });

    $('#credit-card-form .input-textbox').keyup(function () {
        var element = $(this);
        var ele_value = element.val();
        var ele_required = "Field is required";
        var ele_name = "Please enter name as pan card";
        var ele_email = "Please enter valid email";
        var ele_phoneNumber = "Please enter valid number";
        var ele_pinCode = "Please enter valid PIN Code";

        $(this).next('.error-msgs').remove();
        $(this).after('<span class="error-msgs"></span>');

        if ($(element).val() != '') {
            if ($(element).data('type') === 'name') {
                var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

                if (ele_value != '' && !regName.test(ele_value)) {
                    $(element).parents('.form-textbox').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_name);
                }
                else {
                    $(element).parents('.form-textbox').removeClass('textboxerror');
                    $(element).next().text('');
                    $(this).next('.error-msgs').remove();
                }
            }

            if ($(element).data('type') === 'mobile') {
                if (!validateMobile(element)) {
                    $(element).parents('.form-textbox').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_phoneNumber);
                } else {
                    $(element).parents('.form-textbox').removeClass('textboxerror');
                    $(element).next().text('');
                    $(this).next('.error-msgs').remove();
                }
            }
            if ($(element).data('type') === 'email') {
                var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

                if (ele_value != '' && !regEmail.test(ele_value)) {
                    $(element).parents('.form-textbox').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_email);
                }
                else {
                    $(element).parents('.form-textbox').removeClass('textboxerror');
                    $(element).next().text('');
                    $(this).next('.error-msgs').remove();
                }
            }
            if ($(element).data('type') === 'pincode') {
                if (ele_value != '' && ele_value.length != 6) {
                    $(element).parents('.textbox-box').addClass('textboxerror');
                    $(element).next('.error-msgs').text(ele_pinCode);
                }
                else {
                    $(element).parents('.textbox-box').removeClass('textboxerror');
                    $(element).next('.error-msgs').text('');
                }
            }
        } else {
            $(element).parents('.form-textbox').addClass('textboxerror');
            $(element).next('.error-msgs').text(ele_required);
        }
    });



    $('#credit-otp .js-resendOTP').click(function () {
        apiCall.generateOTP();
        if (document.querySelectorAll('body .modal-backdrop').length > 1) {
            var backdropContainer = document.querySelector('body .modal-backdrop');
            backdropContainer.remove();
        }
        $(this).parents('#credit-otp').find('.js-OtpBox .input-textbox').val('').parents('.form-group').removeClass('active');
        $(this).parents('#credit-otp').find('.js-OtpBox .input-textbox:first-child').focus();
        $(this).parents('#credit-otp').find('button').addClass('btn-disabled');
        $('#credit-otp .input-error').addClass('d-none');
        $('#credit-otp .label-name').removeClass('d-none');
    });

    $("#credit-otp .js-OtpBox .input-textbox").keyup(function () {

        $(this).parents('.form-group').removeClass('textboxerror');

        if (this.value.length == this.maxLength) {
            $(this).next('.input-textbox').focus();
            $(this).next('.input-textbox').removeClass('pointer-none');
        } else {
            $(this).prev('.input-textbox').focus();
            $(this).addClass('pointer-none');
            $('#credit-otp .input-textbox:first').removeClass('pointer-none');
        }

        $(this).parents('.form-group').addClass('active');
        var ele_input = $('.js-OtpBox .input-textbox');

        $(ele_input).each(function () {
            if ($(this).val().length != 0) {
                $(this).parents('#credit-otp').find('.jsCreditSubmitOtp').removeClass('btn-disabled');
                $(this).parents('.form-group').addClass('active');
            }
            else {
                $(this).parents('#credit-otp').find('.jsCreditSubmitOtp').addClass('btn-disabled');
            }
        });

    }).focus(function () {
        $('.error-msgs').remove();
        $(this).parents('.form-group').removeClass('textboxerror');
    }).blur(function () {
        if ($(this).val().length != 0) {
            $(this).parents('.form-group').addClass("active");
        } else if ($(this).val().length == 0) {
            $(this).parents('.form-group').removeClass("active");
        }
    });

    $('.js-CrediteditNumber').click(function () {
        document.querySelectorAll('.js-OtpBox input').forEach(function (inp) {
            if (inp.value != '')
                inp.value = '';
        });
        $('#credit-card-modal').removeClass('popover-show');
        $('#credit-card-modal').removeAttr('style');
        $('body').removeClass('popover-modal-open');
        $('.modal-backdrop').remove();
    })

    $('.jsCreditSubmitOtp').click(function () {
        apiCall.verifyOTP();
        document.querySelectorAll('.js-OtpBox input').forEach(function (inp) {
            if (inp.value != '')
                inp.value = '';
        });
    })

    $('.jsCreditCardClear').click(function () {
        document.querySelectorAll('.js-OtpBox input').forEach(function (inp) {
            if (inp.value != '')
                inp.value = '';
        });
        $('[data-form="creditcard-thanks"]').addClass('d-none');
        $('[data-form="creditcard-otp"]').removeClass('d-none');
        $('#credit-card-form .input-textbox').val('');
        /*19-10-2022*/
        $('#credit-card-form .js-Selectsingle').val('').trigger('change');
        /*19-10-2022*/
        /*23-12-2022*/
        $('#credit-card-form .jsCityDropdown .select-input').text('Select');
        $('#credit-card-form .jsSearchVali li').removeClass('active');
        $('[data-city="optionsType"]').removeClass('hidden');
        $('[data-city="manualType"]').addClass('hidden');
        $('.js-OtpBox .input-textbox').val('');
        $('.jsCreditSubmitOtp').addClass('btn-disabled');
        /*23-12-2022*/
    })

})

$('.jsSearchVali li').click(function () {
    $('.jsSearchVali li').removeClass('active')
    $(this).addClass('active');
    var selectVar = $('.jsSearchVali li.active a').html();
    $('.jsCityDropdown').removeClass('show');
    $('.jsCityDropdown .select-input').html(selectVar);
    $('.textbox-search input').val('');
    $('.jsSearchVali li').removeAttr('style');
    $('#credit-card-form .select-input:visible').parents('.form-group').removeClass('textboxerror');
    $('#credit-card-form .select-input:visible').next('.error-msgs').remove();
})

$('.jsAddManually').click(function () {
    $('[data-city]').addClass('hidden');
    $('[data-city="manualType"]').removeClass('hidden').addClass('jstext');
    $('[data-city="manualType"]').find('.input-textbox').focus();
    $('[data-city="optionsType"] #searchCity').val('');

    document.querySelector('[data-city="manualType"] input').addEventListener('keyup', function () {
        if (document.querySelector('[data-city="manualType"] input').value !== '') {
            var validCity = /^[a-zA-Z0-9 _]*$/;
            var manualCityAdd = document.querySelector('[data-city="manualType"] input').value
            if (!validCity.test(manualCityAdd)) {
                document.querySelector('[data-city="manualType"] .error-msgs').innerText = 'Invalid City Name'
            }
        }
    })
})

$("#searchCity").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    if (value) {
        $("#searchCityList li").filter(function () {
            $(this).toggle($(this).text().toLowerCase().includes(value))
        });
    }
    if ($(this).val() == '') {
        $("#searchCityList li").removeAttr('style');
        $(".textbox-search").find('.error-msgs').remove();
    }
});

$(document).on("click", function (event) {
    var $trigger = $(".close-on-outside");
    if ($trigger !== event.target && !$trigger.has(event.target).length) {
        $('.jsCityDropdown').removeClass('show');
        $('#searchCity').val('');
        $('.jsSearchVali li').removeAttr('style');
    }
    $('#searchCity').click(function (e) {
        e.stopPropagation();
    })
    $('.jstext input').blur(function () {
        if ($('.jstext input').val() == '') {
            $('[data-city="manualType"]').addClass('hidden');
            $('[data-city="optionsType"]').removeClass('hidden');
            $('#searchCityList li').removeAttr('style');
        }
    })
});

$('.jsCityDropdown .select-input').click(function () {
    document.querySelector('.jsCityDropdown').classList.toggle('show');
    if (document.querySelector('.jsCityDropdown').classList.contains('show')) {
        document.querySelectorAll('#searchCityList li').forEach(function (city) {
            city.addEventListener('click', function (e) {
                var selectedCity = e.currentTarget.innerText;
                document.querySelector('.jsCityDropdown button').innerHTML = selectedCity;
                if (selectedCity) {
                    document.querySelector('.jsCityDropdown').classList.remove('show');
                    document.querySelectorAll('.error-msgs').forEach(function (err) {
                        if (err.parentElement.classList.contains('jsCityDropdown')) {
                            err.remove();
                        }
                    })
                }
            })
        })
    }
})

// REgex for mobile //
function validateMobile(mobileField) {
    var re = /^[6-9]\d{9}$/i;
    var check = re.test($(mobileField).val());
    if ($(mobileField).val().length != 10 || !check) {
        return false;
    } else {
        return true;
    }
}
