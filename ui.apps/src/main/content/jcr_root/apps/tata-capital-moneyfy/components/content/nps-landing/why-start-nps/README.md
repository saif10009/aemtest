Why Start NPS
====
Why Start NPS component written in HTL, used to provide different reasons why we should start NPS.

## Feature
* This is a multifield component.
* We have used three tabs to differentiate between NPS PPF and Fixed Deposits.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./mainHeading` Used for rendering title of the component.
2. `./reasons` Used tab which defines all the reasons.
3. `./reasonsList` Used to create multifield in component.
4. `./heading` Used for rendering heading of card on page.
5. `./selectHeight` Used as a dropdown to select the height.
6. `./heading` Used to render the reasons.
7. `./benefitsofNps` Used as first tab in which benefits of NPS are described.
8. `./headingNps` Used to render main heading for first tab.
9. `./benefitsofNpsList` Used to create multifield in component.
10. `./headingNps` Used to describe benefits of NPS.
11. `./benefitsofNpsDescription` Used to provide some information about NPS.
12. `./benefitsofPpf`Used as second tab in which benefits of PPF are described.
13. `./headingPpf`Used to render main heading for second tab.
14. `./benefitsofPpfList` Used to create multifield in component.
15. `./headingPpf` Used to describe benefits of PPF.
16. `./benefitsofPpfDescription`Used to provide some information about PPF.
12. `./benefitsofFixedDeposits`Used as third tab in which benefits of fixed deposits are described.
13. `./headingFixedDeposit`Used to render main heading for third tab.
14. `./benefitsofFixedDepositList` Used to create multifield in component.
15. `./headingFixedDeposits` Used to describe benefits of PPF.
16. `./benefitsofFixedDepositesDescription`Used to provide some information about Fixed Deposits.


## Client Libraries 
The component provides a `moneyfy.why-start-nps` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.nps-landing-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-nps-landing.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5