/*fd calculator render js start*/
(function (_global) {
    var fdCalculatorRenderFn = (function (jsHelper) {
        var fdCalculatorRenderObj = {}

        function renderfdCalculation(e, fdInputObj) {
            var fdVal = document.getElementById("fdVal");
            var totalInvestment = document.getElementById("totalInvestment");
            var totalEarned = document.getElementById("totalEarned");
            var resultText = document.getElementById('starts-investing-text');
            if ((isNaN(fdInputObj.fdInput)) || (fdInputObj.fdInput < 5000) || (fdInputObj.fdInput > 10000000)) {
                fdVal.innerHTML = '₹ 0';
                totalInvestment.innerHTML = '₹ 0';
                totalEarned.innerHTML = '₹ 0';
                resultText.innerHTML = "If you invest ₹ 0 today by "+e.data.fdMonths+" you will get";
                fdInputObj.fdHighchartReact(0, 0, 0)
            }
            else {
                fdVal.innerHTML =
                    "₹ " + e.data.fd.toLocaleString('en-IN');
                totalInvestment.innerHTML = '₹ ' + fdInputObj.fdInput.toLocaleString('en-IN');
                totalEarned.innerHTML = '₹ ' + (e.data.fd - fdInputObj.fdInput).toLocaleString('en-IN');
                resultText.innerHTML = "If you invest ₹ " + fdInputObj.fdInput.toLocaleString('en-IN') + " today by "+e.data.fdMonths+" you will get";
                fdInputObj.fdHighchartReact(fdInputObj.fdInput, (e.data.fd - fdInputObj.fdInput), 100)
            }
        }

        fdCalculatorRenderObj.renderfdCalculation = renderfdCalculation;
        return jsHelper.freezeObj(fdCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fdCalculatorRenderObj", fdCalculatorRenderFn)
})(this || window || {});
  /*fd calculator render js end*/