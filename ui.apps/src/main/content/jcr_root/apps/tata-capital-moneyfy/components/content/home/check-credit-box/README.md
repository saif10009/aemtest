Check Credit Box
====
This component witten in HTL, allowing to render check credit box on page.

## Feature
* This is authorable component.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./image` - Used to render image for component on page.
3. `./heading` - Used to render heading of component.
4. `./description` - Used to render description of componet.
5. `./buttonText ,./button-link ,./new-tab` - Consider button title its redirection link.
 

## Client Libraries
The component provides a `moneyfy.check-credit-box` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5