/* Why-Moneyfy tab js start*/
(function(_global) {
    var whyMoneyfyBizObj = (function(jsHelper) {
        var whyMoneyfyObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            whymoneyfySlick();
        });

        function whymoneyfySlick() {
            if ($(window).width() > 767) {
                if ($('#whyMonefySlider').hasClass('slick-initialized')) {
                    $('#whyMonefySlider').slick('unslick');
                }
            } else {
                $('#whyMonefySlider').not('.slick-initialized').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true,
                    responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 360,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }
                    ]
                });
            }
        }
        return jsHelper.freezeObj(whyMoneyfyObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "whyMoneyfyBizObj", whyMoneyfyBizObj)
})(this || window || {});
/* Why-Moneyfy tab js end*/