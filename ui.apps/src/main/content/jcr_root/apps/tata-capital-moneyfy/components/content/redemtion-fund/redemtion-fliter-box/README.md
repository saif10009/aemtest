Redemtion filter box
====
The `Redemtion filter box` component is for adding Choose from amc component on moneyfy mututal funds page.



## Feature
* It is an static component.



## Edit Dialog Properties
This is an static component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.redemtion-fliter-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML. `moneyfy.jquery`,`moneyfy.aos`,`moneyfy.slick` these dependancies libraries are use for load jquery,aos and slick file in component.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5