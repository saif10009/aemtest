/*header js*/
(function (_global) {
    var headerBizObj = (function (jsHelper) {
        var headerBizObj = {};

        document.addEventListener("DOMContentLoaded", function () {
            if (!getUserObj().isLoggedIn) {
                headerRenderObj.checkUserLogin(true);
            } else {
                headerRenderObj.checkUserLogin(false);
                initializeLoginHeaderEvents(getUserObj().userData);
            }

            var pathName = window.location.href;
            var pathArray = pathName.split('/');
            pathArray.splice(0,3);
            var siteSection;
            var siteSubSection;
            if(pathArray.length == 4){
                siteSection = pathArray[0] + ':' + pathArray[1];
                siteSubSection = pathArray[pathArray.length-2] + ':' + pathArray.pop().replace('.html',''); 
            }else if(pathArray.length == 3){
                siteSection = pathArray[0] + ':' + pathArray[1];
                siteSubSection = pathArray.pop().replace('.html','');
            }else if(pathArray.length == 2){
                siteSection = pathArray[0];
                siteSubSection = pathArray.pop().replace('.html','');
            }else if(pathArray.length == 1){
                siteSection = pathArray[0];
                siteSubSection = pathArray[0];
            }
            if(pathArray)
            var pageName = pathName.split('/').pop().replace('.html','');
            if (getUserObj().isLoggedIn) {
                pageInitialization(pageName, siteSection, siteSubSection, 'pageType', pathName, 'Logged In', userIdObj.userId)
            } else {
                pageInitialization(pageName, siteSection, siteSubSection, 'pageType', pathName, 'Not Logged In', userIdObj.userId)
            }

            initializeHeader();
            adobeAnalytics();

            /* fund search js initialize start */
            $('[data-id="SearchResultDesktop"]').on('input', function () {
                var inputForDesktop = $('[data-id="SearchResultDesktop"]').val();
                //var inputVal = inputForDesktop;
                //inputForDesktop = inputForDesktop.trim();
                if (inputForDesktop.length >= 3) {
                    searchApiBizFn(inputForDesktop, inputForDesktop.toLowerCase());
                }
                if ($('.header-search').hasClass('opened')) {
                    $('body').css('overflow', 'hidden');
                }
            });

            $('[data-id="SearchResultMobile"]').on('input', function () {
                var inputForMobile = $('[data-id="SearchResultMobile"]').val();
                if (inputForMobile.length >= 3) {
                    searchApiBizFn(inputForMobile, inputForMobile.toLowerCase());
                }
            });
            /* fund search js initialize end */

            // Investment Choices and Risk Profile
            $('[data-action="investment-choices"]').click('click', function () {
                location.href = appConfig.jocataDomain + "?action=investment-choices&source=" + window.location.href;
            });

            $('[data-action="risk-profile"]').click('click', function () {
                location.href = appConfig.jocataDomain + "?action=risk-profile&source=" + window.location.href;
            });
        });

        function getUserObj(){
            if(sessionStorage.getItem('user') != null){
                var userObj = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                return { "isLoggedIn" : true, "userData" : userData};
            }else if(sessionStorage.getItem('user') == null){
                return { "isLoggedIn" : false, "userData" : "null"};
            }    
        }

        function initializeLoginHeaderEvents(userData) {
            var userData = userData;
            var splitName = userData.fullname.split(' ');
            document.querySelector('[data-username]').innerHTML = splitName[0].charAt(0).toUpperCase() + splitName[0].slice(1) + ' ' + splitName[1].charAt(0).toUpperCase() + splitName[1].slice(1);;
            document.querySelector('[data-mobilenumber]').innerHTML = userData.mobileNumber;
            var managerNameInitials = userData.fullname.split(' ').shift().charAt(0).toUpperCase() + userData.fullname.split(' ').pop().charAt(0).toUpperCase();
            $('[data-userprofile="true"]').html(managerNameInitials);
            // show Logout Popup on Logout click
            $('[data-headeraction="logout"]').on('click', function (e) {
                var ele_target = document.getElementById('logout-modal');
                setTimeout(function () {
                    $(ele_target).addClass('popover-show');
                }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');
            });
            // remove userObject form sessionStorage & reedirect to homePage
            $('[data-logout="true"]').on('click', function () {
                sessionStorage.removeItem('user');
                if(window.location.href.includes('tclu')){
                    location.href = '/moneyfy';
                } else {
                    location.href = '/'
                }
            });
        }

        function adobeAnalytics() {
            $('[data-anchorList] li a').click(function (event) {
                event.preventDefault();
                var menuTitle = $(this).text().trim();
                menuInteraction(menuTitle, 'header',userIdObj.userId)
                location.href = $(this).attr('href');
            });

            $('.nav-logo a').click(function (event) {
                event.preventDefault();
                menuInteraction('Logo Image', 'header',userIdObj.userId);
                location.href = $(this).attr('href')
            });
            $('[data-mobmenu] a').click(function (event) {
                event.preventDefault();
                var menuTitle = $(this).text().trim();
                menuInteraction(menuTitle, 'header',userIdObj.userId);
                location.href = $(this).attr('href')
            })
        }

        function initializeHeader() {
            /*start animation js*/
            AOS.init({
                duration: 1000,
            });
            /*end animation js*/

            if (document.querySelector('[data-loginbtn]') != null) {
                document.querySelector('[data-loginbtn]').addEventListener('click', function () {
                    var ctaText = $(this).text().trim();
                    allCTAInteraction(ctaText,ctaText, 'header', userIdObj.userId);
                    location.href = appConfig.jocataDomain;
                });
            }

            /*footer Back To Top*/
            $('.jsBackToTop').click(function () {
                $('html, body').animate({
                    scrollTop: 0
                }, '300');
            })

            /*Login profile js*/
            $(document).mouseup(function (e) {
                var container = $('.custom-login-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('.custom-login-container').addClass('d-none');
                    $('.custom-login-container').removeClass('opened-mob-login');
                    $('.jsLoginProfile').removeClass('active');
                    if ($(window).width() < 992) {
                        $('body').removeClass('scroll-hide');
                      }
                }
                $('.wishlist-fund-list').removeClass('mob-wishlistmodal-scroll');

                // when deskSearch length is 0 & width > 767 code - START
                if ($(window).width() > 767) {
                    var deskSearch = $('.header-search')
                    if (!deskSearch.is(e.target) && deskSearch.has(e.target).length === 0) {
                      $('.header-search').removeClass('desktop-opened');
                      $('.recent-searches').removeClass('d-none')
                      $('.search-results').addClass('d-none')
                      $('.header-search .jsSearchTextBox').val('');
                      $('.desktop-search-textbox').click(function(e){
                        e.stopPropagation();
                      })
                    }      
                  }
                // when deskSearch length is 0 & width > 767 code - END
            });

            $('.jsLoginProfile').click(function () {
                $(this).toggleClass('active');
                $(this).siblings('.custom-login-container').toggleClass('d-none');
                $('.mob-header').removeClass('active');
                $('[data-search="funds"]').removeClass('opened');
                $('.hamburger-menu').removeClass('animate-hamburger');
                // remove class desktop opened for jsLoginProfile click
                if($(window).width() > 767) {
                    $('.header-search').removeClass('desktop-opened');
                }
            });

            if ($(window).width() < 768) {
                $('.jsLoginProfile').click(function () {
                    $('.custom-login-container').toggleClass('opened-mob-login');
                });

                $('.custom-login-dropdown .login-content-list li.login-sub-menu>a').click(function () {
                    $('.custom-login-dropdown .login-submenu-content').slideToggle();
                    $('.custom-login-dropdown .login-content-list li.login-sub-menu').toggleClass('opend-submenu');
                });
            }

            if ($(window).width() < 992) {
                $('.jsLoginProfile').click(function () {
                  /*19-4-2022*/ 
                  $('.js-close-details').removeClass('active-sub');
                  /*19-4-2022*/ 
                  if($(this).hasClass('active')){
                    $('.js-fixContainer').css('z-index', '3');
                    $('body').addClass('scroll-hide');
                  }
                  else {
                    $('body').removeClass('scroll-hide');        
                  }
                })
            }
            
            if ($(window).width() < 768){
                $('.jsLoginProfile').click(function () {
                  $('.header-search .jsSearchTextBox').val('');      
                  $('.header-search').removeClass('opened');
                  $('.search-results').addClass('d-none');  
                  /*19-4-2022*/ 
                  $('.search-overlay').remove();
                  $('body').removeClass('tab-overflow');
                  /*19-4-2022*/ 
                })
            }
            /*Login profile js*/

            /*header Search dropdown js*/
            $.fn.hasAttr = function (dropdown) {
                return this.attr(dropdown) !== undefined;
            };

            /*var isOverview = false
             if (isOverview == false) {
                 headerRenderObj.renderLoansCardsPopup();
            }*/

            /*function onPopupClickAnchor(toSupportedLink) {
                var anchorContainer = document.querySelector("#submenuLoansCards_popup_href");
                var anchor = document.createElement('a');
                if(toSupportedLink === 'credit-card') {
                    anchor.setAttribute('href', '/content/tata-capital-moneyfy/en/' + toSupportedLink + '.html');
                } else if(toSupportedLink === '') {
                    anchor.setAttribute('href', '/content/tata-capital-moneyfy/en/loans' + toSupportedLink + '.html');
                } else {
                    anchor.setAttribute('href', '/content/tata-capital-moneyfy/en/loans/' + toSupportedLink + '.html');
                }
                anchor.setAttribute('class', 'btn-blue radius100');
                anchor.innerText = 'OK'
                anchorContainer.appendChild(anchor);
                if (anchorContainer.childNodes.length > 5) {
                    anchorContainer.removeChild(anchor);
                }
                var closeBtn = false
                var popUpButtonClose = document.querySelector('#submenuLoansCards_popup button');
                popUpButtonClose.addEventListener('click', function () {
                    closeBtn = true;
                    if (closeBtn == true) {
                        anchorContainer.removeChild(anchor);
                        if(isOverview == false){
                            document.querySelector('#submenuLoansCards_popup').style.display = 'none';
                            $('body').removeClass('popover-modal-open');
                            document.querySelector('#submenuLoansCards_popup').classList.remove('popover-show');
                            document.body.removeChild(document.querySelector('.modal-backdrop'))
                        } 
                    }
                });
            }*/

            var desktopScreen = true;
            if ($(window).width() > 767 && desktopScreen) {
            desktopScreen = false;    
            document.querySelectorAll('.second-right .submenu-arrow')[1].querySelector('a').href = "/content/tata-capital-moneyfy/en/loans.html"
            document.querySelector('[data-submenulistname = "Overview"]').parentElement.remove();
            } 

            $(document).click(function (e) {
                /*var anchorLink = e.target.dataset.submenulistname;
                if (anchorLink !== undefined) {
                    if(anchorLink == 'Overview'){
                        isOverview = true;
                        if (($(window).width() < 767 && isOverview) || (($(window).width() < 991 && $(window).width() > 767) && isOverview)){
                            document.querySelector('#submenuLoansCards_popup').innerHTML = '';
                            headerRenderObj.renderLoansCardsPopupOverview() 
                            } 
                        } else if (($(window).width() < 767 && !isOverview) || (($(window).width() < 991 && $(window).width() > 767) && !isOverview)){
                            var ele_pop = $(this).text().trim();
                            if(ele_pop === 'Credit Card'){
                            document.querySelector('[data-submenulistname="Credit Card"]').innerHTML =   '';
                            headerRenderObj.renderLoansCreditCardsPopup();
                        }
                    } else if (($(window).width() < 767 && !isOverview) || (($(window).width() < 991 && $(window).width() > 767) && !isOverview)){
                        document.querySelector('#submenuLoansCards_popup').innerHTML = '';
                        headerRenderObj.renderLoansCardsPopup();
                } 
                    isOverview = false; 
                    var toSupportedLink = anchorLink.split(' ').join('-').toLowerCase();
                    if (toSupportedLink === 'loan-against-securities') {
                        toSupportedLink = 'loan-against-security';
                    } if (toSupportedLink === 'overview') {
                        toSupportedLink = ''; 
                    }  
                    onPopupClickAnchor(toSupportedLink);
                }*/
                if ($(window).width() > 767) {
                    var clicked = $(e.target);
                    var opened = $(".header-search").hasClass("opened");
                    if (opened === true && !clicked.hasClass("jsSearchTextBox")) {
                        $(".header-search").removeClass("opened");
                        $('body').css('overflow', 'scroll');
                    }
                }
            });

                /*10-12-2021*/
                // if ($(window).width() < 991 && $(window).width() > 767) {
                //     $('.search-overlay').remove();
                //     $('body').removeClass('tab-overflow');
                //     $(".js-fixContainer").css('z-index', '8');
                // }
                /*10-12-2021*/   
         
            // $(".header-search-dropdown").on("click", function (event) {
            //     event.stopPropagation();
            // });
            /*header Search dropdown js*/

            /*21-9-2022*/ 
            if($(window).width() > 767) {
                $('.jsDesktopSearchBtn').click(function(){      
                $('.header-search').toggleClass('desktop-opened');
                })    
            }
            /*21-9-2022*/ 

            /*21-01-22*/
            if ($(window).width() < 991 && $(window).width() > 767) {
                $('.header-search .jsSearchTextBox').keyup(function () {
                    if ($('.header-search .jsSearchTextBox').val() != '') {
                        $('body').addClass('tab-overflow');
                        $('.search-overlay').remove();
                        $('body').append('<div class="search-overlay"></div>');
                        $(".js-fixContainer").css('z-index', '8');
                        $('.mob-header').removeClass('active');
                        $('.hamburger-menu').removeClass('animate-hamburger');
                    } else {
                        $('.search-overlay').remove();
                        $('body').removeClass('tab-overflow');
                        $(".js-fixContainer").css('z-index', '10');
                    }
                });
            }

            if ($(window).width() < 768) {
                $('.jsMobileSearchBtn').click(function () {
                    if (!$('.header-search').hasClass('opened')) {
                        $('body').addClass('tab-overflow');
                        $('body').append('<div class="search-overlay"></div>');
                        $(".js-fixContainer").css('z-index', '8')
                    } else {
                        $('body').removeClass('tab-overflow');
                        $('.search-overlay').remove();
                        $(".js-fixContainer").css('z-index', '10')
                    }
                });
                $('.jsMobileCloseBtn').click(function () {
                    $('body').removeClass('tab-overflow');
                    $('.search-overlay').remove();
                    $(".js-fixContainer").css('z-index', '10');
                });
            }
            /*21-01-22*/

            /*10-12-2021*/
            if ($(window).width() < 991 && $(window).width() > 767) {
                $('.jsDesktopSearchBtn').click(function(){      
                    $('.header-search').toggleClass('desktop-opened');
                  }) 
                $('.header-search .jsSearchTextBox').keyup(function () {
                    if ($('.header-search .jsSearchTextBox').val() != '') {
                        $('body').addClass('tab-overflow');
                        $('.search-overlay').remove();
                        $('body').append('<div class="search-overlay"></div>');
                    } else {
                        $('.search-overlay').remove();
                        $('body').removeClass('tab-overflow');
                    }
                });
            }
            if ($(window).width() < 768) {
                $('.jsMobileSearchBtn').click(function () {
                    if (!$('.header-search').hasClass('opened')) {
                        $('body').addClass('tab-overflow');
                        $('body').append('<div class="search-overlay"></div>');
                    } else {
                        $('body').removeClass('tab-overflow');
                        $('.search-overlay').remove();
                    }
                });
                $('.jsMobileCloseBtn').click(function () {
                    $('body').removeClass('tab-overflow');
                    $('.search-overlay').remove();
                });
            }
            /*10-12-2021*/
            /*header js*/
            if ($(window).width() > 767) {
                $(".header-search .jsSearchTextBox").keyup(function () {
                    if ($(this).val().length >= 3) {
                        $(".header-search").addClass("opened");
                        $(".search-results").removeClass("d-none");
                        $(".no-result-found").removeClass("d-none");
                        $('body').css('overflow', 'hidden');
                    } else {
                        $(".header-search").removeClass("opened");
                        $(".search-results").addClass("d-none");
                        $(".no-result-found").addClass("d-none");
                        $('body').css('overflow', 'scroll');
                    }
                });
            } else {
                $(".jsMobileSearchBtn").click(function () {
                    $(".header-search").toggleClass("opened");
                    $("body").removeClass("scroll-hide");
                    $(".mob-header").removeClass("active");
                    $(".js-close-details").removeClass("active-sub");
                    $(".hamburger-menu").removeClass("animate-hamburger");
                });

                $(".header-search .jsSearchTextBox").keyup(function () {
                    if ($(this).val().length > 0) {
                        $(".search-results").removeClass("d-none");
                        $(".no-result-found").removeClass("d-none");
                    } else {
                        $(".search-results").addClass("d-none");
                        $(".no-result-found").addClass("d-none");
                    }
                });

                $(".jsMobileCloseBtn").click(function () {
                    $(".header-search .jsSearchTextBox").val("");
                    $(".header-search").removeClass("opened");
                    $(".search-results").addClass("d-none");
                    $(".no-result-found").addClass("d-none");
                });
            }
            
            /*$(".jsMainMenu>li.submenu-arrow").hover(
                function () {
                    $(".jsSubMenu>li.sub-submenu-arrow").addClass("active");
                    // var links = document.querySelectorAll('.jsSubMenu')[1].querySelectorAll('a');
                    // links.forEach(function (link)  {
                    //     link.href = 'javascript:void(0)'
                    // })
                },
            );*/

            $(".jsSubMenu>li.sub-submenu-arrow").hover(
                function () {
                    $(".jsSubMenu .inner-dropdown").addClass("d-block");
                    $(this).addClass("active");
                },
                function () {
                    $(".jsSubMenu .inner-dropdown").removeClass("d-block");
                    $(this).removeClass("active");
                }
            );


            /*mobile menu js*/
            $(".js-mobsubmenu .js-accord-col-mob .submenu-head").each(function () {
                $(this).click(function () {
                    $(this).toggleClass('active');
                    $(this).siblings('.accord-body').slideToggle();
                    $(this).parents('.js-accord-col-mob').siblings('.js-accord-col-mob').find('.submenu-head').removeClass('active');
                    $(this).parents('.js-accord-col-mob').siblings('.js-accord-col-mob').find('.accord-body').slideUp();
                });
            });

            $(".js-showDetail").click(function () {
                $('body').addClass('scroll-hide');
                ele_showDetail = $(this).attr("data-value");
                $(".js-close-details").removeClass("active-sub");
                $("#" + ele_showDetail).addClass("active-sub");
            });

            $(".js-back").click(function () {
                $('body').addClass('scroll-hide');
                $(".js-close-details").removeClass("active-sub");
            });

            $(".hamburger-menu").click(function () {
                this.classList.toggle("animate-hamburger");
                $(".mob-header").toggleClass("active");
                $(".js-close-details").removeClass("active-sub");

                if ($(".mob-header").hasClass("active")) {
                    $("body").addClass("scroll-hide");
                } else {
                    $("body").removeClass("scroll-hide");
                }

                $(".header-search .jsSearchTextBox").val("");
                $(".header-search").removeClass("opened");
                $(".search-results").addClass("d-none");

                if ($('.hamburger-menu').hasClass('animate-hamburger')) {
                    $(".js-fixContainer").css('z-index', '8');
                } else {
                    $(".js-fixContainer").css('z-index', '10');
                }
                /*10-12-2021*/
                $('.search-overlay').remove();
                $('body').removeClass('tab-overflow');
                /*10-12-2021*/

                // remove desktop opened class on hamburger-menu click
                if($(window).width() > 767) {
                    $('.header-search').removeClass('desktop-opened');
                }
            });
            /*header js*/

                // toggle desktop open class, remove active and animate-hamburger class for mobile - START
                if ($(window).width() < 991 && $(window).width() > 767) {    
                    $('.jsDesktopSearchBtn').click(function(){      
                        $('.header-search').toggleClass('desktop-opened');
                        $('.hamburger-menu').removeClass('animate-hamburger');
                        $('.mob-header').removeClass('active');       
                    })  
                }
                // toggle desktop open class, remove active and animate-hamburger class for mobile - END

            $(window).on("resize", function () {
                if ($(".slick-slider") != undefined) {
                    $('.slick-slider').slick('refresh');
                    header_fixed();
                }
            });

            $(window).on("load", function () {
                header_fixed();
            });

            $(window).on("scroll", function () {
                header_fixed();
                if ($(window).width() > 991) {
                    // 21-01-22
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        }
                        else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();


                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }
                    // 21-01-22
                }
            });

        }

        function scrollInitialize() {
            $('.filterList-mCustomScrollbar').mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });
        }

        function searchApiBizFn(requestData, inputVal) {
            let requestObj = {
                "body": {}
            };
            requestObj.body.searchTerm = requestData;
            headerApiObj.headerSearch(requestObj).then(function (response) {
                if (response.status.toLowerCase() == 'success') {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    headerRenderObj.headerSearchRenderFn(responseData.responseJson, inputVal);
                    scrollInitialize();
                    $('[data-atr="search-list"] li a').click(function (event) {
                        event.preventDefault();
                        var searchTerm = $(this).text().trim();
                        internalSearch(searchTerm, userIdObj.userId)
                        window.open($(this).attr('href'));
                    })
                }
            }).catch(function (error) {
                console.log(error);
            });

        }

        
        window.addEventListener('load', function(){
        if (getUserObj().isLoggedIn){
        var cartCountReqObj = {
            "header": {
            "authToken": shaftAuthToken
            },
            "body": {
            "mobileNumber": getUserObj().userData.mobileNumber,
            "tatId": getUserObj().userData.tatId,
            "product": "moneyfy"
            }
        }
        var cartCountNew = 0;
        headerApiObj.cartCountFn(cartCountReqObj).then(function(response){
            if (response.status.toLowerCase() == 'success') { 
                var responseData = jsHelper.isDef(response) && jsHelper.isObj(response) ? JSON.parse(response.response) : response;
                if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                    cartCountNew = responseData.responseJson.body.count;
                    document.querySelector('[data-headeraction="cart"] .cartCount').innerText = '('+cartCountNew+')';
				} else {
                    cartCountNew = 0;
                };
            };
        }).catch(function (error) {
            console.log(error);
        });
        }
        });

        /*header scroll fixed animation*/
        var TOPBAR_HEIGHT = $(".top-bar").outerHeight();
        var SECONDBAR_HEIGHT = $(".second-bar").innerHeight();
        function header_fixed() {
            var windowScroll = $(window).scrollTop();
            var topbarHeight = $(".top-bar").outerHeight();

            if (windowScroll > topbarHeight) {
                $("header").addClass("affix");
                $(".wrapper").css("padding-top", SECONDBAR_HEIGHT);
            } else {
                $("header").removeClass("affix");
                $(".wrapper").css("padding-top", TOPBAR_HEIGHT + SECONDBAR_HEIGHT);
            }
        }
        headerBizObj.getUserObj = getUserObj;
        //headerObj.getUserWatchList = getUserWatchList;
        return jsHelper.freezeObj(headerBizObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, "headerBizObj", headerBizObj);
})(this || window || {});
  /*header js*/