Goal Based Investment
====
The `Goal Based Investment` component is for adding goal based investment types on moneyfy mututal funds page. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as goal type, icons & onclick actions are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used for authoring the title of the component.
2. `./goalList` Used in multifields for looping purpose.
3. `./typeName` Used to label the goal types in the component.
4. `./redirectLink` are used to give the redirection link to the respective goal type cards.
5. `./newTab` are used to check if the tabs should open in new tab.
6. `./image ./hoverImage` are used to select the image paths for each goals in the component.


## Client Libraries
The component provides a `moneyfy.goal-based-investment` editor client library category that includes JavaScript and CSS
handling for dialog interaction.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5