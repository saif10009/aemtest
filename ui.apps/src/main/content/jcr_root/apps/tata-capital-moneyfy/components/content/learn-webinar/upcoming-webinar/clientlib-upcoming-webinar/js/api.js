function upcomingWebinarApiCall(upcomingWebinarObj) {
   
    return new Promise(function (resolve, reject) {
        
        $.ajax({
            type: "POST",
            url: "/content/tata-capital/mdm.upcomingWebinar.json",
            data: JSON.stringify(upcomingWebinarObj),
            async: true,
            contentType: "application/json",
            dataType:'json',
            success: function (res) {
                resolve(res)
            },
            failure: function (res) {
                reject(res)
            }
        })
    })
}