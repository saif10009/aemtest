Home Page Banner
====
The `Home Page Banner` component is for adding banners on moneyfy. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as descriptions, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./click ./background` Used for checking the clickablity and image background type of the banners.
2. `./buttonOne ./buttonTwo` Used for labeling the buttons.
3. `./desktopImage ./mobileImage ./tabletImage ./typeImage ./laptopImage` Used to select the images for the banners in the component.
4. `./description` are used for the short description that can be given to the respective cards.
5. `./bannerLink ./btn1Link ./btn2Link` are used for select redirection paths if the tabs should open in new tab.
6. `./checkOne ./checkTwo ./bannerCheck` are used to check if the link should open in new tabs.


## Client Libraries
The component provides a `moneyfy.home-page-banner` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The clientlib of this component is embedded in the `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib` folder.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5