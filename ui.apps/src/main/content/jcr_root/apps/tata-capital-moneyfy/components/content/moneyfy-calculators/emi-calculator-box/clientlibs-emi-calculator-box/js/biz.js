/*emi calculator js start*/
(function (_global) {
    var emiCalculatorBizObj = (function (jsHelper) {
        var emiCalculatorObj = {}
        var chartIntialObj = {}

        document.addEventListener('DOMContentLoaded', function () {
            emiCalculatorInitializer();
            emiCalculatorEvents();
            emiHighchartReact(10000000, 3748187, 100)
            amortization(10000000, 0.005583333333333333, 120, 114568.22787161371, 3748187.344593644);
            $('[data-emi-btn="emiredirection"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('emi-calculator-box',userId,'EMI calculator',ctaText);   
                        }else{
                            calculatorCtaInteraction('emi-calculator-box',userId,'EMI calculator',ctaText);
                        }      
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('emi-calculator-box',"anonymous user",'EMI calculator',ctaText);    
                        }else{
                            calculatorCtaInteraction('emi-calculator-box',"anonymous user",'EMI calculator',ctaText);
                        }   
                    }
                } catch (err) {
                    console.log(err);
                }
                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    location.href = $(this).attr('href');
                }
            })

            $('#yearCreateOption').on('change',function(event){
                var ctaText = $('#select2-yearCreateOption-container').prop('title');
                try {
                    selectYears(userIdObj.userId,ctaText,'emi-calculator-box');    
                } catch (err) {
                    console.log(err);
                }
            });

        })
        function emiCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
            // Range slider end
            if ($('.single-select2').is(":visible")) {
                $('.single-select2').select2({
                    minimumResultsForSearch: -1
                });
            }
            var dataDynamic = {
                data_1Y: [{
                    name: "Total Principle amount paid",
                    data: [],
                    color: "#48A476",
                    amount: [],
                },
                {
                    name: "Total Interest Paid",
                    data: [],
                    color: "#2C6EB5",
                    amount: [],
                }],
            };
            var yearDynamic = {
                year_1Y: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12],
            };
            var chartAmortization = Highcharts.chart("emi-calculator-amortization-chart", {
                chart: {
                    type: "spline",
                    margin: [0, 0, 30, 0],
                    animation: {
                        enabled: false,
                    },
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                subtitle: {
                    text: "",
                },

                xAxis: {
                    categories: yearDynamic.year_1Y,
                    style: {
                        color: "#606060",
                        fontSize: "12px",
                        fontFamily: "Poppins",
                        fontWeight: "400",
                    },

                    crosshair: {
                        width: 1,
                        color: "#2c6eb566",
                        dashStyle: "shortdot",
                    },
                },
                yAxis: {
                    title: {
                        text: "",
                        style: {
                            color: "#606060",
                            fontSize: "12px",
                            fontFamily: "Poppins",
                            fontWeight: "400",
                        },
                    },
                    tickInterval: 10,
                    visible: false,
                },

                // 10-1-2022
                tooltip: {
                    formatter: function (i) {
                        var s = '';
                        s += '<div class="custom-tooltip amortization-tooltip"><p class="chart-head">' + this.x + ' month Amortization </p>';
                        $.each(this.points, function (i, point) {
                            s += '<div class="tooltip-item"><span class="tooltip-label"><span class="dot" style="background-color: ' + point.series.color + '"></span> ' + point.series.name + '</span>',
                                s += '<span class="tooltip-result"><b>₹' + point.y + '</b> out of ₹';
                            s += point.series.options.amount[this.point.index],
                                s += '</span></div>'
                        });
                        return s += '</div>';
                    },
                    // 10-1-2022
                    shared: true,
                    useHTML: true,
                    backgroundColor: "#333",
                    shadow: false,
                    borderWidth: 0,
                    borderRadius: 6,
                    style: {
                        fontFamily: "Poppins",
                    },
                },

                plotOptions: {
                    series: {
                        color: "#48A476",
                        showInLegend: false,

                        marker: {
                            enabled: false,
                            symbol: "cirle",
                        },
                    },
                },

                series: dataDynamic.data_1Y,
            });
            chartIntialObj.dataDynamic = dataDynamic;
            chartIntialObj.yearDynamic = yearDynamic;
            chartIntialObj.chartAmortization = chartAmortization;
        }
        function emiCalculatorEvents() {
            var tenureObj = {
                emiTenureYear: document.getElementById("emi_tenure_year"),
                emiTenureMonth: document.getElementById("emi_tenure_month"),
                emiErrorMsg: document.getElementById("emiErrorMsg"),
            }
            //chart amortization change
            $(".js-yearSelect").on("change", function () {
                var returnData = $(".js-yearSelect option:selected").attr("value");
                var returnYear = $(".js-yearSelect option:selected").attr("year");
                chartIntialObj.chartAmortization.update({
                    series: chartIntialObj.dataDynamic[returnData],
                    xAxis: {
                        categories: chartIntialObj.yearDynamic[returnYear],
                    },
                });
            });
            // year month toggle
            $('[data-yrMthFilter]').click(function () {
                filterVal = $(this).attr('data-yrMthFilter');
                $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
                $(this).addClass('active');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
            });

            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");
                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });

            
            $('[data-yrMthFilter="year"]').click(function () {
                emiCalculationFn(Number(tenureObj.emiTenureYear.value) * 12);
            })
            $('[data-yrMthFilter="month"]').click(function () {
                emiCalculationFn(Number(tenureObj.emiTenureMonth.value));
            })
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").on("input", function () {
                    if (document.querySelector('[class="yr-mth-btn active"]').innerHTML == 'Years') {
                        emiCalculationFn(Number(tenureObj.emiTenureYear.value) * 12)
                    } else {
                        emiCalculationFn(Number(tenureObj.emiTenureMonth.value));
                    }
                });
            }
            //emi input amount keyup event
            document.getElementById("emiAmount").addEventListener('keyup', function () {
                if (parseFloat(this.value.replace(/,/g, '')) > 500000000) {
                    tenureObj.emiErrorMsg.innerHTML = '';
                    tenureObj.emiErrorMsg.innerHTML = 'Amount should not greater than ₹500000000';
                } else if (parseFloat(this.value.replace(/,/g, '')) < 25000) {
                    tenureObj.emiErrorMsg.innerHTML = '';
                    tenureObj.emiErrorMsg.innerHTML = 'Amount should not less than ₹25000';
                } else {
                    tenureObj.emiErrorMsg.innerHTML = '';
                }
                if (document.querySelector('[class="yr-mth-btn active"]').innerHTML == 'Years') {
                    emiCalculationFn(Number(tenureObj.emiTenureYear.value) * 12)
                } else {
                    emiCalculationFn(Number(tenureObj.emiTenureMonth.value));
                }
            })
        }

        function emiCalculationFn(emiTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            var emiObj = {
                calculator: "EMI",
                emiamount: Number(parseFloat(document.getElementById('emiAmount').value.replace(/,/g, ''))),
                emiTenure: emiTenure,
                emiRoi: Number(document.getElementById('emiroi').value) / (100 * 12),
            }
            myWorker.postMessage(emiObj);
            myWorker.onmessage = function (e) {
                emiObj.emiHighchartReact = emiHighchartReact;
                emiCalculatorRenderObj.renderEmiCalculation(e, emiObj);
                amortization(emiObj.emiamount, emiObj.emiRoi, e.data.emiMonths, e.data.emi, e.data.interest);
                myWorker.terminate();
            }
        }
        function emiHighchartReact(invest, interest, initialAxis) {
            var reactChart = (invest / (invest + interest)) * 100;
            // suggested strategy chart js start
            Highcharts.chart("emi-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });
        }
        function dataArr(targetElement, yearName, yearSplit, mpt, mbt, it, ipt, indexValue) {
            targetElement[0].data = mpt[indexValue];
            targetElement[0].amount = mbt[indexValue];
            targetElement[1].data = it[indexValue];
            targetElement[1].amount = ipt[indexValue];
            chartIntialObj.yearDynamic[yearName] = yearSplit[indexValue];
            var returnData = $(".js-yearSelect option:selected").attr("value");
            var returnYear = $(".js-yearSelect option:selected").attr("year");
            chartIntialObj.chartAmortization.update({
                series: chartIntialObj.dataDynamic[returnData],
                xAxis: {
                    categories: chartIntialObj.yearDynamic[returnYear],
                },
            });
        }
        function amortization(monthlyBalance, interestRate, terms, emi, payout) {
            // amortization chart js start
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            myWorker.postMessage({
                calculator: "Amort",
                monthlyBalance: monthlyBalance,
                interestRate: interestRate,
                terms: terms,
                emi: emi,
                payout: payout,
            })
            myWorker.onmessage = function (e) {
                dynamicdata(terms, e.data.yearSplit, e.data.monthlyPrincipalTotal, e.data.monthlyBalanceTotal, e.data.interestTotal, e.data.interestPayoutTotal);
                myWorker.terminate();
            }
        }
        function dynamicdata(terms, yearSplit, monthlyPrincipalTotal, monthlyBalanceTotal, interestTotal, interestPayoutTotal) {
            var yearList = document.querySelector('#yearCreateOption');
            yearList.innerHTML = '';
            for (var count = 1; count <= Math.ceil(terms / 12); count++) {
                var dataName = 'data_' + count + 'Y';
                var yearName = 'year_' + count + 'Y';
                yearList.innerHTML += '<option value="' + dataName + '" year="' + yearName + '">' + count + ' year' + '</option>';
                chartIntialObj.yearDynamic[yearName] = [];
                chartIntialObj.dataDynamic[dataName] = [
                    {
                        name: "Total Principle amount paid",
                        data: [],
                        color: "#48A476",
                        amount: [],
                    },
                    {
                        name: "Total Interest Paid",
                        data: [],
                        color: "#2C6EB5",
                        amount: [],
                    },
                ];
                dataArr(chartIntialObj.dataDynamic[dataName], yearName, yearSplit, monthlyPrincipalTotal, monthlyBalanceTotal, interestTotal, interestPayoutTotal, (count - 1))
            }
        }
        return jsHelper.freezeObj(emiCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "emiCalculatorBizObj", emiCalculatorBizObj)
})(this || window || {});
/*emi calculator js end*/