(function (_global) {

    var chooseFromAmcApiFn = (function (jsHelper) {
        var chooseFromAmcApiObj = {};

        var chooseFromAmc = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getFundsThroughAmc(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        chooseFromAmcApiObj.chooseFromAmc = chooseFromAmc;

        return jsHelper.freezeObj(chooseFromAmcApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'chooseFromAmcApiObj', chooseFromAmcApiFn);
})(this || window || {});
