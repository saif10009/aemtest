/*choose from amc  biz.js start*/
(function (_global) {
    var nfoFn = (function (jsHelper) {
        var nfoObj = {}

        document.addEventListener('DOMContentLoaded', function () {
            /*tab js*/
            $('.jsTabList [data-tab]').click(function () {
                $(this).parents('.jsTabList').find('li a').removeClass('active');
                $(this).addClass('active');
                var ele_id = $(this).attr('data-tab');
                $(this).parents('.jsTabContainer').find('.jsTabRow').addClass('d-none');
                $('#' + ele_id).fadeIn().removeClass('d-none');
            })
            /*tab js*/

            /* Invest Now click button */
            $('[data-investnow="true"]').click(function () {
                var schemaId = $(this).parents('.fund-list-li').data('schemeid');
                var source = window.location.href;
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                fundInvestNow(fundType, fundName, fundRiskCat, userIdObj.userId);
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                    });
                } else {
                    location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                }
            });

            $('.openNfo').click(function(){
                window.open($(this).data('pagepath')); 
               })


        });
    return jsHelper.freezeObj(nfoObj);
    //adobe analytics 7-23
})(jsHelper)
_global.jsHelper.defineReadOnlyObjProp(_global, "nfoObj", nfoFn)
}) (this || window || {});
/*choose from amc biz.js end*/

 
