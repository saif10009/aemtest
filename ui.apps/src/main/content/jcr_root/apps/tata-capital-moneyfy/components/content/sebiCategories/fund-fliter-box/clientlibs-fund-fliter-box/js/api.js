
/* SEBI fund filter api js start */
(function (_global) {
    var fundFilterApiFn = (function (jsHelper) {
        var fundFilterApiObj = {};

        var compareSearch = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getSearchApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Add To WatchList Api calling */
        var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from Portfolio Api calling */
         var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        fundFilterApiObj.fetchFundsData = getFundsData;
        fundFilterApiObj.fetchFromPortfolio = fetchFromPortfolio;
        fundFilterApiObj.fetchwatchListApi = fetchwatchListApi;
        fundFilterApiObj.addTowatchListApi = addTowatchListApi;
        fundFilterApiObj.compareSearch = compareSearch;

        return jsHelper.freezeObj(fundFilterApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundFilterApiObj', fundFilterApiFn);
})(this || window || {});
/* SEBI fund filter api js end */