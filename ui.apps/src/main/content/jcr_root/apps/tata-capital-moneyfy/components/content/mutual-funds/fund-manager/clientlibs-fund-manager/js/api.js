/*fund manager api js start*/
(function (_global) {
    var fundManagerApiFnObj = (function (jsHelper) {
        var fundManagerApiObj = {};
        var fundManager = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getFundsViaFundManagerName(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        fundManagerApiObj.fundManager = fundManager;
        return jsHelper.freezeObj(fundManagerApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundManagerApiObj', fundManagerApiFnObj);
})(this || window || {});