/*goal based investment biz js start*/
(function (_global) {
  var goalBasedInvestmentBizFn = (function (jsHelper) {
    var goalBasedInvestmentBizObj = {};
    document.addEventListener("DOMContentLoaded", function () {
      adobeAnalytics();

      //CTA on goal cards click
      $('[data-goaltype]').click(function (e) {
        //console.log($(this).data('goaltype'));
        if (headerBizObj.getUserObj().isLoggedIn) {
          location.href = appConfig.jocataDomain + '?action=goals' + '&screen=' + $(this).data('goaltype');
        } else {
          var goalType = $(this).data('goaltype');
          showLoginPopup();
          document.querySelector('[data-login="true"]').addEventListener('click', function () {
            location.href = appConfig.jocataDomain + '?action=goals' + '&screen=' + goalType;
          });
        }
      });

    });

    function showLoginPopup() {
      var ele_target = document.getElementById('login-modal');
      setTimeout(function () { $(ele_target).addClass('popover-show'); }, 80);
      $(ele_target).css('display', 'block');
      $('.slick-slider').slick('slickGoTo', 0);
      $('body').addClass('popover-modal-open');
      $('body').append('<div class="modal-backdrop"></div>');
    }

    function adobeAnalytics() {
      $("[data-listgoalbase] a").click(function (event) {
        event.preventDefault();
        var goalItem = $(this).text().trim();
        goalBaseInvestmentClick(goalItem, userIdObj.userId);
        location.href = $(this).attr('href');
      });
    }
    return jsHelper.freezeObj(goalBasedInvestmentBizObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "goalBasedInvestmentBizObj", goalBasedInvestmentBizFn);
})(this || window || {});

/*goal based investment biz js end*/
