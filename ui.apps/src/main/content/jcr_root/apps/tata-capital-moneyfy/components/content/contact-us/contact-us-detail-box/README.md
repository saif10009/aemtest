Contact us detail box
====
This is a `Contact us detail box` component used in contact us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as descriptions, images, headings are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./investmentHeading ,./queriesHeading ./loanHeading ,./` Used for rendering headings of the component.
2. `./loanImage , ./investmentImage ,./quriesImage` Used for rendering image on page.
3. `./queriesDescription ,./investmentDescription ,./loanBottomDescription ,./loanDescription` used for add html elements in component.
4. `./loanItemMultifield ,./queriesListMultifield ,./investmentListMultifield ,./loanItemMultifield` used for add multifield in component.   


## Client Libraries
The component provides a `moneyfy.contact-us-detail-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5