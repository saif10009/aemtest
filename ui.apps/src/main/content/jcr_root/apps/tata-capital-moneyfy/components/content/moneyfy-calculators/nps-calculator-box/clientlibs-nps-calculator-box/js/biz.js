/*nps calculator js start*/
(function (_global) {
    var npsCalculatorBizObj = (function (jsHelper) {
        var npsCalculatorObj = {}
        var insuranceDateObj = { monthsArr: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'] }
        document.addEventListener('DOMContentLoaded', function () {
            dateCreate(1, 31)
            monthCreate(insuranceDateObj.monthsArr)
            yearCreate(1961, 2003)
            npsCalculatorInitializer();
            npsCalculatorEvents();
            npsHighchartReact(5947297, 4750000, 100);
            $('[data-nps-start="npsstartbtn"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('nps-calculator-box',userId,'NPS calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('nps-calculator-box',userId,'NPS calculator',ctaText);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('nps-calculator-box',"anonymous user",'NPS calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('nps-calculator-box',"anonymous user",'NPS calculator',ctaText);
                        }
                    }
                } catch (err) {
                    console.log(err);
                }
                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    location.href = $(this).attr('href');
                }
            })
        })
        function yearCreate(startyear, lastYear) {
            for (i = startyear; i <= lastYear; i++) {
                var yearDropdown = document.getElementById('yearDropdown');
                yearDropdown.innerHTML += '<li class="ak"><a href="javascript:void(0)">' + i + '</a></li>'
            }
        }
        function dateCreate(startDate, lastDate) {
            for (i = startDate; i <= lastDate; i++) {
                var dateChange = document.getElementById('dateChange');
                dateChange.innerHTML += '<li class="ak"><a href="javascript:void(0)">' + i + '</a></li>'
            }
        }
        function monthCreate(arr) {
            for (i = 0; i < arr.length; i++) {
                var monthDropdown = document.getElementById('monthDropdown');
                monthDropdown.innerHTML += '<li><a href="javascript:void(0)">' + arr[i] + '</a></li>'
            }
        }
        function npsCalculatorInitializer() {
            if ($('.single-select2').is(":visible")) {
                $('.single-select2').select2({
                    minimumResultsForSearch: -1
                });

                $('.nps-select').on('change', function () {
                    var data = $(this).find("option:selected").text();
                    if (data != 'Select') {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').removeClass('hide-data');
                    } else {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').addClass('hide-data');
                    }
                });
            }
            $('.mCustomScroll').mCustomScrollbar({
                axix: "x"
            });
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            };
        }
        function npsCalculatorEvents() {
            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });
            /*dropdown js*/
            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

            $('.jsDropDownMenuGet li a').click(function () {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');

            });

            /*Close dropdown when click on link*/
            $('.jsDropDownMenuClose li a').click(function () {
                $(this).parents('.custom-dropdown').find('.js-filterBtn').removeClass('active');
                $(this).parents('.rating-filter-card').slideUp('fast');
            });
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();
                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });

            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").on("input", function () {
                    npsCalculatorCalculationFn(dateList.innerText, monthList.innerText, yearList.innerText);
                });
            }
            var dateList = document.getElementById('dateList');
            var monthList = document.getElementById('monthList');
            var yearList = document.getElementById('yearList');
            var dobList = $('.dropdown-list-menu').find('li');
            $(dobList).click(function () {
                var insuranceDate = $('#dateChange').find('li');
                var insure = insuranceDateObj.monthsArr.indexOf(monthList.innerText) + 1;
                if ((insure % 2 == 0 || monthList.innerText == 'Nov' || monthList.innerText == 'Sep') && (monthList.innerText != 'Aug') && (monthList.innerText != 'Dec') && (monthList.innerText != 'Oct')) {
                    insuranceDate[30].style.display = "none";
                    insuranceDate[29].style.display = 'block';
                    insuranceDate[28].style.display = 'block';
                    if (dateList.innerText > 30) {
                        dateList.innerText = 30
                    }
                } else {
                    insuranceDate[30].style.display = "block";
                    insuranceDate[29].style.display = 'block';
                    insuranceDate[28].style.display = 'block';
                    
                }
                if (monthList.innerText == 'Feb') {
                    if (((yearList.innerText % 4 == 0) && (yearList.innerText % 100 != 0)) || (yearList.innerText % 400 == 0)) {
                        insuranceDate[30].style.display = 'none';
                        insuranceDate[29].style.display = 'none';
                        insuranceDate[28].style.display = 'block';
                        if (dateList.innerText > 29) {
                            dateList.innerText = 1
                        }
                    } else {
                        insuranceDate[30].style.display = 'none';
                        insuranceDate[29].style.display = 'none';
                        insuranceDate[28].style.display = 'none';
                        if (dateList.innerText > 28) {
                            dateList.innerText = 1
                        }
                    }

                }
                npsCalculatorCalculationFn(dateList.innerText, monthList.innerText, yearList.innerText);
            })

            $('#investAmount').on('keyup', function () {
                var npsErrorMsg=document.getElementById('npsErrorMsg');
                if (parseFloat(this.value.replace(/,/g, '')) > 1000000) {
                    npsErrorMsg.innerHTML = '';
                    npsErrorMsg.innerHTML = 'Amount should not greater than 1000000';
                } else if (parseFloat(this.value.replace(/,/g, '')) < 500) {
                    npsErrorMsg.innerHTML = '';
                    npsErrorMsg.innerHTML = 'Amount should not less than ₹500';
                }
                else {
                    npsErrorMsg.innerHTML = '';
                }
                npsCalculatorCalculationFn(dateList.innerText, monthList.innerText, yearList.innerText);
            })
            $('.custom-border-radio').on('click', function () {
                npsCalculatorCalculationFn(dateList.innerText, monthList.innerText, yearList.innerText);
            })

        }
        function npsCalculatorCalculationFn(date, mon, year) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            var npsInputObj = {
                calculator: "NPS",
                npsAmount: Number(parseFloat(document.getElementById("investAmount").value.replace(/,/g, ''))),
                taxSlab: parseFloat(parseFloat($('#taxSlab[type=radio]:checked').parent('.custom-border-radio').text().replace(/\n/g, '').trim())
                ) / 100,
                tenureYears: parseFloat($('#pensionYear[type=radio]:checked').parent('.custom-border-radio').text().replace(/\n/g, '').trim()),
                date: date,
                mon: mon,
                year: year,
                expectedCorpusInput: Number(document.getElementById('expectedCorpus').value),
                expectReturnsInput: Number(document.getElementById("expectReturns").value) / 100,
                expectedPentionInput: Number(document.getElementById("expectedPention").value),
            }
            myWorker.postMessage(npsInputObj);

            myWorker.onmessage = function (e) {
                npsInputObj.npsHighchartReact = npsHighchartReact;
                npsCalculatorRenderObj.renderNpsCalculation(e, npsInputObj);
                myWorker.terminate();
            }
        }
        function npsHighchartReact(invest, interest, initialAxis) {
            var reactChart = (invest / (invest + interest)) * 100;
            // Range slider end
            Highcharts.chart("nps-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Interest earned ",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });
        }
        return jsHelper.freezeObj(npsCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "npsCalculatorBizObj", npsCalculatorBizObj)
})(this || window || {});
  /*nps calculator js end*/