Similar Funds
====
The `Similar Funds` component is for showing the funds related to the current fund in the fund-detail page. 



## Feature
* It is an dynamic component with data populated using JS.   



## Edit Dialog Properties
There are no properties stored.


## Client Libraries
The component provides a `moneyfy.similar-funds` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5