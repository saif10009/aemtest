Refer
====
This component witten in HTL, allowing to render moneyfy reference on page.

## Feature
* This is authorable component.
* Used to provide refernce of moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./imgDesktop` - Used to render image for desktop.
2. `./imgMobile` - Used to render image for mobile view.
3. `./referHeading` - Used to render heading of component.
4. `./referDescription` - Used to render description of componet.
5. `./referButton` - Consider button tittle.
 

## Client Libraries
The component provides a `moneyfy.refer` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is already included in component HTML file.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5