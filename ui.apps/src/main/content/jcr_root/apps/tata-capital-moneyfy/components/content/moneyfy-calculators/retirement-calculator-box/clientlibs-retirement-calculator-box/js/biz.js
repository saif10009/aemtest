/*retirement calculator js start*/
(function (_global) {
    var retirementCalculatorBizObj = (function (jsHelper) {
        var retirementCalculatorObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            retirementCalculatorInitializer();
            retirementCalculatorEvents();
            retirementHighchartReact(0, 0, 0);

            $('[data-start-investing="retirement"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('retirement-calculator-box',userId,'Retirement Calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('retirement-calculator-box',userId,'Retirement Calculator',ctaText);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('retirement-calculator-box',"anonymous user",'Retirement Calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('retirement-calculator-box',"anonymous user",'Retirement Calculator',ctaText);
                        }
                    }
                } catch (err) {
                    console.log(err);
                }
                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    location.href = $(this).attr('href');
                }
            })
        })
        function retirementCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
        }
        function retirementCalculatorEvents() {
            // Calulator Input change start
            if ($(".js-calculatorRangeSlider").is(":visible")) {

                $(".js-calculatorRangeSlider").on("input", function () {
                    retirementCalculationFn();
                });
            }
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            var errorEleObj = {
                currentErrorMsg: document.getElementById('currentErrorMsg'),
                retirementErrorMsg: document.getElementById('retirementErrorMsg'),
                lifeErrorMsg: document.getElementById('lifeErrorMsg'),
                currentExpenseErrorMsg: document.getElementById('currentExpenseErrorMsg'),
                currentAccCorpusErrorMsg: document.getElementById('currentAccCorpusErrorMsg'),
            }
            var allInputBox = document.getElementsByClassName('allInputBox');
            allInputBox[0].addEventListener('keyup', function () {
                if ((parseFloat(this.value) >= parseFloat(allInputBox[1].value))) {
                    currentErrorMsg.innerHTML = '';
                    currentErrorMsg.innerHTML = 'Your Current age should be less than the Retirement age.';
                } else if ((parseFloat(this.value) < 20) || (parseFloat(this.value) > 80)) {
                    currentErrorMsg.innerHTML = ''
                    ageMinMax(this, 80, errorEleObj.currentErrorMsg, 20, "Your Maximum age should be 80", 'Your Current age should be greater than 20.')
                }
                else {
                    currentErrorMsg.innerHTML = '';
                }
                retirementCalculationFn()

            });
            allInputBox[1].addEventListener('keyup', function () {
                if ((parseFloat(this.value) <= parseFloat(allInputBox[0].value))) {
                    retirementErrorMsg.innerHTML = '';
                    retirementErrorMsg.innerHTML = 'Your retirement  age should be greater than the curent age.';
                } else if ((parseFloat(this.value) < 20) || (parseFloat(this.value) > 80)) {
                    retirementErrorMsg.innerHTML = ''
                    ageMinMax(this, 80, errorEleObj.retirementErrorMsg, 20, "Your Maximum  retirement age should be 80", 'Your retirement age should be greater than 20.')
                }
                else {
                    retirementErrorMsg.innerHTML = '';
                }
                retirementCalculationFn()
            });
            allInputBox[2].addEventListener('keyup', function () {
                if ((parseFloat(this.value)) < (parseFloat(allInputBox[1].value))) {
                    lifeErrorMsg.innerHTML = '';
                    lifeErrorMsg.innerHTML = 'Your life expectency age should be greater than the retirement age.';
                }
                else if ((parseFloat(this.value) < 0) || (parseFloat(this.value) > 100)) {
                    lifeErrorMsg.innerHTML = ''
                    ageMinMax(this, 100, errorEleObj.lifeErrorMsg, 0, "Your life expectancy age should be less than 100.", '')
                }
                else {
                    lifeErrorMsg.innerHTML = '';
                }
                retirementCalculationFn()

            });
            allInputBox[3].addEventListener('keyup', function () {
                currentExpenseErrorMsg.innerHTML = ''
                ageMinMax(this, 1000000000, errorEleObj.currentExpenseErrorMsg, 1, "Your current expence should not greater than the ₹100 crores.", 'Your current expence should be greater than the ₹1.')
                retirementCalculationFn()
            });
            allInputBox[4].addEventListener('keyup', function () {
                currentAccCorpusErrorMsg.innerHTML = ''
                    ageMinMax(this, 100000000000, errorEleObj.currentAccCorpusErrorMsg, 0, "Your current accumlated should not greater than the ₹10000 crore.",'')
                    retirementCalculationFn()
                
            });
        }
        function ageMinMax(ele, max, errorSpan, min, maxMsg, minMsg) {
            if (parseFloat(ele.value.replace(/,/g, '')) > max) {
                errorSpan.innerHTML = '';
                errorSpan.innerHTML = maxMsg;
            } else if (parseFloat(ele.value.replace(/,/g, '')) < min) {
                errorSpan.innerHTML = '';
                errorSpan.innerHTML = minMsg;
            } else {
                errorSpan.innerHTML = '';
            }
        }
        function retirementCalculationFn() {
            retirementInputObj = {
                calculator: "Retirement",
                currentExpenceInput: Number(parseFloat(document.getElementById("currentExpence").value.replace(/,/g, ''))),
                currentCorpusInput: Number(parseFloat(document.getElementById("currentCorpus").value.replace(/,/g, ''))),
                retirementAge: Number(document.getElementById("retirementAge").value),
                currentAge: Number(document.getElementById("currentAge").value),
                lifeExpectancy: Number(document.getElementById("lifeExpectancy").value),
                preRoi: Number(document.getElementById("preRoi").value) / 100,
                postRoi: Number(document.getElementById("postRoi").value) / 100,
                infletion: Number(document.getElementById("infletion").value) / 100,
            }
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");

            myWorker.postMessage(retirementInputObj);

            myWorker.onmessage = function (e) {
                retirementInputObj.retirementHighchartReact = retirementHighchartReact;
                retirementCalculatorRenderObj.renderRetirementCalculation(e, retirementInputObj);
                myWorker.terminate();
            }

        }
        function retirementHighchartReact(invest, interest, tInvest) {
            var reactChart = (interest / (invest + interest)) * 100;
            // Range slider end
            Highcharts.chart("retirement-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "105%",
                                innerRadius: "60%",
                                y: tInvest,
                            },
                        ],
                    },
                    {
                        name: "Interest earned ",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });
        }
        return jsHelper.freezeObj(retirementCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "retirementCalculatorBizObj", retirementCalculatorBizObj)
})(this || window || {});
  /*retirement calculator js end*/
