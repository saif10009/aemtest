Insurance calculator
====
The `Insurance calculator` component can be used to show insurance calculator in page. 



## Feature
* It is an static component.
* You can easily add this component in any page.    


## Client Libraries
The component provides a `moneyfy.insurance-calculator-box` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

