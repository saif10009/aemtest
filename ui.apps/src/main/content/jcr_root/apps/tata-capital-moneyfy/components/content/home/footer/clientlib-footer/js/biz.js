/*footer js*/
(function (_global) {
  var footerBizObj = (function (jsHelper) {
    var footerObj = {};

    document.addEventListener("DOMContentLoaded", function () {
      initializeFooter();
      adobeAnalytics();
      //getUserWatchList();
    });

    function adobeAnalytics() {

      $('[data-footertop] .footerTop li a').click(function (event) {
        event.preventDefault();
        var menuTitle = $(this).text().trim();
        menuInteraction(menuTitle, 'footer' , userIdObj.userId)
        location.href = $(this).attr('href')
      });

      $('.footer-logo').click(function (event) {
        event.preventDefault();
        menuInteraction('Footer Logo Image', 'footer' ,userIdObj.userId)
        location.href = $(this).attr('href')
      });

      $('[data-fappstorelink] a').click(function (event) {
        event.preventDefault();
        var checkVal = event.currentTarget.attributes[2].nodeValue;
        if (checkVal == 'app_store') {
          appStoreImgClick('App Store', userIdObj.userId, 'footer')
          window.open($(this).attr('href'))
        } else {
          appStoreImgClick('Google Play', userIdObj.userId, 'footer')
          window.open($(this).attr('href'))
        }
      });

    }

    function initializeFooter() {
      $('#jsLoginModalSlider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
      });

      $('#footer-accordian .footer-heading-top h5').each(function () {
        $(this).click(function () {
          $(this).toggleClass('active');
          $(this).parents('.footer-heading-top').siblings('.footer-heading-bottom').slideToggle();
          $(this).parents('.flex-row-col').siblings('.flex-row-col').find('.footer-heading-top h5').removeClass('active');
          $(this).parents('.flex-row-col').siblings('.flex-row-col').find('.footer-heading-bottom').slideUp();
        });
      });
    }

    return jsHelper.freezeObj(footerObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "footerBizObj", footerBizObj);
})(this || window || {});
/*/*footer js*/
