/*insurence banner js start*/
(function (_global) {
    var insurenceBannerBoxBizObj = (function (jsHelper) {
        var insurenceBannerBoxObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            if ($('.banner-image').length > 1) {
                insurenceBannerBoxSlick();
            }
            insurenceBannerBoxInitializer();
        })
        function insurenceBannerBoxSlick() {
            $('#jsBannerSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                autoplay: false,
            });
        }
        function insurenceBannerBoxInitializer(){
            setTimeout(function () {
                if ($('.banner-slider').hasClass('slick-initialized')) {
                  $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                  $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                  $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                  $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                }
              }, 2000);
        }


        return jsHelper.freezeObj(insurenceBannerBoxObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "insurenceBannerBoxBizObj", insurenceBannerBoxBizObj)
})(this || window || {});
  /*insurence banner js end*/
