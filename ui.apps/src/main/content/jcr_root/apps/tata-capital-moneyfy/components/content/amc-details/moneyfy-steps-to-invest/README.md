Steps To Invest
====
The `Steps To Invest` component is for displaying steps on how to start investing in moneyfy amc details page.



## Feature
* It is an authorable component.
* It is an multifield based component.
* All the various elements of the component such as title, subtitles, descriptions, & images are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title ./stepTitle` Used for rendering titles and the steps heading in the component.
2. `./stepsList` Used for rendering the steps in the component.
3. `./stepImage` Used to select the images in the component.
4. `./description` are used for the short description that can be given to the respective steps.
6. `./buttonText ./buttonLink ./newTab` are for adding the button text, button redirection link & checkbox to choose to open link in new tab.


## Client Libraries
The component provides a `moneyfy.moneyfy-steps-to-invest` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5