/*********************CITY TRIGGER - [START]************************/
var apiCall = {};
var calcObj = {};
(function(){
var otpRefNumber;

function fetchData(){
    var calc = $('[data-title="compound-calc"]').text();
    var calcType = $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g,'');

if (calc=='Compound Interest Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        investmentAmount:$('[data-type="investment"]').val(),
        expectedRoi: $('[data-type="roi"]').val(),
        tenureYears: $('[data-type="tenure"]').val(),
        futureValue: $('[data-type="futureVal"]').text().replace('₹ ',''),
        estimatedGain: $('[data-type="estimateGain"]').text().replace('₹ ',''),
    }
}else if(calc=='Life insurance Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        currentAge:$('[data-type="age"]').val(),
        retirementAge: $('[data-type="RetirementAge"]').val(),
        monthlyIncome: $('[data-type="monthlyIncome"]').val(),
        expectedIncrease: $('[data-type="expectedIncrease"]').val(),
        returnInvestment: $('[data-type="returnInvestment"]').val(),
        coverAmount: $('[data-type="coverAmount"]').text(),
    }
}else if(calc=='Systematic Investment Plan (SIP) Calculator' && calcType=='SIP'){
    calcObj = {
        calcType: $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g,'')+ ' Calculator',
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        investmentAmount:$('[data-type="investmentAmount"]').val(),
        expectedReturn: $('[data-type="expectedReturn"]').val(),
        investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') ? $('.year-wrap [data-type="investmentTenureSip"]').val() :  Math.round((Number($('.month-wrap [data-type="investmentTenureSip"]').val())/12).toString()),
        futureValue: $('[data-type="futureValue"]').text().replace('₹ ',''),
        estimatedGain: $('[data-type="estimatedGain"]').text().replace('₹ ',''),
    }
}else if(calc=='Systematic Investment Plan (SIP) Calculator' && calcType=='Lumpsum'){
    calcObj = {
        calcType: $('.js-CalculatorTabList').find('li').find('.active').text().replace(/\s+/g,'')+ ' Calculator',
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        investmentAmount:$('[data-type="investmentAmountLump"]').val(),
        expectedReturn: $('[data-type="expectedReturnLump"]').val(),
        investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') ? $('.year-wrap [data-type="investmentTenureLumpsum"]').val() :  Math.round((Number($('.month-wrap [data-type="investmentTenureLumpsum"]').val())/12)).toString(),
        futureValue: $('[data-type="futureValueLump"]').text().replace('₹ ',''),
        estimatedGain: $('[data-type="estimatedGainLump"]').text().replace('₹ ',''),
    }
}else if(calc=='EMI Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        principalAmount:$('[data-type="principalAmount"]').val(),
        expectedInterestRate: $('[data-type="expectedInterestRate"]').val(),
        investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') ? $('.year-wrap [data-type="investmentTenure"]').val() :  Math.round((Number($('.month-wrap [data-type="investmentTenure"]').val())/12)).toString(),
        totalAmount: $('[data-type="totalAmount"]').text().replace('₹ ',''),
        emiValue: $('[data-type="emiValue"]').text().replace('₹ ',''),
    }
}else if(calc=='Recurring Deposit Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        monthlyContribution:$('[data-type="monthlyContribution"]').val(),
        expectedReturn: $('[data-type="expectedReturn"]').val(),
        investmentTenureYears: document.querySelector('[data-yrmthfilter="year"]').classList.contains('active') ? $('.year-wrap [data-type="investmentTenureSip"]').val() :  Math.round((Number($('.month-wrap [data-type="investmentTenureSip"]').val())/12)).toString(),
        expectedMaturity: $('[data-type="expectedMaturity"]').text().replace('₹ ',''),
        earningAmount: $('[data-type="earningAmount"]').text().replace('₹',''),
    }
}else if(calc=='Fixed Deposit Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        lumpsumInvestment:$('[data-type="lumpsumInvestmnet"]').val(),
        expectedRate: $('[data-type="expectedRate"]').val(),
        investmentTenure: $('[data-type="investmentTenure"]').val(),
        compoundingTenure: $('.compound-tenure-radio').find('input:checked').val(),
        maturityValue: $('[data-type="maturityValue"]').text().replace('₹ ',''),
        earningAmount: $('[data-type="earningAmounting"]').text().replace('₹ ',''),
    }
}else if(calc=='PPF Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        yearlyContribution:$('[data-type="yearlyContribution"]').val(),
        expectedReturn: $('[data-type="expectedReturn"]').val(),
        tenureYears: $('[data-type="tenure"]').val(),
        maturityValue: $('[data-type="maturityValue"]').text().replace('₹ ',''),
        earningAmount: $('[data-type="earningAmounting"]').text().replace('₹',''),
    }
}else if(calc=='Retirement Calculator'){
    calcObj = {
        calcType: $('[data-title="compound-calc"]').text(),
        custName: $('[data-type="name"]').val(),
        mobileNumber: $('[data-type="mobile"]').val(),
        city: $('[data-type="city"]').val(),
        emailId: $('[data-type="email"]').val(),
        currentAge:$('[data-type="CurrentAge"]').val(),
        monthlyExpense: $('[data-type="monthlyExpence"]').val(),
        accumulatedSavings: $('[data-type="accumulatedSaving"]').val(),
        expectedReturn: $('[data-type="Inflation"]').val(),
        expectedPreRetirementReturn: $('[data-type="preRetirmentReturn"]').val(),
        expectedPostRetirementReturn: $('[data-type="postRetiremnetReturn"]').val(),
        retirementAge: $('[data-type="retirementAge"]').val(),
        costOfLiving: $('[data-type="costOfLiving"]').val(),
        monthlySIP: $('[data-type="monthlySIP"]').text().replace('₹ ',''),
        expectedYearlyExpenditure: $('[data-type="expectedYearlyExpenditure"]').text().replace('₹ ',''),
        accumulatedRetirementKitty: $('[data-type="accumulatedRetirementKitty"]').text().replace('₹ ',''),
    }
}
}



function citySearchApiCall(method, url, data) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                switch (this.status) {
                    case 200:
                        resolve(this.responseText);
                        break;
                    default:
                        reject(this.responseText);
                }
            }
        }
        xhr.open(method, url);
        xhr.send(data);
    });
}

$(function () {
    if ($(".city").length) {
        $.ajax({
            url: '/content/tata-capital/mdm.cityproductmaster.json',
            type: "GET",
            async: false,
            success: function (data, status, xhr) {
                var obj = JSON.parse(xhr.responseText);
                var mainObj = {};

                for (var item in obj.Master) {
                    var subObj = {};
                    if (!mainObj.hasOwnProperty(obj.Master[item].product)) {
                        var arr = [];
                        mainObj[obj.Master[item].product] = subObj;
                        subObj['productName'] = obj.Master[item]['product-name'];
                        arr.push(obj.Master[item].city);
                        subObj['cities'] = arr;

                    } else {
                        var subObj = mainObj[obj.Master[item].product];
                        var arr = subObj['cities']
                        arr.push(obj.Master[item].city);
                    }
                }
                response = mainObj;
                var productCode = 'PL';
                $(".city").html();
                $(".city").append("<option value=''></option>");
                // $(".city").append('<li><a href="javascript:void(0)"></a></li>')

                if (productCode == "LAP" || productCode == "LAPOD") {
                    productCode = "HE";
                }
                //                    if ((productCode == "MO101") || (productCode == "TR102") || (productCode == "HE103") || (productCode == "HO104") || (productCode == "PR105") || (productCode == "SIUL106") || (productCode == "SITR107") || (productCode == "HELI108")) {
                //                        productCode = "INSURANCE-HOME";
                //                    }
                if ((productCode == 'TW101') || (productCode == 'MO101') || (productCode == 'PR105') || (productCode == 'SITR107') || (productCode == 'HE103') || (productCode == 'HE104') || (productCode == 'HE105') || (productCode == 'WS101') || (productCode == 'WP101') || (productCode == 'HA101') || (productCode == 'HA101') || (productCode == 'HC101') || (productCode == 'CS101') || (productCode == 'PE101') || (productCode == 'RS101') || (productCode == 'CP101') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'CIS101') || (productCode == "HO104")) {
                    productCode = "INSURANCE";
                }
                if (response[productCode] !== undefined && response[productCode].cities.length > 0) {
                    response[productCode].cities.sort(function(a, b){
                        if(a < b) { return -1; }
                        if(a > b) { return 1; }
                        return 0;
                    })
                    .forEach(function (element) {
                        $(".city").append('<option value=' + element + '>' + element + '</option>');
                        // $(".city").append('<li><a href="javascript:void(0)">'+ element + '</a></li>')
                        
                    });
                } else {
                    console.log("CSV City issue");
                }
            },
            error: function () {

                console.log("FAIL");
            }
        });
    }
}); 

/*********************CITY TRIGGER - [END]************************/

var generateOTPSuccess = ( '<button data-dismiss="popover-modal" class="popover-modal-close jsGetResultClose"><i class="icon-close"></i></button>'+
'              <div class="get-result-forms" id="jsVeryfyOTP">'+
'                <h4>Verify OTP</h4>'+
'                <p class="medium">An OTP is sent to your registered mobile number +91 ********<span class="jsShowWhatsappNumber">85</span></p>'+
''+
'                <div class="whatsapp-otp-verify">'+
'                  <div class="enter-otp-box">                  '+
'                    <div class="otp-col js-OtpBox">'+
'                      <input type="text" class="input-textbox only-numeric-input" maxlength="1">'+
'                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1"> '+
'                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1">'+
'                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1">'+
'                    </div> '+
'                    <div class="resend-otp">'+
'                      <!-- <span><i class="icon-watch"></i> 00:23</span> -->'+
'                      <a href="javascript:;" class="resend js-resendOTP">Resend</a>'+
'                    </div>            '+
'                  </div>     '+
'                </div>               '+
'                '+
'                <div class="btn-proceed-whatsapp">'+
'                  <a href="javascript:void(0);" class="btn-blue radius100 jsVeryfyBtn btn-disabled">Proceed</a>'+
'                </div>'+
'              </div>');

//   var onGenerateOtpSuccess = function () {
//             $("#jsVeryfyOTP .js-OtpBox .input-textbox").keyup(function () {
//                 if (this.value.length == this.maxLength) {
//                     $(this).next('.input-textbox').focus();
//                     $(this).next('.input-textbox').removeClass('pointer-none');
//                 } else {
//                     $(this).prev('.input-textbox').focus();
//                     $(this).addClass('pointer-none');
//                     $('#jsVeryfyOTP .input-textbox:first').removeClass('pointer-none');
//                 }
    
//                 var ele_input = $('.js-OtpBox .input-textbox');
//                 $(ele_input).each(function () {
//                     if ($(this).val().length != 0) {
//                         $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').removeClass('btn-disabled');
//                         $(this).parents('.form-group').addClass('active');
//                     }
//                     else {
//                         $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
//                     }
//                 });
//             });
    
//             $('.jsVeryfyBtn').click(function () {
//                 $('#jsGetResultForms .input-textbox').val('');
//                 var values = []
//                 $('#jsVeryfyOTP .js-OtpBox .input-textbox').each(function (i, ele) { values.push(ele.value) });
//                 console.log(values.join(""))
//                 apiCall.verifyOTP();
//             })
//             $('#jsVeryfyOTP .js-resendOTP').click(function () {
//                 $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox').val('');
//                 $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
//                 $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
//                 var getMobileNumber = calcObj.mobileNumber;
//                 var firstTwoDigit = getMobileNumber.toString().slice(-2);
//                 document.querySelector('#jsVeryfyOTP .jsShowWhatsappNumber').innerText = firstTwoDigit;
//                 apiCall.generateOTP();
//             });
    
//             $('.jsGetResultClose').click(function () {
//                 $('[data-forms]').removeClass('d-none');
//                 $('[data-otpverify]').addClass('d-none');
//                 $('[otp-successfull]').addClass('d-none');
//                 $('[otp-unsuccessfull]').addClass('d-none');
//                 $('#jsGetResultForms .input-textbox').val('');
//                 $('#jsGetResultForms .form-group').removeClass('textboxerror');
//                 $('#jsGetResultForms .input-textbox').next().text('');
//                 $('#jsGetResultForms .single-select3').val(null).trigger('change');
//                 $('#jsGetResultForms [data-type="checkbox"]').prop("checked", false);
//             })
//         };
/*********************GENERATE OTP - [START]************************/
function generateOTP() {
    fetchData();
    var otpMobileNumber = {
        otpMobile: $('[data-type="mobile"]').val()
    };
    //apiCall('POST', '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp',JSON.stringify())
    $('.js-loader').addClass('show');
    $.ajax({
        url: '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp',
        type: "POST",
        async: false,
        data: otpMobileNumber,
        success: function (data, status, xhr) {
             $('.js-loader').removeClass('show');
            otpRefNumber = data.otpRefNo;
             $('.js-OtpBox input:nth-child(1)').focus();
             $('body').addClass('popover-modal-open');
            document.querySelector('[data-otpverify="verifyOTP"]').innerHTML = generateOTPSuccess;
            onGenerateOtpSuccess()
        } , 
        failure : function(data,status,xhr){
            $('.js-loader').removeClass('d-none');
            $('.js-loader').removeClass('show');
            $('#getResulton .input-textbox').val('');
            $('[data-otpverify="verifyOTP"]').addClass('d-none');
            $('[data-thank="thankYou"]').removeClass('d-none')
            $('#getResulton').addClass('popover-show');
            $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
            '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
            '<h3>Verification unsuccessful</h3>'+
            '<div class="get-edit-link">'+
            '<p>'+ data.errorMessage +'</p>'+ '</div>' +
            '<div class="btn-try-again">'+
            '<a href="javascript:void(0);" class="btn-blue radius100 jsTryAgainOtp">try again</a>'+
            '</div>')
        } 
    })
}
/*********************GENERATE OTP - [END]************************/

/*********************VERIFY OTP - [START]************************/

function verifyOTP() {
$('.js-loader').removeClass('show');
    var array = [];
    $('.js-OtpBox input').each(function (e, i) {
        array.push(i.value)
    })
    array.join('')
    var verifyMobileNumber = {
        "otpRefNumber": otpRefNumber,
        "otpValue": array.join('')
    };
    //apiCall('POST', '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp', otpMobileNumber)

    $('.js-loader').removeClass('d-none');
    $('[data-otpverify="verifyOTP"').addClass('d-none');

    $.ajax({
        url: '/content/tata-capital-moneyfy/moneyfyapi.verifyOtp',
        type: "POST",
        async: true,
        dataType: "json",
        contentType: "application/json",
        data: verifyMobileNumber,
        success: function (data, status, xhr) {
            if (data.retStatus.toUpperCase() == 'SUCCESS') { 
                $.ajax({
                    url: '/content/tata-capital-moneyfy/moneyfyapi.calcEmailServlet',
                    type: "POST",
                    async: true,
                    data : JSON.stringify(calcObj),
                    dataType: "json",
                    contentType: "application/json",
                    success: function (data, status, xhr) {
                        if ((data.status).toUpperCase() == 'SUCCESS') {
                            $('.js-loader').addClass('d-none');
                            $('.js-loader').removeClass('show');
                            $('#getResulton .input-textbox').val('');
							$('[data-thank="thankYou"]').removeClass('d-none');
                            $('.get-result-thankyou').html('<div class="get-result-thankyou"><img src="/content/dam/tata-capital-moneyfy/calculator-template-image/success.png" alt="" class="mgs-icons"><h3>Thank you</h3><p>The results have been sent to your email address at <span class="js-encryptEmail">'+ data.emailId +'</span></p>')
                        }else{
                            $('.js-loader').removeClass('d-none');
                            $('.js-loader').removeClass('show');
                            $('#getResulton .input-textbox').val('');
                            $('[data-otpverify="verifyOTP"]').addClass('d-none');
                            $('[data-thank="thankYou"]').removeClass('d-none')
                            $('#getResulton').addClass('popover-show');
                            $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
                            '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
                            '<h3>Verification unsuccessful</h3>'+
                            '<div class="get-edit-link">'+
                            '<p>'+ data.errorMessage +'</p>'+ '</div>' +
                            '<div class="btn-try-again">'+
                            '<a href="javascript:void(0);" class="btn-blue radius100 jsTryAgainOtp">try again</a>'+
                            '</div>')
                        }
                    }
                })
            } else if (data.retStatus.toUpperCase() === 'FAILURE') {
                $('.js-loader').removeClass('d-none');
                $('.js-loader').removeClass('show');
                $('#getResulton .input-textbox').val('');
                $('[data-otpverify="verifyOTP"]').addClass('d-none');
                $('[data-thank="thankYou"]').removeClass('d-none')
                $('#getResulton').addClass('popover-show');
                $('.get-result-thankyou').html('<div class="get-result-thankyou">'+
                '<img src="/content/dam/tata-capital-moneyfy/calculators/unsuccessful.svg" alt="" class="mgs-icons"> '+
                '<h3>Verification unsuccessful</h3>'+
                '<div class="get-edit-link">'+
                '<p>'+ data.errorMessage +'</p>'+ '</div>' +
                '<div class="btn-try-again">'+
                '<a href="javascript:void(0);" class="btn-blue radius100 jsTryAgainOtp">try again</a>'+
                '</div>')
            }
        } 
    })

} 
/*
function modalCallapi(method, url, data) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                switch (this.status) {
                    case 200:
                        resolve(this.responseText);
                        break;
                    default:
                        reject(this.responseText);
                }
            }
        }
        xhr.open(method, url);
        xhr.send(data);
    });
}

modalCallapi('POST', '/content/tata-capital-moneyfy/moneyfyapi.verifyOtp', verifyMobileNumber).then(function (response) {
    console.log(response);
    var responseObj = JSON.parse(response)
    console.log(responseObj);

})*/

apiCall.citySearchApiCall = citySearchApiCall;
apiCall.generateOTP = generateOTP;
apiCall.verifyOTP = verifyOTP;

apiCall.calcObj = calcObj;
})();
/*********************VERIFY OTP - [END]************************/
