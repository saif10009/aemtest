/*rd calculator js start*/
(function (_global) {
    var rdCalculatorBizObj = (function (jsHelper) {
        var rdCalculatorObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            rdCalculatorInitializer();
            rdCalculatorEvents();
            rdHighchartReact(1800000, 191219, 100);
            if ($('[data-rdcalbtn]') != undefined) {
                $('[data-rdcalbtn]').click(function () {
                    var ctaText = $(this).text().trim();
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('rd-calculator', userId, 'recurring deposit calculator', ctaText);
                        }else{
                            calculatorCtaInteraction('rd-calculator', userId, 'recurring deposit calculator', ctaText);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('rd-calculator', "anonymous user", 'recurring deposit calculator', ctaText);
                        }else{
                            calculatorCtaInteraction('rd-calculator', "anonymous user", 'recurring deposit calculator', ctaText);
                        }
                    }
                })
            }
        })

        function rdCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }

        }
        function rdCalculatorEvents() {
            var tenureObj = {
                rdTenureYear: document.getElementById("rd_tenure_year"),
                rdTenureMonth: document.getElementById("rd_tenure_month"),
                rdErrorMsg: document.getElementById("rdErrorMsg"),
            }
            // year month toggle
            $('[data-yrMthFilter]').click(function () {
                filterVal = $(this).attr('data-yrMthFilter');
                $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
                $(this).addClass('active');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
            });
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end
             $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            //calculator js
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").on("input", function () {
                    var activeBtn = document.querySelector('[class="yr-mth-btn active"]');
                    if (activeBtn.innerHTML == 'Years') {
                        var convertYears = Number(tenureObj.rdTenureYear.value) * 12;
                        rdCalculationFn(convertYears)
                    } else {
                        rdCalculationFn(Number(tenureObj.rdTenureMonth.value));
                    }
                });
            }
            document.getElementById("rd_amount").addEventListener('keyup', function () {
                if (parseFloat(this.value.replace(/,/g, '')) > 1000000) {
                    tenureObj.rdErrorMsg.innerHTML = '';
                    tenureObj.rdErrorMsg.innerHTML = 'Amount should not greater than ₹1000000';
                } else if (parseFloat(this.value.replace(/,/g, '')) < 500) {
                    tenureObj.rdErrorMsg.innerHTML = '';
                    tenureObj.rdErrorMsg.innerHTML = 'Amount should not less than ₹500';
                } else {
                    tenureObj.rdErrorMsg.innerHTML = '';
                }
                if (document.querySelector('[class="yr-mth-btn active"]').innerHTML == 'Years') {
                    rdCalculationFn(Number(tenureObj.rdTenureYear.value) * 12)
                } else {
                    rdCalculationFn(Number(tenureObj.rdTenureMonth.value));
                }
            })
            $('[data-yrMthFilter="year"]').click(function () {
                rdCalculationFn(Number(tenureObj.rdTenureYear.value) * 12);
            })
            $('[data-yrMthFilter="month"]').click(function () {
                rdCalculationFn(Number(tenureObj.rdTenureMonth.value));
            })
        }

        function rdCalculationFn(rdTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");

            var rdInputObj = {
                calculator: "RD",
                rdAmount: Number(parseFloat(document.getElementById("rd_amount").value.replace(/,/g, ''))),
                rdTenure: rdTenure,
                roi: Number(document.getElementById("rd_roi").value) / 100,
            }
            myWorker.postMessage(rdInputObj);
            myWorker.onmessage = function (e) {
                rdInputObj.rdHighchartReact = rdHighchartReact;
                rdCalculatorRenderObj.renderRdCalculation(e, rdInputObj);
                myWorker.terminate();
            }
            
        };

        function rdHighchartReact(invest, interest, initialAxis) {
            var reactChart = (invest / (invest + interest)) * 100;
            // Range slider end
            Highcharts.chart("rd-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });

        }
        return jsHelper.freezeObj(rdCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "rdCalculatorBizObj", rdCalculatorBizObj)
})(this || window || {});
/*rd calculator js end*/