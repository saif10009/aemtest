/*top picks js start*/
(function (_global) {
    var topPicksRenderFn = (function (jsHelper) {
        var topPicksRenderObj = {}
        var renderTopPicks = function (catType) {
            for (var category in catType) {
                var renderHtmlStr = '';
                var riskType = {
                    "low": "low",
                    "low to moderate": "low-moderate-risk",
                    "moderate": "moderate",
                    "moerately high": "moderate-risk",
                    "high": "high-risk",
                    "very high": ""
                }
                var schemeId;
                var source = window.location.href;
                var investNowRedirection;
                var topPicsData = document.querySelector('[data-attr="' + category + '"]');
                catType[category].forEach(function (element) {
                    schemeId = element.schemeId;
                    investNowRedirection = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemeId + '&source=' + source;
                    renderHtmlStr += '<div class="top-pickup-ul" data-schemeid="' + element.schemeId + '">' +
                        '<div class="top-pickup-li">' +
                        '<div class="card-div top-pickup-card">' +
                        '<div class="top-pickup-top">' +
                        '<div class="pickup-text-logo">' +
                        '<span class="pickup-logo">' +
                        '<img src="' + (element.iconPath == "" ? '/content/dam/tata-capital-moneyfy/amc-icons/moneyfy-default-fund-icon.png' : element.iconPath) + '" alt="">' +
                        '</span>' +
                        '<h5>' + element.fundName + '</h5>' +
                        '                          </div>' +
                        '                          <div class="top-pickup-progress">' +
                        '                            <div class="pickup-progrees">' +
                        '                                <img src="/content/dam/tata-capital-moneyfy/mutual-funds/progress.svg" alt="">' +
                        '                                <div class="custom-tooltips ' + riskType[element.riskType.toLowerCase()] + '">' +
                        '                                  <div class="custom-tooltips-inner">' +
                        '                                    <span>' + element.riskType + '</span>' +
                        '                                  </div>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                          </div>' +
                        '                          <div class="pickup-rows">' +
                        '                            <div class="pickup-left">' +
                        '                              <p>NAV</p>' +
                        '                              <span class="pickup-dates">As on ' + (element.updatedDate == '' ? '-' : element.updatedDate) + '</span>' +
                        '                              <h4>' + element.nav + '</h4>' +
                        '                            </div>' +
                        '                            <div class="pickup-right">' +
                        '                              <div class="pickup-ul">' +
                        '                                <div class="pickup-li">' +
                        '                                  <p>1Y</p>' +
                        '                                  <h6><span class="' + (Number(JSON.parse(element.cagr1y).value) > 0 ? "icon-caret-top" : "icon-caret-down") + '"></span>' + JSON.parse(element.cagr1y).value + '</h6>' +
                        '                                </div>' +
                        '                                <div class="pickup-li">' +
                        '                                  <p>3Y</p>' +
                        '                                  <h6><span class="' + (Number(JSON.parse(element.cagr3y).value) > 0 ? "icon-caret-top" : "icon-caret-down") + '"></span>' + JSON.parse(element.cagr3y).value + '</h6>' +
                        '                                </div>  ' +
                        '                              </div>' +
                        '                            </div>' +
                        '                          </div>' +
                        '                        </div>' +
                        '                        <div class="top-pickup-bottom">' +
                        '                          <div class="top-pickup-btn-ul">' +
                        '                            <div class="top-pickup-btn-li">' +
                        '                              <a href="' + investNowRedirection + '" type="button" class="btn-blue btn-blue-outline btn-h40 radius100 w-100" data-sw-iframe-component="topPicks" data-investnow="true" data-sw-iframe="true" data-sw-iframe-url="' + investNowRedirection + '">Invest Now</a>' +
                        '                            </div>' +
                        '                            <div class="top-pickup-btn-li">' +
                        '                              <a href="' + element.pagePath + '" target="_blank" class="btn-view-all btn-h40" tabindex="0" data-toppicksexplore="topPicksExploreMore" data-sw-iframe-component="topPicks" data-sw-iframe="true" data-sw-iframe-url="' + element.pagePath + '">Explore more <span class="icon-arrow-right"></span></a>' +
                        '                            </div>' +
                        '                          </div>' +
                        '                        </div>' +
                        '                      </div>' +
                        '                    </div>' +
                        '                  </div>';
                });
                if (catType[category] == 0) {
                    topPicsData.innerHTML = '<center>' + 'No Mutual Funds Found' + '</center>';
                } else {
                    topPicsData.innerHTML = renderHtmlStr;
                }

            }
        }
        topPicksRenderObj.renderTopPicks = renderTopPicks;
        return jsHelper.freezeObj(topPicksRenderObj);
        //adobe analytics 64
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "topPicksRenderObj", topPicksRenderFn)
})(this || window || {});
/* top picks js end */