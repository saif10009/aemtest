About us leadership box
====
This is a `About us leadership box` component used in about us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as images, headings, title and button txt is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading ./leadershipHeading` Used for rendering heading of the component.
2. `./leadershipDescription` Used for rendering description of the component.
3. `./leadershipLogoImage` Used for rendering image on page.
5. `./leadershipButton , ./button` Used to rendering the name of the button.
6. `./leadershipButtonLink , ./buttonLink` Used to render redirection link of a card.
7. `./leadershipButtonTarget , ./buttontnTarget` Enable open redirection link in a new tab.
8. `./leadershipItemMultifield` Used to create multifield in component.


## Client Libraries
The component provides a `moneyfy.about-our-leadership-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5