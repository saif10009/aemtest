(function (_global) {
    var customerCareApiFnObj = (function (jsHelper) {
        var customerCareApiObj = {};
        var customerCare = function (data) {
            return apiUtility.downloadAppApiFn(data);
        }
        customerCareApiObj.customerCare = customerCare;

        return jsHelper.freezeObj(customerCareApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'customerCareApiObj', customerCareApiFnObj);
})(this || window || {});