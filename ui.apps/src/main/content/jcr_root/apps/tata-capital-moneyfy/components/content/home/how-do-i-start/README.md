How do I start
====
This is a `How do I start` component which shows three simple steps to start investing with Moneyfy. 

## Feature
* The component renders three steps to get started with Moneyfy.


### Edit Dialog Properties
Since it is an static component there are no properties in edit dialog.


## Client Libraries
The component provides a `moneyfy.howDoIstart` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is already included in component HTML file.
The clientlib of this component are embedded in the `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib` folder. 


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5