Mutual Fund Description
====
The `Mutual Fund Description` component is for displaying detail info of amc on amc-details page.



## Feature
* The data is populated using pageProperties.


## Edit Dialog Properties
There are no properties stored in edit dialog of the component.


## Client Libraries
The component provides a `moneyfy.mutual-fund-description` editor client library category that includes JavaScript and CSS.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5