Know more
====
The `Know more` component can be used to show more information about the investing or emi plans in page. 



## Feature
* It is an authrable component.
* You can easily add this component in any page.
* All the various elements of the component such as images, headings, title and button txt is authorable.
* One can easily edit the contents of the component with the help of edit dialog. 


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./color` Used for select color of the heading of row.
2. `./heading ./rowHeading, ./innerHeading` Used for rendering heading of the component.
3. `./rowImage ./hoverImage` are used to select the different images paths of these component.
4. `./rowDescription, ./descritption` render description of components.
5. `./knowMoreMultifield, ./knowMoreInnerMultifield` used for add multifield 


## Client Libraries
The component provides a `moneyfy.know-more-box` editor client library category that includes CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

