FAQ Card
====
`FAQ Card` component used to author FAQ's on a page.

## Feature
* This is a nested multifield component.
* It shows popular question answers about Mutual Funds.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used to render the heading of a component.
2. `./tabTitleMobile` Used to render the heading for mobile view of a component.
3. `./dropdownList` It is used to create Multifield.
4. `./tabTitleCard` Used to render heading of cards.
5. `./faqList` It is used to create Nested Multifield.
6. `./questions`Used to render questions.
7. `./description` Used to render answers.

## Client Libraries
The component provides a `moneyfy.faq-card` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.faqs-detail-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-faqs-detail`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5