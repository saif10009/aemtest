/*fund manager list  biz.js start*/
(function (_global) {
    var fundManagersFundListBizFn = (function (jsHelper) {
        var fundManagersFundListObj = {}
        var numberOfFundsPerPage = {
            'fundManagerEquity' : 8,
            'fundManagerDebt' : 8,
            'fundManagerHybrid' : 8,
        };
        var searchmutualFundList = [];
        var schemeIdMapObj = {};
        var portfolioFundIds = [];
        var watchlistFunds;
        var addFromType = '';
        var checkedFund = [];
        var selectedFundObj = {};
        var compareWrap = $('.compare-choose-wrap');
        if ($(window).width() < 991) {
            var compareWrap = $('.mob-filter-container .compare-choose-wrap');
        }
        document.addEventListener('DOMContentLoaded', function () {
            loader();
            // condition for rendering funds based on login state
            if (headerBizObj.getUserObj().isLoggedIn) {
                getUserWatchList(renderFunds);
            } else {
                renderFunds();
            }

            //clear all checkBox onLoad
            setTimeout(function () {
                document.querySelectorAll('input[type="checkbox"]:checked').forEach(function (checkbox) {
                    checkbox.checked = false;
                });
            }, 200);

            compareFunds();
            compareFundSearchTypeahead();
            initializeEvents();

            $('#compareFunds').click(function () {
                location.href = "/content/tata-capital-moneyfy/en/compare-mutual-funds.html"
            });

            document.querySelector('[data-watchlist="portfolio-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromPortfolio').classList.remove('popover-show');
                document.getElementById('addFromPortfolio').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetWatchlist + '"] [data-fetchwatchlist="watchListBtn"]').click();
            });

            document.querySelector('[data-portfolio="watchlist-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromWishList').classList.remove('popover-show');
                document.getElementById('addFromWishList').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetPortfolio + '"] [data-fetchportfolio="portfolioBtn"]').click();
            });

        });

        function loader() {
            $('.js-loader').addClass('show');
            setTimeout(function () { 
                $('.js-loader').removeClass('show');
            }, 2000);
        }

        function showFundsAndSortbyEvent() {
            $('.jsDropDownMenuGet li a').click(function (event) {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');
                $('[data-filter-card]').slideUp();
                $(this).find(".jsPagination").empty();
                var category = $(this).parents('.fund-content-row').data('type');
                console.log(category);
                if ($(this).parents('.jsDropDownMenuGet').hasClass('fundSize-filter')) {
                    fundsShown(event, category);
                } else {
                    var sortBy = event.target.innerText.toLowerCase();
                    console.log(sortBy);
                    filterFunds(sortBy.toLowerCase(), category);
                }

                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });
        }

        function getUserWatchList(callRenderFunds) {
            if (headerBizObj.getUserObj().isLoggedIn) {
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "tenure": "3y"
                    }
                }
                fundManagerFundListApiObj.fetchwatchListApi(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        var watchListArray = responseData.responseJson.body.watchListDetails;
                        watchListArray.forEach(function (fund) {
                            watchListSchemeIds.push(fund.schemeId);
                        });
                        if (jsHelper.isDef(callRenderFunds)) {
                            callRenderFunds();
                        }
                    } else {
                        if (jsHelper.isDef(callRenderFunds)) {
                            callRenderFunds();
                        }
                    }
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }

        function addWatchListBizFn(schemeId, schemeName) {
            if (!headerBizObj.getUserObj().isLoggedIn) {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain + '?action=watchlist&schemeId=' + schemeId + '&source=' + window.location.href
                });
            } else {
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy",
                        "schemeId": schemeId,
                    }
                };
                fundManagerFundListApiObj.addTowatchListApi(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        if (responseData.responseJson.body.status.statusMessage == 'Fund added to Watchlist') {
                            if ($('.watchListError').hasClass('d-none')) {
                                $('.watchListError').removeClass('d-none');
                            }
                            watchListSchemeIds.push(schemeId);
                            document.getElementById('addWatchlistMsg').innerHTML = 'Fund added to Watchlist';
                            showAddWatchListPopup();
                            document.querySelector('[data-schemeid="' + schemeId + '"]').querySelectorAll('.js-addWatchlist').forEach(function (element) {
                                element.classList.add('addedWatchlist');
                                $('[data-schemeid="' + schemeId + '"] .wishlist-text').html('Added To Watchlist');
                            });
                        }
                    } else {
                        showWatchListErrorPopup();
                    }
                }).catch(function (error) {
                    console.log(error);
                    showWatchListErrorPopup();
                });
            }
        }

        function reInitializeEvents(category) {
            $('[data-type="' + category +'"]' + ' [data-viewdetailbtn]').click(function (event) {
                event.preventDefault();
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                fundViewDetails(fundType, fundName, fundRiskCat, userIdObj.userId);
                window.open($(this).attr('href'))
            })

            // add to watchList
            $('[data-type="' + category +'"]' + ' [data-addwatchlist]').click(function () {
                var schemeId = $(this).parents('.fund-list-li').data('schemeid');
                var schemeName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                addToWishList(fundType, fundName, fundRiskCat, userIdObj.userId)
                if (!$(this).hasClass('addedWatchlist')) {
                    addWatchListBizFn(schemeId, schemeName);   
                } else {
                    showWatchListExistPopup();
                }
            });

            // reIntialize InvestNow
            $('[data-type="' + category +'"] [data-investnow="true"]').click(function () {
                var schemaId = $(this).parents('.fund-list-li').data('schemeid');
                var source = window.location.href;
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                fundInvestNow(fundType, fundName, fundRiskCat, userIdObj.userId);
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                    });
                } else {
                    location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                }
            });

            $('[data-type="' + category +'"]' + ' .js-viewMoreToggleBtn').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('active');
                $(this).find('.text-wrap').text(function (i, text) {
                  return text === "More" ? "Less" : "More";
                });
                var toggleDiv = $(this).attr('data-btn');
                $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });
            
            // add compare check js
            $('[data-type="' + category +'"]' + ' .js-addCompare').change(function () {

                var fundName = $(this).parents('.similar-mutual-funds-items').find('.fund-name').data('schemename');
                var schemeID = $(this).parents('.fund-list-li').data('schemeid');
                var fundLength = 3;
                if (window.outerWidth < 768) {
                    document.getElementById('compareCheck').innerHTML = "Compare Upto 2 Funds";
                    $('[data-select-modal="searchFund-modal-3"]').addClass('d-none');
                } else {
                    document.getElementById('compareCheck').innerHTML = "Compare Upto 3 Funds"
                }

                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                if (Object.values(selectedFundObj).length > fundLength) {
                    $(this).find('input').prop("checked", false);
                } else {
                    if ($(this).find('input').is(':checked')) {
                        var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                        var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                        var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                        compareFund(fundType, fundName, fundRiskCat, userIdObj.userId)
                        checkedFund.push(fundName);
                        if (checkedFund.length > fundLength) {
                            $(this).find('input').prop("checked", false);
                            checkedFund.pop(fundName);
                        }
                        if (Object.values(selectedFundObj).length < fundLength) {
                            selectedFundObj[fundName] = schemeID;
                        }
                    } else {
                        delete selectedFundObj[fundName]
                        checkedFund = jQuery.grep(checkedFund, function (value) {
                            return value != fundName;
                        });
                    }
                    compareWrap.each(function (i) {
                        $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);
                        if (checkedFund[i] != undefined) {
                            $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                            $(this).find('.js-chooseCompareBtn').addClass('d-none');
                        } else {
                            $(this).find('.js-selectedCompareBtn').addClass('d-none');
                            $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                        }
                    });
                }


                if (checkedFund.length >= 1) {
                    $('.js-filtersBox').addClass('d-none');
                    $('.js-compareBox').removeClass('d-none');
                    $('.js-filterWrap').addClass('show-compare');
                    $('.js-compareActive').addClass('has-compare');
                    $('.js-compareActiveNone').addClass('has-opacity');
                } else {
                    showFiterBox();
                    // 07-03-2022 start
                    removeWishlistModal();
                    // 07-03-2022 end
                }

                // 07-03-2022 start
                $('.js-fundCount').text(checkedFund.length);
                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                // 07-03-2022 end
                setSessionSchemeId(selectedFundObj);
            });

            if ($(window).width() < 768) {
                $('[data-type="' + category +'"]' + ' .js-similarMFcard').click(function(e){
                  $(this).find('.tag-more-wrap .js-viewMoreToggleBtn[data-btn]').trigger('click');
                });
                $('[data-type="' + category +'"]' + " .js-similarMFcard .form-select2, .js-similarMFcard .watchlist-compare-btn, .js-similarMFcard .invest-detail-wrap").click(function (e) {
                  e.stopPropagation();
                });
              }
        }

        /* compare funds biz js */
        function searchApi(requestData) {
            var requestObj = {
                "body": {}
            };
            requestObj.body.searchTerm = requestData;
            fundManagerFundListApiObj.compareSearch(requestObj).then(function (response) {
                if (response.status.toLowerCase() == 'success') {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    responseData = responseData.responseJson.results;
                    if (responseData != undefined) {
                        searchmutualFundList.length = 0;
                        schemeIdMapObj = {};
                        responseData.forEach(function (fundObj) {
                            searchmutualFundList.push(fundObj.pageTitle);
                            schemeIdMapObj[fundObj.pageTitle] = fundObj.schemeId;
                        });
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        function showFiterBox() {
            $('.js-filtersBox').removeClass('d-none');
            $('.js-compareBox').addClass('d-none');
            $('.js-filterWrap').removeClass('show-compare');
            $('.js-compareActive').removeClass('has-compare');
            $('.js-compareActiveNone').removeClass('has-opacity');
        };

        function showPopupReturns() {
            $('.portfolioWatchListReturns').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info up-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span> ' + value + '% ');
                } else if (Number.isNaN(Number(value))) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass();
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info down-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span> ' + value + '% ');
                }
            });
        }

        function removeWishlistModal() {
            $('#wishlist-modal').removeClass('popover-show').hide();
            $('body').removeClass('overflow-hidden popover-modal-open');
            $('.modal-backdrop').remove();
        }

        function checkFundsAddedToCompare() {
            compareFundArray = Object.values(selectedFundObj);
            var selector = addFromType == 'watchList' ? 'fetchFromWatchlist' : 'fetchFromPortfolio';
            document.getElementById(selector).querySelectorAll('[data-schemeid]').forEach(function (fund) {
                if (compareFundArray.indexOf(Number(fund.dataset.schemeid)) != -1) {
                    $('[data-schemeid="' + fund.dataset.schemeid + '"] .js-addCompare').find('input').prop("checked", true);
                } else {
                    if ($(window).width() >= 767) {
                        if (compareFundArray.length >= 3) {
                            $('#'+selector+' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    } else {
                        if (compareFundArray.length >= 2) {
                            $('#'+selector+' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    }
                }
            });
        }

        function getFundsData() {
            var requestBody = {
                "body": {
                    "schemeIds": addFromType == 'watchList' ? watchListSchemeIds : portfolioFundIds,
                }
            };

            fundManagerFundListApiObj.fetchFundsData(requestBody).then(function (response) {
                var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                responseData = convertCagrArrayToObject(responseData.responseJson.watchlistDetails);
                fundManagersFundListRenderObj.renderWatchListPortfolioFunds(responseData, addFromType);
                checkFundsAddedToCompare();
                watchlistFunds = responseData;
                var mCustomScrollType = addFromType == 'watchList' ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
                $('.js-modalClose').click(function () {
                    var popupType = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
                    document.querySelector(popupType + ' [data-search="watchlist-portfolio"]').value = '';
                    $('body').removeClass('overflow-hidden');
                    $(mCustomScrollType).mCustomScrollbar("destroy");
                    $('#jsSearchTypehead .typeahead').typeahead('destroy');

                });

                if($(window).width() > 767) {
                    $(mCustomScrollType).mCustomScrollbar({
                        axis: "y",
                        mouseWheel: {
                          enable: true,
                        },
                        documentTouchScroll: false,
                        scrollButtons: {
                          enable: true,
                          scrollAmount: 320,
                          scrollType: "stepped",
                        },
                    });
                }

                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                searchWatchListFund();
                if (addFromType == 'watchList') {
                    fetchWatchListPopup();
                } else {
                    fetchPortfolioPopup();
                }
                popupCompareEvents();
            }).catch(function (error) {
                console.log(error);
            });
        }

        function getPortfolio(targetModel) {
            if (headerBizObj.getUserObj().isLoggedIn) {
                $('.js-loader').addClass('show');
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy"
                    }
                }
                fundManagerFundListApiObj.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        if (responseData.responseJson.body.mutualFundList != undefined) {
                            var portfolioFundsArray = responseData.responseJson.body.mutualFundList;
                            portfolioFundIds.length = 0;
                            portfolioFundsArray.forEach(function (fund) {
                                portfolioFundIds.push(fund.schemeId);
                            });
                        }
                    }
                    if (portfolioFundIds.length > 0) {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.add('d-none');
                        }
                        addFromType = 'portfolio';
                        getFundsData();
                    } else {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.add('d-none');
                        }
                        setTargetForSearch(targetModel);
                        fetchPortfolioPopup();
                    }
                    $('.js-loader').removeClass('show');
                }).catch(function (error) {
                    $('.js-loader').removeClass('show');
                    console.log(error);
                });
            } else {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain;
                });
            }
        }

        function setTargetForSearch(targetModel) {
            document.querySelector('[data-portfolio="watchlist-popover"]').dataset['targetPortfolio'] = targetModel;
            document.querySelector('[data-search="watchlist-popover"]').dataset['targetSearch'] = targetModel;
            document.querySelector('[data-watchlist="portfolio-popover"]').dataset['targetWatchlist'] = targetModel;
            document.querySelector('[data-search="portfolio-popover"]').dataset['targetSearch'] = targetModel;
        }

        function setSessionSchemeId(selectedFundObj) {
            console.log(selectedFundObj);
            var data = Object.keys(selectedFundObj).map(function (value) {
                return selectedFundObj[value].toString();
            });
            sessionStorage.setItem('moneyfyCompareFunds', JSON.stringify(data));
        }

        function compareFunds() {
            // remove from compare js
            $('.js-removeCompare').click(function () {
                var selectedFundName = $(this).parents('.js-selectedCompareBtn').find('.text-wrap').text();

                $('.similar-mutual-funds-items').each(function () {
                    if ($(this).find('.fund-name').text() === selectedFundName) {
                        $(this).find('.js-addCompare input').prop("checked", false);
                    }
                });
                delete selectedFundObj[selectedFundName];
                checkedFund = jQuery.grep(checkedFund, function (value) {
                    return value != selectedFundName;
                });
                compareWrap.each(function (i) {
                    $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);

                    if (checkedFund[i] != undefined) {
                        $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                        $(this).find('.js-chooseCompareBtn').addClass('d-none');
                    } else {
                        $(this).find('.js-selectedCompareBtn').addClass('d-none');
                        $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                    }
                });

                if (checkedFund.length < 1) {
                    showFiterBox();
                    removeWishlistModal();
                }

                var fundLength = 3;
                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                $('.js-fundCount').text(checkedFund.length);

                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });

            // cancel compare js
            $('.js-cancelCompare').click(function () {
                checkedFund = [];
                selectedFundObj = {};
                $('.js-addCompare input').prop("checked", false);

                $('.js-selectedCompareBtn').addClass('d-none');
                $('.js-chooseCompareBtn').removeClass('d-none');

                showFiterBox();
                $('#wishlist-modal').removeClass('popover-show').hide();
                $('body').removeClass('overflow-hidden popover-modal-open');
                $('.modal-backdrop').remove();
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });

            /* compare box js */
            $('[data-search="fund"]').on('input', function () {
                var searchVal = $(this).val();
                //console.log(searchVal);
                searchVal = searchVal.trim();
                if (searchVal.length >= 3) {
                    searchApi(searchVal);                    
                }
            });

            $('.js-chooseCompareBtn .jsOpenModal').click(function () {
                $('#jsMutualFundSearchTypehead input').val('');
            });

            // search select js
            $('.modal-search-wrap').on('click', '.tt-selectable', function () {
                var modalId = $(this).parents('.search-fund-modal').attr('id');
                var searchVal = $('#' + modalId + ' .tt-input').val();
                //console.log(schemeIdMapObj[searchVal]);
                //console.log(searchVal);
                selectedFundObj[searchVal] = schemeIdMapObj[searchVal];
                $('[data-schemeid="' + schemeIdMapObj[searchVal] + '"] .js-addCompare').find('input').prop("checked", true);
                //document.getElementById(modalId).setAttribute('data-selectedSchemeId',schemeIdMapObj[searchVal]);
                $('[data-select-modal="' + modalId + '"]').find('.js-chooseCompareBtn').addClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-selectedCompareBtn').removeClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-selectedCompareBtn .text-wrap').text(searchVal);

                // close modal
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();

                checkedFund.push(searchVal);
                $('.js-fundCount').text(checkedFund.length);
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });
        }

        function popupCompareEvents() {
            // add compare check js
            var selector = addFromType == "watchList" ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
            $(selector + ' .js-addCompare').change(function () {

                var fundName = $(this).parents('.similar-mutual-funds-items').find('.fund-name').data('schemename');
                var schemeID = $(this).parents('.fund-list-li').data('schemeid');
                var fundLength = 3;
                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                if (Object.values(selectedFundObj).length > fundLength) {
                    $(this).find('input').prop("checked", false);
                } else {
                    if ($(this).find('input').is(':checked')) {
                        checkedFund.push(fundName);
                        if (checkedFund.length > fundLength) {
                            $(this).find('input').prop("checked", false);
                            checkedFund.pop(fundName);
                        }
                        if (Object.values(selectedFundObj).length < fundLength) {
                            selectedFundObj[fundName] = schemeID;
                        }
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", true)
                    } else {
                        delete selectedFundObj[fundName]
                        checkedFund = jQuery.grep(checkedFund, function (value) {
                            return value != fundName;
                        });
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", false);
                    }
                    compareWrap.each(function (i) {
                        $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);
                        if (checkedFund[i] != undefined) {
                            $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                            $(this).find('.js-chooseCompareBtn').addClass('d-none');
                        } else {
                            $(this).find('.js-selectedCompareBtn').addClass('d-none');
                            $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                        }
                    });
                }


                if (checkedFund.length >= 1) {
                    $('.js-filtersBox').addClass('d-none');
                    $('.js-compareBox').removeClass('d-none');
                    $('.js-filterWrap').addClass('show-compare');
                    $('.js-compareActive').addClass('has-compare');
                    $('.js-compareActiveNone').addClass('has-opacity');
                } else {
                    showFiterBox();

                    // 07-03-2022 start
                    removeWishlistModal();
                    // 07-03-2022 end
                }

                // 07-03-2022 start
                $('.js-fundCount').text(checkedFund.length);

                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                // 07-03-2022 end

                setSessionSchemeId(selectedFundObj);

            });
        }

        function formSelectInitialize() {
            $('.form-select2').click(function () {
                if ($(this).parents('body').hasClass('popover-modal-open')) {
                    $('.select2-dropdown').attr('style', 'z-index: 1051 !important; width: 100px;');
                    console.log($('.select2-container--open').length);
                    if ($('.select2-container--open').length !== 0) {
                        $('.wishlist-fund-list').addClass('mob-wishlistmodal-scroll');
            
                    } else {
                        $('.wishlist-fund-list').removeClass('mob-wishlistmodal-scroll');
                    }
                }
            });
        }

        function convertCagrArrayToObject(array) {
            if (array.length > 0) {
                array.forEach(function (element, index) {
                    var cagrObj = {};
                    element.cagrValues.forEach(function (cagr, index) {
                        cagrObj[cagr.tenure] = cagr.value;
                    });
                    element.cagrValues = cagrObj;
                });
                return array;
            } else {
                return [];
            }
        }

        function searchWatchListFund() {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var searchFund = watchlistFunds.map(function (fund) {
                return fund.schemeDetails.name;
            });
            var selector = addFromType == 'watchList' ? 'addFromWishList' : 'addFromPortfolio';
            $('#' + selector + ' #jsSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },{
                name: 'searchFund',
                source: substringMatcher(searchFund)
            });

            $('#' + selector + " .tt-menu").on('click', function () {
                var searchVal = $('#' + selector + ' [data-search="watchlist-portfolio"]').val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    fundManagersFundListRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    fundManagersFundListRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });
        }

        function compareFundSearchTypeahead() {
            /* compare fund search typeahead js */
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    // an array that will be populated with substring matches
                    matches = [];
                    // regex used to determine if a string contains the substring `q`
                    substringRegex = new RegExp(q, 'i');
                    // iterate through the pool of strings and for any string that
                    // contains the substring `q`, add it to the `matches` array
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });
                    cb(matches);
                };
            };

            $('.js-chooseCompareBtn .jsOpenModal').click(function () {
                $('#jsMutualFundSearchTypehead input').val('');
            });

            /* mutual fund search modal */
            $('#jsMutualFundSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
                {
                    name: 'searchmutualFundList',
                    source: substringMatcher(searchmutualFundList)
                });
            /* compare fund search typeahead js */

            // watchList & portFolio popup search
            $("[data-search='watchlist-portfolio']").on('keyup', function () {
                try {
                    $('.wishlist-mCustomScrollbar').mCustomScrollbar("destroy");    
                } catch (error) {
                    console.log(error);
                }
                
                var searchVal = $(this).val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    fundManagersFundListRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    fundManagersFundListRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                    $('.wishlist-mCustomScrollbar').mCustomScrollbar({
                        axis: "y",
                        mouseWheel: {
                            enable: true,
                        },
                        documentTouchScroll: false,
                        scrollButtons: {
                            enable: true,
                            scrollAmount: 320,
                            scrollType: "stepped",
                        },
                    });
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });
        }

        function fundsShown(event, category) {
            var perPage = event.target.innerText.split(' ');
            perPage = Number(perPage[0]);
            numberOfFundsPerPage[category] = perPage;
            fundManagersFundListRenderObj.renderFunds(fundListObj[category], category);
            fundFilterBoxPagination(perPage, category);
            $('.single-select2').select2({
                minimumResultsForSearch: -1
            });
            reInitializeEvents(category);
            showReturns();
            addEllipseToFundName();
        }

        function initializeEvents() {
            // addFromWatchList & addFromPortfolio
            document.querySelectorAll('[data-fetchwatchlist="watchListBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-box-item').data('compareselect');
                    if (headerBizObj.getUserObj().isLoggedIn) {
                        if (watchListSchemeIds.length > 0) {
                            if (!document.querySelector('#addFromWishList [data-watchlist="false"]').classList.contains('d-none')) {
                                document.querySelector('#addFromWishList [data-watchlist="false"]').classList.add('d-none');
                            }
                            if (document.querySelectorAll('#fetchFromWatchlist .fund-list-li').length > 0) {
                                $('#fetchFromWatchlist .fund-list-li').empty();
                            }
                            addFromType = 'watchList';
                            getFundsData();
                        } else {
                            if (!document.querySelector('#addFromWishList [data-watchlist="true"]').classList.contains('d-none')) {
                                document.querySelector('#addFromWishList [data-watchlist="true"]').classList.add('d-none');
                            }
                            setTargetForSearch(targetModel);
                            fetchWatchListPopup();
                        }
                    } else {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = appConfig.jocataDomain;
                        });
                    }
                });
            });

            document.querySelectorAll('[data-fetchportfolio="portfolioBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-box-item').data('compareselect');
                    getPortfolio(targetModel);
                });
            });

            $('.jsModalOverflowHide').click(function () {
                $('body').addClass('overflow-hidden');
            });

            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });

            /*tab js*/
            $('.jsTabList [data-tab]').click(function () {
                var tabText = $(this).text().trim();
                tabInteraction(tabText,"fund-managers-fund-list",userIdObj.userId);
                $(this).parents('.jsTabList').find('li a').removeClass('active');
                $(this).addClass('active');
                var ele_id = $(this).attr('data-tab');
                $(this).parents('.jsTabContainer').find('.jsTabRow').addClass('d-none');
                $('#' + ele_id).fadeIn().removeClass('d-none');
            });
            /*tab js*/

            /*modal js*/
            $('[data-popovermodal="compare-popover"]').click(function () {
                var ele_target = $(this).attr('data-target');
                setTimeout(function () {
                    $(ele_target).addClass('popover-show');
                }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');
                $(this).hasClass('jsWhiteBackdrop') ? $('.modal-backdrop').addClass('white-backdrop') : $('.modal-backdrop').removeClass('white-backdrop');
            });

            $('[data-dismiss="popover-modal"]').on('click', function () {
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                if ($('body').hasClass('overflow-hidden')) {
                    $('body').removeClass('overflow-hidden');
                }
                $('.modal-backdrop').remove();
            });
            /*modal js*/

            // fund manager tab pagination js
            $('.fund-manager-tabs [data-tab]').click(function () {
                var items = $(".jsPagination:visible .fund-list-li");
                var category = $(this).data('cat');
                var numItems = items.length;
                var perPage = numberOfFundsPerPage[category];
                items.slice(perPage).hide();
                $('.pagination-list:visible').pagination({
                    items: numItems,
                    itemsOnPage: perPage,
                    prevText: "<i class='icon-arrow-left'></i><span>Previous</span>",
                    nextText: "<span>Next</span><i class='icon-arrow-right'></i>",
                    onPageClick: function (pageNumber) {

                        var showFrom = perPage * (pageNumber - 1);
                        var showTo = showFrom + perPage;
                        items.hide().slice(showFrom, showTo).show();

                        $('html, body').animate({
                            scrollTop: $(".jsPagination:visible").offset().top - 280
                        }, 500);
                    },
                    onInit: function () {
                        pageNumber = 1
                        var showFrom = perPage * (pageNumber - 1);
                        var showTo = showFrom + perPage;
                        items.hide().slice(showFrom, showTo).show();

                    }
                });
            });

            // dropdown js
            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

            $('.js-ToggleBtn').click(function () {
                $(this).toggleClass('active');
                var toggleDiv = $(this).attr('data-btn');
                $('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });

            // $('.js-viewMoreToggleBtn').click(function () {
            //     $(this).toggleClass('active');
            //     $(this).find('.text-wrap').text(function (i, text) {
            //         return text === "More" ? "Less" : "More";
            //     });
            //     var toggleDiv = $(this).attr('data-btn');
            //     $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            // });

            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }
            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

            $(window).on('resize', function () {
                // $('.slick-slider')[0].slick.refresh();
                $('.slick-slider').slick('refresh');
                header_fixed();
            });

            $(window).on('load', function () {
                header_fixed();
            });

            $(window).on('scroll', function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        }
                        else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }


                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    //console.log('container pos ' + conatiner_filter_position);
                    //console.log('win scroll top ' + scrollTop);

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }


                }
            });

            $('.jsMobSorting').click(function () {
                var category = $(this).parents('.fund-content-row').data('type');
                console.log(category);
                /* Sorting code for Sort-by */
                $("input[name='sort']").click(function () {
                    var sortBy = $("input[name='sort']:checked").val();
                    //console.log(sortBy);
                    filterFunds(sortBy.toLowerCase(), category);
                });
                $('.jsSelectSortList li input').prop('checked', false);
                $(this).parents('body').append('<div class="sorting-backdrop"></div>')
                $(this).parents('body').addClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').addClass('opened-sort')
            });

            $('.jsSelectSortList li input, .jsSortByClose').on('click', function () {
                $(this).parents('body').find('.sorting-backdrop').remove();
                $(this).parents('body').removeClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').removeClass('opened-sort')
            });

        }

        /* Mobile View show returns value based on dropdown */
        function showReturns() {
            $('.returnsVal').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info up-trend');
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span>' + value + '%');
                } else if (Number.isNaN(Number(value))) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass();
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info down-trend');
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span>' + value + '%');
                }   
            });
        }

        /* sort funds based on sortby value */
        function filterFunds(sortBy, category) {
            var type = '[data-type=' + category + ']';
            $(type).find('[data-list="fundList"]').html('');
            if (sortBy === 'morning star - high to low') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(b.morningStar) - Number(a.morningStar);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            } else if (sortBy === 'morning star - low to high') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(a.morningStar) - Number(b.morningStar);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            } else if (sortBy === 'value research - high to low') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(b.valueResearch) - Number(a.valueResearch);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            } else if (sortBy === 'value research - low to high') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(a.valueResearch) - Number(b.valueResearch);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            } else if (sortBy === 'returns - high to low') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(b.cagrValues['3y']) - Number(a.cagrValues['3y']);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            } else if (sortBy === 'returns - low to high') {
                var sortedArray = fundListObj[category].sort(function (a, b) {
                    return Number(a.cagrValues['3y']) - Number(b.cagrValues['3y']);
                });
                fundManagersFundListRenderObj.renderFunds(sortedArray, category);;
            }

            $('.single-select2').select2({
                minimumResultsForSearch: -1
            });

            fundFilterBoxPagination(numberOfFundsPerPage[category], category);
            showReturns();
            formSelectInitialize();
            reInitializeEvents(category);
            addEllipseToFundName();
        }

        function renderFunds() {
            for (category in fundListObj) {
                if (fundListObj[category].length > 0) {
                    fundListObj[category].forEach(function (fund) {
                        var fundCagr = JSON.parse(fund.cagrValues);
                        fund.cagrValues = {};
                        fundCagr.forEach(function (cagr) {
                            fund.cagrValues[cagr.tenure] = cagr.value;
                        });
                    });
                }
                // showFunds options render dynamically
                var showFunds = [8, 10, 15, 20];
                showFunds.forEach(function (showFundNumber) {
                    if (fundListObj[category].length > showFundNumber) {
                        document.querySelector('[data-type="' + category + '"]').querySelector('.fundSize-filter').innerHTML += '<li class="' + (showFundNumber == 8 ? "active" : "") + '">'
                            + '<a href="javascript:void(0)">' + showFundNumber + ' Funds</a>'
                            + '</li>';
                    }
                });
                showFundsAndSortbyEvent();
                filterFunds('returns - high to low', category);
            }
        }

        function fundFilterBoxPagination(perPage, category) {
            var items = $("[data-type='" + category + "']" + " .jsPagination .fund-list-li");
            var numItems = items.length;
            items.slice(perPage).hide();
            if ($("[data-type='" + category + "']").is(":visible")) {
                $("[data-type='" + category + "']" + " #pagination-container").pagination({
                    items: numItems,
                    itemsOnPage: perPage,
                    prevText: "<i class='icon-arrow-left'></i><span>Previous</span>",
                    nextText: "<span>Next</span><i class='icon-arrow-right'></i>",
                    onPageClick: function (pageNumber) {
                        var showFrom = perPage * (pageNumber - 1);
                        var showTo = showFrom + perPage;
                        items.hide().slice(showFrom, showTo).show();

                        $('html, body').animate({
                            scrollTop: $(".jsPagination").offset().top - 250
                        }, 500);
                    }
                });
            }
        }

        function addEllipseToFundName() {
            /*2 line Dot in mutual fund strip*/
            if ($(window).width() > 767) {
                var showChar = 45;
            }
            else if ($(window).width() > 374) {
                var showChar = 32;
            }
            else {
                var showChar = 20;
            }

            $('.similar-mutual-funds-items .name-rating-wrap h6').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {
                    var showLine = content.substr(0, showChar);
                    var remainContent = content.substr(showChar, content.length - showChar);
                    var allContent = showLine + '<span class="remaining-content d-none">' + remainContent + '</span> <span>...</span>';
                    $(this).html(allContent);
                }
            })
        }

        /*header scroll fixed animation*/
        var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
        var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();
        function header_fixed() {
            var windowScroll = $(window).scrollTop();
            var topbarHeight = $('.top-bar').outerHeight();

            if (windowScroll > topbarHeight) {
                $('header').addClass('affix');
                $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
            } else {
                $('header').removeClass('affix');
                $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
            }
        }

        function reduceDigit(str, val) {
            str = str.toString();
            str = str.slice(0, (str.indexOf(".")) + val + 1);
            return Number(str);
        }
        fundManagersFundListObj.reduceDigit = reduceDigit;
        fundManagersFundListObj.renderFunds = renderFunds;
        return jsHelper.freezeObj(fundManagersFundListObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundManagersFundListObj", fundManagersFundListBizFn)
})(this || window || {});
/*fund manager biz.js end*/