Fund Review
====
The `Fund Review` component is for displaying an overview of an specific fund. 



## Feature
* It is an dynamic component with data populated using JS.    



## Edit Dialog Properties
There are no properties stored.


## Client Libraries
The component provides a `moneyfy.fund-review` editor client library category that includes JavaScript and CSS.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5