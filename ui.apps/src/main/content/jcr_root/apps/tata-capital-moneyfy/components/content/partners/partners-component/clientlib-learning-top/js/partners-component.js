  /*product listing slider 12-12-2022*/
    $('#jsProductListingSlider').slick({
    dots: true,
    infinite: false,
    speed: 300,
    slidesToShow: 2,
    slidesToScroll: 1,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 360,
        settings: {
          dots: true,
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
    ]
  });
  
  /*product listing slider 12-12-2022*/
  if (screen.width >= 992) {
    var count = ($('#jsProductListingSlider').slick('getSlick').slideCount);
     if(count == 1 || count == 2){
      document.querySelector('#jsProductListingSlider').classList.remove('slider-dots')
     }   
  }

  if (screen.width <= 992) {
    var count = ($('#jsProductListingSlider').slick('getSlick').slideCount);
     if(count == 1){
      document.querySelector('#jsProductListingSlider').classList.remove('slider-dots')
     }   
  }