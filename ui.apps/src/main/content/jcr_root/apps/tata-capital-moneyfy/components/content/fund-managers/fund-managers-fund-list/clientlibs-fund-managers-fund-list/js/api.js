/* fundManagerFundList api js start */
(function (_global) {
    var fundManagerFundListApiFn = (function (jsHelper) {
        var fundManagerFundListApiObj = {};
        var compareSearch = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getSearchApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }

        /* Add To WatchList Api calling */
        var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
                
         /* Fetch from Portfolio Api calling */
         var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        fundManagerFundListApiObj.fetchFundsData = getFundsData;
        fundManagerFundListApiObj.fetchFromPortfolio = fetchFromPortfolio;
        fundManagerFundListApiObj.fetchwatchListApi = fetchwatchListApi;
        fundManagerFundListApiObj.addTowatchListApi = addTowatchListApi;
        fundManagerFundListApiObj.compareSearch = compareSearch;
        return jsHelper.freezeObj(fundManagerFundListApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundManagerFundListApiObj', fundManagerFundListApiFn);
})(this || window || {});
/* fundManagerFundList api js end */