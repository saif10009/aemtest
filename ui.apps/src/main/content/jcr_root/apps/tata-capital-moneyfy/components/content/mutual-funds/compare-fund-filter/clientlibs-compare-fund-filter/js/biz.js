/*compare fund biz .js*/
(function (_global) {
    var compareFundFilterFn = (function (jsHelper) {
        var compareFundFilterBizObj = {}
        var searchmutualFundList = [];
        var schemeIdMapObj = {};
        var schemeIDvalues = JSON.parse(sessionStorage.getItem('moneyfyCompareFunds'));
        var isFundAddedThroughSearch;
        var searchIndex = 0;
        var chartCompare;
        var watchListSchemeIds = [];
        var portfolioFundIds = [];
        var addFromType;
        var checkedFund = [];
        var watchlistFunds;
        var temp;
        //var selectedFundObj = {};

        document.addEventListener('DOMContentLoaded', function () {
            initializeEvents();
            if (schemeIDvalues.length !== 0) {
                rendercompareFundData(schemeIDvalues, isFundAddedThroughSearch);
                rendercompareGraph(schemeIDvalues);
            }
            showCompareFundLength();
            // initialize search typeAhead event JS
            compareSearchTypeahead();

            // initialize watchList event JS
            document.querySelector('[data-watchlist="portfolio-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromPortfolio').classList.remove('popover-show');
                document.getElementById('addFromPortfolio').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetWatchlist + '"] [data-fetchwatchlist="watchListBtn"]').click();
            });

            document.querySelector('[data-portfolio="watchlist-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromWishList').classList.remove('popover-show');
                document.getElementById('addFromWishList').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetPortfolio + '"] [data-fetchportfolio="portfolioBtn"]').click();
            });

            // addFromWatchList & addFromPortfolio
            document.querySelectorAll('[data-fetchwatchlist="watchListBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-item-wrap').data('compareselect');
                    getUserWatchList(targetModel);
                });
            });

            document.querySelectorAll('[data-fetchportfolio="portfolioBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-item-wrap').data('compareselect');
                    getPortfolio(targetModel);
                });
            });

            document.querySelectorAll('.js-compareFunds').forEach(function (element) {
                element.addEventListener('click', function () {
                    var schemes = schemeIDvalues.filter(function (data) {
                        return data != '-';
                    });
                    rendercompareFundData(schemes, false);
                    rendercompareGraph(schemes);
                    var popupType = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
                    document.querySelector(popupType + ' [data-search="watchlist-portfolio"]').value = '';
                })
            });
            
            document.querySelectorAll('.js-closeCompareModel').forEach(function (element) {
                element.addEventListener('click', function () {
                    schemeIDvalues = temp;
                    showCompareFundLength();
                    var popupType = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
                    document.querySelector(popupType + ' [data-search="watchlist-portfolio"]').value = '';
                    console.log(schemeIDvalues);
                })
            });


            investNow();
        });

        function showCompareFundLength() {
            var schemes = schemeIDvalues.filter(function (data) {
                return data != '-';
            });
            $('.js-fundCount').text(schemes.length);
        }

        function compareSearchTypeahead() {
            $('.modal-search-wrap').on('click', '.tt-selectable', function () {
                var modalId = $(this).parents('.search-fund-modal').attr('id');
                var searchVal = $('#' + modalId + ' .tt-input').val();
                //var index = $('[data-select-modal="' + modalId + '"]').data('fundindex');
                $('[data-select-modal="' + modalId + '"]').find('.js-compareDropdown').addClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-compareSelected').removeClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-compareSelected .text-wrap').text(searchVal);

                var fundItem = $('[data-select-modal="' + modalId + '"]').find('.js-compareSelected').attr('data-item');
                $('.compare-feature.' + fundItem + '').removeClass('hide-data');

                // close modal
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();
                var obj = {
                    fundId: schemeIdMapObj[searchVal],
                    fundName: searchVal
                }
                schemeIDvalues[searchIndex] = obj.fundId;
                var fundData = schemeIDvalues.filter(function (data) {
                    return data != '-';
                });
                temp = schemeIDvalues;
                console.log('SchemeDetails :',schemeIDvalues);
                showCompareFundLength();
                rendercompareFundData(fundData, true);
                rendercompareGraph(fundData);
            });
            //  $('.compare-graph-card').css('display', 'none');

            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            $('#jsMutualFundSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            }, {
                name: 'searchmutualFundList',
                source: substringMatcher(searchmutualFundList)
            });
        }

        function investNow() {
            /* Invest Now click button */
            $('[data-investnow="true"]').click(function () {
                var schemaId = $(this).data('schemeid');
                var source = window.location.href;
                var schemeName = $(this).data('schemename');
                var riskType = $(this).data('risktype');
                fundInvestNow('',schemeName,riskType,userIdObj.userId);
                console.log('Invest Now redirect link :', appConfig.jocataDomain + '?action=invest' + '&schemaId=' + schemaId + '&source=' + source)
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                    });
                } else {
                    location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                }
            });
        }

        function getUserWatchList(targetModel) {
            if (headerBizObj.getUserObj().isLoggedIn) {
                $('.js-loader').addClass('show');
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy"
                    }
                }
                console.log(requestObj);
                compareFundsApiObj.fetchwatchListApi(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        var watchListArray = responseData.responseJson.body.watchListDetails;
                        watchListSchemeIds.length = 0;
                        watchListArray.forEach(function (fund) {
                            watchListSchemeIds.push(fund.schemeId);
                        });
                    }
                    if (watchListSchemeIds.length > 0) {
                        if (!document.querySelector('#addFromWishList [data-watchlist="false"]').classList.contains('d-none')) {
                            document.querySelector('#addFromWishList [data-watchlist="false"]').classList.add('d-none');
                        }
                        if (document.querySelectorAll('#fetchFromWatchlist .fund-list-li').length > 0) {
                            $('#fetchFromWatchlist .fund-list-li').empty();
                        }
                        addFromType = 'watchList';
                        getFundsData();
                    } else {
                        if (!document.querySelector('#addFromWishList [data-watchlist="true"]').classList.contains('d-none')) {
                            document.querySelector('#addFromWishList [data-watchlist="true"]').classList.add('d-none');
                        }
                        setTargetForSearch(targetModel);
                        fetchWatchListPopup();
                    }
                    $('.js-loader').removeClass('show');
                }).catch(function (error) {
                    console.log(error);
                    $('.js-loader').removeClass('show');
                });
            } else {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain;
                });
            }
        }

        function getFundsData() {
            var requestBody = {
                "body": {
                    "schemeIds": addFromType == 'watchList' ? watchListSchemeIds : portfolioFundIds,
                }
            };

            compareFundsApiObj.fetchFundsData(requestBody).then(function (response) {
                var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                responseData = convertCagrArrayToObject(responseData.responseJson.watchlistDetails);
                comparefundRenderObj.renderWatchListPortfolioFunds(responseData, addFromType);
                checkFundsAddedToCompare();
                watchlistFunds = responseData;
                $('.jsModalOverflowHide').click(function () {
                    $('body').addClass('overflow-hidden');
                });

                $('.js-modalClose').click(function () {
                    $('body').removeClass('overflow-hidden');
                    $('.wishlist-mCustomScrollbar').mCustomScrollbar("destroy");
                });

                if ($(window).width() > 767) {
                    $('.wishlist-mCustomScrollbar').mCustomScrollbar({
                        axis: "y",
                        mouseWheel: {
                            enable: true,
                        },
                        documentTouchScroll: false,
                        scrollButtons: {
                            enable: true,
                            scrollAmount: 320,
                            scrollType: "stepped",
                        },
                    });
                } else {
                    $('.wishlist-mCustomScrollbar').mCustomScrollbar("destroy");
                }

                

                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                searchWatchListFund();
                formSelectInitialize();
                addEllipseToFundName();
                showPopupReturns();
                popupCompareEvents();
                if (addFromType == 'watchList') {
                    fetchWatchListPopup();
                } else {
                    fetchPortfolioPopup();
                }
            }).catch(function (error) {
                console.log(error);
            });
        }

        function getPortfolio(targetModel) {
            if (headerBizObj.getUserObj().isLoggedIn) {
                $('.js-loader').addClass('show');
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy"
                    }
                }
                compareFundsApiObj.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        if (responseData.responseJson.body.mutualFundList != undefined) {
                            var portfolioFundsArray = responseData.responseJson.body.mutualFundList;
                            portfolioFundIds.length = 0;
                            portfolioFundsArray.forEach(function (fund) {
                                portfolioFundIds.push(fund.schemeId);
                            });   
                        }
                    }
                    if (portfolioFundIds.length > 0) {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.add('d-none');
                        }
                        addFromType = 'portfolio';
                        getFundsData();
                    } else {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.add('d-none');
                        }
                        setTargetForSearch(targetModel);
                        fetchPortfolioPopup();
                    }
                    $('.js-loader').removeClass('show');
                }).catch(function (error) {
                    console.log(error);
                    $('.js-loader').removeClass('show');
                });
            } else {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain;
                });
            }
        }

        function convertCagrArrayToObject(array) {
            array.forEach(function (element, index) {
                var cagrObj = {};
                element.cagrValues.forEach(function (cagr, index) {
                    cagrObj[cagr.tenure] = cagr.value;
                });
                element.cagrValues = cagrObj;
            });
            return array;
        }

        function searchWatchListFund() {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var searchFund = watchlistFunds.map(function (fund) {
                return fund.schemeDetails.name;
            });
            var selector = addFromType == 'watchList' ? 'addFromWishList' : 'addFromPortfolio';
            $('#' + selector + ' #jsSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },{
                name: 'searchFund',
                source: substringMatcher(searchFund)
            });

            $('#' + selector + " .tt-menu").on('click', function () {
                var searchVal = $('#' + selector + ' [data-search="watchlist-portfolio"]').val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    comparefundRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    comparefundRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });

            // watchList & portFolio popup search
            $("[data-search='watchlist-portfolio']").on('keyup', function () {
                var mCustomScrollType = addFromType == 'watchList' ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
                $(mCustomScrollType).mCustomScrollbar("destroy");
                var searchVal = $(this).val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    comparefundRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    comparefundRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                    if($(window).width() > 767) {
                        $(mCustomScrollType).mCustomScrollbar({
                            axis: "y",
                            mouseWheel: {
                              enable: true,
                            },
                            documentTouchScroll: false,
                            scrollButtons: {
                              enable: true,
                              scrollAmount: 320,
                              scrollType: "stepped",
                            },
                        });
                    }else {
                        $(mCustomScrollType).mCustomScrollbar("destroy");
                    }
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });
        }

        function addEllipseToFundName() {
            /*2 line Dot in mutual fund strip*/
            if ($(window).width() > 767) {
                var showChar = 45;
            }
            else if ($(window).width() > 374) {
                var showChar = 32;
            }
            else {
                var showChar = 20;
            }
            $('.similar-mutual-funds-items .name-rating-wrap h6').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {
                    var showLine = content.substr(0, showChar);
                    var remainContent = content.substr(showChar, content.length - showChar);
                    var allContent = showLine + '<span class="remaining-content d-none">' + remainContent + '</span> <span>...</span>';
                    $(this).html(allContent);
                }
            })
        }

        function showPopupReturns() {
            $('.portfolioWatchListReturns').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info up-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span> ' + value + '% ');
                } else if (Number.isNaN(Number(value))) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass();
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info down-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span> ' + value + '% ');
                }
            });
        }

        function formSelectInitialize() {
            $('.form-select2').click(function () {
                if ($(this).parents('body').hasClass('popover-modal-open')) {
                    $('.select2-dropdown').attr('style', 'z-index: 1051 !important; width: 100px;');
                    console.log($('.select2-container--open').length);
                    if ($('.select2-container--open').length !== 0) {
                        $('.wishlist-fund-list').addClass('mob-wishlistmodal-scroll');
            
                    } else {
                        $('.wishlist-fund-list').removeClass('mob-wishlistmodal-scroll');
                    }
                }
            });
        }

        function setTargetForSearch(targetModel) {
            document.querySelector('[data-portfolio="watchlist-popover"]').dataset['targetPortfolio'] = targetModel;
            document.querySelector('[data-search="watchlist-popover"]').dataset['targetSearch'] = targetModel;
            document.querySelector('[data-watchlist="portfolio-popover"]').dataset['targetWatchlist'] = targetModel;
            document.querySelector('[data-search="portfolio-popover"]').dataset['targetSearch'] = targetModel;
        }

        function checkFundsAddedToCompare() {
            compareFundArray = schemeIDvalues.filter(function (fundId) {
                return fundId != '-';
            });
            var selector = addFromType == 'watchList' ? 'fetchFromWatchlist' : 'fetchFromPortfolio';
            document.getElementById(selector).querySelectorAll('[data-schemeid]').forEach(function (fund) {
                if (compareFundArray.indexOf(fund.dataset.schemeid) != -1) {
                    $('[data-schemeid="' + fund.dataset.schemeid + '"] .js-addCompare').find('input').prop("checked", true);
                } else {
                    if ($(window).width() >= 767) {
                        if (compareFundArray.length >= 3) {
                            $('#'+selector+' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    } else {
                        if (compareFundArray.length >= 2) {
                            $('#'+selector+' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    }
                }
            });
        }

        function popupCompareEvents() {
            // add compare check js
            var selector = addFromType == "watchList" ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
            $(selector + ' .js-addCompare').change(function () {
                //var fundName = $(this).parents('.similar-mutual-funds-items').find('.fund-name').data('schemename');
                var schemeID = $(this).parents('.fund-list-li').data('schemeid');
                var fundLength = 3;
                if ($(window).width() < 768) {
                    fundLength = 2;
                }
                
                checkedFund = schemeIDvalues
                var checkedFundLength = schemeIDvalues.filter(function (scheme) {
                    return scheme != '-';
                }).length;
                if (checkedFundLength > fundLength) {
                    $(this).find('input').prop("checked", false);
                } else {
                    if ($(this).find('input').is(':checked')) {
                        checkedFund[schemeIDvalues.indexOf('-')] = String(schemeID);
                        if (checkedFund.length > fundLength) {
                            $(this).find('input').prop("checked", false);
                            checkedFund.pop(schemeID);
                        }
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", true);
                    } else {
                        checkedFund[checkedFund.indexOf(String(schemeID))] = '-';
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", false);
                    }
                } 
                schemeIDvalues = checkedFund;
                checkedFundLength = schemeIDvalues.filter(function (scheme) {
                    return scheme != '-';
                }).length;
                console.log('SchemeDetails :',schemeIDvalues);
                $('.js-fundCount').text(checkedFundLength);
                if (checkedFundLength === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                console.log('Temp :', temp);
            });
        }

        //Search API
        function searchfundApi(searchRequest) {
            var searchRequestObj = {
                "body": {}
            };
            searchRequestObj.body.searchTerm = searchRequest;
            compareFundsApiObj.searchApi(searchRequestObj).then(function (response) {
                if (response.status.toLowerCase() == 'success') {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    responseData = responseData.responseJson.results;
                    if (responseData != undefined) {
                        searchmutualFundList.length = 0;
                        schemeIdMapObj = {};
                        responseData.forEach(function (fundObj) {
                            searchmutualFundList.push(fundObj.pageTitle);
                            schemeIdMapObj[fundObj.pageTitle] = fundObj.schemeId;
                        });
                    }

                }

            }).catch(function (error) {
                console.log(error)
            })
        }

        function initializeEvents() {
            /* Search API call */
            $('[data-search="search-input"]').keyup(function () {
                var searchInput = $(this).val();
                searchfundApi(searchInput);
            })
            /* Search API End */

            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });

            $('.js-removeCompareItem').click(function () {
                $(this).parents('.compare-item').find('.js-compareDropdown').removeClass('d-none');
                $(this).parents('.compare-item').find('.js-compareSelected').addClass('d-none');
                var fundItem = $(this).parents('.compare-item').find('.js-compareSelected').attr('data-item');
                $('.compare-feature.' + fundItem + '').addClass('hide-data');
                $('[data-accordion-card="sectoral-holdings-card"] .' + fundItem).empty();                
                $('[data-accordion-card="sectoral-holdings-card"] .' + fundItem).html('<div class="feature-item"></div>');
                var searchindex = $(this).attr('data-index');
                schemeIDvalues[searchindex] = "-";
                showCompareFundLength();
                var localTemp = [];
                schemeIDvalues.forEach(function (value) {
                    localTemp.push(value);
                });
                temp = localTemp;
                console.log('Temp :', temp);
                console.log('SchemeDetails :',schemeIDvalues);
            });

            $('.js-compareDropdown .jsOpenModal').click(function () {
                $('#jsMutualFundSearchTypehead input').val('');
            });

            $('[data-accordion]').click(function () {
                var fundSectionName = $(this).find('h5').text().trim();
                fundSectionExpend(fundSectionName, userIdObj.userId)
                $(this).parents('.custom-accordion').toggleClass('active');
                $(this).parents('.custom-accordion').find('[data-accordion-card]').slideToggle();
            });

            // dropdown js
            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

            /*modal js*/
            $('[data-popovermodal="compare-popover-modal"]').click(function () {
                var ele_target = $(this).attr('data-target');
                setTimeout(function () {
                    $(ele_target).addClass('popover-show');
                }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');
                $(this).hasClass('jsWhiteBackdrop') ? $('.modal-backdrop').addClass('white-backdrop') : $('.modal-backdrop').removeClass('white-backdrop');
                searchIndex = $(this).attr('data-index');
            });

            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }

            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

            $(window).on('scroll', function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        } else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();
                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }
                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }


                }
            });

            $('[data-attr="explore-more"]').click(function(event){
                event.preventDefault();
                var schemeName = $(this).data('schemename');
                var riskType = $(this).data('risktype');
                fundViewDetails('',schemeName,riskType,userIdObj.userId);
                window.open($(this).attr('href'));
            })

        }

        function compareCharts() {

            /* compare box js */

            $('.js-chooseCompareBtn .jsOpenModal').click(function () {
                $('#jsMutualFundSearchTypehead input').val('');
            });

        }

        function convertInDays(date1, date2) {
            var date = new Date(date1);
            var date1 = new Date(String(date.getMonth() + 1 + '/' + date.getDate() + '/' + Math.abs(date.getFullYear())));
            var diffTime = Math.abs(date2 - date1);
            var diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            return diffDays;
        }

        function parseData(data) {
            var convertedObj = {};
            data.forEach(value => {
                value.schemeMasterGraphResponseList.forEach(data => {
                    if (!convertedObj.hasOwnProperty(data.navDate)) {
                        convertedObj[data.navDate] = {};
                    }
                    convertedObj[data.navDate][value.schemeId] = data.navValue;
                });
            });
            return convertedObj;
        }

        function getGraphDate(days, data) {
            if (days !== "") {
                return Object.keys(data).filter(function (elementDate) {
                    return convertInDays(new Date(elementDate), new Date()) <= days;
                }).sort(function (a, b) {
                    return new Date(b.date) - new Date(a.date);
                });
            } else {
                return Object.keys(data).sort(function (a, b) {
                    return new Date(b.date) - new Date(a.date);
                });
            }
        }

        function compareGraph(responseArray) {

            var dataDynamic = {
                dataDefault: [],
                data_1M: [],
                data_3M: [],
                data_6M: [],
                data_1Y: [],
                data_3Y: [],
                data_5Y: [],
                data_Max: []
            };

            var daysObj = {
                dataDefault: 30,
                data_1M: 30,
                data_3M: 90,
                data_6M: 180,
                data_1Y: 365,
                data_3Y: (365 * 3),
                data_5Y: (365 * 5),
                data_Max: ""
            }
            var convertedData = parseData(responseArray);
            responseArray.forEach(function (element, index) {
                var fundDataObj = {};
                var colorObj = {
                    "0": "#2C6EB5",
                    "1": "#48A476",
                    "2": "#CC66CC"
                }
                var selector = '[data-select="filter-' + (index + 1) + '"]';
                var fundName = $(selector).find('[data-attr="schemeDetails.name"]').html();
                //var fundDataObj = sortGraphArray(responseArray[index].schemeMasterGraphResponseList, fund);

               // console.log("fundDataObj :", fundDataObj);
                for (var dataType in dataDynamic) {
                    var obj = {};
                    obj.name = fundName;
                    obj.data = getGraphDate(daysObj[dataType], convertedData).map(value => convertedData[value][element.schemeId] ? Number(convertedData[value][element.schemeId]) : "");
                    obj.color = colorObj[schemeIDvalues.indexOf(responseArray[index].schemeId.toString())];
                    obj.id = 'item-' + (index + 1);
                    dataDynamic[dataType].push(obj);
                }
                //console.log("dataDynamic :", dataDynamic);
            });

            renderGraph(dataDynamic.dataDefault, daysObj["dataDefault"], convertedData);
            // change data on click desktop
            $('[data-return]').click(function () {
                $('[data-return]').removeClass('active');
                $(this).addClass('active');
                var returnType = $(this).attr('data-return');
                renderGraph(dataDynamic[returnType], daysObj[returnType], convertedData);
            });

            // 11-11-2021 remove chart js
            $('.js-removeCompareItem').click(function () {
                var seriesLength = chartCompare.series.length;
                var modalId = $(this).parents('.compare-item-wrap').attr('data-select-modal');
                var removeFundName;
                if (modalId == "searchFund-modal-1") {
                    removeFundName = $('[data-select="filter-1"]').find('[data-attr="schemeDetails.name"]').html();
                } else if (modalId == "searchFund-modal-2") {
                    removeFundName = $('[data-select="filter-2"]').find('[data-attr="schemeDetails.name"]').html();
                } else if (modalId == "searchFund-modal-3") {
                    removeFundName = $('[data-select="filter-3"]').find('[data-attr="schemeDetails.name"]').html();
                }

                for (var i = seriesLength - 1; i > -1; i--) {
                    if (chartCompare.series[i].name == removeFundName) {
                        for (var key in dataDynamic) {
                            dataDynamic[key].splice(i, 1);
                        }
                        chartCompare.series[i].hide();
                    }
                }
            });

        }

        function rendercompareFundData(results, isFundAddedThroughSearch) {
            var responseArray;
            var requestObj = {
                "body": {
                    "schemeId": results
                }
            }
            compareFundsApiObj.compareFundApi(requestObj).then(function (response) {
                // var responseData = jsHelper.isDef(response) && !jsHelper.isObj(response) ? JSON.parse(response) : response;
                if (response.status == "SUCCESS") {
                    responseArray = response.response.compareDetails;
                console.log('SchemeDetails :',schemeIDvalues);
                    comparefundRenderObj.renderFund(responseArray,schemeIDvalues);
                    compareCharts();
                    //investNow();
                } else {
                    console.log('api call failed')
                }

                if (isFundAddedThroughSearch != true) {
                    if (results.length === 2) {
                        setTimeout(function () {
                            $('.remove-compare').eq(2).click();
                        });
                    } else if (results.length === 1) {
                        setTimeout(function () {
                            $('.remove-compare').eq(2).click();
                            $('.remove-compare').eq(1).click();
                        });
                    } else if (results.length === 0) {
                        setTimeout(function () {
                            $('.remove-compare').click();
                        });
                    }
                } else {
                    isFundAddedThroughSearch = false;
                }

            }).catch(function (error) {
                console.log(error);
            });

        }

        function rendercompareGraph(results) {
            $('.js-loader').addClass('show');
            var responseArray;
            var requestObj = {
                "body": {
                    "schemeIds": results
                }
            }
            compareFundsApiObj.getGraph(requestObj).then(function (response) {
                if (response.status.toLowerCase() == "success") {
                    var responseData = jsHelper.isDef(response) && jsHelper.isObj(response) ? JSON.parse(response.response) : response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        console.log(responseData);
                        responseArray = responseData.responseJson.body.graphDetails;
                        compareGraph(responseArray);
                        $('[data-tab="graph"] li .active').removeClass('active')
                        $('[data-tab="graph"] li:first a').addClass('active')
                        $('.js-loader').removeClass('show');
                    }
                } else {
                    console.log('There was some error in getting response.')
                }
            }).catch(function (error) {
                console.log(error);
            });

        }

        function renderGraph(graphData, days, convertedData) {
            if (jsHelper.isDef(chartCompare)) {
                chartCompare.destroy();
            }

            chartCompare = Highcharts.chart('compare-chart', {

                chart: {
                    type: 'spline',
                    animation: {
                        enabled: false
                    }
                },
                credits: {
                    enabled: false
                },
                title: {
                    text: "",
                },
                subtitle: {
                    text: "",
                },

                xAxis: {
                    categories: getGraphDate(days, convertedData),
                    style: {
                        color: "#606060",
                        fontSize: "12px",
                        fontFamily: "Poppins",
                        fontWeight: "400",
                    },

                    crosshair: {
                        width: 1,
                        color: '#2c6eb566',
                        dashStyle: 'shortdot'
                    }
                },
                yAxis: {
                    title: {
                        text: "",
                        style: {
                            color: "#606060",
                            fontSize: "12px",
                            fontFamily: "Poppins",
                            fontWeight: "400",
                        },
                    },
                    tickInterval: 10,
                    visible: false,
                },

                tooltip: {
                    shared: true,
                    headerFormat: '<div class="custom-tooltip compare-tooltip">',
                    pointFormat: '<div class="tooltip-item"><span class="tooltip-label"><span class="dot" style="background-color: {series.color}"></span> {series.name}</span>' +
                        '<span class="tooltip-result"><span class="icon-caret-top"></span> ₹{point.y:.1f}</span></div>',
                    footerFormat: '</div>',
                    shared: true,
                    useHTML: true,
                    backgroundColor: '#333',
                    shadow: false,
                    borderWidth: 0,
                    borderRadius: 6,
                    style: {
                        fontFamily: "Poppins",
                    },
                },

                plotOptions: {
                    series: {
                        color: '#48A476',
                        showInLegend: false,

                        marker: {
                            enabled: false,
                            symbol: 'cirle'
                        }
                    }

                },

                series: graphData,
            });

        }

        function reduceDigit(str, val) {
            str = str.toString();
            str = str.slice(0, (str.indexOf(".")) + val + 1);
            return Number(str);
        }

        /*header scroll fixed animation*/
        var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
        var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();

        function header_fixed() {
            var windowScroll = $(window).scrollTop();
            var topbarHeight = $('.top-bar').outerHeight();
            if (windowScroll > topbarHeight) {
                $('header').addClass('affix');
                $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
            } else {
                $('header').removeClass('affix');
                $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
            }
        }
        compareFundFilterBizObj.reduceDigit = reduceDigit;
        //compareFundFilterObj.renderDataPosition = renderDataPosition;
        compareFundFilterBizObj.schemeIDvalues = schemeIDvalues;
        return jsHelper.freezeObj(compareFundFilterBizObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "compareFundFilterBizObj", compareFundFilterFn)
})(this || window || {});
/*compare fund biz.js end*/