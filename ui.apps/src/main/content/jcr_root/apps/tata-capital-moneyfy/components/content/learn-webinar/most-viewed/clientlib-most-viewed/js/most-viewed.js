document.addEventListener('DOMContentLoaded', function() {
  $('.blog-slider-col').click(function () {
    var video = $(this).attr('data-video')
    var embedVideo = video.substring(video.lastIndexOf('/') + 1);
    var videos = 'https://youtube.com/embed/' + embedVideo;
    $('#video-modal .play-video-boxs').find('iframe').attr('src', videos);
    var head = $(this).data('heading');
    $('.play-video-right').find("h6").text(head)
    var subhead = $(this).data('subheading');
    $('.play-video-right').find("h5").text(subhead)
    var desc = $(this).data('desc');
    $('.play-video-right').find("p").text(desc)
    var nameDate = $(this).data('namedate');
    $('.play-video-right').find("span").text(nameDate)
    $('.play-video-right').find('a').attr('data-subheading', subhead)
    $('.play-video-right').find('a').attr('data-video', video)
  });


 var videoDataObj = {} 
  $('[data-target="#share-modal"]').click(function (e) {
    e.stopImmediatePropagation()
    videoDataObj.videoText = $(this).attr('data-subheading');
    videoDataObj.videoLinks = $(this).attr('data-video');
    var video = $(this).attr('data-video')
    var embedVideo = video.substring(video.lastIndexOf('/') + 1);
    var updatedLink = 'https://www.youtube.com/watch?v=' + embedVideo;
    originalLink = videoDataObj.videoLinks;
    $('.copy-links input').val(updatedLink);
  })

$('[data-copybtn]').click(function (event) {
  event.preventDefault()
  var ctaText = $(this).text().trim();
  $(this).parents('.copy-links').find('input').select();
  document.execCommand("copy");
})

$('[data-shareicon]').click(function (event) {
  event.preventDefault()
  if ($(this).data('shareicon') == 'facebook') {
    var facebookWindow = window.open(
      "https://www.facebook.com/sharer/sharer.php?hashtag=" +
      videoDataObj.videoText +
      "&" +
      "u=" +
      originalLink,
      "facebook-popup",
      "height=350,width=600"
    );
    if (facebookWindow.focus) {
      facebookWindow.focus();
    }
  } else if ($(this).data('shareicon') == 'twitter') {
    var twitterWindow = window.open(
      "https://twitter.com/share?text=" + (videoDataObj.videoText) + "&" + "url=" + originalLink
    );
    if (twitterWindow.focus) {
      twitterWindow.focus();
    }
  } else if ($(this).data('shareicon') == 'whatsapp') {
    var whatsappWindow = window.open(
        "https://api.whatsapp.com/send?text=" + (videoDataObj.videoText + " " +originalLink)
      );
      if (whatsappWindow.focus) {
        whatsappWindow.focus();
      }
  } else if ($(this).data('shareicon') == 'linkedin') {
    var linkedinWindow = window.open(
        "https://www.linkedin.com/shareArticle?mini\x3dtrue\x26url\x3d" + (videoDataObj.videoText) + "&" + "url=" + originalLink, "width\x3d300,height\x3d200,personalbar\x3d0,toolbar\x3d0,scrollbars\x3d0,resizable\x3d0", 'linkedin-popup'
      );
      if (linkedinWindow.focus) {
        linkedinWindow.focus();
      }
  } else {
    location.href = $(this).attr('href')
  }
})
})
