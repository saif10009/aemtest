    /*testimonial js*/
    (function (_global) {
        var testimonialBizObj = (function (jsHelper) {
            var testimonialObj = {};
            document.addEventListener('DOMContentLoaded', function () {
                var requestObj = { "body": {} };
                testimonalApiObj.testimonalFn(requestObj).then(function (res) { 
                    console.log(res);
                    var testimonalMdmRes = JSON.parse(res.response); 
                    testimonalRenderObj.renderTestimonal(testimonalMdmRes.Master);
                    initializeTestimonialSlick();
                    if (window.outerWidth > 768) {
                        if ($('.customer-slider-col').length <= 3) {
                            $('.customer-slider').removeClass('slider-dots');
                            
                        }
                    } else {
                        if ($('.customer-slider-col').length == 0) {
                           $('.customer-slider').removeClass('slider-dots');
                        }
                    }
                });
                
            });
            function initializeTestimonialSlick() {
                $('.jsCustomerSlider').slick({
                    dots: true,
                    infinite: true,
                    speed: 300,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    responsive: [
                        {
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 3,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 992,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 768,
                            settings: {
                                slidesToShow: 2,
                                slidesToScroll: 1,
                                variableWidth: true,
                            }
                        }
                    ]
                });
            }
            return jsHelper.freezeObj(testimonialObj);
        })(jsHelper);
        _global.jsHelper.defineReadOnlyObjProp(_global, 'testimonialBizObj', testimonialBizObj);
    })(this || window || {});
    /*testimonial js*/