
(function (_global) {

    var compareFundsApi = (function (jsHelper) {
        var compareFundsApiObj = {};
        // get fundDetails api calling
        var compareFundApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.comparePagePostApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response.responseJson
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        // get graph data calling
        var getGraph = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getGraph(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        // search api calling
        var searchApi = function (searchRequestObj) {
            return new Promise(function (resolve) {
                apiUtility.getSearchApi(searchRequestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        // Fetch from WatchList Api calling
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        
        /* Fetch from Portfolio Api calling */
        var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        compareFundsApiObj.fetchFundsData = getFundsData;
        compareFundsApiObj.fetchFromPortfolio = fetchFromPortfolio;
        compareFundsApiObj.fetchwatchListApi = fetchwatchListApi;
        compareFundsApiObj.compareFundApi = compareFundApi;
        compareFundsApiObj.searchApi = searchApi;
        compareFundsApiObj.getGraph = getGraph;


        return jsHelper.freezeObj(compareFundsApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'compareFundsApiObj', compareFundsApi);
})(this || window || {});
