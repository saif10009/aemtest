/*fund manager list  biz.js start*/
(function (_global) {
    var fundManagerListBizFnObj = (function (jsHelper) {
        var fundManagerListObj = {};
        var filteredArray = [];
        var deskSortBy = ['returns - high to low'];
        var mobSortBy = ['returns - high to low'];
        document.addEventListener('DOMContentLoaded', function () {
            sortFilterFn('returns - high to low');
            registerFilterEvents();
            registerManagerListEvents();
            customScrollBar();
            searchFundManager();
            //createArray();
            loader();
            
            //clear all checkBox onLoad
            setTimeout(function () {
                document.querySelectorAll('input[name="sort"]:checked').forEach(function (checkbox) {
                    checkbox.checked = false;
                });
            }, 200)

            $("input[name='sort']").click(function (event) {
                $('[data-filter-card]').slideUp('fast')
                if (window.outerWidth <= 768) {
                    sortFilterFn(mobSortBy[0].toLowerCase());
                }
                else { sortFilterFn(deskSortBy[0].toLowerCase()) } 
            });

            /* Sorting code for Sort By Highest Returns */
            if (window.outerWidth <= 768) {
                $("input[name='sortBy']").click(function () {
                    mobSortBy = [];
                    var sortBy = $("input[name='sortBy']:checked").val();
                    mobSortBy.push(sortBy);
                    loader();
                    sortFilterFn(sortBy.toLowerCase());
                });
            } else {
                document.querySelector('.getSortBy').addEventListener('click', function (event) {
                    deskSortBy = [];
                    loader();
                    var sortBy = event.target.innerText.toLowerCase();
                    deskSortBy.push(sortBy);
                    sortFilterFn(sortBy);
                });
            }
        });

        function filterManagerList(selectedValues) {
            var amcFilterArr = selectedValues.filter(function (filterObj) {
                return filterObj.filterType == "amc";
            });

            var fundSizeFilterArr = selectedValues.filter(function (filterObj) {
                return filterObj.filterType == "fundSize";
            });

            var amcFundFilter = [];
            var fundSizeFilter = [];
            var resultArr = [];
            if (amcFilterArr.length > 0) {
                amcFundFilter = amcFilter(amcFilterArr.map(function (element) {
                    return element.filterVal;
                }), fundManagerArray);
                if (fundSizeFilterArr.length > 0) {
                    fundSizeFilter = fundSizeList(fundSizeFilterArr.map(function (element) {
                        return (
                            {
                                start: element.filterVal.split('-')[0],
                                end: element.filterVal.split('-')[1]
                            }
                        )
                    }), amcFundFilter);
                    return resultArr = fundSizeFilter;
                } else {
                    return amcFundFilter;
                }
            } else if (fundSizeFilterArr.length > 0) {
                fundSizeFilter = fundSizeList(fundSizeFilterArr.map(function (element) {
                    return (
                        {
                            start: element.filterVal.split('-')[0],
                            end: element.filterVal.split('-')[1]
                        }
                    )
                }), fundManagerArray);
                return resultArr = fundSizeFilter;
            } else {
                return resultArr;
            }
        }

        function amcFilter(amcArray, fundArray) {
            var filterData = fundArray.filter(function (fundValue) {
                return jsHelper.arrIncludes(amcArray, fundValue.amcId);
            });
            return filterData;
        }

        function fundSizeList(fundsizeArray, fundArray) {
            var filterData = fundArray.filter(function (element) {
                var filterArray = fundsizeArray.filter(function (value) {
                    return (Number(element.totalFundSize) > Number(value.start)) && (Number(element.totalFundSize) <= Number(value.end));
                })
                return filterArray.length > 0;
            })
            return filterData;
        }

        var substringMatcher = function (strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;
                // an array that will be populated with substring matches
                matches = [];
                // regex used to determine if a string contains the substring `q`
                substringRegex = new RegExp(q, 'i');
                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function (i, str) {
                    if (substringRegex.test(str)) {
                        matches.push(str);
                    }
                });
                cb(matches);
            };
        };


        function sortFilterFn(sortBy) {
            console.log("Highest Returns Sorted")
            document.querySelector('#fundManagerList').innerHTML = '';
            var checkValues = [];
            document.querySelectorAll('input[name="sort"]:checked').forEach((checkbox) => {
                let obj = {};
                obj['filterVal'] = checkbox.dataset.val;
                obj['filterType'] = checkbox.dataset.type;
                checkValues.push(obj);
            });
            filteredArray = filterManagerList(checkValues);
            var filterArrayThroughSortBy = checkValues.length > 0 ? filteredArray : fundManagerArray;
            if (sortBy === 'returns - high to low') {
                sortedArray = filterArrayThroughSortBy.sort(function (a, b) {
                    return Number(b.highestReturns) - Number(a.highestReturns);
                }); 
                fundManagerListRenderObj.renderManagerList(sortedArray);
            } else if (sortBy === 'returns - low to high') {
                sortedArray = filterArrayThroughSortBy.sort(function (a, b) {
                    return Number(a.highestReturns) - Number(b.highestReturns);
                });
                fundManagerListRenderObj.renderManagerList(sortedArray);
            } else if (sortBy === 'fund size - high to low') {
                sortedArray = filterArrayThroughSortBy.sort(function (a, b) {
                    return Number(b.totalFundSize) - Number(a.totalFundSize);
                });

                fundManagerListRenderObj.renderManagerList(sortedArray);
            }
            else if (sortBy === 'fund size - low to high') {
                sortedArray = filterArrayThroughSortBy.sort(function (a, b) {
                    return Number(a.totalFundSize) - Number(b.totalFundSize);
                });

                fundManagerListRenderObj.renderManagerList(sortedArray);
            }
            adobeAnaltics();

        }

        function registerManagerListEvents() {
            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });

            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

        }

        function registerFilterEvents() {

            $('.jsDropDownMenuGet li a').click(function () {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');
                $('[data-filter-card]').slideUp('fast');

                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });

            // filters js
            // category filter js
            $('[data-category]').click(function () {
                $('[data-category]').removeClass('active');
                $('[data-category-item]').removeClass('active');
                $(this).addClass('active');
                var categoryName = $(this).attr('data-category');
                $('[data-category-item="' + categoryName + '"]').addClass('active');
            });
            // hide selected tag - clear all section if no tag is there
            function hideFilterTagSection() {
                if ($('.filter-tags .slick-track').children().length == 0) {
                    $('.filter-tags-wrap').slideUp();
                }

            }
            // filter tag check js
            $('.js-filterCheck').change(function () {
                var filterVal = $(this).find('input').val();
                var filterTagHtml = '<li class="filter-tag" data-tag="' + filterVal + '">' + filterVal + '<a href="javascript:void(0);" class="remove-tag js-removeTag"><span class="icon-close"></span></a></li>'
                if ($(this).find('input').is(':checked')) {
                    setTimeout(function () {
                        $('.filter-tags').append(filterTagHtml);
                    }, 1500);
                } else {
                    // $('[data-tag="'+filterVal+'"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                }

                setTimeout(function () {
                    $('.filter-tags-wrap').slideDown();
                    $('.js-FilterSlide').slick('refresh');

                    var filterSlideCount = $('.js-FilterSlide li').length;
                    if (filterSlideCount <= 5) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                    if ($(".js-AmcFilterSlideInit").is(":visible")) {
                        var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                        if (filterSlideCount <= 3) {
                            $('.js-FilterSlide').addClass('overflow-visible');
                        } else {
                            $('.js-FilterSlide').removeClass('overflow-visible');
                        }
                    }
                    hideFilterTagSection();
                }, 1500);

                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");

                // mobile add yellow dot to filter title if checkbox checked
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    var checkboxLength = $('[data-filter-item="' + parentCard + '"]').find('input:checked').length;
                    if (checkboxLength > 0) {
                        $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    } else {
                        $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');
                    }

                    if ($(".js-fundCategoryBox").is(":visible")) {
                        var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                        if (fundCategoryCheckboxLength > 0) {
                            $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                        } else {
                            $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                        }
                    }

                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }

            });
            // clear all js
            $('.js-clearAll').click(function () {
                filteredArray = [];
                fundManagerListRenderObj.renderManagerList(fundManagerArray);
                // $('.filter-tag').remove();
                $('.js-FilterSlide').slick('slickRemove', null, null, true);
                $('.js-filterCheck input').prop("checked", false);
                $('.filter-card .js-searchInput').val('');

                $('.js-FilterSlide').slick('refresh');
                hideFilterTagSection();

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    $('[data-filter-link]').removeClass('hasFilter');
                    $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                }

                // initialize loader
                loader();

                // remove filter count
                $('.js-filterCount').removeClass('show').text('');
                adobeAnaltics();
            });
            // filter tag remove js
            $('.filter-tags-wrap').on('click', '.filter-tag .js-removeTag', function () {
                //var filterList = document.querySelectorAll('input[name="sort"]:checked')
                var tagName = $(this).parent('.filter-tag').attr('data-tag');
                $(this).parent('.filter-tag[data-slick-index]').remove();
                $('.js-filterCheck input[value="' + tagName + '"]').prop("checked", false);
                if (window.outerWidth <= 768) {
                    sortFilterFn(mobSortBy[0].toLowerCase());
                }
                else { sortFilterFn(deskSortBy[0].toLowerCase()) }
                $('.js-FilterSlide').slick('slickRemove', tagName);
                $('.js-FilterSlide').slick('refresh');
                var filterSlideCount = $('.js-FilterSliderInit li').length;
                if (filterSlideCount <= 5) {
                    $('.js-FilterSliderInit').addClass('overflow-visible');
                } else {
                    $('.js-FilterSliderInit').removeClass('overflow-visible');
                }

                if ($(".js-AmcFilterSlideInit").is(":visible")) {
                    var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                    if (filterSlideCount <= 3) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                }

                hideFilterTagSection();
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");


                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }
            });
            // clear filter card js
            $('.js-clearBtn').click(function () {
                $(this).parents('.filter-card').find('input').prop("checked", false);
                if (window.outerWidth <= 768) {
                    sortFilterFn(mobSortBy[0].toLowerCase());
                }
                else { sortFilterFn(deskSortBy[0].toLowerCase()) }

                // amc filter search clear and show list js
                $(this).parents('.filter-card').find('.js-searchInput').val('');
                $(this).parents('.filter-card').find('.amc-filter-list li').show();
                $(this).parents('.filter-card').find('.js-noSearchResult').addClass('d-none');

                $(this).parents('.filter-card').find('input').each(function () {
                    var filterVal = $(this).val();
                    $('[data-tag="' + filterVal + '"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                });
                hideFilterTagSection();
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");
                $('.js-FilterSlide').slick('refresh');
                var filterSlideCount = $('.js-FilterSliderInit li').length;
                if (filterSlideCount <= 5) {
                    $('.js-FilterSliderInit').addClass('overflow-visible');
                } else {
                    $('.js-FilterSliderInit').removeClass('overflow-visible');
                }

                if ($(".js-AmcFilterSlideInit").is(":visible")) {
                    var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                    if (filterSlideCount <= 3) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                }

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');

                    // initialize loader
                    loader();

                    // remove filter count
                    $('.js-filterCount').removeClass('show').text('');
                }

                if ($(".js-fundCategoryBox").is(":visible")) {
                    var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                    if (fundCategoryCheckboxLength > 0) {
                        $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                    } else {
                        $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                    }
                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }
            });
            // select all filter card js
            $('.js-selectAllBtn').click(function () {
                $(this).parents('.filter-card').find('input').prop("checked", true);

                $(this).parents('.filter-card').find('input').each(function () {
                    var filterVal = $(this).val();
                    $('[data-tag="' + filterVal + '"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));

                    var filterTagHtml = '<li class="filter-tag" data-tag="' + filterVal + '">' + filterVal + '<a href="javascript:void(0);" class="remove-tag js-removeTag"><span class="icon-close"></span></a></li>'

                    setTimeout(function () {
                        $('.filter-tags').append(filterTagHtml);
                    }, 1500);
                });

                setTimeout(function () {
                    $('.filter-tags-wrap').slideDown();
                    $('.js-FilterSlide').slick('refresh');
                    var filterSlideCount = $('.js-FilterSliderInit li').length;
                    if (filterSlideCount <= 5) {
                        $('.js-FilterSliderInit').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSliderInit').removeClass('overflow-visible');
                    }

                    if ($(".js-AmcFilterSlideInit").is(":visible")) {
                        var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                        if (filterSlideCount <= 3) {
                            $('.js-FilterSlide').addClass('overflow-visible');
                        } else {
                            $('.js-FilterSlide').removeClass('overflow-visible');
                        }
                    }
                }, 1500);
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");


                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }

                // mobile add yellow dot from filter title
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }

                if ($(".js-fundCategoryBox").is(":visible")) {
                    var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                    if (fundCategoryCheckboxLength > 0) {
                        $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                    } else {
                        $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                    }
                }

            });
            // filter search js
            $(".js-searchInput").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".amc-filter-list li").filter(function () {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
                if ($(".amc-filter-list li").is(':visible')) {
                    $('.js-noSearchResult').addClass('d-none');
                } else {
                    $('.js-noSearchResult').removeClass('d-none');
                }
            });

            function checkboxCount() {
                if ($('.js-inlineFilterClearAll input:checked').length >= 1) {
                    $('.js-clearAll').show();
                } else {
                    $('.js-clearAll').hide();
                }
            }

            if ($(".js-inlineFilterClearAll").is(":visible")) {

                $('.js-inlineFilterClearAll').change(function () {
                    // initialize loader
                    if ($(window).width() > 991) {
                        loader();
                    }
                    checkboxCount();
                });

                $('.filter-tags-wrap').on('click', '.filter-tag .js-removeTag', function () {
                    checkboxCount();
                });
                $('.js-clearBtn').click(function () {
                    checkboxCount();
                });
                $('.js-clearAll').click(function () {
                    $('.js-inlineFilterClearAll input').prop("checked", false);
                    checkboxCount();
                });
            }
            /* -------- mobile filter js start -------- */

            // show filter
            $('.js-showFilter').click(function () {
                $('.js-mobFilterBox').addClass('open');
                $('body').addClass('stopScroll');
            });
            // hide filter
            $('.js-closeFilter').click(function () {
                $('.js-mobFilterBox').removeClass('open');
                $('body').removeClass('stopScroll');
            });
            // show filter respective card
            $('[data-filter-link]').click(function () {
                $('[data-filter-link]').removeClass('active');
                $(this).addClass('active');
                var filterCardItem = $(this).attr('data-filter-link');
                $('[data-filter-item]').removeClass('active');
                $('[data-filter-item="' + filterCardItem + '"]').addClass('active');
            });
            // show fund category filter
            $('.js-fundCategoryBoxOpen').click(function () {
                $('.js-fundCategoryBox').addClass('active');
                $('[data-filter-item="Popular Categories"]').addClass('active');

                $('[data-filter-link="Popular Categories"]').addClass('active');
            });
            // hide fund category filter
            $('.js-closefundCategory').click(function () {
                $('.js-fundCategoryBox').removeClass('active');
                $('[data-filter-item]').removeClass('active');
                $('[data-filter-item="Rating"]').addClass('active');
                $('[data-filter-link]').removeClass('active');
                $('[data-filter-link="Rating"]').addClass('active');
            });
            // apply filer
            $('.js-applyFilter').click(function () {
                // initialize loader
                loader();

                // close filter
                setTimeout(function () {
                    $('body').removeClass('stopScroll');
                    $('.js-mobFilterBox').removeClass('open');
                }, 1500);

                // filter count
                var filterCount = $('.js-filterListWrap li .hasFilter').length;
                if (filterCount > 0) {
                    $('.js-filterCount').addClass('show').text(filterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }

                // sebi filter count
                var sebiFilterCount = $('.js-filterListWrap li .hasFilter').length;
                if (sebiFilterCount > 0) {
                    $('.js-filterCount').addClass('show').text(sebiFilterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }
            });

            /* -------- mobile filter js end -------- */
            $('.js-FilterSliderInit').slick({
                autoplay: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });
            $('.js-AmcFilterSlideInit').slick({
                autoplay: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });
            /*jsMobSorting*/
            $('.jsMobSorting').click(function () {
                $('.jsSelectSortList li input').prop('checked', false);
                $(this).parents('body').append('<div class="sorting-backdrop"></div>')
                $(this).parents('body').addClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').addClass('opened-sort')
            })
            $('.jsSelectSortList li input, .jsSortByClose').on('click', function () {
                $(this).parents('body').find('.sorting-backdrop').remove();
                $(this).parents('body').removeClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').removeClass('opened-sort')
            })

            /*start amc listing search*/

            var searchamc = ['Aditya Birla Sun Life Mutual Fund', 'Axis Mutual Fund', 'Baroda Mutual Fund', 'BNP Paribas Mutual Fund', 'BOI AXA Mutual Fund', 'Canara Robeco Mutual Fund', 'DSP Mutual Fund', 'Edelweiss Mutual Fund', 'Franklin Templeton  Mutual Fund', 'HDFC Mutual Fund', 'HSBC Mutual Fund', 'ICICI Prudential Mutual Fund', 'IDBI Mutual Fund', 'IIFL Mutual Fund', 'Indianbulls Mutrual Funds ', 'JM Financial Mutual Fund', 'Kotak Mahindra Mutual Fund', 'LIC Mutual Fund'];
            $('#jsSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
                {
                    name: 'searchamc',
                    source: substringMatcher(searchamc)
                });


            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }

            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

            $(window).on('scroll', function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;
                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        }
                        else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }

                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();
                    // console.log('header height '+headerHeight);
                    // console.log('ele pos '+ele_position);
                    // console.log('container pos '+conatiner_position);
                    // console.log('win scroll top '+scrollTop);
                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }
                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();
                    //console.log('container pos ' + conatiner_filter_position);
                    //console.log('win scroll top ' + scrollTop);
                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }
                }

            });
            /*header scroll fixed animation*/
            var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
            var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();
            function header_fixed() {
                var windowScroll = $(window).scrollTop();
                var topbarHeight = $('.top-bar').outerHeight();

                if (windowScroll > topbarHeight) {
                    $('header').addClass('affix');
                    $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
                } else {
                    $('header').removeClass('affix');
                    $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
                }
            }
        }
        function searchFundManager() {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var searchFundManager = fundManagerArray.map(function (amc) {
                return amc.fundManagerName
            });
            console.log(searchFundManager)

            $('#jsSearchTypeheadMob .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            },
                {
                    name: 'searchFundManager',
                    source: substringMatcher(searchFundManager)
                });

            $("[data-search='searchFundManager']").on('keyup', function () {
                var searchVal = $(this).val();
                console.log(searchVal)
                if (searchVal.length > 0) {
                    var searchArray = [];
                    fundManagerArray.forEach(function (amc) {
                        if (amc.fundManagerName.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(amc);
                        }
                    });
                    fundManagerListRenderObj.renderManagerList(searchArray);
                } else {
                    fundManagerListRenderObj.renderManagerList(fundManagerArray);
                    adobeAnaltics();
                }
            });

            $(".tt-menu").on('click', function () {
                console.log('tt-input', $('.tt-input').val())
                var searchVal = $('.tt-input').val();
                console.log(searchVal)
                if (searchVal.length > 0) {
                    var searchArray = [];
                    fundManagerArray.forEach(function (amc) {
                        if (amc.fundManagerName.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(amc);
                        }
                    });
                    fundManagerListRenderObj.renderManagerList(searchArray);
                } else {
                    fundManagerListRenderObj.renderManagerList(fundManagerArray);
                }
                adobeAnaltics();
            });
        }

        function customScrollBar() {
            $('.mCustomScroll').mCustomScrollbar({});

            $('.filterTags-mCustomScrollbar').mCustomScrollbar({
                axis: "x",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });

            $('.filterList-mCustomScrollbar').mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });
        }

        // loader js
        function loader() {
            $('.js-loader').addClass('show');
            setTimeout(function () {
                $('.js-loader').removeClass('show');
            }, 1500);
        }

        function adobeAnaltics(){
            $('[data-fundManagerCard="fundmangercard"]').click(function(event){
                event.preventDefault();
                var noofFunds = $(this).data('nooffunds');
                var fundSize = $(this).data('fundsize');
                var highestReturn = $(this).data('highreturns');
                fundmanagerClick('', noofFunds, fundSize,highestReturn, userIdObj.userId);
                location.href = $(this).attr('href');
            })
        }

        return jsHelper.freezeObj(fundManagerListObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundManagerListBizObj", fundManagerListBizFnObj)
})(this || window || {});
/*fund manager biz.js end*/