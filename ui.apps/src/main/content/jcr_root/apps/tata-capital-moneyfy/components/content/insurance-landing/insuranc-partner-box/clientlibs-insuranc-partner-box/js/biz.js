/*insurence partner box js start*/
(function (_global) {
    var insurancePartnerBizObj = (function (jsHelper) {
        var insurancePartnerObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            insurancePartnerSlick();
        })
        function insurancePartnerSlick() {
            $('#jsInsurancePartnerSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                  {
                    breakpoint: 992,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    }
                  },
                  {
                    breakpoint: 768,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    }
                  },
                  {
                    breakpoint: 500,
                    settings: {
                      slidesToShow: 1,
                      slidesToScroll: 1,
                    }
                  }
                ]
              });
        }
        return jsHelper.freezeObj(insurancePartnerObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "insurancePartnerBizObj", insurancePartnerBizObj)
})(this || window || {});
  /*insurence partner box js end*/
