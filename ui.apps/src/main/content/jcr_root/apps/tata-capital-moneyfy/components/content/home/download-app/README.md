Download Apps
====
This component witten in HTL, allowing to render moneyfy document.

## Feature
* Used to send sms on phone as per phone number.


### Edit Dialog Properties
This is statick component son no editable dialog is required for this.

## Client Libraries
The component provides a `moneyfy.download-app` editor client library category that includes JavaScript and CSS
handling for dialog interaction. The `render.js` consist all the rendering logic of component on page. It is embeded inside `moneyfy.homePage` clientlib, which call inside template level.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5