Testimonial
====
The `Testimonial` component is for rendering the reviews or responses of existing users that have been using moneyfy. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as title, name of the user, ratings and response text is made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering titles of the component.
2. `./names` Used for rendering the names of the reviewers in the component.
3. `./ratings` Used to select the different ratings given by the particular user in the component.
4. `./response` Used to render response of the reviewers.



## Client Libraries
The component provides a `tata.testimonial` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The clientlib of this component is embedded in the `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib` folder.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5