/*faq-detail page js start*/
(function (_global) {

    var faqcardFn = (function (jsHelper) {
        var faqcardObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            var urlSearchParams = new URLSearchParams(window.location.search);
            var activeTab = (window.location.search).split('=').pop();

            var params = Object.fromEntries(urlSearchParams.entries());
            if (params.hasOwnProperty("redirect")) {
                $("#" + params.redirect).addClass('animation-class');
                $('.js-filterBtn a').text(document.querySelector('.'+params.redirect).text);
                window.scrollTo({ left: 0, top: $("#" + params.redirect).position().top, behaviour: 'smooth' });
                $(".js-faqSectionList li").removeClass("active");
                $(".js-faqSectionList li").find('a.' + activeTab).parents('li').addClass('active');
                $(".js-activeTabs li").removeClass("active");
                $(".js-activeTabs li").find('a.' + activeTab).parents('li').addClass('active');
                var activetext = $(".js-activeTabs").find('li.active').text();
                $('.js-filterBtn a').text(activetext);

            }
            if ($('.single-select2').is(":visible")) {
                $('.single-select2').select2({
                    minimumResultsForSearch: -1
                });

                $('.nps-select').on('change', function () {
                    var data = $(this).find("option:selected").text();
                    console.log(data);
                    if (data != 'Select') {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').removeClass('hide-data');
                    } else {
                        $(this).parents('.feature-wrap').find('.feature-item-wrap').addClass('hide-data');
                    }
                });
            }
            clickFn();
        });

        function clickFn() {
            /*------blue tab js start------>*/
            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp('fast');
                    $('.js-filterBtn').removeClass('active');
                }
            });
            // dropdown js
            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });

            $(window).on('scroll', function () {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        } else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }

                }
            });

            var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
            var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();

            function header_fixed() {
                var windowScroll = $(window).scrollTop();
                var topbarHeight = $('.top-bar').outerHeight();

                if (windowScroll > topbarHeight) {
                    $('header').addClass('affix');
                    $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
                } else {
                    $('header').removeClass('affix');
                    $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
                }
            }

            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }
            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

            // faq detail - on click go to section
            $('.js-faqSectionList a').click(function (event) {
                event.preventDefault();

                $('.js-faqSectionList li').removeClass('active');
                $(this).parents('li').addClass('active');

                if ($('.faq-detail-blue-tab').hasClass('affix')) {
                    $('html, body').animate({
                        scrollTop: $($.attr(this, 'href')).offset().top - 90
                    }, 500);
                } else {
                    if ($(window).width() > 991) {
                        $('html, body').animate({
                            scrollTop: $($.attr(this, 'href')).offset().top - 148
                        }, 500);
                    }
                }

                if ($(window).width() < 991) {
                    var sectionText = $(this).text();
                    $('.js-filterBtn a').text(sectionText);
                    $('.js-filterBtn').removeClass('active');
                    $('[data-filter-card]').slideUp();

                    $('html, body').animate({
                        scrollTop: $($.attr(this, 'href')).offset().top - 90
                    }, 500);
                }
            });
            /************ accordian Js *************/
            $('[data-toggles="collapses"]').click(function (ele) {
                var ele_parents = $(ele.target).parents('.accordian').attr('id');

                $('#' + ele_parents).find('.collapse').slideUp();
                $('#' + ele_parents).find('.accordian-card').removeClass('active');

                if ($(ele.target).parent().siblings().css('display') == "block") {
                    $(ele.target).parent().siblings().slideUp();
                    $(ele.target).parents('.accordian-card').removeClass('active');
                } else {
                    $(ele.target).parent().siblings().slideDown();
                    $(ele.target).parents('.accordian-card').addClass('active');
                }
            });

            $('.mCustomScroll').mCustomScrollbar({});
            $('.mCustomScroll').mCustomScrollbar({
                axix: "x"
            });

            $('.faqtable-mCustomScrollbar').mCustomScrollbar({
                axis: "x",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    // enable: true,
                    scrollAmount: 320,
                    // scrollType: "stepped",
                },
            });
            $('.filterTags-mCustomScrollbar').mCustomScrollbar({
                axis: "x",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });

            $('.filterList-mCustomScrollbar').mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });
            /*faq details tab slider*/
            $('#jsFaqDetailTab1').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                variableWidth: true,
                responsive: [
                  {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 5,          
                    }
                  },
                  {
                    breakpoint: 769,
                    settings: {
                      slidesToShow: 2,
                    }
                  }
                ]
              });
            /*faq details tab slider*/
            /*------faq card js end------>*/
        }
        return jsHelper.freezeObj(faqcardObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'faqcardObj', faqcardFn);
})(this || window || {});

/*faq-detail page js end*/