Fund List Filters
====
The `Fund List Filters` component is for adding mutual fund list filter on moneyfy funds list page. 



## Feature
* It is an static component.   



## Edit Dialog Properties
There are no proerties stored in the edit dialog of the component.

## Client Libraries
The component provides a `moneyfy.fund-list-filters` editor client library category that includes CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5