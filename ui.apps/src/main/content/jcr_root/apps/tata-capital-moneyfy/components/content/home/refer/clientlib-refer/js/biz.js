/*refer js*/
(function (_global) {

    var referBizObj = (function (jsHelper) {
        var referObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            adobeAnalytics();
        });
        function adobeAnalytics() {
            $('[data-refer] a').click(function (event) {
                event.preventDefault();
                var clickButtonTitle = $(this).text().trim();
                var clickTextTitle = $(this).parents('.refer-left').find('h6').text().trim();
                bannerInteraction(clickTextTitle, clickButtonTitle, 'refer', userIdObj.userId)
                //location.href = $(this).attr('href')
            });
        }

        return jsHelper.freezeObj(referObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'referBizObj', referBizObj);
})(this || window || {});
/*refer js*/