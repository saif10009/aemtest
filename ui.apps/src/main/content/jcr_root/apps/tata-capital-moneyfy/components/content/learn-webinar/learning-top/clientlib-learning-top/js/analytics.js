(function (_global) {
    var analyticsObjCall = (function (jsHelper) {
        var analyticsCall = {}
        document.querySelectorAll('.learn-tabs li').forEach(function (tab) {
            tab.addEventListener('click', function () {
                console.log(tab.innerText);
                var tabTitle = tab.innerText.toLowerCase();
                if(!getUserObj().isLoggedIn){
                    var userId = 'anonymous user';
                } else if (getUserObj().isLoggedIn){
                    var userId = getUserObj().userData.appCode;
                }
                var componentName = 'learn webinar';
                tabInteraction(tabTitle, componentName, userId);
            });
        })

        return jsHelper.freezeObj(analyticsCall);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "analyticsObjCall", analyticsObjCall)
})(this || window || {});