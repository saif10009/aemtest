(function (_global) {
    var calcGetEmailRenderFn = (function (jsHelper) {
        var calcGetEmailRenderObj = {};
        var generateOTPSuccessPopup = ('<button data-dismiss="popover-modal" class="popover-modal-close jsGetResultClose"><i class="icon-close"></i></button>' +
            '              <div class="get-result-forms" id="jsVeryfyOTP">' +
            '                <h4>Verify OTP</h4>' +
            '                <p class="medium">An OTP is sent to your registered mobile number +91 ********<span class="jsShowWhatsappNumber">85</span></p>' +
            '' +
            '                <div class="whatsapp-otp-verify">' +
            '                  <div class="enter-otp-box">                  ' +
            '                    <div class="otp-col js-OtpBox">' +
            '                      <input type="text" class="input-textbox only-numeric-input" maxlength="1">' +
            '                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1"> ' +
            '                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1">' +
            '                      <input type="text" class="input-textbox only-numeric-input pointer-none" maxlength="1">' +
            '                    </div> ' +
            '                    <div class="resend-otp">' +
            '                      <!-- <span><i class="icon-watch"></i> 00:23</span> -->' +
            '                      <a href="javascript:;" class="resend js-resendOTP">Resend</a>' +
            '                    </div>            ' +
            '                  </div>     ' +
            '                </div>               ' +
            '                ' +
            '                <div class="btn-proceed-whatsapp">' +
            '                  <a href="javascript:void(0);" class="btn-blue radius100 jsVeryfyBtn btn-disabled">Proceed</a>' +
            '                </div>' +
            '              </div>');

        calcGetEmailRenderObj.generateOTPSuccessPopup = generateOTPSuccessPopup;

        return jsHelper.freezeObj(calcGetEmailRenderObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, "calcGetEmailRenderObj", calcGetEmailRenderFn);
})(this || window || {});
