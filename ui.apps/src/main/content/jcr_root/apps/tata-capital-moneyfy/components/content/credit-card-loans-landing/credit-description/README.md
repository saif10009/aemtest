Credit card description
====
This is a `credit card description` component used in credit card loan page.

## Feature
* This is a multifield component.
* This component provides wide range of life goals.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./creditHeading` Used for rendering title of the component.
2. `./creditDescription` Used for rendering description of the component.
3. `./creditImg` Used for rendering image on page.
4. `./creditBtnOnelink` Used to render redirection link of a card.
5. `./creditBtnOneTarget` Enable open redirection link in a new tab.
6. `./creditBtnOne` Used to rendering the name of the button.
7. `./creditBtnTwolink` Used to render redirection link of a card.
8. `./creditBtnTowTarget` Enable open redirection link in a new tab.
9. `./creditBtnTwo` Used to rendering the name of the button.
10. `./listDataMulti` Used to create multifield in component.
11. `./listData` Used for rendering list items.


## Client Libraries
The component provides a `moneyfy.credit-description` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5