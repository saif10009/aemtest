Trusts
====
This is a `Trusts` component which gives three key features why we should trust Moneyfy. 

## Feature
* This is a multifield component.
* This component promotes features of Moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./colDropdown` Used to select columns.
4. `./threeCol` Used for select option.
4. `./fourCol` Used for select option.
2. `./trustList` Used to create multifield in component.
3. `./image` Used for rendering image on page.
4. `./heading` Used for rendering heading of image on page.


## Client Libraries
The component provides a `moneyfy.trusts` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5