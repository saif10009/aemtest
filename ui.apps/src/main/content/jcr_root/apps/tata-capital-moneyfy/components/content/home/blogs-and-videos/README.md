Blogs & Videos
====
The `Blogs & Videos` component is for rendering the blogs and videos content on the moneyfy. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as title, subtitles, descriptions, & video information of blogs & videos are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./blog & ./videos` Used for rendering titles of the component.
2. `./blogsList & ./videoList` Used for rendering the blogs & videos respectively in the component.
3. `./blogImg ./videoImg ./playBtn` Used to select the images or svg's for blogs, videos & video platbutton in the component.
4. `./blogTitle ./videoTitle` Used to give headings to blog & video cards.
5. `./blogDiscription & ./videoDiscription` are used for the short description that can be given to the respective cards.
6. `./videoDuration & ./videoInfo` are for adding the information about video such as date, editors name and duration of the videos.



## Client Libraries
The component provides a `tata.blogs-and-videos` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The clientlib of this component is embedded in the `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib` folder.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5