/*3-10-22*/
if ($('.js-our-partners-list').find('.js-partners-list').length < 7) {
    $('.js-our-partners-list').siblings('.view-more-btn').addClass('d-none');
  }
  $('.js-our-partners-list .js-partners-list').slice(0, 6).show();
  $("#jsOurPartnersLoadMore").on('click', function (e) {
    e.preventDefault();
    $(".js-our-partners-list .js-partners-list:hidden").slice(0, 3).fadeIn();
    if ($(".js-our-partners-list .js-partners-list:hidden").length == 0) {
      $("#jsOurPartnersLoadLess").removeClass('d-none').fadeIn('slow');
      $("#jsOurPartnersLoadMore").hide();
    }
  });
  $("#jsOurPartnersLoadLess").on('click', function (e) {
    e.preventDefault();
    $('.js-our-partners-list .js-partners-list:not(:lt(6))').fadeOut();
    $("#jsOurPartnersLoadMore").fadeIn('slow');
    $("#jsOurPartnersLoadLess").hide();
  });  
  /*3-10-2022*/