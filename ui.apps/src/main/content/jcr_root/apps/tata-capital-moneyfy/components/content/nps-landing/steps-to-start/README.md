Steps to Start
====
Steps to Start component written in HTL, used to redirect the proper path to get start with NPS.

## Feature
* This is a multifield component.
* Used to provide sequence of how to start NPS.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering title of the component.
2. `./contributeHead` Used for rendering main heading of the component.
3. `./multi, ./multifield` Used to create multifield in component.
4. `./contributeNowLink, ./npsButtonLink,` Used for rendering links on page.
5. `./buttonTitleProceed, ./newTabProceed` Used for open link in new tab.
6. `./contributeNowTarget, ./npsButtonTarget` Used for render links target on page.
7.  `./npsButtonText, ./contributeNowButton` used for render button titles .

## Client Libraries
The component provides a `moneyfy.steps-to-start` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.nps-landing-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-nps-landing`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5