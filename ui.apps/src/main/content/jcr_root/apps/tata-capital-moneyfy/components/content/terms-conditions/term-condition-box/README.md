Term condition box
====
This is a `Term condition box` component used in terms condition page.

## Feature
* This is a multifield component.
* All the various elements of the component such as descriptions, heading are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading` Used for rendering headings of the component.
2. `./description` Used for rendering description of the  component.
3. `./termConditionMultifield` Used for repeating html in page


## Client Libraries
The component provides a `moneyfy.term-condition-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5