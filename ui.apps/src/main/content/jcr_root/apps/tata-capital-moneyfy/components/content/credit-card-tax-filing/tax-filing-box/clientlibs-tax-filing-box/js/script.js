
if ($(window).width() > 991) {
    if ($('#jsTaxFillingSlider').hasClass('slick-initialized')) {
      $('#jsTaxFillingSlider').slick('unslick');
    }
  } else {
    $('#jsTaxFillingSlider').not('.slick-initialized').slick({
      dots: true,
      infinite: false,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      arrows: false,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 360,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        }
      ]
    });
  }
  /*tax-filing slider*/