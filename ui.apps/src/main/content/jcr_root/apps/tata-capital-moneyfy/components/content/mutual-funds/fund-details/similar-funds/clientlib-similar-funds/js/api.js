/* similarFund api js start */
(function (_global) {
    var similarFundApiFn = (function (jsHelper) {
        var similarFundApiObj = {};
         /* Add To WatchList Api calling */
         var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        similarFundApiObj.fetchwatchListApi = fetchwatchListApi;
        similarFundApiObj.addTowatchListApi = addTowatchListApi;
        return jsHelper.freezeObj(similarFundApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'similarFundApiObj', similarFundApiFn);
})(this || window || {});
/* similarFund api js end */
