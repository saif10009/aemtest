(function (_global) {
    var podcastAnalyticsFn = (function (jsHelper) {
        var podcastAnalyticsObj = {};
        window.addEventListener("load", function(event){
            if (!getUserObj().isLoggedIn){
                var userId = 'anonymous user';
              } else if(getUserObj().isLoggedIn) {
                var userId = getUserObj().userData.appCode;
            }
            document.querySelectorAll('.podcasts .media-video-col .play-pause-btn').forEach(function(btn){
                btn.addEventListener('click', function(e){
                    try{
                        var ctaTitle = getParentElement(e.currentTarget, 4).querySelector('[data-podcastheading="heading"]').innerText.trim();
                        var componentName = getParentElement(e.currentTarget, 9).classList[0].split('-').join(' ');
                        var sectionName = getParentElement(e.currentTarget, 10).classList[0];
                        playaudioPodcast(sectionName, componentName, ctaTitle, userId);
                    } catch (error) {
                        console.log('element not found', error);
                    }
                });
            });
        });
    podcastAnalyticsObj.podcastAnalyticsFn = podcastAnalyticsFn; 
    return jsHelper.freezeObj(podcastAnalyticsObj);
    })(jsHelper);
_global.jsHelper.defineReadOnlyObjProp(_global, "calcGetEmailApiObj", podcastAnalyticsFn);
})(this || window || {});