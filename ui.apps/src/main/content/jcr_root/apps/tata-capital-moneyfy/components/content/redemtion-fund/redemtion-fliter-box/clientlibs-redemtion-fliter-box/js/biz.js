/*fund-filter-box js start*/
(function(_global) {
    var fundFilterBoxBizObj = (function(jsHelper) {
        var fundFilterBoxObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            fundFilterBoxMouseup();
            fundFilterBoxClick();
            $('.mCustomScroll').mCustomScrollbar({});
            setTimeout(function() {
                $('.mCustomScroll').mCustomScrollbar('destroy');
                $('.mCustomScroll').mCustomScrollbar({});

            }, 200);
             
            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
              }
            
            fundFilterBoxPagination();
            fundFilterBoxCustomScroll();
           
            /*header scroll fixed animation*/
            var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
            var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();

            function header_fixed() {
                var windowScroll = $(window).scrollTop();
                var topbarHeight = $('.top-bar').outerHeight();

                if (windowScroll > topbarHeight) {
                    $('header').addClass('affix');
                    $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
                } else {
                    $('header').removeClass('affix');
                    $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
                }
            }

        })
        function fundFilterBoxMouseup() {
            $(document).mouseup(function(e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp();
                    $('.js-filterBtn').removeClass('active');
                }
            });

        }
        function fundFilterBoxClick() {

            $('.jsMobSorting').click(function() {
                $('.jsSelectSortList li input').prop('checked', false);
                $(this).parents('body').append('<div class="sorting-backdrop"></div>')
                $(this).parents('body').addClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').addClass('opened-sort')
            })
            $('.jsSelectSortList li input, .jsSortByClose').on('click', function() {
                $(this).parents('body').find('.sorting-backdrop').remove();
                $(this).parents('body').removeClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').removeClass('opened-sort')
            })

            /*jsMobSorting*/

            $('.js-ToggleBtn').click(function() {
                $(this).toggleClass('active');
                var toggleDiv = $(this).attr('data-btn');
                $('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });

            $('.js-viewMoreToggleBtn').click(function() {
                $(this).toggleClass('active');
                $(this).find('.text-wrap').text(function(i, text) {
                    return text === "More" ? "Less" : "More";
                });
                var toggleDiv = $(this).attr('data-btn');
                $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });


            $('.jsDropDownMenuGet li a').click(function() {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');

                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });
            $('.single-select2').select2({
                minimumResultsForSearch: -1
            });
        }
        function fundFilterBoxCustomScroll() {

            $(window).on('scroll', function() {
                // header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        } else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }

                    // instant redemtion sort fix
                    // var conatiner_position = $('.js-sortfixContainer').position().top - headerHeight - 59;
                    // console.log('conatiner_position' + conatiner_position);
                    // if (scrollTop > conatiner_position) {
                    //   $('.js-sortfixContainer .mob-filter-container').addClass('affix');
                    //   console.log('true')
                    // }else{
                    //   $('.js-sortfixContainer .mob-filter-container').removeClass('affix');
                    //   console.log('false')
                    // }

                }
            });

        }

        function fundFilterBoxPagination() {
            var items = $(".jsPagination .fund-list-li");
            var numItems = items.length;
            var perPage = 8;
            items.slice(perPage).hide();

            $('#pagination-container').pagination({
                items: numItems,
                itemsOnPage: perPage,
                prevText: "<i class='icon-arrow-left'></i><span>Previous</span>",
                nextText: "<span>Next</span><i class='icon-arrow-right'></i>",
                onPageClick: function(pageNumber) {
                    var showFrom = perPage * (pageNumber - 1);
                    var showTo = showFrom + perPage;
                    items.hide().slice(showFrom, showTo).show();
                }
            });

            // dropdown js
            $('.js-filterBtn').click(function() {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp();

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });
            // remove from compare js
            $('.js-removeCompare').click(function() {
                var selectedFundName = $(this).parents('.js-selectedCompareBtn').find('.text-wrap').text();

                $('.similar-mutual-funds-items').each(function() {
                    if ($(this).find('.fund-name').text() === selectedFundName) {
                        $(this).find('.js-addCompare input').prop("checked", false);
                    }
                });
                checkedFund = jQuery.grep(checkedFund, function(value) {
                    return value != selectedFundName;
                });
                compareWrap.each(function(i) {
                    $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);

                    if (checkedFund[i] != undefined) {
                        $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                        $(this).find('.js-chooseCompareBtn').addClass('d-none');
                    } else {
                        $(this).find('.js-selectedCompareBtn').addClass('d-none');
                        $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                    }
                });

                if ($('.js-addCompare input:checked').length < 1) {
                    showFiterBox();
                }
            });


            /*filter js statrt*/
            // loader js
            function loader() {
                $('.js-loader').addClass('show');
                setTimeout(function() {
                    $('.js-loader').removeClass('show');
                }, 1500);
            }

            // filters js

            // category filter js
            $('[data-category]').click(function() {
                $('[data-category]').removeClass('active');
                $('[data-category-item]').removeClass('active');
                $(this).addClass('active');
                var categoryName = $(this).attr('data-category');
                $('[data-category-item="' + categoryName + '"]').addClass('active');
            });

            // hide selected tag - clear all section if no tag is there
            function hideFilterTagSection() {
                // if ($('.filter-tags').children().length == 0) {
                //   $('.filter-tags-wrap').slideUp();
                // }
                if ($('.filter-tags .slick-track').children().length == 0) {
                    $('.filter-tags-wrap').slideUp();
                }

            }

            // filter tag check js
            $('.js-filterCheck').change(function() {
                var filterVal = $(this).find('input').val();

                var filterTagHtml = '<li class="filter-tag" data-tag="' + filterVal + '">' + filterVal + '<a href="javascript:void(0);" class="remove-tag js-removeTag"><span class="icon-close"></span></a></li>'


                if ($(this).find('input').is(':checked')) {
                    setTimeout(function() {
                        $('.filter-tags').append(filterTagHtml);
                    }, 1500);
                } else {
                    // $('[data-tag="'+filterVal+'"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                }

                setTimeout(function() {
                    $('.filter-tags-wrap').slideDown();
                    $('.js-FilterSlide').slick('refresh');

                    var filterSlideCount = $('.js-FilterSlide li').length;
                    if (filterSlideCount <= 5) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                    if ($(".js-AmcFilterSlideInit").is(":visible")) {
                        var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                        if (filterSlideCount <= 3) {
                            $('.js-FilterSlide').addClass('overflow-visible');
                        } else {
                            $('.js-FilterSlide').removeClass('overflow-visible');
                        }
                    }
                    hideFilterTagSection();
                }, 1500);

                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");

                // mobile add yellow dot to filter title if checkbox checked
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    var checkboxLength = $('[data-filter-item="' + parentCard + '"]').find('input:checked').length;
                    if (checkboxLength > 0) {
                        $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                    } else {
                        $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');
                    }

                    if ($(".js-fundCategoryBox").is(":visible")) {
                        var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                        if (fundCategoryCheckboxLength > 0) {
                            $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                        } else {
                            $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                        }
                    }

                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }

            });

            // clear all js
            $('.js-clearAll').click(function() {
                // $('.filter-tag').remove();
                $('.js-FilterSlide').slick('slickRemove', null, null, true);
                $('.js-filterCheck input').prop("checked", false);
                $('.filter-card .js-searchInput').val('');

                $('.js-FilterSlide').slick('refresh');
                hideFilterTagSection();

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    $('[data-filter-link]').removeClass('hasFilter');
                    $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                }

                // initialize loader
                loader();

                // remove filter count
                $('.js-filterCount').removeClass('show').text('');
            });

            // filter tag remove js
            $('.filter-tags-wrap').on('click', '.filter-tag .js-removeTag', function() {
                var tagName = $(this).parent('.filter-tag').attr('data-tag');
                $(this).parent('.filter-tag[data-slick-index]').remove();
                $('.js-filterCheck input[value="' + tagName + '"]').prop("checked", false);

                $('.js-FilterSlide').slick('slickRemove', tagName);
                $('.js-FilterSlide').slick('refresh');
                var filterSlideCount = $('.js-FilterSliderInit li').length;
                if (filterSlideCount <= 5) {
                    $('.js-FilterSliderInit').addClass('overflow-visible');
                } else {
                    $('.js-FilterSliderInit').removeClass('overflow-visible');
                }

                if ($(".js-AmcFilterSlideInit").is(":visible")) {
                    var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                    if (filterSlideCount <= 3) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                }

                hideFilterTagSection();
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");


                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }
            });

            // clear filter card js
            $('.js-clearBtn').click(function() {
                $(this).parents('.filter-card').find('input').prop("checked", false);

                // amc filter search clear and show list js
                $(this).parents('.filter-card').find('.js-searchInput').val('');
                $(this).parents('.filter-card').find('.amc-filter-list li').show();
                $(this).parents('.filter-card').find('.js-noSearchResult').addClass('d-none');

                $(this).parents('.filter-card').find('input').each(function() {
                    var filterVal = $(this).val();
                    $('[data-tag="' + filterVal + '"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));
                });
                hideFilterTagSection();
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");
                $('.js-FilterSlide').slick('refresh');
                var filterSlideCount = $('.js-FilterSliderInit li').length;
                if (filterSlideCount <= 5) {
                    $('.js-FilterSliderInit').addClass('overflow-visible');
                } else {
                    $('.js-FilterSliderInit').removeClass('overflow-visible');
                }

                if ($(".js-AmcFilterSlideInit").is(":visible")) {
                    var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                    if (filterSlideCount <= 3) {
                        $('.js-FilterSlide').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSlide').removeClass('overflow-visible');
                    }
                }

                // mobile remove yellow dot from filter title
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').removeClass('hasFilter');
                }

                if ($(".js-fundCategoryBox").is(":visible")) {
                    var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                    if (fundCategoryCheckboxLength > 0) {
                        $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                    } else {
                        $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                    }
                }

                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }
            });

            // select all filter card js
            $('.js-selectAllBtn').click(function() {
                $(this).parents('.filter-card').find('input').prop("checked", true);

                $(this).parents('.filter-card').find('input').each(function() {
                    var filterVal = $(this).val();
                    $('[data-tag="' + filterVal + '"]').remove();
                    $('.js-FilterSlide').slick('slickRemove', $('[data-tag="' + filterVal + '"]').attr('data-slick-index'));

                    var filterTagHtml = '<li class="filter-tag" data-tag="' + filterVal + '">' + filterVal + '<a href="javascript:void(0);" class="remove-tag js-removeTag"><span class="icon-close"></span></a></li>'

                    setTimeout(function() {
                        $('.filter-tags').append(filterTagHtml);
                    }, 1500);
                });

                setTimeout(function() {
                    $('.filter-tags-wrap').slideDown();
                    $('.js-FilterSlide').slick('refresh');
                    var filterSlideCount = $('.js-FilterSliderInit li').length;
                    if (filterSlideCount <= 5) {
                        $('.js-FilterSliderInit').addClass('overflow-visible');
                    } else {
                        $('.js-FilterSliderInit').removeClass('overflow-visible');
                    }

                    if ($(".js-AmcFilterSlideInit").is(":visible")) {
                        var filterSlideCount = $('.js-AmcFilterSlideInit li').length;
                        if (filterSlideCount <= 3) {
                            $('.js-FilterSlide').addClass('overflow-visible');
                        } else {
                            $('.js-FilterSlide').removeClass('overflow-visible');
                        }
                    }
                }, 1500);
                // $('.filterTags-mCustomScrollbar').mCustomScrollbar("update");


                // initialize loader
                if ($(window).width() > 991) {
                    loader();
                }

                // mobile add yellow dot from filter title
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }

                if ($(".js-fundCategoryBox").is(":visible")) {
                    var fundCategoryCheckboxLength = $('.fund-category-box .filter-link.hasFilter').length;
                    if (fundCategoryCheckboxLength > 0) {
                        $('.js-fundCategoryBoxOpen').addClass('hasFilter');
                    } else {
                        $('.js-fundCategoryBoxOpen').removeClass('hasFilter');
                    }
                }

            });

            // filter search js
            $(".js-searchInput").on("keyup", function() {
                var value = $(this).val().toLowerCase();
                $(".amc-filter-list li").filter(function() {
                    $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                });
                if ($(".amc-filter-list li").is(':visible')) {
                    $('.js-noSearchResult').addClass('d-none');
                } else {
                    $('.js-noSearchResult').removeClass('d-none');
                }
            });

            function checkboxCount() {
                if ($('.js-inlineFilterClearAll input:checked').length >= 1) {
                    $('.js-clearAll').show();
                } else {
                    $('.js-clearAll').hide();
                }
            }

            if ($(".js-inlineFilterClearAll").is(":visible")) {

                $('.js-inlineFilterClearAll').change(function() {
                    // initialize loader
                    if ($(window).width() > 991) {
                        loader();
                    }
                    checkboxCount();
                });

                $('.filter-tags-wrap').on('click', '.filter-tag .js-removeTag', function() {
                    checkboxCount();
                });
                $('.js-clearBtn').click(function() {
                    checkboxCount();
                });
                $('.js-clearAll').click(function() {
                    $('.js-inlineFilterClearAll input').prop("checked", false);
                    checkboxCount();
                });
            }

            /* -------- mobile filter js start -------- */

            // show filter
            $('.js-showFilter').click(function() {
                $('.js-mobFilterBox').addClass('open');
                $('body').addClass('stopScroll');
            });

            // hide filter
            $('.js-closeFilter').click(function() {
                $('.js-mobFilterBox').removeClass('open');
                $('body').removeClass('stopScroll');
            });

            // show filter respective card
            $('[data-filter-link]').click(function() {
                $('[data-filter-link]').removeClass('active');
                $(this).addClass('active');
                var filterCardItem = $(this).attr('data-filter-link');
                $('[data-filter-item]').removeClass('active');
                $('[data-filter-item="' + filterCardItem + '"]').addClass('active');
            });

            // show fund category filter
            $('.js-fundCategoryBoxOpen').click(function() {
                $('.js-fundCategoryBox').addClass('active');
                $('[data-filter-item="Popular Categories"]').addClass('active');

                $('[data-filter-link="Popular Categories"]').addClass('active');
            });

            // hide fund category filter
            $('.js-closefundCategory').click(function() {
                $('.js-fundCategoryBox').removeClass('active');
                $('[data-filter-item]').removeClass('active');
                $('[data-filter-item="Rating"]').addClass('active');
                $('[data-filter-link]').removeClass('active');
                $('[data-filter-link="Rating"]').addClass('active');
            });

            // apply filer
            $('.js-applyFilter').click(function() {

                // initialize loader
                loader();

                // close filter
                setTimeout(function() {
                    $('body').removeClass('stopScroll');
                    $('.js-mobFilterBox').removeClass('open');
                }, 1500);

                // filter count
                var filterCount = $('.js-filterListWrap li .hasFilter').length;
                if (filterCount > 0) {
                    $('.js-filterCount').addClass('show').text(filterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }

                // sebi filter count
                var sebiFilterCount = $('.js-filterListWrap li .hasFilter').length;
                if (sebiFilterCount > 0) {
                    $('.js-filterCount').addClass('show').text(sebiFilterCount);
                } else {
                    $('.js-filterCount').removeClass('show').text('');
                }
            });

            /* -------- mobile filter js end -------- */

            $('.js-FilterSliderInit').slick({
                autoplay: false,
                slidesToShow: 5,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });
            $('.js-AmcFilterSlideInit').slick({
                autoplay: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                dots: false,
                variableWidth: true,
                infinite: false,
            });


        }

        return jsHelper.freezeObj(fundFilterBoxObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundFilterBoxBizObj", fundFilterBoxBizObj)
})(this || window || {});
/*fund-filter-box js end*/