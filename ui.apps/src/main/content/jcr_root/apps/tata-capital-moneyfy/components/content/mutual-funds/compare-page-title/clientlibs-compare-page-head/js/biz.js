/*compare page title  biz.js start*/
(function (_global) {
    var comparePageTitleBizFn = (function (jsHelper) {
        var comparePageTitleObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            if ($('[data-comparepagebtn]') != undefined) {
                $('[data-comparepagebtn]').click(function (event) {
                    event.preventDefault();
                    var ctaTitle = $(this).parents('.head-wrap').find('h1').text().trim();
                    var ctaText = $(this).text().trim();
                    allCTAInteraction(ctaText, ctaTitle, 'compare-page-title', userIdObj.userId)
                    location.href = $(this).attr('href')
                })
            }
        });

        return jsHelper.freezeObj(comparePageTitleObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "comparePageTitleBizObj", comparePageTitleBizFn)
})(this || window || {});
/*compare page title biz.js end*/