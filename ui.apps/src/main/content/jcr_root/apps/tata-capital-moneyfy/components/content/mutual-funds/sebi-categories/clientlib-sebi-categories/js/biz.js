/*Sebi Categories js*/
(function (_global) {
  var sebiCategoryBizObj = (function (jsHelper) {
    var sebiCategoryObj = {};

    document.addEventListener("DOMContentLoaded", function () {
      initializeSebiCategory();
      adobeAnalytics();
    });

    function initializeSebiCategory() {
      /*tab js*/
      $(".jsTabList [data-tab]").click(function () {
        $(this).parents(".jsTabList").find("li a").removeClass("active");
        $(this).addClass("active");
        var ele_id = $(this).attr("data-tab");
        $(this).parents(".jsTabContainer").find(".jsTabRow").addClass("d-none");
        $("#" + ele_id)
          .fadeIn()
          .removeClass("d-none");
      });
      /*tab js*/

      if ($(".jsSebiEquityList").find("li").length < 9) {
        $(".jsSebiEquityList").siblings(".view-all-btns").addClass("d-none");
      }

      $(".jsSebiEquityList li").slice(0, 8).show();
      $("#jsViewMoreEquity").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiEquityList li:hidden").slice(0, 8).fadeIn();
        if ($(".jsSebiEquityList li:hidden").length == 0) {
          $("#jsViewLessEquity").removeClass("d-none").fadeIn("slow");
          $("#jsViewMoreEquity").hide();
        }
      });
      $("#jsViewLessEquity").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiEquityList li:not(:lt(8))").fadeOut();
        $("#jsViewMoreEquity").fadeIn("slow");
        $("#jsViewLessEquity").hide();
      });
      /*sebi category equity*/

      /*sebi category debt*/
      if ($(".jsSebiDebtList").find("li").length < 9) {
        $(".jsSebiDebtList").siblings(".view-all-btns").addClass("d-none");
      }
      $(".jsSebiDebtList li").slice(0, 8).show();
      $("#jsViewMoreDebt").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiDebtList li:hidden").slice(0, 8).fadeIn();
        if ($(".jsSebiDebtList li:hidden").length == 0) {
          $("#jsViewLessDebt").removeClass("d-none").fadeIn("slow");
          $("#jsViewMoreDebt").hide();
        }
      });
      $("#jsViewLessDebt").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiDebtList li:not(:lt(8))").fadeOut();
        $("#jsViewMoreDebt").fadeIn("slow");
        $("#jsViewLessDebt").hide();
      });
      /*sebi category debt*/

      /*sebi category hybrid*/
      if ($(".jsSebiHybridList").find("li").length < 9) {
        $(".jsSebiHybridList").siblings(".view-all-btns").addClass("d-none");
      }
      $(".jsSebiHybridList li").slice(0, 8).show();
      $("#jsViewMoreHybrid").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiHybridList li:hidden").slice(0, 8).fadeIn();
        if ($(".jsSebiHybridList li:hidden").length == 0) {
          $("#jsViewLessHybrid").removeClass("d-none").fadeIn("slow");
          $("#jsViewMoreHybrid").hide();
        }
      });
      $("#jsViewLessHybrid").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiHybridList li:not(:lt(8))").fadeOut();
        $("#jsViewMoreHybrid").fadeIn("slow");
        $("#jsViewLessHybrid").hide();
      });
      /*sebi category hybrid*/

      /*sebi category other*/
      if ($(".jsSebiOtherList").find("li").length < 9) {
        $(".jsSebiOtherList").siblings(".view-all-btns").addClass("d-none");
      }
      $(".jsSebiOtherList li").slice(0, 8).show();
      $("#jsViewMoreOther").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiOtherList li:hidden").slice(0, 8).fadeIn();
        if ($(".jsSebiOtherList li:hidden").length == 0) {
          $("#jsViewLessOther").removeClass("d-none").fadeIn("slow");
          $("#jsViewMoreOther").hide();
        }
      });
      $("#jsViewLessOther").on("click", function (e) {
        e.preventDefault();
        $(".jsSebiOtherList li:not(:lt(8))").fadeOut();
        $("#jsViewMoreOther").fadeIn("slow");
        $("#jsViewLessOther").hide();
      });
      /*sebi category other*/
    }
    function adobeAnalytics(){
        $('[data-sebicatlist] a').click(function(event){
            event.preventDefault();
            var tabTitle = $(this).text().trim();
            tabInteraction(tabTitle,'sebi-categories',userIdObj.userId)
        })
        $('[data-tabcontentlist]').click(function(event){
            event.preventDefault();
            var fundCategory = $('[data-sebicatlist]').find('[class="active"]').text().trim();
            var fundType = $(this).text().trim();
            fundCategoryItemClick(fundCategory, userIdObj.userId,fundType)
            location.href = $(this).attr('href');
        })
        $('[data-moremutual]').click(function(event){
            event.preventDefault();
            var ctaTitle = $('[data-sebicatlist]').find('[class="active"]').text().trim();
            var ctaText = $(this).text().trim();
            allCTAInteraction(ctaText,ctaTitle,'sebi-categories',userIdObj.userId);
            location.href = $(this).attr("href");
          })
    }
    return jsHelper.freezeObj(sebiCategoryObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "sebiCategoryBizObj", sebiCategoryBizObj);
})(this || window || {});
/* Sebi Categories js*/
