/*fd calculator js start*/
(function (_global) {
    var fdCalculatorBizObj = (function (jsHelper) {
        var fdCalculatorObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            fdCalculatorInitializer();
            fdCalculatorEvents();
            fdCalculatorCalculationFn(60)
            $('[data-fd-btn="fdredirection"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('fd-calculator-box', userId, 'FD calculator', ctaText);    
                        }else{
                            calculatorCtaInteraction('fd-calculator-box', userId, 'FD calculator', ctaText);
                        }   
                        
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('fd-calculator-box', "anonymous user", 'FD calculator', ctaText);
                        }else{
                            calculatorCtaInteraction('fd-calculator-box', "anonymous user", 'FD calculator', ctaText);
                        }
                    }
                } catch (err) {
                    console.log(err);
                }
                
                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    location.href = $(this).attr('href');
                }
            })
        })
        function fdCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
        }

        function fdCalculatorEvents() {
            // year month toggle
            $('[data-yrMthFilter]').click(function () {
                filterVal = $(this).attr('data-yrMthFilter');
                $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
                $(this).addClass('active');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
            });
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            var tenureObj = {
                ppfTenureYear: document.getElementById("fd_tenure_year"),
                ppfTenureMonth: document.getElementById("fd_tenure_month"),
                compundingInput: document.getElementById('compounding'),
                compundTenureBtn: document.getElementsByClassName('custom-border-radio'),
                fdErrorMsg: document.getElementById("fdErrorMsg"),

            }
            if ($(".js-calculatorRangeSlider").is(":visible")) {

                $(".js-calculatorRangeSlider").on("input", function () {
                    activeTenure(tenureObj)
                });
            }
            document.getElementById("fd_amount").addEventListener('keyup', function () {
                if (parseFloat(this.value.replace(/,/g, '')) > 10000000) {
                    tenureObj.fdErrorMsg.innerHTML = '';
                    tenureObj.fdErrorMsg.innerHTML = 'Amount should not greater than ₹10000000';
                } else if (parseFloat(this.value.replace(/,/g, '')) < 5000) {
                    tenureObj.fdErrorMsg.innerHTML = '';
                    tenureObj.fdErrorMsg.innerHTML = 'Amount should not less than ₹5000';
                }
                else {
                    tenureObj.fdErrorMsg.innerHTML = '';
                }
                activeTenure(tenureObj)
            })
            $('[data-yrMthFilter="year"]').click(function () {
                fdCalculatorCalculationFn(Number(tenureObj.ppfTenureYear.value) * 12);
            })
            $('[data-yrMthFilter="month"]').click(function () {
                fdCalculatorCalculationFn(Number(tenureObj.ppfTenureMonth.value));
            })
            tenureObj.compundTenureBtn[0].addEventListener('click', function () {
                tenureObj.compundingInput.value = 1;
                activeTenure(tenureObj)
            });
            tenureObj.compundTenureBtn[1].addEventListener('click', function () {
                tenureObj.compundingInput.value = 2;
                activeTenure(tenureObj)
            });
            tenureObj.compundTenureBtn[2].addEventListener('click', function () {
                tenureObj.compundingInput.value = 4;
                activeTenure(tenureObj)
            });
            tenureObj.compundTenureBtn[3].addEventListener('click', function () {
                tenureObj.compundingInput.value = 12;
                activeTenure(tenureObj)
            });

        }

        function activeTenure(tenureObj) {
            if (document.querySelector('[class="yr-mth-btn active"]').innerHTML == 'Years') {
                var convertYears = Number(tenureObj.ppfTenureYear.value) * 12;
                fdCalculatorCalculationFn(convertYears)
            } else {
                fdCalculatorCalculationFn(Number(tenureObj.ppfTenureMonth.value));
            }
        }

        function fdCalculatorCalculationFn(fdTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");

            var fdInputObj = {
                calculator: "FD",
                fdInput: Number(parseFloat(document.getElementById("fd_amount").value.replace(/,/g, ''))),
                fdTenure: fdTenure,
                roi: Number(document.getElementById("fd_roi").value) / 100,
                compoundFactor: Number(parseFloat(document.getElementById("compounding").value.replace(/,/g, ''))),
            }
            myWorker.postMessage(fdInputObj);
            myWorker.onmessage = function (e) {
                fdInputObj.fdHighchartReact = fdHighchartReact;
                fdCalculatorRenderObj.renderfdCalculation(e, fdInputObj);
                myWorker.terminate();
            }
        }

        function fdHighchartReact(invest, interest, initialAxis) {
            var reactChart = (invest / (invest + interest)) * 100;
            // Range slider end
            Highcharts.chart("fd-calculator-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: reactChart,
                            },
                        ],
                    },
                ],
            });

        }

        return jsHelper.freezeObj(fdCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fdCalculatorBizObj", fdCalculatorBizObj)
})(this || window || {});
/*fd calculator js end*/