Corporate fd banner box
====
This is a `Corporate fd banner box` component used in credit card loan page.

## Feature
* This is a multifield component.
* This component provides modify or edit the content on the component of the page.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./tabs` Used for add tabs on dialog box.
1. `./bannerLeft` this is a one tab.
1. `./bannerCard` this is second tab.
2. `./bannerHeading` Used for rendering heading of the component.
3. `./bannerTitle` Used for rendering title of the component.
4. `./btnLink` Used to render redirection link of a card.
5. `./btnTarget` Enable open redirection link in a new tab.
6. `./btnText` Used to rendering the name of the button.
7. `./bannerDescription` Used for rendering description of component.
8. `./enableCard` Used to enable and disable the card.
13. `./bannerImg` Used for rendering image on page.



## Client Libraries
The component provides a `moneyfy.corporate-fd-banner-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5