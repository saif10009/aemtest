Disclaimer
====
This component witten in HTL, allowing to render disclaimer on page.

## Feature
* This is authorable component.
* Used to provide disclaimer of moneyfy.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `heading` - Used to render heading for disclaimer.
2. `description` - Used to render information of disclaimer.


## Client Libraries
The component provides a `moneyfy.disclaimer` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page.
The clientlib embeded in `moneyfy.nps-landing-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-nps-landing`.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5