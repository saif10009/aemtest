Investment Products
====
This is a authorable why investment-products component that can be used to author the various investment options provided by moneyfy.

## Feature
* This is a slick-slider based multifield component.
* The title, heading, images all can be authored based on the requirements.

### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering title of the component.
2. `./heading` Used for rendering heading of the component.
3. `./desktopImage ./mobileImage ./typeImage` Used to select the different desktop-view, mobile-view, typeImage paths for these component.
4. `./description` Used to render description of an product type.
5. `./button1 ./button2` are used for custom text to be rendered on the buttons.

## Client Libraries
The component provides a `moneyfy.investmentProduct` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is already included in component HTML file.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5