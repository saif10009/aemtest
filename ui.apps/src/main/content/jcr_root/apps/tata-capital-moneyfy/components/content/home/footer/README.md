Footer
====
This component witten in HTL, allowing to render moneyfy footer.



## Feature
* Used to provide various redirection link of moneyfy page.
* Provide you the varoius way to contact with moneyfy.
* Provide varous social media network by using this you can reach out on social media `Moneyfy` page
* Provide termss and condition of moneyfy page.



### Edit Dialog Properties

This is `cq:disgn_dialog` based component which is based on `Moneyfy-Footer` ploicy.


## Client Libraries
The component provides a `moneyfy.footer` editor client library category that includes JavaScript and CSS handling for dialog interaction. It is embeded inside `moneyfy.homePage` clientlib, which call inside template level.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

