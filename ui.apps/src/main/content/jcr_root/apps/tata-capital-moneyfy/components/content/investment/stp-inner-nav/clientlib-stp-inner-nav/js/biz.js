/*dropdown js start*/
(function(_global) {
    var dropdownFn = (function(jsHelper) {
        var dropdownObj = {};
        document.addEventListener('DOMContentLoaded', function() {
            clickEvents();
            innerNavInitializer();
        });

        function clickEvents() {

            $('.js-filterBtns').click(function() {
                $('.js-filterBtns').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp('fast');

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });
        }

        function innerNavInitializer() {

            $(window).on('scroll', function() {
                header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        } else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '10' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }
                }
            });

            var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
            var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();

            function header_fixed() {
                var windowScroll = $(window).scrollTop();
                var topbarHeight = $('.top-bar').outerHeight();

                if (windowScroll > topbarHeight) {
                    $('header').addClass('affix');
                    $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
                } else {
                    $('header').removeClass('affix');
                    $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
                }
            }

            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }

            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }

        }
        return jsHelper.freezeObj(dropdownObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'dropdownObj', dropdownFn);
})(this || window || {});
/*dropdown js end*/