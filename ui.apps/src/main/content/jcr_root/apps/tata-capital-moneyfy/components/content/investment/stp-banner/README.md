STP banner 
====
The `STP banner` component is for adding Banner on moneyfy investment STP page. 



## Feature
* All the various elements of the component such as title , heading , descriptions,image and link are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used to add title text.
2. `./heading` Used to render heading.
3. `./description` Used for description.
4. `./buttonTitle` Used to render button text.
5. `./buttonLink` used to redirect button link.
6. `./newTab` used to redirect link in new tab.
7. `./videoLink` Used to enter link for video.
8. `./bannerImg` Used to render image on banner.


## Client Libraries
The component provides a `moneyfy.stp-banner` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.stp-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-stp.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5