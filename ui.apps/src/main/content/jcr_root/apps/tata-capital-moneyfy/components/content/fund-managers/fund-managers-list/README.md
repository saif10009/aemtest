Fund Managers List
====
The `Fund Managers List` component is for showing the list of managers on the fund-managers-list page.



## Feature
* It is an dynamic component.



## Edit Dialog Properties
This is an dynamic component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.choose-from-amc` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5