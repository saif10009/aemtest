/*credit card loan js start*/
(function (_global) {
    var goldInsuranceBizObj = (function (jsHelper) {
        var goldInsuranceObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            if ($('[data-log-popup="logpop"]')) {
                $('[data-log-popup="logpop"]').click(function (event) {
                    event.preventDefault();
                    var anchorLink = $(this).attr('href');
                    if (!headerBizObj.getUserObj().isLoggedIn) {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = appConfig.jocataDomain + anchorLink;
                        });
                    } else {
                        location.href = appConfig.jocataDomain + anchorLink;
                    }
                })
            }
            adobeAnalyticsCalls();
        });

        function adobeAnalyticsCalls() {
            $('[data-adobeAnalytics="true"]').click(function () {
                var bannerTitle = document.querySelector('[data-bannertitle]').dataset.bannertitle;
                var banneCTA = document.querySelector('[data-bannercta]').dataset.bannercta;
                bannerInteraction(bannerTitle, banneCTA, 'gold-and-insurance-banner', userIdObj.userId);
            });

            $('[data-adobeAnalytics="insuranceBanner"]').click(function () {
                var insuranceName = document.querySelector('[data-insuranceBannerTitle]').dataset.insurancebannertitle;
                var banneCTA = $(this).text().trim();
                insurancePageCtaInteraction(insuranceName, userIdObj.userId, 'gold-and-insurance-banner', banneCTA);
            });
        }

        return jsHelper.freezeObj(goldInsuranceObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "goldInsuranceBizObj", goldInsuranceBizObj)
})(this || window || {});
  /*credit card loan js end*/
