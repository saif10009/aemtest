Amc Details Filter Box
====
The `Amc Details Filter Box` component is for adding the filter box on the amc-details page.



## Feature
* It is an static component.



## Edit Dialog Properties
The component has no properties stored in edit dialog.


## Client Libraries
The component provides a `moneyfy.amc-details-filter-box` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5