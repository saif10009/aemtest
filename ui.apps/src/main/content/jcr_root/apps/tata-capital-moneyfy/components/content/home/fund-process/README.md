Fund Process
====
The `Fund Process` component can be used to list the various options to start with mutual fund investing with moneyfy. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as images, headings, title and button txt as well as the popup is made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title ./popupTitle` Used for rendering titles of the components.
2. `./heading ./subHeading` Used for rendering heading & subheading of the component.
3. `./appleImg ./googleImg ./mobileImage ./popupImg` are used to select the different applestore, playstore, mobile-view, popup dialog images paths of these components.
4. `./description` Used to render description of an product type.
5. `./getLink` are used for custom text to be rendered on the buttons.
6. `./background` are used for choose option from dropdown.


## Client Libraries
The component provides a `tata.fundProcess` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The clientlib of this component are embedded in the `/apps/tata-capital-moneyfy/clientlibs/home-page/homePageClientlib` folder.  


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5

