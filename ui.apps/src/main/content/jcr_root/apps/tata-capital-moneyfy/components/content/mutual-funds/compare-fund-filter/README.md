Compare Fund Filter
====
The `Compare Fund Filter` component is for adding compare fund box on the compare page.



## Feature
* It is an static component.


## Edit Dialog Properties
There are no properties stored in the edit dialog of the component.

## Client Libraries
The component provides a `moneyfy.compare-fund-filter` editor client library category that includes JavaScript and CSS. `moneyfy.jquery` these dependancies libraries are use for load jquery,aos and slick file in component.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5