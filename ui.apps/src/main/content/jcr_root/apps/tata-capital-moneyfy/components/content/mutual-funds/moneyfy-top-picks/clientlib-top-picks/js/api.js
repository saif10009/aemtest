(function (_global) {

    var topPicksApiFn = (function (jsHelper) {
        var topPicksApiObj = {};


        return jsHelper.freezeObj(topPicksApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'topPicksApiObj', topPicksApiFn);
})(this || window || {});