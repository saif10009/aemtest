
(function (_global) {
    var calcGetEmailBizFn = (function (jsHelper) {
        var calcGetEmailBizObj = {};

        var fieldCount = $('#jsGetResultForms .jsValueOK').length;
        $('.single-select3').select2({
            dropdownParent: $('.full-select2')
        });

        $(document).on('focus', '.select2.select2-container', function (e) {
            if (e.originalEvent && $(this).find(".select2-selection--single").length > 0) {
                $(this).siblings('select').select2('open');
                setTimeout(function () {
                    const searchField = document.querySelector('.select2-search__field');
                    if (searchField) {
                        searchField.focus();
                    }
                }, 10)
            }
        });

        // Mobile validation 
        function validateMobile(mobileField) {
            var re = /^[6-9][0-9]{9}$/;
            var check = re.test($(mobileField).val());
            if ($(mobileField).val().length != 10 || !check) {
                return false;
            } else {
                return true;
            }
        }

        $('#jsGetResultForms [data-type="checkbox"]').change(function () {
            if (this.checked) {
                $(this).parents('.form-group').removeClass('textboxerror');
            } else {
                $(this).parents('.form-group').addClass('textboxerror');
            }
        });

// 19-4-start
        $('#jsGetResultForms input[type="checkbox"]').change(function() {
            //console.log(this.val)
            if (this.checked){
                $(this).removeClass('jsValueOK')
            } else {
                $(this).addClass('jsValueOK')
            }
            fieldCount = $('#jsGetResultForms .jsValueOK').length;
            //console.log('this is fieldCOunt', fieldCount)

            if (fieldCount === 0){
                $('#jsGetResultForms .jsGetResultBtn').removeClass('btn-disabled')
            } else{
                $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled')
            }
        });

        $('.jsGetResultClose').click(function(){
            $('[data-forms]').removeClass('d-none');
            $('[data-otpverify]').addClass('d-none');
            $('[otp-successfull]').addClass('d-none');
            $('[otp-unsuccessfull]').addClass('d-none');
            $('#jsGetResultForms .input-textbox').val('');
            $('#jsGetResultForms .form-group').removeClass('textboxerror');
            $('#jsGetResultForms .input-textbox').next().text('');
            $('#jsGetResultForms .single-select3').val(null).trigger('change');
            $('#jsGetResultForms [data-type="checkbox"]').prop("checked", false);
            //reset
            $('.single-select3').each(function () {
                $('.single-select3').parents('.form-textbox-new').addClass('active');
                $('.single-select3').addClass('jsValueOK')
            });
            $('#jsGetResultForms .input-textbox[data-type]').addClass('jsValueOK');
            fieldCount = $('#jsGetResultForms .jsValueOK').length;
            $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled')
        })
// 19-4 end

        $('#jsGetResultForms .single-select3').change(function () {
            $(this).parents('.form-group').removeClass('textboxerror');
            $(this).next('.error-msgs').remove();
// 19-4 start
            $(this).removeClass('jsValueOK')
            fieldCount = $('#jsGetResultForms .jsValueOK').length;
            //console.log(fieldCount)
            if (fieldCount === 0){
                $('#jsGetResultForms .jsGetResultBtn').removeClass('btn-disabled')
            } else{
                $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled')
            }
// 19-4 end
        });

        $('#jsGetResultForms .jsGetResultBtn').click(function () {
            var ele_input = $('#jsGetResultForms .input-textbox:visible');
            var selectElements = $('#jsGetResultForms .select2-hidden-accessible[data-type]:visible');
            var checkboxElements = $('#jsGetResultForms [data-type="checkbox"]');
            // 19-4 start
            var checkEleWithoutDataType = $('#jsGetResultForms input[type="checkbox"]');
            // 19-4 end
            var errors = [];
            allFilled = true;
            var ele_required = "Field is required";

            $(selectElements).each(function () {
                var select = $(this);
                $(select).parents('.form-group').find('.error-msgs').remove();

                if ($(select).val() == '') {
                    allFilled = false;
                    $(select).parents('.form-group').addClass('textboxerror');
                    $(select).next('.error-msgs').remove();
                    $(select).after('<span class="error-msgs">' + ele_required + '</span>');
                    errors.push(ele_required);
                } else {
                    $(select).parents('.form-group').removeClass('textboxerror');
                    $(select).next('.error-msgs').remove();
                }
            });

            $(ele_input).each(function () {
                var element = $(this);
                var ele_value = element.val();
                var ele_name = "Enter full name as per PAN card";
                var ele_phoneNumber = "Please enter valid Phone Number";
                var ele_email = "Please enter valid email ID";

                $(element).next().remove();

                if (element.is(":visible")) {
                    if (element.val() != '') {
                        $(element).after('<span class="error-msgs"></span>');
                        if ($(element).data('type') === 'name') {
                            var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

                            if (ele_value != '' && !ele_value.match(regName)) {
                                $(element).parents('.form-group').addClass('textboxerror');
                                $(element).next('.error-msgs').text(ele_name);
                                errors.push(ele_name);
                            }
                            else {
                                $(element).parents('.form-group').removeClass('textboxerror');
                                $(element).next().text('');
                            }
                        }
                        if ($(element).data('type') === 'email') {
                            var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;
                            if (ele_value != '' && !ele_value.match(regEmail)) {
                                $(element).parents('.form-group').addClass('textboxerror');
                                $(element).next('.error-msgs').text(ele_email);
                                errors.push(ele_email);
                            }
                            else {
                                $(element).parents('.form-group').removeClass('textboxerror');
                                $(element).next().text('');
                            }
                        }
                        if ($(element).data('type') === 'mobile') {
                            if (!validateMobile(element)) {
                                $(element).parents('.form-group').addClass('textboxerror');
                                $(element).next('.error-msgs').text(ele_phoneNumber);
                                errors.push(ele_phoneNumber);
                            } else {
                                $(element).parents('.form-group').removeClass('textboxerror');
                                $(element).next().text('');
                                $(this).next('.error-msgs').remove();
                            }
                        }
                    } else {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).after('<span class="error-msgs">' + ele_required + '</span>');
                        errors.push(ele_required);
                    }
                }
            });

            $('#jsGetResultForms [data-type="checkbox"]').next('.error-msgs').remove();
            // checkbox validation
            if (checkboxElements.prop("checked") == false) {
                checkboxElements.parents('.form-group').addClass('textboxerror');
                $(checkboxElements).parents('.agree-checkbox').next('.error-msgs').remove();
                checkboxElements.parents('.agree-checkbox').after('<span class="error-msgs">' + ele_required + '</span>');
                errors.push(ele_required);
            } else {
                $(checkboxElements).parents('.form-group').removeClass('textboxerror');
                $(checkboxElements).parents('.agree-checkbox').next('.error-msgs').remove();
            }

            if (checkEleWithoutDataType.prop("checked") == false ){
                errors.push(ele_required);
            }
            console.log(errors.length)
            
            if (errors.length == 0) {
                calcGetEmailApiObj.generateOTP();
                var getWhatsappNub = $('.jsGetWhatsappNumber').val();
                var jsGetNumberLast = String(getWhatsappNub).slice(-2);
                $('.jsShowWhatsappNumber').text(jsGetNumberLast);

                var getEmail = $('#jsGetResultForms .js-emailField').val();
                var emailEncrypt = getEmail.replace(/(\w{2})(.*)(\w{2})@(.*)/, '$1XXXXXX$3@$4');
                $('.js-encryptEmail').text(emailEncrypt);
                //console.log(emailEncrypt);
                //$('[data-otpverify]').removeClass('d-none');
                $('[data-forms]').addClass('d-none');
                $('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
                // $('#jsGetResultForms .input-textbox').val(''); 
                //$('#jsGetResultForms .single-select3').val(null).trigger('change');
            } else {
                $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled');
            }
            // 19-4 end
        });

        $('#jsGetResultForms .input-textbox[data-type]').keyup(function () {
            var element = $(this);
            var ele_value = element.val();
            var ele_required = 'Field is required';
            var ele_name = "Enter full name as per PAN card";
            var ele_phoneNumber = "Please enter valid number";
            var ele_email = "Please enter valid email ID";

            $(this).next('.error-msgs').remove();
            $(this).after('<span class="error-msgs"></span>');
            $(this).parents('.form-group').addClass('textboxerror');

            if ($(element).val() != '') {
                if ($(element).data('type') === 'name') {
                    var regName = /^[a-zA-Z]+ [a-zA-Z]+$/;

                    if (ele_value != '' && !ele_value.match(regName)) {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).next('.error-msgs').text(ele_name);
                        $(element).addClass('jsValueOK')
                    }
                    else {
                        $(element).parents('.form-group').removeClass('textboxerror');
                        $(element).next().text('');
                        $(element).removeClass('jsValueOK')
                    }
                }
                if ($(element).data('type') === 'mobile') {
                    if (!validateMobile(element)) {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).next('.error-msgs').text(ele_phoneNumber);
                        $(element).addClass('jsValueOK')
                    } else {
                        $(element).parents('.form-group').removeClass('textboxerror');
                        $(element).next().text('');
                        $(this).next('.error-msgs').remove();
                        $(element).removeClass('jsValueOK')
                    }
                }
                if ($(element).data('type') === 'email') {
                    var regEmail = /^[a-zA-Z0-9_.]+[@][a-zA-Z0-9]+[\.][a-zA-z0-9]{2,4}$/gm;

                    if (ele_value != '' && !ele_value.match(regEmail)) {
                        $(element).parents('.form-group').addClass('textboxerror');
                        $(element).next('.error-msgs').text(ele_email);
                        $(element).addClass('jsValueOK')
                    }
                    else {
                        $(element).parents('.form-group').removeClass('textboxerror');
                        $(element).next().text('');
                        $(element).removeClass('jsValueOK')
                    }
                }

            } else {
                $(element).next('.error-msgs').text(ele_required);
            }
            // 19-4 start
            fieldCount = $('#jsGetResultForms .jsValueOK').length;
            //console.log(fieldCount)
        
            if (fieldCount === 0){
                $('#jsGetResultForms .jsGetResultBtn').removeClass('btn-disabled')
            } else{
                $('#jsGetResultForms .jsGetResultBtn').addClass('btn-disabled')
            }
            //  19-4 end
        });


        var onGenerateOtpSuccess = function () {
            $("#jsVeryfyOTP .js-OtpBox .input-textbox").keyup(function () {
                if (this.value.length == this.maxLength) {
                    $(this).next('.input-textbox').focus();
                    $(this).next('.input-textbox').removeClass('pointer-none');
                } else {
                    $(this).prev('.input-textbox').focus();
                    $(this).addClass('pointer-none');
                    $('#jsVeryfyOTP .input-textbox:first').removeClass('pointer-none');
                }

                var ele_input = $('.js-OtpBox .input-textbox');
                $(ele_input).each(function () {
                    if ($(this).val().length != 0) {
                        $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').removeClass('btn-disabled');
                        $(this).parents('.form-group').addClass('active');
                    }
                    else {
                        $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
                    }
                });
            });

            $('.jsVeryfyBtn').click(function () {
                //$('#jsGetResultForms .input-textbox').val('');
                var values = []
                $('#jsVeryfyOTP .js-OtpBox .input-textbox').each(function (i, ele) { values.push(ele.value) });
                console.log(values.join(""))
                calcGetEmailApiObj.verifyOTP();
                $('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
            });

            $('#jsVeryfyOTP .js-resendOTP').click(function () {
                console.log(calcGetEmailApiObj.fetchData().city);
                $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox').val('');
                $(this).parents('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
                $(this).parents('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
                var getMobileNumber = calcGetEmailApiObj.fetchData().mobileNumber;
                var firstTwoDigit = getMobileNumber.toString().slice(-2);
                document.querySelector('#jsVeryfyOTP .jsShowWhatsappNumber').innerText = firstTwoDigit;
                calcGetEmailApiObj.generateOTP();
            });

            /* document.querySelector('.btn-try-again .jsTryAgainOtp').click(function(){
                $('[otp-unsuccessfull]').addClass('d-none');
                document.querySelector('[data-thank="thankYou"]').classList.add('d-none');
                $('[data-otpverify]').removeClass('d-none');
                $('#jsVeryfyOTP').find('.js-OtpBox .input-textbox:first-child').focus();
                $('#jsVeryfyOTP').find('.jsVeryfyBtn').addClass('btn-disabled');
                calcGetEmailApiObj.generateOTP();
            });

            document.querySelector('.btn-try-again .jsTryAgainOtp').addEventListener('click', function(){
                document.querySelector('[data-thank="thankYou"]').classList.add('d-none');
                document.querySelector('[data-otpverify="verifyOTP"]').classList.remove('d-none'); 
                calcGetEmailApiObj.tryAgainGenerateOtp();
            });*/

           /* $('.jsGetResultClose').click(function () {
                $('[data-forms]').removeClass('d-none');
                $('[data-otpverify]').addClass('d-none');
                $('[otp-successfull]').addClass('d-none');
                $('[otp-unsuccessfull]').addClass('d-none');
                //$('#jsGetResultForms .input-textbox').val('');
                $('#jsGetResultForms .form-group').removeClass('textboxerror');
                $('#jsGetResultForms .input-textbox').next().text('');
                $('#jsGetResultForms .single-select3').val(null).trigger('change');
                $('#jsGetResultForms [data-type="checkbox"]').prop("checked", false);
            });*/
        };

        function showLoginPopupSetting() {
            if ((window.location.href.split('/').pop() == 'mutual-funds' || (window.location.href.split('/').pop().split('.')[0] == 'mutual-funds')) &&
                !getUserObj().isLoggedIn && (oneTimeMfPopup == false)) {
                sessionStorage.setItem("loginPopupCount", 0);
            }
            setTimeout(function () {
                if ((Number(sessionStorage.getItem("loginPopupCount")) == 0) && (isNotMobileApp) && !getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain;
                    });
                }
                sessionStorage.setItem("loginPopupCount", 1);
            }, 550);
        }

        document.querySelectorAll('.get-result-mgs a').forEach(function (getResult) {
            getResult.addEventListener('click', function () {
                document.querySelector('#getResulton .jsGetResultClose').addEventListener('click', function () {
                    showLoginPopupSetting()
                });
                document.querySelector('[data-thank="thankYou"] .jsGetResultClose').addEventListener('click', function () {
                    showLoginPopupSetting()
                });
                if (mfLoginTimeoutId) {
                    clearTimeout(mfLoginTimeoutId);
                }
            });
        });

        // city search api call
        calcGetEmailApiObj.apiCall('GET', '/content/tata-capital/mdm.cityproductmaster.json').then(function (response) {
            var obj = JSON.parse(response);
            var mainObj = {};
            for (var item in obj.Master) {
                var subObj = {};
                if (!mainObj.hasOwnProperty(obj.Master[item].product)) {
                    var arr = [];
                    mainObj[obj.Master[item].product] = subObj;
                    subObj['productName'] = obj.Master[item]['product-name'];
                    arr.push(obj.Master[item].city);
                    subObj['cities'] = arr;

                } else {
                    var subObj = mainObj[obj.Master[item].product];
                    var arr = subObj['cities']
                    arr.push(obj.Master[item].city);
                }
            }
            response = mainObj;
            var productCode = 'PL';
            if (productCode == "LAP" || productCode == "LAPOD") {
                productCode = "HE";
            }
            if ((productCode == 'TW101') || (productCode == 'MO101') || (productCode == 'PR105') || (productCode == 'SITR107') || (productCode == 'HE103') || (productCode == 'HE104') || (productCode == 'HE105') || (productCode == 'WS101') || (productCode == 'WP101') || (productCode == 'HA101') || (productCode == 'HA101') || (productCode == 'HC101') || (productCode == 'CS101') || (productCode == 'PE101') || (productCode == 'RS101') || (productCode == 'CP101') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'CIS101') || (productCode == "HO104")) {
                productCode = "INSURANCE";
            }
            if (response[productCode] !== undefined && response[productCode].cities.length > 0) {
                response[productCode].cities.sort(function (a, b) {
                    if (a < b) { return -1; }
                    if (a > b) { return 1; }
                    return 0;
                })
                    .forEach(function (element) {
                        var citiesContainer = document.querySelector('#searchCity');
                        citiesContainer.innerHTML += '<option>' + ' <a href="javascript:void(0)">' + element + '</a>' + '</option>'
                    });
            } else {
                console.log("CSV City issue");
            }
        });

        document.querySelectorAll('.jsGetResultClose').forEach(function (el) {
            el.addEventListener('click', function () {
                $('#getResulton .input-textbox').val('');
            });
        });

        calcGetEmailBizObj.onGenerateOtpSuccess = onGenerateOtpSuccess;

        return jsHelper.freezeObj(calcGetEmailBizObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, "calcGetEmailBizObj", calcGetEmailBizFn);
})(this || window || {});
