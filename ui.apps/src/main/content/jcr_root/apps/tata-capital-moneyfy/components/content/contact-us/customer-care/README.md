Customer care
====
This is a `Customer care` component used in contact us page.

## Feature
* This is a multifield component.
* All the various elements of the component such as descriptions, images, headings, redirection links, redirection target  are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./weAreBasedHeading ,./appStoreHeading` Used for rendering headings of the component.
2. `./cultureBoxImage , ./investmentImage ,./quriesImage ,./image` Used for rendering image on page.
3. `./infoColDescription ,./infoColDescription ,./description` used for render description in component.
4. `./appStoreLinkListMultifield ,./phoneMailListMultifield ,./socialListMultifield` Used for add multifield in component.
5. `./appStoreImageLink ,./link` Used to give redirection link to image.
6. `./appStoreImageTarget ,./target` Used give target to link


## Client Libraries
The component provides a `moneyfy.moneyfy.customer-care` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5