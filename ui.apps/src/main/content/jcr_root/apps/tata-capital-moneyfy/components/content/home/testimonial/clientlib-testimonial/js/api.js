(function (_global) {

    var testimonalApiFn = (function (jsHelper) {
        var testimonalApiObj = {};

        var testimonalFn = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.testimonalApi(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        testimonalApiObj.testimonalFn = testimonalFn;

        return jsHelper.freezeObj(testimonalApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'testimonalApiObj', testimonalApiFn);
})(this || window || {});