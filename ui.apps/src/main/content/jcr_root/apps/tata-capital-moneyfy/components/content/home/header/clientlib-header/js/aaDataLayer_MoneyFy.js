var digitalData=digitalData || {};  
digitalData.page=digitalData.page||{}; 				// Information of the page access by consumer
digitalData.product=digitalData.product||{};
digitalData.impression=digitalData.impression||{}; 				// Information of product accees or purchesed by consumer.
digitalData.consumer=digitalData.consumer||{}; 				// Information about the consumer
digitalData.cart=digitalData.cart||{};				// Information of Order or application made by consumer.
digitalData.event=digitalData.event||{};				// ClickStream events made by consumer.
digitalData.system=digitalData.system||{};              //contain the information which provided by the system not fill by user.

//this function will call on each page load
function pageInitialization(pageName,siteSection,siteSubSection,pageType,pathName,loginStatus,userId)
{
    try{
        digitalData.page.pageName=pageName||"",
        digitalData.page.siteSection=siteSection||"",
        digitalData.page.siteSubSection=siteSubSection||"",
        digitalData.page.pageType=pageType||"",
        digitalData.page.pathName=pathName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loginStatus=loginStatus||"",
        digitalData.event.name="page initialization",
        callSatellite('all-page-initialization')
    }catch(er){}
}

//this function will fire when user click on any banner image or banner cta.
function bannerInteraction(bannerTitle,bannerCTA,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.bannerTitle=bannerTitle||"",
        digitalData.event.eventContext.bannerCTA=bannerCTA||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="banner Interaction",
        callSatellite("banner-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user click on Menu ITEM
function menuInteraction(menuTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.menuTitle=menuTitle||"", 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="menu Interaction",
        callSatellite("menu-interaction")
    }
    catch(err){console.log(err)}
}

//this function will call when side widget will open

function widgetnteraction(widgetTitle,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.widgetTitle=widgetTitle||"", 
        digitalData.consumer.userId=userId||"", 
        digitalData.event.eventName="widget Interaction",
        callSatellite("widget-interaction")
    }
    catch(err){console.log(err)}
}

//this function will fire when user interact with any tab over the website
function tabInteraction(tabTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.tabTitle=tabTitle||"", 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="tab Interaction",
        callSatellite("tab-interaction")
    }
    catch(err){console.log(err)}
}

//this function will fire when user interact with any tab over the website from NLI
function tabInteractionNLI(tabTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.tabTitle=tabTitle||"", 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="tab Interaction",
        callSatellite("tab-interaction-NLI",{tabTitle:tabTitle,componentName:componentName,userId:userId})
    }
    catch(err){console.log(err)}
}

//this function will fire for all the CTA's and Tiles interaction
function allCTAInteraction(ctaText,ctaTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="all cta Interaction",
        callSatellite("all-cta-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user click on Blog item 
function blogItemView(blogTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.blogTitle=blogTitle||"",  
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="blog item view ",
        callSatellite("blog-item-interaction")
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on video
function videoItemClick(videoTitle,componentName,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.videoTitle=videoTitle||"",  
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="video item click",
        callSatellite("video-item-interaction")
    }
    catch(err){console.log(err)}
} 

//this function will fire when user submit the Get App Link Form
function getAppLink(mobileNo,userId,componentName){
    try{
        digitalData.consumer=digitalData.consumer||{},
        digitalData.consumer.mobileNo=mobileNo==undefined?"":window.btoa(mobileNo),
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="get app Link",
        callSatellite('get-app-link')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on play store or App store img or icone click

function appStoreImgClick(storeName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.storeName=storeName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="store icon click",
        callSatellite('store-icon-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the internal search
function internalSearch(searchTerm,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.searchTerm=searchTerm||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="internal search",
        callSatellite('internal-search-term')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the filter
function filterApplied(filterType,FilterValue,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.filterType=filterType||"",
        digitalData.event.eventContext.FilterValue=FilterValue||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="filter applied",
        callSatellite('filter-applied')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did the add to wishlist
function addToWishList(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="add to wish list",
        callSatellite('add-to-wish-list')
    }
    catch(err){console.log(err)}
}

//this function will fire when user did compare fund
function compareFund(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="compare fund",
        callSatellite('compare-fund')
    }
    catch(err){console.log(err)}
}


//this function will fire when user click on Invest now button
function fundInvestNow(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund invest now",
        callSatellite('fund-invest-now')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on Invest now button from NLI section
function fundInvestNowNLI(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund invest now",
        callSatellite('fund-invest-now-NLI',{fundType:fundType,fundName:fundName,fundRiskCat:fundRiskCat,userId:userId})
    }
    catch(err){console.log(err)}
}


//this function will fire when user click on explore more/ view details
function fundViewDetails(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund view details",
        callSatellite('fund-view-details')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on explore more/ view details from NLI section
function fundViewDetailsNLI(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName="fund view details",
        callSatellite('fund-view-details-NLI',{fundType:fundType,fundName:fundName,fundRiskCat:fundRiskCat,userId:userId})
    }
    catch(err){console.log(err)}
}

//this function will call when user click on similar AMC items
function amcItemClick(amcTitle,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.amcTitle=amcTitle||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventName='amc title click',
        callSatellite('amc-item-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on goal based investment icon
function goalBaseInvestmentClick(goalItem,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.goalItem=goalItem||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='goal base item click',
        callSatellite('goal-base-item-click')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on fund category item 
function fundCategoryItemClick(fundCategory,userId,fundType){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundCategory=fundCategory||"",
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='fundCategory title click',
        callSatellite('fundCategory-item-click')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on fund category item from NLI
function fundCategoryItemClickNLI(fundCategory,userId,fundType){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundCategory=fundCategory||"",
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='fundCategory title click',
        callSatellite('fundCategory-item-click-NLI',{fundCategory:fundCategory,userId:userId,fundType:fundType})
    }
    catch(err){console.log(err)}
}

//this function will fire when fund section expended
function fundSectionExpend(fundSectionName,userId){
try{
    digitalData.event.eventContext=digitalData.event.eventContext||{},
    digitalData.event.eventContext.fundSectionName=fundSectionName||"",
    digitalData.consumer.userId=userId||"",
    digitalData.event.eventName='fund section expend',
    callSatellite('fund-section-expend')
}
catch(err){console.log(err)}
}

//this function will fire when FAQ Expend
function faqExpend(faqTitle,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.faqTitle=faqTitle||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='faq expend',
        callSatellite('faq-expend')
    }
    catch(err){console.log(err)}
    }

//this function will fire when user click on start SIP
function startSIP(componentName,userId){
    try{digitalData.event.eventContext=digitalData.event.eventContext||{},
    digitalData.event.eventContext.componentName=componentName||"",
    digitalData.consumer.userId=userId||"",
    digitalData.event.eventName='start sip click',
    callSatellite('start-sip-click')}
    catch(err){console.log(err)}
}

//this function will call when user click on insurance item 
function insuranceItemClick(insuranceName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.insuranceName=insuranceName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='insurance item click',
        callSatellite('insurance-item-click')
    }
    catch(err){console.log(err)}
}


//this function will call when user click on individual insurance page cta interaction
function insurancePageCtaInteraction(insuranceName,userId,componentName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.insuranceName=insuranceName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='insurance page cta interaction',
        callSatellite('insurance-page-cta-interaction')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on loan item   
function loanItemClick(loanName,userId,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loanName=loanName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='loan item click',
        callSatellite('loan-item-click')
    }
    catch(err){console.log(err)}
}

//this function will call when user click on individual loan page cta interaction
function loanPageCtaInteraction(loanName,userId,componentName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.loanName=loanName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='loan page cta interaction',
        callSatellite('loan-page-cta-interaction')
    }
    catch(err){console.log(err)}
}


//this function will call when user click on start investing
function calculatorCtaInteraction(componentName,userId,calculatorName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.calculatorName=calculatorName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='calculator cta interaction',
        callSatellite('calculator-cta-interaction')

    }catch(er){}
}

//this function will call when user click on start investing from NLI section
function calculatorCtaInteractionNLI(componentName,userId,calculatorName,ctaText){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.calculatorName=calculatorName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='calculator cta interaction',
        callSatellite('calculator-cta-interaction-NLI',{componentName:componentName,userId:userId,calculatorName:calculatorName,ctaText:ctaText})

    }catch(er){}
}

//this function will call when user click on Open NPS Proceed button   
function openNPSProceed(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='open-nps-proceed',
        callSatellite('open-nps-proceed')
    }catch(er){}
}

//this function will call when user click on how do you like contribute   
function likeToContributeClick(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"", 
        digitalData.event.eventName='how-do-contribute-cta',
        callSatellite('how-do-contribute-cta')
    }catch(er){}
}

//this function will fire when user clicks on proceed button  
function proceedbuttonClick(mobileNo){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.consumer=digitalData.consumer||{},
        //digitalData.consumer.mobileNo=mobileNo==undefined?"": window.btoa(mobileNo),
        digitalData.event.eventContext.mobileNo,//changed according to digital data variable
        digitalData.event.eventName='proceed-button-click',
        callSatellite('proceed-button-click')
    }catch(er){}
}


//this function will fire when user click on continue button after entering otp 
function otpcontinueClick(mobileNo,userId,otpStatus){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.consumer=digitalData.consumer||{},
        digitalData.consumer.mobileNo=mobileNo==undefined?"": window.btoa(mobileNo),
	digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.otpStatus=otpStatus||"",
        digitalData.event.eventName='otp-continue-click',
        callSatellite('otp-continue-click')
    }catch(er){}
}

//this function will fire when user click on login after entering mpin
function mpinSubmit(userId,mpinStatus){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.consumer=digitalData.consumer||{},
        digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.mpinStatus=mpinStatus||"",
        digitalData.event.eventName='mpin-login-click',
        callSatellite('mpin-login-click')
    }catch(er){}
}


//this function will fire when user click on login after entering mpin
function mpinloginClick(userId,mpinStatus){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.consumer=digitalData.consumer||{},
        digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.mpinStatus=mpinStatus||"",
        digitalData.event.eventName='mpin-login-click',
        callSatellite('mpin-login-click')
    }catch(er){}
}

//this function will fire when user click on proceed to pay
function proceedtoPay(fundType,fundName,fundRiskCat,userId,investmentOption,amount){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
	digitalData.consumer.userId=userId||"",	
	digitalData.event.eventContext.investmentOption=investmentOption||"",           //pass SIP or one time
	digitalData.event.eventContext.amount=amount||"",	
	digitalData.event.eventName='proceed-to-pay',
        callSatellite('proceed-to-pay')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on proceed to pay
function setupautopayClick(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
	digitalData.consumer.userId=userId||"",		
	digitalData.event.eventName='setup-autopay-click',
        callSatellite('setup-autopay-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on KYC
function kycInteraction(userId,ctaText,stepName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.ctaText=ctaText||"",
	digitalData.event.eventContext.stepName=stepName||"",		
	digitalData.event.eventName='kyc-interaction',
        callSatellite('kyc-interaction')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on bank detail verify
function bankdetailVerify(userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventName='bank-details-verify',
	callSatellite('bank-details-verify')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on start goal
function startgoalClick(userId,goalName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.goalName=goalName||"",
	digitalData.event.eventName='start-goal-click',
	callSatellite('start-goal-click')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on remove from watchlist
function removefromwatchList(fundType,fundName,fundRiskCat,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.fundType=fundType||"",
        digitalData.event.eventContext.fundName=fundName||"",
        digitalData.event.eventContext.fundRiskCat=fundRiskCat||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventName='remove-from-watchlist',
        callSatellite('remove-from-watchlist')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on continue with google on registration page
function continuewithGoogle(userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventName='continue-with-google',
	callSatellite('continue-with-google')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on continue after filling the details registration page
function continuewithformDetails(userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventName='continue-with-form-details',
	callSatellite('continue-with-form-details')
    }
    catch(err){console.log(err)}
}

//this function will fire after signup
function signupSuccess(userId,signupSource){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventContext.signupSource=signupSource||"",     //pass continue with google or continue with form details in signup source
	digitalData.event.eventName='signup-success',
	callSatellite('signup-success')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on add nominee
function addNominee(userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventName='add-nominee',
	callSatellite('add-nominee')
    }
    catch(err){console.log(err)}
}

//this function will fire when user click on add bank
function addBank(userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
	digitalData.consumer=digitalData.consumer||{},     
       	digitalData.consumer.userId=userId||"",
	digitalData.event.eventName='add-bank',
	callSatellite('add-bank')
    }
    catch(err){console.log(err)}
}

//this function will call when user selects the no of years  
function selectYears(userId,ctaText,componentName){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{}, 
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.consumer.userId=userId||"",
        digitalData.event.eventContext.ctaText=ctaText||"",     //pass selected no of years in cta Text
        digitalData.event.eventName='select-years',
        callSatellite('select-years')
    }catch(er){}
}

//this function will fire when user click on any component on fundmanager
function fundmanagerClick(fundmanagerId, noofFunds, fundSize,highestReturn, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.noofFunds=noofFunds||"",
        digitalData.event.eventContext.fundSize=fundSize||"",
        digitalData.event.eventContext.highestReturn=highestReturn||"",
        digitalData.consumer.userId=userId||"",
	digitalData.consumer.fundmanagerId=fundmanagerId||"",
        digitalData.event.eventName="fund manager click",
        callSatellite('moneyfy-fund-manager-click')
    }
    catch(err){console.log(err)}
}

//***************************learn center********************************************

//when user click on share icon on any article on learn center
function shareInitiate(sectionName,componentName,ctaTitle,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="share initiate",
        callSatellite('share-initiate')
    }
    catch(err){console.log(err)}
}

//when user click on any icon after clicking on share icon
function shareiconClick(sectionName,componentName,ctaTitle,popupType,shareOption, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.popupType=popupType||"",
        digitalData.event.eventContext.shareOption=shareOption||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="share icon click",
        callSatellite('share-icon-click')
    }
    catch(err){console.log(err)}
}

//when user click on exit button on popup
function popupexitClick(sectionName,componentName,ctaTitle,popupType,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.popupType=popupType||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="popup exit click",
        callSatellite('popup-exit-click')
    }
    catch(err){console.log(err)}
}

//when user play video on clicking any article
function playvideoInitiate(sectionName,componentName,ctaTitle,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="play video initiate",
        callSatellite('play-video-initiate')
    }
    catch(err){console.log(err)}
}

//when user play the audio on podcast section​
function playaudioPodcast(sectionName, componentName, ctaTitle, userId) {
    try {
        digitalData.event.eventContext = digitalData.event.eventContext || {},
        digitalData.event.eventContext.sectionName = sectionName || "",
        digitalData.event.eventContext.componentName = componentName || "",
        digitalData.event.eventContext.ctaTitle = ctaTitle || "",
        digitalData.consumer.userId = userId || "",
        digitalData.event.eventName = "play audio podcast",
        callSatellite('play-audio-podcast')
    }
    catch (err) { console.log(err) }
}

//when user click on share icon on video popup
function sharevideoInitiate(sectionName,componentName,ctaTitle,popupType,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.popupType=popupType||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="share video initiate",
        callSatellite('share-video-initiate')
    }
    catch(err){console.log(err)}
}

//when user initiate register now on webinar
function registrationstepOne(sectionName,componentName,ctaTitle,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",        
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="registration step one",
        callSatellite('registration-step-one')
    }
    catch(err){console.log(err)}
}

//when user click on register now button after entering the details
function registernowClick(sectionName,componentName,ctaTitle,ctaText,mobileNo,emailID,userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",        
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.mobileNo=mobileNo==undefined?"": window.btoa(mobileNo),
        digitalData.consumer.emailID=emailID==undefined?"": window.btoa(emailID),
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="register now click",
        callSatellite('register-now-click')
    }
    catch(err){console.log(err)}
}

//when user select any cta on sort by input field
function selectSortby(sectionName,componentName,ctaTitle,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="select sortby",
        callSatellite('select-sortby')
    }
    catch(err){console.log(err)}
}

//when user click on read more on blogs
function blogreadmoreClick(sectionName,componentName,ctaTitle,ctaText, userId){
    try{
        digitalData.event.eventContext=digitalData.event.eventContext||{},
        digitalData.event.eventContext.sectionName=sectionName||"",
        digitalData.event.eventContext.componentName=componentName||"",
        digitalData.event.eventContext.ctaTitle=ctaTitle||"",
        digitalData.event.eventContext.ctaText=ctaText||"",
        digitalData.consumer.userId=userId||"",
	    digitalData.event.eventName="blog read more click",
        callSatellite('blog-read-more-click')
    }
    catch(err){console.log(err)}
}


//common function for _satellite. eventData is optional for this function we will use this if required later.
function callSatellite(eventName,eventData){
    try{
        _satellite.track(eventName,eventData)
    }catch(er){console.log(er)}
}
// _satellite.track('listing-next-page')
// _satellite.track('listing-prev-page')