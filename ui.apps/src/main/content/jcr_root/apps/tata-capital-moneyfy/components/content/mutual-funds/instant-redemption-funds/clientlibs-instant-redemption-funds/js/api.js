/*Fund Listing Filter  APi.js */
(function(_global) {
    var fundListAPiCallFn = (function(jsHelper) {
        var fundListfilterObj = {};

        var compareSearch = function(requestObj) {
            return new Promise(function(resolve) {
                apiUtility.getSearchApi(requestObj).then(function(response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function(error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }

        var fundListFilter = function(requestObj) {
            return new Promise(function(resolve) {
                apiUtility.getFundsViaFilter(requestObj).then(function(response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function(error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        
        /* Add To WatchList Api calling */
        var addTowatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.addToWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

        /* Fetch from WatchList Api calling */
        var fetchwatchListApi = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromWatchList(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch from Portfolio Api calling */
         var fetchFromPortfolio = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }

         /* Fetch Watchlist/Portfolio funds data api calling */
         var getFundsData = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.fetchFundsData(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        fundListfilterObj.fetchFundsData = getFundsData;
        fundListfilterObj.fetchFromPortfolio = fetchFromPortfolio;
        fundListfilterObj.fetchwatchListApi = fetchwatchListApi;
        fundListfilterObj.addTowatchListApi = addTowatchListApi;
        fundListfilterObj.compareSearch = compareSearch;
        fundListfilterObj.fundListFilter = fundListFilter;

        return jsHelper.freezeObj(fundListfilterObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundListfilterObj', fundListAPiCallFn);
})(this || window || {});
/* Fund Listing Filter APi.js end*/
