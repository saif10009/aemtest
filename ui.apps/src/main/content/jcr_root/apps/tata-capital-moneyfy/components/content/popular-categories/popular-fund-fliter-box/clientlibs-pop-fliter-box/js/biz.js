/*popular categories Filter search js start*/
(function (_global) {
    var popularfilterBizFnObj = (function (jsHelper) {
        var renderArray = [];
        var numberOfFundsPerPage = 8;
        var totalFunds;
        var searchmutualFundList = [];
        var schemeIdMapObj = {};
        var watchlistFunds;
        var compareFundArray;
        var portfolioFundIds = [];
        var popularfilterBizObj = {}
        var addFromType = '';
        var checkedFund = [];
        var sortBy = "returns - high to low";
        var sortByObj = {
            "morning star - high to low": { "apiKey": "morningStarSort", "type": "DESC" },
            "value research - high to low": { "apiKey": "valueResearchSort", "type": "DESC" },
            "returns - high to low": { "apiKey": "tenureSort", "type": "DESC" },
            "morning star - low to high": { "apiKey": "morningStarSort", "type": "ASC" },
            "value research - low to high": { "apiKey": "valueResearchSort", "type": "ASC" },
            "returns - low to high": { "apiKey": "tenureSort", "type": "ASC" },
        }
        var selectedFundObj = {};
        var compareWrap = $('.compare-choose-wrap');
        if ($(window).width() < 991) {
            var compareWrap = $('.mob-filter-container .compare-choose-wrap');
        }
        document.addEventListener('DOMContentLoaded', function () {
            renderFundListData();
            popFilterClick();
            clickEvents();
            compareFunds();
            compareSearchTypeahead();

            // Sortby for mobile
            $('.jsMobSorting').click(function () {
                $('.jsSelectSortList li input').prop('checked', false);
                $(this).parents('body').append('<div class="sorting-backdrop"></div>')
                $(this).parents('body').addClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').addClass('opened-sort')
            })

            /* Number of funds to be shown */
            document.querySelector("#noOfFunds").addEventListener("click", function (event) {
                $('.js-loader').addClass('show');
                var perPage = event.target.innerText.split(' ');
                perPage = Number(perPage[0]);
                numberOfFundsPerPage = perPage;
                renderFundListData(0, numberOfFundsPerPage, sortBy);
                // popularCategoryFundRenderObj.renderFilteredFunds(renderArray);
                // fundFilterBoxPagination(perPage);
                setTimeout(function () {
                    $('.js-loader').removeClass('show');
                }, 1700);

                $('.single-select2').select2({
                    minimumResultsForSearch: -1
                });
                investNow();
                // add to watchList
                $('[data-addwatchlist]').click(function () {
                    var schemeId = $(this).parents('.fund-list-li').data('schemeid');
                    var schemeName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                    var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                    var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                    addToWishList(fundType, schemeName, fundRiskCat, userIdObj.userId)
                    if (!$(this).hasClass('addedWatchlist')) {
                        addWatchListBizFn(schemeId, schemeName);
                    } else {
                        showWatchListExistPopup();
                    }
                });

                $('.js-viewMoreToggleBtn').click(function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    $(this).toggleClass('active');
                    $(this).find('.text-wrap').text(function (i, text) {
                        return text === "More" ? "Less" : "More";
                    });
                    var toggleDiv = $(this).attr('data-btn');
                    $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
                });

                if ($(window).width() < 768) {
                    $('.js-similarMFcard').click(function (e) {
                        $(this).find('.tag-more-wrap .js-viewMoreToggleBtn[data-btn]').trigger('click');
                    });
                    $(".js-similarMFcard .form-select2, .js-similarMFcard .watchlist-compare-btn, .js-similarMFcard .invest-detail-wrap").click(function (e) {
                        e.stopPropagation();
                    });
                }
                addCompare();
                showReturns();
                adobeAnalytics();
                addEllipseToFundName();
            });

            document.querySelector('[data-watchlist="portfolio-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromPortfolio').classList.remove('popover-show');
                document.getElementById('addFromPortfolio').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetWatchlist + '"] [data-fetchwatchlist="watchListBtn"]').click();
            });

            document.querySelector('[data-portfolio="watchlist-popover"]').addEventListener('click', function (event) {
                document.getElementById('addFromWishList').classList.remove('popover-show');
                document.getElementById('addFromWishList').removeAttribute('style');
                $('body').removeClass('popover-modal-open');
                document.querySelector('[data-compareselect="' + event.target.dataset.targetPortfolio + '"] [data-fetchportfolio="portfolioBtn"]').click();
            });

        });

        function showFunds(renderArray) {
            document.querySelector('#noOfFunds').innerHTML = '';
            var showFunds = [8, 10, 15, 20];
            if (Number(renderArray) <= 8) {
                document.querySelector('.fund-list-result-two').classList.add('d-none');
            } else {
                document.querySelector('.fund-list-result-two').classList.remove('d-none');
            }
            showFunds.forEach(function (showFundNumber) {
                if (renderArray >= showFundNumber) {
                    document.querySelector('#noOfFunds').innerHTML += '<li class="' + (showFundNumber == numberOfFundsPerPage ? "active" : "") + '">'
                        + '<a href="javascript:void(0)">' + showFundNumber + ' Funds</a>'
                        + '</li>'
                }
            });
        }

        function adobeAnalytics() {
            $('[data-viewdetailbtn]').click(function (event) {
                event.preventDefault();
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                fundViewDetails(fundType, fundName, fundRiskCat, userIdObj.userId)
                window.open($(this).attr('href'))
            })
            $('.pagination-list .next').click(function (event) {
                event.preventDefault();
                _satellite.track('listing-next-page');
                location.href = $(this).attr('href')
            })
            $('.pagination-list .prev').click(function (event) {
                event.preventDefault();
                _satellite.track('listing-next-page');
                location.href = $(this).attr('href')
            })
        }

        function getUserWatchList() {
            if (headerBizObj.getUserObj().isLoggedIn) {
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "tenure": "3y"
                    }
                }
                popularCategoriesfilterObj.fetchwatchListApi(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        var watchListArray = responseData.responseJson.body.watchListDetails;
                        watchListArray.forEach(function (fund) {
                            watchListSchemeIds.push(fund.schemeId);
                        });
                        //filterFunds('returns - high to low');
                    }
                    popularCategoryFundRenderObj.renderFilteredFunds(renderArray);
                    document.querySelector('#fundListSize').innerHTML = totalFunds;
                    initializeEvents();
                }).catch(function (error) {
                    console.log(error);
                });
            }
        }

        function addWatchListBizFn(schemeId, schemeName) {
            if (!headerBizObj.getUserObj().isLoggedIn) {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain + '?action=watchlist&schemeId=' + schemeId + '&source=' + window.location.href
                });
            } else {
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy",
                        "schemeId": schemeId,
                    }
                };
                popularCategoriesfilterObj.addTowatchListApi(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        if (responseData.responseJson.body.status.statusMessage == 'Fund added to Watchlist') {
                            if ($('.watchListError').hasClass('d-none')) {
                                $('.watchListError').removeClass('d-none');
                            }
                            watchListSchemeIds.push(schemeId);
                            document.getElementById('addWatchlistMsg').innerHTML = 'Fund added to Watchlist';
                            showAddWatchListPopup();
                            document.querySelector('[data-schemeid="' + schemeId + '"]').querySelectorAll('.js-addWatchlist').forEach(function (element) {
                                element.classList.add('addedWatchlist');
                                $('[data-schemeid="' + schemeId + '"] .wishlist-text').html('Added To Watchlist');
                            });
                        }
                    } else {
                        showWatchListErrorPopup();
                    }
                }).catch(function (error) {
                    console.log(error);
                    showWatchListErrorPopup();
                });
            }
        }

        function investNow() {
            /* Invest Now click button */
            $('[data-investnow="true"]').click(function () {
                var schemaId = $(this).parents('.fund-list-li').data('schemeid');
                var source = window.location.href;
                var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                fundInvestNow(fundType, fundName, fundRiskCat, userIdObj.userId);
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                    });
                } else {
                    location.href = appConfig.jocataDomain + '?action=invest' + '&schemeId=' + schemaId + '&source=' + source;
                }
            });
        }

        function compareSearchTypeahead() {
            /* compare fund search typeahead js */
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substrRegex;
                    matches = [];
                    substrRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substrRegex.test(str)) {
                            matches.push(str);
                        }
                    });
                    cb(matches);
                };

            };
            /* mutual fund search modal */

            $('#jsMutualFundSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            }, {
                name: 'searchmutualFundList',
                source: substringMatcher(searchmutualFundList)
            });

            // watchList & portFolio popup search
            $("[data-search='watchlist-portfolio']").on('keyup', function () {
                var mCustomScrollType = addFromType == 'watchList' ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
                $(mCustomScrollType).mCustomScrollbar("destroy");
                var searchVal = $(this).val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    popularCategoryFundRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    popularCategoryFundRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                    if ($(window).width() > 767) {
                        $(mCustomScrollType).mCustomScrollbar({
                            axis: "y",
                            mouseWheel: {
                                enable: true,
                            },
                            documentTouchScroll: false,
                            scrollButtons: {
                                enable: true,
                                scrollAmount: 320,
                                scrollType: "stepped",
                            },
                        });
                    } else {
                        $(mCustomScrollType).mCustomScrollbar("destroy");
                    }
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });
        }

        function popFilterClick() {

            /* Sorting code for Sort-by */
            if (window.outerWidth <= 991) {
                $("input[name='sort']").click(function () {
                    var sortByVal = $("input[name='sort']:checked").val();
                    console.log(sortBy);
                    sortBy = sortByVal.toLowerCase();
                    renderFundListData(0, numberOfFundsPerPage, sortBy);
                });
            } else {
                document.querySelector('.getSortBy').addEventListener('click', function (event) {
                    var sortByVal = event.target.innerText.toLowerCase();
                    console.log(sortBy);
                    sortBy = sortByVal.toLowerCase();
                    renderFundListData(0, numberOfFundsPerPage, sortBy);
                });
            }

            // document click js
            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp();
                    $('.js-filterBtn').removeClass('active');
                }
            });

            // dropdown js
            $('.js-filterBtn').click(function () {
                $('.js-filterBtn').not(this).removeClass('active');
                $(this).toggleClass('active');
                $('[data-filter-card]').slideUp();

                var filterCard = $(this).attr('data-filter');
                var toggleFilterCard = $(this).parents('.custom-dropdown').find('[data-filter-card="' + filterCard + '"]');
                toggleFilterCard.is(':visible') ? toggleFilterCard.slideUp('fast') : toggleFilterCard.slideDown('fast');
            });


        }

        function popFilterCustomScroll() {
            $('.mCustomScroll').mCustomScrollbar({});

            $('.filterTags-mCustomScrollbar').mCustomScrollbar({
                axis: "x",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });

            $('.filterList-mCustomScrollbar').mCustomScrollbar({
                axis: "y",
                mouseWheel: {
                    enable: true,
                },
                documentTouchScroll: false,
                scrollButtons: {
                    enable: true,
                    scrollAmount: 320,
                    scrollType: "stepped",
                },
            });

            $('.jsDropDownMenuGet li a').click(function () {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');



                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });
            $('.single-select2').select2({
                minimumResultsForSearch: -1
            });


        }

        function searchApi(requestData) {
            let requestObj = {
                "body": {}
            };
            requestObj.body.searchTerm = requestData;
            popularCategoriesfilterObj.fundSearchApi(requestObj).then(function (response) {
                if (response.status.toLowerCase() == 'success') {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    responseData = responseData.responseJson.results;
                    if (responseData != undefined) {
                        searchmutualFundList.length = 0;
                        schemeIdMapObj = {};
                        responseData.forEach(function (fundObj) {
                            searchmutualFundList.push(fundObj.pageTitle);
                            schemeIdMapObj[fundObj.pageTitle] = fundObj.schemeId;
                        });
                    }
                }
            }).catch(function (error) {
                console.log(error);
            });

        }

        /* Mobile View show returns value based on dropdown */
        function showReturns() {
            $('.returnsVal').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info up-trend');
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span>' + value + '%');
                } else if (Number.isNaN(Number(value))) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass();
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').removeClass().addClass('trend-info down-trend');
                    $(this).parents('.name-return-wrap').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span>' + value + '%');
                }
            });
        }

        /* sort funds based on sortby value */
        function initializeEvents() {
            $('.single-select2').select2({
                minimumResultsForSearch: -1
            });
            investNow();

            addCompare();
            showReturns();
            formSelectInitialize();
            adobeAnalytics();
            addEllipseToFundName();

            $('.js-viewMoreToggleBtn').click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).toggleClass('active');
                $(this).find('.text-wrap').text(function (i, text) {
                    return text === "More" ? "Less" : "More";
                });
                var toggleDiv = $(this).attr('data-btn');
                $(this).parents('.similar-mutual-funds-items').find('[data-toggleCard="' + toggleDiv + '"]').slideToggle();
            });

            if ($(window).width() < 768) {
                $('.js-similarMFcard').click(function (e) {
                    $(this).find('.tag-more-wrap .js-viewMoreToggleBtn[data-btn]').trigger('click');
                });
                $(".js-similarMFcard .form-select2, .js-similarMFcard .watchlist-compare-btn, .js-similarMFcard .invest-detail-wrap").click(function (e) {
                    e.stopPropagation();
                });
            }

            // add to watchList
            $('[data-addwatchlist]').click(function () {
                var schemeId = $(this).parents('.fund-list-li').data('schemeid');
                var schemeName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                addToWishList(fundType, schemeName, fundRiskCat, userIdObj.userId)
                if (!$(this).hasClass('addedWatchlist')) {
                    addWatchListBizFn(schemeId, schemeName);
                } else {
                    showWatchListExistPopup();
                }
            });
        }

        function formSelectInitialize() {
            $('.form-select2').click(function () {
                if ($(this).parents('body').hasClass('popover-modal-open')) {
                    $('.select2-dropdown').attr('style', 'z-index: 1051 !important; width: 100px;');
                    console.log($('.select2-container--open').length);
                    if ($('.select2-container--open').length !== 0) {
                        $('.wishlist-fund-list').addClass('mob-wishlistmodal-scroll');

                    } else {
                        $('.wishlist-fund-list').removeClass('mob-wishlistmodal-scroll');
                    }
                }
            });
        }

        function clickEvents() {
            // addFromWatchList & addFromPortfolio
            document.querySelectorAll('[data-fetchwatchlist="watchListBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-box-item').data('compareselect');
                    if (headerBizObj.getUserObj().isLoggedIn) {
                        if (watchListSchemeIds.length > 0) {
                            if (!document.querySelector('#addFromWishList [data-watchlist="false"]').classList.contains('d-none')) {
                                document.querySelector('#addFromWishList [data-watchlist="false"]').classList.add('d-none');
                            }
                            if (document.querySelectorAll('#fetchFromWatchlist .fund-list-li').length > 0) {
                                $('#fetchFromWatchlist .fund-list-li').empty();
                            }
                            addFromType = 'watchList';
                            getFundsData();
                        } else {
                            if (!document.querySelector('#addFromWishList [data-watchlist="true"]').classList.contains('d-none')) {
                                document.querySelector('#addFromWishList [data-watchlist="true"]').classList.add('d-none');
                            }
                            setTargetForSearch(targetModel);
                            fetchWatchListPopup();
                        }
                    } else {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = appConfig.jocataDomain;
                        });
                    }
                });
            });

            document.querySelectorAll('[data-fetchportfolio="portfolioBtn"]').forEach(function (element) {
                element.addEventListener('click', function () {
                    var targetModel = $(this).parents('.compare-box-item').data('compareselect');
                    getPortfolio(targetModel);
                });
            });

            $('.jsSelectSortList li input, .jsSortByClose').on('click', function () {
                $(this).parents('body').find('.sorting-backdrop').remove();
                $(this).parents('body').removeClass('tb-overflow-hidden');
                $(this).parents('body').find('.mob-sortby-box').removeClass('opened-sort')


            });

            $('.jsModalOverflowHide').click(function () {
                $('body').addClass('overflow-hidden');
            });

            $('.jsDropDownMenuGet li a').click(function () {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');
                $('[data-filter-card]').slideUp();
                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });

            $(document).mouseup(function (e) {
                var container = $('.custom-dropdown');
                if (!container.is(e.target) && container.has(e.target).length === 0) {
                    $('[data-filter-card]').slideUp();
                    $('.js-filterBtn').removeClass('active');
                }
            });

            /*modal js*/
            $('[data-popovermodal="compare-popover"]').click(function () {
                var ele_target = $(this).attr('data-target');
                setTimeout(function () {
                    $(ele_target).addClass('popover-show');
                }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');
                $(this).hasClass('jsWhiteBackdrop') ? $('.modal-backdrop').addClass('white-backdrop') : $('.modal-backdrop').removeClass('white-backdrop');
            });

            $('[data-dismiss="popover-modal"]').on('click', function () {
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                if ($('body').hasClass('overflow-hidden')) {
                    $('body').removeClass('overflow-hidden');
                }
                $('.modal-backdrop').remove();
            });
            /*modal js*/
        }

        function showFiterBox() {
            $('.js-filtersBox').removeClass('d-none');
            $('.js-compareBox').addClass('d-none');
            $('.js-filterWrap').removeClass('show-compare');
            $('.js-compareActive').removeClass('has-compare');
            $('.js-compareActiveNone').removeClass('has-opacity');
        };

        function showPopupReturns() {
            $('.portfolioWatchListReturns').on('change', function (e) {
                var value = $(this).val();
                if (Number.isNaN(value) == false && Number(value) > 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info up-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-up"></span> ' + value + '% ');
                } else if (Number.isNaN(Number(value))) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass();
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('- %');
                } else if (Number.isNaN(value) == false && Number(value) <= 0) {
                    $(this).parents('.return-info').find('[data-rate="rate"]').removeClass().addClass('text-right trend-info down-trend');
                    $(this).parents('.return-info').find('[data-rate="rate"]').html('<span class="icon-angle-down"></span> ' + value + '% ');
                }
            });
        }

        function removeWishlistModal() {
            $('#wishlist-modal').removeClass('popover-show').hide();
            $('body').removeClass('overflow-hidden popover-modal-open');
            $('.modal-backdrop').remove();
        }

        function checkFundsAddedToCompare() {
            compareFundArray = Object.values(selectedFundObj);
            var selector = addFromType == 'watchList' ? 'fetchFromWatchlist' : 'fetchFromPortfolio';
            document.getElementById(selector).querySelectorAll('[data-schemeid]').forEach(function (fund) {
                if (compareFundArray.indexOf(Number(fund.dataset.schemeid)) != -1) {
                    $('[data-schemeid="' + fund.dataset.schemeid + '"] .js-addCompare').find('input').prop("checked", true);
                } else {
                    if ($(window).width() >= 767) {
                        if (compareFundArray.length >= 3) {
                            $('#' + selector + ' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    } else {
                        if (compareFundArray.length >= 2) {
                            $('#' + selector + ' [data-schemeid="' + fund.dataset.schemeid + '"] .similar-mutual-funds-items').addClass('disable-fund-card');
                        }
                    }
                }
            });
        }

        function getFundsData() {
            var requestBody = {
                "body": {
                    "schemeIds": addFromType == 'watchList' ? watchListSchemeIds : portfolioFundIds,
                }
            };

            popularCategoriesfilterObj.fetchFundsData(requestBody).then(function (response) {
                var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                responseData = convertCagrArrayToObject(responseData.responseJson.watchlistDetails);
                popularCategoryFundRenderObj.renderWatchListPortfolioFunds(responseData, addFromType);
                checkFundsAddedToCompare();
                watchlistFunds = responseData;
                var mCustomScrollType = addFromType == 'watchList' ? '#fetchFromWatchlist' : '#fetchFromPortfolio';

                $('.js-modalClose').click(function () {
                    var popupType = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
                    document.querySelector(popupType + ' [data-search="watchlist-portfolio"]').value = '';
                    $('body').removeClass('overflow-hidden');
                    $(mCustomScrollType).mCustomScrollbar("destroy");
                    $('#jsSearchTypehead .typeahead').typeahead('destroy');
                });

                if ($(window).width() > 767) {
                    $(mCustomScrollType).mCustomScrollbar({
                        axis: "y",
                        mouseWheel: {
                            enable: true,
                        },
                        documentTouchScroll: false,
                        scrollButtons: {
                            enable: true,
                            scrollAmount: 320,
                            scrollType: "stepped",
                        },
                    });
                } else {
                    $(mCustomScrollType).mCustomScrollbar("destroy");
                }

                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                searchWatchListFund();
                if (addFromType == 'watchList') {
                    fetchWatchListPopup();
                } else {
                    fetchPortfolioPopup();
                }
                popupCompareEvents();
            }).catch(function (error) {
                console.log(error);
            });
        }

        function getPortfolio(targetModel) {
            if (headerBizObj.getUserObj().isLoggedIn) {
                $('.js-loader').addClass('show');
                var userData = headerBizObj.getUserObj().userData;
                var requestObj = {
                    "body": {
                        "mobileNumber": userData.mobileNumber,
                        "tatId": userData.tatId,
                        "product": "moneyfy"
                    }
                }
                popularCategoriesfilterObj.fetchFromPortfolio(requestObj).then(function (response) {
                    var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                    if (responseData.responseJson.header.status.toLowerCase() == 'success') {
                        if (responseData.responseJson.body.mutualFundList != undefined) {
                            var portfolioFundsArray = responseData.responseJson.body.mutualFundList;
                            portfolioFundIds.length = 0;
                            portfolioFundsArray.forEach(function (fund) {
                                portfolioFundIds.push(fund.schemeId);
                            });
                        }
                    }
                    if (portfolioFundIds.length > 0) {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="false"]').classList.add('d-none');
                        }
                        addFromType = 'portfolio';
                        getFundsData();
                    } else {
                        if (!document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.contains('d-none')) {
                            document.querySelector('#addFromPortfolio [data-portfolio="true"]').classList.add('d-none');
                        }
                        setTargetForSearch(targetModel);
                        fetchPortfolioPopup();
                    }
                    $('.js-loader').removeClass('show');
                }).catch(function (error) {
                    console.log(error);
                    $('.js-loader').removeClass('show');
                });
            } else {
                showLoginPopup();
                document.querySelector('[data-login="true"]').addEventListener('click', function () {
                    location.href = appConfig.jocataDomain;
                });
            }
        }

        function setTargetForSearch(targetModel) {
            document.querySelector('[data-portfolio="watchlist-popover"]').dataset['targetPortfolio'] = targetModel;
            document.querySelector('[data-search="watchlist-popover"]').dataset['targetSearch'] = targetModel;
            document.querySelector('[data-watchlist="portfolio-popover"]').dataset['targetWatchlist'] = targetModel;
            document.querySelector('[data-search="portfolio-popover"]').dataset['targetSearch'] = targetModel;
        }

        function setSessionSchemeId(selectedFundObj) {
            console.log(selectedFundObj);
            var data = Object.keys(selectedFundObj).map(function (value) {
                return selectedFundObj[value].toString();
            });
            sessionStorage.setItem('moneyfyCompareFunds', JSON.stringify(data));
        }

        function addCompare() {
            // add compare check js
            $('.js-addCompare').change(function () {

                var fundName = $(this).parents('.similar-mutual-funds-items').find('.fund-name').data('schemename');
                var schemeID = $(this).parents('.fund-list-li').data('schemeid');
                var fundLength = 3;
                if (window.outerWidth < 768) {
                    document.getElementById('compareCheck').innerHTML = "Compare Upto 2 Funds";
                    $('[data-select-modal="searchFund-modal-3"]').addClass('d-none');
                } else {
                    document.getElementById('compareCheck').innerHTML = "Compare Upto 3 Funds"
                }

                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                if (Object.values(selectedFundObj).length > fundLength) {
                    $(this).find('input').prop("checked", false);
                } else {
                    if ($(this).find('input').is(':checked')) {
                        checkedFund.push(fundName);
                        var fundName = $(this).parents('.fund-list-li').find('h6').data('schemename');
                        var fundRiskCat = $(this).parents('.fund-list-li').find('[data-risktype]').data('risktype');
                        var fundType = $(this).parents('.fund-list-li').find('[data-catname]').data('catname');
                        compareFund(fundType, fundName, fundRiskCat, userIdObj.userId)
                        if (checkedFund.length > fundLength) {
                            $(this).find('input').prop("checked", false);
                            checkedFund.pop(fundName);
                        }
                        if (Object.values(selectedFundObj).length < fundLength) {
                            selectedFundObj[fundName] = schemeID;
                        }
                    } else {
                        delete selectedFundObj[fundName]
                        checkedFund = jQuery.grep(checkedFund, function (value) {
                            return value != fundName;
                        });
                    }
                    compareWrap.each(function (i) {
                        $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);
                        if (checkedFund[i] != undefined) {
                            $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                            $(this).find('.js-chooseCompareBtn').addClass('d-none');
                        } else {
                            $(this).find('.js-selectedCompareBtn').addClass('d-none');
                            $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                        }
                    });
                }


                if (checkedFund.length >= 1) {
                    $('.js-filtersBox').addClass('d-none');
                    $('.js-compareBox').removeClass('d-none');
                    $('.js-filterWrap').addClass('show-compare');
                    $('.js-compareActive').addClass('has-compare');
                    $('.js-compareActiveNone').addClass('has-opacity');
                } else {
                    showFiterBox();
                    // 07-03-2022 start
                    removeWishlistModal();
                    // 07-03-2022 end
                }

                // 07-03-2022 start
                $('.js-fundCount').text(checkedFund.length);
                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                // 07-03-2022 end
                setSessionSchemeId(selectedFundObj);
            });
        }

        function compareFunds() {
            // remove from compare js
            $('.js-removeCompare').click(function () {
                var selectedFundName = $(this).parents('.js-selectedCompareBtn').find('.text-wrap').text();

                $('.similar-mutual-funds-items').each(function () {
                    if ($(this).find('.fund-name').text() === selectedFundName) {
                        $(this).find('.js-addCompare input').prop("checked", false);
                    }
                });
                delete selectedFundObj[selectedFundName];
                checkedFund = jQuery.grep(checkedFund, function (value) {
                    return value != selectedFundName;
                });
                compareWrap.each(function (i) {
                    $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);

                    if (checkedFund[i] != undefined) {
                        $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                        $(this).find('.js-chooseCompareBtn').addClass('d-none');
                    } else {
                        $(this).find('.js-selectedCompareBtn').addClass('d-none');
                        $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                    }
                });

                if (checkedFund.length < 1) {
                    showFiterBox();
                    removeWishlistModal();
                }

                var fundLength = 3;
                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                $('.js-fundCount').text(checkedFund.length);

                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });

            // cancel compare js
            $('.js-cancelCompare').click(function () {
                checkedFund = [];
                selectedFundObj = {};
                $('.js-addCompare input').prop("checked", false);

                $('.js-selectedCompareBtn').addClass('d-none');
                $('.js-chooseCompareBtn').removeClass('d-none');

                showFiterBox();
                $('#wishlist-modal').removeClass('popover-show').hide();
                $('body').removeClass('overflow-hidden popover-modal-open');
                $('.modal-backdrop').remove();
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });

            /* compare box js */
            $('[data-search="fund"]').on('input', function () {
                var searchVal = $(this).val();
                //console.log(searchVal);
                searchVal = searchVal.trim();
                if (searchVal.length >= 3) {
                    searchApi(searchVal);
                }
            });

            $('.js-chooseCompareBtn .jsOpenModal').click(function () {
                $('#jsMutualFundSearchTypehead input').val('');
            });

            // search select js
            $('.modal-search-wrap').on('click', '.tt-selectable', function () {
                var modalId = $(this).parents('.search-fund-modal').attr('id');
                var searchVal = $('#' + modalId + ' .tt-input').val();
                //console.log(schemeIdMapObj[searchVal]);
                //console.log(searchVal);
                selectedFundObj[searchVal] = schemeIdMapObj[searchVal];
                $('[data-schemeid="' + schemeIdMapObj[searchVal] + '"] .js-addCompare').find('input').prop("checked", true);
                //document.getElementById(modalId).setAttribute('data-selectedSchemeId',schemeIdMapObj[searchVal]);
                $('[data-select-modal="' + modalId + '"]').find('.js-chooseCompareBtn').addClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-selectedCompareBtn').removeClass('d-none');
                $('[data-select-modal="' + modalId + '"]').find('.js-selectedCompareBtn .text-wrap').text(searchVal);

                // close modal
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();

                checkedFund.push(searchVal);
                $('.js-fundCount').text(checkedFund.length);
                setSessionSchemeId(selectedFundObj);
                console.log('checkList', checkedFund);
            });
        }

        function popupCompareEvents() {
            // add compare check js
            var selector = addFromType == "watchList" ? '#fetchFromWatchlist' : '#fetchFromPortfolio';
            $(selector + ' .js-addCompare').change(function () {

                var fundName = $(this).parents('.similar-mutual-funds-items').find('.fund-name').data('schemename');
                var schemeID = $(this).parents('.fund-list-li').data('schemeid');
                var fundLength = 3;
                if ($(window).width() < 768) {
                    fundLength = 2;
                }

                if (Object.values(selectedFundObj).length > fundLength) {
                    $(this).find('input').prop("checked", false);
                } else {
                    if ($(this).find('input').is(':checked')) {
                        checkedFund.push(fundName);
                        if (checkedFund.length > fundLength) {
                            $(this).find('input').prop("checked", false);
                            checkedFund.pop(fundName);
                        }
                        if (Object.values(selectedFundObj).length < fundLength) {
                            selectedFundObj[fundName] = schemeID;
                        }
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", true)
                    } else {
                        delete selectedFundObj[fundName]
                        checkedFund = jQuery.grep(checkedFund, function (value) {
                            return value != fundName;
                        });
                        $('[data-schemeid="' + schemeID + '"] .js-addCompare').find('input').prop("checked", false);
                    }
                    compareWrap.each(function (i) {
                        $(this).find('.js-selectedCompareBtn .text-wrap').text(checkedFund[i]);
                        if (checkedFund[i] != undefined) {
                            $(this).find('.js-selectedCompareBtn').removeClass('d-none');
                            $(this).find('.js-chooseCompareBtn').addClass('d-none');
                        } else {
                            $(this).find('.js-selectedCompareBtn').addClass('d-none');
                            $(this).find('.js-chooseCompareBtn').removeClass('d-none');
                        }
                    });
                }


                if (checkedFund.length >= 1) {
                    $('.js-filtersBox').addClass('d-none');
                    $('.js-compareBox').removeClass('d-none');
                    $('.js-filterWrap').addClass('show-compare');
                    $('.js-compareActive').addClass('has-compare');
                    $('.js-compareActiveNone').addClass('has-opacity');
                } else {
                    showFiterBox();

                    // 07-03-2022 start
                    removeWishlistModal();
                    // 07-03-2022 end
                }

                // 07-03-2022 start
                $('.js-fundCount').text(checkedFund.length);

                if (checkedFund.length === fundLength) {
                    $('.wishlist-fund-list .similar-mutual-funds-items').addClass('disable-fund-card');
                    $('.js-addCompare input:checked').parents('.similar-mutual-funds-items').removeClass('disable-fund-card');
                } else {
                    $('.wishlist-fund-list .similar-mutual-funds-items').removeClass('disable-fund-card');
                }
                // 07-03-2022 end
                setSessionSchemeId(selectedFundObj);
            });
        }

        function searchWatchListFund() {
            var substringMatcher = function (strs) {
                return function findMatches(q, cb) {
                    var matches, substringRegex;
                    matches = [];
                    substringRegex = new RegExp(q, 'i');
                    $.each(strs, function (i, str) {
                        if (substringRegex.test(str)) {
                            matches.push(str);
                        }
                    });

                    cb(matches);
                };
            };

            var searchFund = watchlistFunds.map(function (fund) {
                return fund.schemeDetails.name;
            });
            var selector = addFromType == 'watchList' ? 'addFromWishList' : 'addFromPortfolio';
            $('#' + selector + ' #jsSearchTypehead .typeahead').typeahead({
                hint: false,
                highlight: true,
                minLength: 1
            }, {
                name: 'searchFund',
                source: substringMatcher(searchFund)
            });

            $('#' + selector + " .tt-menu").on('click', function () {
                var searchVal = $('#' + selector + ' [data-search="watchlist-portfolio"]').val();
                if (searchVal.length > 0) {
                    var searchArray = [];
                    watchlistFunds.forEach(function (fund) {
                        if (fund.schemeDetails.name.toLowerCase() == searchVal.toLowerCase()) {
                            searchArray.push(fund);
                        }
                    });
                    popularCategoryFundRenderObj.renderWatchListPortfolioFunds(searchArray, addFromType);
                } else {
                    popularCategoryFundRenderObj.renderWatchListPortfolioFunds(watchlistFunds, addFromType);
                }
                $('.single-select2').select2({ minimumResultsForSearch: -1 });
                showPopupReturns();
                formSelectInitialize();
                addEllipseToFundName();
                checkFundsAddedToCompare();
                popupCompareEvents();
            });
        }

        function fundFilterBoxPagination(perPage, totalFund, pageNumber) {
            var items = $(".jsPagination .fund-list-li");
            //var numItems = items.length;
            items.slice(perPage).hide();
            // 21-01-22
            if (!jsHelper.isDef(pageNumber, true)) {
                if ($(".mf-pagination").is(":visible")) {
                    $('.mf-pagination:visible').pagination({
                        items: Number(totalFund),
                        itemsOnPage: perPage,
                        displayedPages: 3,
                        edges: 1,
                        pageNumber: pageNumber ? pageNumber : 1,
                        prevText: "<i class='icon-arrow-left'></i><span>Previous</span>",
                        nextText: "<span>Next</span><i class='icon-arrow-right'></i>",
                        onPageClick: function (pageNumber) {
                            var rowNumber = ((pageNumber * perPage) - perPage);
                            renderFundListData(rowNumber, perPage, sortBy, pageNumber);
                        }
                    });
                }
            }
            // 21-01-22
        }

        function convertCagrArrayToObject(array) {
            if (array.length > 0) {
                array.forEach(function (element, index) {
                    var cagrObj = {};
                    element.cagrValues.forEach(function (cagr, index) {
                        cagrObj[cagr.tenure] = cagr.value;
                    });
                    element.cagrValues = cagrObj;
                });
                return array;
            } else {
                return [];
            }
        }

        function renderFundListData(rowNum, totalRows, sortBy, pageNumber) {
            $('.js-loader').addClass('show');
            var requestObj = {
                "body": {
                    "resultRange": {
                        rowNum: jsHelper.isDef(rowNum) ? rowNum : 0,
                        totalRows: jsHelper.isDef(totalRows, true) ? totalRows : 8
                    },
                    "themeName": popCatName,
                }
            };

            if (jsHelper.isDef(sortBy)) {
                requestObj["body"]["sortBy"] = {
                    [sortByObj[sortBy].apiKey]: sortByObj[sortBy].type
                };
            } else {
                requestObj["body"]["sortBy"] = {
                    "tenureSort": "DESC"
                };
            }

            popularCategoriesfilterObj.popularCategoryFundList(requestObj).then(function (response) {
                $('.js-loader').addClass('show');
                var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                if (response.status == "SUCCESS") {
                    renderArray = responseData.responseJson == undefined ? 0 : responseData.responseJson.themesDetails;
                    totalFunds = responseData.responseJson.resultSetCount;
                    if (renderArray.length > 0) {
                        showFunds(responseData.responseJson.resultSetCount);
                        renderArray = convertCagrArrayToObject(renderArray);
                        console.log('responseData', renderArray)
                        if (headerBizObj.getUserObj().isLoggedIn) {
                            getUserWatchList();
                        } else {
                            popularCategoryFundRenderObj.renderFilteredFunds(renderArray);
                            initializeEvents();
                        }
                        fundFilterBoxPagination(numberOfFundsPerPage, totalFunds, pageNumber);
                        document.querySelector('#fundListSize').innerHTML = totalFunds;
                    } else {
                        $('[data-id="fundDropdown"]').addClass('d-none');
                        popularCategoryFundRenderObj.renderFilteredFunds([]);
                        $('.js-loader').removeClass('show');
                    }
                    //addCompare();
                    popFilterCustomScroll();
                }
                $('.js-loader').removeClass('show');
            }).catch(function (error) {
                $('[data-id="fundDropdown"]').addClass('d-none');
                popularCategoryFundRenderObj.renderFilteredFunds([]);
                $('.js-loader').removeClass('show');
                console.log(error);
            });
        }

        function addEllipseToFundName() {
            /*2 line Dot in mutual fund strip*/
            if ($(window).width() > 767) {
                var showChar = 45;
            }
            else if ($(window).width() > 374) {
                var showChar = 32;
            }
            else {
                var showChar = 20;
            }
            $('.similar-mutual-funds-items .name-rating-wrap h6').each(function () {
                var content = $(this).html();
                if (content.length > showChar) {
                    var showLine = content.substr(0, showChar);
                    var remainContent = content.substr(showChar, content.length - showChar);
                    var allContent = showLine + '<span class="remaining-content d-none">' + remainContent + '</span> <span>...</span>';
                    $(this).html(allContent);
                }
            })
        }

        function reduceDigit(str, val) {
            str = str.toString();
            str = str.slice(0, (str.indexOf(".")) + val + 1);
            return Number(str);
        }
        popularfilterBizObj.reduceDigit = reduceDigit;
        return jsHelper.freezeObj(popularfilterBizObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "popularfilterBizObj", popularfilterBizFnObj)
})(this || window || {});
/*popular categories Filter search js end*/