/*sebi categories box js start*/
(function(_global) {
    var sebiCatBoxBizFnObj = (function(jsHelper) {
        var sebiCatBoxBizObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            if($('[data-sebiregisterbtn]') != undefined){
                $('[data-sebiregisterbtn]').click(function(){
                    var ctaText = $(this).text().trim();
                    var ctaTitle = $(this).parents('.invest-inlarge-box').find('h3').text().trim();
                    allCTAInteraction(ctaText,ctaTitle,'sebi-category-box', userIdObj.userId)
                    location.href = appConfig.jocataDomain;
                })
            }

            if (headerBizObj.getUserObj().isLoggedIn) { 
                if(!document.querySelector('[data-journey="invest-inlarge"]').classList.contains('d-none')){
                    document.querySelector('[data-journey="invest-inlarge"]').classList.add('d-none');
                }
            } else {
                if(document.querySelector('[data-journey="invest-inlarge"]').classList.contains('d-none')){
                    document.querySelector('[data-journey="invest-inlarge"]').classList.remove('d-none');
                }
            }
        });

        return jsHelper.freezeObj(sebiCatBoxBizObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "sebiCatBoxBizObj", sebiCatBoxBizFnObj)
})(this || window || {});
/*sebi categories box js end*/  