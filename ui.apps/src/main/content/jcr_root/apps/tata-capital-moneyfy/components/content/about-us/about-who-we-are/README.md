About who we are
====
This is a `About who we are` component used in about us page.

## Feature
* This is a authorable component.
* All the various elements of the component such as headings, description is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./mainHeading` Used for rendering main heading of the component.
2. `./heading` Used for rendering heading of the component.
3. `./description` Used for rendering description of the component.


## Client Libraries
The component provides a `moneyfy.about-who-we-are` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5