/*purpose pillar box slider js start*/
(function (_global) {
    var purposePillarBoxBizObj = (function (jsHelper) {
        var purposePillarBoxObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            purposePillarBoxSlick();
        })
        function purposePillarBoxSlick() {
            $('#purposePillarSlider').slick({
                dots: true,
                infinite: false,
                speed: 500,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 2,

                        }
                    },
                    {
                        breakpoint: 359,
                        settings: {
                            slidesToShow: 1,
                            variableWidth: true

                        }
                    }
                ]
            });
        }
        return jsHelper.freezeObj(purposePillarBoxObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "purposePillarBoxBizObj", purposePillarBoxBizObj)
})(this || window || {});
  /*purpose pillar box slider js end*/
