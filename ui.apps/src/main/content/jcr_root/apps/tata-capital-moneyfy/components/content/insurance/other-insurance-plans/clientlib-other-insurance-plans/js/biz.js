/*other insurance plan js start*/
(function (_global) {
  var otherInsurancePlanBizObj = (function (jsHelper) {
      var otherInsurancePlanObj = {}
      document.addEventListener('DOMContentLoaded', function () {
          otherInsurancePlanSlick();
          adobeAnalyticsCall();
      })

      function adobeAnalyticsCall() {
        $('[data-adobeAnalytics="otherInsuranceProducts"]').click(function (event) {
            event.preventDefault();
            var insuranceName = $(this).data('card-plan-name');
            insuranceItemClick(insuranceName, userIdObj.userId, 'other-insurance-plans');
            location.href = $(this).attr('href');
        });
      }

      function otherInsurancePlanSlick() {
          $('#jsOtherInsurancePlan').slick({
              dots: true,
              infinite: false,
              speed: 300,
              slidesToShow: 4,
              slidesToScroll: 1,
              arrows: true,
              responsive: [
                  {
                      breakpoint: 1024,
                      settings: {
                          slidesToShow: 3,
                          slidesToScroll: 1,
                      }
                  },
                  {
                      breakpoint: 768,
                      settings: {
                          slidesToShow: 2,
                          slidesToScroll: 1,
                      }
                  }
              ]
          });
      }

      return jsHelper.freezeObj(otherInsurancePlanObj);
  })(jsHelper)
  _global.jsHelper.defineReadOnlyObjProp(_global, "otherInsurancePlanBizObj", otherInsurancePlanBizObj)
})(this || window || {});
/*other insurance plan js end*/
