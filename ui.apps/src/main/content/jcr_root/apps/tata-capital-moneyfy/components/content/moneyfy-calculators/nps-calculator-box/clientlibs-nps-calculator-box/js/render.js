/*nps calculator render js start*/
(function (_global) {
    var npsCalculatorRenderFn = (function (jsHelper) {
        var npsCalculatorRenderObj = {}
        function renderNpsCalculation(e, npsInputObj) {
            var totalCorpus = document.getElementById('totalCorpus');
            var maximumWithdrawalTotal = document.getElementById('maximumWithdrawalTotal');
            var totalInvestment = document.getElementById('totalInvestment');
            var onlyPayout = document.getElementById('onlyPayout');
            var PayoutInterest = document.getElementById('PayoutInterest');
            var monthlyExpectedPension = document.getElementById('monthlyExpectedPension');
            var corpusTotalPurchase = document.getElementById('corpusTotalPurchase');
            var investPara = document.getElementById('investPara');
            var taxUpto = document.getElementById('taxUpto');
            if ((isNaN(npsInputObj.npsAmount)) || (npsInputObj.npsAmount < 500) || (npsInputObj.npsAmount > 1000000)) {
                totalCorpus.innerHTML = '₹ 0';
                maximumWithdrawalTotal.innerHTML = '₹ 0';
                corpusTotalPurchase.innerHTML = '₹ 0';
                totalInvestment.innerHTML = '₹ 0';
                // totalInterest.innerHTML = '₹' + Math.round(expectedCorpus); requirement not given
                onlyPayout.innerHTML = '₹ 0';
                PayoutInterest.innerHTML = '₹ 0';
                taxUpto.innerHTML = '₹ 0';
                investPara.innerHTML = '0';
                monthlyExpectedPension.innerHTML = '₹ 0';
                npsInputObj.npsHighchartReact(0, 0, 0)
            } else {
                totalCorpus.innerHTML = '₹ ' + (Math.round(e.data.expectedCorpus)).toLocaleString('en-IN');
                maximumWithdrawalTotal.innerHTML = '₹ ' + e.data.maxWithdrawal.toLocaleString('en-IN');
                corpusTotalPurchase.innerHTML = '₹ ' + Math.round(e.data.pensionPurchaseCorpus).toLocaleString('en-IN');
                totalInvestment.innerHTML = '₹ ' + e.data.totalInvestments.toLocaleString('en-IN');
                // totalInterest.innerHTML = '₹ ' + Math.round(expectedCorpus); requirement not given
                monthlyExpectedPension.innerHTML = '₹ ' + Math.round(e.data.monthlyPension).toLocaleString('en-IN');
                onlyPayout.innerHTML = '₹ ' + e.data.expectedPension.toLocaleString('en-IN');
                PayoutInterest.innerHTML = '₹ ' + e.data.pensionWithPrincipal.toLocaleString('en-IN');
                taxUpto.innerHTML = '₹ ' + Math.round(e.data.taxSaving80CCC).toLocaleString('en-IN');
                investPara.innerHTML = npsInputObj.npsAmount.toLocaleString('en-IN');
                npsInputObj.npsHighchartReact(5947297,e.data.totalInvestments, 100)
            }
        }

        npsCalculatorRenderObj.renderNpsCalculation = renderNpsCalculation;
        return jsHelper.freezeObj(npsCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "npsCalculatorRenderObj", npsCalculatorRenderFn)
})(this || window || {});
  /*nps calculator render js end*/