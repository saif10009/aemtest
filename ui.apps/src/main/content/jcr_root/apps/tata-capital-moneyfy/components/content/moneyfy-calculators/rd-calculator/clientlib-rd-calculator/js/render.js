/*rd calculator render js start*/
(function (_global) {
    var rdCalculatorRenderFn = (function (jsHelper) {
        var rdCalculatorRenderObj = {}

        function renderRdCalculation(e,rdInputObj) {
            var totalInvestment = document.getElementById("totalInvestment");
            var totalInterest = document.getElementById("totalInterest");
            var resultText = document.getElementById('starts-investing-text');
            var rdVal = document.getElementById("rdVal");
            if ((isNaN(rdInputObj.rdAmount)) || (rdInputObj.rdAmount < 500) || (rdInputObj.rdAmount > 1000000)) {
                rdVal.innerHTML = '₹0';
                totalInvestment.innerHTML = '₹0';
                totalInterest.innerHTML = '₹0';
                resultText.innerHTML = "If you invest ₹0 per year from today for " + e.data.months + " you will get";
                rdInputObj.rdHighchartReact(0, 0, 0)
            }
            else {
                rdVal.innerHTML =
                    "₹ " + Number(e.data.rd.toFixed(2)).toLocaleString('en-IN');
                totalInvestment.innerHTML = e.data.investment.toLocaleString('en-IN');
                totalInterest.innerHTML = Number((e.data.rd - e.data.investment).toFixed(2)).toLocaleString('en-IN');
                resultText.innerHTML = "If you invest ₹ " + rdInputObj.rdAmount.toLocaleString('en-IN') + " per year from today for " + e.data.months + " you will get";
                rdInputObj.rdHighchartReact((e.data.investment), (e.data.rd - e.data.investment), 100)
            }
        }

        rdCalculatorRenderObj.renderRdCalculation = renderRdCalculation;
        return jsHelper.freezeObj(rdCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "rdCalculatorRenderObj", rdCalculatorRenderFn)
})(this || window || {});
  /*rd calculator render js end*/