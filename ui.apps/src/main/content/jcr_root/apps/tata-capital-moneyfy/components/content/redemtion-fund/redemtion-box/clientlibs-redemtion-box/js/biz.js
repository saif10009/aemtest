/*choose from amc  biz.js start*/
(function (_global) {
    var chooseFromAmcBizFn = (function (jsHelper) {
        var chooseFromAmcObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            renderAmcData();

        });

		function renderAmcData() {
            let requestObj ={
			    "body": {}
			};

            chooseFromAmcApiObj.chooseFromAmc(requestObj).then(function(response){
                var responseData = jsHelper.isDef(response.response) && !jsHelper.isObj(response.response) ? JSON.parse(response.response) : response.response;
                var headerStatus = responseData.responseJson.header.status.toLowerCase();
                var bodyStatus = responseData.responseJson.body.status.toLowerCase();
                if (headerStatus == 'success') {
                    if (bodyStatus == 'success') {
						chooseFromAmcRenderObj.renderAmc(responseData.responseJson.body.fundsThroughAmcVOList)
                		initializeChooseAmcSlick();
                    }
                }else{
					window.alert('There was some error in getting Choose From Amc api response.');
                }
            }).catch(function(error){
				console.log(error);
            });

        }

        function initializeChooseAmcSlick() {
            $('#amcSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                if ($('.inner-amc-slide .fund-manager-rows').length <= 3) {
                    if ($(window).width() > 1199) {
                        $('.inner-amc-slide').removeClass('slider-dots');
                    }
                }
            });

            $('#amcSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                    {
                        breakpoint: 1200,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 992,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 1,
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            // variableWidth: true,
                            centerMode: true,
                            centerPadding: '20px'
                        }
                    },
                    {
                        breakpoint: 375,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: true,
                            centerMode: true,
                        }
                    },
                    {
                        breakpoint: 360,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            variableWidth: true,
                            centerMode: true,
                        }
                    }
                ]
            });
        }

        return jsHelper.freezeObj(chooseFromAmcObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "chooseFromAmcBizObj", chooseFromAmcBizFn)
})(this || window || {});
/*choose from amc biz.js end*/