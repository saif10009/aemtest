Error Banner
====
The `Error Banner` component used as a banner of the page. 


## Feature
* It is an authorable component. 


## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./description` used for form description.
2. `./buttonTitle` Used to render button text.
3. `./buttonLink` used to redirect button link.
4. `./newTab` used to redirect link in new tab.

## Client Libraries
The component provides a `moneyfy.error-banner` editor client library category that includes JavaScript and CSS
handling for dialog interaction.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5