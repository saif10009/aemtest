/*********************CITY TRIGGER - [START]************************/
var apiCall = {};
(function(){
var otpRefNumber;
var getSubSource = getURLParams(location.href);
var City;
        if(document.querySelector('[data-city="manualType"] input').value!==""){
       City = document.querySelector('[data-city="manualType"] input').value;
}else{
      City=$('[data-city="optionsType"]').find('button').text();
}
var iciciCardApiObj = {
    name: $('[data-type="name"]').val(),
    mobileNumber: $('[data-type="mobile"]').val(),
    city: City,
    pinCode: $('[data-type="pincode"]').val(),
    emailId: $('[data-type="email"]').val(),
    subsource: getSubSource.subsource ? getSubSource.subsource : 'null',
    productName: 'Tata Cards',
    productCode: 'Tata Cards'
}

function citySearchApiCall(method, url, data) {
    return new Promise(function (resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (this.readyState === 4) {
                switch (this.status) {
                    case 200:
                        resolve(this.responseText);
                        break;
                    default:
                        reject(this.responseText);
                }
            }
        }
        xhr.open(method, url);
        xhr.send(data);
    });
}

/* $(function () {
    if ($(".city").length) {
        $.ajax({
            url: '/content/tata-capital/mdm.cityproductmaster.json',
            type: "GET",
            async: false,
            success: function (data, status, xhr) {
                var obj = JSON.parse(xhr.responseText);
                var mainObj = {};

                for (var item in obj.Master) {
                    var subObj = {};
                    if (!mainObj.hasOwnProperty(obj.Master[item].product)) {
                        var arr = [];
                        mainObj[obj.Master[item].product] = subObj;
                        subObj['productName'] = obj.Master[item]['product-name'];
                        arr.push(obj.Master[item].city);
                        subObj['cities'] = arr;

                    } else {
                        var subObj = mainObj[obj.Master[item].product];
                        var arr = subObj['cities']
                        arr.push(obj.Master[item].city);
                    }
                }
                response = mainObj;
                var productCode = 'CREDIT CARDS';
                $(".city").html();
                $(".city").append("<option value=''></option>");
                // $(".city").append('<li><a href="javascript:void(0)"></a></li>')

                if (productCode == "LAP" || productCode == "LAPOD") {
                    productCode = "HE";
                }
                //                    if ((productCode == "MO101") || (productCode == "TR102") || (productCode == "HE103") || (productCode == "HO104") || (productCode == "PR105") || (productCode == "SIUL106") || (productCode == "SITR107") || (productCode == "HELI108")) {
                //                        productCode = "INSURANCE-HOME";
                //                    }
                if ((productCode == 'TW101') || (productCode == 'MO101') || (productCode == 'PR105') || (productCode == 'SITR107') || (productCode == 'HE103') || (productCode == 'HE104') || (productCode == 'HE105') || (productCode == 'WS101') || (productCode == 'WP101') || (productCode == 'HA101') || (productCode == 'HA101') || (productCode == 'HC101') || (productCode == 'CS101') || (productCode == 'PE101') || (productCode == 'RS101') || (productCode == 'CP101') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'CIS101') || (productCode == "HO104")) {
                    productCode = "INSURANCE";
                }
                if (response[productCode] !== undefined && response[productCode].cities.length > 0) {
                    response[productCode].cities.sort(function(a, b){
                        if(a < b) { return -1; }
                        if(a > b) { return 1; }
                        return 0;
                    })
                    .forEach(function (element) {
                        $(".city").append('<option value=' + element + '>' + element + '</option>');
                        // $(".city").append('<li><a href="javascript:void(0)">'+ element + '</a></li>')
                        
                    });
                } else {
                    console.log("CSV City issue");
                }
            },
            error: function () {

                console.log("FAIL");
            }
        });
    }
}); */

/*********************CITY TRIGGER - [END]************************/

/*********************GENERATE OTP - [START]************************/

function generateOTP() {

    var otpMobileNumber = {
        otpMobile: $(".input-mob-number .input-textbox.only-numeric-input").val()
    };
    //apiCall('POST', '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp',JSON.stringify())
    $('.js-loader').addClass('show');
    $.ajax({
        url: '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp',
        type: "POST",
        async: false,
        data: otpMobileNumber,
        success: function (data, status, xhr) {
            $('.js-loader').removeClass('show');
            otpRefNumber = data.otpRefNo;
            var getMobileNumber = $('#credit-card-form input[data-type="mobile"]').val();
            var firstTwoDigit = getMobileNumber.toString().slice(-2);
            $('.jsShowMob').text(firstTwoDigit);

            setTimeout(function () {
                $('#credit-card-modal').addClass('popover-show');
            }, 80);
            $('#credit-card-modal').css('display', 'block');
            $('.js-OtpBox input:nth-child(1)').focus();
            $('body').addClass('popover-modal-open');
            $('body').append('<div class="modal-backdrop"></div>');
        }
    })
}
/*********************GENERATE OTP - [END]************************/

/*********************VERIFY OTP - [START]************************/

function verifyOTP() {
    var array = [];
    $('.js-OtpBox input').each(function (e, i) {
        array.push(i.value)
    })
    array.join('')
    var verifyMobileNumber = {
        "otpRefNumber": otpRefNumber,
        "otpValue": array.join('')
    };
    //apiCall('POST', '/content/tata-capital-moneyfy/moneyfyapi.iciciGenerateOtp', otpMobileNumber)
    $('#credit-card-modal').removeClass('popover-show');
    $('.creditcard-otp-mod').addClass('d-none');
    $('.js-loader').addClass('show');
    console.log('loader started')
    $.ajax({
        url: '/content/tata-capital-moneyfy/moneyfyapi.verifyOtp',
        type: "POST",
        async: true,
        data: verifyMobileNumber,
        success: function (data, status, xhr) {
            if (data.retStatus == 'SUCCESS') { 
                $.ajax({
                    url: '/content/tata-capital-moneyfy/moneyfyapi.iciciCreditCardLeadApi',
                    type: "POST",
                    async: true,
                    data: apiCall.iciciCardApiObj,
                    success: function (data, status, xhr) {
                        $('.js-loader').removeClass('show');
                        console.log('loader end')
                        if ((data.Status).toUpperCase() == 'SUCCESS') {
                            $('#credit-card-form .input-textbox').val('');
                            $('[data-form="creditcard-otp"]').addClass('d-none');
                            $('[data-form="creditcard-thanks"]').removeClass('d-none')
                            $('#credit-card-modal').addClass('popover-show');
                            $('.thankyou-inner').html('<div class="thankyou-inner"><img src="/content/dam/tata-capital-moneyfy/icici-credit-card/ticks2.svg" alt=""><h4>You have successfully registered for an ICICI Credit Card.</h4><p>A Tata Capital executive will get in touch to process your application.</p><p class="lead">Your Lead Id  : <b>' + data.LeadId + '</b></p></div>')
                        } else if((data.Status).toUpperCase() == 'ERROR'){
                            $('#credit-card-form .input-textbox').val('');
                            $('[data-form="creditcard-otp"]').addClass('d-none');
                            $('[data-form="creditcard-thanks"]').removeClass('d-none')
                            $('#credit-card-modal').addClass('popover-show');
                            $('.thankyou-inner').html('<div class="thankyou-inner"><img src="/content/dam/tata-capital-moneyfy/icici-credit-card/ticks2.svg" alt=""><h4>'+ data.Message +'</h4></div>')
                        } 
                    }
                })
            } else if (data.retStatus == 'FAILURE') {
                $('.js-loader').removeClass('show');
                $('#credit-card-form .input-textbox').val('');
                $('[data-form="creditcard-otp"]').addClass('d-none');
                $('[data-form="creditcard-thanks"]').removeClass('d-none')
                $('#credit-card-modal').addClass('popover-show');
                $('.thankyou-inner').html('<div class="thankyou-inner"><h4>' + data.errorMessage + '</h4></div>');
            }
        }   
    })
}

apiCall.citySearchApiCall = citySearchApiCall;
apiCall.generateOTP = generateOTP;
apiCall.verifyOTP = verifyOTP;
apiCall.getSubSource = getSubSource;
apiCall.iciciCardApiObj = iciciCardApiObj;
})()
/*********************VERIFY OTP - [END]************************/
