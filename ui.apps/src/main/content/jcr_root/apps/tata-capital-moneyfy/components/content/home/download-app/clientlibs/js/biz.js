/*download app biz js start*/
(function (_global) {

    var downloadAppBizFn = (function (jsHelper) {
        var downloadAppBizObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            clickEvents();
            keyupEvents();
            adobeAnalytics();
        });
        function adobeAnalytics() {	
            $('[data-downloadapp] a').click(function () {	
                var widgetTitle = $(this).text().trim();	
                widgetnteraction(widgetTitle,userIdObj.userId)	
            })	
            $('[data-storeimg]').click(function(event){	
                event.preventDefault();	
                if($(this).data('storeimg') == 'appStoreImg'){	
                   appStoreImgClick('App Store', userIdObj.userId,'download-app')	
                   console.log('app')	
                   window.open($(this).attr('href'));	
                }else{	
                    appStoreImgClick('Google Play', userIdObj.userId,'download-app')	
                    console.log('google',$(this).attr('href'))	
                    window.open($(this).attr('href'));	
                }	
            })	
        }
        
        function clickEvents() {
            /*modal js*/
            $('[data-popovermodal="popover-modal"]').click(function () {
                var ele_target = $(this).attr('data-target');
                var ele_pop = $(this).text().trim();
                setTimeout(function () {
                    $(ele_target).addClass('popover-show');
                }, 80);
                $(ele_target).css('display', 'block');
                $('body').addClass('popover-modal-open');
                $('body').append('<div class="modal-backdrop"></div>');
                if(ele_pop === 'Credit Card'){
                    $('#submenuLoansCards_popup').find('#term-p').text('')
                    $('#submenuLoansCards_popup').find('#term-p').html('<b>Credit Card offerings are brought to you by third-party entities at their sole discretion. Tata Securities Limited (TSL)/ Moneyfy is merely acting as a Referrer and is only displaying the features of the Credit Cards on its platform and such display must not be construed as an offer or advice to transact in such Products. The mentioned details regarding the Credit Cards are as communicated by such third-party entities and TSL/ Moneyfy does not make any claims, warranties, or representations, express or implied regarding the Credit Cards or features thereof and shall assume no responsibility or liability in relation to the same or otherwise with respect to the Credit Cards whatsoever. Terms and conditions apply</b>')
                }else{
                    $('#submenuLoansCards_popup').find('#term-p').text('')
                    $('#submenuLoansCards_popup').find('#term-p').html('<b>Home Loans are brought to you by Tata Capital Housing Finance Limited(TCHFL).All' + 
                    '                            other Loans are brought to you by Tata Capital Financial Services Limited (TCFSL).All ' +
                    '                            loans are at the sole discretion of TCHFL and TCFSL respectively.Terms and conditions' +
                    '                            apply.</b>')
                }
            });
            $('[data-dis="dismiss"]').on('click', function () {
                $('.modal-backdrop').remove();
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('body').append('<div class="modal-backdrop"></div>');                
            })
            $('[data-dismiss="popover-modal"]').on('click', function () {
                $(this).parents('.popover-modal').removeClass('popover-show');
                $(this).parents('.popover-modal').removeAttr('style');
                $('.height-scroll').removeAttr('style');
                $('body').removeClass('popover-modal-open');
                $('.modal-backdrop').remove();  
            });
            /*modal js*/

            /*get app js*/

            $('.js-getapp-form-modal button').click(function (e) {
                var ele_input = $(this).parents('.js-getapp-form-modal').find('input');
                var errors = [];
                var ele_required = "Field is required";

                $(ele_input).each(function () {
                    var element = $(this);
                    var ele_phoneNumber = "Please enter valid number";

                    $(element).next('.error-msgs').remove();
                    $(element).after('<span class="error-msgs"></span>');

                    if (element.is(":visible")) {
                        if ($(element).val() != '') {
                            if ($(element).data('type') === 'mobile') {
                                if (!validateMobile(element)) {
                                    $(element).next('.error-msgs').text(ele_phoneNumber);
                                    errors.push(ele_phoneNumber);
                                } else {
                                    $(element).next('.error-msgs').remove();
                                }
                            }

                        } else {
                            $(element).next('.error-msgs').text(ele_required);
                            errors.push(ele_required);
                        }
                    }
                });

                if (errors.length == 0) {
                    e.preventDefault();

                    var getMobileNumber = $('.js-getapp-form-modal input').val();
                    var lastTwoDigit = getMobileNumber.toString().slice(-2);
                    if($(this).data('getlink')){
                    getAppLink(getMobileNumber, userIdObj.userId,'download-app')
                    }
                    $('.jsShowMobileNumber').text(lastTwoDigit);
                    var ele_target = $(this);

                    var current_fs = ele_target.data('current');
                    var next_fs = ele_target.data('next');
                    var formdata = {};
                    formdata.mobile = getMobileNumber;
                    $(this).parents('.form-wizard').find('#' + current_fs).addClass('hidden');
                    $('.js-getapp-form-modal').find('.get-app-loader').removeClass('hidden');
                    setTimeout(function () {
                        $('.js-getapp-form-modal').find('.get-app-loader').addClass('hidden');

                    }, 1000)
                    var requestData = {
                        "body": {
                            "mobile": getMobileNumber
                           }
                    }
                    downloadAppApiObj.downloadApp(requestData).then(function (response) {
                        setTimeout(function () {
                             $('.js-getapp-form-modal').find('.get-app-loader').addClass('hidden');
                             $('.js-getapp-form-modal.form-wizard').find('#' + next_fs).removeClass('hidden');

                        }, 1000);
                    }).catch(function (error) {
                        setTimeout(function () {
                            console.log('api failed');
                        }, 1000);
                    });
                }
            });


            $('.jsEditGetAppModal').click(function () {
                $(this).parents('.js-getapp-form-modal').find('.error-msgs').remove();
                $(this).parents('.js-getapp-form-modal').find('#moneyfy01').removeClass('hidden');
                $(this).parents('.js-getapp-form-modal').find('#moneyfy02').addClass('hidden');
                $(this).parents('.js-getapp-form-modal').find('.get-app-loader').addClass('hidden');
                $('#download-modal').find('.downloadapp-api-error').addClass('hidden');
            })

            $('.jsOpenModal').click(function () {
                $('.js-getapp-form-modal input').val('');
                $('.js-getapp-form-modal').find('.error-msgs').remove();
                $('.js-getapp-form-modal #moneyfy01').removeClass('hidden');
                $('.js-getapp-form-modal #moneyfy02').addClass('hidden');
                $('.js-getapp-form-modal .get-app-loader').addClass('hidden');
            })
            /*get app js*/
        }

        function keyupEvents() {
            /*get app js*/
            $('.only-numeric-input').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, '').replace(/[^\w\s]/gi, ""));
            });



            /* get app modal*/
            $('.js-getapp-form-modal input').keyup(function () {
                var element = $(this);
                var ele_required = "Field is required";
                var ele_phoneNumber = "Please enter valid number";

                $(element).next('.error-msgs').remove();
                $(element).after('<span class="error-msgs"></span>');

                if (element.is(":visible")) {
                    if ($(element).val() != '') {
                        if ($(element).data('type') === 'mobile') {
                            if (!validateMobile(element)) {
                                $(element).next('.error-msgs').text(ele_phoneNumber);
                            } else {
                                $(element).next('.error-msgs').remove();
                            }
                        }

                    } else {
                        $(element).next('.error-msgs').text(ele_required);
                    }
                }
            });
               /*27-8-2021*/
        }
        function validateMobile(mobileField) {
            var re = /^[4-9][0-9]{9}$/;
            var check = re.test($(mobileField).val());
            if ($(mobileField).val().length != 10 || !check) {
                return false;
            } else {
                return true;
            }
        }
        return jsHelper.freezeObj(downloadAppBizObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'downloadAppBizObj', downloadAppBizFn);
})(this || window || {});

/*download app biz js end*/