/* fundDetail api js start */
(function (_global) {
    var fundDetailApiFn = (function (jsHelper) {
        var fundDetailApiObj = {};
        var getGraph = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getGraph(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })
        }
        fundDetailApiObj.getGraph = getGraph;
        return jsHelper.freezeObj(fundDetailApiObj);
    })(jsHelper);
    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundDetailApiObj', fundDetailApiFn);
})(this || window || {});
/* fundDetail api js end */
