Fund Managers Detail Banner
====
The `Fund Managers Detail Banner` component is for showing the detail of an manager on the fund-managers-detail page.



## Feature
* The data is populated in the component through pageProperties.



## Edit Dialog Properties
The component has no properties stored in edit dialog.


## Client Libraries
The component provides a `moneyfy.fund-managers-detail-banner` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5