document.addEventListener('DOMContentLoaded', function(){
    apiCall.citySearchApiCall('GET', '/content/tata-capital/mdm.cityproductmaster.json').then(function (response) {
    var obj = JSON.parse(response);
    var mainObj = {};
    for (var item in obj.Master) {
        var subObj = {};
        if (!mainObj.hasOwnProperty(obj.Master[item].product)) {
            var arr = [];
            mainObj[obj.Master[item].product] = subObj;
            subObj['productName'] = obj.Master[item]['product-name'];
            arr.push(obj.Master[item].city);
            subObj['cities'] = arr;

        } else {
            var subObj = mainObj[obj.Master[item].product];
            var arr = subObj['cities']
            arr.push(obj.Master[item].city);
        }
    }
    response = mainObj;
    var productCode = 'PL';
    if (productCode == "LAP" || productCode == "LAPOD") {
        productCode = "HE";
    }
    if ((productCode == 'TW101') || (productCode == 'MO101') || (productCode == 'PR105') || (productCode == 'SITR107') || (productCode == 'HE103') || (productCode == 'HE104') || (productCode == 'HE105') || (productCode == 'WS101') || (productCode == 'WP101') || (productCode == 'HA101') || (productCode == 'HA101') || (productCode == 'HC101') || (productCode == 'CS101') || (productCode == 'PE101') || (productCode == 'RS101') || (productCode == 'CP101') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'TR102') || (productCode == 'CIS101') || (productCode == "HO104")) {
        productCode = "INSURANCE";
    }
    if (response[productCode] !== undefined && response[productCode].cities.length > 0) {
        response[productCode].cities.sort(function(a, b){
            if(a < b) { return -1; }
            if(a > b) { return 1; }
            return 0;
        })
        .forEach(function (element) {
            var citiesContainer = document.querySelector('#searchCity');
            citiesContainer.innerHTML += '<option>' + ' <a href="javascript:void(0)">'+ element +'</a>' + '</option>'
        });
    } else {
        console.log("CSV City issue");
    }
    });
})