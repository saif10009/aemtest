/*chooseFromAMC render.js start*/
(function(_global) {
    var chooseFromAmcRenderFnObj = (function(jsHelper) {
        var chooseFromAmcRenderObj = {}

        let renderAmc = function (amcArray) {
            let amcData = document.getElementById('amcSlider');
            let count = 1;
                amcArray.forEach(function(amc){
                    if(count<=5){
                     count = count + 1;   
                    amcData.innerHTML += '<div class="fund-manager-rows">'+
                    '                <div class="fund-manager-col">'+
                    '                  <div class="card-div fund-manager-card" data-ID='+amc['mfFundHouseId']+'>'+
                    '                    <div class="fund-manager-top">'+
                    '                      <div class="amc-text-logo">'+
                    '                        <span class="amc-logo">'+
                    '                          <img src="https://tata-capital-mds.web.app/tata-moneyfy-new/img/amc-axis.png" alt="">'+
                    '                        </span>'+
                    '                        <h5>'+amc['fundName']+'</h5>'+
                    '                      </div>'+
                    '                    </div>'+
                    '                    <div class="fund-manager-bottom">'+
                    '                      <div class="fund-ul">'+
                    '                        <div class="fund-li">'+
                    '                          <p>No. of funds</p>'+
                    '                          <h6>'+amc['numberOfFunds']+'</h6>'+
                    '                        </div>'+
                    '                        <div class="fund-li">'+
                    '                          <p>Total Fund Size</p>'+
                        '                          <h6>'+ (amc['totalFundSize'] ? amc['totalFundSize'] : "-") +'</h6>'+
                    '                        </div>'+
                    '                        <div class="fund-li">'+
                    '                          <p>Highest Returns</p>'+
                    '                          <h6>'+amc['highestReturns']+'</h6>'+
                    '                        </div>'+
                    '                      </div>'+
                    '                    </div>'+
                    '                  </div>'+
                    '                  <div class="card-div fund-manager-card mob-card-show" style="display:none">'+
                    '                    <div class="fund-manager-top">'+
                    '                      <div class="amc-text-logo">'+
                    '                        <span class="amc-logo">'+
                    '                          <img src="https://tata-capital-mds.web.app/tata-moneyfy-new/img/amc-axis.png" alt="">'+
                    '                        </span>'+
                    '                        <h5>'+amc['fundName']+'</h5>'+
                    '                      </div>'+
                    '                    </div>'+
                    '                    <div class="fund-manager-bottom">'+
                    '                      <div class="fund-ul">'+
                    '                        <div class="fund-li">'+
                    '                          <p>No. of funds</p>'+
                    '                          <h6>'+amc['numberOfFunds']+'</h6>'+
                    '                        </div>'+
                    '                        <div class="fund-li">'+
                    '                          <p>Total Fund Size</p>'+
                    '                          <h6>'+ (amc['totalFundSize'] ? amc['totalFundSize'] : "-") +'</h6>'+
                    '                        </div>'+
                    '                        <div class="fund-li">'+
                    '                          <p>Highest Returns</p>'+
                    '                          <h6>'+amc['highestReturns']+'</h6>'+
                    '                        </div>'+
                    '                      </div>'+
                    '                    </div>'+
                    '                  </div>'+
                    '                </div>'+
                    '              </div>';
                        
                    ;
                    }    
                });

        }

        chooseFromAmcRenderObj.renderAmc = renderAmc;

        return jsHelper.freezeObj(chooseFromAmcRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "chooseFromAmcRenderObj", chooseFromAmcRenderFnObj);
})(this || window || {});
/*chooseFromAmc render.js end*/
