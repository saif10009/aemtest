/*Service request js start*/
(function (_global) {
    var serviceRequestBizObj = (function (jsHelper) {
        var serviceRequestObj = {}

        document.addEventListener('DOMContentLoaded', function () {
            $('[data-servicer-call="sevicecall"]').click(function () {
                var ctaText = $(this).text().trim();
                var ctaTitle = $(this).parent().find('h4').text().trim();
                allCTAInteraction(ctaText,ctaTitle,'service-request',userIdObj.userId)
            })
        })

        return jsHelper.freezeObj(serviceRequestObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "serviceRequestBizObj", serviceRequestBizObj)
})(this || window || {});
/*Service request js end*/