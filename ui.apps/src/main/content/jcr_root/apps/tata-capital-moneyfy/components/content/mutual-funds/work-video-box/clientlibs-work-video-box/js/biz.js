//mobile code
if ($(window).width() < 780) {
window.addEventListener('touchstart', function(){
	var ytVideoFrame = document.querySelector('.work-video');
	ytVideoFrame.innerHTML = '<iframe class="ytFrame" width="100%" height="100%" src="https://www.youtube.com/embed/l0sOLzz6wlE"'+
'    title="YouTube video player" frameborder="0"'+
'    allow="accelerometer; autoplay; clipboard-write; gyroscope; picture-in-picture" allowfullscreen>'+
'</iframe>'
});
}

//desktop
if ($(window).width() >= 780) {
window.addEventListener('load', function(){
	var ytVideoFrame = document.querySelector('.work-video');
	ytVideoFrame.innerHTML = '<iframe class="ytFrame" width="100%" height="100%" src="https://www.youtube.com/embed/l0sOLzz6wlE"'+
'    title="YouTube video player" frameborder="0"'+
'    allow="accelerometer; autoplay; clipboard-write; gyroscope; picture-in-picture" allowfullscreen>'+
'</iframe>'
});
}