Sebi Categories
====
The `Sebi Categories` component is for showing the various mutual sebi categories on moneyfy mutual fund page. 



## Feature
* It is an multifield based component.
* All the various elements of the component such as category and sub-category names, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title ./heading` Used for adding the title and heading of the component.
2. `./equityImage ./debtImage ./hybridImage ./othersImage` Used for provifding the catgory images.
3. `./image ./hoverImage` Used to select the images for the categories in the component.
4. `./equityPath ./debtPath ./hybridPath ./othersPath ./redirectPath` are used for the redirection paths on the button click in each categories.
5. `./equityNewTab ./debtNewTab ./hybridNewTab ./othersNewTab ./newTab` are used to check if the link should open in new tabs.
6. `./subCategoryName` is used for giving the sub category names.


## Client Libraries
The component provides a `moneyfy.sebi-categories` editor client library category that includes JavaScript and CSS
handling for dialog interaction.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5