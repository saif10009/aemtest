/*insurance why moneyfy js start*/
(function (_global) {
  var insuranceWhyMoneyfyBizObj = (function (jsHelper) {
      var insuranceWhyMoneyfyObj = {}
      document.addEventListener('DOMContentLoaded', function () {
          insuranceWhyMoneyfySlick();
      })
      function insuranceWhyMoneyfySlick() {
          if ($(window).width() > 767) {
              if ($('#whyMonefySlider').hasClass('slick-initialized')) {
                  $('#whyMonefySlider').slick('unslick');
              }
              if ($('#aboutUsDataSlider').hasClass('slick-initialized')) {
                  $('#aboutUsDataSlider').slick('unslick');
              }
          } else {
              $('#whyMonefySlider').not('.slick-initialized').slick({
                  dots: true,
                  infinite: false,
                  speed: 300,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  arrows: true,
                  responsive: [
                      {
                          breakpoint: 1024,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1,
                          }
                      },
                      {
                          breakpoint: 992,
                          settings: {
                              slidesToShow: 1,
                              slidesToScroll: 1,
                          }
                      },
                      {
                          breakpoint: 768,
                          settings: {
                              slidesToShow: 2,
                              slidesToScroll: 1,
                          }
                      },
                      {
                          breakpoint: 360,
                          settings: {
                              slidesToShow: 1,
                              slidesToScroll: 1,
                          }
                      }
                  ]
              });

              $('#aboutUsDataSlider').not('.slick-initialized').slick({
                  dots: true,
                  infinite: false,
                  speed: 300,
                  slidesToShow: 2,
                  slidesToScroll: 1,
                  arrows: true,
                  variableWidth: true,
              });
          }
      }
      return jsHelper.freezeObj(insuranceWhyMoneyfyObj);
  })(jsHelper)
  _global.jsHelper.defineReadOnlyObjProp(_global, "insuranceWhyMoneyfyBizObj", insuranceWhyMoneyfyBizObj)
})(this || window || {});
/*insurance why moneyfy js end*/
