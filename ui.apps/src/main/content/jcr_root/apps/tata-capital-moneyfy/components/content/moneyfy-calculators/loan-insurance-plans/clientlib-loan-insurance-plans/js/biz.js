/*loan insurance plan js start*/
(function (_global) {
    var loanInsurancePlanBizObj = (function (jsHelper) {
        var loanInsurancePlanObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            loanInsurancePlanSlick();
            loanInsurancePlanClickFn();
        })
      function loanInsurancePlanClickFn() {
        $('[data-loanitem="loanitemclick"]').click(function (event) {
          event.preventDefault();
          var loanName = $(this).find('h5').text().trim();
          loanItemClick(loanName, userIdObj.userId, 'loan-insurance-plans');
          location.href = $(this).attr('href');
        })
      }
        function loanInsurancePlanSlick() {
            $('#jsLoanOffered').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                  {
                    breakpoint: 1024,
                    settings: {
                      slidesToShow: 3,
                      slidesToScroll: 1,
                    }
                  },
                  {
                    breakpoint: 769,
                    settings: {
                      slidesToShow: 2,
                      slidesToScroll: 1,
                    }
                  }
                ]
              });
        }
        return jsHelper.freezeObj(loanInsurancePlanObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "loanInsurancePlanBizObj", loanInsurancePlanBizObj)
})(this || window || {});
  /*loan insurance plan js end*/
