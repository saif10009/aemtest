/* Fund Manager List api.js start */
(function (_global) {

    var fundManagerListApiFn = (function (jsHelper) {
        var fundManagerListApiObj = {};

        var fundManagerList = function (requestObj) {
            return new Promise(function (resolve) {
                apiUtility.getFundsViaFundManagerName(requestObj).then(function (response) {
                    var responseObj = {
                        status: "SUCCESS",
                        response: response
                    }
                    resolve(responseObj);
                }).catch(function (error) {
                    var responseObj = {
                        status: "FAILURE",
                        response: error
                    }
                    resolve(responseObj)
                });
            })

        }
        fundManagerListApiObj.fundManagerList = fundManagerList;

        return jsHelper.freezeObj(fundManagerListApiObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'fundManagerListApiObj', fundManagerListApiFn);
})(this || window || {});
/* Fund Manager List api.js end */