Invest In Box
====
The `Invest In Box` component is for adding invest in mutual fund descriptive box on moneyfy funds list page. 



## Feature
* It is an static component.   



## Edit Dialog Properties
There are no proerties stored in the edit dialog of the component.

## Client Libraries
The component provides a `moneyfy.fund-list-invest-in-box` editor client library category that includes CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5