    /*IhoProductSlider slider js*/
    (function (_global) {
      var ihoProductSliderBizObj = (function (jsHelper) {
          var ihoProductSliderObj = {}
          document.addEventListener('DOMContentLoaded', function () {
              ihoProductSliderSlickInitilizer();
              $('[data-product-adobe-action]').click(function (event) {
                  event.preventDefault();
                  if ($(this).data('product-adobe-action') == "withpopup") {
                      var clickTextTitle = $(this).parents('.product-item').find('[data-card-heading]').data('card-heading');
                      var  clickButtonTitle= $(this).text().trim();
                      allCTAInteraction(clickButtonTitle, clickTextTitle, 'product-box', userIdObj.userId)
                  } else {
                    var clickTextTitle = $(this).parents('.product-item').find('[data-card-heading]').data('card-heading');
                    var  clickButtonTitle= $(this).text().trim();
                      allCTAInteraction(clickButtonTitle, clickTextTitle, 'product-box', userIdObj.userId)
                      location.href = $(this).attr('href')
                  }

              });	
          })
          function ihoProductSliderSlickInitilizer() {
              if ($(window).width() > 991) {
                  if ($('#ihoProductSlider').hasClass('slick-initialized')) {
                      $('#ihoProductSlider').slick('unslick');
                  }
              } else {
                  $('#ihoProductSlider').not('.slick-initialized').slick({
                      dots: true,
                      infinite: false,
                      speed: 300,
                      slidesToShow: 1,
                      slidesToScroll: 1,
                      arrows: false,
                      adaptiveHeight: true
                  });
              }
          }


          return jsHelper.freezeObj(ihoProductSliderObj);
      })(jsHelper)
      _global.jsHelper.defineReadOnlyObjProp(_global, "ihoProductSliderBizObj", ihoProductSliderBizObj)
  })(this || window || {});
/*ihoProductSlider Slider js end*/