/*fund-filter render.js start*/
(function (_global) {
    var fundFilterBoxRenderFn = (function (jsHelper) {
        var fundFilterBoxRenderObj = {}
        function renderFilteredFunds(sortArray, renderArrayLength) {
            document.querySelector('#amcFundList').innerHTML = '';
            if (sortArray.length > 0) {
                var renderHtmlStr = "";
                sortArray.forEach(function (fund, index) {
                    var iconPath = '/content/dam/tata-capital-moneyfy/amc-icons/';
                    var path = '/content/tata-capital-moneyfy/en/mutual-funds/';
                    var fundPath = fund.amcName + '/' + fund.schemeDetails.name;
                    iconPath = iconPath + fund.amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.png');
                    path = path.concat(fundPath.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.html'));
                    path = path.replace(/[-]+/g, '-');
                    if (fund.isNfo == 0) {
                        renderHtmlStr += '<div class="fund-list-li js-similarMFcard" data-schemeId="' + fund.schemeDetails.id + '">' +
                            '                    <div class="card-div similar-mutual-funds-items">' +
                            '                        <div class="top-row">' +
                            '                            <div class="info-rating-wrap">' +
                            '                                <div class="name-return-wrap">' +
                            '                                    <div class="img-name-wrap">' +
                            '                                        <div class="img-wrap">' +
                            '                                            <img src="' + (amcIconList.indexOf(iconPath) != -1 ? iconPath : '/content/dam/tata-capital-moneyfy/amc-icons/moneyfy-default-fund-icon.png') + '"' +
                            '                                                alt="" />' +
                            '                                        </div>' +
                            '                                        <div class="name-rating-wrap">' +
                            '                                            <a href="'+path+'" class="mf-fund-name" data-viewdetailbtn="viewDetailBtn"><h6 class="fund-name two-lines" data-schemeName="' + fund.schemeDetails.name + '">' + fund.schemeDetails.name + '</h6></a>' +
                            '                                            <div class="rating-wrap d-none d-md-flex">' +
                            '                                                <p>' +
                            '                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                            '                                                    Morning Star' +
                            '                                                </p>' +
                            '                                                <p>' +
                            '                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.valueResearch) != '-' ? fund.ratings.valueResearch : '-') + ' <span class="icon-star"></span></span>' +
                            '                                                    Value Research' +
                            '                                                </p>' +
                            '                                            </div>' +
                            '                                        </div>' +
                            '                                    </div>' +
                            '' +
                            '                                    <div class="return-info d-md-none">' +
                            '                                        <div class="form-select2">' +
                            '                                            <select class="single-select2 returnsVal" data-placeholder="-- Select --">' +
                            '                                               ' + '<option value=' + isNullOrZeroCheck(fund.cagrValues['1m']) + '>' + '1M Returns' + '</option>' +
                            '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1y']) + '>' + '1Y Returns' + '</option>' +
                            '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['3y']) + '>' + '3Y Returns' + '</option>' +
                            '                                            </select>' +
                            '                                        </div>' +
                            '                                        <h6 class="trend-info' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? ' up-trend"' : ' down-trend"') : '-') + 'data-rate="rate"' + '>' +
                            '                                            <span class=' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? "icon-angle-up" : "icon-angle-down") : '-') + '></span> ' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%' +
                            '                                        </h6>' +
                            '                                    </div>' +
                            '' +
                            '                                </div>' +
                            '                                <div class="rating-wrap d-md-none">' +
                            '                                    <p>' +
                            '                                        <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                            '                                        Morning Star' +
                            '                                    </p>' +
                            '                                    <p>' +
                            '                                        <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.valueResearch) != '-' ? fund.ratings.valueResearch : '-') + ' <span class="icon-star"></span></span>' +
                            '                                        Value Research' +
                            '                                    </p>' +
                            '                                </div>' +
                            '                            </div>' +
                            '                            <div class="returs-box d-none d-md-flex">' +
                            '                                <div class="returs-wrap">' +
                            '                                    <div class="return-item">' +
                            '                                        <p class="label-text">NAV</p>' +
                            '                                        <h6>' + (isNullOrEmptyCheck(fund.nav) != '-' ? fundListBizObj.reduceDigit(fund.nav, 2) : '-') + '</h6>' +
                            '                                    </div>' +
                            '                                    <div class="return-item">' +
                            '                                        <p class="label-text">1M Returns</p>' +
                            '                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1m']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['1m'], 2) : '-') + '%</h6>' +
                            '                                    </div>' +
                            '                                    <div class="return-item">' +
                            '                                        <p class="label-text">1Y Returns</p>' +
                            '                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1y']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['1y'], 2) : '-') + '%</h6>' +
                            '                                    </div>' +
                            '                                    <div class="return-item">' +
                            '                                        <p class="label-text">3Y Returns</p>' +
                            '                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['3y']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['3y'], 2) : '-') + '%</h6>' +
                            '                                    </div>' +
                            '                                </div>' +
                            '                            </div>' +
                            '                            <div class="invest-more-wrap d-none d-md-block">' +
                            '                                <a href="javascript:void(0);" class="btn-blue radius100 invest-now-btn" data-investnow="true">invest now</a>' +
                            '                                <a href="javascript:void(0)" class="btn-view-all js-viewMoreToggleBtn"' +
                            '                                    data-btn="mf-dektop-view-more">' +
                            '                                    <span class="text-wrap">More</span> <span class="icon-arrow-arrow"></span>' +
                            '                                </a>' +
                            '                            </div>' +
                            '                        </div>' +
                            '                        <div class="desktop-view-more" data-toggleCard="mf-dektop-view-more">' +
                            '                            <div class="returs-wrap">' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Min. Investment</p>' +
                            '                                    <h6>Rs.' + (isNullOrEmptyCheck(fund.minSipAmt) != '-' ? fund.minSipAmt : fund.minLumpsumAmt) + '</h6>' +
                            '                                </div>' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Fund Size</p>' +
                            '                                    <h6>Rs.' + (isNullOrEmptyCheck(fund.fundSize) != '-' ? fund.fundSize : '-') + ' Cr</h6>' +
                            '                                </div>' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Fund Manager</p>' +
                            '                                    <h6>' + (isNullOrEmptyCheck(fund.fundManagerName) != '-' ? fund.fundManagerName : '-') + '</h6>' +
                            '                                </div>' +
                            '                            </div>' +
                            '                        </div>' +
                            '                        <div class="bottom-row">' +
                            '                            <div class="tag-more-wrap">' +
                            '                                <div class="tag-wrap">' +
                            '                                    <span class="tag-item blue-tag" data-risktype="' + fund.schemeDetails.riskType + '">' + (fund.schemeDetails.riskType != '' ? fund.schemeDetails.riskType : '-') + '</span>' +
                            '                                    <span class="tag-item violent-tag" data-catname="' + fund.schemeDetails.catName + '">' + (fund.schemeDetails.catName != '' ? fund.schemeDetails.catName : '-') + '</span>' +
                            '                                    <span class="tag-item green-tag">' + (fund.schemeDetails.optName != '' ? fund.schemeDetails.optName : '-') + '</span>' +
                            '                                </div>' +
                            '                                <a href="javascript:void(0)" class="btn-view-all js-viewMoreToggleBtn  d-md-none"' +
                            '                                    data-btn="mf-view-more">' +
                            '                                    <span class="text-wrap">More</span> <span class="icon-arrow-arrow"></span>' +
                            '                                </a>' +
                            '                            </div>' +
                            '                            <div class="button-wrap d-none d-md-flex">' +
                            '                                <div class="watchlist-compare-btn">' +
                            '                                    <a href="javascript:void(0);" data-addwatchlist="watchListBtn" class="add-watchlist-wrap js-addWatchlist ' + (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'addedWatchlist' : '') + '">' +
                            '                                        <span class="watchlist-icon"><i class="icon-binocular"></i></span>' +
                            '                                        <span class="js-toggleAdd wishlist-text">' + (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'Added to watchlist' : 'ADD to watchlist') + '</span>' +
                            '                                    </a>' +
                            '                                    <div class="custom-checkbox add-compare-wrap">' +
                            '                                        <label class="js-addCompare">' +
                            '                                            <input type="checkbox" />' +
                            '                                            <span class="checkbox-wrap"></span>' +
                            '                                            ADD TO compare' +
                            '                                        </label>' +
                            '                                    </div>' +
                            '                                    <a href="' + path + '" target="_blank" class="btn-view-all" data-viewdetailbtn="viewDetailBtn">view details<span' +
                            '                                            class="icon-arrow-right"></span></a>' +
                            '                                </div>' +
                            '                                <div class="view-btn-wrap">' +
                            '' +
                            '                                </div>' +
                            '                            </div>' +
                            '' +
                            '                        </div>' +
                            '' +
                            '                        <div class="view-more-wrap d-md-none" data-toggleCard="mf-view-more">' +
                            '                            <div class="returs-wrap">' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">NAV</p>' +
                            '                                    <h6>' + (isNullOrEmptyCheck(fund.nav) != '-' ? fundListBizObj.reduceDigit(fund.nav, 2) : '-') + '</h6>' +
                            '                                </div>' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Min Investment</p>' +
                            '                                    <h6>₹' + (isNullOrEmptyCheck(fund.minSipAmt) != '-' ? fund.minSipAmt : fund.minLumpsumAmt) + '</h6>' +
                            '                                </div>' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Fund Size</p>' +
                            '                                    <h6>₹' + (isNullOrEmptyCheck(fund.fundSize) != '-' ? fund.fundSize : '-') + ' cr</h6>' +
                            '                                </div>' +
                            '                                <div class="return-item">' +
                            '                                    <p class="label-text">Fund Manager</p>' +
                            '                                    <h6>' + (isNullOrEmptyCheck(fund.fundManagerName) != '-' ? fund.fundManagerName : '-') + '</h6>' +
                            '                                </div>' +
                            '                            </div>' +
                            '                            <div class="watchlist-compare-btn">' +
                            '                                <a href="javascript:void(0);" data-addwatchlist="watchListBtn" class="add-watchlist-wrap js-addWatchlist ' + (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'addedWatchlist' : '') + '">' +
                            '                                        <span class="watchlist-icon"><i class="icon-binocular"></i></span>' +
                            '                                        <span class="js-toggleAdd wishlist-text">' + (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'Added to watchlist' : 'ADD to watchlist') + '</span>' +
                            '                                    </a>' +
                            '                                <div class="custom-checkbox add-compare-wrap">' +
                            '                                    <label class="js-addCompare">' +
                            '                                        <input type="checkbox">' +
                            '                                        <span class="checkbox-wrap"></span>' +
                            '                                        ADD TO compare' +
                            '                                    </label>' +
                            '                                </div>' +
                            '                            </div>' +
                            '                            <div class="invest-detail-wrap">' +
                            '                                <a href="javascript:void(0);" class="btn-blue radius100 invest-now-btn" data-investnow="true">invest now</a>' +
                            '                                <a href="' + path + '" target="_blank" class="btn-view-all" data-viewdetailbtn="viewDetailBtn">view details<span' +
                            '                                        class="icon-arrow-right"></span></a>' +
                            '                            </div>' +
                            '                        </div>' +
                            '                    </div>' +
                            '                </div>';
                    }
                });
                document.querySelector('#fundListSize').innerHTML = renderArrayLength;
                document.querySelector('#amcFundList').innerHTML = renderHtmlStr;
            } else {
                document.querySelector('#fundListSize').innerHTML = renderArrayLength;
                document.querySelector('#amcFundList').innerHTML = "No funds found in these category.";
            }
        }

        function renderWatchListPortfolioFunds(renderArray,addFromType) {
            var fundRenderStr = '';
            var selector = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
            if (renderArray.length > 0) {
                renderArray.forEach(function (fund) {
                    var iconPath = '/content/dam/tata-capital-moneyfy/amc-icons/';
                    iconPath = iconPath + fund.amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.png');
                    fundRenderStr += '<div class="fund-list-li" data-schemeid="' + fund.schemeDetails.id + '">' +
                        '                                <div class="card-div similar-mutual-funds-items">' +
                        '                                    <div class="top-row">' +
                        '                                        <div class="checkbox-name-wrap">' +
                        '                                            <div class="custom-checkbox">' +
                        '                                                <label class="js-addCompare">' +
                        '                                                    <input type="checkbox">' +
                        '                                                    <span class="checkbox-wrap"></span>' +
                        '                                                </label>' +
                        '                                            </div>' +
                        '                                            <div class="info-rating-wrap">' +
                        '                                                <div class="name-return-wrap">' +
                        '                                                    <div class="img-name-wrap">' +
                        '                                                        <div class="img-wrap">' +
                        '                                                            <img src="'+ iconPath +'" alt="" />' +
                        '                                                        </div>' +
                        '                                                        <div class="name-rating-wrap">' +
                        '                                                            <h6 class="fund-name two-lines" data-schemename="' + fund.schemeDetails.name + '">' + fund.schemeDetails.name + '</h6>' +
                        '                                                            <div class="rating-wrap d-none d-md-flex">' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Morning Star' +
                        '                                                                </p>' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Value Research' +
                        '                                                                </p>' +
                        '                                                            </div>' +
                        '                                                        </div>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="returs-box d-none d-lg-block">' +
                        '                                            <div class="d-flex">' +
                        '                                                <div class="returs-wrap">' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">NAV</p>' +
                        '                                                        <h6>' + fundListBizObj.reduceDigit(fund.nav, 2) + '</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1M Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1m']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['1m'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1y']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['1y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">3Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['3y']) != "-" ? fundListBizObj.reduceDigit(fund.cagrValues['3y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="return-info d-lg-none">' +
                        '                                            <div class="form-select2">' +
                        '                                                <select class="single-select2 portfolioWatchListReturns" data-placeholder="-- Select --">' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1m']) + '>1M Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1y']) + '>1Y Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['3y']) + '>3Y Returns</option>' +
                        '                                                </select>' +
                        '                                            </div>' +
                        '                                            <h6 class="trend-info text-right' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? ' up-trend"' : ' down-trend"') : '-') + 'data-rate="rate"' + '>' +
                        '                                            <span class=' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? "icon-angle-up" : "icon-angle-down") : '-') + '></span> ' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%' +
                        '                                        </h6>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                    <div class="rating-mob">' +
                        '                                        <div class="rating-wrap d-md-none">' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Morning Star' +
                        '                                            </p>' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Value Research' +
                        '                                            </p>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                </div>' +
                        '                            </div>';
                });    
            } else {
                fundRenderStr = '<p class="no-found">No funds found.</p>'
            }
            
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
        }

        fundFilterBoxRenderObj.renderFilteredFunds = renderFilteredFunds;
        fundFilterBoxRenderObj.renderWatchListPortfolioFunds = renderWatchListPortfolioFunds;

        return jsHelper.freezeObj(fundFilterBoxRenderObj);
        //adobe analytics  112 113 134 178
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "fundFilterBoxRenderObj", fundFilterBoxRenderFn)
})(this || window || {});
/*fund-filter-list render.js end*/