function pageTypeAnchorElem() {
  return ('<ul class="learn-tabs">' +
    '  <li>' +
    '      <a href="/content/tata-capital-moneyfy/en/learn-academy.html?pageType=learn">Learn Academy</a>' +
    '  </li>' +
    '  <li>' +
    '      <a href="/content/tata-capital-moneyfy/en/learn-webinar.html?pageType=webinar">Webinar</a>' +
    '  </li>' +
    '  <li>' +
    '      <a href="/content/tata-capital-moneyfy/en/blog.html?pageType=blogs">Blogs</a>' +
    '  </li>' +
    '  <li>' +
    '      <a href="/content/tata-capital-moneyfy/en/podcasts.html?pageType=podcasts">Podcasts</a>' +
    '  </li>' +
    '</ul>');
}

if (!window.location.href.includes('pageType')) {
  $('.learn-tabs li').click(function () {
    $('.learn-tabs li.active').removeClass('active');
    $(this).addClass('active');
  });
}

if (window.location.href.includes('pageType')) {
  var uls = document.querySelector('.learn-tabs');
  uls.remove();
  var pageTypeElem = document.querySelector('.learning-menu-top');
  pageTypeElem.innerHTML = pageTypeAnchorElem();
  $('.learn-tabs li').click(function () {
    $(this).addClass('active');
  });
  var currentUrl = window.location.href.split('?').pop();
  var newUrl = currentUrl.split('=').pop();
  var li = $('.learn-tabs li')
  li.each(function (ele, ind) {
    if ($(ind).text().toLowerCase().trim().includes(newUrl)) {
      $(ind).addClass('active')
    } else {
      $(ind).removeClass('active')
    }
  });
}
