How to buy digital gold
====
The `How to buy digital gold` can be used to author the various steps on how to buy digital gold on moneyfy digital gold landing page.

## Feature
* This is a multifield component.
* Used to provide steps on how to buy digital gold.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./title` Used for rendering title of the component.
2. `./heading` Used for rendering heading of the component.
3. `./stepImage` Used to select image path for this component.
4. `./stepTitle` Used to enter the title of a card.

## Client Libraries
The component provides a `moneyfy.whyMoneyfy` editor client library category that includes JavaScript and CSS.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5