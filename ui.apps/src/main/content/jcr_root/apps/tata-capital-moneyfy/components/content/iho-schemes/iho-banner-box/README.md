iho-banner-box
====
The `iho-banner-box` component is for adding product box on iho-schemes page.



## Feature
* It is an multifield based component.
* All the various elements of the component such as descriptions, images & buttons are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./ihoHeading` Used for add section heading.
2. `./ihoDescription` used to add description to section.
3. `./ihoTitle` Used to add title of card.
5. `./ihoImg` are used for add image.
6. `./ihoBtn` are used for add button title.
7. `./ihoBtnLink` are used for add button link path.
8. `./ihoBtnTarget` are used to check if the link should open in new tabs.


## Client Libraries
The component provides a `moneyfy.iho-banner-box` editor client library category that includes JavaScript and CSS 


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5