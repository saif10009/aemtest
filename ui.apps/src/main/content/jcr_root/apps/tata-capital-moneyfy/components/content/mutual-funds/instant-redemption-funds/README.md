Fund List Funds
====
The `Fund List Funds` component is for adding funds list on moneyfy funds list page.



## Feature
* It is an dynamic component with data populated using api.



## Edit Dialog Properties
This is an dynamic component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.fund-list-funds` editor client library category that includes JavaScript and CSS.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5