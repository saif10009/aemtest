(function (_global) {
    var analyticsObjCall = (function (jsHelper) {
        var analyticsCall = {}
        document.querySelectorAll('.faq-card .accordian-title').forEach(function (el) {
            el.addEventListener('click', function (e) {
                var faqTitle = e.currentTarget.innerText;
                var activeElement = el.parentElement.parentElement.classList.contains('active')
                if (!activeElement) {
                    faqExpend(faqTitle, userIdObj.userId)
                }
            })
        });

        return jsHelper.freezeObj(analyticsCall);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "analyticsObjCall", analyticsObjCall)
})(this || window || {});