Difference between SIP STP SWP
====
Difference between SIP STP SWP component written in HTL, used to provide different reasons why we should start NPS.

## Feature
* This is a multifield component.
* We have used three tabs to differentiate between SIP STP and SWP.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./headingDescription` Used for rendering title of the component.
2. `./featureType` Used tab which defines type of feature.
3. `./featuresList` Used to create multifield in component.
4. `./heading` Used for rendering heading.
5. `./selectHeight` Used as a dropdown to select the height.
6. `./heading` Used to render the reasons.
7. `./featuresofSip` Used as first tab in which features of SIP are described.
8. `./headingSip` Used to render main heading for first tab.
9. `./featuresofSip` Used to create multifield in component.
10. `./headingSip` Used to describe feature of SIP.
11. `./featuresofSipDescription` Used to provide some information about SIP.
12. `./featuresofStp`Used as second tab in which features of STP are described.
13. `./headingStp`Used to render main heading for second tab.
14. `./featuresofStp` Used to create multifield in component.
15. `./headingStp` Used to describe features of STP.
16. `./featuresofStpDescription`Used to provide some information about STP.
17. `./featuresofSwp`Used as third tab in which benefits of fixed deposits are described.
18. `./headingSwp`Used to render main heading for third tab.
19. `./featuresofSwp` Used to create multifield in component.
20. `./headingSwp` Used to describe feature of SWP.
21. `./featuresofSwpDescription`Used to provide some information about SWP.


## Client Libraries 
The component provides a `moneyfy.difference-between-sip-stp-swp` editor client library category that includes JavaScript and CSS
handling for dialog interaction.The `biz.js` consist all the rendering logic of component on page. The clientlib embeded in `moneyfy.stp-page` clientlib path `/apps/tata-capital-moneyfy/clientlibs/clientlib-stp.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5