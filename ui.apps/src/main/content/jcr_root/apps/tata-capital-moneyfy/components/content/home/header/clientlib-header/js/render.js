/*header js*/
(function (_global) {
  var headerRenderObj = (function (jsHelper) {
    var headerRenderObj = {};
    var count = 0;
    function checkUserLogin(status) {
      if (status) {
        document.querySelector('.header-login').innerHTML = '<button type="button" class="btn-blue btn-yellow radius100" data-loginbtn="loginBtn">Register / Login</button>';
      } else {
        document.querySelector('.header-login').innerHTML = '<div class="custom-login-dropdown">' +
          '    <a href="javascript:void(0)" class="login-profile jsLoginProfile">' +
          '        <span class="login-profile-text" data-userprofile="true"></span>' +
          '    </a>' +
          '    <div class="custom-login-container d-none" data-loginmenu="">' +
          '        <div class="custom-login-cont-inner">' +
          '            <div class="custom-login-content">' +
          '                <div class="user-login-prfile">' +
          '                    <a href="javascript:void(0)" class="login-profile-text" data-userprofile="true"></a>' +
          '                    <div class="user-login-right">' +
          '                        <h6 data-username="true"></h6>' +
          '                        <span class="user-numbers" data-mobilenumber="true"></span>' +
          '                    </div>' +
          '                </div>' +
          '                <ul class="login-content-list" id="headerProfile">' +
          '                    <li data-headeraction="profile">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/profile/profile-overview">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img1.svg" alt="">' +
          '                            </span>' +
          '                            Profile' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="dashboard">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/dashboard">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img2.svg" alt="">' +
          '                            </span>' +
          '                            Dashboard' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="cart">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/cart">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img3.svg" alt="">' +
          '                            </span>' + 
          '                            Cart <span class="cartCount">'+'('+count+')</span>' + 
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="portfolio">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/investment/portfolio">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img3.svg" alt="">' +
          '                            </span>' +
          '                            My Portfolio' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="manage-investment">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/investment/manage-investment">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img4.svg" alt="">' +
          '                            </span>' +
          '                            Manage Investment' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="transactions">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/investment/transactions">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img4.svg" alt="">' +
          '                            </span>' +
          '                            My Transactions' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="watchlist">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/watchlist">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img5.svg" alt="">' +
          '                            </span>' +
          '                            Watchlist' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="assistance">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/profile/help">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img6.svg" alt="">' +
          '                            </span>' +
          '                            Help and Assistance' +
          '                        </a>' +
          '                    </li>' +
          '                    <li data-headeraction="mandateBook">' +
          '                        <a href="'+ appConfig.jocataDomain +'/application/mandate-book?action=mandate-book">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img8.svg" alt="">' +
          '                            </span>' +
          '                            Mandate Book' +
          '                        </a>' +
          '                    </li>' +
          '                </ul>' +
          '                <ul class="login-content-list logout-content">' +
          '                    <li data-headeraction="logout">' +
          '                        <a href="javascript:void(0)">' +
          '                            <span class="login-icon">' +
          '                                <img src="/content/dam/tata-capital-moneyfy/home-page/login/login-img7.svg" alt="">' +
          '                            </span>' +
          '                            Log out' +
          '                        </a>' +
          '                    </li>' +
          '                </ul>' +
          '            </div>' +
          '            <div class="mob-header-bottom d-block d-md-none">' +
          '                <a href="javascript:void(0)">' +
          '                    <img src="img/cell.svg" alt="">' +
          '                    Contact Us <span> 1860 267 60 60</span>' +
          '                </a>' +
          '            </div>' +
          '        </div>' +
          '' +
          '    </div>' +
          '</div>';
      }
    }


   /* function renderLoansCardsPopupOverview(){
      var popEl = document.getElementById('submenuLoansCards_popup');
            popEl.innerHTML = '<div class="popover-modal-dialog popover-modal-dialog-centered popover-modal-md3">'+
            '            <div class="popover-modal-content radius10">'+
            '                <div class="popover-modal-body">'+
            '                    <button data-dismiss="popover-modal" class="popover-modal-close js-contributeModalClose"><i'+
            '                            class="icon-close"></i></button>'+
            '                    <div class="modal-body-inner3">'+
            '                        <div class="terms-text text-center js-termsBox" id ="submenuLoansCards_popup_href">'+
            '                            <p id= "term-p">' + '<b>' +
            '                            Home Loans are brought to you by Tata Capital Housing Finance Limited(TCHFL).All other Loans' +
            '                            are brought to you by Tata Capital Financial Services Limited (TCFSL).All loans are at the' +
            '                            sole discretion of TCHFL and TCFSL respectively.Terms and conditions apply.</b></p>' +
            '                        </div>'+
            '                    </div>'+
            '                </div>'+
            '            </div>'+
            '        </div>';
            }
  
    function renderLoansCardsPopup(){
      var popEl = document.getElementById('submenuLoansCards_popup');
            popEl.innerHTML = '<div class="popover-modal-dialog popover-modal-dialog-centered popover-modal-md3">'+
            '            <div class="popover-modal-content radius10">'+
            '                <div class="popover-modal-body">'+
            '                    <button data-dismiss="popover-modal" class="popover-modal-close js-contributeModalClose"><i'+
            '                            class="icon-close"></i></button>'+
            '                    <div class="modal-body-inner3">'+
            '                        <div class="terms-text text-center js-termsBox" id ="submenuLoansCards_popup_href">'+
            '                            <p id= "term-p">' + '<b>'+
            '                            Home Loans are brought to you by Tata Capital Housing Finance Limited(TCHFL).All' + 
            '                            other Loans are brought to you by Tata Capital Financial Services Limited (TCFSL).All ' +
            '                            loans are at the sole discretion of TCHFL and TCFSL respectively.Terms and conditions' +
            '                            apply.</b></p>' +
            '                        </div>'+
            '                    </div>'+
            '                </div>'+
            '            </div>'+
            '        </div>';
          }
          function renderLoansCreditCardsPopup(){
            var popE2 = document.querySelector('[data-submenulistname="Credit Card"]')
                  popE2.innerHTML = '<div class="popover-modal-dialog popover-modal-dialog-centered popover-modal-md3">'+
                  '            <div class="popover-modal-content radius10">'+
                  '                <div class="popover-modal-body">'+
                  '                    <button data-dismiss="popover-modal" class="popover-modal-close js-contributeModalClose"><i'+
                  '                            class="icon-close"></i></button>'+
                  '                    <div class="modal-body-inner3">'+
                  '                        <div class="terms-text text-center js-termsBox" id ="submenuLoansCards_popup_href">'+
                  '                            <p id= "term-p">' + '<b>'+
                  '                           Credit Card offerings are brought to you by third-party entities at their sole discretion. Tata Securities Limited (TSL)/ Moneyfy is merely acting as a Referrer and is only displaying the features of the Credit Cards on its platform and such display must not be construed as an offer or advice to transact in such Products. The mentioned details regarding the Credit Cards are as communicated by such third-party entities and TSL/ Moneyfy does not make any claims, warranties, or representations, express or implied regarding the Credit Cards or features thereof and shall assume no responsibility or liability in relation to the same or otherwise with respect to the Credit Cards whatsoever. Terms and conditions apply.</b></p>' +
                  '                        </div>'+
                  '                    </div>'+
                  '                </div>'+
                  '            </div>'+
                  '        </div>';
                }*/
    function headerSearchRenderFn(data, inputData, isWhiteSpace) {
      var searchUl = document.querySelector('[data-atr="search-list"]');
      var resultNotFound = document.querySelector('.no-result-found');
      searchUl.innerHTML = "";
      resultNotFound.innerHTML = "";
      if (data != null && data.results != undefined) {
        if (data.results.length != 0 && !isWhiteSpace) {
          var htmlRenderStr = "";
          data.results.forEach(function (value) {
            var pageTitle = value.pageTitle;
            var pageTitleCompare = value.pageTitle.toLowerCase();
            var keyword = pageTitle.substring(pageTitleCompare.indexOf(inputData), pageTitleCompare.indexOf(inputData) + inputData.length);
            var keywordBefore = pageTitle.substring(0, pageTitleCompare.indexOf(inputData));
            var keywordAfter = pageTitle.substring(pageTitleCompare.indexOf(inputData) + inputData.length, pageTitleCompare.length);
            htmlRenderStr += '<li>' +
              '<a target="_blank" href="' + value.pagePath + '">' + keywordBefore + '<strong>' + keyword + '</strong>' + keywordAfter + '<span class="closed-btn">View</span>' + '</a>' +
              '</li>';

          });
          searchUl.innerHTML = '<ul class="list-search-info filterList-mCustomScrollbar" >' + htmlRenderStr +
            '</ul>';
        } else {
          resultNotFound.innerHTML = '<p class="text-result-error">' +
            '<img src="/content/dam/tata-capital-moneyfy/home-page/error.svg" alt="" /> Sorry, we could not find a page related to "' + inputData +
            '"</p>';
        }
      } else {
        resultNotFound.innerHTML = '<p class="text-result-error">' +
          '<img src="/content/dam/tata-capital-moneyfy/home-page/error.svg" alt="" /> Sorry, we could not find a page related to "' + inputData +
          '"</p>';
      }
    }
    headerRenderObj.checkUserLogin = checkUserLogin;
    headerRenderObj.headerSearchRenderFn = headerSearchRenderFn;
    headerRenderObj.count = count;
    /* headerRenderObj.renderLoansCardsPopup = renderLoansCardsPopup;
    // headerRenderObj.renderLoansCardsPopupOverview = renderLoansCardsPopupOverview;
    headerRenderObj.renderLoansCreditCardsPopup = renderLoansCreditCardsPopup;*/

    return jsHelper.freezeObj(headerRenderObj);
  })(jsHelper);

  _global.jsHelper.defineReadOnlyObjProp(_global, "headerRenderObj", headerRenderObj);
})(this || window || {});
  /*header js*/