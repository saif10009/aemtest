Purpose pillar box
====
This is a `Purpose pillar box` component.

## Feature
* This is a multifield component.
* All the various elements of the component such as images, headings, title and button txt is authorable.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./heading ./imageHeading` Used for rendering headings in the component.
2. `./imageDescription` Used for rendering description of the component.
3. `./image` Used for rendering image on page.
4. `./buttonLink` Used to render redirection link.
5. `./buttonTarget` Enable open redirection link in a new tab.
6. `./button` Used to rendering the name of the button.
7. `./purposePillarListMultifield` Used to create multifield in component.



## Client Libraries
The component provides a `moneyfy.about-purpose-pillar-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5