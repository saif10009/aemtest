/*smallcase banner slider js*/
    (function (_global) {
        var smallcaseBannerBizObj = (function (jsHelper) {
            var smallcaseBannerObj = {}
            document.addEventListener('DOMContentLoaded', function () {
                if ($('.banner-image').length > 1) {
                    smallcaseBannerSlick()
                }
                $('[data-adobe-action]').click(function (event) {
                    event.preventDefault();
                    var ctaTitle = $(this).parents().find('[data-banner-heading]').data('banner-heading');
                    
                    if($(this).data('adobe-action')=="smallCasePopup"){
                        var ctaText = $(this).text().trim();
                        allCTAInteraction(ctaText,ctaTitle, 'smallcase-direct-equity-banner', userIdObj.userId)
                    }else{
                        var ctaText = $(this).text().trim();
                        allCTAInteraction(ctaText,ctaTitle, 'smallcase-direct-equity-banner', userIdObj.userId);
                        location.href = $(this).attr('href');
                    }

                  })
                
                $('[data-smallcase-action="smallcasepopup"]').click(function (event) {
                    event.preventDefault();
                    var anchorLink = $(this).attr('href')
                    if (!headerBizObj.getUserObj().isLoggedIn) {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = appConfig.jocataDomain + anchorLink;
                        });
                    } else {
                        location.href = appConfig.jocataDomain + anchorLink;
                    }
                })
                
            })
            function smallcaseBannerSlick() {
                $('#jsBannerSlider').slick({
                    dots: true,
                    infinite: false,
                    speed: 300,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    autoplay: false,
                });
                setTimeout(function () {
                    if ($('.banner-slider').hasClass('slick-initialized')) {
                        $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                        $('.slick-slider').parents('.banner-box.mutual-fund-banner').removeClass('banner-heightinner');
                    }
                }, 2000);
            }
            return jsHelper.freezeObj(smallcaseBannerObj);
        })(jsHelper)
        _global.jsHelper.defineReadOnlyObjProp(_global, "smallcaseBannerBizObj", smallcaseBannerBizObj)
    })(this || window || {});
  /*smallcase banner Slider js end*/