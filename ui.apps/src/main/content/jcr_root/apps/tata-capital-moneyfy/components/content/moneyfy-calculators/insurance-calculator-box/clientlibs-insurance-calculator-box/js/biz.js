/*insurance calculator biz js start*/
(function (_global) {
    var insuranceCalculatorBizObj = (function (jsHelper) {
        var insuranceCalculatorObj = {}
        var insureObj = {
            MonthlyIncomeLimit: document.getElementById("MonthlyIncomeLimit"),
            currentAgeLimit: document.getElementById("currentAgeLimit"),
            retirementAgeLimit: document.getElementById("retirementAgeLimit"),
            currentAgeInput:document.getElementById("currentAge"),
            retirementAgeInput: document.getElementById("retirementAge"),
        }
        document.addEventListener('DOMContentLoaded', function () {
            insuranceCalculatorInitializer();
            insuranceCalculatorEvents();
            insuranceCalculatorCal();
        })
        function insuranceCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
            // Range slider end
        }

        function insuranceCalculatorEvents() {
            
            /* annual income keyup event */
            document.getElementById('annualIncome').addEventListener('keyup', function () {
                var insuranceErrorMsg = document.getElementById("insuranceErrorMsg");
                if (parseFloat(this.value.replace(/,/g, '')) > 100000000000) {
                    insuranceErrorMsg.innerHTML = '';
                    insuranceErrorMsg.innerHTML = 'Amount should not greater than ₹100000000000';
                }
                else {
                    insuranceErrorMsg.innerHTML = '';
                }
                insuranceCalculatorCal();
            })

            /* slider change event */
            if ($(".js-calculatorRangeSlider").is(":visible")) {

                $(".js-calculatorRangeSlider").on("input", function () {
                    insuranceCalculatorCal();
                });
            }

            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            document.getElementById("monthlyIncome").addEventListener('keyup', function () {
                // if (parseFloat(this.value.replace(/,/g, '')) > 100000) { requirement not cleared.
                //     tenureObj.emiErrorMsg.innerHTML = '';
                //     tenureObj.emiErrorMsg.innerHTML = 'Amount should not greater than ₹500000000';
                // } else 
                if (parseFloat(this.value.replace(/,/g, '')) < 1000) {
                    insureObj.MonthlyIncomeLimit.innerHTML = '';
                    insureObj.MonthlyIncomeLimit.innerHTML = 'Amount should not less than ₹1000';
                } else {
                    insureObj.MonthlyIncomeLimit.innerHTML = '';
                }
                insuranceCalculatorCal();
            });

            insureObj.currentAgeInput.addEventListener('keyup', function () {
                if (parseFloat(this.value.replace(/,/g, '')) >= insureObj.retirementAgeInput.value) {
                    insureObj.currentAgeLimit.innerHTML = '';
                    insureObj.currentAgeLimit.innerHTML = 'Your current age should not greater than retirement age';
                } else if (parseFloat(this.value.replace(/,/g, '')) > 80) {
                    insureObj.currentAgeLimit.innerHTML = '';
                    insureObj.currentAgeLimit.innerHTML = 'Your current age Should be 80 or below';
                } else {
                    insureObj.currentAgeLimit.innerHTML = '';
                }
                insuranceCalculatorCal();
            })

            insureObj.retirementAgeInput.addEventListener('keyup', function () {
                if (parseFloat(this.value.replace(/,/g, '')) <= insureObj.currentAgeInput.value) {
                    insureObj.retirementAgeLimit.innerHTML = '';
                    insureObj.retirementAgeLimit.innerHTML = 'Your retirement age should greater than current age';
                } else if (parseFloat(this.value.replace(/,/g, '')) > 80) {
                    insureObj.retirementAgeLimit.innerHTML = '';
                    insureObj.retirementAgeLimit.innerHTML = 'Your retirement age Should be 80 or below';
                } else {
                    insureObj.retirementAgeLimit.innerHTML = '';
                }
                insuranceCalculatorCal();
            })
        }

        function insuranceCalculatorCal() {
            var inputObj = {
                calculator: "Insurance",
                currentAge: Number(insureObj.currentAgeInput.value),
                retirementAge: Number(insureObj.retirementAgeInput.value),
                monthlyIncome: parseFloat(document.getElementById("monthlyIncome").value.replace(/,/g, '')),
                expectedIncome: Number(document.getElementById("expectedIncome").value) / 100,
                roi: Number(document.getElementById("roi").value) / 100,
            }
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            myWorker.postMessage(inputObj)
            myWorker.onmessage = function (e) {
                insuranceCalculatorRenderObj.renderInsuranceCalculation(e, inputObj);
                myWorker.terminate();
            }
        }
        return jsHelper.freezeObj(insuranceCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "insuranceCalculatorBizObj", insuranceCalculatorBizObj)
})(this || window || {});
  /*insurance calculator biz js end*/