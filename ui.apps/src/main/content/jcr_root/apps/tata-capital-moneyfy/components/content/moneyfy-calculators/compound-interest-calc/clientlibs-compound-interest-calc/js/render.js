/*investment calculator render js start*/
(function (_global) {
    var investmentCalculatorRenderFn = (function (jsHelper) {
        var investmentCalculatorRenderObj = {}

        function renderInvestmentSipCalculation(e, sipInputbj) {
            var sipFvVal = document.getElementById("sipFvVal");
            var sipTotalInvestment = document.getElementById("sipTotalInvestment");
            var sipTotalEarned = document.getElementById("sipTotalEarned");
            var sipResultText = document.getElementById('sip-starts-investing-text');
            if ((isNaN(sipInputbj.sipAmount)) || (sipInputbj.sipAmount < 500) || (sipInputbj.sipAmount > 1000000)) {
                sipFvVal.innerHTML = '₹ 0';
                sipTotalInvestment.innerHTML = '₹ 0';
                sipTotalEarned.innerHTML = '₹ 0';
                sipResultText.innerHTML = "If you invest ₹0 per month, in " + e.data.sipMonths + " you will get";
                sipInputbj.sipHighchartReact(0, 0, 0)
            }
            else {
                sipFvVal.innerHTML =
                    "₹ " + Math.round(e.data.sip).toLocaleString('en-IN');
                sipTotalInvestment.innerHTML = "₹ " + e.data.sipInvestment.toLocaleString('en-IN');
                sipTotalEarned.innerHTML = "₹ " + Math.round(e.data.sip - e.data.sipInvestment).toLocaleString('en-IN');
                sipResultText.innerHTML = "If you invest ₹ " + sipInputbj.sipAmount.toLocaleString('en-IN') + " per month, in " + e.data.sipMonths + " you will get";
                sipInputbj.sipHighchartReact((e.data.sipInvestment), (e.data.sip - e.data.sipInvestment), 100)
            }
        }
        function renderInvestmentLumsumCalculation(e, lumsumInputObj) {
            var lumsumFvVal = document.getElementById("lumsumFvVal");
            var lumsunTotalInvestment = document.getElementById("lumsunTotalInvestment");
            var lumsumTotalEarned = document.getElementById("lumsumTotalEarned");
            var lumsumResultText = document.getElementById('lumsum-starts-investing-text');
            if ((isNaN(lumsumInputObj.lumsumAmount)) || (lumsumInputObj.lumsumAmount < 500) || (lumsumInputObj.lumsumAmount > 10000000)) {
                lumsumFvVal.innerHTML = '₹ 0';
                lumsunTotalInvestment.innerHTML = '₹ 0';
                lumsumTotalEarned.innerHTML = '₹ 0';
                lumsumResultText.innerHTML = "If you invest ₹0 today, in " + e.data.lumsumMonths + " you will get";
                lumsumInputObj.lumsumHighchartReact(0, 0, 0)
            }
            else {
                lumsumFvVal.innerHTML =
                    "₹ " + Math.round(e.data.lumsum).toLocaleString('en-IN');
                lumsunTotalInvestment.innerHTML = "₹ " + lumsumInputObj.lumsumAmount.toLocaleString('en-IN');
                lumsumTotalEarned.innerHTML = "₹ " + Math.round(e.data.lumsum - lumsumInputObj.lumsumAmount).toLocaleString('en-IN');
                lumsumResultText.innerHTML = "If you invest ₹ " + (lumsumInputObj.lumsumAmount).toLocaleString('en-IN') + " today, in " + e.data.lumsumMonths + " you will get";
                lumsumInputObj.lumsumHighchartReact(lumsumInputObj.lumsumAmount, Math.round(e.data.lumsum - lumsumInputObj.lumsumAmount), 100)
            }
        }

        investmentCalculatorRenderObj.renderInvestmentSipCalculation = renderInvestmentSipCalculation;
        investmentCalculatorRenderObj.renderInvestmentLumsumCalculation = renderInvestmentLumsumCalculation;
        return jsHelper.freezeObj(investmentCalculatorRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "investmentCalculatorRenderObj", investmentCalculatorRenderFn)
})(this || window || {});
  /*investment calculator render js end*/