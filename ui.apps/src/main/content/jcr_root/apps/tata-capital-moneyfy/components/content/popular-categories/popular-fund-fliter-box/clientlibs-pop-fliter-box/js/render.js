/*amc-detail-fund-list render.js start*/
(function (_global) {
    var popularCategoryFundRenderFn = (function (jsHelper) {
        var popularCategoryFundRenderObj = {}
        function renderFilteredFunds(sortedArray) {
            document.querySelector('.popCatFundList').innerHTML = '';
            document.querySelector('[data-attr="fund-list"]').innerHTML = sortedArray.length;
            if (sortedArray.length > 0) {
                var htmlRenderStr = '';
                sortedArray.forEach(function (fund, index) {
                    var path = '/content/tata-capital-moneyfy/en/mutual-funds/';
                    var fundPath = fund.amcName + '/' + fund.schemeDetails.name;
                    var pagePath = path.concat(fundPath.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.html'));
                    pagePath = pagePath.replace(/[-]+/g, '-');
                    var iconPath = '/content/dam/tata-capital-moneyfy/amc-icons';
                    iconPath = iconPath + '/' + fund.amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-") + '.png';
                    htmlRenderStr += '<div class="fund-list-li js-similarMFcard" data-schemeId="' + fund.schemeDetails.id + '">' +
                        '                    <div class="card-div similar-mutual-funds-items">' +
                        '                        <div class="top-row">' +
                        '                            <div class="info-rating-wrap">' +
                        '                                <div class="name-return-wrap">' +
                        '                                    <div class="img-name-wrap">' +
                        '                                        <div class="img-wrap">' +
                        '                                            <img src="' + (amcIconList.indexOf(iconPath) != -1 ? iconPath : '/content/dam/tata-capital-moneyfy/amc-icons/moneyfy-default-fund-icon.png') + '"' +
                        '                                                alt="" />' +
                        '                                        </div>' +
                        '                                        <div class="name-rating-wrap">' +
                        '                                            <a href="'+pagePath+'" class="mf-fund-name" data-viewdetailbtn="viewDetailBtn"><h6 class="fund-name two-lines" data-schemename="'+fund.schemeDetails.name+'">' + fund.schemeDetails.name + '</h6></a>' +
                        '                                            <div class="rating-wrap d-none d-md-flex">' +
                        '                                                <p>' +
                        '                                                    <span class="rate">' + (fund.ratings.morningStar == "" ? '-' : fund.ratings.morningStar) + ' <span class="icon-star"></span></span>' +
                        '                                                    Morning Star' +
                        '                                                </p>' +
                        '                                                <p>' +
                        '                                                    <span class="rate">' + (fund.ratings.valueResearch == "" ? '-' : fund.ratings.valueResearch) + ' <span class="icon-star"></span></span>' +
                        '                                                    Value Research' +
                        '                                                </p>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '' +
                        '                                    <div class="return-info d-md-none">' +
                        '                                        <div class="form-select2">' +
                        '                                           <select class="single-select2 returnsVal" data-placeholder="-- Select --">' +
                        '                                               ' + '<option value=' + isNullOrZeroCheck(fund.cagrValues['1m']) + '>' + '1M Returns' + '</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1y']) + '>' + '1Y Returns' + '</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['3y']) + '>' + '3Y Returns' + '</option>' +
                        '                                            </select>' +
                        '                                        </div>' +
                        '                                        <h6 class="trend-info' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? Number(fund.cagrValues['1m']) > 0 ? ' up-trend"' : ' down-trend"' : '-') + 'data-rate="rate"' + '>' +
                        '                                            <span class=' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? Number(fund.cagrValues['1m']) > 0 ? "icon-angle-up" : "icon-angle-down" : '-') + '></span> ' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%' +
                        '                                        </h6>' +
                        '                                    </div>' +
                        '' +
                        '                                </div>' +
                        '                                <div class="rating-wrap d-md-none">' +
                        '                                    <p>' +
                        '                                        <span class="rate">' + fund.ratings.morningStar + ' <span class="icon-star"></span></span>' +
                        '                                        Morning Star' +
                        '                                    </p>' +
                        '                                    <p>' +
                        '                                        <span class="rate">' + fund.ratings.valueResearch + ' <span class="icon-star"></span></span>' +
                        '                                        Value Research' +
                        '                                    </p>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                            <div class="returs-box d-none d-md-flex">' +
                        '                                <div class="returs-wrap">' +
                        '                                    <div class="return-item">' +
                        '                                        <p class="label-text">NAV</p>' +
                        '                                        <h6>' + (Number(fund.nav).toFixed(2) == 'NaN' ? '-' : Number(fund.nav).toFixed(2)) + '</h6>' +
                        '                                    </div>' +
                        '                                    <div class="return-item">' +
                        '                                        <p class="label-text">1M Returns</p>' +
                        '                                        <h6>' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%</h6>' +
                        '                                    </div>' +
                        '                                    <div class="return-item">' +
                        '                                        <p class="label-text">1Y Returns</p>' +
                        '                                        <h6>' + isNullOrZeroCheck(fund.cagrValues['1y']) + '%</h6>' +
                        '                                    </div>' +
                        '                                    <div class="return-item">' +
                        '                                        <p class="label-text">3Y Returns</p>' +
                        '                                        <h6>' + isNullOrZeroCheck(fund.cagrValues['3y']) + '%</h6>' +
                        '                                    </div>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                            <div class="invest-more-wrap d-none d-md-block">' +
                        '                                <a href="javascript:void(0);" class="btn-blue radius100 invest-now-btn" data-investnow="true">invest now</a>' +
                        '                                <a href="javascript:void(0)" class="btn-view-all js-viewMoreToggleBtn"' +
                        '                                    data-btn="mf-dektop-view-more">' +
                        '                                    <span class="text-wrap">More</span> <span class="icon-arrow-arrow"></span>' +
                        '                                </a>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                        <div class="desktop-view-more" data-toggleCard="mf-dektop-view-more">' +
                        '                            <div class="returs-wrap">' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Min. Investment</p>' +
                        '                                    <h6>Rs.' + (fund.minSipAmt != undefined ? fund.minSipAmt :  fund.minLumpsumAmt)+ '</h6>' +
                        '                                </div>' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Fund Size</p>' +
                        '                                    <h6>Rs.' + (fund.fundSize != '' ? fund.fundSize : '-') + ' Cr</h6>' +
                        '                                </div>' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Fund Manager</p>' +
                        '                                    <h6>' + (fund.fundManagerName != '' ? fund.fundManagerName : '-') + '</h6>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                        <div class="bottom-row">' +
                        '                            <div class="tag-more-wrap">' +
                        '                                <div class="tag-wrap">' +
                        '                                    <span class="tag-item blue-tag k" data-risktype="'+fund.schemeDetails.riskType+'">' + (fund.schemeDetails.riskType != '' ? fund.schemeDetails.riskType : '-') + '</span>' +
                        '                                    <span class="tag-item violent-tag">' + (fund.schemeDetails.optName != '' ? fund.schemeDetails.optName : '-') + '</span>' +
                        '                                    <span class="tag-item green-tag" data-catname="'+fund.schemeDetails.catName+'">' + (fund.schemeDetails.catName != '' ? fund.schemeDetails.catName : '-') + '</span>' +
                        '                                </div>' +
                        '                                <a href="javascript:void(0)" class="btn-view-all js-viewMoreToggleBtn  d-md-none"' +
                        '                                    data-btn="mf-view-more">' +
                        '                                    <span class="text-wrap">More</span> <span class="icon-arrow-arrow"></span>' +
                        '                                </a>' +
                        '                            </div>' +
                        '                            <div class="button-wrap d-none d-md-flex">' +
                        '                                <div class="watchlist-compare-btn">' +
                        '                                    <a href="javascript:void(0);" data-addwatchlist="watchListBtn" class="add-watchlist-wrap js-addWatchlist '+ (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'addedWatchlist' : '') +'">' +
                        '                                        <span class="watchlist-icon"><i class="icon-binocular"></i></span>' +
                        '                                        <span class="js-toggleAdd wishlist-text">'+ (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'Added to watchlist' : 'ADD to watchlist') +'</span>' +
                        '                                    </a>' +
                        '                                    <div class="custom-checkbox add-compare-wrap">' +
                        '                                        <label class="js-addCompare">' +
                        '                                            <input type="checkbox" />' +
                        '                                            <span class="checkbox-wrap"></span>' +
                        '                                            ADD TO compare' +
                        '                                        </label>' +
                        '                                    </div>' +
                        '                                    <a href="' + pagePath + '" class="btn-view-all" data-viewdetailbtn="viewDetailBtn">view details<span' +
                        '                                            class="icon-arrow-right"></span></a>' +
                        '                                </div>' +
                        '                                <div class="view-btn-wrap">' +
                        '' +
                        '                                </div>' +
                        '                            </div>' +
                        '' +
                        '                        </div>' +
                        '' +
                        '                        <div class="view-more-wrap d-md-none" data-toggleCard="mf-view-more">' +
                        '                            <div class="returs-wrap">' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">NAV</p>' +
                        '                                    <h6>' + (fund.nav != undefined ? fund.nav : '-') + '</h6>' +
                        '                                </div>' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Min Investment</p>' +
                        '                                    <h6>₹' + (fund.minSipAmt != undefined ? fund.minSipAmt :  fund.minLumpsumAmt) + '</h6>' +
                        '                                </div>' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Fund Size</p>' +
                        '                                    <h6>₹' + (fund.fundSize != undefined ? fund.fundSize : '-') + ' cr</h6>' +
                        '                                </div>' +
                        '                                <div class="return-item">' +
                        '                                    <p class="label-text">Fund Manager</p>' +
                        '                                    <h6>' + (fund.fundManagerName != undefined ? fund.fundManagerName : '-') + '</h6>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                            <div class="watchlist-compare-btn">' +
                        '                                <a href="javascript:void(0);" data-addwatchlist="watchListBtn" class="add-watchlist-wrap js-addWatchlist '+ (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'addedWatchlist' : '') +'">' +
                        '                                        <span class="watchlist-icon"><i class="icon-binocular"></i></span>' +
                        '                                        <span class="js-toggleAdd wishlist-text">'+ (watchListSchemeIds.indexOf(Number(fund.schemeDetails.id)) != -1 ? 'Added to watchlist' : 'ADD to watchlist') +'</span>' +
                        '                                </a>' +
                        '                                <div class="custom-checkbox add-compare-wrap">' +
                        '                                    <label class="js-addCompare">' +
                        '                                        <input type="checkbox">' +
                        '                                        <span class="checkbox-wrap"></span>' +
                        '                                        ADD TO compare' +
                        '                                    </label>' +
                        '                                </div>' +
                        '                            </div>' +
                        '                            <div class="invest-detail-wrap">' +
                        '                                <a href="javascript:void(0);" class="btn-blue radius100 invest-now-btn" data-investnow="true">invest now</a>' +
                        '                                <a href="' + pagePath + '" class="btn-view-all" data-viewdetailbtn="viewDetailBtn">view details<span' +
                        '                                        class="icon-arrow-right"></span></a>' +
                        '                            </div>' +
                        '                        </div>' +
                        '                    </div>' +
                        '                </div>';
                });
                document.querySelector('.popCatFundList').innerHTML = htmlRenderStr;
                document.querySelectorAll('.popCatFundList .rating-wrap .rate').forEach(function(rating){
                    if(Number(rating.innerText.trim()) == 0 || rating.innerText.trim() == '-' ) {
                        rating.parentElement.remove();
                    }
                  });
            } else {
                //document.querySelector('#fundListSize').innerHTML = sortedArray.length;
                document.querySelector('.fund-list-result-two').classList.add('d-none');
                document.querySelector('.fund-list-result-right').classList.add('d-none');
                Array.from(document.querySelectorAll('.pagination-box')).forEach(function (element) { element.classList.add('d-none'); });
                document.querySelector('.popCatFundList').innerHTML = '<p style="margin-left: 20px;font-weight: 500;font-size: 15px;">There are no funds in this category.</p>';
            }
        }

        function renderWatchListPortfolioFunds(renderArray, addFromType) {
            var fundRenderStr = '';
            var selector = addFromType == 'watchList' ? '#addFromWishList' : '#addFromPortfolio';
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
            if (renderArray.length > 0) {
                renderArray.forEach(function (fund) {
                    var iconPath = '/content/dam/tata-capital-moneyfy/amc-icons/';
                    iconPath = iconPath + fund.amcName.toLowerCase().replace(/[&\\\#,+()$~%.'":*?@<>{}]/g, '-').replace(/\s/g, "-").concat('.png');
                    fundRenderStr += '<div class="fund-list-li" data-schemeid="' + fund.schemeDetails.id + '">' +
                        '                                <div class="card-div similar-mutual-funds-items">' +
                        '                                    <div class="top-row">' +
                        '                                        <div class="checkbox-name-wrap">' +
                        '                                            <div class="custom-checkbox">' +
                        '                                                <label class="js-addCompare">' +
                        '                                                    <input type="checkbox">' +
                        '                                                    <span class="checkbox-wrap"></span>' +
                        '                                                </label>' +
                        '                                            </div>' +
                        '                                            <div class="info-rating-wrap">' +
                        '                                                <div class="name-return-wrap">' +
                        '                                                    <div class="img-name-wrap">' +
                        '                                                        <div class="img-wrap">' +
                        '                                                            <img src="'+ iconPath +'" alt="" />' +
                        '                                                        </div>' +
                        '                                                        <div class="name-rating-wrap">' +
                        '                                                            <h6 class="fund-name two-lines" data-schemename="' + fund.schemeDetails.name + '">' + fund.schemeDetails.name + '</h6>' +
                        '                                                            <div class="rating-wrap d-none d-md-flex">' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Morning Star' +
                        '                                                                </p>' +
                        '                                                                <p>' +
                        '                                                                    <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span' +
                        '                                                                            class="icon-star"></span></span>' +
                        '                                                                    Value Research' +
                        '                                                                </p>' +
                        '                                                            </div>' +
                        '                                                        </div>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="returs-box d-none d-lg-block">' +
                        '                                            <div class="d-flex">' +
                        '                                                <div class="returs-wrap">' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">NAV</p>' +
                        '                                                        <h6>' + popularfilterBizObj.reduceDigit(fund.nav, 2) + '</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1M Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1m']) != "-" ? popularfilterBizObj.reduceDigit(fund.cagrValues['1m'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">1Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['1y']) != "-" ? popularfilterBizObj.reduceDigit(fund.cagrValues['1y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                    <div class="return-item">' +
                        '                                                        <p class="label-text">3Y Returns</p>' +
                        '                                                        <h6>' + (isNullOrZeroCheck(fund.cagrValues['3y']) != "-" ? popularfilterBizObj.reduceDigit(fund.cagrValues['3y'], 2) : '-') + '%</h6>' +
                        '                                                    </div>' +
                        '                                                </div>' +
                        '                                            </div>' +
                        '                                        </div>' +
                        '                                        <div class="return-info d-lg-none">' +
                        '                                            <div class="form-select2">' +
                        '                                                <select class="single-select2 portfolioWatchListReturns" data-placeholder="-- Select --">' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1m']) + '>1M Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['1y']) + '>1Y Returns</option>' +
                        '                                                    <option value=' + isNullOrZeroCheck(fund.cagrValues['3y']) + '>3Y Returns</option>' +
                        '                                                </select>' +
                        '                                            </div>' +
                        '                                            <h6 class="trend-info text-right' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? ' up-trend"' : ' down-trend"') : '-') + 'data-rate="rate"' + '>' +
                        '                                            <span class=' + (isNullOrZeroCheck(fund.cagrValues['1m']) != '-' ? (Number(fund.cagrValues['1m']) > 0 ? "icon-angle-up" : "icon-angle-down") : '-') + '></span> ' + isNullOrZeroCheck(fund.cagrValues['1m']) + '%' +
                        '                                        </h6>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                    <div class="rating-mob">' +
                        '                                        <div class="rating-wrap d-md-none">' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Morning Star' +
                        '                                            </p>' +
                        '                                            <p>' +
                        '                                                <span class="rate">' + (isNullOrEmptyCheck(fund.ratings.morningStar) != '-' ? fund.ratings.morningStar : '-') + ' <span class="icon-star"></span></span>' +
                        '                                                Value Research' +
                        '                                            </p>' +
                        '                                        </div>' +
                        '                                    </div>' +
                        '                                </div>' +
                        '                            </div>';
                });    
            } else {
                fundRenderStr = '<p class="no-found">No funds found.</p>'
            }
            document.querySelector(selector + ' .wishlist-fund-list').innerHTML = fundRenderStr;
        }

        popularCategoryFundRenderObj.renderFilteredFunds = renderFilteredFunds;
        popularCategoryFundRenderObj.renderWatchListPortfolioFunds = renderWatchListPortfolioFunds;
        return jsHelper.freezeObj(popularCategoryFundRenderObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "popularCategoryFundRenderObj", popularCategoryFundRenderFn)
})(this || window || {});
/*amc-detail-fund-list render.js end*/

