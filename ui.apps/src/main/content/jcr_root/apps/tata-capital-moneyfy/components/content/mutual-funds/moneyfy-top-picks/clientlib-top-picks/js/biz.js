/*biz.js*/
/*top picks js start*/
(function (_global) {
    var topPicksBizFn = (function (jsHelper) {
        var topPicksObj = {};
        document.addEventListener('DOMContentLoaded', function () {
            var topPicsList = JSON.parse(topList)
            console.log('topList', topPicsList)
            renderTopPicks(topPicsList);
            topPicksClick();
        });

        function adobeAnalytics() {
            $('[data-topickstablist] a').click(function (event) {
                event.preventDefault();
                var tabTitle = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            tabInteractionNLI(tabTitle, "moneyfy-top-picks", userId);    
                        }else{
                            tabInteraction(tabTitle, "moneyfy-top-picks", userId);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            tabInteractionNLI(tabTitle, "moneyfy-top-picks", "anonymous user");    
                        }else{
                            tabInteraction(tabTitle, "moneyfy-top-picks", "anonymous user");
                        }
                    }
                } catch (err) {
                    console.log(err);
                }
            });

            $('[data-toppicksexplore]').click(function (event) {
                event.preventDefault();
                var fundName = $(this).parents('.top-pickup-card').find('h5').text().trim();
                var fundRiskCat = $(this).parents('.top-pickup-card').find('.custom-tooltips-inner').text().trim();
                var fundType = $('[data-topickstablist]').find('[class="active"]').text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            fundViewDetailsNLI(fundType, fundName, fundRiskCat, userId);    
                        }else{
                            fundViewDetails(fundType, fundName, fundRiskCat, userId);
                        }    
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            fundViewDetailsNLI(fundType, fundName, fundRiskCat, "anonymous user");    
                        }else{
                            fundViewDetails(fundType, fundName, fundRiskCat, "anonymous user");
                        } 
                    }
                } catch (err) {
                    console.log(err);
                }
                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    window.open($(this).attr('href'))
                }
            })
        }

        function renderTopPicks(topPicsList) {
            var topPicks = { "Equity": [], "Debt": [], "Hybrid": [] };
            topPicsList.forEach(function (fund) {
                if (fund.catName.toLowerCase() == "equity") {
                    topPicks["Equity"].push(fund);
                } else if (fund.catName.toLowerCase() == "debt") {
                    topPicks["Debt"].push(fund);
                } else if (fund.catName.toLowerCase() == "hybrid") {
                    topPicks["Hybrid"].push(fund);
                }
            });
            topPicksRenderObj.renderTopPicks(topPicks);
            for (var category in topPicks) {
                if (window.outerWidth > 768) {
                    if (topPicks[category].length <= 3) {
                        document.querySelector('[data-attr="' + category + '"]').classList.remove('slider-dots');
                    }
                } else {
                    if (topPicks[category].length == 0) {
                        document.querySelector('[data-attr="' + category + '"]').classList.remove('slider-dots');
                    }
                }
            }
            investNow();
            topPicksSlick();
            adobeAnalytics();
        }

        function investNow() {
            /* Invest Now click button */
            $('[data-investnow="true"]').click(function (event) {
                event.preventDefault();
                var redirectionPath = event.target.dataset.swIframeUrl;
                var fundName = $(this).parents('.top-pickup-card').find('h5').text().trim();
                var fundRiskCat = $(this).parents('.top-pickup-card').find('.custom-tooltips-inner').text().trim();
                var fundType = $('[data-topickstablist]').find('[class="active"]').text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        /* if(document.querySelector('.xf-web-container') != null){
                            fundInvestNowNLI(fundType, fundName, fundRiskCat, userId);    
                        }else{
                            fundInvestNow(fundType, fundName, fundRiskCat, userId);
                        }    */
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            fundInvestNowNLI(fundType, fundName, fundRiskCat, "anonymous user");    
                        }else{
                            fundInvestNow(fundType, fundName, fundRiskCat, "anonymous user");
                        }
                    }
                } catch (err) {
                    console.log(err);
                }

                try{
                    if (!headerBizObj.getUserObj().isLoggedIn) {
                        showLoginPopup();
                        document.querySelector('[data-login="true"]').addEventListener('click', function () {
                            location.href = redirectionPath;
                        });
                    } else {
                        if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                            location.href = redirectionPath;
                        }
                    }
                }catch(err){
                    console.log(err);
                }   
            });
        }

        function topPicksSlick() {
            /*jsTopPickEquitySlider*/
            $('#jsTopPickEquitySlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                if ($('#jsTopPickEquitySlider .top-pickup-ul').length <= 3) {
                    if ($(window).width() > 1199) {
                        $('#jsTopPickEquitySlider .top-pickup-slider').removeClass('slider-dots');
                        $('#jsTopPickEquitySlider .slick-dots').remove();
                    }
                }
            });

            $('#jsTopPickEquitySlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px',
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px',
                    }
                }
                ]
            });

            $('#jsTopPickdebtSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                if ($('#jsTopPickdebtSlider .top-pickup-ul').length <= 3) {
                    if ($(window).width() > 1199) {
                        $('#jsTopPickdebtSlider .top-pickup-slider').removeClass('slider-dots');
                        $('#jsTopPickdebtSlider .slick-dots').remove();
                    }
                }
            });

            $('#jsTopPickdebtSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px',
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px'
                    }
                }
                ]
            });

            $('#jsTopPickHybridSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
                if ($('#jsTopPickHybridSlider .top-pickup-ul').length <= 3) {
                    if ($(window).width() > 1199) {
                        $('#jsTopPickHybridSlider .top-pickup-slider').removeClass('slider-dots');
                        $('#jsTopPickHybridSlider .slick-dots').remove();
                    }
                }
            });

            $('#jsTopPickHybridSlider').slick({
                dots: true,
                infinite: false,
                speed: 300,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                responsive: [{
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px',
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        // variableWidth: true,
                        centerMode: true,
                        centerPadding: '20px',
                    }
                }
                ]
            });



            /*jsTopPickEquitySlider*/
        }

        function topPicksClick() {
            $('.jsTabList [data-tab]').click(function () {
                $(this).parents('.jsTabList').find('li a').removeClass('active');
                $(this).addClass('active');
                var ele_id = $(this).attr('data-tab');
                $(this).parents('.jsTabContainer').find('.jsTabRow').addClass('d-none');
                $('#' + ele_id).fadeIn().removeClass('d-none');
                $('#jsTopPickEquitySlider, #jsTopPickdebtSlider, #jsTopPickHybridSlider').slick('refresh');
            })
        }

        return jsHelper.freezeObj(topPicksObj);
        //adobe analytics 13-27,42,51-54
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "topPicksBizObj", topPicksBizFn)
})(this || window || {});
/*top picks js end*/