Contact us banner box
====
This is a `Contact us banner box` component used in contact us page.

## Feature
* This is a authorable component.
* All the various elements of the component such as heading, images are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.


### Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./contactUsheading` Used for rendering headings of the component.
2. `./contactUsDescription` used for rendering description of the component. 
3. `./contactUsImage` Used for rendering image on page.


## Client Libraries
The component provides a `moneyfy.contact-us-banner-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction. It is already included in component HTML file.



## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5