/*investment avenues tab js*/
(function (_global) {

    var investmentProductsBizObj = (function (jsHelper) {
        var investmentProductsObj = {};

        document.addEventListener('DOMContentLoaded', function () {
            initializeInvestmentSlick();
            adobeAnalytics();
        });
        function adobeAnalytics(){	
          $('[data-investmenttab] a').click(function (event) {	
              event.preventDefault();	
              var clickButtonTitle = $(this).text().trim();	
              tabInteraction(clickButtonTitle, 'investment-product',userIdObj.userId)	
          });	
          $('[data-tableft] a').click(function (event) {	
              event.preventDefault();	
              var clickButtonTitle = $(this).text().trim();	
              var clickTextTitle = $(this).parents('.tab-left').find('h3').text().trim();	
              bannerInteraction(clickTextTitle, clickButtonTitle, 'investment-product',userIdObj.userId)	;
              location.href = $(this).attr('href')
          });	
      }


        function initializeInvestmentSlick() {      
              
            $('.js-investment-products-content').slick({
              slidesToShow: 1,
              slidesToScroll: 1,
              arrows: false,
              fade: true,
              asNavFor: '.js-investment-products-tabs',
              infinite: true,
              dots: false,
              autoplay: false,
              swipe: false,
              responsive: [            
                {
                  breakpoint: 768,
                  settings: {
                    swipe: true,
                    arrows: true,
                    dots: true,          
                  }
                }
              ]
            });
          
            /*27-8-2021*/ 
            $('.js-investment-products-tabs').slick({
              slidesToShow: 2,
              slidesToScroll: 1,
              asNavFor: '.js-investment-products-content',
              arrows: false,
              dots: false,
              infinite: true,
              variableWidth: true,
              focusOnSelect: true,
              autoplay: false,
              responsive: [            
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1, 
                    infinite: true,          
                  }
                },{
                  breakpoint: 640,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1, 
                    infinite: true,          
                  }
                },
              ]
            });
          
            // var investmentTabLength = $('.list-investment-tabs .slick-slide').length;  
            // if (investmentTabLength === 3) {
            //     $('.inner-tab-outer').css('max-width', '640px');
            // }
            // else {
            //     $('.inner-tab-outer').css('max-width', '840px');
            // }  
             
        }

        return jsHelper.freezeObj(investmentProductsObj);
    })(jsHelper);

    _global.jsHelper.defineReadOnlyObjProp(_global, 'investmentProductsBizObj', investmentProductsBizObj);
})(this || window || {});
/*investment avenues tab js*/