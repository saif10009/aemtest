/*fund manager biz js start*/
(function (_global) {
  var amcFundManagerBizObj = (function (jsHelper) {
    var amcFundManagerObj = {}

    document.addEventListener('DOMContentLoaded', function () {
      initializeFundManagerSlick();
      $('[data-mfamcviewall]').click(function(event){
        event.preventDefault();
        var ctaTitle = $(this).parents('.top-headings').find('h2').text().trim();
        var ctaText = $(this).text().trim();
        allCTAInteraction(ctaText,ctaTitle,'amc-fund-managers',userIdObj.userId)
        location.href = $(this).attr('href');
    })
    $('[data-fundmanagercard]').click(function(event){
        event.preventDefault();
        var ctaTitle = $(this).find('[data-fundmanagername]').data('fundmanagername');
        var ctaText = $(this).find('[data-fundmanagername]').data('fundmanagername');
        allCTAInteraction(ctaText,ctaTitle,'amc-fund-managers',userIdObj.userId)
        location.href = $(this).attr('href');
    })
    });

    function initializeFundManagerSlick() {
      $('#fundManagerSlider').on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        if ($('.inner-fund-manager .fund-manager-rows').length <= 3) {
          if ($(window).width() > 1199) {
            $('.inner-fund-manager').removeClass('slider-dots');
          }
        }
      });

      // 21-01-22
      $('#fundManagerSlider').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 1200,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,

            }
          },
          {
            breakpoint: 992,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 2,
            }
          },
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              // variableWidth: true,
              centerMode: true,
              centerPadding: '20px'
            }
          },
          {
            breakpoint: 375,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: true,
              centerMode: true,
            }
          },
          {
            breakpoint: 360,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              variableWidth: true,
              centerMode: true,
            }
          }
        ]
      });

      $('#fundManagerSlider30').slick({
        dots: true,
        infinite: false,
        speed: 300,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              // variableWidth: true,
              centerMode: true,
              centerPadding: '20px'
            }
          },
        ]
      });
      // 21-01-22
    }
    return jsHelper.freezeObj(amcFundManagerObj);
    //adobe analytics 8-12
  })(jsHelper)
  _global.jsHelper.defineReadOnlyObjProp(_global, "amcFundManagerBizObj", amcFundManagerBizObj)
})(this || window || {});
/*fund manager biz js end*/
