/*risk-info js start*/
(function(_global) {
    var stepsToStartBizObj = (function(jsHelper) {
        var stepsToStartObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            $('[data-adobe-analytics="contributemodal"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                likeToContributeClick(userIdObj.userId, ctaText, 'steps-to-start');
            })
            $('[data-adobe-analytics="proceed"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                openNPSProceed(userIdObj.userId,ctaText,'steps-to-start');
                window.open($(this).attr('href'));
            })
            $('[data-adobe-action-nps]').click(function (event) {
                event.preventDefault();
                var ctaTitle = $(this).parents().find('[data-nps-heading]').data('nps-heading');
                var ctaText = $(this).text().trim();
                allCTAInteraction(ctaText, ctaTitle, 'smallcase-direct-equity-banner', userIdObj.userId);
            })
        })

        return jsHelper.freezeObj(stepsToStartObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "stepsToStartBizObj", stepsToStartBizObj)
  })(this || window || {});
  /*risk-info js end*/