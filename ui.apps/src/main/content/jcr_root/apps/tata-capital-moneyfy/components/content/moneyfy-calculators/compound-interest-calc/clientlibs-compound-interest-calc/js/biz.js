/*investment calculator js start*/
(function (_global) {
    var investmentCalculatorBizObj = (function (jsHelper) {
        var investmentCalculatorObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            investmentCalculatorInitializer();
            investmentCalculatorEvents();
            //sipHighchartReact(1500000, 562159, 100);
            lumsumHighchartReact(100000, 76234, 100);
            $('[data-sip-start="sipstartbtn"]').click(function (event) {
                event.preventDefault();
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('investment-calculator-box',userId,'SIP calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('investment-calculator-box',userId,'SIP calculator',ctaText);
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('investment-calculator-box',"anonymous user",'SIP calculator',ctaText);
                        }else{
                            calculatorCtaInteraction('investment-calculator-box',"anonymous user",'SIP calculator',ctaText);
                        }
                    }
                } catch (err) {
                    console.log(err);
                }

                if (document.getElementsByClassName('iframe-wrapper').length == 0) {
                    location.href = $(this).attr('href');
                }
            })
        })
        function investmentCalculatorInitializer() {
            // Range slider start
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".js-calculatorRangeSlider").ionRangeSlider({
                    skin: "round",
                    postfix: "%",
                    prettify_enabled: true,
                    prettify_separator: ",",
                    step: 0.1,
                    onStart: rangeSliderSet,
                    onChange: rangeSliderSet,
                    onUpdate: rangeSliderSet,
                });
                function rangeSliderSet(data) {
                    commaSeparatedValue = data.from.toLocaleString("en-IN");
                    data.input.parents(".textbox-box").find(".js-showCalulatorRangeValue").val(commaSeparatedValue);
                }
            }
        }
        function investmentCalculatorEvents() {
            // year month toggle
            $('[data-yrMthFilter]').click(function () {
                filterVal = $(this).attr('data-yrMthFilter');
                $(this).parents('.year-month-toggle').find('[data-yrMthFilter]').removeClass('active');
                $(this).addClass('active');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard]').addClass('d-none');
                $(this).parents('.year-month-toggle').find('[data-data-yrMthCard="' + filterVal + '"]').removeClass('d-none');
            });

            /*calculator tab js*/
          /*  $('.js-CalculatorTabList [data-tab]').click(function () {
                var ctaText = $(this).text().trim();
                try {
                    if(sessionStorage.getItem('user') != null){
                        var userObj  = CryptoJS.AES.decrypt(JSON.parse(sessionStorage.getItem('user')), "yfyenom");
                        var userData = JSON.parse(userObj.toString(CryptoJS.enc.Utf8));
                        var userId = userData.appCode;
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('investment-calculator-box',userId,'SIP calculator',ctaText)
                        }else{
                            calculatorCtaInteraction('investment-calculator-box',userId,'SIP calculator',ctaText)
                        }   
                    }else{
                        if(document.querySelector('.xf-web-container') != null){
                            calculatorCtaInteractionNLI('investment-calculator-box',"anonymous user",'SIP calculator',ctaText)
                        }else{
                            calculatorCtaInteraction('investment-calculator-box',"anonymous user",'SIP calculator',ctaText)
                        }
                    } 
                } catch (err) {
                    console.log(err);                    
                }
                $(this).parents('.js-CalculatorTabList').find('li a').removeClass('active');
                $(this).addClass('active');
                var ele_id = $(this).attr('data-tab');
                $(this).parents('.jsTabContainer').find('.jsTabRow').addClass('d-none');
                $('#' + ele_id).removeClass('d-none');
            }) */
            /*calculator tab js*/
            // Calulator Input change start
            // $(".js-showCalulatorRangeValue").keyup(function (event) {
            //     var keycode = event.keyCode ? event.keyCode : event.which;
            //     $(this).trigger("change");
            // });
            $('.only-numeric-input-text').keyup(function (e) {
                $(this).val($(this).val().replace(/[^\d.-]/g, ''));
            });
            $(".js-showCalulatorRangeValue").on("change", function () {
                $this = $(this);
                var parents = $(this).parents(".textbox-box");
                var slider = parents.find(".custom-range-slider-wrap input");
                var my_range = slider.data("ionRangeSlider");

                setTimeout(function () {
                    var rangeValue = $this.val();

                    rangeValue = rangeValue.replace(/,/g, "");
                    my_range.update({
                        from: rangeValue,
                        extra_classes: "no-transition",
                    });
                }, 100);
            });
            // Calulator Input change end
            $('.price-only-comma').keyup(function () {
                if ($(this).val() != "") {
                    var rupeeValue = parseFloat($(this).val().replace(/,/g, ''));
                    commaSeparatedValue = rupeeValue.toLocaleString('en-IN');
                    $(this).val(commaSeparatedValue);
                }
            });
            //calculator events
            var tenureObj = {
                sipTenureYear: document.getElementById("sip_tenure_year"),
                sipTenureMonth: document.getElementById("sip_tenure_month"),
                lumsumTenureYear: document.getElementById("lumsum_tenure_year"),
                lumsumTenureMonth: document.getElementById("lumsum_tenure_month"),
                lumsumErrorMsg :document.getElementById('lumsumErrorMsg'),
                sipErrorMsg:document.getElementById('sipErrorMsg')
            }
            if ($(".js-calculatorRangeSlider").is(":visible")) {
                $(".lumsumSlider").on("input", function () {
                    if (document.querySelector('#lumsumTenureBtn .active').innerHTML == 'Years') {
                        lumsumCalculationFn((Number(tenureObj.lumsumTenureYear.value) * 12))
                    } else {
                        lumsumCalculationFn(Number(tenureObj.lumsumTenureMonth.value));
                    }
                });

                $(".sipSlider").on("input", function () {
                    if (document.querySelector('#SipTenureBtn .active').innerHTML == 'Years') {
                        sipCalculationFn((Number(tenureObj.sipTenureYear.value) * 12))
                    } else {
                        sipCalculationFn(Number(tenureObj.sipTenureMonth.value));
                    }
                });
            }
          /*  document.getElementById("sip_amount").addEventListener('keyup', function () {
                minMaxFn(this, 1000000,500,sipErrorMsg);
                if (document.querySelector('#SipTenureBtn .active').innerHTML == 'Years') {
                    sipCalculationFn((Number(tenureObj.sipTenureYear.value) * 12))
                } else {
                    sipCalculationFn(Number(tenureObj.sipTenureMonth.value));
                }
            }); */
            document.getElementById("lumsum_amount").addEventListener('keyup', function () {
                minMaxFn(this, 10000000,500,lumsumErrorMsg);
                if (document.querySelector('#SipTenureBtn .active').innerHTML == 'Years') {
                    lumsumCalculationFn(Number(tenureObj.lumsumTenureMonth.value))
                } else {
                    lumsumCalculationFn(Number(tenureObj.lumsumTenureMonth.value));
                }
            });
            $('#SipTenureBtn [data-yrMthFilter="year"]').click(function () {
                sipCalculationFn((Number(tenureObj.sipTenureYear.value) * 12))
            })
            $('#SipTenureBtn [data-yrMthFilter="month"]').click(function () {
                sipCalculationFn(Number(tenureObj.sipTenureMonth.value))
            })
            $('#lumsumTenureBtn [data-yrMthFilter="year"]').click(function () {
                lumsumCalculationFn((Number(tenureObj.lumsumTenureYear.value) * 12));
            })
            $('#lumsumTenureBtn [data-yrMthFilter="month"]').click(function () {
                lumsumCalculationFn(Number(tenureObj.lumsumTenureMonth.value))
            })
            //calculator events
        }
        function minMaxFn(ele, max,min,errorMsg) {
            if (parseFloat(ele.value.replace(/,/g, '')) > max) {
                errorMsg.innerHTML = '';
                errorMsg.innerHTML = 'Amount should not greater than ₹'+max;
            } else if (parseFloat(ele.value.replace(/,/g, '')) < min) {
                errorMsg.innerHTML = '';
                errorMsg.innerHTML = 'Amount should not less than ₹'+min;
            }
            else {
                errorMsg.innerHTML = '';
            }
        }
        function sipCalculationFn(sipTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            var sipInputbj = {
                calculator: "SIP",
                sipAmount: Number(parseFloat(document.getElementById("sip_amount").value.replace(/,/g, ''))),
                sipTenure: sipTenure,
                sipRoi: Number(document.getElementById("sip_roi").value) / (100 * 12),
            }

            myWorker.postMessage(sipInputbj);
            myWorker.onmessage = function (e) {
               // sipInputbj.sipHighchartReact = sipHighchartReact;
                investmentCalculatorRenderObj.renderInvestmentSipCalculation(e, sipInputbj);
                myWorker.terminate();
            }
        };
        function lumsumCalculationFn(lumsumTenure) {
            var myWorker = new Worker("/content/tata-capital-moneyfy/web-worker.js");
            var lumsumInputObj = {
                calculator: "Lumpsum",
                lumsumAmount: parseFloat(document.getElementById("lumsum_amount").value.replace(/,/g, '')),
                lumsumRoi: Number(document.getElementById("lumsum_roi").value) / 100,
                lumsumTenure: lumsumTenure,
            }
            myWorker.postMessage(lumsumInputObj);
            myWorker.onmessage = function (e) {
                lumsumInputObj.lumsumHighchartReact = lumsumHighchartReact;
                investmentCalculatorRenderObj.renderInvestmentLumsumCalculation(e, lumsumInputObj)
            }
        };
      /*  function sipHighchartReact(invest, interest, initialAxis) {
            var sipChart = (invest / (invest + interest)) * 100;
            // suggested strategy sip chart js start
            Highcharts.chart("mf-calculaotr-sip-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: sipChart,
                            },
                        ],
                    },
                ],
            });
        } */
        function lumsumHighchartReact(invest, interest, initialAxis) {
            var lumsumChart = (invest / (invest + interest)) * 100;
            // suggested strategy lumsum chart js start
            Highcharts.chart("mf-calculaotr-lumsum-strategy-graph", {
                chart: {
                    type: "solidgauge",
                    height: "100%",
                    margin: [0, 0, 0, 0]
                },
                credits: {
                    enabled: false,
                },
                title: {
                    text: "",
                },
                tooltip: {
                    enabled: false,
                },
                pane: {
                    startAngle: 0,
                    endAngle: 360,
                },
                plotOptions: {
                    series: {
                        states: {
                            hover: {
                                enabled: false,
                            },
                        },

                        dataLabels: {
                            enabled: false,
                        },
                    },
                },

                yAxis: {
                    min: 0,
                    max: 100,
                    lineWidth: 0,
                    tickPositions: [],
                },
                series: [
                    {
                        name: "Interest earned",
                        data: [
                            {
                                color: "#66CC99",
                                radius: "105%",
                                innerRadius: "60%",
                                y: initialAxis,
                            },
                        ],
                    },
                    {
                        name: "Total investment",
                        data: [
                            {
                                color: "#2C6EB5",
                                radius: "115%",
                                innerRadius: "58%",
                                y: lumsumChart,
                            },
                        ],
                    },
                ],
            });
        }
        return jsHelper.freezeObj(investmentCalculatorObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "investmentCalculatorBizObj", investmentCalculatorBizObj)
})(this || window || {});
/*investment calculator js end*/