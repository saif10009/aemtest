Work video box
====
The `Work video box` component is for adding Work video box on moneyfy mututal funds page. 



## Feature
* It is an multifield component.
* All the various elements of the component such as title , heading , descriptions and link are made authorable.
* One can easily edit the contents of the component with the help of edit dialog.    



## Edit Dialog Properties
The following properties are written to JCR for this Hosting Service Request component and are expected to be available as `Resource` properties:

1. `./workVideoBoxTitle` Used for add title text.
2. `./workVideoBoxHeading` Used for add heading text.
3. `./workVideoBoxDescription` Used for add description text.
4. `./description` are used for the short description that can be given to the respective cards.
5. `./workVideoBoxLink` are used for add video path.


## Client Libraries
The component provides a `moneyfy.work-video-box` editor client library category that includes CSS
handling for dialog interaction and it call in HTML


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5