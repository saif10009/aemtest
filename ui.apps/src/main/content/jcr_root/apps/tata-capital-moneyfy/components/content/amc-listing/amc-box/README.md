AMC box
====
The `AMC box` component is for adding AMC box on amc listing page.



## Feature
* It is an static component.



## Edit Dialog Properties
This is an static component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.amc-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML. `moneyfy.jquery`,`moneyfy.aos`,`moneyfy.slick`,`moneyfy.jquery-ui`,`moneyfy.mCustomScrollbar`,`moneyfy.select2`,`moneyfy.simplepagination`,`moneyfy.typehead` these dependancies libraries are use for load jquery,aos and slick file in component.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5