Sebi Categories Box
====
The `Sebi Categories Box` component is for adding Invest inlarge box on moneyfy mututal funds sebi categories page.



## Feature
* It is an static component.



## Edit Dialog Properties
This is an static component so there is no requirement to add properties.


## Client Libraries
The component provides a `moneyfy.sebi-categories-box` editor client library category that includes JavaScript and CSS
handling for dialog interaction and it call in HTML. `moneyfy.jquery`,`moneyfy.aos`,`moneyfy.slick`,`moneyfy.jquery-ui` these dependancies libraries are use for load jquery,aos and slick file in component.

## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5