Header
====
This component witten in HTL, allowing to render moneyfy Header on page.



## Feature
* Used to provide child page redirection of moneyfy page.
* Provide search functionality where user can search as per there requirement.
* Provide Login and register feature.



### Edit Dialog Properties

This is `cq:disgn_dialog` based component which is based on `Moneyfy-Header` ploicy.

## Client Libraries
The component provides a `moneyfy.header` editor client library category that includes JavaScript and CSS handling for dialog interaction.The `render.js` consist all the rendering logic of component on page. It is embeded inside `moneyfy.homePage` clientlib, which call inside template level.


## Information
* **Vendor**: Teknopoint
* **Version**: 1.0
* **Compatibility**: AEM 6.5