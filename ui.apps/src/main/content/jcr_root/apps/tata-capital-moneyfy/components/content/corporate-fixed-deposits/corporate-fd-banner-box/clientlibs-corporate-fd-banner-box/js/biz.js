/*corporate fd banner js start*/
(function (_global) {
    var corporateFdBannerBizObj = (function (jsHelper) {
        var corporateFdBannerObj = {}
        document.addEventListener('DOMContentLoaded', function () {
            corporateFdBannerClickFn();
        })
        function corporateFdBannerClickFn(){
            $('[data-target="co-fd-redirect"]').click(function (event) {
                event.preventDefault();
                var anchorLink = $(this).attr('href');
                var bannerTitle = $('.banner-left').find('h1').text().trim();
                var bannerCTA = $(this).text().trim()
                bannerInteraction(bannerTitle,bannerCTA,'corporate-fd-banner-box',userIdObj.userId)
                if (!headerBizObj.getUserObj().isLoggedIn) {
                    showLoginPopup();
                    document.querySelector('[data-login="true"]').addEventListener('click', function () {
                        location.href = appConfig.jocataDomain + anchorLink;
                    });
                } else {
                    location.href = appConfig.jocataDomain + anchorLink;
                }
            })
        }
        return jsHelper.freezeObj(corporateFdBannerObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "corporateFdBannerBizObj", corporateFdBannerBizObj)
})(this || window || {});
  /*corporate fd banner js end*/