/*popular categories blue tab*/
(function(_global) {
    var popularbluetabBizFnObj = (function(jsHelper) {
        var popularbluetabBizObj = {}
        document.addEventListener('DOMContentLoaded', function() {
            var fundFilter = $('[data-mobfilter="mob-filter"]').find('.active').find('a').html();
            $('[data-mobfilter="mob-filter"]').find('a').html(fundFilter);
            if (screen.width <= 600) {
                $('.js-compareBox').find('p').html('Compare Upto 2 Funds');
                $('.d-md-blocks').css('display', 'none');
            }
            blueTabClick();
            blueTabScrollEvent();
            adobeAnalytics();
            // show current page name selected in dropdown
            $('[data-id="ActiveList"]').find('.dropdown-button a').text($('[data-id="ActiveList"]').find('.dropdown-card .active a').text());

            /* compareFunds button click js */
            $('[data-id="compareFunds"]').click(function () {
                location.href = "/content/tata-capital-moneyfy/en/compare-mutual-funds.html";
            });
             
        });
        /*header scroll fixed animation*/
        var TOPBAR_HEIGHT = $('.top-bar').outerHeight();
        var SECONDBAR_HEIGHT = $('.second-bar').innerHeight();

        function header_fixed() {
            var windowScroll = $(window).scrollTop();
            var topbarHeight = $('.top-bar').outerHeight();

            if (windowScroll > topbarHeight) {
                $('header').addClass('affix');
                $('.wrapper').css('padding-top', SECONDBAR_HEIGHT);
            } else {
                $('header').removeClass('affix');
                $('.wrapper').css('padding-top', (TOPBAR_HEIGHT + SECONDBAR_HEIGHT));
            }
        }

        function blueTabClick() {
            $('#js-selectPopularCategory li a').click(function(){
                var txt = $(this).text();
                $('.js-filterBtn').find('a').text(txt);
            });
            
            /*blue tab*/
            if ($(window).width() > 1199) {
                var BluetabLinkCount = $('.jsTabLength li:not(.show-tab)').length;
            }
            if ($(window).width() < 1200) {
                var BluetabLinkCount = $('.jsTabLength li').length;
            }
            $('.showTabLink').text(BluetabLinkCount);
            $('.jsBlueMoreLink').click(function() {
                $(this).toggleClass('active');
                $(this).find('#blueLinkMore').toggleClass('d-none');
                $(this).find('#blueLinkLess').toggleClass('d-none');
                $('.blue-tab-slide').slideToggle('500');
            })


            $('.jsDropDownMenuGet li a').click(function() {
                var getMenuLink = $(this).text();
                $(this).parents('.custom-dropdown-new').find('.jsDropDownMenuShow').text(getMenuLink);
                $(this).parents('.jsDropDownMenuGet').find('li').removeClass('active');
                $(this).parents('li').addClass('active');



                /* -------- sebi mobile filter js start -------- */
                if ($(window).width() < 991) {
                    var parentCard = $(this).parents('.filter-card-item').attr('data-filter-item');
                    $('[data-filter-link="' + parentCard + '"]').addClass('hasFilter');
                }
                /* -------- sebi mobile filter js end -------- */
            });
        }

        function blueTabScrollEvent() {
            if ($(window).width() > 991) {
                $('.js-floating').before('<div class="floating"></div>');
            }
            if ($(window).width() < 991) {
                $('.js-fixContainer').before('<div class="mobFloating"></div>');
            }
            $(window).on('scroll', function() {
                // header_fixed();

                if ($(window).width() > 991) {
                    var headerHeight = $('header').height();
                    if ($('.js-floating').length > 0) {
                        var ele_floating = $('.js-floating');
                        var ele_height = $(ele_floating).height();
                        var ele_position = $('.floating').position().top - headerHeight;
                        var scrollTop = $(window).scrollTop();
                        if (scrollTop >= ele_position) {
                            ele_floating.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '6' }).prev('.floating').css('height', ele_height);
                        } else {
                            ele_floating.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.floating').css('height', '0');
                        }
                    }
                    if ($(".js-rightSideFix").is(":visible")) {
                        var scroll = $(window).scrollTop();
                        var div_height = $(".js-rightSideFix").height();
                        var mutual_fund_top = $(".similar-mutual-funds").offset().top - 270;

                        if (scroll + div_height < mutual_fund_top) {
                            $('.js-rightSideFix').removeClass('affix-absolute');
                        } else {
                            $('.js-rightSideFix').addClass('affix-absolute');
                        }
                    }
                }
                if ($(window).width() < 991) {
                    var headerHeight = $('header').height();;
                    var conatiner_position = $('.js-fixContainer').position().top - headerHeight;
                    var ele_selector = $('.js-fixOnTop');
                    var scrollTop = $(window).scrollTop();

                    if (scrollTop > conatiner_position) {
                        ele_selector.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' });
                    } else {
                        ele_selector.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' });
                    }

                    var conatiner_filter_position = $('.mobFloating:visible').position().top - headerHeight;
                    var ele_selector_filter = $('.js-fixContainer');
                    var ele_height = $(ele_selector_filter).height();

                    if (scrollTop > conatiner_filter_position) {
                        ele_selector_filter.addClass('affix').css({ 'width': '100%', 'top': headerHeight, 'z-index': '8' }).prev('.mobFloating').css('height', ele_height);
                    } else {
                        ele_selector_filter.removeClass('affix').css({ 'width': '100%', 'top': '0', 'z-index': '1' }).prev('.mobFloating').css('height', '0');
                    }


                }
            });
        }
        function adobeAnalytics(){	
            $('[data-bluetablist] li a').click(function(event){	
                event.preventDefault();	
                var tabTitle = $(this).text().trim();	
                console.log(tabTitle)	
                tabInteraction(tabTitle,'popularCategoriesTab',userIdObj.userId)	
                location.href = $(this).attr('href');	
            })	
        }

        return jsHelper.freezeObj(popularbluetabBizObj);
    })(jsHelper)
    _global.jsHelper.defineReadOnlyObjProp(_global, "popularbluetabBizObj", popularbluetabBizFnObj)
})(this || window || {});
/*popular categories blue tab js end*/