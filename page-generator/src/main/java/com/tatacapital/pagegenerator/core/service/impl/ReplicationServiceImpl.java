package com.tatacapital.pagegenerator.core.service.impl;

import com.day.cq.replication.ReplicationActionType;
import com.day.cq.replication.ReplicationException;
import com.day.cq.replication.Replicator;
import com.tatacapital.pagegenerator.core.services.ReplicationService;
import com.tatacapital.pagegenerator.core.services.ResourceHelper;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.Objects;


@Component(service = ReplicationService.class, immediate = true)
public class ReplicationServiceImpl implements ReplicationService {

    @Reference
    ResourceHelper resourceHelper;
    ResourceResolver resourceResolver;
    Session session;
    @Reference
    private Replicator replicator;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean replicateContent(String[] path) {
        try {
            resourceResolver = resourceHelper.getServiceResourceResolver();
            session = resourceResolver.adaptTo(Session.class);
            long start = System.currentTimeMillis();
            logger.info("Replication Started : {}", start);
            for (int i = 0; i < path.length; i++) {
                    logger.info("Page Path to be Replicated : {}", path[i]);
                    try{
                        replicator.replicate(session, ReplicationActionType.ACTIVATE, path[i]);
                    }catch (Exception e)
                    {
                        logger.info("Replication Failed for this path: {}", path[i]);
                        e.printStackTrace();
                    }
            }
            long end = System.currentTimeMillis();
            logger.info("Replication completed : {} and Time Taken : {}", end, start - end);
            session.save();
            return true;
        } catch (LoginException e) {
            logger.error("Replicate Content ERROR ::" + e.getMessage());
        } catch (RepositoryException e) {
            logger.error("Repository Exception ERROR ::" + e.getMessage());
        } finally {
            if (Objects.nonNull(resourceResolver))
                resourceResolver.close();
            if (Objects.nonNull(session))
                session.logout();
        }
        return false;
    }

}



