package com.tatacapital.pagegenerator.core.service.impl;

import com.tatacapital.pagegenerator.core.services.MoneyfyPageGenConfigService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Designate(ocd= MoneyfyPageGenConfigImpl.Config.class)
@Component(service = MoneyfyPageGenConfigService.class,immediate = true)
public class MoneyfyPageGenConfigImpl implements MoneyfyPageGenConfigService {

    Logger logger = LoggerFactory.getLogger(getClass());

    @ObjectClassDefinition(name="Moneyfy Page Generation Ocd")
    public @interface Config
    {
        @AttributeDefinition(name = "Page Gen Get Data URL" ,description = "Enter your URI here")
        String pageGenDataUrl() default "http://172.27.16.54:8080/shaft/api/moneyfy-aem-channel-provider/generate-aem-json/partner";

        @AttributeDefinition(name = "Page Gen SHAFT Authorization" ,description = "Enter your token here")
        String pageGenAuthorization() default "MTI4OjoxMDAwMDo6MzUwODAzN2ZkMjUyNDMxZTZjOTE3Y2I1YmZlMWQ2ZGU6OjM0MmMwZDFlYzU2NmY2MGQ0MDlmMDhjZTQ1ODI4ODYxOjp2MitXWlhBYVZRVWQ3bENwaTB3N3FpdDdwcEtPNlZvSmtxQXN5TkE4b2IwPQ==";

        @AttributeDefinition(name = "Page Gen Report Shaft URL" ,description = "Enter your URI here")
        String url() default "http://172.27.16.54:8080/shaft/api/moneyfy-db-channel/page-generation-report/partner";

        @AttributeDefinition(name = "CDN URL" ,description = "Enter your url here")
        String cdnUrl() default "https://insight.n7.io/hm/purgecache/client";

        @AttributeDefinition(name = "CDN Purge Token" ,description = "Enter your token here")
        String cdnPurgeToken() default "Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0YXRhY2FwaXRhbCIsImRvbWFpbk5hbWUiOiJ3d3cudGF0YWNhcGl0YWwuY29tIiwidXNlck5hbWUiOiJudml6aW9uX3NwYXJrcyIsImlhdCI6MTY0ODAzOTM0MSwiZXhwIjoxNjc5NTc1MzQxfQ.aYr9wH9Pw1DpoFI0Vx2RFMpncNiS6Q1GzZDP210L1FHJrQqRHoC0db608yqT3oFD6HXpLj23dA-Eok9g3QABRQ";

        @AttributeDefinition(name = "CDN Domain URL" ,description = "Enter your domain url to purge here")
        String cdnDomainUrl() default "https://tclu.tatacapital.com/";

        @AttributeDefinition(name = "CDN Purge URL" ,description = "Enter your page url to purge here")
        String cdnPagePurgeUrl() default "https://www.tatacapital.com/cug/moneyfy/mutual-funds/*";

        @AttributeDefinition(name = "Enable Replication",  description = "Enable Replication here")
        boolean replication_enabled() default false;

        @AttributeDefinition(name = "Enable Backup",  description = "Enable Backup of Page Generation here")
        boolean backup_enabled() default false;

    }

    private String url;
    private String cdnUrl;
    private String cdnPurgeToken;
    private String cdnPagePurgeUrl;
    private String cdnDomainUrl;
    private String pageGenDataUrl;
    private String pageGenAuthorization;
    private boolean replicationEnabled;
    private boolean backupEnabled;

    @Modified
    @Activate
    public void activate(Config config)
    {
        this.pageGenDataUrl=config.pageGenDataUrl();
        this.pageGenAuthorization=config.pageGenAuthorization();
        this.url=config.url();
        this.cdnUrl=config.cdnUrl();
        this.cdnPurgeToken=config.cdnPurgeToken();
        this.cdnPagePurgeUrl=config.cdnPagePurgeUrl();
        this.cdnDomainUrl = config.cdnDomainUrl();
        this.replicationEnabled = config.replication_enabled();
        this.backupEnabled = config.backup_enabled();
    }

    @Override
    public String getUri() {
        return url;
    }

    @Override
    public String getCdnUrl() {
        return cdnUrl;
    }

    @Override
    public String getCdnDomainUrl() {
        return cdnDomainUrl;
    }

    @Override
    public String getCdnPurgeToken() {
        return cdnPurgeToken;
    }

    @Override
    public String getCdnPagePurgeUrl() {
        return cdnPagePurgeUrl;
    }

    @Override
    public String getPageGenDataUrl() {
        return pageGenDataUrl;
    }

    @Override
    public String getPageGenAuthorization() {
        return pageGenAuthorization;
    }

    @Override
    public boolean getReplicationEnabled() {
        return replicationEnabled;
    }

    @Override
    public boolean getBackupEnabled() {
        return backupEnabled;
    }

}
