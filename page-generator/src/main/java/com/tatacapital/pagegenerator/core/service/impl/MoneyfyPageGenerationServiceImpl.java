package com.tatacapital.pagegenerator.core.service.impl;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.day.cq.wcm.api.WCMException;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tatacapital.pagegenerator.core.constant.MoneyfyConstants;
import com.tatacapital.pagegenerator.core.services.MoneyfyPageGenerationService;
import com.tatacapital.pagegenerator.core.services.ResourceHelper;
import com.tatacapital.pagegenerator.core.services.MoneyfyPageGenConfigService;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component(service = MoneyfyPageGenerationService.class, immediate = true)
public class MoneyfyPageGenerationServiceImpl implements MoneyfyPageGenerationService {

    public static Map<String, String> map;
    Logger log = LoggerFactory.getLogger(getClass());
    @Reference
    HttpClientBuilderFactory httpClientBuilderFactory;

    @Reference
    ResourceHelper resourceHelper;

    @Reference
    MoneyfyPageGenConfigService moneyfyPageGenConfigService;

    @Override
    public boolean createAndBackup() {
        ResourceResolver resourceResolver = null;
        Session session = null;
        log.info("****** Create Backup function called ******");
        try {
            resourceResolver = resourceHelper.getServiceResourceResolver();
            session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (session.itemExists(MoneyfyConstants.MUTUAL_FUND_BACKUP)) {
                log.info("****** MF Backup page exist ******");
                pageManager.delete(pageManager.getPage(MoneyfyConstants.MUTUAL_FUND_BACKUP), false, true);
            }
            if (session.itemExists(MoneyfyConstants.MF_HOME_PAGE_PATH)) {
                log.info("****** Mutual funds page exist ******");
                Page sourcePage = pageManager.getPage(MoneyfyConstants.MF_HOME_PAGE_PATH);
                Resource sourcePath = resourceResolver.getResource(MoneyfyConstants.MF_HOME_PAGE_PATH);
                Page containingPage = pageManager.getContainingPage(sourcePath);
                if (containingPage.listChildren().hasNext()) {
                    log.info("****** Mutual funds list page ******");
                    Page mutualFundsBkpPage = pageManager.move(sourcePage, MoneyfyConstants.MUTUAL_FUND_BACKUP, null, false, false, null);
                    pageManager.copy(mutualFundsBkpPage,MoneyfyConstants.MF_HOME_PAGE_PATH,null,true,false);
                }
                session.save();
            }
            return true;
        } catch (LoginException e) {
            log.error("Login Exception : {}", e.getMessage());
        } catch (WCMException e) {
            log.error("WCM Exception : {}", e.getMessage());
        } catch (Exception e) {
            log.error("Exception : {}", e.getMessage());
        } finally {
            if (session.isLive())
                session.logout();
            if (resourceResolver.isLive())
                resourceResolver.close();
        }
        return false;
    }


    //Method to create AMC pages
    public static int amcPageCounter;
    public static int amcPageNotCreatedCounter;
    @Override
    public List<String> createAMCPages(JsonObject pageGenJsonObj) {
        List<String> listOfAmcPages = new ArrayList<>();
        map = new HashMap<>();
        amcPageCounter= 0;
        amcPageNotCreatedCounter=0;
        try {
            JsonArray fundsThroughAmcArray = pageGenJsonObj.getAsJsonArray("amcMaster");
            log.info("AMC Data received count : {}", fundsThroughAmcArray.size());
            ResourceResolver resourceResolver = resourceHelper.getServiceResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

            if (session != null) {

                for (JsonElement amcPageJsonElement : fundsThroughAmcArray) {

                    JsonObject amcPageJsonObj = amcPageJsonElement.getAsJsonObject();
                    JsonObject obj = amcPageJsonObj.getAsJsonObject();
                    obj.addProperty("pageType", "AMC");
                    String amcFundName = obj.get("name").getAsString();
                    String amcID = obj.get("amcId").getAsString();
                    String finalAmcFundName = amcFundName.trim().toLowerCase()
                            .replaceAll("\\s+", "-")
                            .replaceAll("[^a-zA-Z0-9]", "-")
                            .replaceAll("\\-+", "-")
                            .replaceAll("-[^a-zA-Z]*$","");
                    log.error("amc fund page name : {}",finalAmcFundName);
                    obj.addProperty("iconPath", "/content/dam/tata-capital-moneyfy/amc-icons/".concat(finalAmcFundName).concat(".png"));
                    if (!session.itemExists(obj.get("iconPath").getAsString())) {
                        obj.addProperty("iconPath",MoneyfyConstants.DEFAULT_ICON_PATH);
                    }
                    if (!session.itemExists(MoneyfyConstants.MF_HOME_PAGE_PATH.concat("/amc"))) {
                        Page amcListingPage = pageManager.create(MoneyfyConstants.MF_HOME_PAGE_PATH, "amc", MoneyfyConstants.AMC_LISTING_TEMPLATE, "amc-listing");
                        Node amcListingNode = amcListingPage.adaptTo(Node.class);
                        Node pageProperties = amcListingNode.getNode("jcr:content");
                        pageProperties.setProperty("jcr:title", "Moneyfy - Check Out All the AMC Listings Offered By Us");
                        pageProperties.setProperty("jcr:description", "Need to know the AMC Listings present with Moneyfy? Get the listing of all our trusted AMCs and get the detail knowledge of each AMC with us now");
                        pageProperties.setProperty("pageType", "AMC_LISTING");
                        pageProperties.setProperty("pageTitle", "AMC");
                        listOfAmcPages.add(MoneyfyConstants.MF_HOME_PAGE_PATH.concat("/amc"));
                    }
                    if(session.itemExists(MoneyfyConstants.MF_HOME_PAGE_PATH)){
                        Page amcPage = pageManager.create(MoneyfyConstants.MF_HOME_PAGE_PATH, finalAmcFundName, MoneyfyConstants.AMC_TEMPLATE_PATH, amcFundName);
                        amcPageCounter++;
                        setAmcPageProperties(amcPage, amcPageJsonObj, session);
                    }else{
                        amcPageNotCreatedCounter++;
                    }//method to set page properties
                    listOfAmcPages.add(MoneyfyConstants.MF_HOME_PAGE_PATH.concat("/").concat(finalAmcFundName));
                    map.put(amcID, MoneyfyConstants.MF_HOME_PAGE_PATH.concat("/").concat(finalAmcFundName));
                }
            }
            log.info("AMC Pages Generated {} out of {}", String.valueOf(amcPageCounter), String.valueOf(fundsThroughAmcArray.size()));
            return listOfAmcPages;
        } catch (Exception e) {
            log.error("Exception in creating AMC pages : {}",e.toString());
        }
        return null;
    }

    //Method to create FUND Detail pages
    public static int fundCounter;
    public static int fundPageNotCreatedCounter;
    @Override
    public List<String> createFundDetailPages(JsonObject pageGenJsonObj) throws IOException {

        fundCounter = 0;
        fundPageNotCreatedCounter = 0;
        List<String> listOfFundDetailPage = new ArrayList<>();
        try {
            JsonArray fundDetailArray = pageGenJsonObj.getAsJsonArray("fundMaster");
            log.info("Fund Data received count : {}", fundDetailArray.size());
            ResourceResolver resourceResolver = resourceHelper.getServiceResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

            if (session != null) {
                List<String> invalidFunds = new ArrayList<>();
                for (JsonElement fundDetailJsonElement : fundDetailArray) {
                    JsonObject fundDetailJsonObj = fundDetailJsonElement.getAsJsonObject();
                    fundDetailJsonObj.addProperty("pageType", "FUND");
                    String amcId = fundDetailJsonObj.get("amcId").getAsString();
                    JsonObject schemeDetailsJsonObject = fundDetailJsonObj.getAsJsonObject("schemeDetails");
                    if (map.containsKey(amcId)) {
                        String fundName = !schemeDetailsJsonObject.get("name").getAsString().equals("") ? schemeDetailsJsonObject.get("name").getAsString() : "not-found";
                        String finalName = fundName.
                                trim().toLowerCase()
                                .replaceAll("\\s+", "-")
                                .replaceAll("[^a-zA-Z0-9]", "-")
                                .replaceAll("\\-+", "-")
                                .replaceAll("-[^a-zA-Z]*$","");
                        log.error("Fund detail page name : {}",finalName);
                        String fundDetailParentPath = map.get(amcId);
                        Page fundDetailPage = pageManager.create(fundDetailParentPath, finalName, MoneyfyConstants.FUND_DETAIL_TEPLATE_PATH, fundName, true);
                        fundCounter++;
                        fundDetailJsonObj.addProperty("amcPagePath", fundDetailParentPath);
                        setFundsPageProperties(fundName, fundDetailPage, fundDetailJsonObj, session);
                        listOfFundDetailPage.add(fundDetailParentPath.concat("/").concat(finalName));
                    } else {
                        invalidFunds.add(schemeDetailsJsonObject.get("name").getAsString());
                        fundPageNotCreatedCounter++;
                    }
                }
                listOfFundDetailPage.add(MoneyfyConstants.MF_HOME_PAGE_PATH);
                log.info("Invalid Funds Pages count : {} | Invalid Fund Name : {}", invalidFunds.size(), invalidFunds);
                log.info("Funds Pages Created count : {}", fundCounter);
            }
            log.info("FUND Pages Generated {} out of {}", String.valueOf(fundCounter), String.valueOf(fundDetailArray.size()));
            log.info("FUND Pages not Generated : {}", fundPageNotCreatedCounter);
            return listOfFundDetailPage;
        } catch (Exception e) {
            log.error("Exception in creating fund detail pages : {}", e.getMessage());
        }
        return null;
    }

    //Method to create FUND Manager pages
    public static int fundManagerCounter;
    public static int fundManagerPageNotCreatedCounter;
    @Override
    public List<String> createFundMangerPages(JsonObject pageGenJsonObj) {
        List<String> listOfFundManagerPages = new ArrayList<>();
        fundManagerCounter = 0;
        fundManagerPageNotCreatedCounter=0;
        try {
            JsonArray fundsThroughFundManagerArray = pageGenJsonObj.getAsJsonArray("fundManagerMaster");
            log.info("Fund Manager Data received count : {}", fundsThroughFundManagerArray.size());
            ResourceResolver resourceResolver = resourceHelper.getServiceResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);

            if (session != null) {
                for (JsonElement fundManagerJsonElement : fundsThroughFundManagerArray) {
                    JsonObject fundManagerJsonObj = fundManagerJsonElement.getAsJsonObject();
                    String fundManagerName = fundManagerJsonObj.get("name").getAsString();
                    String finalFundManagerName = fundManagerName
                            .trim().toLowerCase()
                            .replaceAll("\\s+", "-")
                            .replaceAll("[^a-zA-Z0-9]", "-")
                            .replaceAll("\\-+", "-")
                            .replaceAll("-[^a-zA-Z]*$","");
                    log.error("Fundmanager page name : {}",finalFundManagerName);
                    if (!session.itemExists(MoneyfyConstants.FUND_MANAGER_PAGE_PATH)) {
                        Page fundListingPage = pageManager.create(MoneyfyConstants.MF_HOME_PAGE_PATH, MoneyfyConstants.FUND_MANAGER_PAGE_NAME, MoneyfyConstants.FUND_MANAGER_TEMPLATE_PATH, "Fund Managers");
                        Node newNode = fundListingPage.adaptTo(Node.class);
                        Node pageProperties = newNode.getNode("jcr:content");
                        pageProperties.setProperty("jcr:title", "Moneyfy - Find Out All the Fund Manager Listings Offered Here");
                        pageProperties.setProperty("jcr:description", "Want to see the Fund Managers associated with Moneyfy? Get the listing of all our Fund Managers and get the detail knowledge of each Mutual Fund investments now");
                        pageProperties.setProperty("pageType", "FUND_MANAGER_LISTING");
                        pageProperties.setProperty("pageTitle", "Fund Managers");
                        listOfFundManagerPages.add(MoneyfyConstants.FUND_MANAGER_PAGE_PATH);
                    }
                    if(session.itemExists(MoneyfyConstants.FUND_MANAGER_PAGE_PATH)) {
                        Page fundManagerPage = pageManager.create(MoneyfyConstants.FUND_MANAGER_PAGE_PATH, finalFundManagerName, MoneyfyConstants.FUND_MANAGER_DETAIL_TEMPLATE_PATH, fundManagerName);
                        fundManagerCounter++;
                        setFundManagerPageProperties(fundManagerPage, fundManagerJsonObj, session);//method to set page properties
                    }
                    else {
                        fundManagerPageNotCreatedCounter++;
                    }
                    listOfFundManagerPages.add(MoneyfyConstants.FUND_MANAGER_PAGE_PATH.concat("/").concat(finalFundManagerName));
                }
                log.info("Fund Manager Pages created count : {}", fundManagerCounter);
            }
            return listOfFundManagerPages;
        } catch (Exception e) {
            log.error("Exception in creating fund manager pages : {}", e.getMessage());
        }
        return null;
    }

    //Method to create SEBI Cat pages
    public static int sebiCounter;
    public static int sebiPageNotCreatedCounter;
    @Override
    public List<String> createSEBICatPages(JsonObject pageGenJsonObj) {
        sebiCounter = 0;
        sebiPageNotCreatedCounter=0;
        List<String> listOfSebiPages = new ArrayList<>();
        try {
            JsonArray sebiCategoryPagesArray = pageGenJsonObj.getAsJsonArray("sebiCategoryMaster");
            log.info("SEBI Category Data received count : {}", sebiCategoryPagesArray.size());
            ResourceResolver resourceResolver = resourceHelper.getServiceResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
            if (session != null) {
                for (JsonElement sebiCategoryJsonElement : sebiCategoryPagesArray) {
                    JsonObject sebiCategoryJsonObj = sebiCategoryJsonElement.getAsJsonObject();
                    sebiCategoryJsonObj.addProperty("pageType", "SEBI_SUB_CATEGORY");
                    String categoryName = sebiCategoryJsonObj.get("catName").getAsString().toUpperCase();
                    String sebiCategoryPagePath = null;
                    String sebiCatType = null;
                    switch (categoryName) {
                        case "DEBT":
                            sebiCategoryPagePath = MoneyfyConstants.SEBI_CATEGORY_PAGE_PATH_OF_DEBT;
                            sebiCatType = "sebi-categories-debt";
                            break;
                        case "EQUITY":
                            sebiCategoryPagePath = MoneyfyConstants.SEBI_CATEGORY_PAGE_PATH_OF_EQUITY;
                            sebiCatType = "sebi-categories-equity";
                            break;
                        case "HYBRID":
                            sebiCategoryPagePath = MoneyfyConstants.SEBI_CATEGORY_PAGE_PATH_OF_HYBRID;
                            sebiCatType = "sebi-categories-hybrid";
                            break;
                        case "OTHER":
                            sebiCategoryPagePath = MoneyfyConstants.SEBI_CATEGORY_PAGE_PATH_OF_OTHERS;
                            sebiCatType = "sebi-categories-others";
                            break;
                        default:
                            System.out.print("!!!!!");
                    }
                    String sebiSubCatName = sebiCategoryJsonObj.get("subCatName").getAsString();
                    String finalSebiSubCatName = sebiSubCatName
                            .trim().toLowerCase()
                            .replaceAll("\\s+", "-")
                            .replaceAll("[^a-zA-Z0-9]", "-")
                            .replaceAll("\\-+", "-")
                            .replaceAll("-[^a-zA-Z]*$","");
                    log.error("sebi sub category page name : {}",finalSebiSubCatName);
                    sebiCategoryJsonObj.addProperty("iconPath", "/content/dam/tata-capital-moneyfy/mutual-funds/".concat(sebiCatType).concat("/").concat(finalSebiSubCatName));
                    if (!session.itemExists(sebiCategoryPagePath)) {
                        Page sebiPage = pageManager.create(MoneyfyConstants.MF_HOME_PAGE_PATH, sebiCatType, MoneyfyConstants.SEBI_CATEGORY_TEMPLATE_PATH, sebiCatType.replaceAll("-", " ").toUpperCase());
                        Node node = sebiPage.adaptTo(Node.class);
                        Node pageProperties = node.getNode("jcr:content");
                        pageProperties.setProperty("pageType", "SEBI_CATEGORY");
                        listOfSebiPages.add(sebiCategoryPagePath);
                    }
                    if(session.itemExists(sebiCategoryPagePath)) {
                        Page newPage = pageManager.create(sebiCategoryPagePath, finalSebiSubCatName, MoneyfyConstants.SEBI_CATEGORY_TEMPLATE_PATH, sebiSubCatName);
                        setSEBICatPageProperties(newPage, sebiCategoryJsonObj, session);
                        sebiCounter++;
                    }
                    else
                    {
                        sebiPageNotCreatedCounter++;
                    }
                    listOfSebiPages.add(sebiCategoryPagePath.concat("/").concat(finalSebiSubCatName));
                }
            }
            log.info("SEBI Category Pages created count : {}", sebiCounter);
            return listOfSebiPages;
        } catch (LoginException | RepositoryException | WCMException e) {
            log.error("Exception in creating SEBI category pages : {}", e.getMessage());
        }
        return null;
    }

    //Method to set SEBI Cat page properties
    private void setSEBICatPageProperties(Page sebiPages, JsonObject sebiCategoryJsonObj, Session session) throws RepositoryException {
        if (sebiPages != null) {
            Node newNode = sebiPages.adaptTo(Node.class);
            Node pageProperties = newNode.getNode("jcr:content");
            String fundName = sebiCategoryJsonObj.get("subCatName").getAsString();
            sebiCategoryJsonObj.addProperty("jcr:description", "Explore " + fundName + " Funds and Invest in best " + fundName + " funds. Get all the necessary information about " + fundName + " and Start your Investment journey with Moneyfy now");
            sebiCategoryJsonObj.addProperty("jcr:title", fundName + " Funds:Invest in Top " + fundName + " Funds - Moneyfy");
            sebiCategoryJsonObj.addProperty("pageType", "SEBI_SUB_CATEGORY");
            sebiCategoryJsonObj.addProperty("pageTitle", sebiCategoryJsonObj.get("subCatName").getAsString());
            if (pageProperties != null) {
                for (Map.Entry<String, JsonElement> entry : sebiCategoryJsonObj.entrySet()) {
                    final String propKey = entry.getKey();
                    final JsonElement jsonElement = entry.getValue();
                    if (jsonElement.isJsonPrimitive()) {
                        pageProperties.setProperty(propKey, jsonElement.getAsString());
                    } else {
                        pageProperties.setProperty(propKey, jsonElement.toString());
                    }
                }
                session.save();
            }
        }
    }

    //Method to set FUND Detail page properties
    private void setFundsPageProperties(String fundName, Page fundDetailPages, JsonObject fundDetailsJsonObject, Session session) throws RepositoryException {
        if (fundDetailPages != null) {
            Node newNode = fundDetailPages.adaptTo(Node.class);
            Node pageProperties = newNode.getNode("jcr:content");
            fundDetailsJsonObject.addProperty("jcr:description", fundName + " Online: Get "+ fundName+" NAV, returns, performance and compare mutual funds for better returns at Moneyfy. INVEST NOW!");
            fundDetailsJsonObject.addProperty("jcr:title", "Invest in " + fundName + " Funds (G) Online | Moneyfy");
            fundDetailsJsonObject.addProperty("pageTitle", fundName);
            fundDetailsJsonObject.addProperty("id", fundDetailsJsonObject.getAsJsonObject("schemeDetails").get("id").getAsString());
            fundDetailsJsonObject.addProperty("pageType", "FUND");
            if (pageProperties != null) {
                for (Map.Entry<String, JsonElement> entry : fundDetailsJsonObject.entrySet()) {
                    final String propKey = entry.getKey();
                    final JsonElement jsonElement = entry.getValue();
                    if (jsonElement.isJsonPrimitive()) {
                        pageProperties.setProperty(propKey, jsonElement.getAsString());
                    } else {
                        pageProperties.setProperty(propKey, jsonElement.toString());
                    }
                }
                session.save();
            }
        }
    }

    //Method to set AMC page properties
    private void setAmcPageProperties(Page amcPages, JsonObject amcJsonObject, Session session) throws RepositoryException {
        if (amcPages != null) {
            Node amcPageNode = amcPages.adaptTo(Node.class);
            Node pageProperties = amcPageNode.getNode("jcr:content");
            String[] amcFundName = amcJsonObject.get("name").getAsString().split("Fund");
            String fundName = amcFundName[0].trim();
            String fundNameWithoutMutual = fundName.replaceAll("Mutual", "");
            amcJsonObject.addProperty("jcr:description", "Invest in " + fundName + " Funds Online in India with Tata Capital Moneyfy. Get complete details of all " + fundNameWithoutMutual + "MF Schemes, NAV, Performance, Returns and Ratings in few clicks.");
            amcJsonObject.addProperty("jcr:title", fundName + " Fund Schemes, NAV and Returns 2023 | Tata Capital Moneyfy");
            amcJsonObject.addProperty("pageType", "AMC");
            amcJsonObject.addProperty("pageTitle", amcJsonObject.get("name").getAsString());
            if (pageProperties != null) {
                for (Map.Entry<String, JsonElement> entry : amcJsonObject.entrySet()) {
                    final String propKey = entry.getKey();
                    final JsonElement jsonElement = entry.getValue();
                    if (jsonElement.isJsonPrimitive()) {
                        if (propKey.equals("exploreSeq")) {
                            pageProperties.setProperty(propKey, jsonElement.getAsInt());
                        } else {
                            pageProperties.setProperty(propKey, jsonElement.getAsString());
                        }
                    } else if (!jsonElement.isJsonPrimitive()) {
                        if (propKey.equals("exploreSeq")) {
                            pageProperties.setProperty(propKey, jsonElement.getAsInt());
                        } else {
                            pageProperties.setProperty(propKey, jsonElement.toString());
                        }
                    }
                }
                session.save();
            }
        }
    }

    //Method to set FUND Manager page properties
    private void setFundManagerPageProperties(Page fundManagerPages, JsonObject fundManagerJsonObject, Session session) throws RepositoryException {
        if (fundManagerPages != null) {
            Node fundManagerPageNode = fundManagerPages.adaptTo(Node.class);
            Node pageProperties = fundManagerPageNode.getNode("jcr:content");
            String fundManagerNameArray[] = fundManagerJsonObject.get("name").getAsString().split("Fund");
            String fundManagerName = fundManagerNameArray[0].trim();
            fundManagerJsonObject.addProperty("jcr:description", fundManagerName + " Fund Manager - Role of a fund manager includes buying and selling of securities based on their research and analysis. Check out the mutual fund managers across all the categories for best guidance now");
            fundManagerJsonObject.addProperty("jcr:title", fundManagerName + " - Mutual Funds Info: Fund Managers in India - Moneyfy");
            fundManagerJsonObject.addProperty("pageType", "FUND_MANAGER");
            fundManagerJsonObject.addProperty("pageTitle", fundManagerJsonObject.get("name").getAsString());
            if (pageProperties != null) {
                for (Map.Entry<String, JsonElement> entry : fundManagerJsonObject.entrySet()) {
                    final String propKey = entry.getKey();
                    final JsonElement jsonElement = entry.getValue();
                    if (jsonElement.isJsonPrimitive()) {
                        pageProperties.setProperty(propKey, jsonElement.getAsString());
                    } else {
                        pageProperties.setProperty(propKey, jsonElement.toString());
                    }
                }
                session.save();
            }
        }
    }

    @Override
    public boolean purgeCdn() {
        try {
            URIBuilder uriBuilder = new URIBuilder(moneyfyPageGenConfigService.getCdnUrl());
            log.info("CDN API URL : {}",uriBuilder);
            HttpClient httpClient = httpClientBuilderFactory.newBuilder().build();
            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Authorization", moneyfyPageGenConfigService.getCdnPurgeToken());

            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("domain", moneyfyPageGenConfigService.getCdnDomainUrl());
            jsonObject.addProperty("groupName", "Other");

            JsonArray jsonArray = new JsonArray();
            jsonArray.add(moneyfyPageGenConfigService.getCdnPagePurgeUrl());

            jsonObject.add("mediaPaths", jsonArray);

            httpPost.setEntity(new StringEntity(jsonObject.toString()));
            log.info("CDN API REQUEST : {}",jsonObject);
            HttpResponse httpResponse = httpClient.execute(httpPost);
            log.info("CDN API Status : {}",httpResponse.getStatusLine().getStatusCode());

            BufferedReader br = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String output;
            String responseJsonString = "";
            while ((output = br.readLine()) != null) {
                responseJsonString += output;
            }
            log.info("CDN API RESPONSE DATA : {}",responseJsonString);

            return true;
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public String getPageGenerationData(String transactionId) {
        try
        {
            log.info(" SHAFT Generate AEMJson API calling start");
            URIBuilder uriBuilder = new URIBuilder(moneyfyPageGenConfigService.getPageGenDataUrl());
            log.info("API URL $$$$--> "+uriBuilder);
            HttpClient httpClient = httpClientBuilderFactory.newBuilder().build();
            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Cookie","Authorization=".concat(moneyfyPageGenConfigService.getPageGenAuthorization()));
            JsonObject requestBody = new JsonObject();

            JsonObject jsonHeader = new JsonObject();
            jsonHeader.addProperty("authToken",moneyfyPageGenConfigService.getPageGenAuthorization());

            JsonObject jsonBody = new JsonObject();
            jsonBody.addProperty("transactionId",transactionId);
            requestBody.add("header", jsonHeader);
            requestBody.add("body", jsonBody);
            log.info("request {} :",requestBody);

            httpPost.setEntity(new StringEntity(requestBody.toString()));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            log.info("RESPONSE $$$$ -> {}",httpResponse.getStatusLine().getStatusCode());

            BufferedReader br = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String output;
            String responseJsonString = "";
            while ((output = br.readLine()) != null) {
                responseJsonString += output;
            }
            /*            log.info("##### responseString Data : " + responseJsonString);*/
            log.debug("AEM Json Data: {} ",responseJsonString);
            return responseJsonString;
        }
        catch (Exception e)
        {
            log.error("Exception in getPageGenerationData() : {}",e.getMessage());
        }

        return null;
    }

    public String transactionCall(JsonObject jsonObject,Map<String,String> processTimeMap)
    {
        try {
            URIBuilder uriBuilder = new URIBuilder(moneyfyPageGenConfigService.getUri());
            log.info("API URL $$$$--> {}", uriBuilder);
            HttpClient httpClient = httpClientBuilderFactory.newBuilder().build();
            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Cookie","Authorization=".concat(moneyfyPageGenConfigService.getPageGenAuthorization()));

            JsonObject jsonRequest = new JsonObject();
            JsonObject jsonHeader = new JsonObject();
            JsonObject jsonBody = new JsonObject();

            jsonHeader.addProperty("authToken", moneyfyPageGenConfigService.getPageGenAuthorization());
            jsonBody.addProperty("transactionId",processTimeMap.get("transactionId"));
            jsonBody.addProperty("jobId" ,processTimeMap.get("jobId"));
            jsonBody.addProperty("topicName",processTimeMap.get("topic"));
            jsonBody.addProperty("totalProcessTimeInMS",processTimeMap.get("totalTimeTaken"));
            jsonBody.addProperty("isCdnPurged",processTimeMap.get("isCdnPurged"));
            jsonBody.addProperty("isPagesReplicated",processTimeMap.get("isPagesReplicated"));

            JsonObject amcDetailsJson = new JsonObject();
            amcDetailsJson.addProperty("totalPages",jsonObject.getAsJsonArray("amcMaster").size() );
            amcDetailsJson.addProperty("totalPagesCreated",amcPageCounter );
            amcDetailsJson.addProperty("totalPagesFailed",amcPageNotCreatedCounter );
            amcDetailsJson.addProperty("processingTimeInMS",processTimeMap.get("pageAMC") );

            JsonObject fundDetailsJson = new JsonObject();
            fundDetailsJson.addProperty("totalPages",jsonObject.getAsJsonArray("fundMaster").size() );
            fundDetailsJson.addProperty("totalPagesCreated",fundCounter );
            fundDetailsJson.addProperty("totalPagesFailed",fundPageNotCreatedCounter );
            fundDetailsJson.addProperty("processingTimeInMS",processTimeMap.get("pageFund") );

            JsonObject fundManagerDetailsJson = new JsonObject();
            fundManagerDetailsJson.addProperty("totalPages",jsonObject.getAsJsonArray("fundManagerMaster").size());
            fundManagerDetailsJson.addProperty("totalPagesCreated",fundManagerCounter );
            fundManagerDetailsJson.addProperty("totalPagesFailed", fundManagerPageNotCreatedCounter);
            fundManagerDetailsJson.addProperty("processingTimeInMS",processTimeMap.get("pageFundManager"));

            JsonObject sebiDetailsJson = new JsonObject();
            sebiDetailsJson.addProperty("totalPages",jsonObject.getAsJsonArray("sebiCategoryMaster").size());
            sebiDetailsJson.addProperty("totalPagesCreated",sebiCounter );
            sebiDetailsJson.addProperty("totalPagesFailed",sebiPageNotCreatedCounter );
            sebiDetailsJson.addProperty("processingTimeInMS",processTimeMap.get("pageSEBI"));

            jsonBody.add("amcDetails",amcDetailsJson);
            jsonBody.add("fundDetails",fundDetailsJson);
            jsonBody.add("fundManagerDetails",fundManagerDetailsJson);
            jsonBody.add("sebiDetails",sebiDetailsJson);


            jsonRequest.add("header", jsonHeader);
            jsonRequest.add("body",jsonBody);
            log.info("Request Body for Transaction Call :",jsonRequest);

            httpPost.setEntity(new StringEntity(jsonRequest.toString()));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            log.info("RESPONSE $$$$ -> ",httpResponse.getStatusLine().getStatusCode());

            BufferedReader br = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));

            String output;
            String responseJsonString = "";
            while ((output = br.readLine()) != null) {
                responseJsonString += output;
            }
            log.info("##### responseString Data : " + responseJsonString);
            return responseJsonString;

        }catch (Exception e)
        {
            e.printStackTrace();
        }

        return "";
    }



}
