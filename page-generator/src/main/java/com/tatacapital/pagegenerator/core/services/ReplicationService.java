package com.tatacapital.pagegenerator.core.services;

import java.util.ArrayList;

public interface ReplicationService {

    //boolean replicatePageContent(String path[]);

    boolean replicateContent(String[] path);
}