package com.tatacapital.pagegenerator.core.services;

public interface MoneyfyPageGenConfigService {
    String getUri();
    String getCdnUrl();
    String getCdnPurgeToken();
    String getCdnPagePurgeUrl();
    String getCdnDomainUrl();
    String getPageGenDataUrl();
    String getPageGenAuthorization();
    boolean getReplicationEnabled();
    boolean getBackupEnabled();
}
