package com.tatacapital.pagegenerator.core.jobs;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.pagegenerator.core.services.MoneyfyPageGenConfigService;
import com.tatacapital.pagegenerator.core.services.MoneyfyPageGenerationService;
import com.tatacapital.pagegenerator.core.services.ReplicationService;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.consumer.JobConsumer;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

@Component(service = JobConsumer.class, property = {
        JobConsumer.PROPERTY_TOPICS + "=" + PageGenJobConsumer.TOPIC
})
public class PageGenJobConsumer implements JobConsumer {

    public static final String TOPIC = "moneyfy/pageGen";

    private boolean isGenInProgress = false;

    @Reference
    MoneyfyPageGenerationService moneyfyPageGenerationService;

    @Reference
    MoneyfyPageGenConfigService moneyfyPageGenConfigService;

    @Reference
    ReplicationService replicationService;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public JobResult process(Job job) {
        long start = System.currentTimeMillis();
        List<String> totalGeneratedPages = new ArrayList<>();
        logger.info("PageGen Job Process Started : {}", start);
        try {
            if (isGenInProgress) {
                return JobResult.CANCEL;
            }
            isGenInProgress = true;
            logger.info("Property Names : {}", job.getPropertyNames());

            String transactionIdJsonString = job.getProperty("jsonStr", String.class);
            JsonObject transactionIdJsonObj = new JsonParser().parse(transactionIdJsonString).getAsJsonObject();
            String transactionId = transactionIdJsonObj.get("transactionId").getAsString();
            logger.info("transactionId sent by SHAFT : {}",transactionId);
            String pageGenerationData = moneyfyPageGenerationService.getPageGenerationData(transactionId);
            JsonObject pageGenJsonObj = new JsonParser().parse(pageGenerationData).getAsJsonObject();
            String jobId = job.getId();

            boolean isBackupEnabled = moneyfyPageGenConfigService.getBackupEnabled();

            if(isBackupEnabled) {
                boolean pageRenameStatus = moneyfyPageGenerationService.createAndBackup();
                long pageBkp = System.currentTimeMillis();
                logger.info("PageGen Job Process Backup Done : {} => {}", pageBkp, (pageBkp - start));
            }

            // method to create amc pages
            totalGeneratedPages.addAll(moneyfyPageGenerationService.createAMCPages(pageGenJsonObj)); //method to create amc pages
            long pageAMC = System.currentTimeMillis();
            logger.info("PageGen Job Process AMC Done : {} => {}", pageAMC, (pageAMC - start));

            // method to create fund details pages
            totalGeneratedPages.addAll(moneyfyPageGenerationService.createFundDetailPages(pageGenJsonObj));
            long pageFund = System.currentTimeMillis();
            logger.info("PageGen Job Process FUND Done : {} => {}", pageFund, (pageFund - start));

            // method to create fund manager pages
            totalGeneratedPages.addAll(moneyfyPageGenerationService.createFundMangerPages(pageGenJsonObj));
            long pageFundManager = System.currentTimeMillis();
            logger.info("PageGen Job Process Fund Managers Done : {} => {}", pageFundManager, (pageFundManager - start));

            // method to create SEBI cat pages
            totalGeneratedPages.addAll(moneyfyPageGenerationService.createSEBICatPages(pageGenJsonObj));
            long pageSEBI = System.currentTimeMillis();
            logger.info("PageGen Job Process SEBI Cat Done : {} => {}", pageSEBI, (pageSEBI - start));

            //Replicate created pages to the Publish
            boolean isReplicationEnabled = moneyfyPageGenConfigService.getReplicationEnabled();
            if(isReplicationEnabled) {
                String[] path = totalGeneratedPages.toArray(new String[totalGeneratedPages.size()]);
                boolean flag = replicationService.replicateContent(path);
                long pageReplicated = System.currentTimeMillis();
                logger.info("PageGen Job Process Replication Done : {} => {}", pageReplicated, (pageReplicated - start));

                long transactionEndTime = System.currentTimeMillis();

                boolean purgeCdn = moneyfyPageGenerationService.purgeCdn();

                Map<String, String> transactionDuration = new HashMap<String, String>();

                transactionDuration.put("pageAMC", String.valueOf((pageAMC - start)));
                transactionDuration.put("pageFund", String.valueOf((pageFund - start)));
                transactionDuration.put("pageFundManager", String.valueOf((pageFundManager - start)));
                transactionDuration.put("pageSEBI", String.valueOf((pageSEBI - start)));
                transactionDuration.put("totalTimeTaken", String.valueOf(transactionEndTime - start));
                transactionDuration.put("isCdnPurged", String.valueOf(purgeCdn));
                transactionDuration.put("isPagesReplicated", String.valueOf(flag));
                transactionDuration.put("transactionId", transactionId);
                transactionDuration.put("jobId", jobId);
                transactionDuration.put("topic", TOPIC);

                String responseString = moneyfyPageGenerationService.transactionCall(pageGenJsonObj, transactionDuration);
                logger.info("Response from shaft update api:{}", responseString);
            }

        } catch (Exception e) {
            logger.info("PageGen Error : {}", e.getMessage());

        }
        logger.info("PageGenJobConsumer process executed !!");

        long end = System.currentTimeMillis();
        logger.info("PageGen Job Process Completed : {}", end);
        logger.info("Total Created Pages : {}", totalGeneratedPages.size());
        logger.info("PageGen Job Process Time Taken : {}", (end - start));

        isGenInProgress = false;
        //httpCall();
        return JobResult.OK;

    }

}
