package com.tatacapital.pagegenerator.core.services;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMException;
import com.google.gson.JsonObject;
import org.apache.sling.api.resource.LoginException;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.*;

public interface MoneyfyPageGenerationService {

    List<String> createSEBICatPages(JsonObject pageGenJsonBody);

    List<String> createFundMangerPages(JsonObject pageGenJsonBody);

    List<String> createAMCPages(JsonObject pageGenJsonBody);

    List<String> createFundDetailPages(JsonObject pageGenJsonBody) throws IOException;

    boolean createAndBackup();

    boolean purgeCdn() throws URISyntaxException, IOException;

    String getPageGenerationData(String transactionId);

    String transactionCall(JsonObject pageGenJsonBody,Map<String,String> map);
}

