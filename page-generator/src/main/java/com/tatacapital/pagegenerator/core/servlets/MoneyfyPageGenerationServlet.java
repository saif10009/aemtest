package com.tatacapital.pagegenerator.core.servlets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.pagegenerator.core.jobs.PageGenJobConsumer;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.event.jobs.Job;
import org.apache.sling.event.jobs.JobManager;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@Component(service = Servlet.class, immediate = true, property = {
        Constants.SERVICE_DESCRIPTION + "=Moneyfy Page Generation",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=moneyfyPageCreation"
})
public class MoneyfyPageGenerationServlet extends SlingAllMethodsServlet {

    @Reference
    JobManager jobManager;

    Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("page generator servlet called");
    }

    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        LOGGER.info("In doPost() of MoneyfyPageGenerationServlet");
        response.setContentType("application/json");
        String transactionId = request.getReader().lines().collect(Collectors.joining());
        LOGGER.info("shaft AEMHandler request {} :",transactionId);
        Map<String, Object> props = new HashMap<>();
        props.put("jsonStr", transactionId);
        Job job = jobManager.addJob(PageGenJobConsumer.TOPIC, props);

        JsonObject transactionIdJsonObj = new JsonParser().parse(transactionId).getAsJsonObject();

        JsonObject shaftResponse = new JsonObject();
        shaftResponse.addProperty("Status", "Success");
        shaftResponse.addProperty("Message", "Transaction ID reached to AEM !!");
        shaftResponse.addProperty("jobId", job.getId());
        shaftResponse.addProperty("transactionId", transactionIdJsonObj.get("transactionId").getAsString());
        shaftResponse.addProperty("jobStatusName", job.getJobState().name());
        shaftResponse.addProperty("jobTopicName", job.getTopic());
        shaftResponse.addProperty("jobCreatedInstance", job.getCreatedInstance());
        shaftResponse.addProperty("jobTargetInstance", job.getTargetInstance());
        shaftResponse.addProperty("jobQueueName", job.getQueueName());
        LOGGER.info("shaft response {} :",shaftResponse);
        response.getWriter().println(shaftResponse);

    }

}
