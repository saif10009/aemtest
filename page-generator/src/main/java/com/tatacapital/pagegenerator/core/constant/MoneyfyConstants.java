package com.tatacapital.pagegenerator.core.constant;


import com.day.cq.wcm.api.Page;

public class MoneyfyConstants {
    public static final String FUND_DETAIL_TEPLATE_PATH = "/conf/tata-capital-moneyfy/settings/wcm/templates/fund-detail";
    public static final String ROOT_PATH = "/content/tata-capital-moneyfy/en/";
    public static final String MF_HOME_PAGE_PATH = ROOT_PATH.concat("mutual-funds");
    public static final String AMC_TEMPLATE_PATH = "/conf/tata-capital-moneyfy/settings/wcm/templates/amc-details";
    public static final String MUTUAL_FUND_BACKUP = ROOT_PATH.concat("mutual-funds-bkp");
    public static final String AMC_LISTING_TEMPLATE="/conf/tata-capital-moneyfy/settings/wcm/templates/amc-listing";

    public static final String FUND_MANAGER_PAGE_NAME = "fund-managers";
    public static final String FUND_MANAGER_PAGE_PATH = MF_HOME_PAGE_PATH + "/" + FUND_MANAGER_PAGE_NAME;
    public static final String FUND_MANAGER_TEMPLATE_PATH = "/conf/tata-capital-moneyfy/settings/wcm/templates/fund-manager";
    public static final String FUND_MANAGER_DETAIL_TEMPLATE_PATH = "/conf/tata-capital-moneyfy/settings/wcm/templates/fund-manager-detail";


    public static final String SEBI_CATEGORY_TEMPLATE_PATH = "/conf/tata-capital-moneyfy/settings/wcm/templates/sebi-categories";
    public static final String SEBI_CATEGORY_PAGE_PATH_OF_EQUITY = MF_HOME_PAGE_PATH.concat("/sebi-categories-equity");
    public static final String SEBI_CATEGORY_PAGE_PATH_OF_DEBT = MF_HOME_PAGE_PATH.concat("/sebi-categories-debt");
    public static final String SEBI_CATEGORY_PAGE_PATH_OF_HYBRID = MF_HOME_PAGE_PATH.concat("/sebi-categories-hybrid");
    public static final String SEBI_CATEGORY_PAGE_PATH_OF_OTHERS = MF_HOME_PAGE_PATH.concat("/sebi-categories-others");

    public static final String DEFAULT_ICON_PATH = "/content/dam/tata-capital-moneyfy/amc-icons/moneyfy-default-fund-icon.png";


}
