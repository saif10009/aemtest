package com.tatacapital.pagegenerator.core.servlets;

import com.tatacapital.pagegenerator.core.services.ReplicationService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;

@Component(service = Servlet.class,immediate = true,property = {
        Constants.SERVICE_DESCRIPTION+"=Replication Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=ReplicationServlet"
})
public class ReplicationServlet extends SlingAllMethodsServlet {



    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        response.getWriter().println("hi");
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {


        response.getWriter().println("hello");

        String page[] = {"/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/sailesh-raj-bhan","/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/neeraj-kumar","/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/manoj-kumar","/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/santosh-shukla"};



    }
}
