package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.*;
import com.tatacapital.moneyfy.core.models.pojo.AmcListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.text.NumberFormat;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AMCListingModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private List<AmcListingPojo> amcList = new ArrayList<>();

    Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {

        try {
            Resource resource = resourceResolver.getResource("/content/tata-capital-moneyfy/en/mutual-funds/");

            if (resource != null) {

                Page parentPage = resource.adaptTo(Page.class);
                if (parentPage != null) {

                    Iterator<Page> listChildPages = parentPage.listChildren();

                    while (listChildPages.hasNext()) {

                        Page childPage = listChildPages.next();
                        if (childPage.getProperties().get("pageType").equals("AMC")) {
                            AmcListingPojo amcListingPojo = new AmcListingPojo();
                            String aumString = !childPage.getProperties().get("aum").toString().equals("") ? childPage.getProperties().get("aum").toString():"0";
                            amcListingPojo.setAum(aumString);
                            amcListingPojo.setFundName(childPage.getProperties().get("name").toString());
                            amcListingPojo.setHighestReturns(childPage.getProperties().get("highestReturns").toString());
                            amcListingPojo.setNumberOfFunds(childPage.getProperties().get("totalFunds").toString());
                            amcListingPojo.setAmcId(childPage.getProperties().get("amcId").toString());
                            amcListingPojo.setPagePath(childPage.getPath().toString().concat(".html"));
                            amcListingPojo.setIconPath(childPage.getProperties().get("iconPath").toString());
                            amcList.add(amcListingPojo);
                        }
                    }

                }

            }

        } catch (Exception e) {

            log.info("Exception : {}", e);
            e.printStackTrace();
        }

    }

    public List<AmcListingPojo> getAmcList() {
        amcList.size();
        return amcList;

    }

    public String getAmcFundList() {
        Gson gson = new Gson();
        String amcFundList = gson.toJson(amcList);
        return amcFundList;
    }

    public List getExpFundThroughAmc(){
        List<AmcListingPojo> expFundAmcList= new ArrayList<>();
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [nt:base] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='AMC' AND s.[exploreSeq] IS NOT NULL ORDER BY s.[exploreSeq]";
            stringBuffer.append(fundManagerQuery);
            log.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            log.info("Asset query : " + stringBuffer.toString());
            query.setLimit(6);
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                AmcListingPojo amcListingPojo = new AmcListingPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                //fundListingPojo.setIconPath(!childPage.getProperties().get("iconPath").equals("") ? childPage.getProperties().get("iconPath").toString() : "");
                if (Objects.nonNull(page)) {
                    amcListingPojo.setFundName(page.getProperties().get("pageTitle").toString());
                    amcListingPojo.setNumberOfFunds(page.getProperties().get("totalFunds").toString());
                    amcListingPojo.setHighestReturns(page.getProperties().get("highestReturns").toString());
                    amcListingPojo.setPagePath(page.getPath().concat(".html"));
                    amcListingPojo.setIconPath(page.getProperties().get("iconPath").toString());
                    String aumString = !page.getProperties().get("aum").toString().equals("") ? page.getProperties().get("aum").toString():"0";
                    String aum = format(aumString);
                    amcListingPojo.setAum(aum);

                    expFundAmcList.add(amcListingPojo);
                }
            }

        } catch (RepositoryException e) {
            log.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }
        return expFundAmcList;
    }

    public String format(String value){
        Float currencyValueConvertIntoFloat = Float.valueOf(value);
        int currencyValueRoundOf = Math.round(currencyValueConvertIntoFloat);
        String currencyValueInString = String.valueOf(currencyValueRoundOf);

        String temp ="";
        //Removing extra '0's
        while(currencyValueInString.substring(0,1).equals("0") && currencyValueInString.length()>1) {
            currencyValueInString = currencyValueInString.substring(1);
        }

        if(currencyValueInString.length()<=3){
            return currencyValueInString;

        }

        else if(currencyValueInString.length()%2 !=0) {
            temp = currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);}

        else {
            temp = currencyValueInString.substring(0, 1);
            currencyValueInString = currencyValueInString.substring(1);
        }
        while (currencyValueInString.length() > 3) {
            temp += "," + currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);

        }

        return temp+","+currencyValueInString;
    }


}
