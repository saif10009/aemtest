package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class InstantRedemptionFundListingModel {

    @Inject
    @Default(values = "")
    String pageName;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private List<FundListingPojo> fundList = new ArrayList<>();

    FundListingPojo fundListingPojo;
    Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {

        String pagePath = "/content/tata-capital-moneyfy/en/mutual-funds/";
        pagePath = pagePath.concat(getPageNodeName(pageName));
        try {
            Resource resource = resourceResolver.getResource(pagePath);
            if (resource != null) {

                Page parentPage = resource.adaptTo(Page.class);
                if (parentPage != null) {

                    Iterator<Page> listChildPages = parentPage.listChildren();

                    while (listChildPages.hasNext()) {

                        Page childPage = listChildPages.next();
                        if (childPage.getProperties().get("pageType").equals("FUND")) {
                            fundListingPojo = new FundListingPojo();
                            //fundListingPojo.setIconPath(!childPage.getProperties().get("iconPath").equals("") ? childPage.getProperties().get("iconPath").toString() : "");
                            fundListingPojo.setPagePath(childPage.getPath().concat(".html"));
                            JsonObject schemeDetails = new JsonParser().parse(childPage.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                            JsonObject ratings = new JsonParser().parse(childPage.getProperties().get("ratings").toString()).getAsJsonObject();
                            fundListingPojo.setFundName(schemeDetails.get("name").getAsString());
                            fundListingPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                            float nav = !childPage.getProperties().get("nav").toString().equals("") ? Float.parseFloat(childPage.getProperties().get("nav").toString()) : 0;
                            nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                            fundListingPojo.setNav(nav);
                            fundListingPojo.setCagrValues(childPage.getProperties().get("cagrValues").toString());
                            fundListingPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                            //fundListingPojo.setFundManagerName(childPage.getProperties().get("fundManagerName").toString());
                            fundListingPojo.setMorningStar(ratings.get("morningStar").getAsString());
                            fundListingPojo.setValueResearch(ratings.get("valueResearch").getAsString());

                            fundListingPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                            String minSipAmt = !childPage.getProperties().get("minSipAmt").toString().equals("") ? childPage.getProperties().get("minSipAmt").toString() : "0";
                            String minLumpSumAmt = Objects.nonNull(childPage.getProperties().get("minLumpsumAmt")) ?
                                    (childPage.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : childPage.getProperties().get("minLumpsumAmt").toString()) : "0";
                            fundListingPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                            fundListingPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                            String fundSize = !childPage.getProperties().get("fundSize").toString().equals("") ? childPage.getProperties().get("fundSize").toString() : "0";
                            fundListingPojo.setFundSize(Float.valueOf(fundSize));
                            fundListingPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                            fundList.add(fundListingPojo);
                        }
                    }


                }

            }

        } catch (Exception e) {

            log.info("Exception : {}", e);
            e.printStackTrace();
        }

    }
    private String getPageNodeName(String fundName) {

        String finalPageName = fundName.toLowerCase().replace(" ", "-");

        if (finalPageName.contains("*")) {
            String finalPage = finalPageName.replace("*", "-");
            return finalPage;
        } else if (finalPageName.contains("&")) {

            String finalPage = finalPageName.replace("&", "-and-");
            return finalPage;
        }

        return finalPageName;
    }

    public List<FundListingPojo> getFundList() {
        fundList.size();
        return fundList;
    }


    public String getStringFundList() {
        Gson gson = new Gson();
        String jsonStringFundList = gson.toJson(fundList);
        return jsonStringFundList;
    }
/*
    public Map<String,String> getCagrVal(){
        Map<String, String> cagrObj = new HashMap<>();
        try {
            JsonArray cagrArray = new JsonParser().parse(fundListingPojo.getCagrValues()).getAsJsonArray();
            cagrArray.forEach(element -> {
                JsonObject jsonObject = element.getAsJsonObject();
                cagrObj.put(jsonObject.get("tenure").getAsString(), jsonObject.get("value").getAsString());
            });
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return cagrObj;
    }

 */
public List<Map<String,String>> getAmcFundListingCagr(){
    ArrayList fundCagrList = new ArrayList();
    List<FundListingPojo> similarFunds = getFundList();
//    Map<String, String> cagrObj = new HashMap<>();
    try {
        for(int i=0; i<similarFunds.size(); i++){
            Map<String, Float> cagrObj = new HashMap<>();
            JsonArray cagrArray = new JsonParser().parse(similarFunds.get(i).getCagrValues()).getAsJsonArray();
            cagrArray.forEach(element -> {
                JsonObject jsonObject = element.getAsJsonObject();
                cagrObj.put(jsonObject.get("tenure").getAsString(), new Float(jsonObject.get("value").getAsString()));
            });
            fundCagrList.add(cagrObj);
        }
    }catch (Exception e){
        log.error(e.getMessage());
    }
    return fundCagrList;
}
}

