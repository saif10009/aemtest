
package com.tatacapital.moneyfy.core.service.impl;

import com.tatacapital.moneyfy.core.models.ConfigModel;
import com.tatacapital.moneyfy.core.services.MoneyfyConfigs;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Designate(ocd = MoneyfyConfigsImpl.Config.class)
@Component(service= MoneyfyConfigs.class, immediate = true)
public class MoneyfyConfigsImpl implements MoneyfyConfigs {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private Map<String, String> configMap;

  @ObjectClassDefinition(name = "TATA Capital Moneyfy Configuration", description = "TATA Capital Moneyfy Configuration")
  public static @interface Config {

    @AttributeDefinition(name = "FA ID", required = true, description = "Set FollowAnalytics Id")
    String followAnalyticsId() default "Add FA ID";

    @AttributeDefinition(name = "Bundle ID", required = true, description = "Set FollowAnalytics Bundle")
    String followAnalyticsBundle() default "Add Bundle ID";

    @AttributeDefinition(name = "Default Icon", required = true, description = "Set FollowAnalytics Default Icon")
    String followAnalyticsDefaultIcon() default "/content/dam/tata-capital-moneyfy/MoneyfyMLogo-02.jpg";

    @AttributeDefinition(name = "Dtm Script", required = true, description = "Add Dtm Script here")
    String dtmScript() default "https://assets.adobedtm.com/e4c76be5b9e7/cf191e3ffdcb/launch-e48e28a6924b-development.min.js";

    @AttributeDefinition(name = "Jocata Domain URL ", required = true, description = "Add Jocata Domain URL here")
    String jocataDomainUrl() default "";

    @AttributeDefinition(name = "Moengage App Id ", description = "Add Moengage App Id here")
    String moengageAppId() default "PLBRDCVUS0YE8ME3E8XSHV5D_DEBUG";

    @AttributeDefinition(name = "Debug log ", description = "Add Debug log here")
    String debugLog() default "1";

    @AttributeDefinition(name = "Shaft Host URL", description = "Set shaft host url")
    String shaftHostUrl() default "http://172.27.16.52:7505";

    @AttributeDefinition(name = "Shaft Auth Token",description = "Set shaft auth token url")
    String shaftAuthToken() default "MTI4OjoxMDAwMDo6NGVjOTUxNDU5MWY2NzE0NzZiNDc1YjhlMmZkOTNjNzY6OmNkMWYxMzdjNTU0NjBmOWY1NTFlNjdmYjJmYWRhMTQxOjowL2I0NW5qd2dVeUU1UjkrYk93aFYrMnZ3cEh3cVpLUnVoK1VyQjdTNXpJPQ==";

    @AttributeDefinition(name = "Shaft Email Notification URI",description = "Set Shaft Email Notification URI here")
    String shaftEmailNotificationUri() default "/partner/api/services/notification/email-notification";

    @AttributeDefinition(name = "SenseForth Script URL", description = "Enter SenseForth Script URL Here")
    String senseForthScriptUrl() default "https://smartsearch.senseforth.com/TataCapMLSmartSearch/TataCap-SmartSearch/sf-smart-search-loader.js";
    
    @AttributeDefinition(name = "MDM Calc Form Detail Url",description = "Enter MDM Calc Form Detail Url here")
    String mdmCalcDetailUrl() default "https://tclu.tatacapital.com/web/api/mdm/calcformdetails";

  }

  @Activate
  @Modified
  protected void activate(Config config) {
    configMap = new HashMap<>();
    configMap.put("followAnalyticsId", config.followAnalyticsId());
    configMap.put("followAnalyticsBundle", config.followAnalyticsBundle());
    configMap.put("followAnalyticsDefaultIcon",config.followAnalyticsDefaultIcon());
    configMap.put("dtmScript",config.dtmScript());
    configMap.put("jocataDomainUrl",config.jocataDomainUrl());
    configMap.put("moengageAppId",config.moengageAppId());
    configMap.put("debugLog",config.debugLog());
    configMap.put("shaftHostUrl",config.shaftHostUrl());
    configMap.put("shaftAuthToken",config.shaftAuthToken());
    configMap.put("shaftEmailNotificationUri",config.shaftEmailNotificationUri());
    configMap.put("senseForthScriptUrl",config.senseForthScriptUrl());
    configMap.put("mdmCalcDetailUrl",config.mdmCalcDetailUrl());
    ConfigModel.getInstance().setConfigMap(configMap);
  }

  @Override
  public Map<String, String> getConfig() {
    return configMap;
  }
}