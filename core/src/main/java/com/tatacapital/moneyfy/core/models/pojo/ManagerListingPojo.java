package com.tatacapital.moneyfy.core.models.pojo;

public class ManagerListingPojo {
    private String fundManagerName;
    private String funds;
    private String noOfFunds;
    private String highestReturns;
    private String totalFundSize;
    private String pagePath;
    private String amcId;

    public String getAmcId() {
        return amcId;
    }

    public void setAmcId(String amcId) {
        this.amcId = amcId;
    }

    public String getFundManagerName() {
        return fundManagerName;
    }

    public void setFundManagerName(String fundManagerName) {
        this.fundManagerName = fundManagerName;
    }

    public String getFunds() {
        return funds;
    }

    public void setFunds(String funds) {
        this.funds = funds;
    }

    public String getNoOfFunds() {
        return noOfFunds;
    }

    public void setNoOfFunds(String noOfFunds) {
        this.noOfFunds = noOfFunds;
    }

    public String getHighestReturns() {
        return highestReturns;
    }

    public void setHighestReturns(String highestReturns) {
        this.highestReturns = highestReturns;
    }

    public String getTotalFundSize() {
        return totalFundSize;
    }

    public void setTotalFundSize(String totalFundSize) {
        this.totalFundSize = totalFundSize;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }
}
