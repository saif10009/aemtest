package com.tatacapital.moneyfy.core.services;

import com.google.gson.JsonObject;

public interface CalculatorEmailService {

    JsonObject postCallForCalcEmail(String url,String authToken,JsonObject jsonBody);

}
