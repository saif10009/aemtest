package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.AmcListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.TopPicksPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.*;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class TopPicksModel {
    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    Logger logger = LoggerFactory.getLogger(getClass());

    private List<TopPicksPojo> toppicks = new ArrayList<>();

    TopPicksPojo topPicksPojo;

    @PostConstruct
    public void init() {
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='FUND' ";
            stringBuffer.append(fundManagerQuery);
            logger.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Asset query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                topPicksPojo = new TopPicksPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                JsonObject schemeDetails = new JsonParser().parse(page.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                if (schemeDetails.get("isMoneyfyTopPicks").toString().toUpperCase().contains("Y")) {
                    topPicksPojo.setFundName(schemeDetails.get("name").getAsString());
                    topPicksPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                    topPicksPojo.setCatName(schemeDetails.get("catName").getAsString());
                    topPicksPojo.setSchemeId(schemeDetails.get("id").getAsString());
                    float nav = !page.getProperties().get("nav").toString().equals("") ? Float.parseFloat(page.getProperties().get("nav").toString()) : 0;
                    nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                    topPicksPojo.setNav(nav);
                    topPicksPojo.setPagePath(page.getPath().concat(".html"));
                    String updatedDate = !page.getProperties().get("updatedDate").equals("") ? page.getProperties().get("updatedDate").toString() : "-";
                    topPicksPojo.setUpdatedDate(updatedDate);
                    topPicksPojo.setIconPath(page.getParent().getProperties().get("iconPath").toString());
                    JsonArray cagrJsonArray = new JsonParser().parse(page.getProperties().get("cagrValues").toString()).getAsJsonArray();
                    cagrJsonArray.forEach(jsonElement -> {
                        JsonObject cagrObj = jsonElement.getAsJsonObject();
                        if (cagrObj.get("tenure").toString().contains("1y")) {
                            topPicksPojo.setCagr1y(jsonElement.getAsJsonObject().toString());

                        }
                        if (cagrObj.get("tenure").toString().contains("3y")) {
                            topPicksPojo.setCagr3y(jsonElement.getAsJsonObject().toString());
                        }
                    });
                    toppicks.add(topPicksPojo);
                }
            }

        } catch (AccessDeniedException e) {
            e.printStackTrace();
        } catch (InvalidQueryException e) {
            e.printStackTrace();
        } catch (ItemNotFoundException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
    }

    public String getTopPicksList() {
        Gson gson = new Gson();
        return gson.toJson(toppicks);
    }

}

