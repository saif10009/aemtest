package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.tatacapital.moneyfy.core.models.pojo.SEBIPageDetailsPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SEBIPageListingModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private List<SEBIPageDetailsPojo> dataFromSEBIModelPageList = new ArrayList<>();

    Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init()
    {
        try {
            Resource resource =resourceResolver.getResource("/content/tata-capital-moneyfy/us/en/sebi");
            if(resource != null)
            {
                Page parentPage=resource.adaptTo(Page.class);
                if(parentPage!=null)
                {
                    Iterator<Page> listChildPages=parentPage.listChildren();
                    while (listChildPages.hasNext()) {
                        Page childPage = listChildPages.next();
                        SEBIPageDetailsPojo sebiPageDetailsPojo = new SEBIPageDetailsPojo();
                        sebiPageDetailsPojo.setCatId(childPage.getProperties().get("catId").toString());
                        sebiPageDetailsPojo.setDescription(childPage.getProperties().get("description").toString());
                        sebiPageDetailsPojo.setIconPath(childPage.getProperties().get("iconPath").toString());
                        sebiPageDetailsPojo.setName(childPage.getProperties().get("name").toString());
                        dataFromSEBIModelPageList.add(sebiPageDetailsPojo);
                    }
                }
            }

        }
        catch (Exception e)
        {
            log.info("Exception : {}",e);
            e.printStackTrace();
        }
    }
    public List<SEBIPageDetailsPojo> getDataFromSEBIModelPageList() {

        dataFromSEBIModelPageList.size();
        return dataFromSEBIModelPageList;
    }


}
