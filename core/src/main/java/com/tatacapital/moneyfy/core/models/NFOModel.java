package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.NFOPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class NFOModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;
    Logger logger = LoggerFactory.getLogger(getClass());
    private List<NFOPojo> nfoOpenList = new ArrayList<>();
    private List<NFOPojo> nfoUpcomingList = new ArrayList<>();

    @PostConstruct
    public void init() {
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String nfoQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds]) and s.[pageType]='FUND'";
            stringBuffer.append(nfoQuery);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Query For NFO Fund :{}",stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                NFOPojo nfoPojo = new NFOPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                JsonObject schemeDetails = new JsonParser().parse(page.getProperties().get("schemeDetails").toString()).getAsJsonObject();

                String openingDate = !page.getProperties().get("issueOpenDate").toString().trim().equals("") && !page.getProperties().get("issueOpenDate").toString().equals("null") ? page.getProperties().get("issueOpenDate").toString() : "0";
                String closingDate = !page.getProperties().get("issueCloseDate").toString().trim().equals("") && !page.getProperties().get("issueCloseDate").toString().equals("null") ? page.getProperties().get("issueCloseDate").toString() : "0";
                long currentDate = Calendar.getInstance().getTimeInMillis();
                long openDate = Long.parseLong(openingDate);
                String todaysDate = createDate(String.valueOf(currentDate));
                String openingDateOfFund = createDate(String.valueOf(openDate));
                long closeDate = Long.parseLong(closingDate);

                if (page.getProperties().get("isNfo").equals("1")) {
                    nfoPojo.setFundName(schemeDetails.get("name").getAsString());
                    nfoPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                    nfoPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                    nfoPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                    nfoPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                    String minSipAmt = !page.getProperties().get("minSipAmt").toString().equals("") ? page.getProperties().get("minSipAmt").toString() : "0";
                    String minLumpSumAmt = Objects.nonNull(page.getProperties().get("minLumpsumAmt")) ?
                            (page.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : page.getProperties().get("minLumpsumAmt").toString()) : "0";
                    nfoPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                    nfoPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                    nfoPojo.setIconPath(page.getParent().getProperties().get("iconPath").toString());
                    nfoPojo.setOpeningDate(createDate(openingDate));
                    nfoPojo.setClosingDate(createDate(closingDate));
                    nfoPojo.setPagePath(page.getPath().concat(".html"));
                    nfoOpenList.add(nfoPojo);
                } else if (page.getProperties().get("isNfo").equals("2")) {
                    nfoPojo.setFundName(schemeDetails.get("name").getAsString());
                    nfoPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                    nfoPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                    nfoPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                    nfoPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                    String minSipAmt = !page.getProperties().get("minSipAmt").toString().equals("") ? page.getProperties().get("minSipAmt").toString() : "0";
                    String minLumpSumAmt = Objects.nonNull(page.getProperties().get("minLumpsumAmt")) ?
                            (page.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : page.getProperties().get("minLumpsumAmt").toString()) : "0";
                    nfoPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                    nfoPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                    nfoPojo.setIconPath(page.getParent().getProperties().get("iconPath").toString());
                    nfoPojo.setOpeningDate(createDate(openingDate));
                    nfoPojo.setClosingDate(createDate(closingDate));
                    nfoPojo.setPagePath(page.getPath().concat(".html"));
                    long dayLeft = countOfDayLeft(todaysDate, openingDateOfFund);
                    nfoPojo.setCount(dayLeft);
                    nfoUpcomingList.add(nfoPojo);
                }
            }
        } catch (RepositoryException e) {
            logger.error("Exception in NFO Model : {}",e.getMessage());
        }


    }

    public List<NFOPojo> getNfoOpenList() {
        nfoOpenList.size();
        return nfoOpenList;
    }

    public List<NFOPojo> getNfoUpcomingList() {
        nfoUpcomingList.size();
        return nfoUpcomingList;
    }

    public String createDate(String a) {
        DateFormat format = new SimpleDateFormat("dd MMM, yyyy");
        long milliSeconds = Long.parseLong(a);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return format.format(calendar.getTime());
    }

    public long countOfDayLeft(String openedDate, String currenDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM, yyyy");
        try {
            Date dateOfOpeningDate = sdf.parse(openedDate);
            Date dateOfCurrentDate = sdf.parse(currenDate);
            long difference_In_Time = dateOfCurrentDate.getTime() - dateOfOpeningDate.getTime();
            long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
            return difference_In_Days;
        } catch (Exception e) {
            logger.error("Exception in NFO Model in countOfDayLeft method : {}",e.getMessage());
        }
        return 0;
    }


}
