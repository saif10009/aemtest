package com.tatacapital.moneyfy.core.service.impl;

import com.tatacapital.moneyfy.core.services.ResourceHelper;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ResourceResolverFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Component(service= ResourceHelper.class,immediate = true)
public class ResourceHelperImpl implements ResourceHelper {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Reference
    ResourceResolverFactory resResolverFactory;

    @Override
    public ResourceResolver getResourceResolver() throws LoginException {

        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put(ResourceResolverFactory.SUBSERVICE, "subservice-tcl-moneyfy");
            ResourceResolver resourceResolver = resResolverFactory.getServiceResourceResolver(map);
            logger.info("resourceResolver"+resourceResolver);
            return resourceResolver;
        } catch(Exception e) {
            logger.info("Error on resource"+e.getMessage());
        }
        return null;
    }
}
