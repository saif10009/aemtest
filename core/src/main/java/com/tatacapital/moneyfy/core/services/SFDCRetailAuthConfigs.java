package com.tatacapital.moneyfy.core.services;

import com.tatacapital.moneyfy.core.models.SFDCAuthModel;

public interface SFDCRetailAuthConfigs {
    SFDCAuthModel getAuthModel();
}
