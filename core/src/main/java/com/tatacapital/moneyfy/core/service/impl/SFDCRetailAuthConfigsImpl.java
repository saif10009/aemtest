package com.tatacapital.moneyfy.core.service.impl;

import com.tatacapital.moneyfy.core.models.SFDCAuthModel;
import com.tatacapital.moneyfy.core.services.SFDCRetailAuthConfigs;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Designate(ocd=SFDCRetailAuthConfigsImpl.Config.class)
@Component(service= SFDCRetailAuthConfigs.class)
public class SFDCRetailAuthConfigsImpl implements SFDCRetailAuthConfigs {

    @ObjectClassDefinition(name = "Tata Capital Moneyfy Retail Section SFDC Configs", description = "SFDC Moneyfy Lead Configuration")
    public static @interface Config {

        @AttributeDefinition(name = "SFDC Login URL", required = true, description = "Set salesAuth_prod_url")
        String sfdc_login_url() default "https://test.salesforce.com/services/oauth2/token";

        @AttributeDefinition(name = "Client ID", required = true, description = "Set Client ID")
        String client_id() default "3MVG910YPh8zrcR1yVy5MQ5lXnaKXsLSjchyKSZOkzYKGcQRmNYUGBCliFPTy_FVVjn1oAJxLzmoJm3KinMFI";

        @AttributeDefinition(name = "Client Secret", required = true, description = "Set Client Secret")
        String client_secret() default "7288814731641970699";

        @AttributeDefinition(name = "Grant Type", required = true, description = "Set Grant Type")
        String grant_type() default "password";

        @AttributeDefinition(name = "User Name", required = true, description = "Set User Name")
        String username() default "integrationuser@tatacapital.com.tclsit";

        @AttributeDefinition(name = "Password", required = true, description = "Set Password")
        String password() default "TataCap@1234";

        @AttributeDefinition(name="service uri",required = true,description = "Set Service Uri")
        String serviceUri() default "/services/apexrest/UpdateLeadRec";

    }

    SFDCAuthModel authModel;

    @Activate
    @Modified
    // http://blogs.adobe.com/experiencedelivers/experience-management/osgi_activate_deactivatesignatures/
    protected void activate(Config config) {
        authModel = new SFDCAuthModel();
        authModel.setSfdcLoginURL(config.sfdc_login_url());
        authModel.setClientId(config.client_id());
        authModel.setClientSercret(config.client_secret());
        authModel.setUserName(config.username());
        authModel.setPassword(config.password());
        authModel.setGrantType(config.grant_type());
        authModel.setServiceUri(config.serviceUri());


    }

    @Override
    public SFDCAuthModel getAuthModel() {
        return authModel;
    }


}

