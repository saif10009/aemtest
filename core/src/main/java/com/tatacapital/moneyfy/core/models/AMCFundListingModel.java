package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AMCFundListingModel {

    @Inject
    @Default(values = "")
    String pageName;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private List<FundListingPojo> fundList = new ArrayList<>();

    FundListingPojo fundListingPojo;
    Logger log = LoggerFactory.getLogger(getClass());

    @PostConstruct
    public void init() {

        String pagePath = "/content/tata-capital-moneyfy/en/mutual-funds/";
        pagePath = pagePath.concat(getPageNodeName(pageName));
        try {
            Resource resource = resourceResolver.getResource(pagePath);
            if (resource != null) {

                Page parentPage = resource.adaptTo(Page.class);
                if (parentPage != null) {

                    Iterator<Page> listChildPages = parentPage.listChildren();

                    while (listChildPages.hasNext()) {

                        Page childPage = listChildPages.next();
                        if (childPage.getProperties().get("pageType").equals("FUND")) {
                            fundListingPojo = new FundListingPojo();
                            //fundListingPojo.setIconPath(!childPage.getProperties().get("iconPath").equals("") ? childPage.getProperties().get("iconPath").toString() : "");
                            JsonObject schemeDetails = new JsonParser().parse(childPage.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                            JsonObject ratings = new JsonParser().parse(childPage.getProperties().get("ratings").toString()).getAsJsonObject();

                            if(schemeDetails.get("optName").getAsString().trim().equalsIgnoreCase("g")) {
                                fundListingPojo.setPagePath(childPage.getPath().concat(".html"));
                                fundListingPojo.setFundName(schemeDetails.get("name").getAsString());
                                fundListingPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                                float nav = !childPage.getProperties().get("nav").toString().equals("") && !childPage.getProperties().get("nav").toString().equals("null") ? Float.parseFloat(childPage.getProperties().get("nav").toString()) : 0;
                                nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                                fundListingPojo.setNav(nav);
                                fundListingPojo.setCagrValues(childPage.getProperties().get("cagrValues").toString());
                                fundListingPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                                fundListingPojo.setFundManagerName(childPage.getProperties().get("fundManagerName").toString());
                                fundListingPojo.setMorningStar(ratings.get("morningStar").getAsString());
                                fundListingPojo.setValueResearch(ratings.get("valueResearch").getAsString());
                                fundListingPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                                String minSipAmt = !childPage.getProperties().get("minSipAmt").toString().equals("") ? childPage.getProperties().get("minSipAmt").toString() : "0";
                                String minLumpSumAmt = Objects.nonNull(childPage.getProperties().get("minLumpsumAmt")) ?
                                        (childPage.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : childPage.getProperties().get("minLumpsumAmt").toString()) : "0";
                                fundListingPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                                fundListingPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                                String fundSize = childPage.getProperties().get("fundSize").toString().equals("null") ? "0": childPage.getProperties().get("fundSize").toString().trim().equals("") ? "0" : childPage.getProperties().get("fundSize").toString();
                                fundListingPojo.setFundSize(Float.valueOf(fundSize));
                                fundListingPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                                fundListingPojo.setIconPath(childPage.getParent().getProperties().get("iconPath").toString());
                                JsonArray cagrJsonArray = new JsonParser().parse(childPage.getProperties().get("cagrValues").toString()).getAsJsonArray();
                                cagrJsonArray.forEach(jsonElement -> {
                                    JsonObject cagrObj = jsonElement.getAsJsonObject();
                                    String tenure = cagrObj.get("tenure").getAsString();
                                    if(tenure.equals("1m"))
                                    {
                                       if(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") || jsonElement.getAsJsonObject().get("value").getAsString().trim().equals("") || Float.parseFloat(jsonElement.getAsJsonObject().get("value").getAsString())<=0.0f)
                                       {
                                           fundListingPojo.setGreaterThanZero(false);
                                       }
                                       else
                                       {
                                           fundListingPojo.setGreaterThanZero(true);
                                       }
                                    }
                                    switch (tenure)
                                    {
                                        case "1m":
                                            fundListingPojo.setOneMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;
                                        case "3m":
                                            fundListingPojo.setThreeMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;
                                        case "6m":
                                            fundListingPojo.setSixMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;
                                        case "1y":
                                            fundListingPojo.setOneYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;
                                        case "3y":
                                            fundListingPojo.setThreeYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            fundListingPojo.setThreeYearValue(Float.valueOf(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") ? "0.0" : jsonElement.getAsJsonObject().get("value").getAsString()));
                                            break;
                                        case "5y":
                                            fundListingPojo.setFiveYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;
                                        case "mx":
                                            fundListingPojo.setMaxReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                            break;

                                    }
                                });
                                fundList.add(fundListingPojo);
                            }
                        }
                    }


                }

            }

        } catch (Exception e) {

            log.info("Exception : {}", e);
            e.printStackTrace();
        }

    }
    private String getPageNodeName(String fundName) {

        String finalPageName = fundName.toLowerCase().replace(" ", "-");

        if (finalPageName.contains("*")) {
            String finalPage = finalPageName.replace("*", "-");
            return finalPage;
        } else if (finalPageName.contains("&")) {

            String finalPage = finalPageName.replace("&", "-");
            return finalPage;
        }

        return finalPageName;
    }

    public List<FundListingPojo> getFundList() {
        fundList.size();
         return fundList;
    }

    public List getSort()
    {
        Collections.sort(fundList, new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                FundListingPojo c1 = (FundListingPojo) o1;
                FundListingPojo c2 = (FundListingPojo) o2;
                return c2.getThreeYearValue().compareTo(c1.getThreeYearValue());
            }
        });
        return fundList;
    }




    public String getStringFundList() {
        Gson gson = new Gson();
        String jsonStringFundList = gson.toJson(fundList);
        return jsonStringFundList;
    }
/*
    public Map<String,String> getCagrVal(){
        Map<String, String> cagrObj = new HashMap<>();
        try {
            JsonArray cagrArray = new JsonParser().parse(fundListingPojo.getCagrValues()).getAsJsonArray();
            cagrArray.forEach(element -> {
                JsonObject jsonObject = element.getAsJsonObject();
                cagrObj.put(jsonObject.get("tenure").getAsString(), jsonObject.get("value").getAsString());
            });
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return cagrObj;
    }

 */
public List<Map<String,String>> getAmcFundListingCagr(){
    ArrayList fundCagrList = new ArrayList();
    List<FundListingPojo> similarFunds = getFundList();
//    Map<String, String> cagrObj = new HashMap<>();
    try {
        for(int i=0; i<similarFunds.size(); i++){
            Map<String, Float> cagrObj = new HashMap<>();
            JsonArray cagrArray = new JsonParser().parse(similarFunds.get(i).getCagrValues()).getAsJsonArray();
            cagrArray.forEach(element -> {
                JsonObject jsonObject = element.getAsJsonObject();
                cagrObj.put(jsonObject.get("tenure").getAsString(), new Float(jsonObject.get("value").getAsString()));
            });
            fundCagrList.add(cagrObj);
        }
    }catch (Exception e){
        log.error(e.getMessage());
    }
    return fundCagrList;
}
}

