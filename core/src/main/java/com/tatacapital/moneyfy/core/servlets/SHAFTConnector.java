package com.tatacapital.moneyfy.core.servlets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.services.SHAFTConnectorService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.stream.Collectors;


@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=SHAFT Connector Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tata-capital-moneyfy/shaft-connector" ,
        "sling.servlet.extension=json"
})
public class SHAFTConnector extends SlingAllMethodsServlet {

    private final transient Logger log = LoggerFactory.getLogger(getClass());

    @Reference
    transient SHAFTConnectorService shaftConnectorService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

        response.setContentType("application/json");

        String apiName = request.getResource().getValueMap().get("api.name").toString();
        String apiAuthToken = request.getResource().getParent().getValueMap().get("api.authToken").toString();
        String finalApiUrl = request.getResource().getParent().getValueMap().get("api.domainPath").toString().concat(apiName);

        String jsonBodyStr =  request.getReader().lines().collect(Collectors.joining());

        JsonParser parse = new JsonParser();
        JsonObject requestObject = parse.parse(jsonBodyStr).getAsJsonObject();
        JsonObject header = new JsonObject();

        header.addProperty("authToken",apiAuthToken);
        requestObject.add("header", header);

        String logApiName = apiName.substring(30);

        log.info(logApiName+" api request : {}",requestObject);

        try {

            String jsonResponse = shaftConnectorService.executePostMethod(apiAuthToken,finalApiUrl,requestObject);
            response.getWriter().println(jsonResponse);

        } catch (Exception e){
            e.printStackTrace();
        }

    }
}
