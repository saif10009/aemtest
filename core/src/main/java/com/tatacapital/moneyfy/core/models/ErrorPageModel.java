package com.tatacapital.moneyfy.core.models;

import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

@Model(adaptables= SlingHttpServletRequest.class,defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ErrorPageModel {

    @Self
    SlingHttpServletRequest request;

    @ScriptVariable
    SlingHttpServletResponse response;

    String errorPageUrl;

/*    public String getNliErrorPage()
    {
        return "/content/tata-capital/en/error";
    }*/

    public String getNliErrorPage()
    {
        return "/content/tata-capital-web/en/error";
    }

    public String getMoneyfyErrorPage()
    {
        return "/content/tata-capital-moneyfy/en/error";
    }

    @PostConstruct
    public void init() {

        if(response != null){
            response.setStatus(404);
        }
        String requestURL = request.getRequestURL().toString();
        if(requestURL.contains("content/tata-capital-moneyfy/"))
        {
            errorPageUrl=getMoneyfyErrorPage();
        }
        else
        {
            errorPageUrl=getNliErrorPage();
        }

    }

    public String getErrorPageUrl() {
        return errorPageUrl;
    }


}
