package com.tatacapital.moneyfy.core.servlets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.services.MoneyfyIciciCreditCardJourneyService;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Objects;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Verify Otp Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=verifyOtp"
})
public class MoneyfyIciciCreditCardVerifyOtpServlet extends SlingAllMethodsServlet {

    @Reference
    MoneyfyIciciCreditCardJourneyService moneyfyIciciCreditCardJourneyService;

    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

        JsonObject otpVerifyRequestBody = new JsonObject();
        String otpRefNumber = request.getParameter("otpRefNumber");
        String otpValue = request.getParameter("otpValue");
        log.info("OTP REFERENCE NUMBER : {}",otpRefNumber);
        log.info("OTP VALUE : {}",otpValue);
        if(Objects.isNull(otpRefNumber) || otpRefNumber.trim().equals("") || otpValue.trim().equals(""))
        {
            response.setContentType("application/json");
            response.getWriter().println("{\"retStatus\":\"FAILURE\",\"message\":\"null\",\"errorMessage\":\"OTP Reference Id does not belong to you!\",\"sysErrorCode\":\"ERROTP201\",\"sysErrorMessage\":Error in Verifying OTP\"}");
            return;
        }
        long startTime = System.currentTimeMillis();
        log.info("Otp generation start time :{}", startTime);
        boolean isOtpByPass = moneyfyIciciCreditCardJourneyService.getOtpEnabled();
        if(isOtpByPass) {
            response.setContentType("application/json");
            response.getWriter().println("{\"errorMessage\":\"null\",\"sysErrorMessage\":\"null\",\"sysErrorCode\":\"null\",\"retStatus\":\"SUCCESS\",\"errorMessage\":null,\"otpRefNo\":\"12345\"}");
            log.info("Otp generation End Time :{}",System.currentTimeMillis());
            log.info("OTP Generation Time Difference :{}", (System.currentTimeMillis() - startTime));
        }else{
            otpVerifyRequestBody.addProperty("otpRefNo", otpRefNumber);
            otpVerifyRequestBody.addProperty("otp", otpValue);
            log.info("OPT VERIFICATION REQUEST : {}",otpVerifyRequestBody);
            String otpVerifyApiUrl = moneyfyIciciCreditCardJourneyService.getConfig().get("otpVerfiyApiurl");
            String otpVerifyAuthorizationValue  = moneyfyIciciCreditCardJourneyService.getConfig().get("otpApiAuthorization");
            String otpVerifyApiResponse = moneyfyIciciCreditCardJourneyService.otpApiCall(otpVerifyApiUrl, otpVerifyAuthorizationValue, otpVerifyRequestBody.toString());
            log.info("VERIFY OTP RESPONSE : {}",otpVerifyApiResponse);
            if(Objects.isNull(otpVerifyApiResponse))
            {
                response.getWriter().println("VERIFY API REQUEST BODY IS INVALID");
                return;
            }
            String retStatus = new JsonParser().parse(otpVerifyApiResponse).getAsJsonObject().get("retStatus").getAsString();
            JsonObject responseObj = new JsonObject();
            if(retStatus.equalsIgnoreCase("failure")) {
                responseObj.addProperty("retStatus", "failure");
                responseObj.addProperty("message", "Error in Verifying OTP");
            }else {
                responseObj.addProperty("retStatus", "success");
                responseObj.addProperty("message", "Verification is successfull");
            }
            response.setContentType("application/json");
            response.getWriter().print(otpVerifyApiResponse);
        }
    }
}
