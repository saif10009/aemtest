package com.tatacapital.moneyfy.core.models;
public class SFDCAuthModel {

    String sfdcLoginURL;
    String clientSercret;
    String grantType;
    String userName;
    String password;
    String clientId;

    String bearerToken;
    String instanceURL;

    String closTrackingURL;
    String rlosTrackingURL;
    String serviceUri;

    public boolean isRetail() {
        return retail;
    }
    public void setRetail(boolean retail) {
        this.retail = retail;
    }

    boolean retail;

    boolean authorized = false;


    public String getSfdcLoginURL() {
        return sfdcLoginURL;
    }
    public void setSfdcLoginURL(String sfdcLoginURL) {
        this.sfdcLoginURL = sfdcLoginURL;
    }
    public String getClientSercret() {
        return clientSercret;
    }
    public void setClientSercret(String clientSercret) {
        this.clientSercret = clientSercret;
    }
    public String getGrantType() {
        return grantType;
    }
    public void setGrantType(String grantType) {
        this.grantType = grantType;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getClientId() {
        return clientId;
    }
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    public String getBearerToken() {
        return bearerToken;
    }
    public void setBearerToken(String bearerToken) {
        this.authorized = true;
        this.bearerToken = bearerToken;
    }
    public String getInstaceURL() {
        return instanceURL;
    }
    public void setInstanceURL(String instaceURL) {
        this.instanceURL = instaceURL;
    }

    public boolean getAuthorized() {
        return authorized;
    }
    public String getClosTrackingURL() {
        return closTrackingURL;
    }
    public void setClosTrackingURL(String closTrackingURL) {
        this.closTrackingURL = closTrackingURL;
    }
    public String getRlosTrackingURL() {
        return rlosTrackingURL;
    }
    public void setRlosTrackingURL(String rlosTrackingURL) {
        this.rlosTrackingURL = rlosTrackingURL;
    }

    public String getServiceUri() {
        return serviceUri;
    }

    public void setServiceUri(String serviceUri) {
        this.serviceUri = serviceUri;
    }
}

