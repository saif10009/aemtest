package com.tatacapital.moneyfy.core.models.pojo;

public class SebiCatPagesListingPojo {
    private String subCatName;
    private String subCatPagePath;

    public String getSubCatName() {
        return subCatName;
    }

    public void setSubCatName(String subCatName) {
        this.subCatName = subCatName;
    }

    public String getSubCatPagePath() {
        return subCatPagePath;
    }

    public void setSubCatPagePath(String subCatPagePath) {
        this.subCatPagePath = subCatPagePath;
    }
}

