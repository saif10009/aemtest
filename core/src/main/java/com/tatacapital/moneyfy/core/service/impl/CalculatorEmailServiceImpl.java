package com.tatacapital.moneyfy.core.service.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.ConfigModel;
import com.tatacapital.moneyfy.core.services.CalculatorEmailService;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Component(service = CalculatorEmailService.class,immediate = true)
public class CalculatorEmailServiceImpl implements CalculatorEmailService {

    ConfigModel configModel = ConfigModel.getInstance();
    Logger logger = LoggerFactory.getLogger(getClass());

    @Reference
    HttpClientBuilderFactory httpClientBuilderFactory;

    @Override
    public JsonObject postCallForCalcEmail(String url, String authToken, JsonObject jsonBody) {
        try {
            URIBuilder uriBuilder = new URIBuilder(url);
            logger.info("Api Uri :{}",uriBuilder);
            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setEntity(new StringEntity(jsonBody.toString()));
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Cookie", "Authorization" + "=" + configModel.getValue("shaftAuthToken").toString());
            CloseableHttpClient client = httpClientBuilderFactory.newBuilder().build();
            logger.info("HTTP Post Request Detail :{}", httpPost);
            HttpResponse httpResponse = client.execute(httpPost);
            logger.info("Api Status Code :{}",httpResponse.getStatusLine().getStatusCode());
            BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
            String output;
            StringBuilder responseJsonString = new StringBuilder();
            while ((output = br.readLine()) != null) {
                responseJsonString.append(output);
            }
            logger.info("SHAFT API Response to Backend :{}",responseJsonString);
            JsonObject updatedResponseJson = new JsonParser().parse(responseJsonString.toString()).getAsJsonObject();
            logger.info("SHAFT API Response to Frontend :{}",updatedResponseJson);
            return updatedResponseJson;
        }catch (Exception e)
        {
            logger.error("Exception in CalculatorEmailServiceImpl in postCallForCalcEmail method :{}",e.getMessage());
        }
        return new JsonObject();
    }
}
