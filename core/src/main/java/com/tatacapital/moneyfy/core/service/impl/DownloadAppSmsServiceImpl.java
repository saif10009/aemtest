package com.tatacapital.moneyfy.core.service.impl;

import com.tatacapital.moneyfy.core.models.ConfigModel;
import com.tatacapital.moneyfy.core.services.DownloadAppSmsService;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

@Designate(ocd = DownloadAppSmsServiceImpl.DownloadAppSmsConfig.class)
@Component(service = DownloadAppSmsService.class, immediate = true)
public class DownloadAppSmsServiceImpl implements DownloadAppSmsService {

    private Logger log = LoggerFactory.getLogger(getClass());
    private Map<String, String> configMap;

    @ObjectClassDefinition(name = "Download App Sms Configuration", description = "Download App Sms Configuration")
    public @interface DownloadAppSmsConfig {

        @AttributeDefinition(name = "Download App SMS Url",description = "Add download app SMS url here")
        String download_app_sms_url() default "https://smsprod.tatacapital.com/BE_SMSHUBOCT/messagereceiver";

        @AttributeDefinition(name = "Download App SMS Message",description = "Add download app SMS message here")
        String download_app_sms_message() default "Thank you for your interest in the Tata Capital Monefy App. Click on the below link to download the app http://tiny.cc/swc9jz";

    }

    @Activate
    @Modified
    public void activate (DownloadAppSmsConfig downloadAppSmsConfig) {

        configMap = new HashMap<>();
        configMap.put("downloadAppSmsUrl",downloadAppSmsConfig.download_app_sms_url());
        configMap.put("downloadAppSmsMessage",downloadAppSmsConfig.download_app_sms_message());

        ConfigModel.getInstance().setConfigMap(configMap);
    }

    @Override
    public Map<String, String> getConfig() {

        return configMap;
    }

}
