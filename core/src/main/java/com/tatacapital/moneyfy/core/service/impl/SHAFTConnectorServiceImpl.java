package com.tatacapital.moneyfy.core.service.impl;

import com.google.gson.JsonObject;
import com.tatacapital.moneyfy.core.services.SHAFTConnectorService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.osgi.framework.ServiceException;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Arrays;

@Component(service = SHAFTConnectorService.class, immediate = true)
public class SHAFTConnectorServiceImpl implements SHAFTConnectorService {

    @Reference
    HttpClientBuilderFactory httpClientBuilderFactory;

    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    public String executePostMethod(String apiAuthToken, String finalApiUrl,JsonObject requestJson) {

        try{
            URIBuilder uriBuilder = new URIBuilder(finalApiUrl);
            HttpPost httpPost = new HttpPost(uriBuilder.build());
            httpPost.setEntity(new StringEntity(requestJson.toString()));
            httpPost.setHeader("Content-Type", "application/json");
            httpPost.setHeader("Cookie","Authorization=".concat(apiAuthToken));
            HttpClient httpClient = httpClientBuilderFactory.newBuilder().build();
            HttpResponse httpResponse = httpClient.execute(httpPost);

            Integer httpStatusCode = httpResponse.getStatusLine().getStatusCode();
            log.info("Status code : {}",httpStatusCode);

            if (!Arrays.asList(new Integer[]{200, 201}).contains(httpStatusCode)) {
                log.info("Response failed with status code : {} & reason : {}", httpStatusCode, httpResponse);
                throw new ServiceException("Api failed");
            }else{

                BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
                String output;
                StringBuilder responseJsonString = new StringBuilder();
                log.info("Http response : {}",httpResponse);

                while ((output = br.readLine()) != null) {
                    responseJsonString.append(output);
                }

                log.info("Http API Response to Frontend : {}",responseJsonString);

                return responseJsonString.toString();

            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
}
