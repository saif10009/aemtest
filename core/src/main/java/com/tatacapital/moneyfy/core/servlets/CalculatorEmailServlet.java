package com.tatacapital.moneyfy.core.servlets;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.ConfigModel;
import com.tatacapital.moneyfy.core.services.CalculatorEmailService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Calculator Email Servlet",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=calcEmailServlet"
})
public class CalculatorEmailServlet extends SlingAllMethodsServlet {

    Logger logger = LoggerFactory.getLogger(getClass());

    @Reference
    CalculatorEmailService calculatorEmailService;

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        logger.info("In doPost method in CalculatorEmailServlet :{0}");

        String requestStr = request.getReader().lines().collect(Collectors.joining());

        if(StringUtils.isNotEmpty(requestStr)) {

            JsonObject requestObj = new JsonParser().parse(requestStr).getAsJsonObject();
            logger.info("Request Obj For Calculator Email Servlet :{}",requestObj);

            String calcType = Objects.nonNull(requestObj.get("calcType")) ? requestObj.get("calcType").getAsString() : StringUtils.EMPTY;
            logger.info("Calculator Type :{}", calcType);

            String customerName = Objects.nonNull(requestObj.get("custName")) ? requestObj.get("custName").getAsString() : StringUtils.EMPTY;
            String city = Objects.nonNull(requestObj.get("city")) ? requestObj.get("city").getAsString() : StringUtils.EMPTY;
            String emailID = Objects.nonNull(requestObj.get("emailId")) ? requestObj.get("emailId").getAsString() : StringUtils.EMPTY;
            String mobileNumber = Objects.nonNull(requestObj.get("mobileNumber")) ? requestObj.get("mobileNumber").getAsString() : StringUtils.EMPTY;

            ConfigModel configModel = ConfigModel.getInstance();

            JsonObject rootBody = new JsonObject();
            JsonObject jsonBody = new JsonObject();
            JsonObject jsonHeader = new JsonObject();
            JsonObject templateDetails = new JsonObject();
            jsonBody.addProperty("toEmails", emailID);

            if (StringUtils.isNotEmpty(calcType)) {

                switch (calcType) {
                    case "Retirement Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Your path to a comfortable & secure future.");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("currentAge", Objects.nonNull(requestObj.get("currentAge")) ? requestObj.get("currentAge").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("monthlyExpense", Objects.nonNull(requestObj.get("monthlyExpense")) ? requestObj.get("monthlyExpense").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("accumulatedSavings", Objects.nonNull(requestObj.get("accumulatedSavings")) ? requestObj.get("accumulatedSavings").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedReturn", Objects.nonNull(requestObj.get("expectedReturn")) ? requestObj.get("expectedReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedPreRetirementReturn", Objects.nonNull(requestObj.get("expectedPreRetirementReturn")) ? requestObj.get("expectedPreRetirementReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedPostRetirementReturn", Objects.nonNull(requestObj.get("expectedPostRetirementReturn")) ? requestObj.get("expectedPostRetirementReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("retirementAge", Objects.nonNull(requestObj.get("retirementAge")) ? requestObj.get("retirementAge").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("costOfLiving", Objects.nonNull(requestObj.get("costOfLiving")) ? requestObj.get("costOfLiving").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedYearlyExpenditure", Objects.nonNull(requestObj.get("expectedYearlyExpenditure")) ? requestObj.get("expectedYearlyExpenditure").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("accumulatedRetirementKitty", Objects.nonNull(requestObj.get("accumulatedRetirementKitty")) ? requestObj.get("accumulatedRetirementKitty").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("monthlySIP", Objects.nonNull(requestObj.get("monthlySIP")) ? requestObj.get("monthlySIP").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "SIP Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy SIP Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("investmentAmount", Objects.nonNull(requestObj.get("investmentAmount")) ? requestObj.get("investmentAmount").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedReturn", Objects.nonNull(requestObj.get("expectedReturn")) ? requestObj.get("expectedReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("investmentTenureYears", Objects.nonNull(requestObj.get("investmentTenureYears")) ? requestObj.get("investmentTenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("futureValue", Objects.nonNull(requestObj.get("futureValue")) ? requestObj.get("futureValue").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("estimatedGain", Objects.nonNull(requestObj.get("estimatedGain")) ? requestObj.get("estimatedGain").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "Lumpsum Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy Lumpsum Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("investmentAmount", Objects.nonNull(requestObj.get("investmentAmount")) ? requestObj.get("investmentAmount").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedReturn", Objects.nonNull(requestObj.get("expectedReturn")) ? requestObj.get("expectedReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("investmentTenureYears", Objects.nonNull(requestObj.get("investmentTenureYears")) ? requestObj.get("investmentTenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("futureValue", Objects.nonNull(requestObj.get("futureValue")) ? requestObj.get("futureValue").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("estimatedGain", Objects.nonNull(requestObj.get("estimatedGain")) ? requestObj.get("estimatedGain").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "Compound Interest Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy Compound Interest Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("investmentAmount", Objects.nonNull(requestObj.get("investmentAmount")) ? requestObj.get("investmentAmount").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedRoi", Objects.nonNull(requestObj.get("expectedRoi")) ? requestObj.get("expectedRoi").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("tenureYears", Objects.nonNull(requestObj.get("tenureYears")) ? requestObj.get("tenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("futureValue", Objects.nonNull(requestObj.get("futureValue")) ? requestObj.get("futureValue").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("estimatedGain", Objects.nonNull(requestObj.get("estimatedGain")) ? requestObj.get("estimatedGain").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "EMI Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy EMI Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("principalAmount", Objects.nonNull(requestObj.get("principalAmount")) ? requestObj.get("principalAmount").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedInterestRate", Objects.nonNull(requestObj.get("expectedInterestRate")) ? requestObj.get("expectedInterestRate").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("investmentTenureYears", Objects.nonNull(requestObj.get("investmentTenureYears")) ? requestObj.get("investmentTenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("totalAmount", Objects.nonNull(requestObj.get("totalAmount")) ? requestObj.get("totalAmount").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("emiValue", Objects.nonNull(requestObj.get("emiValue")) ? requestObj.get("emiValue").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "Life insurance Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy Life Insurance Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("currentAge", Objects.nonNull(requestObj.get("currentAge")) ? requestObj.get("currentAge").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("retirementAge", Objects.nonNull(requestObj.get("retirementAge")) ? requestObj.get("retirementAge").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("monthlyIncome", Objects.nonNull(requestObj.get("monthlyIncome")) ? requestObj.get("monthlyIncome").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedIncrease", Objects.nonNull(requestObj.get("expectedIncrease")) ? requestObj.get("expectedIncrease").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("returnInvestment", Objects.nonNull(requestObj.get("returnInvestment")) ? requestObj.get("returnInvestment").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("coverAmount", Objects.nonNull(requestObj.get("coverAmount")) ? requestObj.get("coverAmount").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "Recurring Deposit Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy Recurring Deposit Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("monthlyContribution", Objects.nonNull(requestObj.get("monthlyContribution")) ? requestObj.get("monthlyContribution").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedReturn", Objects.nonNull(requestObj.get("expectedReturn")) ? requestObj.get("expectedReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("investmentTenureYears", Objects.nonNull(requestObj.get("investmentTenureYears")) ? requestObj.get("investmentTenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedMaturity", Objects.nonNull(requestObj.get("expectedMaturity")) ? requestObj.get("expectedMaturity").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("earningAmount", Objects.nonNull(requestObj.get("earningAmount")) ? requestObj.get("earningAmount").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "Fixed Deposit Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy Fixed Deposit Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("lumpsumInvestment", Objects.nonNull(requestObj.get("lumpsumInvestment")) ? requestObj.get("lumpsumInvestment").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedRate", Objects.nonNull(requestObj.get("expectedRate")) ? requestObj.get("expectedRate").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("investmentTenure", Objects.nonNull(requestObj.get("investmentTenure")) ? requestObj.get("investmentTenure").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("compoundingTenure", Objects.nonNull(requestObj.get("compoundingTenure")) ? requestObj.get("compoundingTenure").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("maturityValue", Objects.nonNull(requestObj.get("maturityValue")) ? requestObj.get("maturityValue").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("earningAmount", Objects.nonNull(requestObj.get("earningAmount")) ? requestObj.get("earningAmount").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    case "PPF Calculator":
                        jsonBody.addProperty("templateName", calcType);
                        jsonBody.addProperty("subject", "Tata Capital Moneyfy PPF Calculator Result");
                        templateDetails.addProperty("custName", customerName);
                        templateDetails.addProperty("yearlyContribution", Objects.nonNull(requestObj.get("yearlyContribution")) ? requestObj.get("yearlyContribution").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("expectedReturn", Objects.nonNull(requestObj.get("expectedReturn")) ? requestObj.get("expectedReturn").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("tenureYears", Objects.nonNull(requestObj.get("tenureYears")) ? requestObj.get("tenureYears").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("maturityValue", Objects.nonNull(requestObj.get("maturityValue")) ? requestObj.get("maturityValue").getAsString() : StringUtils.EMPTY);
                        templateDetails.addProperty("earningAmount", Objects.nonNull(requestObj.get("earningAmount")) ? requestObj.get("earningAmount").getAsString() : StringUtils.EMPTY);
                        jsonBody.add("templateDetails", templateDetails);
                        break;
                    default:
                        logger.info("No Calc Type Found For Your Request :{0}");
                        break;
                }
                jsonHeader.addProperty("authToken", configModel.getValue("shaftAuthToken").toString());
                rootBody.add("header", jsonHeader);
                rootBody.add("body", jsonBody);
                logger.info("Request Object For Calculator Email :{}",rootBody);
                JsonObject shaftResponseJsonObj = calculatorEmailService.postCallForCalcEmail(configModel.getValue("shaftHostUrl") + configModel.getValue("shaftEmailNotificationUri").toString(),configModel.getValue("shaftAuthToken").toString(), rootBody);
                logger.info("Response Object For Calculator Email :{}",shaftResponseJsonObj);
                String responseMessage = shaftResponseJsonObj.get("header").getAsJsonObject().get("status").getAsString();
                JsonObject responseObject  = new JsonObject();
                if(responseMessage.equalsIgnoreCase("success"))
                {
                    responseObject.addProperty("status", "success");
                    responseObject.addProperty("emailId", emailID);
                } else {
                    responseObject.addProperty("status", "error");
                    responseObject.addProperty("Message", "Oops something went wrong");
                }
                response.setContentType("application/json");
                response.getWriter().println(responseObject);
            } else {
                logger.error("Calculator Type is Empty :{0}");
            }
        }else {
            logger.error("Request String is Getting Null Or Empty in CalculatorEmailServlet");
        }
    }
}
