package com.tatacapital.moneyfy.core.service.impl;

import com.google.gson.JsonObject;
import com.tatacapital.moneyfy.core.services.FailureResponseService;
import org.osgi.service.component.annotations.Component;

@Component(service = FailureResponseService.class, immediate = true)
public class FailureResponseServiceImpl implements FailureResponseService {

    @Override
    public JsonObject getFailureResponse(Exception exception) {

        JsonObject jsonFailureResponse = new JsonObject();
        jsonFailureResponse.addProperty("status","failure");
        jsonFailureResponse.addProperty("error",exception.getMessage());

        return jsonFailureResponse;
    }
}
