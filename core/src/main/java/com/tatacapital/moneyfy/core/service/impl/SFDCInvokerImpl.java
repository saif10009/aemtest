package com.tatacapital.moneyfy.core.service.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.SFDCAuthModel;
import com.tatacapital.moneyfy.core.services.SFDCCorporateAuthConfigs;
import com.tatacapital.moneyfy.core.services.SFDCInvoker;
import com.tatacapital.moneyfy.core.services.SFDCRetailAuthConfigs;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

@Component(service = SFDCInvoker.class)
public class SFDCInvokerImpl implements SFDCInvoker {

    private static final Logger LOG = LoggerFactory.getLogger(SFDCInvokerImpl.class);

    @Reference
    SFDCRetailAuthConfigs sfdcConfig;

    @Reference
    SFDCCorporateAuthConfigs sfdcCorpConfig;

    @Reference
    private HttpClientBuilderFactory httpFactory;


    SFDCAuthModel model = new SFDCAuthModel();

    private SFDCAuthModel authenticate(boolean retail) {

        try {

            model = retail? sfdcConfig.getAuthModel():sfdcCorpConfig.getAuthModel();

            HttpClient client = httpFactory.newBuilder().build();
//			HttpClient client = HttpClientBuilder.create().build();
            URIBuilder builder;

            builder = new URIBuilder(model.getSfdcLoginURL());

            builder.setParameter("client_secret", model.getClientSercret())
                    .setParameter("grant_type", model.getGrantType())
                    .setParameter("username", model.getUserName());
            builder.setParameter("password", model.getPassword()).setParameter("client_id",
                    model.getClientId());

            HttpPost post = new HttpPost(builder.build());
            post.addHeader("Content-Type", "application/x-www-form-urlencoded");
            HttpResponse httpResponse = client.execute(post);
            LOG.info("##### Authentication  POST Data Send :"+ post);
            BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
            String output;
            String responseJsonString = "";
            while ((output = br.readLine()) != null) {
                responseJsonString = responseJsonString + output;
            }

            JsonObject responseJsonObject = new JsonObject();
            if (responseJsonObject.has("error")) {
                return null;
            }
            responseJsonObject = new JsonParser().parse(responseJsonString).getAsJsonObject();
            LOG.info("##### Authentication Response Data : ->"+ responseJsonString);
            String instanceURL = responseJsonObject.get("instance_url").getAsString();
            String accessToken = responseJsonObject.get("access_token").getAsString();
            LOG.info("Instance URL $$$$-> ", instanceURL);
            LOG.info("Access Token $$$$-> ", accessToken);

            model.setBearerToken(accessToken);
            model.setInstanceURL(instanceURL);
//			model = model;

        } catch (URISyntaxException e) {
            LOG.error("SFDC Authentication Failure!");

        } catch (ClientProtocolException e) {
            LOG.error("SFDC Authentication Failure!");

        } catch (IOException e) {
            LOG.error("SFDC Authentication Failure!");

        }

        return model;
    }

    @Override
    public String sendPostRequest(String serviceURI, String request,SFDCAuthModel model, boolean retail, boolean configUrl) {
        if (!model.getAuthorized())
            model = authenticate(retail);

        URIBuilder builder;
        try {

            if(configUrl == false) {
                builder = new URIBuilder(model.getInstaceURL() + serviceURI);
                LOG.info("API URL $$$$-> "+ model.getInstaceURL() + serviceURI);
            }else {
                builder = new URIBuilder(serviceURI);
                LOG.info("API URL $$$$-> "+ serviceURI);
            }

//			HttpClient client = HttpClientBuilder.create().build();
            HttpClient client = httpFactory.newBuilder().build();
            HttpPost post = new HttpPost(builder.build());
            post.addHeader("Authorization", "Bearer " + model.getBearerToken());
            post.setEntity(new StringEntity(request));
            post.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = client.execute(post);
            LOG.info("response.getStatusLine().getStatusCode()-->" + httpResponse.getStatusLine().getStatusCode());
            LOG.info("Request for API Request  $$$$-> "+ request);

            if (httpResponse.getStatusLine().getStatusCode() == 401) {
                LOG.info("##### Authentication Error Code : "+ httpResponse.getStatusLine().getStatusCode());
                model = authenticate(retail);
                LOG.info("##### Authentication Auth Model Data : " + model);
                String postResponse = sendPostRequest(serviceURI, request,model,retail,configUrl);
                LOG.info("##### Post Request Call Data : serviceURI : "+serviceURI +" request : "+ request + " retail : "+  retail+ " configUrl : "+ configUrl);
                return postResponse;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
            String output;
            String responseJsonString = "";

            while ((output = br.readLine()) != null) {
                responseJsonString = responseJsonString + output;
            }

            JsonObject responseJsonObject = new JsonObject();
            LOG.info("##### responseString Data : " + responseJsonString);
            responseJsonObject = new JsonParser().parse(responseJsonString).getAsJsonObject();
            LOG.info("Response for API Request  $$$$-> "+ responseJsonObject);

            return responseJsonObject.toString();
        } catch (URISyntaxException e) {
           // LOG.error(ErrorUtil.writeException(e));
        } catch (ClientProtocolException e) {
           // LOG.error(ErrorUtil.writeException(e));
        } catch (IOException e) {
          //  LOG.error(ErrorUtil.writeException(e));
        }

        return null;
    }

}
