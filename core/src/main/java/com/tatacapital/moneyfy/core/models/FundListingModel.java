package com.tatacapital.moneyfy.core.models;

import com.google.gson.Gson;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.ArrayList;
import java.util.List;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FundListingModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;
    Logger logger = LoggerFactory.getLogger(getClass());
    private List<String> amcIcons = new ArrayList<>();

    @PostConstruct
    public void init() {
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundListingQuery = "SELECT * FROM [dam:Asset] AS s WHERE ISDESCENDANTNODE([/content/dam/tata-capital-moneyfy/amc-icons])";
            stringBuffer.append(fundListingQuery);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Fund query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getPath());
                amcIcons.add(resource.getPath());
            }
        } catch (RepositoryException e) {
            logger.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }
    }

    public String getAmcIconsPaths() {
        Gson gson = new Gson();
        String amcIconsPaths = gson.toJson(amcIcons);
        return amcIconsPaths;
    }
}

