package com.tatacapital.moneyfy.core.services;

import com.google.gson.JsonObject;

public interface SHAFTConnectorService {

    String executePostMethod(String apiAuthToken, String finalApiUrl, JsonObject requestJson);
}
