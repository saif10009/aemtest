package com.tatacapital.moneyfy.core.service.impl;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.ConfigModel;
import com.tatacapital.moneyfy.core.services.MoneyfyIciciCreditCardJourneyService;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.osgi.services.HttpClientBuilderFactory;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
@Designate(ocd = MoneyfyIciciCreditCardJourneyServiceImpl.Config.class)
@Component(service = MoneyfyIciciCreditCardJourneyService.class,immediate = true)
public class MoneyfyIciciCreditCardJourneyServiceImpl implements MoneyfyIciciCreditCardJourneyService {

    @Reference
    private HttpClientBuilderFactory httpFactory;

    Logger LOG = LoggerFactory.getLogger(getClass());
    private Map<String, String> configMap;
    
    @ObjectClassDefinition(name = "Moneyfy ICICI CC Lead Journey", description = "Moneyfy ICICI CC Lead Journey")
    public static @interface Config {

        @AttributeDefinition(name = "OTP ByPass",  description = "Enable OTP")
        boolean analytics_enabled() default false;

        @AttributeDefinition(name = "generate otp url", required = true, description = "generate otp url")
        String otp_generate_url() default "https://webappfrontendadapter.services.tatacapital.com/rest/webApp/v1.0/otp/generateOTPV1";

        @AttributeDefinition(name = "verify otp url", required = true, description = "verify otp url")
        String otp_verfiy_url() default "https://webappfrontendadapter.services.tatacapital.com/rest/webApp/v1.0/otp/verifyOTP";

        @AttributeDefinition(name = "api authorization", required = true, description = "Set api authorization")
        String api_authorization() default "Basic YThiMGVhNjE6d2Vic2l0ZQ==";

        @AttributeDefinition(name = "icici lead api authorization", required = true, description = "Set lead api authorization Value")
        String credit_card_lead_authorization_value() default "Basic MmU3ZWZhZjg6aW5kdXNmYWNldWF0";

        @AttributeDefinition(name = "icici credit card lead api url", required = true, description = "enter icici credit card lead api url")
        String credit_card_lead_api_url() default "https://apicast-uat-thirdpartyfrontendadapter.tclnprdservice.tatacapital.com/rest/thirdParty/v1.0/sfdc/CreateOrFetchLead";

    }

    boolean OtpEnabled;

    @Activate
    @Modified
    protected void configure(Config config) {
        configMap = new HashMap<>();
        OtpEnabled = config.analytics_enabled();
        configMap.put("otpGenerateApiUrl", config.otp_generate_url());
        configMap.put("otpVerfiyApiurl", config.otp_verfiy_url());
        configMap.put("otpApiAuthorization", config.api_authorization());
        configMap.put("creditCardLeadAuthorizationValue",config.credit_card_lead_authorization_value());
        configMap.put("creditCardLeadApiUrl",config.credit_card_lead_api_url());
        ConfigModel.getInstance().setConfigMap(configMap);
    }

    @Override
    public Map<String, String> getConfig() {
        return configMap;
    }

    @Override
    public boolean getOtpEnabled() {
        return OtpEnabled;
    }

    @Override
    public String otpApiCall(String serviceURI, String authorizationValue, String request) {
        URIBuilder builder;
        String conversationID = generateRandomDigits();
        try {
            builder= new URIBuilder(serviceURI);
            HttpClient client = httpFactory.newBuilder().build();
            HttpPost post = new HttpPost(builder.build());
            post.addHeader("Accept-Encoding", "gzip,deflate");
            post.addHeader("Authorization", authorizationValue);
            post.addHeader("ConversationID", conversationID);
            post.addHeader("SourceName", "Website");
            post.setEntity(new StringEntity(request));
            post.setHeader("Content-type", "application/json");

            HttpResponse httpResponse = client.execute(post);
            LOG.info("response.getStatusLine().getStatusCode()-->" + httpResponse.getStatusLine().getStatusCode());
            LOG.info("Request for API Request  $$$$-> "+ request);

            BufferedReader br = new BufferedReader(new InputStreamReader((httpResponse.getEntity().getContent())));
            String output;
            String responseJsonString = "";
            while ((output = br.readLine()) != null) {
                responseJsonString = responseJsonString + output;
            }

            JsonObject responseJsonObject = new JsonObject();
            LOG.info("##### responseString Data : " + responseJsonString);
            responseJsonObject = new JsonParser().parse(responseJsonString).getAsJsonObject();
            LOG.info("Response for API Request  $$$$-> "+ responseJsonObject);

            return responseJsonObject.toString();

        }catch (Exception e){
            LOG.info("Exception in Generate OTP {}"+e);
            e.printStackTrace();
        }

        return null;
    }
    private static String generateRandomDigits() {
        int m= (int) Math.pow(10, 8 - 1);
        int x = new Random().nextInt(9 * m);
        return Integer.toString(m+x);
    }
}
