package com.tatacapital.moneyfy.core.models.pojo;

public class TopPicksPojo {
    private Float nav;
    private String riskType;
    private String pagePath;
    private String iconPath;
    private String updatedDate;
    private String fundName;
    private String cagr1y;
    private String cagr3y;
    private String catName;
    private String schemeId;


    public String getCagr1y() {
        return cagr1y;
    }

    public void setCagr1y(String cagr1y) {
        this.cagr1y = cagr1y;
    }

    public String getCagr3y() {
        return cagr3y;
    }

    public void setCagr3y(String cagr3y) {
        this.cagr3y = cagr3y;
    }

    public Float getNav() {
        return nav;
    }

    public void setNav(Float nav) {
        this.nav = nav;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }
}
