package com.tatacapital.moneyfy.core.service.impl;

import com.tatacapital.moneyfy.core.models.SFDCAuthModel;
import com.tatacapital.moneyfy.core.services.SFDCCorporateAuthConfigs;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Modified;
import org.osgi.service.metatype.annotations.AttributeDefinition;
import org.osgi.service.metatype.annotations.Designate;
import org.osgi.service.metatype.annotations.ObjectClassDefinition;

@Designate(ocd=SFDCCorporateAuthConfigsImpl.Config.class)
@Component(service= SFDCCorporateAuthConfigs.class)
public class SFDCCorporateAuthConfigsImpl implements SFDCCorporateAuthConfigs {

    @ObjectClassDefinition(name = "Tata Capital Moneyfy Corporate Section SFDC Configs", description = "SFDC Lead Configuration")
    public static @interface Config {

        @AttributeDefinition(name = "SFDC Login URL", required = true, description = "Set salesAuth_prod_url")
        String sfdc_login_url() default "https://test.salesforce.com/services/oauth2/token";

        @AttributeDefinition(name = "Client ID", required = true, description = "Set Client ID")
        String client_id() default "3MVG9Se4BnchkASkqYB1hLNejn5BZqQ8M0O4gejxvqM7cQKhLOyFYee1xglp8Tm4AHN04mXyGLPqAnZTyXdOG";

        @AttributeDefinition(name = "Client Secret", required = true, description = "Set Client Secret")
        String client_secret() default "6017647376689414038";

        @AttributeDefinition(name = "Grant Type", required = true, description = "Set Grant Type")
        String grant_type() default "password";

        @AttributeDefinition(name = "User Name", required = true, description = "Set User Name")
        String username() default "apiuser@tatacapital.com.uat";

        @AttributeDefinition(name = "Password", required = true, description = "Set Password")
        String password() default "TataCap@12345";

        @AttributeDefinition(name = "CLOS Tracking Application URL", required = true, description = "Set CLOS Tracking Application URL")
        String closTrackingApplicationURL() default "http://172.16.17.81:9085/LOSWebApp/services/EWPService";

        @AttributeDefinition(name = "RLOS Tracking Application URL", required = true, description = "Set RLOS Tracking Application URL")
        String rlosTrackingApplicationURL() default "http://172.16.16.59:9081/LOSWebApp/services/EWPService";


    }

    SFDCAuthModel authModel;

    @Activate
    @Modified
    // http://blogs.adobe.com/experiencedelivers/experience-management/osgi_activate_deactivatesignatures/
    protected void activate(Config config) {
        authModel = new SFDCAuthModel();
        authModel.setSfdcLoginURL(config.sfdc_login_url());
        authModel.setClientId(config.client_id());
        authModel.setClientSercret(config.client_secret());
        authModel.setUserName(config.username());
        authModel.setPassword(config.password());
        authModel.setGrantType(config.grant_type());
        authModel.setClosTrackingURL(config.closTrackingApplicationURL());
        authModel.setRlosTrackingURL(config.rlosTrackingApplicationURL());

    }

    @Override
    public SFDCAuthModel getAuthModel() {
        return authModel;
    }


}

