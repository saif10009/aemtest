package com.tatacapital.moneyfy.core.models.pojo;

public class NFOPojo {

    private String fundName;
    private String riskType;
    private Float minSipAmt;
    private Float minLumpsumAmt;
    private String categoryName;
    private String schemeOption;
    private String pagePath;
    private String iconPath;
    private int schemeId;
    private String updatedDate;
    private String openingDate;
    private String closingDate;
    private long count;

    public String getOpeningDate() {
        return openingDate;
    }

    public void setOpeningDate(String openingDate) {
        this.openingDate = openingDate;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public int getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(int schemeId) {
        this.schemeId = schemeId;
    }


    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public Float getMinSipAmt() {
        return minSipAmt;
    }

    public void setMinSipAmt(Float minSipAmt) {
        this.minSipAmt = minSipAmt;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSchemeOption() {
        return schemeOption;
    }

    public void setSchemeOption(String schemeOption) {
        this.schemeOption = schemeOption;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public Float getMinLumpsumAmt() {
        return minLumpsumAmt;
    }

    public void setMinLumpsumAmt(Float minLumpsumAmt) {
        this.minLumpsumAmt = minLumpsumAmt;
    }
}
