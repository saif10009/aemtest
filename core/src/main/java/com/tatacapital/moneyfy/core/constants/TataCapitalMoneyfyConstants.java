package com.tatacapital.moneyfy.core.constants;

public class TataCapitalMoneyfyConstants {
    public static final String ICONPATH="iconPath";
    public static final String CATNAME="catName";
    public static final String MIN_LUMPSUM_AMT="minLumpsumAmt";
}
