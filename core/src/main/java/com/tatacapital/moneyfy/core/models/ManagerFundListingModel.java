package com.tatacapital.moneyfy.core.models;


import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.constants.TataCapitalMoneyfyConstants;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.*;
import java.util.stream.Collectors;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ManagerFundListingModel {

    @Inject
    @Default(values = "")
    String fundmanagerSchemeId;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;
    Logger logger = LoggerFactory.getLogger(getClass());


    public String getFundList() {
        Map<String, List<FundListingPojo>> catBasedFunds = new HashMap<>();
        List<FundListingPojo> equityFundList = new ArrayList<>();
        List<FundListingPojo> debtFundList = new ArrayList<>();
        List<FundListingPojo> hybridFundList = new ArrayList<>();
        if(fundmanagerSchemeId.isEmpty()){
            logger.error("In ManagerFundListingModel fundmanagerSchemeId is getting Empty : {0}");
            return "";
        }
        List<Integer> schemeIdList = Arrays.stream(fundmanagerSchemeId.replaceAll("[\\[\\]\"]", "").split(",")).map(Integer::parseInt).collect(Collectors.toList());
        try {
            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuilder stringBuilder = new StringBuilder();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='FUND'";
            stringBuilder.append(fundManagerQuery);
            logger.info("Asset stringBuffer : {}",stringBuilder);
            Query query = queryManager.createQuery(stringBuilder.toString(), Query.JCR_SQL2);
            logger.info("Asset query : {}",stringBuilder);
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                FundListingPojo fundListingPojo = new FundListingPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                JsonObject schemeDetails = new JsonParser().parse(page.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                JsonObject ratings = new JsonParser().parse(page.getProperties().get("ratings").toString()).getAsJsonObject();
                if (schemeDetails.get("optName").getAsString().equalsIgnoreCase("g") && schemeIdList.contains(schemeDetails.get("id").getAsInt())) {
                    fundListingPojo.setIconPath(page.getParent().getProperties().get(TataCapitalMoneyfyConstants.ICONPATH).toString().equals("") ? page.getParent().getProperties().get(TataCapitalMoneyfyConstants.ICONPATH).toString() : "");
                    fundListingPojo.setPagePath(page.getPath().concat(".html"));
                    fundListingPojo.setFundName(schemeDetails.get("name").getAsString());
                    fundListingPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                    float nav = !page.getProperties().get("nav").toString().equals("") && !page.getProperties().get("nav").toString().equals("null") ? Float.parseFloat(page.getProperties().get("nav").toString()) : 0;
                    nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                    fundListingPojo.setNav(nav);
                    fundListingPojo.setCagrValues(page.getProperties().get("cagrValues").toString());
                    fundListingPojo.setCategoryName(schemeDetails.get(TataCapitalMoneyfyConstants.CATNAME).getAsString());
                    fundListingPojo.setFundManagerName(page.getProperties().get("fundManagerName").toString());
                    fundListingPojo.setMorningStar(ratings.get("morningStar").getAsString());
                    fundListingPojo.setValueResearch(ratings.get("valueResearch").getAsString());
                    fundListingPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                    String minSipAmt = !page.getProperties().get("minSipAmt").toString().equals("") && !page.getProperties().get("minSipAmt").toString().equals("null") ? page.getProperties().get("minSipAmt").toString() : "0";
                    String minLumpSumAmt = getMinLumpSumAmt(page);
                    fundListingPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                    fundListingPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                    String fundSize = !page.getProperties().get("fundSize").toString().equals("") && !page.getProperties().get("fundSize").toString().equals("null") ? page.getProperties().get("fundSize").toString() : "0";
                    fundListingPojo.setFundSize(Float.valueOf(fundSize));
                    fundListingPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                    fundListingPojo.setIconPath(page.getParent().getProperties().get(TataCapitalMoneyfyConstants.ICONPATH).toString());

                    if (schemeDetails.get(TataCapitalMoneyfyConstants.CATNAME).getAsString().equalsIgnoreCase("EQUITY")) {
                        equityFundList.add(fundListingPojo);
                    } else if (schemeDetails.get(TataCapitalMoneyfyConstants.CATNAME).getAsString().equalsIgnoreCase("HYBRID")) {
                        hybridFundList.add(fundListingPojo);
                    } else if (schemeDetails.get(TataCapitalMoneyfyConstants.CATNAME).getAsString().equalsIgnoreCase("DEBT")) {
                        debtFundList.add(fundListingPojo);
                    }
                    catBasedFunds.put("fundManagerEquity", equityFundList);
                    catBasedFunds.put("fundManagerHybrid", hybridFundList);
                    catBasedFunds.put("fundManagerDebt", debtFundList);
                }

            }
        } catch (RepositoryException e) {
            logger.info("RepositoryException : {}",e.getMessage());
        }
        catBasedFunds.put("fundManagerEquity", equityFundList);
        catBasedFunds.put("fundManagerHybrid", hybridFundList);
        catBasedFunds.put("fundManagerDebt", debtFundList);
        Gson gson = new Gson();
        return gson.toJson(catBasedFunds);
    }

    private String getMinLumpSumAmt(Page page)
    {
        String minLumpsumAmt;
        if(Objects.nonNull(page.getProperties().get(TataCapitalMoneyfyConstants.MIN_LUMPSUM_AMT)) &&
                StringUtils.isNotBlank(page.getProperties().get(TataCapitalMoneyfyConstants.MIN_LUMPSUM_AMT).toString()) && !page.getProperties().get(TataCapitalMoneyfyConstants.MIN_LUMPSUM_AMT).toString().equals("null")){
            minLumpsumAmt =  page.getProperties().get(TataCapitalMoneyfyConstants.MIN_LUMPSUM_AMT).toString();
        }else {
            minLumpsumAmt = "0";
        }
        return minLumpsumAmt;
    }


}



