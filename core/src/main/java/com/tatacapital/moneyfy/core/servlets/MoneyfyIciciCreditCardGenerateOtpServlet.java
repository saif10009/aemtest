package com.tatacapital.moneyfy.core.servlets;

import com.google.gson.JsonObject;
import com.tatacapital.moneyfy.core.services.MoneyfyIciciCreditCardJourneyService;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Objects;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=OTP Generate Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=generateOtp"
})
public class MoneyfyIciciCreditCardGenerateOtpServlet extends SlingAllMethodsServlet {

    @Reference
    MoneyfyIciciCreditCardJourneyService moneyfyIciciCreditCardJourneyService;

    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

        JsonObject otpGenerationRequestBody = new JsonObject();
        String mobileNumber = request.getParameter("otpMobile");
        log.info("Mobile Number : {}",mobileNumber);
        if (StringUtils.isEmpty(mobileNumber)) {
            response.setContentType("application/json");
            response.getWriter().println("{\"retStatus\":\"FAILURE\",\"otpRefNo\":\"null\",\"errorMessage\":\"Mobile Number is invalid.Please enter valid Mobile Number\",\"sysErrorCode\":\"ERROTP200\",\"sysErrorMessage\":\"Error in Generating OTP\"}");
        } else {
            otpGenerationRequestBody.addProperty("MobileNumber", mobileNumber);
            long startTime = System.currentTimeMillis();
            log.info("Otp generation start time :{}",startTime);
            boolean isOtpByPass = moneyfyIciciCreditCardJourneyService.getOtpEnabled();
            if (isOtpByPass) {
                response.setContentType("application/json");
                response.getWriter().println("{\"errorMessage\":\"null\",\"sysErrorMessage\":\"null\",\"sysErrorCode\":\"null\",\"retStatus\":\"SUCCESS\",\"errorMessage\":null,\"otpRefNo\":\"12345\"}");
                log.info("Otp generation End Time : {}",System.currentTimeMillis());
                log.info("OTP Generation Time Difference :{}",(System.currentTimeMillis() - startTime));
            } else {
                String otpApiUrl = moneyfyIciciCreditCardJourneyService.getConfig().get("otpGenerateApiUrl");
                String otpAuthorizationValue = moneyfyIciciCreditCardJourneyService.getConfig().get("otpApiAuthorization");
                String otpGenerationResponse = moneyfyIciciCreditCardJourneyService.otpApiCall(otpApiUrl, otpAuthorizationValue, otpGenerationRequestBody.toString());
                log.info("OTP GENERATION RESPONSE : {}",otpGenerationResponse);
                if(Objects.isNull(otpGenerationResponse))
                {
                    response.getWriter().println("Bad Request For Otp Generation");
                }else {
                    log.info("Otp generation End Time : {}",System.currentTimeMillis());
                    log.info("OTP Generation Time Difference : {}", (System.currentTimeMillis() - startTime));
                    response.setContentType("application/json");
                    response.getWriter().print(otpGenerationResponse);
                }
            }
        }
    }
}


