package com.tatacapital.moneyfy.core.servlets;

import com.day.cq.wcm.api.Page;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.services.ResourceHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Objects;
import java.util.stream.Collectors;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Fund Search Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.extension=json"
})
public class FundSearchServlet extends SlingAllMethodsServlet {

    private static final long serialVersionUID = 3169795937693969416L;
    private static final String searchRootPath = "/content/tata-capital-moneyfy/en/";

    @Reference
    ResourceHelper resHelper;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response)
            throws ServletException, IOException {
        response.setHeader("Content-Type", "application/json");
        JsonObject jsonObject = new JsonObject();
        JsonArray fundsArray = new JsonArray();
        //ResourceResolver resourceResolver = null;

        try {
            ResourceResolver resourceResolver = request.getResourceResolver();
            Session session = resourceResolver.adaptTo(Session.class);
            Node queryRoot = session.getNode(searchRootPath);
            String jsonBodyStr =  request.getReader().lines().collect(Collectors.joining());
            JsonObject jsonBody = new JsonParser().parse(jsonBodyStr).getAsJsonObject();
            JsonObject requestData =  jsonBody.get("body").getAsJsonObject();
            String searchTerm = requestData.get("searchTerm").getAsString().trim();
            if (Objects.requireNonNull(searchTerm,"").isEmpty()) {
                jsonObject.add("results", fundsArray);
                response.setStatus(200);
                response.getWriter().print(jsonObject.toString());
            }else if (searchTerm != null && !searchTerm.equals("")) {
                NodeIterator searchResults = null;
                searchResults = performSearchWithSQL(queryRoot, searchTerm.toLowerCase());
                if (searchResults != null) {
                    while (searchResults.hasNext()) {
                        Node searchResultNode = searchResults.nextNode();
                        Resource resource = resourceResolver.getResource(searchResultNode.getParent().getPath());
                        Page page = resource.adaptTo(Page.class);
                        String pageTitle=null;
                        try {
                            pageTitle = Objects.nonNull(page.getProperties().get("pageTitle").toString()) ?
                                    page.getProperties().get("pageTitle").toString() : "";
                        }
                        catch (Exception e){e.printStackTrace();}
                        String pagePath = page.getPath().concat(".html");
                        JsonObject fundDetail = new JsonObject();
                        String schemeId=null;
                        try {
                            schemeId = page.getProperties().get("id").toString();
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        if(pageTitle.equalsIgnoreCase("Compare Mutual Funds"))
                        {
                            continue;
                        }
                        fundDetail.addProperty("schemeId", schemeId);
                        if (pageTitle != null)
                            fundDetail.addProperty("pageTitle", pageTitle);
                        if (pagePath != null)
                            fundDetail.addProperty("pagePath", pagePath);
                        fundsArray.add(fundDetail);
                    }
                    response.setStatus(200);
                    jsonObject.add("results", fundsArray);
                } else {
                    response.setStatus(200);
                    jsonObject.add("results", fundsArray);
                }
                response.getWriter().print(jsonObject.toString());
            }
        } catch (Exception e) {
            response.setStatus(500);
            logger.error("RepositoryException {}", e);
            e.printStackTrace();
        } finally {
           /* if (resourceResolver != null){
                resourceResolver.close();
            }*/
            response.getWriter().close();
        }
    }

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    private NodeIterator performSearchWithSQL(Node queryRoot, String queryTerm) throws RepositoryException {
        QueryManager qm = queryRoot.getSession().getWorkspace().getQueryManager();
        StringBuffer queryBuilder = new StringBuffer();
        queryBuilder.append("SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/]) and LOWER(s.[pageTitle]) like '%"+queryTerm+"%'");
        Query query = qm.createQuery(queryBuilder.toString(), Query.JCR_SQL2);
        QueryResult queryResult = query.execute();
        return queryResult.getNodes();
    }
}


