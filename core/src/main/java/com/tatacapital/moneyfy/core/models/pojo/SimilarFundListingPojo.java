package com.tatacapital.moneyfy.core.models.pojo;

import org.apache.sling.api.resource.ValueMap;

public class SimilarFundListingPojo {
    private String fundName;
    private Float nav;
    private String ratings;
    private String riskType;
    private String minSipAmt;
    private String minLumpsumAmt;
    private String fundManagerName;
    private String fundSize;
    private String cagrValues;
    private String categoryName;
    private String schemeOption;
    private String pagePath;
    private String iconPath;
    private String fundId;
    private String similarFundIds;
    private String morningStar;
    private String valueResearch;
    private String schemeId;
    private boolean isGreaterThanZero;
    private Float threeYearValue;


    public Float getThreeYearValue() {
        return threeYearValue;
    }

    public void setThreeYearValue(Float threeYearValue) {
        this.threeYearValue = threeYearValue;
    }

    public String getOneMonthReturn() {
        return oneMonthReturn;
    }

    public void setOneMonthReturn(String oneMonthReturn) {
        this.oneMonthReturn = oneMonthReturn;
    }

    public String getThreeMonthReturn() {
        return threeMonthReturn;
    }

    public void setThreeMonthReturn(String threeMonthReturn) {
        this.threeMonthReturn = threeMonthReturn;
    }

    public String getSixMonthReturn() {
        return sixMonthReturn;
    }

    public void setSixMonthReturn(String sixMonthReturn) {
        this.sixMonthReturn = sixMonthReturn;
    }

    public String getOneYearReturn() {
        return oneYearReturn;
    }

    public void setOneYearReturn(String oneYearReturn) {
        this.oneYearReturn = oneYearReturn;
    }

    public String getThreeYearReturn() {
        return threeYearReturn;
    }

    public void setThreeYearReturn(String threeYearReturn) {
        this.threeYearReturn = threeYearReturn;
    }

    public String getFiveYearReturn() {
        return fiveYearReturn;
    }

    public void setFiveYearReturn(String fiveYearReturn) {
        this.fiveYearReturn = fiveYearReturn;
    }

    public String getMaxReturn() {
        return maxReturn;
    }

    public void setMaxReturn(String maxReturn) {
        this.maxReturn = maxReturn;
    }

    private String oneMonthReturn;
    private String threeMonthReturn;
    private String sixMonthReturn;
    private String oneYearReturn;
    private String threeYearReturn;
    private String fiveYearReturn;
    private String maxReturn;



    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public Float getNav() {
        return nav;
    }

    public void setNav(Float nav) {
        this.nav = nav;
    }

    public String getRatings() {
        return ratings;
    }

    public boolean isGreaterThanZero() {
        return isGreaterThanZero;
    }

    public void setGreaterThanZero(boolean greaterThanZero) {
        isGreaterThanZero = greaterThanZero;
    }

    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public String getMinSipAmt() {
        return minSipAmt;
    }

    public void setMinSipAmt(String minSipAmt) {
        this.minSipAmt = minSipAmt;
    }

    public String getFundManagerName() {
        return fundManagerName;
    }

    public void setFundManagerName(String fundManagerName) {
        this.fundManagerName = fundManagerName;
    }

    public String getFundSize() {
        return fundSize;
    }

    public void setFundSize(String fundSize) {
        this.fundSize = fundSize;
    }

    public String getCagrValues() {
        return cagrValues;
    }

    public void setCagrValues(String cagrValues) {
        this.cagrValues = cagrValues;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSchemeOption() {
        return schemeOption;
    }

    public void setSchemeOption(String schemeOption) {
        this.schemeOption = schemeOption;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId;
    }

    public String getSimilarFundIds() {
        return similarFundIds;
    }

    public void setSimilarFundIds(String similarFundIds) {
        this.similarFundIds = similarFundIds;
    }

    public String getMorningStar() {
        return morningStar;
    }

    public void setMorningStar(String morningStar) {
        this.morningStar = morningStar;
    }

    public String getValueResearch() {
        return valueResearch;
    }

    public void setValueResearch(String valueResearch) {
        this.valueResearch = valueResearch;
    }

    public String getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(String schemeId) {
        this.schemeId = schemeId;
    }

    public String getMinLumpsumAmt() {
        return minLumpsumAmt;
    }

    public void setMinLumpsumAmt(String minLumpsumAmt) {
        this.minLumpsumAmt = minLumpsumAmt;
    }
}

