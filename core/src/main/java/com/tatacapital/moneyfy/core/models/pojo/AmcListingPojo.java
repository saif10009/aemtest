package com.tatacapital.moneyfy.core.models.pojo;

public class AmcListingPojo {

    private String fundName;
    private String aum;
    private String highestReturns;
    private String pagePath;
    private String numberOfFunds;
    private String amcId;
    private String iconPath;

    public String getAmcId() {
        return amcId;
    }

    public void setAmcId(String amcId) {
        this.amcId = amcId;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public String getAum() {
        return aum;
    }

    public void setAum(String aum) {
        this.aum = aum;
    }

    public String getHighestReturns() {
        return highestReturns;
    }

    public void setHighestReturns(String highestReturns) {
        this.highestReturns = highestReturns;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getNumberOfFunds() {
        return numberOfFunds;
    }

    public void setNumberOfFunds(String numberOfFunds) {
        this.numberOfFunds = numberOfFunds;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }
}
