package com.tatacapital.moneyfy.core.services;

import java.util.Map;

public interface DownloadAppSmsService {

    Map<String,String> getConfig();
}
