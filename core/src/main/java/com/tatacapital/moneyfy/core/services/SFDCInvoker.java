package com.tatacapital.moneyfy.core.services;

import com.tatacapital.moneyfy.core.models.SFDCAuthModel;

public interface SFDCInvoker {
    String sendPostRequest(String serviceURI, String request, SFDCAuthModel model, boolean retail, boolean configUrl);
}
