package com.tatacapital.moneyfy.core.servlets;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.services.SFDCInvoker;
import com.tatacapital.moneyfy.core.services.SFDCRetailAuthConfigs;
import org.apache.commons.lang3.StringUtils;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.osgi.framework.Constants;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import java.io.IOException;
import java.util.Objects;

@Component(service = Servlet.class, property = {
        Constants.SERVICE_DESCRIPTION + "=Lead Journey Servlet",
        "sling.servlet.methods=GET",
        "sling.servlet.methods=POST",
        "sling.servlet.resourceTypes=/apps/tatacapitalmoneyfy/moneyfyapi",
        "sling.servlet.selectors=iciciCreditCardLeadApi"
})
public class SFDCLeadJourneyServlet extends SlingAllMethodsServlet {

    @Reference
    SFDCInvoker sfdcInvoker;

    @Reference
    SFDCRetailAuthConfigs configs;


    Logger log = LoggerFactory.getLogger(getClass());

    @Override
    protected void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {

        JsonObject jsonRequest = new JsonObject();

        JsonObject jsonPLEmployeeForm = new JsonObject();
        JsonObject jsonCustomer = new JsonObject();
        JsonObject jsonNRIDetails = new JsonObject();
        jsonCustomer.addProperty("Customer_Name", Objects.nonNull(request.getParameter("name")) ? request.getParameter("name"):StringUtils.EMPTY);
        jsonCustomer.addProperty("Mobile",Objects.nonNull(request.getParameter("mobileNumber")) ? request.getParameter("mobileNumber"): StringUtils.EMPTY);
        jsonCustomer.addProperty("AlternateMobile",Objects.nonNull(request.getParameter("altNo")) ? request.getParameter("altNo"):StringUtils.EMPTY);
        jsonCustomer.addProperty("Personal_Email",Objects.nonNull(request.getParameter("emailId")) ? request.getParameter("emailId") : StringUtils.EMPTY);
        jsonCustomer.addProperty("Birthdate", Objects.nonNull(request.getParameter("dob")) ? request.getParameter("dob") : "01/01/1998");
        jsonCustomer.addProperty("Aadhaar_Number", "");
        jsonCustomer.addProperty("PAN_Number", request.getParameter("panCard"));
        jsonCustomer.addProperty("Marital_Status", "");
        jsonCustomer.addProperty("Gender", request.getParameter("gender")!= null? request.getParameter("gender"): "");
        jsonCustomer.addProperty("Mothers_Maiden_Name", "");
        jsonCustomer.addProperty("WebTopId","");
        jsonCustomer.addProperty("ProductCode", request.getParameter("productCode") != null ? request.getParameter("productCode"): "");
        jsonCustomer.addProperty("Business_Existence", request.getParameter("businessExistence") != null ? request.getParameter("businessExistence"): "");
        jsonCustomer.addProperty("Unique_Identifier", request.getParameter("uniqueidentifier") != null ? request.getParameter("uniqueidentifier"): "");
        jsonCustomer.addProperty("Residence_and_Employment_Stability", request.getParameter("residenceandemployment") != null ? request.getParameter("residenceandemployment"): "");
        jsonCustomer.addProperty("NameofthePolicyHolder", request.getParameter("nameOfPolicyHolder"));
        jsonCustomer.addProperty("CorporateName", request.getParameter("corporateName") != null ? request.getParameter("corporateName"): "");
        String productCode =  Objects.nonNull(request.getParameter("productCode"))?request.getParameter("productCode"):"Tata Cards";
        if( StringUtils.isNotEmpty(productCode) && productCode.equalsIgnoreCase("hl")){
            jsonCustomer.addProperty("IsOverdraftLoan",true);
        }
        else if(StringUtils.isNotEmpty(productCode) && productCode.equalsIgnoreCase("lapod")){
            jsonCustomer.addProperty("LAPODFlag","Yes");
            jsonCustomer.addProperty("ProductCode","LAP");

        }
        jsonNRIDetails.addProperty("ISDMobile", request.getParameter("ISTmobile"));
        jsonNRIDetails.addProperty("Country", request.getParameter("country"));
        jsonNRIDetails.addProperty("PreferredTimeToCall", request.getParameter("preferredTime"));
        jsonNRIDetails.addProperty("PreferredServicedLocIndia", request.getParameter("locationOfIndia"));
        jsonNRIDetails.addProperty("ObjectiveType", request.getParameter("objective"));

        jsonCustomer.add("NRIDeetails", jsonNRIDetails);

        jsonPLEmployeeForm.add("Customer", jsonCustomer);

        String wealthProductName = request.getParameter("wealthProductName");
        //String security = request.getParameter("security");
        String comment = request.getParameter("cmnt")!= null ? request.getParameter("cmnt") : "null";
        boolean consent = Boolean.parseBoolean(request.getParameter("consent")!= null? request.getParameter("consent") : "false");

        JsonObject jsonLoanDetails = new JsonObject();
        if(wealthProductName != null) {
            jsonLoanDetails.addProperty("Product",request.getParameter("wealthProductName") != null ? request.getParameter("wealthProductName") : request.getParameter("productCode"));
        }
        else if(StringUtils.isNotEmpty(productCode) && productCode.equalsIgnoreCase("lapod")){
            jsonLoanDetails.addProperty("Product","LAP");
        }
        else {
            jsonLoanDetails.addProperty("Product",request.getParameter("productName") != null ? request.getParameter("productName") : request.getParameter("productCode"));
        }
        jsonLoanDetails.addProperty("Make",request.getParameter("manufacturer") != null ? request.getParameter("manufacturer") : "");
        jsonLoanDetails.addProperty("Model",request.getParameter("model")!= null ? request.getParameter("model") :"");
        jsonLoanDetails.addProperty("Variant",request.getParameter("variant") != null ? request.getParameter("variant") : "" );
        jsonLoanDetails.addProperty("Cost_of_two_wheeler", request.getParameter("costOfWheeler")!= null ? Integer.parseInt(request.getParameter("costOfWheeler") ) :null);
        jsonLoanDetails.addProperty("Required_Loan_Tenure",request.getParameter("tenure") != null? request.getParameter("tenure"): "1");
        jsonLoanDetails.addProperty("Required_Loan_Amount",request.getParameter("loanAmount")!= null? request.getParameter("loanAmount") :"1");
        jsonLoanDetails.addProperty("Eligible_loan_Amount","");
        jsonLoanDetails.addProperty("Time_frame_of_Purchase",request.getParameter("timePurchase")!= null? request.getParameter("timePurchase"): "" );
        jsonLoanDetails.addProperty("Property_Value", request.getParameter("propertyValue")!= null? request.getParameter("propertyValue") : "");
        jsonLoanDetails.addProperty("Property_Type", request.getParameter("propertyType")!= null? request.getParameter("propertyType") : "");
        jsonLoanDetails.addProperty("ReasonFor_LAP", request.getParameter("reasonForLap")!= null? request.getParameter("reasonForLap") : "");
        jsonLoanDetails.addProperty("Comments", comment);
        jsonLoanDetails.addProperty("consent",consent);//get source from URL
        String source = request.getParameter("source");
        String subSource = request.getParameter("subsource");

        if(source != null && subSource == null){
            jsonLoanDetails.addProperty("LeadSource", source);
            jsonLoanDetails.addProperty("LeadSubSource", "Website");
        }
        else if(source == null && subSource != null)
        {
            jsonLoanDetails.addProperty("LeadSource", "Digital");
            jsonLoanDetails.addProperty("LeadSubSource",subSource);
        }
        else if(source != null && subSource != null) {
            jsonLoanDetails.addProperty("LeadSource", source);
            jsonLoanDetails.addProperty("LeadSubSource", subSource);
        }
        else {
            jsonLoanDetails.addProperty("LeadSource", "Digital");
            jsonLoanDetails.addProperty("LeadSubSource", "ICICICardsApp");
        }
        jsonLoanDetails.addProperty("LeadStatus", request.getParameter("leadStatus")!= null? request.getParameter("leadStatus") : "");
        jsonPLEmployeeForm.add("LoanDetails", jsonLoanDetails);

        JsonObject occupationDetails = new JsonObject();
        occupationDetails.addProperty("Designation",request.getParameter("designation") != null?request.getParameter("designation") : "");
        occupationDetails.addProperty("Month_Of_Joining","");
        occupationDetails.addProperty("Year_Of_Joining","");
        occupationDetails.addProperty("Office_Email","");
        occupationDetails.addProperty("Entity_Type",request.getParameter("entity") != null?request.getParameter("entity") : "");
        occupationDetails.addProperty("Years_in_Business", request.getParameter("businessYear")!= null?request.getParameter("businessYear") :"");
        occupationDetails.addProperty("ITR_Income", request.getParameter("ITRIncome")!= null?request.getParameter("ITRIncome") :"");
        occupationDetails.addProperty("ITR_Type", request.getParameter("ITRType")!= null?request.getParameter("ITRType") :"");
        occupationDetails.addProperty("Turnover_in_last_FY", request.getParameter("turnover") != null ? request.getParameter("turnover") : "");
        occupationDetails.addProperty("Occupation", request.getParameter("occupation") != null ? request.getParameter("occupation") : "" );
        occupationDetails.addProperty("Annual_Income",request.getParameter("annualIncome") != null ? Integer.parseInt(request.getParameter("annualIncome") ): null);
        occupationDetails.addProperty("Monthly_Income",request.getParameter("salary") != null ? Integer.parseInt(request.getParameter("salary")): null);
        occupationDetails.addProperty("Company_Name", request.getParameter("companyName")!= null ? request.getParameter("companyName") :"");
        occupationDetails.addProperty("Monthly_EMI", request.getParameter("liabilities") != null ? Integer.parseInt(request.getParameter("liabilities")) :null);

        jsonPLEmployeeForm.add("OccupationDetails", occupationDetails);

        JsonObject residenceAddrrss = new JsonObject();
        residenceAddrrss.addProperty("AddressLine", "");
        residenceAddrrss.addProperty("Pincode", request.getParameter("pinCode") != null ? request.getParameter("pinCode") : "");
        residenceAddrrss.addProperty("State", "");
        residenceAddrrss.addProperty("City", request.getParameter("city") != null ? request.getParameter("city") : "");
        residenceAddrrss.addProperty("Accommodation_Type", "");

        jsonPLEmployeeForm.add("ResidenceAddrrss",residenceAddrrss);

        JsonObject officeAddrrss = new JsonObject();
        officeAddrrss.addProperty("AddressLine", "");
        officeAddrrss.addProperty("Pincode",  request.getParameter("pinCode") != null ? request.getParameter("pinCode") : "");
        officeAddrrss.addProperty("State", "");
        officeAddrrss.addProperty("City",  request.getParameter("city") != null ? request.getParameter("city") : "");
        officeAddrrss.addProperty("Years_in_Current_Address", "");
        officeAddrrss.addProperty("Years_in_Current_City", "");
        officeAddrrss.addProperty("Months_in_Current_Address", "");
        officeAddrrss.addProperty("Months_in_Current_City", "");

        jsonPLEmployeeForm.add( "OfficeAddrrss", officeAddrrss);

        String  noOfAdults = request.getParameter("noofAdults")!= null ? request.getParameter("noofAdults") : "";
        String  noOfChildren = request.getParameter("noofChildren")!= null ? request.getParameter("noofChildren") : "";
        String noOfMembers="";
        if (!noOfAdults.equals("") && !noOfChildren.equals("")){
            noOfMembers = "No. of Adult " + noOfAdults + ", "+ "No. of Children " + noOfChildren;
        }

        JsonObject InsuranceDetails = new JsonObject();
        InsuranceDetails.addProperty("Vehicle_Registration_Number", request.getParameter("vehicleRegisterationNumber") != null ? request.getParameter("vehicleRegisterationNumber") : "");
        InsuranceDetails.addProperty("Vehicle_Registration_date", request.getParameter("vehicleRegisterationDate") != null ? request.getParameter("vehicleRegisterationDate") : "");
        InsuranceDetails.addProperty("Year_Of_Manufacture", request.getParameter("manufactureryear") != null ? request.getParameter("manufactureryear") : "");
        InsuranceDetails.addProperty("Travel_To", request.getParameter("travel") != null ? request.getParameter("travel") : "");
        InsuranceDetails.addProperty("Purpose", request.getParameter("purpose") != null ? request.getParameter("purpose") : "");
        InsuranceDetails.addProperty("Family_Members", request.getParameter("familyMember") != null ? request.getParameter("familyMember") : "");
        InsuranceDetails.addProperty("DOB_of_Family_Member", request.getParameter("familyMemberdob") != null ? request.getParameter("familyMemberdob") : "");
        InsuranceDetails.addProperty("Trip_Type", request.getParameter("tripType") != null ? request.getParameter("tripType") : "");
        InsuranceDetails.addProperty("Trip_Start_Date", request.getParameter("tripStartDate") != null ? request.getParameter("tripStartDate") : "");
        InsuranceDetails.addProperty("Trip_End_Date", request.getParameter("tripEndDate") != null ? request.getParameter("tripEndDate") : "");
        InsuranceDetails.addProperty("Medical_Conditions", request.getParameter("medical") != null ? request.getParameter("medical") : "");
        InsuranceDetails.addProperty("Sum_Insured", request.getParameter("loanAmount") != null ? request.getParameter("loanAmount") : "");
        InsuranceDetails.addProperty("Tenor", request.getParameter("tenure") != null ? request.getParameter("tenure") : "");
        InsuranceDetails.addProperty("Smoker", request.getParameter("smoker") != null ? request.getParameter("smoker") : "");
        InsuranceDetails.addProperty("Amount_to_be_invested", request.getParameter("amountInvested") != null ? request.getParameter("amountInvested") : "");
        InsuranceDetails.addProperty("Type_of_securities",request.getParameter("security") != null ? request.getParameter("security") : "");
        InsuranceDetails.addProperty("Total_Portfolio_value",request.getParameter("portfolio") != null ? request.getParameter("portfolio") : "");
        InsuranceDetails.addProperty("Campaign_Id",request.getParameter("campaignid") != null ? request.getParameter("campaignid") : "");
        InsuranceDetails.addProperty("Campaign_Source",request.getParameter("campaignsource") != null ? request.getParameter("campaignsource") : "");
        InsuranceDetails.addProperty("Personalized_Field",request.getParameter("personalizedfield") != null ? request.getParameter("personalizedfield") : "");
        InsuranceDetails.addProperty("Cust_consent",request.getParameter("custconsent") != null ? request.getParameter("custconsent") : "");
        InsuranceDetails.addProperty("Residence_Ownership", request.getParameter("residence")!= null?request.getParameter("residence") :"");
        InsuranceDetails.addProperty("Office_Ownership", request.getParameter("office")!= null?request.getParameter("office") :"");
        InsuranceDetails.addProperty("GST_Registered", request.getParameter("GST")!= null?request.getParameter("GST") :"");
        InsuranceDetails.addProperty("Salary_Credit_By", request.getParameter("salaryCredit")!= null?request.getParameter("salaryCredit") :"");
        InsuranceDetails.addProperty("Total_Work_Experience", request.getParameter("totalWorkExp")!= null?request.getParameter("totalWorkExp") :"");
        InsuranceDetails.addProperty("Existing_EMIs_Per_Month", request.getParameter("emiPerMonth")!= null?request.getParameter("emiPerMonth") :"");
        InsuranceDetails.addProperty("Annual_Business_Income", request.getParameter("annualBusinessIncome")!= null?request.getParameter("annualBusinessIncome") :"");
        InsuranceDetails.addProperty("ID_Type", request.getParameter("uniqueIdentifier")!= null?request.getParameter("uniqueIdentifier") :"");
        InsuranceDetails.addProperty("Comments", comment);
        InsuranceDetails.addProperty("Any_Existing_Ailment", request.getParameter("anyExistingAilment")!= null?request.getParameter("anyExistingAilment") :"");
        InsuranceDetails.addProperty("Any_specific_Requirement", request.getParameter("anySpecificRequirement")!= null?request.getParameter("anySpecificRequirement") :"");
        InsuranceDetails.addProperty("Appliance_in_warranty", request.getParameter("applianceinWarranty")!= null?request.getParameter("applianceinWarranty") :"");
        InsuranceDetails.addProperty("Coverage_Amount", request.getParameter("coverageAmount")!= null?request.getParameter("coverageAmount") :"1");
        InsuranceDetails.addProperty("Does_pet_have_existing_ailment", request.getParameter("doesthePethaveExistingAilment")!= null?request.getParameter("doesthePethaveExistingAilment") :"");
        InsuranceDetails.addProperty("Existing_Life_Insurance", request.getParameter("existingLifeInsurance")!= null?request.getParameter("existingLifeInsurance") :"");
        InsuranceDetails.addProperty("How_many_appliances", request.getParameter("howManyAppliance")!= null?request.getParameter("howManyAppliance") :"");
        InsuranceDetails.addProperty("Nationality", request.getParameter("nationality")!= null?request.getParameter("nationality") :"");
        InsuranceDetails.addProperty("Nature_Of_Business", request.getParameter("natureofBusiness")!= null?request.getParameter("natureofBusiness") :"");
        if(!noOfMembers.equals("")){
            InsuranceDetails.addProperty("No_Of_Members",noOfMembers);
        }else{
            InsuranceDetails.addProperty("No_Of_Members", request.getParameter("noofMembers")!= null?request.getParameter("noofMembers") :"");
        }
        InsuranceDetails.addProperty("Pet_Breed", request.getParameter("petBreed")!= null?request.getParameter("petBreed") :"");
        InsuranceDetails.addProperty("Plan_Selection", request.getParameter("planSelection")!= null?request.getParameter("planSelection") :"");
        InsuranceDetails.addProperty("Total_No_Of_Cards", request.getParameter("totalNoofCards")!= null?request.getParameter("totalNoofCards") :"");
        InsuranceDetails.addProperty("Workforce", request.getParameter("workForce")!= null?request.getParameter("workForce") :"");
        jsonPLEmployeeForm.add("InsuranceDetails", InsuranceDetails);

        JsonArray ReferenceDetails = new JsonArray();
        JsonObject details1 = new JsonObject();
        details1.addProperty("Name", "");
        details1.addProperty("Mobile_Number", "");
        details1.addProperty("Address", "");
        details1.addProperty("Pincode", "");
        ReferenceDetails.add(details1);
        JsonObject details2 = new JsonObject();
        details2.addProperty("Name", "");
        details2.addProperty("Mobile_Number", "");
        details2.addProperty("Address", "");
        details2.addProperty("Pincode", "");
        ReferenceDetails.add(details2);

        jsonPLEmployeeForm.add("ReferenceDetails", ReferenceDetails);

        jsonRequest.add("PLEmployeeForm", jsonPLEmployeeForm);
        log.info("REQUEST BODY : {} ",jsonRequest);

        String responseData = sfdcInvoker.sendPostRequest("/services/apexrest/ApplicationCreator", jsonRequest.toString(),configs.getAuthModel(),true,false);
        log.info("ICICI CREDIT CARD LEAD RESPONSE : {}",responseData);

        JsonObject respObj = new JsonObject();
        JsonObject responseBody = new JsonParser().parse(responseData).getAsJsonObject();
        if(responseBody.get("Message").getAsString().contains("Message:Insert failed"))
        {
            respObj.addProperty("Status", "Error");
            respObj.addProperty("Message", "Your mobile number is already registered for an ICICI credit card");
            respObj.add("LeadId", null);
        }
        else if(responseBody.get("Message").getAsString().contains("Product Code  is Mandatory") || responseBody.get("Status").getAsString().equalsIgnoreCase("Error"))
        {
            respObj.addProperty("Status", "Error");
            respObj.addProperty("Message", "something went wrong.");
            respObj.add("LeadId", null);
        }
        else
        {
            respObj = new JsonParser().parse(responseData).getAsJsonObject();
        }

        response.setContentType("application/json");
        response.getWriter().println(respObj);
    }
}
