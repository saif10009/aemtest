package com.tatacapital.moneyfy.core.services;

import java.util.Map;

public interface MoneyfyConfigs {

	 Map<String, String> getConfig();
}
