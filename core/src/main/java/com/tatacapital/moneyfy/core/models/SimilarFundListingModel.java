package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.SimilarFundListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SimilarFundListingModel {

    @Inject
    @Default(values = "")
    String similarFundIds;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;
    Logger log = LoggerFactory.getLogger(getClass());
    private List<FundListingPojo> fundList = new ArrayList<>();
    List<SimilarFundListingPojo> similarFundsList = new ArrayList<>();
    SimilarFundListingPojo similarFundListingPojo;

    @PostConstruct
    public void init() {

        JsonArray parsedId = new JsonParser().parse(similarFundIds).getAsJsonArray();
        List<String> parsedIds = new Gson().fromJson(parsedId, ArrayList.class);

        try {
            Resource resource = resourceResolver.getResource("/content/tata-capital-moneyfy/en/mutual-funds/");
            if (resource != null) {
                Page parentPage = resource.adaptTo(Page.class);
                if (parentPage != null) {
                    Iterator<Page> listChildPages = parentPage.listChildren();
                    while (listChildPages.hasNext()) {
                        Page childPages = listChildPages.next();
                        if (childPages.getProperties().get("pageType").equals("AMC")) {
                            Iterator<Page> amcChildren = childPages.listChildren();
                            while (amcChildren.hasNext()) {
                                Page fundPage = amcChildren.next();
                                if (fundPage.getProperties().get("pageType").equals("FUND")) {
                                    similarFundListingPojo = new SimilarFundListingPojo();
                                    similarFundListingPojo.setFundId(fundPage.getProperties().get("id").toString());

                                    if (parsedIds.contains(similarFundListingPojo.getFundId())) {
                                        JsonObject schemeDetails = new JsonParser().parse(fundPage.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                                        JsonObject ratings = new JsonParser().parse(fundPage.getProperties().get("ratings").toString()).getAsJsonObject();
                                        if(schemeDetails.get("optName").getAsString().equalsIgnoreCase("g")) {
                                            similarFundListingPojo.setFundName(schemeDetails.get("name").getAsString());
                                            float nav = !fundPage.getProperties().get("nav").toString().equals("") ? Float.parseFloat(fundPage.getProperties().get("nav").toString()) : 0;
                                            nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                                            similarFundListingPojo.setNav(nav);
                                            similarFundListingPojo.setSchemeId(schemeDetails.get("id").getAsString());
                                            similarFundListingPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                                            similarFundListingPojo.setFundManagerName(fundPage.getProperties().get("fundManagerName").toString());
                                            similarFundListingPojo.setMorningStar(ratings.get("morningStar").getAsString());
                                            similarFundListingPojo.setValueResearch(ratings.get("valueResearch").getAsString());
                                            similarFundListingPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                                            similarFundListingPojo.setMinSipAmt(fundPage.getProperties().get("minSipAmt").toString());
                                            String minLumpSumAmt = Objects.nonNull(fundPage.getProperties().get("minLumpsumAmt")) ?
                                                    (fundPage.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : fundPage.getProperties().get("minLumpsumAmt").toString()) : "0";
                                            similarFundListingPojo.setMinLumpsumAmt(minLumpSumAmt);
                                            similarFundListingPojo.setFundSize(fundPage.getProperties().get("fundSize").toString());
                                            similarFundListingPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                                            similarFundListingPojo.setPagePath(fundPage.getPath().concat(".html"));
                                            similarFundListingPojo.setIconPath(fundPage.getParent().getProperties().get("iconPath").toString());
                                            JsonArray cagrValues = new JsonParser().parse(fundPage.getProperties().get("cagrValues").toString()).getAsJsonArray();
                                            cagrValues.forEach(jsonElement -> {
                                                JsonObject cagrObj = jsonElement.getAsJsonObject();
                                                String tenure = cagrObj.get("tenure").getAsString();
                                                if(tenure.equals("1m"))
                                                {
                                                    if(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") || jsonElement.getAsJsonObject().get("value").getAsString().trim().equals("") ||Float.parseFloat(jsonElement.getAsJsonObject().get("value").getAsString())<=0.0f)
                                                    {
                                                        similarFundListingPojo.setGreaterThanZero(false);
                                                    }
                                                    else
                                                    {
                                                        similarFundListingPojo.setGreaterThanZero(true);
                                                    }
                                                }
                                                switch (tenure)
                                                {
                                                    case "1m":
                                                        similarFundListingPojo.setOneMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;
                                                    case "3m":
                                                        similarFundListingPojo.setThreeMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;
                                                    case "6m":
                                                        similarFundListingPojo.setSixMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;
                                                    case "1y":
                                                        similarFundListingPojo.setOneYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;
                                                    case "3y":
                                                        similarFundListingPojo.setThreeYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        similarFundListingPojo.setThreeYearValue(Float.valueOf(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") ? "0.0" : jsonElement.getAsJsonObject().get("value").getAsString()));
                                                        break;
                                                    case "5y":
                                                        similarFundListingPojo.setFiveYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;
                                                    case "mx":
                                                        similarFundListingPojo.setMaxReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                                        break;

                                                }
                                            });
                                            similarFundsList.add(similarFundListingPojo);
                                        }
                                    }
                                }

                            }


                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SimilarFundListingPojo> getSimilarFundsList() {
        return similarFundsList;
    }

    public List<Map<String,String>> getSimilarFundsCagr(){
        ArrayList fundCagrList = new ArrayList();
        List<SimilarFundListingPojo> similarFunds = getSimilarFundsList();
//        Map<String, String> cagrObj = new HashMap<>();
        try {
           for(int i=0; i<similarFunds.size(); i++){
               Map<String, Float> cagrObj = new HashMap<>();
               JsonArray cagrArray = new JsonParser().parse(similarFunds.get(i).getCagrValues()).getAsJsonArray();
               cagrArray.forEach(element -> {
                   JsonObject jsonObject = element.getAsJsonObject();
                   cagrObj.put(jsonObject.get("tenure").getAsString(), new Float(jsonObject.get("value").getAsString()));
               });
               fundCagrList.add(cagrObj);
           }
        }catch (Exception e){
            log.error(e.getMessage());
        }
        return fundCagrList;
    }
}
