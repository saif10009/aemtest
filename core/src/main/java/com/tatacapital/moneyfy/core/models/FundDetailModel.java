package com.tatacapital.moneyfy.core.models;

import com.day.cq.commons.jcr.JcrUtil;
import com.day.cq.wcm.api.Page;
import com.google.gson.*;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.ScriptVariable;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class FundDetailModel {
    static final String MANAGER_PAGE_PATH = "/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/";

    @Inject
    @Default(values = "")
    String amcMasterContactInfo;

    @Inject
    @Default(values = "")
    String fundManagerName;

    @Inject
    @Default(values = "")
    String fundDetailName;

    @Inject
    @Default(values = "")
    String aum;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    String amcAddress;
    String amcVisit;
    String amcMail;
    String amcPhone;
    boolean isOneYearGreaterThanZero;

    @ScriptVariable
    private ValueMap pageProperties;

    public String getAum() {
        return aum;
    }

    public void setAum(String aum) {
        this.aum = aum;
    }

    public String getAmcMasterContactInfo() {
        return amcMasterContactInfo;
    }

    public void setAmcMasterContactInfo(String amcMasterContactInfo) {
        this.amcMasterContactInfo = amcMasterContactInfo;
    }

    public String getAmcAddress() {
        return amcAddress;
    }

    public void setAmcAddress(String amcAddress) {
        this.amcAddress = amcAddress;
    }

    public String getAmcVisit() {
        return amcVisit;
    }

    public void setAmcVisit(String amcVisit) {
        this.amcVisit = amcVisit;
    }

    public String getAmcMail() {
        return amcMail;
    }

    public void setAmcMail(String amcMail) {
        this.amcMail = amcMail;
    }

    public String getAmcPhone() {
        return amcPhone;
    }

    public void setAmcPhone(String amcPhone) {
        this.amcPhone = amcPhone;
    }

    public boolean isOneYearGreaterThanZero() {
        return isOneYearGreaterThanZero;
    }

    public void setOneYearGreaterThanZero(boolean oneYearGreaterThanZero) {
        isOneYearGreaterThanZero = oneYearGreaterThanZero;
    }

    public void getAmcInfo() {
        JsonObject obj = new JsonParser().parse(amcMasterContactInfo).getAsJsonObject();
        setAmcMail(obj.get("mail").getAsString());
        setAmcAddress(obj.get("address").getAsString());
        setAmcPhone(obj.get("phone").getAsString());
        setAmcVisit(obj.get("visit").getAsString());
    }

    @PostConstruct
    public void init() {
        getAmcInfo();
        getSchemeDetails();
    }

    public HashMap<String, Object> getSchemeDetails() {
        JsonObject object = (JsonObject) new JsonParser().parse(pageProperties.get("schemeDetails").toString());
        HashMap<String, Object> schemeDetails = new Gson().fromJson(object.toString(), HashMap.class);
        return schemeDetails;
    }

    public HashMap<String, Object> getRatings() {
        JsonObject object = (JsonObject) new JsonParser().parse(pageProperties.get("ratings").toString());
        HashMap<String, Object> schemeDetails = new Gson().fromJson(object.toString(), HashMap.class);
        return schemeDetails;
    }

    public HashMap<String, Object> getBifurcationPercentage() {
        JsonObject object = (JsonObject) new JsonParser().parse(pageProperties.get("bifurcationePerc").toString());
        HashMap<String, Object> bifurcationPercentage = new Gson().fromJson(object.toString(), HashMap.class);
        return bifurcationPercentage;
    }

    public Map<String, Object> getCagrValues() {
        Map<String, Object> cagrObj = new HashMap<>();
        try {
            JsonArray cagrArray = new JsonParser().parse(pageProperties.get("cagrValues").toString()).getAsJsonArray();
            cagrArray.forEach(element -> {
                JsonObject jsonObject = element.getAsJsonObject();
                 String oneMonth= jsonObject.get("tenure").getAsString();
                 if(oneMonth.equals("1m"))
                 {
                     if(jsonObject.get("value").getAsString().equals("null") || jsonObject.get("value").getAsString().trim().equals("") ||Float.parseFloat(jsonObject.get("value").getAsString()) <0.0f)
                     {
                         setOneYearGreaterThanZero(false);
                     }
                     else
                     {
                         setOneYearGreaterThanZero(true);
                     }
                 }
                if(jsonObject.get("value").getAsString().trim().equals("") || jsonObject.get("value").getAsString().equals("null") || jsonObject.get("value").getAsString().equals("0.0"))
                {
                    cagrObj.put(jsonObject.get("tenure").getAsString(),"-");
                }else
                {
                    String cagrVal=jsonObject.get("value").getAsString();
                    cagrObj.put(jsonObject.get("tenure").getAsString(), cagrVal);
                }

            });
        } catch (Exception e) {
            //log.error(e.getMessage());
        }
        return cagrObj;
    }

    public List<String> getExitLoadValues() {
        List<String> exitLoadValues = new ArrayList<>();
        try {
            JsonArray jsonArray = new JsonParser().parse(pageProperties.get("exitLoadValues").toString()).getAsJsonArray();
            exitLoadValues = new Gson().fromJson(jsonArray, ArrayList.class);
        } catch (Exception e) {
            //log.error(e.getMessage());
        }
        return exitLoadValues;
    }

    public List<String> getCompanyHoldings() {
        JsonObject holdingJsonObject = new JsonObject();
        holdingJsonObject = new JsonParser().parse(pageProperties.get("holdingDetails").toString()).getAsJsonObject();
        JsonArray jsonArray = new JsonArray();
        jsonArray = holdingJsonObject.get("company").getAsJsonArray();
        List<String> companyHoldings = new Gson().fromJson(jsonArray, ArrayList.class);
        return companyHoldings;
    }

    public List<String> getSectors() {
        List<String> listOfSectors = new ArrayList<>();
        JsonObject jsonObject = new JsonObject();
        JsonArray jsonArrayOfSector = new JsonArray();
        JsonObject holdingDetailsJsonObject = new JsonParser().parse(pageProperties.get("holdingDetails").toString()).getAsJsonObject();
        JsonArray sectorArr = holdingDetailsJsonObject.get("sector").getAsJsonArray();
        for(JsonElement jsonElement : sectorArr)
        {
            JsonObject sectorJsonObj = new JsonObject();
            JsonObject sectorObj = jsonElement.getAsJsonObject();
            String name = sectorObj.get("name").getAsString();
            String percentage = sectorObj.get("percentage").getAsString();
            float percentageFloatValue = Float.parseFloat(percentage);
            String percentageInTwoDigit = String.format("%.2f", percentageFloatValue);
            sectorJsonObj.addProperty("name",name);
            sectorJsonObj.addProperty("percentage",percentageInTwoDigit);
            jsonArrayOfSector.add(sectorJsonObj);
        }
        jsonObject.add("sector", jsonArrayOfSector);
        JsonArray jsonArray = jsonObject.get("sector").getAsJsonArray();
        listOfSectors = new Gson().fromJson(jsonArray, ArrayList.class);
        return listOfSectors;
    }

    public Float getNavPercentage() {
        float navPerc = Float.parseFloat(pageProperties.get("navPerc").toString());
        navPerc = Float.valueOf(String.format(Locale.getDefault(), "%.2f", navPerc));
        return navPerc;
    }

    public float getNav() {
        float nav = Float.parseFloat(pageProperties.get("nav").toString());
        nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
        return nav;
    }

    public String getFundSize() {
        String fundSize = pageProperties.get("fundSize").toString();
        String fundRoundOfSize=null;
        if(fundSize.equals("null") || fundSize.trim().equals(""))
        {
            fundRoundOfSize="-";
        }
        else
            {
            fundRoundOfSize = format(fundSize);
        }
        return fundRoundOfSize;
    }

    public Map<String, String> getManagerDetails(){
        Map<String, String> managerDetails = new HashMap<>();
        String managerNodeName = JcrUtil.createValidName(fundManagerName,JcrUtil.HYPHEN_LABEL_CHAR_MAPPING);
        Resource resource = resourceResolver.getResource(MANAGER_PAGE_PATH.concat(managerNodeName));
        Page page = resource.adaptTo(Page.class);
        managerDetails.put("managerName",page.getProperties().get("name").toString());
        managerDetails.put("fundOffered",page.getProperties().get("totalFunds").toString());
        managerDetails.put("highestReturn",page.getProperties().get("highestReturns").toString());
        managerDetails.put("description",page.getProperties().get("jcr:description").toString());
        String fundSizeValue = page.getProperties().get("totalFundSize").toString();
        String roundOfStr = format(fundSizeValue);
        managerDetails.put("fundSize",roundOfStr);
        return managerDetails;
    }

    public Map<String,Object> getMinSipAndLumpSum()
    {
        Map<String, Object> investmentDetails = new HashMap<>();

        String minSipAmt = pageProperties.get("minSipAmt").toString().equals("null") ? "-": pageProperties.get("minSipAmt").toString().trim().equals("") ? "-" :  pageProperties.get("minSipAmt").toString();
        String minLumpSumAmt = pageProperties.get("minLumpsumAmt").toString().equals("null") ? "-": pageProperties.get("minLumpsumAmt").toString().trim().equals("") ? "-" :  pageProperties.get("minLumpsumAmt").toString();

        if(minSipAmt.equals("-"))
        {
            investmentDetails.put("minSip","-");
        }

        if(minLumpSumAmt.equals("-"))
        {
            investmentDetails.put("minLumpSumAmt","-");
        }
        if(!minSipAmt.equals("-"))
        {
            Float minSip = Float.parseFloat(minSipAmt);
            investmentDetails.put("minSip",Math.round(minSip));
        }
        if(!minLumpSumAmt.equals("-"))
        {
            Float minLumpSum = Float.parseFloat(minLumpSumAmt);
            investmentDetails.put("minLumpSumAmt",Math.round(minLumpSum));
        }
        return investmentDetails;
    }

    public Map<String,String> getAumFromAmc()
    {
        Map<String, String> aumDetail = new HashMap<>();
        String aumValue = aum;
        String aumInCurrencyFormat = format(aumValue);
        aumDetail.put("aum",aumInCurrencyFormat);
        return aumDetail;
    }


    public String format(String value){
        Float currencyValueConvertIntoFloat = Float.valueOf(value);
        int currencyValueRoundOf = Math.round(currencyValueConvertIntoFloat);
        String currencyValueInString = String.valueOf(currencyValueRoundOf);

        String temp ="";
        //Removing extra '0's
        while(currencyValueInString.substring(0,1).equals("0") && currencyValueInString.length()>1) {
            currencyValueInString = currencyValueInString.substring(1);
        }

        if(currencyValueInString.length()<=3){
            return currencyValueInString;

        }

        else if(currencyValueInString.length()%2 !=0) {
            temp = currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);}

        else {
            temp = currencyValueInString.substring(0, 1);
            currencyValueInString = currencyValueInString.substring(1);
        }
        while (currencyValueInString.length() > 3) {
            temp += "," + currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);

        }

        return temp+","+currencyValueInString;
    }

    public Map<String,String> getSubCatNameWithPath()
    {
        Map<String, String> subCatName = new HashMap<>();
        JsonObject object = (JsonObject) new JsonParser().parse(pageProperties.get("schemeDetails").toString());
        String subCategoryName = (object.get("subCatName").equals("null") || object.get("subCatName").getAsString().trim().equals("")) ? "-": object.get("subCatName").getAsString();
        String catName = object.get("catName").getAsString();
        String subCatNameWithHypen = subCategoryName.trim().toLowerCase()
                                        .replaceAll("\\s+", "-")
                                        .replaceAll("[^a-zA-Z0-9]", "-")
                                        .replaceAll("\\-+", "-")
                                        .replaceAll("-[^a-zA-Z]*$","");
        subCatName.put("subCatName", subCategoryName);
        if(catName.equalsIgnoreCase("other"))
        {
            catName = catName.concat("s");
        }
        subCatName.put("pagePath", "/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-"+catName.toLowerCase()+"/"+subCatNameWithHypen+".html");
        return subCatName;
    }
}
