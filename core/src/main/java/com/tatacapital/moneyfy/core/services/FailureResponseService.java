package com.tatacapital.moneyfy.core.services;

import com.google.gson.JsonObject;

public interface FailureResponseService {

    JsonObject getFailureResponse(Exception exception);
}
