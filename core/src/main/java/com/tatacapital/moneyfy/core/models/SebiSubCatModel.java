package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.SebiCatCardPojo;
import com.tatacapital.moneyfy.core.models.pojo.SebiCatPagesListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class SebiSubCatModel {

    @Inject
    @Default(values = "")
    String subCatName;

    public String sebiSubCatFundList;
    /*    public String jsonStringFundList;*/

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;
    Logger logger = LoggerFactory.getLogger(getClass());

   private List<FundListingPojo> subCatFundList = new ArrayList<>();

    @PostConstruct
    public void init() {

        /*Session session = null;*/
        try {

            Session session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='FUND'";
            stringBuffer.append(fundManagerQuery);
            logger.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Asset query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext())
            {
                FundListingPojo fundListingPojo = new FundListingPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                JsonObject schemeDetails = new JsonParser().parse(page.getProperties().get("schemeDetails").toString()).getAsJsonObject();
                if (schemeDetails.get("subCatName").getAsString().toUpperCase().equals(subCatName.toUpperCase()))
                {
                    if(schemeDetails.get("optName").getAsString().equalsIgnoreCase("g"))
                    {
                        fundListingPojo.setIconPath(!page.getParent().getProperties().get("iconPath").toString().equals("") ? page.getParent().getProperties().get("iconPath").toString() : "");
                        fundListingPojo.setPagePath(page.getPath().concat(".html"));
                        JsonObject ratings = new JsonParser().parse(page.getProperties().get("ratings").toString()).getAsJsonObject();
                        fundListingPojo.setFundName(schemeDetails.get("name").getAsString());
                        fundListingPojo.setSchemeId(schemeDetails.get("id").getAsInt());
                        float nav = !page.getProperties().get("nav").toString().equals("") && !page.getProperties().get("nav").toString().equals("null") ? Float.parseFloat(page.getProperties().get("nav").toString()) : 0;
                        nav = Float.valueOf(String.format(Locale.getDefault(), "%.2f", nav));
                        fundListingPojo.setNav(nav);
                        fundListingPojo.setCagrValues(page.getProperties().get("cagrValues").toString());
                        fundListingPojo.setCategoryName(schemeDetails.get("catName").getAsString());
                        fundListingPojo.setFundManagerName(page.getProperties().get("fundManagerName").toString());
                        fundListingPojo.setMorningStar(ratings.get("morningStar").getAsString());
                        fundListingPojo.setValueResearch(ratings.get("valueResearch").getAsString());
                        fundListingPojo.setRiskType(schemeDetails.get("riskType").getAsString());
                        String minSipAmt = page.getProperties().get("minSipAmt").toString().equals("") ? "0" : page.getProperties().get("minSipAmt").toString().equals("null") ? "0" :page.getProperties().get("minSipAmt").toString();
                        String minLumpSumAmt = Objects.nonNull(page.getProperties().get("minLumpsumAmt")) ?
                                (page.getProperties().get("minLumpsumAmt").toString().trim().equals("") ? "0" : page.getProperties().get("minLumpsumAmt").toString()) : "0";
                        fundListingPojo.setMinSipAmt(Float.valueOf(minSipAmt));
                        fundListingPojo.setMinLumpsumAmt(Float.valueOf(minLumpSumAmt));
                        String fundSize = page.getProperties().get("fundSize").toString().equals("") ? "0" :page.getProperties().get("fundSize").toString().equals("null") ? "0" : page.getProperties().get("fundSize").toString();
                        fundListingPojo.setFundSize(Float.valueOf(fundSize));
                        fundListingPojo.setSchemeOption(schemeDetails.get("optName").getAsString());
                        JsonArray cagrJsonArray = new JsonParser().parse(page.getProperties().get("cagrValues").toString()).getAsJsonArray();
                        cagrJsonArray.forEach(jsonElement -> {
                            JsonObject cagrObj = jsonElement.getAsJsonObject();
                            String tenure = cagrObj.get("tenure").getAsString();
                            if(tenure.equals("1m"))
                            {
                                if(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") || jsonElement.getAsJsonObject().get("value").getAsString().trim().equals("") || Float.parseFloat(jsonElement.getAsJsonObject().get("value").getAsString())<=0.0f)
                                {
                                    fundListingPojo.setGreaterThanZero(false);
                                }
                                else
                                {
                                    fundListingPojo.setGreaterThanZero(true);
                                }
                            }
                            switch (tenure)
                            {
                                case "1m":
                                    fundListingPojo.setOneMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;
                                case "3m":
                                    fundListingPojo.setThreeMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;
                                case "6m":
                                    fundListingPojo.setSixMonthReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;
                                case "1y":
                                    fundListingPojo.setOneYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;
                                case "3y":
                                    fundListingPojo.setThreeYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    fundListingPojo.setThreeYearValue(Float.valueOf(jsonElement.getAsJsonObject().get("value").getAsString().equals("null") ? "0.0" : jsonElement.getAsJsonObject().get("value").getAsString()));
                                    break;
                                case "5y":
                                    fundListingPojo.setFiveYearReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;
                                case "mx":
                                    fundListingPojo.setMaxReturn(jsonElement.getAsJsonObject().get("value").getAsString());
                                    break;

                            }
                        });


                        subCatFundList.add(fundListingPojo);
                    }
                }

            }

        } catch (RepositoryException e) {
            logger.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }

    }

    public List<FundListingPojo> getFundList() {
        subCatFundList.size();
        return subCatFundList;
    }
    public List getSort()
    {
        Collections.sort(subCatFundList, new Comparator<Object>() {

            @Override
            public int compare(Object o1, Object o2) {
                FundListingPojo c1 = (FundListingPojo) o1;
                FundListingPojo c2 = (FundListingPojo) o2;
                return c2.getThreeYearValue().compareTo(c1.getThreeYearValue());
            }
        });
        return subCatFundList;
    }

   /* public List<Map<String, String>> getSebiSubCatFundsCagr() {
        ArrayList fundCagrList = new ArrayList();
        List<FundListingPojo> similarFunds = getSebiSubCatFundList();
        try {
            for (int i = 0; i < similarFunds.size(); i++) {
                Map<String, Float> cagrObj = new HashMap<>();
                JsonArray cagrArray = new JsonParser().parse(similarFunds.get(i).getCagrValues()).getAsJsonArray();
                cagrArray.forEach(element -> {
                    JsonObject jsonObject = element.getAsJsonObject();
                    cagrObj.put(jsonObject.get("tenure").getAsString(), new Float(jsonObject.get("value").getAsString()));
                });
                fundCagrList.add(cagrObj);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return fundCagrList;
    }*/

    public String getSebiCatPageList() {
        Map<String, Object> catBasedPages = new HashMap<>();
        ArrayList equityPageList = new ArrayList<>();
        ArrayList debtPageList = new ArrayList<>();
        ArrayList hybridPageList = new ArrayList<>();
        ArrayList othersPageList = new ArrayList<>();
        Session session = null;
        try {
            session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='SEBI_SUB_CATEGORY'";
            stringBuffer.append(fundManagerQuery);
            logger.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Asset query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                SebiCatPagesListingPojo sebiCatPagesListingPojo = new SebiCatPagesListingPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                //fundListingPojo.setIconPath(!childPage.getProperties().get("iconPath").equals("") ? childPage.getProperties().get("iconPath").toString() : "");
                sebiCatPagesListingPojo.setSubCatPagePath(page.getPath().concat(".html"));
                sebiCatPagesListingPojo.setSubCatName(page.getProperties().get("subCatName").toString());

                if (page.getProperties().get("catName").toString().toUpperCase().equals("EQUITY")) {
                    equityPageList.add(sebiCatPagesListingPojo);
                } else if (page.getProperties().get("catName").toString().toUpperCase().equals("HYBRID")) {
                    hybridPageList.add(sebiCatPagesListingPojo);
                } else if (page.getProperties().get("catName").toString().toUpperCase().equals("DEBT")) {
                    debtPageList.add(sebiCatPagesListingPojo);
                } else if (page.getProperties().get("catName").toString().toUpperCase().equals("OTHER")) {
                    othersPageList.add(sebiCatPagesListingPojo);
                }
            }
            catBasedPages.put("Equity", equityPageList);
            catBasedPages.put("Hybrid", hybridPageList);
            catBasedPages.put("Debt", debtPageList);
            catBasedPages.put("Others", othersPageList);

        } catch (RepositoryException e) {
            logger.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }
        Gson gson = new Gson();
        return gson.toJson(catBasedPages);
    }

    public String getStringSebiSubCatFundList() {
        Gson gson = new Gson();
        this.sebiSubCatFundList = gson.toJson(subCatFundList);
        return sebiSubCatFundList;
    }

    public Map<String, ArrayList> getSebiCatCards() {
        Map<String, ArrayList> catBasedCards = new HashMap<>();
        ArrayList equityCat = new ArrayList<>();
        ArrayList debtCat = new ArrayList<>();
        ArrayList hybridCat = new ArrayList<>();
        ArrayList othersCat = new ArrayList<>();
        Session session = null;
        try {
            session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/]) and s.[pageType]='SEBI_SUB_CATEGORY'";
            stringBuffer.append(fundManagerQuery);
            logger.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Asset query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                SebiCatCardPojo sebiCatCardPojo = new SebiCatCardPojo();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                sebiCatCardPojo.setPagePath(page.getPath().concat(".html"));
                sebiCatCardPojo.setTitle(page.getProperties().get("subCatName").toString());
                sebiCatCardPojo.setIconPath(page.getProperties().get("iconPath").toString());
                if(page.getProperties().get("catName").toString().toUpperCase().equals("EQUITY")){
                    equityCat.add(sebiCatCardPojo);
                }else if(page.getProperties().get("catName").toString().toUpperCase().equals("HYBRID")){
                    hybridCat.add(sebiCatCardPojo);
                }else if(page.getProperties().get("catName").toString().toUpperCase().equals("DEBT")){
                    debtCat.add(sebiCatCardPojo);
                }else if(page.getProperties().get("catName").toString().toUpperCase().equals("OTHER")){
                    othersCat.add(sebiCatCardPojo);
                }
            }
            catBasedCards.put("Equity",equityCat);
            catBasedCards.put("Hybrid",hybridCat);
            catBasedCards.put("Debt",debtCat);
            catBasedCards.put("Others",othersCat);

        } catch (RepositoryException e) {
            logger.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }
        return catBasedCards;
    }
}



