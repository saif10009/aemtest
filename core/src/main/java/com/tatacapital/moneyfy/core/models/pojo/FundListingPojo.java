package com.tatacapital.moneyfy.core.models.pojo;

public class FundListingPojo {
    private String fundName;
    private Float nav;
    private String morningStar;
    private String valueResearch;
    private String riskType;
    private Float minSipAmt;
    private Float minLumpsumAmt;
    private String fundManagerName;
    private Float fundSize;
    private String cagrValues;
    private String categoryName;
    private String schemeOption;
    private String pagePath;
    private String iconPath;
    private String oneMonthReturn;
    private String threeMonthReturn;
    private String sixMonthReturn;
    private String oneYearReturn;
    private String threeYearReturn;
    private String fiveYearReturn;
    private String maxReturn;
    private int schemeId;
    private String updatedDate;
    private boolean isGreaterThanZero;
    private Float threeYearValue;

    public boolean isGreaterThanZero() {
        return isGreaterThanZero;
    }

    public void setGreaterThanZero(boolean greaterThanZero) {
        this.isGreaterThanZero = greaterThanZero;
    }

    public int getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(int schemeId) {
        this.schemeId = schemeId;
    }

    public String getMorningStar() {
        return morningStar;
    }

    public void setMorningStar(String morningStar) {
        this.morningStar = morningStar;
    }

    public String getValueResearch() {
        return valueResearch;
    }

    public void setValueResearch(String valueResearch) {
        this.valueResearch = valueResearch;
    }

    public String getIconPath() {
        return iconPath;
    }

    public void setIconPath(String iconPath) {
        this.iconPath = iconPath;
    }

    public String getFundName() {
        return fundName;
    }

    public void setFundName(String fundName) {
        this.fundName = fundName;
    }

    public Float getNav() {
        return nav;
    }

    public void setNav(Float nav) {
        this.nav = nav;
    }

    public String getRiskType() {
        return riskType;
    }

    public void setRiskType(String riskType) {
        this.riskType = riskType;
    }

    public Float getMinSipAmt() {
        return minSipAmt;
    }

    public void setMinSipAmt(Float minSipAmt) {
        this.minSipAmt = minSipAmt;
    }

    public String getFundManagerName() {
        return fundManagerName;
    }

    public void setFundManagerName(String fundManagerName) {
        this.fundManagerName = fundManagerName;
    }

    public Float getFundSize() {
        return fundSize;
    }

    public void setFundSize(Float fundSize) {
        this.fundSize = fundSize;
    }

    public String getCagrValues() {
        return cagrValues;
    }

    public void setCagrValues(String cagrValues) {
        this.cagrValues = cagrValues;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getSchemeOption() {
        return schemeOption;
    }

    public void setSchemeOption(String schemeOption) {
        this.schemeOption = schemeOption;
    }

    public String getPagePath() {
        return pagePath;
    }

    public void setPagePath(String pagePath) {
        this.pagePath = pagePath;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public String getOneMonthReturn() {
        return oneMonthReturn;
    }

    public String getThreeMonthReturn() {
        return threeMonthReturn;
    }

    public String getSixMonthReturn() {
        return sixMonthReturn;
    }

    public String getOneYearReturn() {
        return oneYearReturn;
    }

    public String getThreeYearReturn() {
        return threeYearReturn;
    }

    public String getFiveYearReturn() {
        return fiveYearReturn;
    }

    public String getMaxReturn() {
        return maxReturn;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public void setOneMonthReturn(String oneMonthReturn) {
        this.oneMonthReturn = oneMonthReturn;
    }

    public void setThreeMonthReturn(String threeMonthReturn) {
        this.threeMonthReturn = threeMonthReturn;
    }

    public void setSixMonthReturn(String sixMonthReturn) {
        this.sixMonthReturn = sixMonthReturn;
    }

    public void setOneYearReturn(String oneYearReturn) {
        this.oneYearReturn = oneYearReturn;
    }

    public void setThreeYearReturn(String threeYearReturn) {
        this.threeYearReturn = threeYearReturn;
    }

    public void setFiveYearReturn(String fiveYearReturn) {
        this.fiveYearReturn = fiveYearReturn;
    }

    public void setMaxReturn(String maxReturn) {
        this.maxReturn = maxReturn;
    }

    public Float getThreeYearValue() {
        return threeYearValue;
    }

    public void setThreeYearValue(Float threeYearValue) {
        this.threeYearValue = threeYearValue;
    }

    public Float getMinLumpsumAmt() {
        return minLumpsumAmt;
    }

    public void setMinLumpsumAmt(Float minLumpsumAmt) {
        this.minLumpsumAmt = minLumpsumAmt;
    }
}

