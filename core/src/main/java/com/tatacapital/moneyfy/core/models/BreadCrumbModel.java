package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import com.google.gson.JsonObject;
import com.tatacapital.moneyfy.core.services.ResourceHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.LoginException;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.PathNotFoundException;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.day.cq.wcm.api.NameConstants.NN_CONTENT;

@Model(
        adaptables = {SlingHttpServletRequest.class},
        resourceType = {BreadCrumbModel.COMPONENT_PATH},
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class BreadCrumbModel {

    protected static final String COMPONENT_PATH = "/apps/tata-capital-moneyfy/components/content/mutual-funds/moneyfy-breadcrumb";

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    @Inject
    Page currentPage;

    @Self
    SlingHttpServletRequest request;

    List<JsonObject> breadCrumbList = new ArrayList<>();

    @PostConstruct
    private void init() throws LoginException {
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        Resource res = resourceResolver.getResource(currentPage.getPath());
        String path = res.getPath();
        PageManager pageManager = res.getResourceResolver().adaptTo(PageManager.class);
        String currentPageNode;

        JsonObject BreadcrumbJson = new JsonObject();
        String currentPageName = pageManager.getContainingPage(res).getPageTitle();
        String currentPagePath = pageManager.getContainingPage(res).getPath();
        currentPageNode = pageManager.getPage(currentPagePath).getParent().getName();
        String homeshow = "false";

        try {
            Node node = session.getNode(path);
            if (node.hasProperty("homeshow")) {
                homeshow = node.getProperty("homeshow").getValue().toString();
            }
            String currPagePath = currentPagePath;
            int count = 1;
            while (!currentPageNode.equals("en")) {
                String parentPagePath = pageManager.getPage(currPagePath).getParent().getPath();
                currPagePath = parentPagePath;
                currentPageNode = pageManager.getPage(currPagePath).getParent().getName();
                count++;
            }

            BreadcrumbJson.addProperty("@type", "ListItem");
            if (homeshow == "false") {
                BreadcrumbJson.addProperty("position", count + 1);
            } else {
                BreadcrumbJson.addProperty("position", count);
            }
            String domainName = "https://www.tatacapitalmoneyfy.com/";
            String finalPagePath = currentPagePath.replace("/content/tata-capital-moneyfy/en/",domainName);

            if(currentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-equity")||
                    currentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-debt") ||
                    currentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-hybrid")||
                    currentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-others"))
            {
                BreadcrumbJson.addProperty("name", "SEBI Category");
            }else {
                BreadcrumbJson.addProperty("name", currentPageName);
            }
            BreadcrumbJson.addProperty("item", finalPagePath);
            this.breadCrumbList.add(BreadcrumbJson);


            currPagePath = currentPagePath;
            for (int i = count; i >= 1; i--) {
                JsonObject BreadcrumbJsonParent = new JsonObject();
                String parentPageName = pageManager.getPage(currPagePath).getParent().getPageTitle();
                String parentPagePath = pageManager.getPage(currPagePath).getParent().getPath();
                currPagePath = parentPagePath;

                currentPageNode = pageManager.getPage(currPagePath).getName();

                if (homeshow == "false") {
                    if (currentPageNode.equals("en")) {
                        BreadcrumbJsonParent.addProperty("@type", "ListItem");
                        BreadcrumbJsonParent.addProperty("position", 1);
                        BreadcrumbJsonParent.addProperty("name", "Home");
                        BreadcrumbJsonParent.addProperty("item", "https://www.tatacapitalmoneyfy.com/");
                        this.breadCrumbList.add(BreadcrumbJsonParent);
                    } else {
                        BreadcrumbJsonParent.addProperty("@type", "ListItem");
                        BreadcrumbJsonParent.addProperty("position", i);
                        if( parentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-equity") ||
                                parentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-debt") ||
                                parentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-hybrid")||
                                parentPagePath.equals("/content/tata-capital-moneyfy/en/mutual-funds/sebi-categories-others")
                        )
                        {
                            BreadcrumbJsonParent.addProperty("name", "SEBI Category");
                        }
                        else {
                            BreadcrumbJsonParent.addProperty("name", parentPageName);
                        }
                        BreadcrumbJsonParent.addProperty("item", parentPagePath.replace("/content/tata-capital-moneyfy/en/",domainName));
                        this.breadCrumbList.add(BreadcrumbJsonParent);
                    }
                } else if (!currentPageNode.equals("en")){
                    BreadcrumbJsonParent.addProperty("@type", "ListItem");
                    BreadcrumbJsonParent.addProperty("position", i - 1);
                    BreadcrumbJsonParent.addProperty("name", parentPageName);
                    BreadcrumbJsonParent.addProperty("item", parentPagePath.replace("/content/tata-capital-moneyfy/en/",domainName));
                    this.breadCrumbList.add(BreadcrumbJsonParent);
                }

            }


        } catch (PathNotFoundException e) {
            e.printStackTrace();
        } catch (RepositoryException e) {
            e.printStackTrace();
        }
        Collections.reverse(this.breadCrumbList);

    }

    public List<JsonObject> getBreadCrumbSchemaArr() {
        return this.breadCrumbList;
    }


}
