package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tatacapital.moneyfy.core.models.pojo.AmcListingPojo;
import com.tatacapital.moneyfy.core.models.pojo.FundListingPojo;
import com.tatacapital.moneyfy.core.services.ResourceHelper;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.RepositoryException;
import javax.jcr.Session;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import java.text.NumberFormat;
import java.util.*;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class AMCManagerListingModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    @Inject
    @Default(values = "")
    public String amcId;

    Logger logger = LoggerFactory.getLogger(getClass());

    public List<Map<String,String>> getAmcManagerList() {
        ResourceHelper resourceHelper = null;
        Session session = null;
        ArrayList amcManagerList = new ArrayList();
        try {
            session = resourceResolver.adaptTo(Session.class);
            QueryManager queryManager = session.getWorkspace().getQueryManager();
            StringBuffer stringBuffer = new StringBuffer();
            String fundManagerQuery = "SELECT * FROM [cq:PageContent] AS s WHERE ISDESCENDANTNODE([/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/]) and s.[pageType]='FUND_MANAGER' and s.[amcId]='" + amcId + "'";
            stringBuffer.append(fundManagerQuery);
            logger.info("Asset stringBuffer : " + stringBuffer);
            Query query = queryManager.createQuery(stringBuffer.toString(), Query.JCR_SQL2);
            logger.info("Asset query : " + stringBuffer.toString());
            QueryResult queryResult = query.execute();
            NodeIterator nodeIterator = queryResult.getNodes();
            while (nodeIterator.hasNext()) {
                Map<String, String> managerDetail = new HashMap<>();
                Node node = nodeIterator.nextNode();
                Resource resource = resourceResolver.getResource(node.getParent().getPath());
                Page page = resource.adaptTo(Page.class);
                //fundListingPojo.setIconPath(!childPage.getProperties().get("iconPath").equals("") ? childPage.getProperties().get("iconPath").toString() : "");
                managerDetail.put("fundManagerName",page.getProperties().get("name").toString());
                managerDetail.put("numberOfFunds",page.getProperties().get("totalFunds").toString());
                String fundSize = page.getProperties().get("totalFundSize").toString();
                String totalFundSize = format(fundSize);
                managerDetail.put("fundSize",totalFundSize);
                managerDetail.put("highestReturns",page.getProperties().get("highestReturns").toString());
                managerDetail.put("pagePath",page.getPath().toString());
                amcManagerList.add(managerDetail);
            }
        }catch(RepositoryException e){
            logger.info("RepositoryException : " + e.getMessage());
            e.printStackTrace();
        }
        return amcManagerList;
    }

    public String format(String value){
        Float currencyValueConvertIntoFloat = Float.valueOf(value);
        int currencyValueRoundOf = Math.round(currencyValueConvertIntoFloat);
        String currencyValueInString = String.valueOf(currencyValueRoundOf);

        String temp ="";
        //Removing extra '0's
        while(currencyValueInString.substring(0,1).equals("0") && currencyValueInString.length()>1) {
            currencyValueInString = currencyValueInString.substring(1);
        }

        if(currencyValueInString.length()<=3){
            return currencyValueInString;

        }

        else if(currencyValueInString.length()%2 !=0) {
            temp = currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);}

        else {
            temp = currencyValueInString.substring(0, 1);
            currencyValueInString = currencyValueInString.substring(1);
        }
        while (currencyValueInString.length() > 3) {
            temp += "," + currencyValueInString.substring(0, 2);
            currencyValueInString = currencyValueInString.substring(2);

        }

        return temp+","+currencyValueInString;
    }
}
