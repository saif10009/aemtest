package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import com.google.gson.Gson;
import com.tatacapital.moneyfy.core.models.pojo.ManagerListingPojo;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class ManagerListingModel {

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private List<ManagerListingPojo> managerList = new ArrayList<>();

    Logger log = LoggerFactory.getLogger(getClass());
    @PostConstruct
    public void init() {

        try {
            Resource resource = resourceResolver.getResource("/content/tata-capital-moneyfy/en/mutual-funds/fund-managers/");

            if (resource != null) {

                Page parentPage = resource.adaptTo(Page.class);
                if (parentPage != null) {

                    Iterator<Page> listChildPages = parentPage.listChildren();


                    while (listChildPages.hasNext()) {

                        Page childPage = listChildPages.next();

                        ManagerListingPojo managerListingPojo = new ManagerListingPojo();

                            managerListingPojo.setFundManagerName(childPage.getProperties().get("name").toString());
                            //managerListingPojo.setFunds(Objects.nonNull(childPage.getProperties().get("funds").toString())?childPage.getProperties().get("funds").toString():"-");
                            managerListingPojo.setHighestReturns(Objects.nonNull(childPage.getProperties().get("highestReturns").toString())?childPage.getProperties().get("highestReturns").toString():"-");
                            managerListingPojo.setNoOfFunds(Objects.nonNull(childPage.getProperties().get("totalFunds").toString())?childPage.getProperties().get("totalFunds").toString():"-");
                            managerListingPojo.setAmcId(Objects.nonNull(childPage.getProperties().get("amcId").toString())?childPage.getProperties().get("amcId").toString():"-");
                            managerListingPojo.setTotalFundSize(Objects.nonNull(childPage.getProperties().get("totalFundSize").toString())?childPage.getProperties().get("totalFundSize").toString():"-");
                            managerListingPojo.setPagePath(childPage.getPath().toString().concat(".html"));
                            managerList.add(managerListingPojo);

                    }

                    }


                }



        } catch (Exception e) {

            log.info("Exception : {}", e);
            e.printStackTrace();
        }

    }

    public List<ManagerListingPojo> getManagerList() {
        managerList.size();
        return managerList;
    }

    public String getFundManagerList() {
        Gson gson = new Gson();
        String amcFundList = gson.toJson(getManagerList());
        return amcFundList;
    }

}

