package com.tatacapital.moneyfy.core.services;

import java.util.Map;

public interface MoneyfyIciciCreditCardJourneyService {

    Map<String, String> getConfig();
    boolean getOtpEnabled();
    String otpApiCall(String serviceURI, String authorizationValue, String request);
}
