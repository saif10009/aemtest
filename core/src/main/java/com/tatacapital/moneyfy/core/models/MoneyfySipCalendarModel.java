package com.tatacapital.moneyfy.core.models;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.Source;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.Objects;

@Model(adaptables = SlingHttpServletRequest.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class MoneyfySipCalendarModel {

    @Inject
    Page currentPage;

    @Inject
    @Source("sling-object")
    ResourceResolver resourceResolver;

    private String sipDate;
    private String sipFlag;
    private String lumpSumFlag;

    Page page=null;


    @PostConstruct
    public void init()
    {
        Resource resource = resourceResolver.getResource(currentPage.getPath());
        page = resource.adaptTo(Page.class);
        sipDate = getPageProperty("sipDates");
        sipFlag = getPageProperty("sipFlag");
        lumpSumFlag = getPageProperty("lumpSumFlag");
    }

    public String getPageProperty(String property)
    {
       return Objects.nonNull(page.getProperties().get(property))?page.getProperties().get(property).toString():"-";
    }

    public String getSipDate() {
        return sipDate;
    }

    public String getSipFlag() {
        return sipFlag;
    }

    public String getLumpSumFlag() {
        return lumpSumFlag;
    }

}

