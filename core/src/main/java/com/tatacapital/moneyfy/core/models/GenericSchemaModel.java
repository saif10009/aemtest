package com.tatacapital.moneyfy.core.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.models.annotations.DefaultInjectionStrategy;
import org.apache.sling.models.annotations.Model;
import org.apache.sling.models.annotations.injectorspecific.Self;

import javax.annotation.PostConstruct;
import javax.jcr.*;
import java.util.Iterator;

@Model(
        adaptables = { Resource.class, SlingHttpServletRequest.class },
        defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL
)
public class GenericSchemaModel {

    @Self
    SlingHttpServletRequest request;

    private String schemaString;
    JsonArray schemaJsonArray = new JsonArray();

    @PostConstruct
    public void init() {
        ResourceResolver resourceResolver = request.getResourceResolver();
        Session session = resourceResolver.adaptTo(Session.class);
        Resource res = resourceResolver.getResource(request.getResource().getPath());

        String path = res.getPath();
        String componentPath = res.getResourceType();
        String endpointUrl = null;
        if(componentPath.contains("content/mutual-funds/faq"))
        {
            endpointUrl = path.concat("/multi");
            getMutualFundFaq(session, endpointUrl, resourceResolver);
        }
        else if(componentPath.contains("content/faqs/faqs"))
        {
            endpointUrl = path.concat("/dropdownList");
            getFaqs(session, endpointUrl, resourceResolver);
        }
    }
    public void getMutualFundFaq(Session session,String endpointUrl,ResourceResolver resourceResolver) {
        try {
            Node node = session.getNode(endpointUrl);
            Iterator<Node> nodeList = node.getNodes();
            while (nodeList.hasNext()) {
                Node nodeListNode = nodeList.next();
                String nodePath = nodeListNode.getPath();
                ValueMap valueMap = resourceResolver.getResource(nodePath).adaptTo(ValueMap.class);
                String question = valueMap.get("questions").toString();
                String answer = valueMap.get("description").toString();
                String faqAns = answer.replaceAll("\\<.*?\\>", "");
                String faqAnswereWithoutTag = faqAns.replaceAll("\r\n", "");
                JsonObject questionSchema = new JsonObject();
                questionSchema.addProperty("@type", "Question");
                questionSchema.addProperty("name", question);
                JsonObject ansSchema = new JsonObject();
                ansSchema.addProperty("@type", "Answer");
                ansSchema.addProperty("text", faqAnswereWithoutTag);
                questionSchema.add("acceptedAnswer", ansSchema);
                schemaJsonArray.add(questionSchema);
            }

            schemaString = schemaJsonArray.toString();

        } catch (PathNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void getFaqs(Session session,String endpointUrl,ResourceResolver resourceResolver) {
        try {
            Node node = session.getNode(endpointUrl);
            NodeIterator nodeList = node.getNodes();
            while (nodeList.hasNext()) {
                Node nodeListNode = nodeList.nextNode();
                NodeIterator nodeIterator = nodeListNode.getNodes();
                while(nodeIterator.hasNext())
                {
                    try {
                        Node node1 = nodeIterator.nextNode();
                        NodeIterator nodes = node1.getNodes();
                        while (nodes.hasNext())
                        {
                            Node node2 = nodes.nextNode();
                            NodeIterator nodeIterator1 = node2.getNode("faqList").getNodes();
                            while (nodeIterator1.hasNext()) {
                                Node node3 = nodeIterator1.nextNode();
                                String questions =  node3.getProperty("questions").getString();
                                String description = node3.getProperty("description").getString();
                                String faqDescription = description.replaceAll("\\<.*?\\>", "");
                                String faqDescWithoutTag = faqDescription.replaceAll("\r\n", "");
                                JsonObject questionSchema = new JsonObject();
                                questionSchema.addProperty("@type", "Question");
                                questionSchema.addProperty("name", questions);
                                JsonObject ansSchema = new JsonObject();
                                ansSchema.addProperty("@type", "Answer");
                                ansSchema.addProperty("text", faqDescWithoutTag);
                                questionSchema.add("acceptedAnswer", ansSchema);
                                schemaJsonArray.add(questionSchema);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }

            schemaString = schemaJsonArray.toString();

        } catch (PathNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (RepositoryException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public String getFAQSchemaArr() {
        return this.schemaString;
    }

}

